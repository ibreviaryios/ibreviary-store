//
//  ShortCutItem.m
//  iBreviary
//
//  Created by Leonardo Parenti on 29/02/16.
//  Copyright © 2016 leyo. All rights reserved.
//



#import "ShortCutItem.h"
#import "Home.h"

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 90000


@implementation ShortCutItem

+(SCType)shortCutType:(NSString*)type_string{
    NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                       KtypeVANGELO,@(typeVANGELO),
                       KtypeSUGGEST,@(typeSUGGEST),
                       KtypeSANTO,@(typeSANTO),
                       KtypeMESSA,@(typeMESSA),
                       nil];
    return (SCType)[[d objectForKey:type_string] integerValue];
}



#pragma mark -
#pragma mark FORCE 3D DELEGATE
//==============================
// FORCE 3D DELEGATE
//==============================
+(void)shortCutItemReceived:(UIApplicationShortcutItem *)shortcutItem{
    // react to shortcut item selections
    NSLog(@"A shortcut item was pressed. It was %@.", shortcutItem.localizedTitle);
    
    switch ([ShortCutItem shortCutType:shortcutItem.type]) {
        case typeVANGELO:{
            [URLSCheme openUrlScheme:[URLSCheme urlSchemeForTodayVangel]];
            }
            break;
            
        case typeSUGGEST:{
            [URLSCheme openUrlScheme:[URLSCheme urlSchemeForSuggestedPray]];
        }
            break;
            
        case typeSANTO:{
            //viene controllato da showToday se esiste santo
            [URLSCheme openUrlScheme:[URLSCheme urlSchemeForTodaySantoAtIndex:0]];
        }
            break;

        case typeMESSA:{
            //ancora da fare
        }
            break;
            
        default:
            break;
    }
}



# pragma mark - 3DTouch Support Shortcut Items (dynamic)
//======================================================
// 3DTouch Support Shortcut Items
//======================================================
+ (void)createShortcutItems{
    
    // NOT USED!!!
    
    // create several (dynamic) shortcut items
    UIApplicationShortcutItem *item1 = [[UIApplicationShortcutItem alloc]initWithType:KtypeVANGELO localizedTitle:NSLocalizedString(@"shortcut_vangelo",nil)];
    UIApplicationShortcutItem *item2 = [[UIApplicationShortcutItem alloc]initWithType:KtypeSUGGEST localizedTitle:NSLocalizedString(@"shortcut_suggestion",nil)];
    UIApplicationShortcutItem *item3 = [[UIApplicationShortcutItem alloc]initWithType:KtypeSANTO localizedTitle:NSLocalizedString(@"shortcut_santo",nil)];
    //UIApplicationShortcutItem *item4 = [[UIApplicationShortcutItem alloc]initWithType:KtypeMESSA localizedTitle:NSLocalizedString(@"shortcut_messa",nil)];
    
    // add all items to an array
    NSMutableArray *items = [[NSMutableArray alloc] initWithArray:@[item3, item2, item1]];
    
    // add the array to our app
    [UIApplication sharedApplication].shortcutItems = items;
}

+ (void)createShortcutItemsWithIcons{
    
    if([[UIDevice currentDevice].systemVersion floatValue]<9.f){
        NSLog(@"prevent crash createShortcutItems (iOS < 9)");
        return;
    }
    
    NSLog(@"Creating createShortcutItems");
    
    // VANGELO
    UIApplicationShortcutIcon *vangelo_icon = [UIApplicationShortcutIcon iconWithTemplateImageName:@"short_breviary1.png"];
    
    UIMutableApplicationShortcutItem *item1 = [[UIMutableApplicationShortcutItem alloc]initWithType:KtypeVANGELO localizedTitle:NSLocalizedString(@"shortcut_vangelo",nil) localizedSubtitle:nil icon:vangelo_icon userInfo:nil];

    // SUGGESTED PRAY
    UIApplicationShortcutIcon *suggested_pray = [UIApplicationShortcutIcon iconWithTemplateImageName:@"short_breviary2.png"];
    UIMutableApplicationShortcutItem *item2 = [[UIMutableApplicationShortcutItem alloc]initWithType:KtypeSUGGEST localizedTitle:NSLocalizedString(@"shortcut_suggestion",nil) localizedSubtitle:nil icon:suggested_pray userInfo:nil];
    
    // SANTO
    UIApplicationShortcutIcon *santo_icon = [UIApplicationShortcutIcon iconWithTemplateImageName:@"short_breviary3.png"];
    UIMutableApplicationShortcutItem *item3 = [[UIMutableApplicationShortcutItem alloc]initWithType:KtypeSANTO localizedTitle:NSLocalizedString(@"shortcut_santo",nil) localizedSubtitle:nil icon:santo_icon userInfo:nil];
    
    // MESSA
    UIApplicationShortcutIcon *messa_icon = [UIApplicationShortcutIcon iconWithTemplateImageName:@"short_breviary4.png"];
    UIMutableApplicationShortcutItem *item4 = [[UIMutableApplicationShortcutItem alloc]initWithType:KtypeMESSA localizedTitle:NSLocalizedString(@"shortcut_messa",nil) localizedSubtitle:nil icon:santo_icon userInfo:nil];
    
    // add all items to an array
    NSMutableArray *items = [[NSMutableArray alloc] initWithArray:@[item3, item2, item1]];
    
    [UIApplication sharedApplication].shortcutItems = items;
}


@end

#endif
