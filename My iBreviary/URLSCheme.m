//
//  URLSCheme.m
//  iBreviary
//
//  Created by Leonardo Parenti on 28/02/16.
//  Copyright © 2016 leyo. All rights reserved.
//

#import "URLSCheme.h"
#import "Home.h"
#import "WebBrowser.h"
@interface NSURL ()
@end

@implementation NSURL (custom)


- (NSDictionary *)dictionaryFromQueryComponents
{
    NSMutableDictionary *queryComponents = [NSMutableDictionary dictionary];
    for(NSString *keyValuePairString in [self.query componentsSeparatedByString:@"&"])
    {
        NSArray *keyValuePairArray = [keyValuePairString componentsSeparatedByString:@"="];
        if ([keyValuePairArray count] < 2) continue; // Verify right query.
        NSString *key = [keyValuePairArray objectAtIndex:0];
        NSString *value = [keyValuePairArray objectAtIndex:1];
        NSMutableArray *results = [queryComponents objectForKey:key]; // Remove duplicate key
        if(!results)
        {
            results = [NSMutableArray arrayWithCapacity:1];
            [queryComponents setObject:results forKey:key];
        }
        [results addObject:value];
    }
    return queryComponents;
}

@end


@implementation URLSCheme

#pragma mark - TYPEs
//==================================
//     TYPEs
//==================================
+(NSDictionary*)dictionary{
    NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                       @"KtypeVANGELO",@(todayItem),
                       @"KtypeSUGGEST",@(todayVangelo),
                       @"KtypeSANTO",@(todaySanto),
                       @"KtypeMESSA",@(lastMessa),
                       nil
                       ];
    return d;
}


+(URLType)schemeTypeForUrl:(NSURL*)url{
    NSDictionary *queryParameter = [url dictionaryFromQueryComponents];

    if([[url host] isEqualToString:@"mostra"]){
        if([queryParameter objectForKey:@"vangelo"]){
            return todayVangelo;
        }
        return todayItem;
    }
    
    if([[url host] isEqualToString:@"santo"]){
        return todaySanto;
    }
    
    if(0){
        return lastMessa;
    }

    return UnKnown_scheme;
}


+(Ora_type)getOraType{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"H"];
    int hour = [[formatter stringFromDate:date] intValue];
    
    Ora_type ora_type = nessuna_preghiera;
    
    if(0<=hour && hour<5) ora_type = nessuna_preghiera;
    else if(15<=hour && hour<16) ora_type = nessuna_preghiera;
    else if(5<=hour && hour<7) ora_type = ufficio;
    else if(7<=hour && hour<9) ora_type = lodi;
    else if(9<=hour && hour<15) ora_type = oraMedia;
    else if(16<=hour && hour<21) ora_type = vespri;
    else if(21<=hour && hour<24) ora_type = compieta;
    
    NSLog(@"checkOraType:%i from hour:%i date:%@",ora_type,hour,date);
    return ora_type;
}


#pragma mark - caller
//==================================
//     caller
//==================================

// NSString <------------
+(void)openUrlSchemeWithString:(NSString*)url_string{
    NSURL *url = [NSURL URLWithString:url_string];
    [URLSCheme openUrlScheme:url];
}

+(void)openUrlScheme:(NSURL*)url{
    NSLog(@"%s:%@",__PRETTY_FUNCTION__ , url);
    
    //se non ha scheme iBreviary:// aprilo nel browser
    if(![[[url scheme] uppercaseString] isEqualToString:@"IBREVIARY"]){
        //wrong urlscheme
        return;
    }
    
    
    
    NSString *mex = [NSString stringWithFormat:@"scheme:%@\n", [url scheme]]; //scheme
    mex = [mex stringByAppendingFormat:@"host: %@\n", [url host]]; //host
    mex = [mex stringByAppendingFormat:@"pathComponent: %@\n", [url pathComponents]]; //paths
    mex = [mex stringByAppendingFormat:@"path: %@\n", [url path]]; //path
    mex = [mex stringByAppendingFormat:@"query: %@\n", [url query]]; //es variabile = true
    NSLog(@"URLScheme DebugMex:%@",mex);
    


    
    switch ([URLSCheme schemeTypeForUrl:url]) {
        case todayItem:{
            if([URLSCheme getOraType] == nessuna_preghiera){
                [[NSOperationQueue new] addOperationWithBlock:^{ [[Home sharedInstance] mostraNotificaNoSuggestPray];}];
            }
            else{
                [[NSOperationQueue new] addOperationWithBlock:^{ [Home showTodayItem:[url query]];}];
            }
        }
            break;
        case todayVangelo:{
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [Home showTodayItem:[url query]];
            
            }];
        }
            break;
            
        case todaySanto:{
            if(![url query] || [url query].length == 0){
                
                [[NSOperationQueue new] addOperationWithBlock:^{ [Home showSantoAtPos:0];}];
            }
            [[NSOperationQueue new] addOperationWithBlock:^{ [Home showSantoAtPos:[url query]];}];
        }
            break;
            
        case lastMessa:{
            [[NSOperationQueue new] addOperationWithBlock:^{
                NSLog(@"lastMessa da implementare");
            }];

        }
            break;
            
        default:
            break;
    }

}

#pragma mark -
#pragma mark URLSCHEME TODAY
//============================
// URLSCHEME TODAY
//============================

//-> preghiera
+(NSURL*)urlSchemeForOraType:(Ora_type)ora_type{
    NSMutableString *cmd_string = [[NSMutableString alloc] initWithString:@"iBreviary://mostra?q="];
    
    switch (ora_type) {
        case ufficio:
            [cmd_string appendFormat:@"ufficio"];
            break;
        case lodi:
            [cmd_string appendFormat:@"lodi"];
            break;
        case oraMedia:
            [cmd_string appendFormat:@"oraMedia"];
            break;
        case vespri:
            [cmd_string appendFormat:@"vespri"];
            break;
        case compieta:
            [cmd_string appendFormat:@"compieta"];
            break;
        case nessuna_preghiera:
            //ritorno NIL
            return nil;
            break;
            
        default:
            break;
    }
    NSURL *urlschemeOraType = [NSURL URLWithString:cmd_string];
    
    return urlschemeOraType;
}

//-> shortcut preghiera
+(NSURL*)urlSchemeForSuggestedPray{
    Ora_type ora_type = [URLSCheme getOraType];
    NSMutableString *cmd_string = [[NSMutableString alloc] initWithString:@"iBreviary://mostra"];
    
    switch (ora_type) {
        case ufficio:
            [cmd_string appendFormat:@"?q=ufficio"];
            break;
        case lodi:
            [cmd_string appendFormat:@"?q=lodi"];
            break;
        case oraMedia:
            [cmd_string appendFormat:@"?q=oraMedia"];
            break;
        case vespri:
            [cmd_string appendFormat:@"?q=vespri"];
            break;
        case compieta:
            [cmd_string appendFormat:@"?q=compieta"];
            break;
        case nessuna_preghiera:
            //ritorno solo mostra
            break;
            
        default:
            break;
    }
    NSURL *urlschemeOraType = [NSURL URLWithString:cmd_string];
    
    return urlschemeOraType;
    
}



//-> Vangelo di Oggi
+(NSURL*)urlSchemeForTodayVangel{
    NSMutableString *cmd_string = [[NSMutableString alloc] initWithString:@"iBreviary://mostra?q="];
    [cmd_string appendFormat:@"vangelo"];
    NSURL *urlschemeVangelo = [NSURL URLWithString:cmd_string];
    return urlschemeVangelo;
}

//-> Santo di Oggi
+(NSURL*)urlSchemeForTodaySantoAtIndex:(NSInteger)pos{
    NSMutableString *cmd_string = [[NSMutableString alloc] initWithString:@"iBreviary://santo?pos="];
    [cmd_string appendFormat:@"%i",(int)pos];
    NSURL *urlschemeSanto = [NSURL URLWithString:cmd_string];
    return urlschemeSanto;
}

@end
