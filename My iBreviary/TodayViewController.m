//
//  TodayViewController.m
//  My iBreviary
//
//  Created by leyo on 04/11/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>


#define URL_TODAY_WIDGET @"http://ibreviary.com/myBreviaryWidget.php?"
#define WIDGET_DAY_LOADED @"WIDGET_DAY_LOADED"
#define WIDGET_DAY_TITLE @"WIDGET_DAY_TITLE"
#define WIDGET_DAY_VANGELO @"WIDGET_DAY_VANGELO"
#define WIDGET_DAY_VANGELO_TITLE @"WIDGET_DAY_VANGELO_TITLE"
#define WIDGET_SANTO_ARRAY @"WIDGET_SANTO_ARRAY"
#define WIDGET_AGGIORNAMENTO_BOOL @"WIDGET_AGGIORNAMENTO_BOOL"
#define WIDGET_HEIGHT @"WIDGET_HEIGHT"

@interface UIView (TodayUpdateAutoLayoutConstraints)

@end

//==================
// UIView
//==================
#pragma mark UIView
@implementation UIView (TodayUpdateAutoLayoutConstraints)
- (BOOL) setConstraintConstant:(CGFloat)constant forAttribute:(NSLayoutAttribute)attribute
{
    NSLayoutConstraint * constraint = [self constraintForAttribute:attribute];
    if(constraint)
    {
        [constraint setConstant:constant];
        return YES;
    }else
    {
        [self.superview addConstraint: [NSLayoutConstraint constraintWithItem:self attribute:attribute relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:constant]];
        return NO;
    }
}
- (CGFloat) constraintConstantforAttribute:(NSLayoutAttribute)attribute
{
    NSLayoutConstraint * constraint = [self constraintForAttribute:attribute];
    if (constraint) {
        return constraint.constant;
    }else
    {
        return NAN;
    }
}
- (NSLayoutConstraint*) constraintForAttribute:(NSLayoutAttribute)attribute
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstAttribute = %d && firstItem = %@", attribute, self];
    NSArray *fillteredArray = [[self.superview constraints] filteredArrayUsingPredicate:predicate];
    if(fillteredArray.count == 0)
    {
        return nil;
    }else
    {
        return fillteredArray.firstObject;
    }
}
- (void)hideByHeight:(BOOL)hidden
{
    [self hideView:hidden byAttribute:NSLayoutAttributeHeight];
}
- (void)hideByWidth:(BOOL)hidden
{
    [self hideView:hidden byAttribute:NSLayoutAttributeWidth];
}
- (void)hideView:(BOOL)hidden byAttribute:(NSLayoutAttribute)attribute
{
    if (self.hidden != hidden) {
        CGFloat constraintConstant = [self constraintConstantforAttribute:attribute];
        if (hidden)
        {
            if (!isnan(constraintConstant)) {
                self.alpha = constraintConstant;
            }else
            {
                CGSize size = [self getSize];
                self.alpha = (attribute == NSLayoutAttributeHeight)?size.height:size.width;
            }
            [self setConstraintConstant:0 forAttribute:attribute];
            self.hidden = YES;
        }else
        {
            if (!isnan(constraintConstant) ) {
                self.hidden = NO;
                [self setConstraintConstant:self.alpha forAttribute:attribute];
                self.alpha = 1;
            }
        }
    }
}
@end

//===================
// NSString
//===================
#pragma mark NSString
@implementation NSString (html)
+ (NSString *)stringFromInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if (orientation == UIInterfaceOrientationPortrait) return @"UIInterfaceOrientationPortrait";
    if (orientation ==  UIInterfaceOrientationPortraitUpsideDown) return @"UIInterfaceOrientationPortraitUpsideDown";
    if (orientation ==  UIInterfaceOrientationLandscapeLeft) return @"UIInterfaceOrientationLandscapeLeft";
    if (orientation ==  UIInterfaceOrientationLandscapeRight) return @"UIInterfaceOrientationLandscapeRight";
    return nil;
}

- (NSString *)gtm_stringByEscapingHTMLUsingTable:(HTMLEscapeMap*)table
ofSize:(NSUInteger)size
escapingUnicode:(BOOL)escapeUnicode {
    NSUInteger length = [self length];
    if (!length) {
        return self;
    }
    
    NSMutableString *finalString = [NSMutableString string];
    NSMutableData *data2 = [NSMutableData dataWithCapacity:sizeof(unichar) * length];
    
    // this block is common between GTMNSString+HTML and GTMNSString+XML but
    // it's so short that it isn't really worth trying to share.
    const unichar *buffer = CFStringGetCharactersPtr((CFStringRef)self);
    if (!buffer) {
        // We want this buffer to be autoreleased.
        NSMutableData *data = [NSMutableData dataWithLength:length * sizeof(UniChar)];
        if (!data) {
            // COV_NF_START - Memory fail case
            //LOG(@"couldn't alloc buffer");
            return nil;
            // COV_NF_END
        }
        [self getCharacters:[data mutableBytes]];
        buffer = [data bytes];
    }
    
    if (!buffer || !data2) {
        // COV_NF_START
        //LOG(@"Unable to allocate buffer or data2");
        return nil;
        // COV_NF_END
    }
    
    unichar *buffer2 = (unichar *)[data2 mutableBytes];
    
    NSUInteger buffer2Length = 0;
    
    for (NSUInteger i = 0; i < length; ++i) {
        HTMLEscapeMap *val = bsearch(&buffer[i], table,
                                     size / sizeof(HTMLEscapeMap),
                                     sizeof(HTMLEscapeMap), EscapeMapCompare);
        if (val || (escapeUnicode && buffer[i] > 127)) {
            if (buffer2Length) {
                CFStringAppendCharacters((CFMutableStringRef)finalString,
                                         buffer2,
                                         buffer2Length);
                buffer2Length = 0;
            }
            if (val) {
                [finalString appendString:val->escapeSequence];
            }
            else {
                if(escapeUnicode && buffer[i] > 127) //LOG(@"Illegal Character");
                    [finalString appendFormat:@"&#%d;", buffer[i]];
            }
        } else {
            buffer2[buffer2Length] = buffer[i];
            buffer2Length += 1;
        }
    }
    if (buffer2Length) {
        CFStringAppendCharacters((CFMutableStringRef)finalString,
                                 buffer2,
                                 buffer2Length);
    }
    return finalString;
}

- (NSString *)gtm_stringByEscapingForHTML {
    return [self gtm_stringByEscapingHTMLUsingTable:gUnicodeHTMLEscapeMap
                                             ofSize:sizeof(gUnicodeHTMLEscapeMap)
                                    escapingUnicode:NO];
} // gtm_stringByEscapingHTML

- (NSString *)gtm_stringByEscapingForAsciiHTML {
    return [self gtm_stringByEscapingHTMLUsingTable:gAsciiHTMLEscapeMap
                                             ofSize:sizeof(gAsciiHTMLEscapeMap)
                                    escapingUnicode:YES];
} // gtm_stringByEscapingAsciiHTML

- (NSString *)gtm_stringByUnescapingFromHTML {
    NSRange range = NSMakeRange(0, [self length]);
    NSRange subrange = [self rangeOfString:@"&" options:NSBackwardsSearch range:range];
    
    // if no ampersands, we've got a quick way out
    if (subrange.length == 0) return self;
    NSMutableString *finalString = [NSMutableString stringWithString:self];
    do {
        NSRange semiColonRange = NSMakeRange(subrange.location, NSMaxRange(range) - subrange.location);
        semiColonRange = [self rangeOfString:@";" options:0 range:semiColonRange];
        range = NSMakeRange(0, subrange.location);
        // if we don't find a semicolon in the range, we don't have a sequence
        if (semiColonRange.location == NSNotFound) {
            continue;
        }
        NSRange escapeRange = NSMakeRange(subrange.location, semiColonRange.location - subrange.location + 1);
        NSString *escapeString = [self substringWithRange:escapeRange];
        NSUInteger length = [escapeString length];
        // a squence must be longer than 3 (&lt;) and less than 11 (&thetasym;)
        if (length > 3 && length < 11) {
            if ([escapeString characterAtIndex:1] == '#') {
                unichar char2 = [escapeString characterAtIndex:2];
                if (char2 == 'x' || char2 == 'X') {
                    // Hex escape squences &#xa3;
                    NSString *hexSequence = [escapeString substringWithRange:NSMakeRange(3, length - 4)];
                    NSScanner *scanner = [NSScanner scannerWithString:hexSequence];
                    unsigned value;
                    if ([scanner scanHexInt:&value] &&
                        value < USHRT_MAX &&
                        value > 0
                        && [scanner scanLocation] == length - 4) {
                        unichar uchar = value;
                        NSString *charString = [NSString stringWithCharacters:&uchar length:1];
                        [finalString replaceCharactersInRange:escapeRange withString:charString];
                    }
                    
                } else {
                    // Decimal Sequences &#123;
                    NSString *numberSequence = [escapeString substringWithRange:NSMakeRange(2, length - 3)];
                    NSScanner *scanner = [NSScanner scannerWithString:numberSequence];
                    int value;
                    if ([scanner scanInt:&value] &&
                        value < USHRT_MAX &&
                        value > 0
                        && [scanner scanLocation] == length - 3) {
                        unichar uchar = value;
                        NSString *charString = [NSString stringWithCharacters:&uchar length:1];
                        [finalString replaceCharactersInRange:escapeRange withString:charString];
                    }
                }
            } else {
                // "standard" sequences
                for (unsigned i = 0; i < sizeof(gAsciiHTMLEscapeMap) / sizeof(HTMLEscapeMap); ++i) {
                    if ([escapeString isEqualToString:gAsciiHTMLEscapeMap[i].escapeSequence]) {
                        [finalString replaceCharactersInRange:escapeRange withString:[NSString stringWithCharacters:&gAsciiHTMLEscapeMap[i].uchar length:1]];
                        break;
                    }
                }
            }
        }
    } while ((subrange = [self rangeOfString:@"&" options:NSBackwardsSearch range:range]).length != 0);
    return finalString;
} // gtm_stringByUnescapingHTML

-(NSString *)stringByRemovingHTMLtag {
    NSRange r;
    NSString *s = [self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(BOOL)isEmpty{
    NSString *s = [self copy];
    return (s==nil) || [s isEqualToString:@""];
}

-(BOOL)uguale:(NSString*)s{
    return [self isEqualToString:s];
}

@end

@interface TodayViewController () <NCWidgetProviding>

@end

@implementation TodayViewController


@synthesize defaults,ora_type;
//TITOLO
@synthesize titolo_label, rotella;
//SANTO AREA
@synthesize santo_area;
//ACTION AREA
@synthesize action_area, action_logo, action_label, action_btn, action_line;
//VANGELO AREA
@synthesize  vangelo_area,vangelo_logo, vangelo_label, vangelo_line, vangelo_line_vertical,vangelo_text,vangelo_ipad_btn;


#pragma mark -
#pragma mark init
//=========================
// INIT
//=========================
- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        //ABILITARE questa linea per intercettare cambiamento NSUserDefaults dell'applicazione iBreviary
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDefaultsDidChange:) name:NSUserDefaultsDidChangeNotification object:nil];
        }
    return self;
}

#pragma mark -
#pragma mark DEFAULT GROUP
//=========================
// DEFAULT GROUP
//=========================
-(NSUserDefaults*)defaults{
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"];
    return sharedDefaults;
}

-(BOOL)isIpad{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}

#pragma mark -
#pragma mark viewDidLoad
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    //[self.view setConstraintConstant:self.view.frame.size.width forAttribute:NSLayoutAttributeWidth];
    [super viewDidLoad];
    //NSLog(@"TodayViewController:viewDidLoad");
    [self gui];
    [self modifica];
    // Do any additional setup after loading the view from its nib.
}

-(void)gui{
    UIColor *mainColor = [UIColorFromRGB(0Xcacaca) colorWithAlphaComponent:0.8];
    UIColor *secondColor = [UIColorFromRGB(0Xf0b26e) colorWithAlphaComponent:0.8];
    
    [titolo_label setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.95]];
 
    //->ACTION AREA
    [action_line setBackgroundColor:secondColor];
    [action_label setTextColor:mainColor];
    if(!self.isIpad){
        [action_label setPreferredMaxLayoutWidth:70];
    }
    [action_btn.layer setCornerRadius:5];
    [action_btn.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:0.7] CGColor]];
    [action_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [action_btn setTitleColor:secondColor forState:UIControlStateHighlighted];
    [action_btn.titleLabel setNumberOfLines:1];
    [action_btn.layer setBorderWidth:1.2];
    [action_btn.layer setMasksToBounds:YES];
    
    //->VANGELO AREA
    [vangelo_line setBackgroundColor:secondColor];
    [vangelo_line_vertical setBackgroundColor:secondColor];
    [vangelo_line_vertical setHidden:YES];
    [vangelo_label setTextColor:mainColor];
    vangelo_text.textContainer.maximumNumberOfLines = self.isIpad?0:11;
    vangelo_text.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;
    [vangelo_text setTextContainerInset:UIEdgeInsetsMake(-10, 0, 0, 0)];

    [vangelo_ipad_btn setHidden:NO];
    [vangelo_ipad_btn.layer setCornerRadius:5];
    [vangelo_ipad_btn.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:0.7] CGColor]];
    [vangelo_ipad_btn.layer setBorderWidth:1.2];
    [vangelo_ipad_btn setTitle:[NSString stringWithFormat:@" %@",self.isIpad?NSLocalizedString(@"apri", nil):NSLocalizedString(@"leggitutto", nil)] forState:UIControlStateNormal];
    
    //LOCALIZZAZIONE
    [action_label setText:NSLocalizedString(@"PreghieraConsigliataLabel", nil)];
}


#pragma mark - Appear
//===================
//APPEAR
//===================
- (void)viewWillAppear:(BOOL)animated{
    //NSLog(@"Widget-TODAY_viewWillAppear");
    [self verificoGiornoDisponibile];
    [self checkOraType];
    [super viewWillAppear:animated];
}

-(void)verificoGiornoDisponibile{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    //NSLog(@"giorno in memoria:%@ oggi:%@",[self.defaults objectForKey:WIDGET_DAY_LOADED],[formatter stringFromDate:date]);
    BOOL daAggiornare = ![[formatter stringFromDate:date] isEqualToString:[self.defaults objectForKey:WIDGET_DAY_LOADED]];
//#warning debug da Aggiornare SEMPRE
    //daAggiornare = YES;
    if(daAggiornare){
        [self todayWebservice];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    //NSLog(@"Widget-TODAY_viewDidAppear");
    [super viewDidAppear:animated];
    [self updateView];
}

#pragma mark - Servizi
//===================
//SERVIZI
//===================
-(void)checkOraType{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"H"];
    int hour = [[formatter stringFromDate:date] intValue];
    //NSLog(@"checkOraType:%i from date:%@",hour,date);
    if(0<=hour && hour<5) ora_type = nessuna_preghiera;
    else if(15<=hour && hour<16) ora_type = nessuna_preghiera;
    else if(5<=hour && hour<7) ora_type = ufficio;
    else if(7<=hour && hour<9) ora_type = lodi;
    else if(9<=hour && hour<15) ora_type = oraMedia;
    else if(16<=hour && hour<21) ora_type = vespri;
    else if(21<=hour && hour<24) ora_type = compieta;
    //#warning debug checkora
    //ora_type = oraMedia;
    if([NSThread isMainThread]) [self updateButton];
    else [self performSelectorOnMainThread:@selector(updateButton) withObject:nil waitUntilDone:NO];
}


-(void)todayWebservice{
    [self rotellaSwitch:YES];
    [self.defaults setBool:YES forKey:WIDGET_AGGIORNAMENTO_BOOL];
    [self.defaults synchronize];
    NSString *lang = NSLocalizedString(@"widget_lingua", nil);
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSString *urlString = [NSString stringWithFormat:@"%@l=%@&d=%@",URL_TODAY_WIDGET,lang,[f stringFromDate:[NSDate date]]];
    
// ====================================
// #warning widget debug ita 2014-11-16
// urlString = [NSString stringWithFormat:@"%@l=it&d=2014-11-16",URL_TODAY_WIDGET];
// ====================================
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    //NSLog(@"todayWebservice:%@",url);
    [NSURLConnection sendAsynchronousRequest:theRequest
                                       queue:[[NSOperationQueue alloc] init]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *error) {
                                                    if(error!=nil){
                                                            // NSLog(@"todayWebservice[ERR]: %@",[error description]);
                                                        }
                                                    else{
                                                        NSMutableDictionary *feed = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil]];
                                                        //NSLog(@"FINE JSON widget:%@",[NSString stringWithFormat:@"%@", [feed objectForKey:@"titolo"]]);
                                                        [self.defaults setObject:[NSString stringWithFormat:@"%@", [feed objectForKey:@"titolo"]] forKey:WIDGET_DAY_TITLE];
                                                        //[self.defaults setObject:[NSString stringWithFormat:@"%@", [feed objectForKey:@"vangelo_contenuto"]] forKey:WIDGET_DAY_VANGELO];
                                                        
                                                        //DEBUG NSLOG
                                                        //NSString *t = [feed objectForKey:@"vangelo_contenuto"];
                                                        // NSLog(@"vangelo_contenuto:%@", t);
                                                        
                                                        [self.defaults setObject:[NSString stringWithFormat:@"%@", [[feed objectForKey:@"vangelo_contenuto"] gtm_stringByUnescapingFromHTML]] forKey:WIDGET_DAY_VANGELO];
                                                        
                                                        NSMutableArray *saint_array = [NSMutableArray new];
                                                        for (NSDictionary *row in [feed objectForKey:@"santo"])
                                                             [saint_array addObject:row];
                                                        [self.defaults setObject:saint_array forKey:WIDGET_SANTO_ARRAY];
                                                        
                                                        
                                                        [self.defaults setObject:[NSString stringWithFormat:@"%@", [feed objectForKey:@"vangelo_titolo"]] forKey:WIDGET_DAY_VANGELO_TITLE];
                                                        
                                                        
                                                        NSDateFormatter *formatter = [NSDateFormatter new];
                                                        [formatter setDateFormat:@"yyyy-MM-dd"];
                                                        [self.defaults setObject:[formatter stringFromDate:[NSDate date]] forKey:WIDGET_DAY_LOADED];
                                                        [self.defaults setBool:NO forKey:WIDGET_AGGIORNAMENTO_BOOL];
                                                        [self.defaults synchronize];
                                                        [self updateView];
                                                        }
                                            [self rotellaSwitch:NO];
                               
                                        }];
}


#pragma mark - UPDATE
//===================
//UPDATE BTN ACTION
//===================
-(void)updateButton{
    NSString *titolo_bottone = @"";

    switch (ora_type) {
        case ufficio:
        {
            //NSLog(@"update_ufficio");
            titolo_bottone = NSLocalizedString(@"Ufficio_btn",nil);
            break;
        }
        case lodi:
        {
            //NSLog(@"update_lodi");
            titolo_bottone = NSLocalizedString(@"Lodi_btn",nil);
            break;
        }
        case oraMedia:
        {
            //NSLog(@"update_oraMedia");
            titolo_bottone = NSLocalizedString(@"OraMedia_btn",nil);
            break;
        }
        case vespri:
        {
            //NSLog(@"update_oraMedia");
            titolo_bottone = NSLocalizedString(@"Vespri_btn",nil);
            break;
        }
        case compieta:
        {
            //NSLog(@"update_oraMedia");
            titolo_bottone = NSLocalizedString(@"Compieta_btn",nil);
            break;
        }
        case nessuna_preghiera:
        {
            //NSLog(@"update_nessuna_preghiera");
            break;
        }
        default:
            //do nothing
            break;
    }

    [action_area setHidden:(ora_type == nessuna_preghiera)];
    //NSLog(@"action_area:%@",action_area);
    [action_btn setTitle:titolo_bottone forState:UIControlStateNormal];
    [action_area setConstraintConstant:action_area.hidden?0:61 forAttribute:NSLayoutAttributeHeight];
    //NSLog(@"action_area:%@",action_area);
    
}

//===================
//UPDATE VIEW
//===================
-(void)updateView{
    //NSLog(@"updateView");
    if([NSThread isMainThread]){
        [self modifica];
    }
    else [self performSelectorOnMainThread:@selector(modifica) withObject:nil waitUntilDone:YES];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            //NSLog(@"chiudirotella");
            [rotella setHidden:YES];
            [rotella stopAnimating];
        }];

}
/*
santo":[{
    "santo_titolo": "Santa Margherita di Scozia",
    "santo_type": "Memoria Facoltativa",
    "santo_pos": 0
    }]
*/
-(void)modifica{
    if(![self.titolo_label.text isEqualToString:[self.defaults objectForKey:WIDGET_DAY_TITLE]]){
        //NSLog(@"modifica!");
        [self.titolo_label setText:[self.defaults objectForKey:WIDGET_DAY_TITLE]];
        NSString *vangelo = [self.defaults objectForKey:WIDGET_DAY_VANGELO];
        //NSLog(@"htmlString\n\n================\n%@\n\n================\n",vangelo);
        NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[vangelo dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        [self.vangelo_text setAttributedText:attrStr];
        //NSLog(@"vangelo_text:%@",NSStringFromCGRect(vangelo_text.frame));
        [self buildSaint];
        [rotella setHidden:YES];
        [rotella stopAnimating];
        [vangelo_area layoutSubviews];
        [santo_area layoutSubviews];
        [self.view layoutSubviews];
        CGFloat height = self.view.frame.size.height;
        [self.defaults setFloat:height forKey:WIDGET_HEIGHT];
        [self.defaults synchronize];
        }
//    else// NSLog(@"non modifica!");
}

#pragma mark - BUILD SANTO AREA
//=============================
//BUILD SANTO AREA
//=============================
-(void)buildSaint{
        //NSLog(@"buildSaint:%@",[self.defaults objectForKey:WIDGET_SANTO_ARRAY]);
    UIColor *mainColor = [UIColorFromRGB(0Xcacaca) colorWithAlphaComponent:0.8];
    UIColor *secondColor = [UIColorFromRGB(0Xf0b26e) colorWithAlphaComponent:0.8];
    NSArray *santo_array = [NSArray arrayWithArray:[self.defaults objectForKey:WIDGET_SANTO_ARRAY]];
    if(santo_array.count==0) return;
    for(UIView *row in santo_area.subviews){
        [row removeFromSuperview];
    }
    float row_height = 35;
    for(int i = 0; i < santo_array.count; i++)
    {
        NSDictionary *row = [santo_array objectAtIndex:i];
        UIButton *bottone = [UIButton buttonWithType:UIButtonTypeCustom];
        [bottone setBackgroundColor:[UIColor clearColor]];
        [bottone setTag:[[row objectForKey:@"santo_pos"] integerValue]];
        [bottone setTitle:[[row objectForKey:@"santo_titolo"] uppercaseString] forState:UIControlStateNormal];
        [bottone setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.93] forState:UIControlStateNormal];
        [bottone setTitleColor:secondColor forState:UIControlStateHighlighted];
        [bottone setImage:[UIImage imageNamed:@"logo_santo.png"] forState:UIControlStateNormal];
        [bottone setAdjustsImageWhenHighlighted:NO];
        [bottone setTitleEdgeInsets:UIEdgeInsetsMake(-8.0, 21.5, 0.0, 0.0)];
        [bottone setImageEdgeInsets:UIEdgeInsetsMake(-1.0, 8.0, 0.0, 0.0)];
        [bottone.titleLabel setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:12]];
        [bottone.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
        [bottone setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [bottone setFrame:CGRectMake(20, (i*row_height), self.view.frame.size.width-20, row_height)];
        [bottone addTarget:self action:@selector(showSanto:) forControlEvents:UIControlEventTouchUpInside];
        UILabel *type = [[UILabel alloc] initWithFrame:CGRectMake(58, (i*row_height)+18.5, santo_area.frame.size.width-63, 10)];
        [type setFont:[UIFont fontWithName:FONT_HELVETICANEUE_ITALIC size:9]];
        [type setTextColor:mainColor];
        [type setText:[row objectForKey:@"santo_type"]];
        [santo_area addSubview:type];
        [santo_area addSubview:bottone];
        
    }
    float santoheight = santo_array.count*row_height;
    [santo_area setConstraintConstant:santoheight forAttribute:NSLayoutAttributeHeight];
}
#pragma mark - ROTELLA
//===================
//ROTELLA
//===================
-(void)rotellaSwitch:(BOOL)attiva{
    //NSLog(@"rotellaSwitch:%@",attiva?@"YES":@"NO");
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if(attiva) {
            [rotella startAnimating];
            [rotella setHidden:NO];
            //NSLog(@"attivata(rotella)");
        }
        else{
            [rotella stopAnimating];
            [rotella setHidden:YES];
            //NSLog(@"spenta(rotella)");
        }
    }];
    
}


#pragma mark - Disappear
//===================
//DISAPPEAR
//===================
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark TodayWidget Default delegate
//=========================
// TodayWidget Default delegate
//=========================
- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    //NSLog(@"MyBreviary:widgetPerformUpdateWithCompletionHandler!");
    //completionHandler(NCUpdateResultNewData);
}

- (void)userDefaultsDidChange:(NSNotification *)notification{
    //NSLog(@"widget:userDefaultsDidChange");
    //[self updateView];
}

/*
 05-07 Ufficio
 07-09: Lodi
 09-15h: Ora media
 18-21 Vespri
 21-24 compieta
 */

BOOL fatto = NO;

- (UIEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(UIEdgeInsets)defaultMarginInsets
{
    return UIEdgeInsetsZero;
}
/*
- (UIEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(UIEdgeInsets)defaultMarginInsets{
        //self.preferredContentSize = CGSizeMake(0, 235);
    //NSLog(@"widgetMarginInsetsForProposedMarginInsets");
    if(defaultMarginInsets.left != -30.f){
        return UIEdgeInsetsMake(defaultMarginInsets.top, -30.f, defaultMarginInsets.bottom, defaultMarginInsets.right);
    }
    //NSLog(@"default!");
    return defaultMarginInsets;
}
*/
#pragma mark -
#pragma mark Action
//=========================
// Action
//=========================#pragma mark -
#pragma mark Action
//=========================
// Action
//=========================
-(IBAction)showPreghieraConsigliata:(UIButton*)sender {
    NSMutableString *cmd_string = [[NSMutableString alloc] initWithString:@"iBreviary://mostra?q="];
    switch (ora_type) {
        case ufficio:
            [cmd_string appendFormat:@"ufficio"];
            break;
        case lodi:
            [cmd_string appendFormat:@"lodi"];
            break;
        case oraMedia:
            [cmd_string appendFormat:@"oraMedia"];
            break;
        case vespri:
            [cmd_string appendFormat:@"vespri"];
            break;
        case compieta:
            [cmd_string appendFormat:@"compieta"];
            break;
            
        default:
            return;
            break;
    }
    NSURL *customURL = [NSURL URLWithString:cmd_string];
    [self.extensionContext openURL:customURL
                 completionHandler:^(BOOL success) {
                     if(success == YES){
                         // NSLog(@"iBreviary opened");
                     }
                 }];
}

-(IBAction)showVangelo:(UIButton*)sender {
    NSMutableString *cmd_string = [[NSMutableString alloc] initWithString:@"iBreviary://mostra?q="];
    [cmd_string appendFormat:@"vangelo"];
    NSURL *customURL = [NSURL URLWithString:cmd_string];
    [self.extensionContext openURL:customURL
                 completionHandler:^(BOOL success) {
                     if(success == YES){
                         // NSLog(@"iBreviary opened");
                     }
                 }];
}

-(IBAction)showSanto:(UIButton*)sender {
    //NSLog(@"showSanto:%i",(int)sender.tag);
    NSMutableString *cmd_string = [[NSMutableString alloc] initWithString:@"iBreviary://santo?pos="];
    [cmd_string appendFormat:@"%i",(int)sender.tag];
    __block NSURL *customURL = [NSURL URLWithString:cmd_string];
    
    [self.extensionContext openURL:customURL
                 completionHandler:^(BOOL success) {
                     if(success == YES){
                         // NSLog(@"iBreviary opened:%@",customURL);
                     }
                 }];
}

@end
