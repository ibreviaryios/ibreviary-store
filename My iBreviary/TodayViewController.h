//
//  TodayViewController.h
//  My iBreviary
//
//  Created by leyo on 04/11/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodayViewController : UIViewController

@property (nonatomic, readonly) NSUserDefaults *defaults;
@property (nonatomic) Ora_type ora_type;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *rotella;

//TITOLO
@property (weak, nonatomic) IBOutlet UILabel *titolo_label;
//SANTO AREA
@property (weak, nonatomic) IBOutlet UIView *santo_area;
//ACTION AREA
@property (weak, nonatomic) IBOutlet UIView *action_area;
@property (weak, nonatomic) IBOutlet UIImageView *action_logo;
@property (weak, nonatomic) IBOutlet UILabel *action_label;
@property (weak, nonatomic) IBOutlet UIButton *action_btn;
@property (weak, nonatomic) IBOutlet UIView *action_line;
//VANGELO AREA
@property (weak, nonatomic) IBOutlet UIView *vangelo_area;
@property (weak, nonatomic) IBOutlet UIImageView *vangelo_logo;
@property (weak, nonatomic) IBOutlet UILabel *vangelo_label;
@property (weak, nonatomic) IBOutlet UITextView *vangelo_text;
@property (weak, nonatomic) IBOutlet UIWebView *vangelo_wevView;
@property (weak, nonatomic) IBOutlet UIView *vangelo_line_vertical;
@property (weak, nonatomic) IBOutlet UIButton *vangelo_ipad_btn;
@property (weak, nonatomic) IBOutlet UIView *vangelo_line;
@end
