//
//  ShortCutItem.h
//  iBreviary
//
//  Created by Leonardo Parenti on 29/02/16.
//  Copyright © 2016 leyo. All rights reserved.
//

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 90000

typedef enum : NSUInteger {
    typeVANGELO,
    typeSUGGEST,
    typeSANTO,
    typeMESSA,
} SCType;

#define KtypeVANGELO @"typeVANGELO"
#define KtypeSUGGEST @"typeSUGGEST"
#define KtypeSANTO @"typeSANTO"
#define KtypeMESSA @"typeMESSA"

#import <Foundation/Foundation.h>

@interface ShortCutItem : NSObject

// SHORTCUTITEM
+(void)createShortcutItems;
+(void)createShortcutItemsWithIcons;
+(void)shortCutItemReceived:(UIApplicationShortcutItem *)shortcutItem;

@end

#endif