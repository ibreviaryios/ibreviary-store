//
//  URLSCheme.h
//  iBreviary
//
//  Created by Leonardo Parenti on 28/02/16.
//  Copyright © 2016 leyo. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum : NSUInteger {
    todayItem,
    todayVangelo,
    todaySanto,
    lastMessa,
    UnKnown_scheme
} URLType;

@interface URLSCheme : NSObject

+(Ora_type)getOraType;

//-> preghiera
+(NSURL*)urlSchemeForOraType:(Ora_type)ora_type;

//-> shortCut preghiera
+(NSURL*)urlSchemeForSuggestedPray;

//-> Vangelo di Oggi
+(NSURL*)urlSchemeForTodayVangel;

//-> Santo di Oggi
+(NSURL*)urlSchemeForTodaySantoAtIndex:(NSInteger)pos;



+(void)openUrlScheme:(NSURL*)url;

@end
