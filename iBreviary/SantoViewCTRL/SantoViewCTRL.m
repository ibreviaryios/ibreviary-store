//
//  SantoViewCTRL.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "SantoViewCTRL.h"
#import "Home.h"


@implementation SantoViewCTRL


@synthesize santoImage, webConteiner, sfondo, close, delegate, santo;
//SHARE
@synthesize htmlPdfKit, dtest, share_btn, rotella_share;


#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<SantoViewCTRLDelegate>*)d andSanto:(SantoObject *)s
{
    //iPhone4
    NSString *nibNameOrNil = @"SantoViewCTRL";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"SantoViewCTRL_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"SantoViewCTRL5";
    if(IS_IPHONE_6) nibNameOrNil = @"SantoViewCTRL6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"SantoViewCTRL6Plus";
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        santo = s;
        if(self.isIpad) [self.view setCenter:d.view.center];
    }
    return self;
}

//==================================
// CONDIVIDI SANTO PDF
//==================================
#pragma mark - CONDIVIDI SANTO PDF -
-(IBAction)condividiSantoPDF:(UIButton*)sender{
    [self creaPDF];
}

-(void)removeRotellaSHARE{
    // NSLog(@"removeRotellaSHARE!");
    [rotella_share stopAnimating];
    [rotella_share setHidden:YES];
    [share_btn setHidden:NO];
}

-(void)attivaRotellaSHARE{
    // NSLog(@"attivaRotellaSHARE!");
    [rotella_share startAnimating];
    [rotella_share setHidden:NO];
    [share_btn setHidden:NO];
}

-(void)creaPDF
{
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        [self attivaRotellaSHARE];
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        [self creaPDFStep1];
    }];//fine step1
    
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    
}

-(void)creaPDFStep1{
    // NSLog(@"creaPDFStep1");
    htmlPdfKit = [[BNHtmlPdfKit alloc] initWithPageSize:BNPageSizeA4];
    htmlPdfKit.delegate = self;
    [htmlPdfKit saveHtmlAsPdf:[self.santo htmlPdfString] toFile:[self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"iBreviary - %@.pdf",self.santo.name]]];
}

//==================================
// PDF DELEGATE
//==================================
#pragma mark - PDF DELEGATE -
- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didSavePdfFile:(NSString *)file{
    // NSLog(@"didSavePdfFile:%@",file);
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        self.dtest = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:file]];
        
        self.dtest.delegate = self;
        CGRect frame = share_btn.frame;
        frame.size.width -= 12.f;
        frame.origin.y += 3.f;
        [self.dtest presentOptionsMenuFromRect:frame inView:share_btn.superview animated:YES];
        
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        [self removeRotellaSHARE];
    }];//fine step2
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    
}
- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didFailWithError:(NSError *)error{
    // NSLog(@"didFailWithError:%@",[error description]);
    
}

//==================================
// DOCUMENT DELEGATE
//==================================
#pragma mark - DOCUMENT DELEGATE -
-(void)documentInteractionController:controller willBeginSendingToApplication:(NSString *)application{
    // NSLog(@"documentInteractionController:%@",application);
}



#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.isIpad)
    {
        UIView *v = [[self.view subviews] objectAtIndex:0];
        [v.layer setCornerRadius:10];
        [v.layer setMasksToBounds:YES];
        CGRect frame = [[[Home sharedInstance] view] frame];
        [self.view setFrame:frame];
    }

    [self configWebView];
    
    [rotella_share setHidden:YES];
    
	// immagine bottone santo
	NSString *path = [[NSBundle mainBundle] bundlePath];
	NSURL *baseURL = [NSURL fileURLWithPath:path];
	[santoImage setImage:santo.image];
    NSString *titleHtml =[NSString stringWithFormat:@"<div class='sezione'>%@", santo.name];
    //aggiungo sottotitolo se c'è
    if(![santo.subtitle isEqualToString:@""]) titleHtml = [titleHtml stringByAppendingString:[NSString stringWithFormat:@"<br />%@</div>",santo.subtitle]];
    else
        titleHtml = [titleHtml stringByAppendingString:@"</div>"];

    NSString *header=[NSString stringWithContentsOfFile:[NSString stringWithFormat:self.isIpad?@"%@/headerSanto_ipad.html":@"%@/headerSanto.html",path] encoding:NSUTF8StringEncoding error:nil];
    
    NSString *footer=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/footer.html",path] encoding:NSUTF8StringEncoding error: nil];
    
    if(self.isIpad){
        NSString *h = [NSString stringWithFormat:header,[NSNumber numberWithInt:(int)(self.webConteiner.frame.size.width-20)]];
        [webConteiner loadHTMLString:[NSString stringWithFormat:@"%@%@%@%@",h,titleHtml,santo.text_info,footer] baseURL:baseURL];
    }
    else
        [webConteiner loadHTMLString:[NSString stringWithFormat:@"%@%@%@%@",header,titleHtml,santo.text_info,footer] baseURL:baseURL];

    
    
    if(self.supportBlur){
        [self blurView:self.blurView
       withEffectStyle:UIBlurEffectStyleLight];
        //rimosso il bounce dalla webView
        for (id subview in webConteiner.subviews)
            if ([[subview class] isSubclassOfClass: [UIScrollView class]])
                ((UIScrollView *)subview).bounces = NO;

    }
    else{
        [self.sfondo setHidden:NO];
    }
    }

-(void)configWebView{
    //localizza
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];

    
    webConteiner.backgroundColor = [UIColor clearColor];
	webConteiner.opaque = NO;
    
    if ([[webConteiner subviews] count] > 0)
    {
        
        for (UIView* shadowView in [[[webConteiner subviews] objectAtIndex:0] subviews])
        {
            shadowView.opaque = NO;
            shadowView.backgroundColor = [UIColor clearColor];
            [shadowView setHidden:YES];
        }
        
        // unhide the last view so it is visible again because it has the content
        [[[[[webConteiner subviews] objectAtIndex:0] subviews] lastObject] setHidden:NO];
    }
    
	
    for (UIView *subview in webConteiner.subviews) {
        subview.clipsToBounds = NO;
    }
    webConteiner.clipsToBounds = NO;
    
    [webConteiner.scrollView setShowsHorizontalScrollIndicator:NO];
    [webConteiner.scrollView setShowsVerticalScrollIndicator:YES];
    [webConteiner.scrollView setDelegate:self];

    
}

#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
- (IBAction)FineSantoViewCTRL:(UIButton *)sender{
    // NSLog(@"santo calling delegate...");
    [delegate removeSantoViewCTRL];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if([scrollView isEqual:webConteiner.scrollView]) {
        // NSLog(@"scrollViewWillBeginDragging->Webview");
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if([scrollView isEqual:webConteiner.scrollView]) {
        // NSLog(@"scrollViewDidScroll->Webview:%f",webConteiner.scrollView.contentOffset.y);
        [self posImage];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if([scrollView isEqual:webConteiner.scrollView]) {
        // NSLog(@"scrollViewDidEndDecelerating->Webview:%f",webConteiner.scrollView.contentOffset.y);
    }
}

BOOL piccola = NO;

-(void)posImage{
    CGFloat posizione = webConteiner.scrollView.contentOffset.y;

    
    if(santoImage.alpha==0 && posizione <= 50.f)
        [UIView animateWithDuration:0.6f animations:^{ [santoImage setAlpha:1]; }];
    else if((santoImage.alpha && posizione > 50.f))
        [UIView animateWithDuration:0.6f animations:^{ [santoImage setAlpha:0]; }];
    
}

#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

//=========================
// ENDCLASS
//=========================
@end


