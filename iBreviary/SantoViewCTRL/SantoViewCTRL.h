//
//  SantoViewCTRL.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "SantoObject.h"
#import "BNHtmlPdfKit.h"


@protocol SantoViewCTRLDelegate <NSObject>
- (void)removeSantoViewCTRL;
@end


@interface SantoViewCTRL : BaseController<UIScrollViewDelegate,BNHtmlPdfKitDelegate,UIDocumentInteractionControllerDelegate>


@property (nonatomic, strong) BaseController<SantoViewCTRLDelegate> *delegate;

@property (nonatomic, strong) IBOutlet UIWebView *webConteiner;
@property (nonatomic, strong) IBOutlet UIImageView *sfondo;
@property (nonatomic, strong) IBOutlet UIView *blurView;
@property (nonatomic, strong) IBOutlet UIImageView *santoImage;
@property (nonatomic, strong) IBOutlet UIButton *close;
@property (nonatomic, strong) SantoObject *santo;
//Share
@property (nonatomic, strong) BNHtmlPdfKit *htmlPdfKit;
@property (strong, nonatomic) UIDocumentInteractionController *dtest;
@property (strong, nonatomic) IBOutlet UIButton *share_btn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *rotella_share;


- (id)initWithDelegate:(BaseController<SantoViewCTRLDelegate>*)d andSanto:(SantoObject*)s;


@end

