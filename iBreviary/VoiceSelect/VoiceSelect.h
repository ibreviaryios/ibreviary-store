//
//  VoiceSelect.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"


@protocol VoiceSelectDelegate <NSObject>
-(void)removeVoiceSelect:(NSString*)voce_scelta;
//@optional

@end


@interface VoiceSelect : BaseController


@property (nonatomic, strong) BaseController<VoiceSelectDelegate> *delegate;

@property (nonatomic, strong) NSMutableArray *data_voice;
@property (strong, nonatomic) NSString *sigla;

@property (nonatomic, strong) IBOutlet UIImageView *sfondo;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (nonatomic, strong) IBOutlet UIButton *close;
@property (nonatomic, strong) IBOutlet UILabel *titolo;
@property (strong, nonatomic) IBOutlet UILabel *line;


- (id)initWithDelegate:(BaseController<VoiceSelectDelegate>*)d andSigla:(NSString*)s;


@end

