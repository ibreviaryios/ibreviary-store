//
//  VoiceSelect.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import "VoiceSelect.h"

@implementation VoiceSelect


@synthesize data_voice, sigla, delegate;

@synthesize sfondo, scroller, close, titolo, line;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<VoiceSelectDelegate>*)d andSigla:(NSString *)s
{
    NSString *nibNameOrNil;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) nibNameOrNil = @"VoiceSelect_ipad";
    else
        nibNameOrNil = ([[UIScreen mainScreen] bounds].size.height == 568)?@"VoiceSelect5":@"VoiceSelect";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        sigla = s;
        if(self.isIpad) [self.view setFrame:d.view.frame];
    }
    return self;
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    [self buildSelectors];
    
    [self gui];
}

-(void)buildSelectors{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000

    data_voice = [NSMutableArray new];
    NSArray *a = [AVSpeechSynthesisVoice speechVoices];
    for (AVSpeechSynthesisVoice *v in a)
    {
        if ([v.language rangeOfString:sigla].location != NSNotFound) {
            // NSLog(@"found voice ->%@",v.language);
            [data_voice addObject:v.language];
        }
    }
    
    [data_voice sortUsingSelector:@selector(compare:)];
    
    int i = 0;
    CGFloat width = 192;
    CGFloat row_height = 44;
    for(NSString *row in data_voice)
    {
        UIView *riga = [[UIView alloc] initWithFrame:CGRectMake(0, (10+(i*row_height)), width, row_height)];
        [riga setBackgroundColor:[UIColor clearColor]];
        
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 20, row_height)];
        //[title setText:@"-"];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setFont:[UIFont fontWithName:FONT_HELVETICANEUE_THIN size:20]];
        [title setTextAlignment:NSTextAlignmentLeft];
        [title setTextColor:[UIColor whiteColor]];
        [riga addSubview:title];

        CGRect frame =riga.frame;
        frame.origin.y = 0;
        UIButton *b = [[UIButton alloc] initWithFrame:frame];
        [b setBackgroundColor:[UIColor clearColor]];
        [b.titleLabel setFont:[UIFont fontWithName:FONT_HELVETICANEUE_THIN size:20]];
        [b setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8] forState:UIControlStateNormal];
        [b setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [b setTitle:row forState:UIControlStateNormal];
        [b setAlpha:1];
        [b setTag:i];
        [b addTarget:self action:@selector(Scelta:) forControlEvents:UIControlEventTouchUpInside];
        [riga addSubview:b];
        [scroller addSubview:riga];
        i++;
    }
    
    [scroller setContentSize:CGSizeMake(width, (data_voice.count*row_height)+10)];
#endif
}


-(void)gui{
    
    //[close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
   
    
    NSString *textTitile = NSLocalizedString(@"VOICE_SELEZIONA_MEX", nil);
    
    [titolo setText:[textTitile uppercaseString]];
    
    
    
    //[self.view setBackgroundColor:[UIColorFromRGB(0X3d2914) colorWithAlphaComponent:0.5]];

}

#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
- (IBAction)Scelta:(UIButton *)sender{
    NSString *v = @"";
    if(sender.tag<data_voice.count) v = [data_voice objectAtIndex:sender.tag];
    [delegate removeVoiceSelect:v];
}

- (IBAction)Annulla:(UIButton *)sender{
    NSString *v = @"";
    [delegate removeVoiceSelect:v];
}


#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return self.isIpad?UIInterfaceOrientationMaskAll:UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return self.isIpad;
}

//=========================
// ENDCLASS
//=========================
@end


