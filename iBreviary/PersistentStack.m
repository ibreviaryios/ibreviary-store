#import "PersistentStack.h"
#import "Home.h" //aggiunto per debug

@interface PersistentStack ()


@property (nonatomic,strong) NSURL* modelURL;
@property (nonatomic,strong) NSURL* storeURL;

@property (strong,nonatomic) NSTimer *iCloudUpdateTimer;
@property (readonly,nonatomic) NSUserDefaults *defaults;


@end

@implementation PersistentStack

-(NSUserDefaults*)defaults{
    NSUserDefaults *def = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    return def;
}

- (id)initWithStoreURL:(NSURL*)storeURL modelURL:(NSURL*)modelURL
{
    self = [super init];
    if (self) {
        self.storeURL = storeURL;
        self.modelURL = modelURL;
        [self setupManagedObjectContext];
    }
    return self;
}

+(BOOL)isCloudAvaible{
    NSURL *ubiquityURL = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    BOOL risp = (ubiquityURL == nil);
    if(risp){
        NSLog(@"ubiquityurl %@",ubiquityURL);
    }
    /*
     id token = [[NSFileManager defaultManager] ubiquityIdentityToken];
     if (token == nil)
     {
     // iCloud is not available for this app
     }
     else
     {
     // iCloud is available
     }
     */
    return risp;
}

+(NSString*)checkForiCloudString
{
    if ([PersistentStack isCloudAvaible])
    {
        return NSLocalizedString(@"iCloud is not avalaible", nil);
    }
    //Non Avaible mex
    return NSLocalizedString(@"iCloud is avalaible", nil);
}

+(void)display_iCloudAlertIfNeeded{
    if ([PersistentStack isCloudAvaible]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"iCloud Not Configured"
                                                            message:@"Open iCloud Settings, and make sure you are logged in."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil, nil];
        [alertView show];
    }
}


- (void)setupManagedObjectContext
{
    _managedObjectContext = nil;
    _privateQueueContext = nil;
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    _privateQueueContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];

    
    _managedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
    _managedObjectContext.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
 
    
    _privateQueueContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
    _privateQueueContext.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
    


    
    
    __weak NSPersistentStoreCoordinator *psc = self.managedObjectContext.persistentStoreCoordinator;
    
    NSError* error;
    // the only difference in this call that makes the store an iCloud enabled store
    // is the NSPersistentStoreUbiquitousContentNameKey in options. I use "iCloudStore"
    // but you can use what you like. For a non-iCloud enabled store, I pass "nil" for options.

    // Note that the store URL is the same regardless of whether you're using iCloud or not.
    // If you create a non-iCloud enabled store, it will be created in the App's Documents directory.
    // An iCloud enabled store will be created below a directory called CoreDataUbiquitySupport
    // in your App's Documents directory
    
    //impostazioni per update
    NSMutableDictionary *option = [NSMutableDictionary dictionaryWithDictionary:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}];

    if([self.defaults boolForKey:AGGIORNAMENTO_2015]){
        [NSThread sleepForTimeInterval:0.2];
        NSLog(@"******ATTIVAZIONE CLOUD !!!!!!!!!!! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<MAIN THREAD:%@",[NSThread isMainThread]?@"YES":@"NO");
//#warning RIMOSSO iCLOUD PER TEST CUSTOM MERGE MODEL 1->1
// http://www.pumpmybicep.com/2014/09/17/writing-a-core-data-custom-migration/
        
        [option addEntriesFromDictionary:@{ NSPersistentStoreUbiquitousContentNameKey : @"iCloudStore" }];
    }
    
    
    [self.managedObjectContext.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                                       configuration:nil
                                                                                 URL:self.storeURL
                                                                             options:option
                                                                               error:&error];
    
    if(![self.defaults boolForKey:AGGIORNAMENTO_2015]){
        NSArray *messe = [Messa getMesseIn:self.managedObjectContext];
        NSLog(@"Messe:%@",messe);
        if(messe.count>0){
            [AppDelegate saveContext];
            [NSThread sleepForTimeInterval:0.5];
            NSArray *messepostSave = [Messa getMesseIn:self.managedObjectContext];
            NSLog(@"Messe:%@",messepostSave);
        }
    }
    else{
        [AppDelegate saveContext];
        [NSThread sleepForTimeInterval:0.5];
//        NSArray *messeDopoCloud = [Messa getMesseIn:self.managedObjectContext];
         NSLog(@"DopoCloud");
    }

    
    [self.privateQueueContext.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                                       configuration:nil
                                                                                 URL:self.storeURL
                                                                             options:option
                                                                               error:&error];

    
    
    // iCloud notification subscriptions
    NSNotificationCenter *dc = [NSNotificationCenter defaultCenter];
    
    //resetNotifiche
    [dc removeObserver:self];
    
    [dc addObserver:self
           selector:@selector(storesWillChange:)
               name:NSPersistentStoreCoordinatorStoresWillChangeNotification
             object:psc];
    
    [dc addObserver:self
           selector:@selector(storesDidChange:)
               name:NSPersistentStoreCoordinatorStoresDidChangeNotification
             object:psc];
    
    [dc addObserver:self
           selector:@selector(persistentStoreDidImportUbiquitousContentChanges:)
               name:NSPersistentStoreDidImportUbiquitousContentChangesNotification
             object:psc];

    
    //COREDATA PRIVATE<->PUBLIC mergeChanges
    [dc addObserver:self
           selector:@selector(contextDidSaveMainQueueContext:)
               name:NSManagedObjectContextDidSaveNotification
             object:_managedObjectContext];
    
    [dc addObserver:self
           selector:@selector(contextDidSavePrivateQueueContext:)
               name:NSManagedObjectContextDidSaveNotification
             object:_privateQueueContext];
    
    //Aggiornamento DB
    [dc addObserver:self selector:@selector(setupManagedObjectContext) name:AGGIORNAMENTO2015_TERMINATO object:nil];


    if (error) {
        NSLog(@"error: %@", error);
    }
}

- (NSManagedObjectModel*)managedObjectModel
{
    return [[NSManagedObjectModel alloc] initWithContentsOfURL:self.modelURL];
}

// Subscribe to NSPersistentStoreDidImportUbiquitousContentChangesNotification
- (void)persistentStoreDidImportUbiquitousContentChanges:(NSNotification*)note
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"%@", note);
    
    
    if(!note){
        [[Home sharedInstance] alert:@"iCLoud Debug"
                              andMex:[NSString stringWithFormat:@"%@",note]
                                icon:nil
                            okAction:nil
                          withCancel:YES];
        return;
    }
    
    
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    
    [managedObjectContext performBlockAndWait:^{

        [managedObjectContext mergeChangesFromContextDidSaveNotification:note];
        
        // you may want to post a notification here so that which ever part of your app
        // needs to can react appropriately to what was merged. 
        // An exmaple of how to iterate over what was merged follows, although I wouldn't
        // recommend doing it here. Better handle it in a delegate or use notifications.
        // Note that the notification contains NSManagedObjectIDs
        // and not NSManagedObjects.
        NSDictionary *changes = note.userInfo;
        NSMutableSet *allChanges = [NSMutableSet new];
        [allChanges unionSet:changes[NSInsertedObjectsKey]];
        [allChanges unionSet:changes[NSUpdatedObjectsKey]];
        [allChanges unionSet:changes[NSDeletedObjectsKey]];
        
        for (NSManagedObjectID *objID in allChanges) {
            // do whatever you need to with the NSManagedObjectID
            // you can retrieve the object from with [moc objectWithID:objID]
            NSLog(@"inporting:%@",objID);
        }

    }];
    
    [self createCloudTimer];
    
    if(self.delegate){
        [self.delegate storeDidImport];
    }
    
}

#pragma mark -
#pragma mark Utility method
//===============================
// runOnMain
//===============================
void runOnMain(void (^block)(void))
{
    if ([NSThread isMainThread])
    {
        block();
    }
    else
    {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
};

// Subscribe to NSPersistentStoreCoordinatorStoresWillChangeNotification
// most likely to be called if the user enables / disables iCloud 
// (either globally, or just for your app) or if the user changes
// iCloud accounts.
- (void)storesWillChange:(NSNotification *)note {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    NSLog(@"note:%@",note);
    [managedObjectContext performBlockAndWait:^{
        NSError *error = nil;
        if ([managedObjectContext hasChanges]) {
            [managedObjectContext save:&error];
        }
        
        [managedObjectContext reset];
    }];
    
    if(self.delegate){
        [self.delegate storeWillChange];
    }
    
    [self createCloudTimer];
    // now reset your UI to be prepared for a totally different
    // set of data (eg, popToRootViewControllerAnimated:)
    // but don't load any new data yet.
}

// Subscribe to NSPersistentStoreCoordinatorStoresDidChangeNotification
- (void)storesDidChange:(NSNotification *)note {
    // here is when you can refresh your UI and
    // load new data from the new store
    NSLog(@"storesDidChange");


    [self createCloudTimer];
    
    if(self.delegate){
        [self.delegate storeDidChange];
    }
}

-(void)createCloudTimer{
    if (self.iCloudUpdateTimer != nil){
        [self.iCloudUpdateTimer invalidate];
    }
    self.iCloudUpdateTimer = nil;
    self.iCloudUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                                target:self
                                                            selector:@selector(notifyOfCoreDataUpdates)
                                                            userInfo:nil
                                                                repeats:NO];
}

-(void)notifyOfCoreDataUpdates{
    //aggiorno notifiche lette
    runOnMain(^{
        NSLog(@"**AGGIORNAMENTO_ICLOUD_NOTIFICA**");
        [[NSNotificationCenter defaultCenter] postNotificationName:AGGIORNAMENTO_ICLOUD_NOTIFICA
                                                            object:nil];
    });
}

#pragma mark -
#pragma mark CORE-DATA SAVE
//=========================
// CORE-DATA SAVE CONTEXT
//=========================
- (void)saveContext{
    runOnMain(^{
        [self MainsaveContext];
    });
}

- (void)MainsaveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        // [managedObjectContext processPendingChanges];
        if (![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash // NSLog and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"PersistenStack: Errore saving:%@",[NSString stringWithFormat:@"%@, %@\n%@", error, [error localizedDescription],[error userInfo]]);
            
            [[[UIAlertView alloc] initWithTitle:@"Errore saving" message:[NSString stringWithFormat:@"%@, %@\n%@", error, [error localizedDescription],[error userInfo]] delegate:nil cancelButtonTitle:@"abort" otherButtonTitles:nil] show];
        }
    }
}

- (void)savePrivateContext{
    runOnMain(^{
        [self MainsavePrivateContext];
    });
}



- (void)MainsavePrivateContext
{
    NSError *error = nil;
    @synchronized(self) {
        NSManagedObjectContext *managedObjectContext = self.privateQueueContext;
        if (managedObjectContext != nil) {
            // [managedObjectContext processPendingChanges];
            if (![managedObjectContext save:&error]) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash // NSLog and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog(@"AppdelegatePrivate: Errore saving:%@",[NSString stringWithFormat:@"%@, %@\n%@", error, [error localizedDescription],[error userInfo]]);
                //[[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",error] message:[NSString stringWithFormat:@"%@, %@\n%@", error, [error localizedDescription],[error userInfo]] delegate:nil cancelButtonTitle:@"abort" otherButtonTitles:nil] show];
                abort();
            }
        }
    }
}

#pragma mark CORE-DATA NOTIFACTION
//================================
// CORE-DATA NOTIFACTION
//================================
- (void)contextDidSavePrivateQueueContext:(NSNotification *)notification
{
    //NSLog(@"PersistentStack: contextDidSavePrivateQueueContext");
    @synchronized(self) {
    if([self.privateQueueContext hasChanges]){
        //NSLog(@"privateQueueContext hasChanges");
        [self.managedObjectContext mergeChangesFromContextDidSaveNotification:notification];
        }
    }
}


- (void)contextDidSaveMainQueueContext:(NSNotification *)notification
{
    //NSLog(@"PersistentStack: contextDidSaveMainQueueContext");
    if([self.managedObjectContext hasChanges]){
        //NSLog(@"managedObjectContext hasChanges");
        [self.privateQueueContext performBlock:^{
            [self.privateQueueContext mergeChangesFromContextDidSaveNotification:notification];
        }];
    }
}



@end