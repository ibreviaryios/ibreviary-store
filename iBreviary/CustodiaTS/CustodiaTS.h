//
//  CustodiaTS.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"

@protocol CustodiaTSDelegate <NSObject>
- (void)removeCustodia;
- (void)showCustodiaBlog;
- (void)showCustodiaWebSite;
@end


@interface CustodiaTS : BaseController

    
@property (nonatomic, strong) BaseController<CustodiaTSDelegate> *delegate;
@property (nonatomic, strong) IBOutlet UIButton *web;
@property (nonatomic, strong) IBOutlet UIButton *blog;
@property (nonatomic, strong) IBOutlet UIButton *close;
@property (nonatomic, strong) IBOutlet UITextView *textV;
@property (nonatomic, strong) IBOutlet UILabel *labelV;

- (IBAction)FineCustodia:(UIButton*)sender;
- (IBAction)azioneBlog:(UIButton*)sender;
- (IBAction)azioneWeb:(UIButton*)sender;

@end
