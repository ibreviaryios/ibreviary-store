//
//  CustodiaTS.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "CustodiaTS.h"
#import "WebBrowser.h"
#import "Define.h"

@implementation CustodiaTS

@synthesize web, blog, textV,labelV,close ,delegate;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)init
{

    //iPhone4
    NSString *nibNameOrNil = @"CustodiaTS";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"CustodiaTS_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"CustodiaTS5";
    if(IS_IPHONE_6) nibNameOrNil = @"CustodiaTS6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"CustodiaTS6Plus";

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark -
#pragma mark AZIONI
//=========================
// AZIONI
//=========================
- (IBAction)azioneBlog:(UIButton*)sender
{
    [delegate showCustodiaBlog];
}

- (IBAction)azioneWeb:(UIButton*)sender
{
    [delegate showCustodiaWebSite];
}

#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
-(IBAction)FineCustodia:(UIButton*)sender{
    [delegate removeCustodia];
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.isIpad)
    {
        [self.view.layer setCornerRadius:10];
        [self.view.layer setMasksToBounds:YES];
    }
	// SET DEL TESTO
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
    textV.text = NSLocalizedString(@"CustodiaMEX", nil);
	labelV.text = NSLocalizedString(@"CustodiaMEX", nil);
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!self.isIos7orUpper){
        CGRect fr = self.view.frame;
        fr.origin.y =-20;
        [self.view setFrame:fr];
        fr = [[self.view.subviews objectAtIndex:0] frame];
        fr.origin.y = -20;[[self.view.subviews objectAtIndex:0] setFrame:fr];
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}



@end
