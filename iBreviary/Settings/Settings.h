//
//  Settings.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "Tutorial.h"
#import "FXBlurView.h"

@interface Settings : BaseController<TutorialDelegate>


@property (strong, nonatomic) IBOutlet UILabel *title_settings;
@property (strong, nonatomic) IBOutlet UILabel *linea_title;

@property (strong, nonatomic) IBOutlet UILabel *title_style;
@property (strong, nonatomic) IBOutlet UILabel *linea_style;
@property (strong, nonatomic) IBOutlet UILabel *title_contrasto;
@property (strong, nonatomic) IBOutlet UILabel *linea_contrasto;
@property (strong, nonatomic) IBOutlet UILabel *title_size;
@property (strong, nonatomic) IBOutlet UILabel *linea_size;

//ZOOM
@property (nonatomic,strong) UISlider *zoom;
@property (strong, nonatomic) IBOutlet UIView *zoomView;
@property (strong, nonatomic) IBOutlet UILabel *zoomLabel;

//LUMINOSITA'
@property (nonatomic,strong) UISlider *mySlider;
@property (strong, nonatomic) IBOutlet UIView *lumiView;

//AUTOSCROLL
@property (strong, nonatomic) IBOutlet UIView *autoscroll_view;
@property (strong, nonatomic) IBOutlet UIButton *autoscroll_btn;
@property (strong, nonatomic) IBOutlet UILabel *autoscroll_label;
@property (strong, nonatomic) IBOutlet UILabel *autoscroll_valueLabel;
@property (weak, nonatomic) IBOutlet UIView *autoscroll_area_btn;
@property (weak, nonatomic) IBOutlet UIImageView *autoscroll_icon;

@property (weak, nonatomic) IBOutlet UIView *settingView_ipad;

//LETTURA
@property (strong, nonatomic) IBOutlet UIView *audio_view;
@property (strong, nonatomic) IBOutlet UIButton *audio_btn;
@property (strong, nonatomic) IBOutlet UILabel *audio_label;
@property (weak, nonatomic) IBOutlet UIView *audio_area_btn;
@property (weak, nonatomic) IBOutlet UIButton *slow_btn;
@property (weak, nonatomic) IBOutlet UIButton *fast_btn;
@property (weak, nonatomic) IBOutlet UILabel *audio_speach_label;
@property (weak, nonatomic) IBOutlet UIImageView *autoreadings_icon;


//CSS
@property (strong, nonatomic) IBOutlet UILabel *classic;
@property (strong, nonatomic) IBOutlet UILabel *oldClassic;
@property (strong, nonatomic) IBOutlet UILabel *alternative;
@property (strong, nonatomic) IBOutlet UILabel *night;
@property (strong, nonatomic) IBOutlet UIButton *b0;
@property (strong, nonatomic) IBOutlet UIButton *b1;
@property (strong, nonatomic) IBOutlet UIButton *b2;
@property (strong, nonatomic) IBOutlet UIButton *b3;

@property (strong, nonatomic) IBOutlet UIView *ZOOM_BLOCK;
@property (strong, nonatomic) IBOutlet UIView *black_mask;

//TUTORIAL
@property (strong, nonatomic) FXBlurView *blurView;
@property (strong, nonatomic) Tutorial *tutorialCTRL;


@end
