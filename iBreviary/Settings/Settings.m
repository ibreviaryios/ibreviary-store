//
//  Settings.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "Settings.h"
#import "MenuViewController.h"
#import "Home.h"
#import "SystemData.h"


@implementation Settings

//TITLE
@synthesize title_settings,linea_title;
//SUBTITLE
@synthesize title_style, linea_style, title_contrasto, linea_contrasto, title_size, linea_size;

//=====
//AREA
//=====

//ZOOM AREA
@synthesize zoom, zoomView, zoomLabel,ZOOM_BLOCK;
//LUMINOSITA' AREA
@synthesize mySlider, lumiView;
//CSS AREA
@synthesize classic, alternative, night,oldClassic;
@synthesize b0, b1, b2,b3;
//AUTOSCROLL AREA
@synthesize autoscroll_view, autoscroll_btn, autoscroll_label;
@synthesize autoscroll_icon,autoscroll_valueLabel,autoscroll_area_btn;
//LETTURA AREA
@synthesize audio_view, audio_btn, audio_label,autoreadings_icon;
@synthesize audio_area_btn, slow_btn, fast_btn, audio_speach_label;
@synthesize black_mask;
//TUTORIAL
@synthesize tutorialCTRL,blurView;
//IPAD VIEW
@synthesize settingView_ipad;

static NSArray *scrollValue;

static NSArray *audioRateArray;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)init
{
    
    //iPhone4
    NSString *nibNameOrNil = @"Settings";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"Settings_ipad";
    if(IS_IPAD_PRO) nibNameOrNil = @"Settings_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"Settings5";
    if(IS_IPHONE_6) nibNameOrNil = @"Settings6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"Settings6Plus";

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
    }
    // NSLog(@"Settings:init %@",self);
    return self;
}

//=========================
// StatusBar
//=========================
#pragma mark - StatusBar
- (BOOL)prefersStatusBarHidden
{
    // NSLog(@"Settings:prefersStatusBarHidden:YES");
    return YES;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

//cambio colore statusbar in bianco
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    audioRateArray = [SystemData audioRate];
    scrollValue = [SystemData scrollValue];

    // NSLog(@"AUTOSCROLL:%@",scrollValue);
    
    [self costruisciSliderLUMI];
    [self costruisciSliderZOOM];
    [self aggiornaBottoniCSS];
    
    [self gui];
    // NSLog(@"Settings:viewDidLoad:%@",self.view);
    
    if(!self.isIos7orUpper){
                for(UIView *v in audio_view.subviews) [v removeFromSuperview];
                }
}

-(void)gui{
    //[title setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:15]];
    
    [classic setFont:[UIFont fontWithName:FONT_PALATINO_ROMAN size:23]];
    [oldClassic setFont:[UIFont fontWithName:FONT_CALSON_ANTIQUE size:25]];
    [alternative setFont:[UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size:20]];
    [night setFont:[UIFont fontWithName:FONT_ROMAN_ANTIQUE size:25]];
    
    [classic setTextColor:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:180/255.f]];
    [oldClassic setTextColor:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:180/255.f]];
    [alternative setTextColor:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:180/255.f]];
    [night setTextColor:[UIColorFromRGB(0Xdacdba) colorWithAlphaComponent:0.95]];
    
    [title_style setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:11]];
    [title_contrasto setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:11]];
    [title_size setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:11]];
    [linea_style setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:16]];
    [linea_contrasto setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:14]];
    [linea_size setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:14]];
    
    
    [self updateScrollValueIndex];
    [self localizzaVista];

}

//==============
//LOCALIZZAZIONE
//==============
-(void)localizzaVista{

    [title_settings setText:[NSLocalizedString(@"Settings_title", nil) uppercaseString]];
    [title_contrasto setText:[NSLocalizedString(@"Settings_title_contrasto", nil) uppercaseString]];
    
     NSString *l = @"";
     for(int i=0; i< (title_contrasto.text.length)+2; i++)
     {
     l = [l stringByAppendingString:@"_"];
     }
     [linea_contrasto setText:l];
    
    [title_style setText:[NSLocalizedString(@"Settings_title_style", nil) uppercaseString]];
    l = @"";
    for(int i=0; i< (title_style.text.length); i++)
    {
        l = [l stringByAppendingString:@"_"];
    }
    [linea_style setText:l];
    
    [title_size setText:[NSLocalizedString(@"Settings_title_zoom", nil) uppercaseString]];
    
    l = @"";
    for(int i=0; i< (title_size.text.length)+1; i++)
    {
        l = [l stringByAppendingString:@"_"];
    }
    [linea_size setText:l];
    
    [audio_label setText:[NSLocalizedString(@"Settings_title_letturaTesti", nil) uppercaseString]];
    
    BOOL attivoScroll = [self.defaults boolForKey:AUTOSCROLL_BOOL];
    [autoscroll_label setText:[attivoScroll?NSLocalizedString(@"testiPropri_disabilita", nil):NSLocalizedString(@"Settings_title_autoScroll", nil) uppercaseString]];
    if(![self isIos7orUpper])
    {
        [self.defaults setBool:NO forKey:AUDIOLETTURA_BOOL];
        [self.defaults synchronize];
        [audio_label setText:@""];
        [audio_btn setImage:[UIImage new] forState:UIControlStateNormal];
        [audio_btn setTitle:@"" forState:UIControlStateNormal];
        [audio_btn setUserInteractionEnabled:NO];
        [audio_btn setHidden:YES];
    }
    
}

//===========================
// TUTORIAL DELEGATE
//===========================
#pragma mark - TUTORIAL DELEGATE
- (IBAction)showTutorial:(UIButton*)sender{
    // NSLog(@"showTutorial");
    tutorialCTRL = [[Tutorial alloc] initWithDelegate:self];
    blurView = [[FXBlurView alloc] initWithFrame:self.view.frame];
    [self.blurView setDynamic:NO];
    [self.blurView setTintColor:[UIColor brownColor]];
    [self.blurView addSubview:tutorialCTRL.view];
    [self.blurView setFrame:CGRectMake(232, 24, 10, 15)];
    [self.blurView setAlpha:0];
    
    [self.view addSubview:self.blurView];
    
    [UIView beginAnimations:@"AppareTutorialCTRL" context:NULL];
    [UIView setAnimationDuration:0.1];
    [self.blurView setAlpha:1];
    [UIView commitAnimations];
    
    CGRect to = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView animateWithDuration:0.7 animations:^{[self.blurView setFrame:to];}
                     completion:^(BOOL finished)    {
                         // NSLog(@"Fine Animation");
                     }];
    
}


-(void)removeTutorial{
    // NSLog(@"removeTutorial");
    
    CGRect to = CGRectMake(232, 24, 10, 10);
    
    [UIView animateWithDuration:0.4 animations:^{[self.blurView setFrame:to];}
                     completion:^(BOOL finished)    {
                         [UIView beginAnimations:@"SconpareTutorialCTRL" context:NULL];
                         [UIView setAnimationDuration:0.2];
                         [self.blurView setAlpha:0];
                         [UIView commitAnimations];
                     }];
    
    
}


#pragma mark - Appear
-(void)viewWillAppear:(BOOL)animated{
    // NSLog(@"Settings:viewWillAppear");
    [super viewWillAppear:animated];

}

- (void)viewDidAppear:(BOOL)animated{
    // NSLog(@"Settings:viewDidAppear");
    [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];

    Home* h = [Home sharedInstance];
    
    if(![[h.webContent stringByEvaluatingJavaScriptFromString:@"document.getElementById('ciccio').innerHTML;"] isEqualToString:@""])
    {
        [appDelegate.defaults setObject:[h.webContent stringByEvaluatingJavaScriptFromString:@"document.getElementById('ciccio').innerHTML;"] forKey:ZOOM];
        [appDelegate.defaults synchronize];
    }
    
    zoomLabel.text = [NSString stringWithFormat:@"%@",[appDelegate.defaults objectForKey:ZOOM]];
    [self.zoom setValue:[[appDelegate.defaults objectForKey:ZOOM] integerValue]];
    
    [self.mySlider setValue:[appDelegate.defaults floatForKey:LUMINOSITA] ];
    
    [self aggiornaBottoniCSS];
    
    //CHECKSCROLL
    [self checkScrollBool];
    
    //CHECKAUDIO
    [self checkAUDIOBool];

    [super viewDidAppear:animated];
    [self performSelector:@selector(test) withObject:nil afterDelay:10];
}

-(void)test{
    NSLog(@"self:%@",self.view);
}

-(void)checkAUDIOBool{
    BOOL attivoAudio = [self.defaults boolForKey:AUDIOLETTURA_BOOL];
    [audio_btn setSelected:attivoAudio];
    [audio_view setAlpha:attivoAudio?1:1];
    [audio_label setTextColor:attivoAudio?UIColorFromRGB(0X604e3d):UIColorFromRGB(0X7e6657)];
    [audio_label setTextColor:UIColorFromRGB(0X604e3d)];
    [audio_area_btn setHidden:!attivoAudio];
    [audio_label setText:[attivoAudio?NSLocalizedString(@"testiPropri_disabilita", nil):NSLocalizedString(@"Settings_title_letturaTesti", nil) uppercaseString]];
    //imposta valore di default
    NSInteger index = [[self.defaults objectForKey:AUDIORATE_INDEX] integerValue];
    [audio_speach_label setText:[NSString stringWithFormat:@"%i",(int)index+1]];
    [autoreadings_icon setImage:[UIImage imageNamed:attivoAudio?@"autoReadYes.png":@"autoReadNO.png"]];
}

-(void)checkScrollBool{
    BOOL attivoScroll = [self.defaults boolForKey:AUTOSCROLL_BOOL];
    [autoscroll_btn setSelected:attivoScroll];
    [autoscroll_area_btn setHidden:!attivoScroll];
    [autoscroll_label setTextColor:UIColorFromRGB(0X604e3d)];
    [autoscroll_icon setImage:[UIImage imageNamed:attivoScroll?@"autoScrollYES.png":@"autoScrollNO.png"]];
    [autoscroll_label setText:[attivoScroll?NSLocalizedString(@"testiPropri_disabilita", nil):NSLocalizedString(@"Settings_title_autoScroll", nil) uppercaseString]];

}

#pragma mark Disappear

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    // NSLog(@"Settings: viewDidDisappear");
}

#pragma mark -
#pragma mark SCROLL
//=========================
// SCROLL
//=========================
-(IBAction)AttivaAutoSCROLL_CTRL:(UIButton*)sender{
    BOOL attivo = ![self.defaults boolForKey:AUTOSCROLL_BOOL];
    [self.defaults setBool:attivo forKey:AUTOSCROLL_BOOL];
    [self.defaults synchronize];
    [self checkScrollBool];
    if(attivo){
        NSInteger index = [[self.defaults objectForKey:AUTOSCROLL_INDEX] integerValue];
        NSInteger value = [[scrollValue objectAtIndex:index] integerValue];
        [self updateScrollValueIndex];
        [Home setScrollSpeed:value];
    }
    [Home scrollerCTRL];
}
-(IBAction)decrementScrollVelocity:(UIButton*)sender{
    NSInteger index = [[self.defaults objectForKey:AUTOSCROLL_INDEX] integerValue];
    index -=1;
    if (index<=0) {
        index = 0;
    }
    [self.defaults setObject:[NSNumber numberWithInteger:index] forKey:AUTOSCROLL_INDEX];
    NSInteger value = [[scrollValue objectAtIndex:index] integerValue];
    // NSLog(@"----value[%i]->%i",(int)index,(int)value);
    [self.defaults setObject:[NSNumber numberWithInteger:value] forKey:AUTOSCROLL_VALUE];
    [self.defaults synchronize];
    [self updateScrollValueIndex];
    [Home setScrollSpeed:value];
}


-(IBAction)incrementScrollVelocity:(UIButton*)sender{
    
    NSInteger index = [[self.defaults objectForKey:AUTOSCROLL_INDEX] integerValue];
    index +=1;
    if (index>=scrollValue.count) {
        index = scrollValue.count-1;
    }
    [self.defaults setObject:[NSNumber numberWithInteger:index] forKey:AUTOSCROLL_INDEX];
    NSInteger value = [[scrollValue objectAtIndex:index] integerValue];
    // NSLog(@"++++value[%i]->%i",(int)index,(int)value);
    [self.defaults setObject:[NSNumber numberWithInteger:value] forKey:AUTOSCROLL_VALUE];
    [self.defaults synchronize];

    [self updateScrollValueIndex];
    [Home setScrollSpeed:value];
}

-(void)updateScrollValueIndex{
    int index = [[self.defaults objectForKey:AUTOSCROLL_INDEX] intValue];
    //index = (int)scrollValue.count - index;
    NSString *valueString = [NSString stringWithFormat:@"%i",index+1];
    [autoscroll_valueLabel setText:valueString];
    // NSLog(@"index default:%@ autoscroll:%@",[self.defaults objectForKey:AUTOSCROLL_INDEX], autoscroll_valueLabel.text);
}


#pragma mark -
#pragma mark AUDIO
//=========================
// AUDIO
//=========================
-(IBAction)AttivaAudio:(UIButton*)sender{
    BOOL attivo = ![self.defaults boolForKey:AUDIOLETTURA_BOOL];
    [self.defaults setBool:attivo forKey:AUDIOLETTURA_BOOL];
    [self.defaults synchronize];
    [self checkAUDIOBool];
    [Home audioCTRL];
}

-(IBAction)speachTextSlower:(UIButton*)sender{
    NSInteger index = [[self.defaults objectForKey:AUDIORATE_INDEX] integerValue];
    index -=1;
    if (index<=0) {
        index = 0;
    }
    [self.defaults setObject:[NSNumber numberWithInteger:index] forKey:AUDIORATE_INDEX];
    [self.defaults synchronize];
    [audio_speach_label setText:[NSString stringWithFormat:@"%i",(int)index+1]];
}


-(IBAction)speachTextFaster:(UIButton*)sender{
    
    NSInteger index = [[self.defaults objectForKey:AUDIORATE_INDEX] integerValue];
    index +=1;
    if (index>=audioRateArray.count) {
        index = audioRateArray.count-1;
    }
    [self.defaults setObject:[NSNumber numberWithInteger:index] forKey:AUDIORATE_INDEX];
    [self.defaults synchronize];
    [audio_speach_label setText:[NSString stringWithFormat:@"%i",(int)index+1]];
}

#pragma mark -
#pragma mark ZOOM
//=========================
// ZOOM
//=========================
-(void)costruisciSliderZOOM{
    
    BOOL bigVersion = IS_IPAD || IS_IPHONE_6 || IS_IPHONE_6_PLUS;
    
    //SLIDER ZOOM
    self.zoom =  [[UISlider alloc] initWithFrame:CGRectMake(0,0,bigVersion?140:100,23)];
    self.zoom.transform = CGAffineTransformMakeRotation(M_PI * 1.5);
    [self.zoom setFrame:CGRectMake(0, 0, 23, bigVersion?140:100)];
    self.zoom.maximumValue=bigVersion?320.f:220.f;
    self.zoom.minimumValue=70.f;
    [self.zoom setValue:[[appDelegate.defaults objectForKey:ZOOM] integerValue]];
    //[self.zoom setThumbTintColor:[UIColorFromRGB(0X624334) colorWithAlphaComponent:0.8]];

    UIImage *sliderTrackImage = [[UIImage imageNamed: @"lumi_img@2x.png"] stretchableImageWithLeftCapWidth: 0 topCapHeight: -28];
    
    [self.zoom setThumbImage:sliderTrackImage forState: UIControlStateNormal];
    [self.zoom setMinimumTrackImage: sliderTrackImage forState: UIControlStateNormal];
    [self.zoom setMaximumTrackImage: sliderTrackImage forState: UIControlStateNormal];

    [self.zoom setMinimumTrackTintColor:[UIColorFromRGB(0X624334) colorWithAlphaComponent:0.8]];
    [self.zoom setMaximumTrackTintColor:[UIColorFromRGB(0X938a7e) colorWithAlphaComponent:0.7]];
    [self.zoom addTarget:self
                  action:@selector(handleZoomChange:)
        forControlEvents:UIControlEventTouchDragInside];
    
    //ZOOM SLIDER
    
    //zoomLabel.textColor = [UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:180/255.f];
    //[zoomLabel setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:15]];
    [self.zoomView addSubview:self.zoom];
}


-(IBAction)handleZoomChange:(id)sender{
	// NSLog(@"%@", [NSString stringWithFormat:@"body.style.webkitTextSizeAdjust = %i+\"%%\";",(int)self.zoom.value]);
    if([Home isContentVisible])
    {
    Home* detailViewController = [Home sharedInstance];

    [detailViewController.webContent stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"body.style.webkitTextSizeAdjust = %i+\"%%\";",(int)self.zoom.value]];
    [detailViewController.webContent stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementById('ciccio').innerHTML=%i",(int)self.zoom.value]];
    [detailViewController.webContent stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"zoom=%i;",(int)self.zoom.value]];
    
    [self.defaults setObject:[detailViewController.webContent stringByEvaluatingJavaScriptFromString:@"document.getElementById('ciccio').innerHTML;"] forKey:ZOOM];
    }
    [self.defaults setObject:[NSString stringWithFormat:@"%i",(int)self.zoom.value] forKey:ZOOM];
    [self.defaults synchronize];
    
    zoomLabel.text = [NSString stringWithFormat:@"%@",[self.defaults objectForKey:ZOOM]];
}

-(IBAction)setZoomValue:(UIButton*)sender{
	// NSLog(@"%@", [NSString stringWithFormat:@"body.style.webkitTextSizeAdjust = %i+\"%%\";",(int)self.zoom.value]);
    if([Home isContentVisible])
    {
    Home* detailViewController = [Home sharedInstance];

     [detailViewController.webContent stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"body.style.webkitTextSizeAdjust = %i+\"%%\";",(int)sender.tag]];
     [detailViewController.webContent stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementById('ciccio').innerHTML=%i",(int)sender.tag]];
     [detailViewController.webContent stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"zoom=%i;",(int)self.zoom.value]];
     
     [self.defaults setObject:[detailViewController.webContent stringByEvaluatingJavaScriptFromString:@"document.getElementById('ciccio').innerHTML;"] forKey:ZOOM];
    }
    [self.defaults setObject:[NSString stringWithFormat:@"%i",(int)sender.tag] forKey:ZOOM];
    [self.defaults synchronize];
    
    [self.zoom setValue:sender.tag animated:YES];
    zoomLabel.text = [NSString stringWithFormat:@"%@",[self.defaults objectForKey:ZOOM]];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    // NSLog(@"Settings: gestureRecognizer ??");
    if ([touch.view isKindOfClass:[UIScrollView class]] || [touch.view isKindOfClass:[UIButton class]] || [touch.view isKindOfClass:[UISlider class]])
    {
        // NSLog(@"Settings: gestureRecognizer ignore");
        return NO;
    }
    return YES;
}


#pragma mark -
#pragma mark LUMINOSITA'
//=========================
// LUMINOSITA'
//=========================
-(void)costruisciSliderLUMI{
    //SLIDER LUMINOSITà
	self.mySlider =  [[UISlider alloc] initWithFrame:CGRectMake(0,0,lumiView.frame.size.width,23)];
	self.mySlider.maximumValue=0.7f;
	self.mySlider.minimumValue=0.0f;
    //[self.mySlider setThumbTintColor:[UIColorFromRGB(0X624334) colorWithAlphaComponent:0.8]];
    [self.mySlider setThumbImage:[UIImage imageNamed:@"lumi_img@2x"] forState:UIControlStateNormal];
    [self.mySlider setMinimumTrackTintColor:[UIColorFromRGB(0X928271) colorWithAlphaComponent:0.9]];
    [self.mySlider setMaximumTrackTintColor:[UIColorFromRGB(0Xbfae9d) colorWithAlphaComponent:0.85]];
    
	[self.mySlider setValue:[self.defaults floatForKey:LUMINOSITA]];
    
	[self.mySlider addTarget:self
					  action:@selector(handleSliderChange:)
			forControlEvents:UIControlEventTouchDragInside];
    
    [self.lumiView addSubview:self.mySlider];
}

-(IBAction)handleSliderChange:(id)sender{
	// NSLog(@"value : %f", self.mySlider.value);
	[self.defaults setFloat:mySlider.value forKey:LUMINOSITA];
    [self.defaults synchronize];
    Home *h = [Home sharedInstance];
	h.sfondo_content.alpha = 1 - mySlider.value;
}


#pragma mark -
#pragma mark CSS
//=========================
// CSS
//=========================
-(void)aggiornaBottoniCSS{
    NSInteger i = [self.defaults integerForKey:CSS];
    [b0 setSelected:(i==0)];
    [b1 setSelected:(i==1)];
    [b2 setSelected:(i==2)];
    [b3 setSelected:(i==3)];
    BOOL reverse = (i==2) && [Home isContentVisible];
    [black_mask setHidden:!reverse];
}
-(IBAction)cambiaCSS:(UIButton*)sender{
    if(sender.tag == [appDelegate.defaults integerForKey:CSS]) return;
    [b0 setSelected:(sender.tag==0)];
    [b1 setSelected:(sender.tag==1)];
    [b2 setSelected:(sender.tag==2)];
    [b3 setSelected:(sender.tag==3)];
    [appDelegate.defaults setInteger:sender.tag forKey:CSS];
    [appDelegate.defaults synchronize];
    BOOL reverse = (sender.tag==2) && [Home isContentVisible];
    [black_mask setHidden:!reverse];
    if([Home isContentVisible]) [Home reloadItem];
    [MenuViewController checkCSS];
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================


-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return self.isIpad?UIInterfaceOrientationMaskAll:UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return self.isIpad;
}

// Tell the system It should autorotate
- (BOOL) shouldAutorotate {
    return self.isIpad;
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
}

#pragma mark - Error CLass
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [super viewDidUnload];
}

//=========================
// END CLASS
//=========================
@end
