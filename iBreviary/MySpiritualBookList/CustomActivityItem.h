//
//  CustomActivityItem.h
//  iBreviary
//
//  Created by Leonardo Parenti on 09/05/15.
//  Copyright (c) 2015 leyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Nota.h"

@interface CustomActivityItem : NSObject <UIActivityItemSource>

- (id)initWithNota:(Nota*)n;

-(void)didFinishActivityAction:(BOOL)completed
                      withType:(NSString *)activityType
                   returnItems:(NSArray*)returnedItems
                           err:(NSError *)activityError;


@end
