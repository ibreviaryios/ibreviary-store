//
//  MySpiritualBookList.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "Lingua.h"
#import "FXBlurView.h"
#import "Tutorial.h"
#import "SpiritualCell.h"
#import "NotaCTRL.h"


@interface MySpiritualBookList : BaseController<TutorialDelegate,UIPickerViewDelegate,UIPickerViewDataSource,SpiritualCellDelegate,NewNotaDelegate,ModNotaDelegate>{
}


//SFONDO
@property (strong, nonatomic) IBOutlet UIImageView *sfondo;

//STRUCTURE
@property (strong, nonatomic) NSMutableArray *spiritualArray;

//TABELLA
@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (strong, nonatomic) IBOutlet UILabel *no_item;


//NAVBAR
@property (strong, nonatomic) IBOutlet UIView *nav_bar;
@property (strong, nonatomic) IBOutlet UIButton *close;
@property (strong, nonatomic) IBOutlet UIButton *btn_tutorial;


//TUTORIAL
@property (strong, nonatomic) FXBlurView *blurView;
@property (strong, nonatomic) Tutorial *tutorialCTRL;
@property (strong, nonatomic) IBOutlet UIImageView *tutorial_img;

@end
