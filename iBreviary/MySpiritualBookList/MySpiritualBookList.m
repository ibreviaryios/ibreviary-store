//
//  MySpiritualBookList.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

/*
 //================================
 //AGENDA MENU' PAROLE LOCALIZZATE
 //================================
 "TestiPropri" = "Proprio liturgico";
 "agenda_noitem" = "Nessun Testo Disponibile";
 "TestiPropriNoSelect" = "Nessun Testo Propio Selezionato";
 "testiPropri_abilita" = "Abilita";
 "testiPropri_disabilita" = "Disabilita";
*/

#import "MySpiritualBookList.h"
#import "Nota.h"
#import "Home.h"
#import "SpiritualCell.h"
#import "MainWebView.h"
#import "CustomActivityItem.h"
#import "BNHtmlPdfKit.h"


//====================
// BLOCK define
//====================
void (^animation_block)(void);
void (^completition_block)(BOOL);


@interface MySpiritualBookList ()<BNHtmlPdfKitDelegate,UIDocumentInteractionControllerDelegate>

//SHARE PDF
@property (strong, nonatomic) UIActivityIndicatorView *rotellaShare;
@property (strong, nonatomic) BNHtmlPdfKit *htmlPdfKit;
@property (nonatomic,weak) IBOutlet UIButton *shareFiltred_btn;
@property (strong) UIDocumentInteractionController *documentInteractionCtrl;

@property (nonatomic, assign) CGPoint scrollVelocity;
@property (nonatomic,assign) BOOL expandMenu;
@property (nonatomic,weak) IBOutlet UIView *menu_view;
@property (nonatomic,weak) IBOutlet UIButton *menu_btn;
@property (nonatomic,weak) IBOutlet UIView *search_view;
@property (nonatomic,weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic,strong) NSMutableArray *listaFiltrata;

@property (nonatomic,weak) IBOutlet UILabel *label_noitem;
@property (nonatomic,weak) IBOutlet UIView *view_noitem;
@property (nonatomic,weak) IBOutlet UIImageView *backImg_noitem;

@property (strong, nonatomic) IBOutlet UIButton *btn_closeKeyboard;

@property (nonatomic,weak) IBOutlet UIView *picker_view;
@property (strong, nonatomic) UIPickerView *date_picker;
@property (strong, nonatomic) NSMutableArray *monthData;
@property (strong, nonatomic) NSMutableArray *yearData;

@property (nonatomic,weak) IBOutlet MainWebView *webDetail;
@property (nonatomic,weak) IBOutlet UIView *web_area;
@property (nonatomic,weak) IBOutlet UIView *web_nav;
@property (nonatomic,weak) IBOutlet UIButton *web_close;

@property (nonatomic,weak) IBOutlet UIScrollView *scroll;
@property (nonatomic,weak) IBOutlet UIView *scrollArea;
@property (nonatomic,weak) IBOutlet UIView *scrollNav;
@property (nonatomic,weak) IBOutlet UIButton *scroll_close;
@property (nonatomic,strong) ContentViewForScroller* contentForScroll;
@property (nonatomic,assign) NSInteger zoomContent;
@property (nonatomic,strong) NSTimer *holdTimer;


@property (nonatomic,weak) IBOutlet UILabel *title_menu;
@property (nonatomic,weak) IBOutlet UIView *actionView_menu;

@property (nonatomic,weak) IBOutlet UIButton *tutorial_btn;
@property (nonatomic,assign) BOOL firstTime;

@property (strong, nonatomic) IBOutlet UIImageView *add_btn_img;
@property (strong, nonatomic) IBOutlet UIButton *add_btn;


@end

@implementation MySpiritualBookList

@synthesize listaFiltrata,btn_closeKeyboard;


#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)init
{
    
    //iPhone
    NSString *nibNameOrNil = @"MySpiritualBookList4";
    
    //iPADs
    //if(IS_IPAD) nibNameOrNil = @"Lista_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"MySpiritualBookList5";
    if(IS_IPHONE_6) nibNameOrNil = @"MySpiritualBookList6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"MySpiritualBookList6Plus";

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        [self view];
    }
    return self;
}


// Add this method
- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.spiritualArray = [NSMutableArray new];
    for(Nota*n in [Nota getNoteInContext:[AppDelegate mainManagedObjectContext]]){
        [self.spiritualArray addObject:n];
    }
    
    listaFiltrata = [NSMutableArray arrayWithArray:self.spiritualArray];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(iCloudChangesImported:) name:AGGIORNAMENTO_ICLOUD_NOTIFICA
                                               object:nil];
    
    [self initView];
}



- (void)initView{
    
    self.firstTime = YES;
    
    //TABELLA
    self.tabella.opaque = NO;
    self.tabella.backgroundView = nil;
    
    _searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    [_searchBar setBackgroundColor:[UIColor clearColor]];
    _searchBar.barTintColor = [UIColor clearColor];
    _searchBar.backgroundImage = [UIImage new];
    [self.search_view setHidden:YES];
    
    
    if(IS_IPAD){
        // NAVBAR frame
        CGRect nav_frame = self.nav_bar.frame;
        nav_frame.size.height = 40;
        [self.nav_bar setFrame:nav_frame];
        // MENU
        CGRect frame = (CGRect){0 ,nav_frame.size.height-1 ,self.menu_view.frame.size.width ,78};
        [self.menu_view setFrame:frame];

        [self.actionView_menu setCenter:CGPointMake(frame.size.width/2.0, self.actionView_menu.center.y)];
        [self.title_menu setCenter:CGPointMake(frame.size.width/2.0, self.title_menu.center.y)];
    }
    
    // NAVBAR COLOR
    UIColor *navC = [[UIColor colorFromHexString:@"#7B6356"] colorWithAlphaComponent:0.82];
    [self.nav_bar setBackgroundColor:navC];
    
    self.nav_bar.layer.shadowOpacity = .3;
    self.nav_bar.layer.shadowOffset = (CGSize){0, 3};
    self.nav_bar.layer.shadowColor = [UIColor blackColor].CGColor;
    

    // setting webView
    for (UIView *subview in _webDetail.subviews) {
        subview.clipsToBounds = NO;
    }
    _webDetail.clipsToBounds = NO;
    
    [self.web_nav setFrame:self.nav_bar.frame];
    [self.web_nav setBackgroundColor:navC];
    [self.web_area setFrame:self.view.frame];
    CGRect webFrame = self.web_area.frame;
    webFrame.origin.y = self.web_nav.frame.size.height;
    webFrame.size.height -= self.web_nav.frame.size.height;
    [self.webDetail setFrame:webFrame];

    
    // setting scroller
    for (UIView *subview in _scroll.subviews) {
        subview.clipsToBounds = NO;
    }
    _scroll.clipsToBounds = NO;
    [_scroll setMinimumZoomScale:1.0];
    [_scroll setMaximumZoomScale:2.2];

    [self.scrollNav setFrame:self.nav_bar.frame];
    [self.scrollNav setBackgroundColor:navC];
    [self.scroll setFrame:self.view.frame];
    CGRect sFrame = self.scroll.frame;
    sFrame.origin.y = self.scrollNav.frame.size.height;
    sFrame.size.height -= self.scrollNav.frame.size.height;
    [self.scroll setFrame:sFrame];


    
    //LOCALIZE CLOSE
    [self.close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
    
    [self.tutorial_img setImage:[UIImage filledImageFrom:[UIImage imageNamed:@"tutorialBTN.png"] withColor:[UIColor whiteColor]]];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(toggleMenu:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [self.menu_view addGestureRecognizer:swipeGesture];
    
    UISwipeGestureRecognizer *swipeGesture2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(toggleMenu:)];
    swipeGesture2.direction = UISwipeGestureRecognizerDirectionUp;
    [self.search_view addGestureRecognizer:swipeGesture2];

    [self hideMenuWithAnimation:NO];
    
    [self.view_noitem setHidden:YES];
}

-(void)checkNoItemLabel{
    if(self.label_noitem.text.length>0){
        
        NSString *path = [[NSBundle mainBundle] bundlePath];
        NSString *lingua = NSLocalizedString(@"checklingua", nil);
        NSString *pathFile = [NSString stringWithFormat:@"%@/mySpiritual_noitem_%@.html",path,lingua];
        
        //Debug Lingua Fissa
        //pathFile = [NSString stringWithFormat:@"%@/mySpiritual_noitem_ar.html",path];
        
        if(![self fileExistsAtPath:pathFile]){
            pathFile = [NSString stringWithFormat:@"%@/mySpiritual_noitem_%@.html",path,@"en"];
        }
        
        NSString *htmlString=[NSString stringWithContentsOfFile:pathFile encoding:NSUTF8StringEncoding error: nil];
        
        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        
        self.label_noitem.text = @"";
        
        self.label_noitem.attributedText = attrStr;
        [self.label_noitem sizeToFit];
        CGRect f = self.view_noitem.frame;
        f.size.width = IS_IPAD?350:280;
        [self.view_noitem.layer setCornerRadius:5];
        [self.view_noitem.layer setShadowRadius:5];
        [self.view_noitem.layer setShadowOffset:(CGSize){0,2}];
        [self.view_noitem.layer setShadowOpacity:0.8];
        [self.view_noitem.layer setShadowColor:[UIColor blackColor].CGColor];
        //[self.view_noitem.layer setMasksToBounds:YES];
        [self.view_noitem setFrame:f];
        [self.view_noitem setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)];
        [self.label_noitem setCenter:CGPointMake(self.view_noitem.frame.size.width/2.0, self.view_noitem.frame.size.height/2.0)];
    }
    
    [self.view_noitem setHidden:self.spiritualArray.count>0];
    
}

#pragma mark -
#pragma mark Appear
//=========================
// Appear
//=========================
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self checkNoItemLabel];
    
    if(self.firstTime)
        [self.tabella setHidden:YES];
    // NSLog(@"WILL APPEAR SPIRITUAL BOOK");
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(self.firstTime){
        [self.tabella reloadData];
        [self animateTableAppear];
        [self initPicker];
        self.firstTime = NO;
        
        if(![self.defaults boolForKey:TUTORIAL_MYSPIRITUAL_VISTO]){
            [self.defaults setBool:YES forKey:TUTORIAL_MYSPIRITUAL_VISTO];
            [self.defaults synchronize];
            [self showTutorial:nil];
        }

    }
}

#pragma mark -
#pragma mark Disappear
//=========================
// Disappear
//=========================
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}




//===========================
// TUTORIAL DELEGATE
//===========================
#pragma mark - TUTORIAL DELEGATE
- (IBAction)showTutorial:(UIButton*)sender{
    
    // STEP 1
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        // STEP 1
        [NSThread sleepForTimeInterval:0.4];
        [self showMenuWithAnimation:YES];
        
    }];//fine step1
    
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // STEP 2
        [NSThread sleepForTimeInterval:0.3];
    }];//fine step2
    
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        // STEP 3
        [self showTutorialQueue:sender];
    }];//fine step3
    

    if(sender==nil){
        [step2 addDependency:step1];
        [step3 addDependency:step2];
        [[NSOperationQueue mainQueue] addOperation:step1];
        [[NSOperationQueue mainQueue] addOperation:step2];
    }
    
    [[NSOperationQueue mainQueue] addOperation:step3];
}
    
- (void)showTutorialQueue:(UIButton*)sender{
    [self rotateImage:self.btn_tutorial.imageView withDuration:0.6 onY_axis:YES];
    // NSLog(@"showTutorial");
    
    
    
    CGRect frame = self.view.frame;
    self.tutorialCTRL = [[Tutorial alloc] initWithDelegate:self];
    
    
    self.blurView = [[FXBlurView alloc] initWithFrame:frame];
    
    CGPoint nuovo = [self.view convertPoint:self.btn_tutorial.center fromView:self.actionView_menu];
    [self.blurView setDynamic:NO];
    [self.blurView setTintColor:[UIColor brownColor]];
    [self.blurView addSubview:self.tutorialCTRL.view];
    self.blurView.layer.transform = CATransform3DMakeScale(0.03, 0.03, 1.0);
    [self.blurView setCenter:nuovo];
    [self.blurView setAlpha:0];
    
    [self.view addSubview:self.blurView];
    [UIView beginAnimations:@"AppareTutorialCTRL" context:NULL];
    [UIView setAnimationDuration:0.1];
    [self.blurView setAlpha:1];
    [UIView commitAnimations];
    [UIView animateWithDuration:0.4 animations:^{
                                                self.blurView.layer.transform = CATransform3DMakeScale(1, 1, 1.0);
                                                [self.blurView setCenter:self.view.center];
                                                }
                     completion:^(BOOL finished)    {
                         // NSLog(@"Fine Animation");
                     }];
    
}

-(void)removeTutorial{
    // NSLog(@"removeTutorial");
    [self rotateImage:self.tutorial_btn.imageView withDuration:0.65 onY_axis:YES];

    [UIView animateWithDuration:0.4 animations:^{
                                                self.blurView.layer.transform = CATransform3DMakeScale(0.03, 0.03, 1.0);
                                                CGPoint nuovo = [self.view convertPoint:self.btn_tutorial.center fromView:self.actionView_menu];
                                                nuovo = [self.view convertPoint:nuovo fromView:self.menu_view];
                                                nuovo.x +=32;
                                                nuovo.y -=10;
                                                [self.blurView setCenter:nuovo];
                                                }
                     completion:^(BOOL finished)    {
                         [UIView beginAnimations:@"SconpareTutorialCTRL" context:NULL];
                         [UIView setAnimationDuration:0.15];
                         [self.blurView setAlpha:0.0];
                         [UIView setAnimationDelegate:self.blurView];
                         [UIView setAnimationDidStopSelector:@selector(removeFromSuperview)];
                         [UIView commitAnimations];
                     }];
}

#pragma mark -
#pragma mark AZIONI
//=========================
// AZIONI
//=========================
- (IBAction)azioneDismiss:(UIButton*)sender
{
    [self chiudiTastiera:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [Home dismissMySpiritual];
}

#pragma mark -
#pragma mark --------> Table view
#pragma mark Header/Footer
// HEADER SESSION

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    // NAVBAR MINIMAL
//    CGFloat height = 20;
//    // SE E' ESPANSO
//    if(self.expandMenu) height = 95;
//    
//    //SE c'è la searchBar
//    if(!self.search_view.hidden) height += self.expandMenu?40:20;
//
//    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tabella.frame.size.width, height)];
//    [customView setBackgroundColor:[UIColor redColor]];
    UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
    return customView;
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    // NAVBAR MINIMAL
    CGFloat height = IS_IPAD?40.0:20.0;

    // SE E' ESPANSO
    if(self.expandMenu) height += 75;
    
    //SE c'è la searchBar
    if(!self.search_view.hidden){
        height += self.expandMenu?40:22;
        if(IS_IPAD&!self.expandMenu){
            height += 5;
        }
    }
    
    
    return height;
}


// FOOTER SESSION
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
    return customView;
}

#pragma mark session n°
// NUMERO SESSIONI IN TABELLA
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
#pragma mark row n°
//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    [self.view_noitem setHidden:self.spiritualArray.count>0];
    return self.listaFiltrata.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.isIpad?251.f:251.f;
}

#pragma mark CELL
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SpiritualCell *cell = [self cellForIndexPath:indexPath];
    
    return cell;
}

-(SpiritualCell *)cellForIndexPath:(NSIndexPath *)indexPath{
    //nota
    Nota *nota = [self.listaFiltrata objectAtIndex:indexPath.row];
    //cella
    SpiritualCell *cell = (SpiritualCell*)[self.tabella dequeueReusableCellWithIdentifier:IS_IPAD?@"SpiritualCellPAD":@"SpiritualCell_1"];
    
    if(!cell){
        [self.tabella registerNib:[UINib nibWithNibName:IS_IPAD?@"SpiritualCellPAD":@"SpiritualCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:IS_IPAD?@"SpiritualCellPAD":@"SpiritualCell_1"];
        cell = (SpiritualCell*)[self.tabella dequeueReusableCellWithIdentifier:IS_IPAD?@"SpiritualCellPAD":@"SpiritualCell_1"];
    }
    
    if(IS_IPHONE_6 || IS_IPHONE_6_PLUS){
        CGRect f = cell.frame;
        f.size.width = self.tabella.frame.size.width;
        [cell setFrame:f];
    }
    
    if(_searchBar.text.length>0){
        NSMutableAttributedString * attributeText = [[NSMutableAttributedString alloc] initWithString:nota.text];
        NSRange theRange = [nota.text rangeOfString:_searchBar.text options:NSCaseInsensitiveSearch];
        [attributeText addAttribute:NSBackgroundColorAttributeName value:[UIColor colorFromHexString:@"#FFFF35"] range:theRange];
        
        NSMutableAttributedString * attributeDate = [[NSMutableAttributedString alloc] initWithString:[nota dateString]];
        NSRange theDateRange = [[nota dateString] rangeOfString:_searchBar.text options:NSCaseInsensitiveSearch];
        [attributeDate addAttribute:NSBackgroundColorAttributeName value:[UIColor colorFromHexString:@"#FFFF35"] range:theDateRange];
        
        [cell setAttributeDate:attributeDate
              andAttributeText:attributeText
                       forNota:nota
                 withIndexPath:indexPath
                      delegate:self];
    }
    else{
        [cell setAttributeDate:[[NSAttributedString alloc] initWithString:[nota dateString]]
              andAttributeText:[[NSAttributedString alloc] initWithString:[nota text]]
                       forNota:nota
                 withIndexPath:indexPath
                      delegate:self];
    }
    
    return cell;
}

#pragma mark -
#pragma mark Cell action delegate
-(void)showSourceNota:(Nota*)nota{
    [self showNotaWebContent:[nota source_Htmlstring]];
}
-(void)deleteNota:(Nota*)nota{

    [self alert:NSLocalizedString(@"My Spiritual Book", nil)
         andMex:[NSString stringWithFormat:NSLocalizedString(@"delete_note_confirm", nil),[nota dateString]]
           icon:nil
       okAction:^(UIAlertAction*a){
           [self confirmDeleteNota:nota];
       }
     withCancel:YES];
}

-(void)confirmDeleteNota:(Nota*)nota{
    
    __block NSIndexPath* listaFiltratadeletePath =[NSIndexPath indexPathForRow:[listaFiltrata indexOfObject:nota] inSection:0];

    
    // STEP 1
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"step(1)");
        if(nota!=nil){
            [self.spiritualArray removeObject:nota];
            [listaFiltrata removeObject:nota];
        }
    }];//fine step1
    
    // STEP 2
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"step(2)");
        [self.tabella beginUpdates];
        [self.tabella
         deleteRowsAtIndexPaths:@[listaFiltratadeletePath]withRowAnimation:UITableViewRowAnimationBottom];
        [self.tabella endUpdates];
        [self checkNoItemLabel];

    }];//fine step2
    
    // STEP 3
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"step(3)");
        NSError *error = nil;
        [Nota deleteNota:nota andError:&error forContext:[AppDelegate mainManagedObjectContext]];

        // STEP 3
    }];//fine step3
    

        [step2 addDependency:step1];
        [step3 addDependency:step2];
    [[NSOperationQueue new] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    [[NSOperationQueue new] addOperation:step3];
}

-(void)modNota:(Nota*)nota{
    //===============================
    // NOTA MENAGE
    //===============================
        // NSLog(@"modNota:%@",nota);
        NotaCTRL *newNota = [[NotaCTRL alloc]  initWithNota:nota andModDelegate:self];
        [newNota.view setAlpha:0];
        [self displayViewController:newNota inView:self.view];
        [UIView animateWithDuration:0.5
                         animations:^{
                             [newNota.view setAlpha:1];
                         }
                         completion:^(BOOL fine){
                             [self resetMenuAndList];
                         }];
    }
    
-(void)dismissModMyNotaCtrl:(NotaCTRL*)n
                    andNota:(Nota *)nota_mod
                        mod:(BOOL)modifica{
        
    __block NSIndexPath* listaFiltrataModPath =[NSIndexPath indexPathForRow:[listaFiltrata indexOfObject:nota_mod] inSection:0];

        // STEP 0
        NSBlockOperation *step0 = [NSBlockOperation blockOperationWithBlock:^{
            // NSLog(@"step(0):%i",[NSThread isMainThread]);
            // creo semaforo
            __block dispatch_semaphore_t semo = dispatch_semaphore_create(0);
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     [n.view setAlpha:0];
                                 } completion:^(BOOL fine){
                                     //>>>semaforo verde!
                                     // NSLog(@"semaforo verde!:%i",[NSThread isMainThread]);
                                     dispatch_semaphore_signal(semo);
                                 }];
            });
            //aspetto semaforo verde <<<
            dispatch_semaphore_wait(semo, DISPATCH_TIME_FOREVER);
            // NSLog(@"fine!");
        }];//fine step0
    
    // STEP 1
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        //NSLog(@"step(1)");
        if(nota_mod!=nil){
            self.spiritualArray = [Nota getNoteInContext:[AppDelegate mainManagedObjectContext]];
            NSInteger row = [self.spiritualArray indexOfObject:nota_mod];
            [self.spiritualArray replaceObjectAtIndex:row withObject:nota_mod];
            [listaFiltrata removeAllObjects];
            [listaFiltrata addObjectsFromArray:self.spiritualArray];
            [self ripristinaLista];
        }
    }];//fine step1
    
    // STEP 2
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        //NSLog(@"step(2)");
        [self.tabella beginUpdates];
        [self.tabella
         reloadRowsAtIndexPaths:@[listaFiltrataModPath]withRowAnimation:UITableViewRowAnimationFade];
        [self.tabella endUpdates];
    }];//fine step2
    
    // STEP 3
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"step(3)");
        // STEP 3
        [self removeChildCTRL:n];
        [n removeViewController];
    }];//fine step3
    
    
        [[NSOperationQueue new] addOperation:step0];

    
        if(modifica){
            [step1 addDependency:step0];
            [step2 addDependency:step1];
            [step3 addDependency:step2];
            [[NSOperationQueue mainQueue] addOperation:step1];
            [[NSOperationQueue mainQueue] addOperation:step2];
        }
        else{
            [step3 addDependency:step0];
        }
        
        [[NSOperationQueue new] addOperation:step3];
}

-(IBAction)shareFiltredNotesWithSender:(UIButton*)sender{
    
    // STEP 0
    NSBlockOperation *step0 = [NSBlockOperation blockOperationWithBlock:^{
        //NSLog(@"shareFiltredNotes:step(0)");
        [self.view setUserInteractionEnabled:NO];
        self.rotellaShare = [[UIActivityIndicatorView alloc] initWithFrame:sender.frame];
        [self.rotellaShare setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
        [self.rotellaShare setColor:UIColorFromRGB(0XFFFFFF)];
        [self.rotellaShare setCenter:CGPointMake(self.rotellaShare.center.x, self.rotellaShare.center.y+3)];
        [self.rotellaShare setAlpha:0.0];
        [self.rotellaShare setTransform:CGAffineTransformMakeScale(0.5f, 0.5f)];
        [self.rotellaShare startAnimating];
        [sender.superview addSubview:self.rotellaShare];
        [self.rotellaShare setAlpha:1];
    }];//fine step1
    
    
    
    // STEP 1
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        //NSLog(@"shareFiltredNotes:step(1)");
        [self creaPDF];
    }];//fine step1
    
    
    
    
    [step1 addDependency:step0];
    [[NSOperationQueue mainQueue] addOperation:step0];
    [[NSOperationQueue mainQueue] addOperation:step1];
}


-(void)creaPDF{
    self.htmlPdfKit = [[BNHtmlPdfKit alloc] initWithPageSize:BNPageSizeA4];
    self.htmlPdfKit.delegate = self;
    [self.htmlPdfKit saveHtmlAsPdf:[Nota htmlPdfStringFromArray:self.listaFiltrata] toFile:[self.cacheDir stringByAppendingPathComponent:@"MySPiritualBook.pdf"]];
}

//==================================
// PDF DELEGATE
//==================================
#pragma mark - PDF DELEGATE -
- (void)htmlPdfKit:(BNHtmlPdfKit *)h didSavePdfFile:(NSString *)file{
    // NSLog(@"didSavePdfFile[%i]:%@", h.pageSize,file);
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        self.documentInteractionCtrl = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:file]];
        [self.documentInteractionCtrl setUTI:@"com.adobe.pdf"];
        
        self.documentInteractionCtrl.delegate = self;
        CGRect frame = [self.shareFiltred_btn.superview convertRect:self.shareFiltred_btn.frame toView:[self view]];
        frame.origin.y -= 7;
        frame.origin.x -= 1.5;
        [self.documentInteractionCtrl presentOptionsMenuFromRect:frame inView:self.view animated:YES];
        
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // STEP removeRotellaStep
        [self.rotellaShare stopAnimating];
        [self.rotellaShare removeFromSuperview];
        [self.view setUserInteractionEnabled:YES];
        
    }];//fine step2
    
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    
}
- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didFailWithError:(NSError *)error{
    // NSLog(@"didFailWithError:%@",[error description]);
    
}

#pragma mark - document delegate
-(void)documentInteractionController:controller willBeginSendingToApplication:(NSString *)application{
    // NSLog(@"documentInteractionController:%@",application);
}

-(void)documentInteractionControllerWillPresentOptionsMenu:(UIDocumentInteractionController *)controller{
    // NSLog(@"documentInteractionControllerWillPresentOptionsMenu");
}
-(void)documentInteractionControllerDidDismissOptionsMenu:(UIDocumentInteractionController *)controller{
    //NSLog(@"documentInteractionControllerDidDismissOptionsMenu");
}

//===========================
// SOCIAL
//===========================
#pragma mark - SOCIAL SHARE
-(void)demoImageNota:(Nota*)nota{
    UIImageView *i = [[UIImageView alloc] initWithImage:nota.cartolina];
    [[[Home sharedInstance] view] addSubview:i];
    [i setCenter:[[[Home sharedInstance] CONTENT] center]];
}


-(void)shareNota:(Nota*)nota sender:(SpiritualCell*)sender{
    
    __block UIActivityIndicatorView *rotella = nil;
    
    // STEP 0
    NSBlockOperation *step0 = [NSBlockOperation blockOperationWithBlock:^{
        //NSLog(@"shareNota:step(0)");
        [self.view setUserInteractionEnabled:NO];
        rotella = [[UIActivityIndicatorView alloc] initWithFrame:sender.share_btn.frame];
        [rotella setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
        [rotella setColor:UIColorFromRGB(0XFFFFFF)];
        [rotella setCenter:CGPointMake(rotella.center.x, rotella.center.y+3)];
        [rotella setAlpha:0.0];
        [rotella setTransform:CGAffineTransformMakeScale(0.5f, 0.5f)];
        [rotella startAnimating];
        [sender.share_btn.superview addSubview:rotella];
        [rotella setAlpha:0.8];
    }];//fine step1
    
    // STEP removeRotellaStep
    NSBlockOperation *removeRotellaStep = [NSBlockOperation blockOperationWithBlock:^{
        //NSLog(@"shareNota:removeRotellastep");
        [rotella stopAnimating];
        [rotella removeFromSuperview];
        [self.view setUserInteractionEnabled:YES];
    }];//fine step1

    
    // STEP 1
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        //NSLog(@"shareNota:step(1)");
        [nota cartolina];
    }];//fine step1
    
    // STEP 2
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        //NSLog(@"shareNota:step(2)");
        //[self demoImageNota:nota];
        NSArray *excludedType          = @[UIActivityTypePostToWeibo,
                                           UIActivityTypeAssignToContact,
                                           UIActivityTypeAddToReadingList,
                                           UIActivityTypePostToVimeo,
                                           UIActivityTypePostToTencentWeibo];
        
        
        //you can have your own custom activities too:
        /*
         NSArray *applicationActivities = @[[CustomActivity new],
         [OtherCustomActivity new]];
         */
        CustomActivityItem* activityItem = [[CustomActivityItem alloc] initWithNota:nota];
        NSArray *a;
        if(self.version<9.f){
            a = @[[nota getImageUrl],activityItem];
        }
        else{
            a = @[[nota cartolina],activityItem];
        }
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:a applicationActivities:nil];
        activityVC.excludedActivityTypes = excludedType;
        
        if([[UIApplication sharedApplication] respondsToSelector:(@selector(setCompletionWithItemsHandler:))]){
            activityVC.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
                [activityItem        didFinishActivityAction:completed
                                                    withType:activityType
                                                 returnItems:returnedItems
                                                         err:activityError];
                };
            }
        else{
            [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed) {
                    [activityItem        didFinishActivityAction:completed
                                                        withType:activityType
                                                     returnItems:nil
                                                             err:nil];
                    }];
        }
        
        
        CGRect frame = [sender.share_btn.superview convertRect:sender.share_btn.frame toView:[self view]];
        frame.origin.y -= 7;
        
        if(self.version>7.9){
            [activityVC.popoverPresentationController setSourceRect:frame];
            [activityVC.popoverPresentationController setSourceView:self.view];
        }
        
        [self presentViewController:activityVC animated:YES completion:^{
        
            [[NSOperationQueue mainQueue] addOperation:removeRotellaStep];
        }];
    }];//fine step2
    
    
    
    [step1 addDependency:step0];
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step0];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
}

#pragma mark -
#pragma mark Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // NSLog(@"didSelectRowAtIndexPath:%i",(int)indexPath.row);
    Nota *nota = [self.listaFiltrata objectAtIndex:indexPath.row];
    [self showNotaScroller:nota];
    //[self showNotaWebContent:[nota htmlPdfString]];
}

-(void)showNotaWebContent:(NSString*)content{
    [self.webDetail loadHTMLString:content baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    [self.web_area setAlpha:0];
    [self.web_area setHidden:NO];
    
    
    animation_block = ^{
        [self.web_area setAlpha:1];
    };
    
    completition_block = ^(BOOL finished) {
    };
    
    [UIView animateWithDuration:0.4
                     animations:animation_block
                     completion:completition_block];
}

-(IBAction)closeWebContent:(UIButton*)sender{
    animation_block = ^{
        [self.web_area setAlpha:0];
    };
    
    completition_block = ^(BOOL finished) {
        [self.web_area setHidden:YES];
        [self.webDetail loadHTMLString:@"" baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    };
    
    [UIView animateWithDuration:0.7
                     animations:animation_block
                     completion:completition_block];

}

#pragma mark - WEB-DELEGATE
//=========================
// WEB-DELEGATE
//=========================
-(void)showNotaScroller:(Nota*)nota{
    for(UIView*sub in self.scroll.subviews){
        [sub removeFromSuperview];
    }
    
    self.contentForScroll = [nota fullContentForScrollView:self.scroll];
    self.zoomContent = 0;

    
    [self.scrollArea setAlpha:0];
    [self.scrollArea setHidden:NO];
    
    
    animation_block = ^{
        [self.scrollArea setAlpha:1];
    };
    
    completition_block = ^(BOOL finished) {
        //NSLog(@"self.scrollArea:%@",self.scrollArea);
    };
    
    [UIView animateWithDuration:0.4
                     animations:animation_block
                     completion:completition_block];
}

-(IBAction)closeNotaScroller:(UIButton*)sender{
    animation_block = ^{
        [self.scrollArea setAlpha:0];
    };
    
    completition_block = ^(BOOL finished) {
        [self.scrollArea setHidden:YES];
        for(UIView*sub in self.scroll.subviews){
            [sub removeFromSuperview];
        }
        self.zoomContent = 0;
        self.contentForScroll = nil;
    };
    
    [UIView animateWithDuration:0.7
                     animations:animation_block
                     completion:completition_block];
    
}


-(IBAction)zoomLabel:(UIButton*)sender
{
    [self zoomLabelAction];
    self.holdTimer = [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(zoomLabelAction) userInfo:nil repeats:YES];
}

-(IBAction)reduceLabel:(UIButton*)sender
{
    [self reduceLabelAction];
    self.holdTimer = [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(reduceLabelAction) userInfo:nil repeats:YES];
}

-(IBAction)stopAction: (id)sender
{
    [self.holdTimer invalidate];
    self.holdTimer = nil;
}

-(void)zoomLabelAction{
    if(self.zoomContent > 100) return;
    self.zoomContent += 2;
    [self.contentForScroll incrementFontSize:self.zoomContent forScrollView:self.scroll];
}

-(void)reduceLabelAction{
    if(self.zoomContent <= 0) return;
    self.zoomContent -= 2;
    [self.contentForScroll incrementFontSize:self.zoomContent forScrollView:self.scroll];
}


#pragma mark - WEB-DELEGATE
//=========================
// WEB-DELEGATE
//=========================
- (BOOL)webView:(UIWebView *)w shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    //NSLog(@"shouldStartLoadWithRequest:%@",request);
    
    if ( navigationType == UIWebViewNavigationTypeLinkClicked ) {
          return NO;
    }
    
    return YES;
}


- (void)webViewDidFinishLoad:(UIWebView *)webView{
    //NSLog(@"MySpiritual:webViewDidFinishLoad");
  }


#pragma mark - Animate Table
//==========================
// ANIMATE TABLE
//==========================
-(void)iCloudChangesImported:(NSNotification *)notification {
    NSLog(@"MySpiritual: iCloud aggiornamento avvenuto");
    if(!self.view.superview) return;
    // STEP 1
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        // STEP 1
        [self animateTableDisappear];
        
    }];//fine step1
    
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // STEP 2
        [self.tabella setHidden:YES];
        [listaFiltrata removeAllObjects];
        self.spiritualArray = [Nota getNoteInContext:[AppDelegate mainManagedObjectContext]];
        [listaFiltrata addObjectsFromArray:self.spiritualArray];
        [NSThread sleepForTimeInterval:1];
    }];//fine step2
    
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        // STEP 3
        [self.tabella reloadData];
        [self animateTableAppear];
    }];//fine step3

    
    
    
    
    [step2 addDependency:step1];
    [step3 addDependency:step2];
    [[NSOperationQueue new] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    [[NSOperationQueue mainQueue] addOperation:step3];

}


#pragma mark - Animate Table
//==========================
// ANIMATE TABLE
//==========================
-(void)animateTableAppear{
    
    [self.tabella setHidden:NO];
    NSArray *visibleCell = [self.tabella visibleCells];
    for(__block NSUInteger idx = 0; idx < visibleCell.count; idx++){
        __block UITableViewCell *cell = [visibleCell objectAtIndex:idx];
        CGPoint origin = cell.frame.origin;
        [cell setFrame:CGRectMake(cell.frame.origin.x, self.tabella.frame.size.height+self.tabella.contentOffset.y, cell.frame.size.width, cell.frame.size.height)];
        [UIView animateWithDuration:0.7
                              delay:(0.4+(0.1*(float)idx))
             usingSpringWithDamping:0.9
              initialSpringVelocity:15
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [cell setFrame:CGRectMake(origin.x, origin.y, cell.frame.size.width, cell.frame.size.height)];
                         } completion:^(BOOL fine){
                             
                         }];
        
    }
    [self.tabella setHidden:NO];
}

-(void)animateTableDisappear{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        // NSLog(@"animateTableDisappear:%i",[NSThread isMainThread]);
        
        //creo semaforo
        __block dispatch_semaphore_t sem = dispatch_semaphore_create(0);
        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
        });
        NSArray *reverseCell = [[[self.tabella visibleCells] reverseObjectEnumerator] allObjects];
        dispatch_async(dispatch_get_main_queue(), ^{
            __block NSMutableArray *fine_idx = [NSMutableArray new];
            for(__block NSUInteger idx = 0; idx < reverseCell.count; idx++){
                UITableViewCell *cell = [reverseCell objectAtIndex:idx];
                [UIView animateWithDuration:0.9
                                      delay:0.4+(0.1*(float)idx)
                     usingSpringWithDamping:0.9
                      initialSpringVelocity:16
                                    options:UIViewAnimationOptionCurveEaseIn
                                 animations:^{
                                     [cell setFrame:CGRectMake(cell.frame.origin.x, self.tabella.contentOffset.y+self.tabella.frame.size.height, cell.frame.size.width, cell.frame.size.height)];
                                     
                                 } completion:^(BOOL fineshed){
                                     [fine_idx addObject:@(1)];
                                     //DDLogDebug(@"fine_idx=%d",fine_idx.count);
                                     if(fine_idx.count == reverseCell.count){
                                         //>>>semaforo verde!
                                         dispatch_semaphore_signal(sem);
                                     }
                                 }];
                
            }
        });//fine codice main
        
        //aspetto semaforo verde <<<
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    });
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CATransform3D translation;
//    if(goingUP){
//        translation = CATransform3DMakeTranslation(0, MIN(abs(self.scrollVelocity.y), cell.frame.size.height), 0);
//    } else {
//        translation = CATransform3DMakeTranslation(0, -MIN(abs(self.scrollVelocity.y), cell.frame.size.height), 0);
//    }
//    
//    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
//    cell.layer.shadowOffset = CGSizeMake(10, 10);
//    cell.layer.transform = translation;
//    cell.layer.anchorPoint = CGPointMake(0, 0.5);
//    
//    if(cell.layer.position.x != 0){
//        cell.layer.position = CGPointMake(0, cell.layer.position.y);
//    }
//    
//    [UIView beginAnimations:@"translation" context:NULL];
//    [UIView setAnimationDuration:0.3];
//    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//    cell.layer.transform = CATransform3DIdentity;
//    cell.layer.shadowOffset = CGSizeMake(0, 0);
//    [UIView commitAnimations];
}

CGFloat lastContentOffset;
BOOL goingUP;

#pragma mark - ScrollView delegate (for top animation)
//===================
//ScrollView delegate
//===================

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    if (lastContentOffset > scrollView.contentOffset.y)
//        goingUP = NO;
//    else if (lastContentOffset <= scrollView.contentOffset.y)
//        goingUP = YES;
//    
//    lastContentOffset = scrollView.contentOffset.y;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.scrollVelocity = [scrollView.panGestureRecognizer velocityInView:scrollView];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    self.scrollVelocity = [scrollView.panGestureRecognizer velocityInView:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    self.scrollVelocity = [scrollView.panGestureRecognizer velocityInView:scrollView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    
}


#pragma mark -
#pragma mark ERROR CLASS
//=========================
// ERROR CLASS
//=========================
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================
-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    
    if(self.isIpad){
        // NSLog(@"Supported Orient:ALL");
        return [[Home sharedInstance] supportedInterfaceOrientations];
    }
    
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate {
    return self.isIpad;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    // NSLog(@"MySpiritual:didRotate:%i",self.isLand);
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]){
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    // NSLog(@"MySpiritual: willRotateToInterfaceOrientation");
}

-(void)willRotate{
    // NSLog(@"MySpiritual: willRotateToInterfaceOrientation");
}


-(void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    // NSLog(@"MySpiritual:willTransitionToTraitCollection");
    [self willRotate];
}

-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    //NSLog(@"MySpiritual:viewWillTransitionToSize");
    [self willRotate];
}


#pragma mark - ShowMenu
//=========================
// SHOW MENU
//=========================
-(IBAction)toggleMenu:(UIButton*)sender{
    if (self.expandMenu) {
            [self hideMenuWithAnimation:YES];
        }
    else
        {
            [self showMenuWithAnimation:YES];
        }
}

//SHOW MENU
-(void)showMenuWithAnimation:(BOOL)animated{
    animation_block = ^{
        //calc height
        CGRect frame = self.menu_view.frame;
        frame.size.height = 78;
        [self.menu_view setFrame:frame];
        CGRect searchFrame = self.search_view.frame;
        searchFrame.origin.y = IS_IPAD?122:102;
        [self.search_view setFrame:searchFrame];
        };
    
    completition_block = ^(BOOL finished) {
        self.expandMenu = YES;
        //cambia immagine!!
        [self.menu_btn setImage:[UIImage imageNamed:@"menu_su"] forState:UIControlStateNormal];
        [self.menu_btn setTitle:@"" forState:UIControlStateNormal];
        
        if(listaFiltrata.count>0){

            NSIndexPath *indexPath;
            for(int i=0 ; i < [self.tabella indexPathsForVisibleRows].count ; i++){
                    NSIndexPath *index = [[self.tabella indexPathsForVisibleRows] objectAtIndex:i];
                    CGRect cellRect = [self.tabella rectForRowAtIndexPath:index];
                    BOOL completelyVisible = CGRectContainsRect(self.tabella.bounds, cellRect);
                    if(completelyVisible){
                        indexPath = index;
                        i = (int)[self.tabella indexPathsForVisibleRows].count;
                    }
            }
            //NSLog(@"scrollto:%@",[[listaFiltrata objectAtIndex:indexPath.row] dateString]);
            [self.tabella beginUpdates];
            [self.tabella endUpdates];
            [self.tabella scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
        };

    if(animated){
        [UIView animateWithDuration:0.6
                              delay:0
             usingSpringWithDamping:0.65
              initialSpringVelocity:1
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:animation_block
                         completion:completition_block];
    }
    else
    {
        animation_block();
        completition_block(YES);
        
    };
}

//HIDE MENU
-(void)hideMenuWithAnimation:(BOOL)animated{
    if(self.menu_view.frame.size.height==0) return;
    animation_block = ^{
        CGRect frame = self.menu_view.frame;
        frame.size.height = 0;
        [self.menu_view setFrame:frame];
        
        CGRect searchFrame = self.search_view.frame;
        searchFrame.origin.y = IS_IPAD?44:22;
        [self.search_view setFrame:searchFrame];
        };
    
    completition_block = ^(BOOL finished) {
        self.expandMenu = NO;
        //cambia immagine!!
        [self.menu_btn setTitle:[@"menu" uppercaseString] forState:UIControlStateNormal];
        [self.menu_btn setImage:[UIImage new] forState:UIControlStateNormal];
        if(listaFiltrata.count>0){
            NSIndexPath *indexPath;
            for(int i=0 ; i < [self.tabella indexPathsForVisibleRows].count ; i++){
                NSIndexPath *index = [[self.tabella indexPathsForVisibleRows] objectAtIndex:i];
                CGRect cellRect = [self.tabella rectForRowAtIndexPath:index];
                BOOL completelyVisible = CGRectContainsRect(self.tabella.bounds, cellRect);
                if(completelyVisible){
                    indexPath = index;
                    i = (int)[self.tabella indexPathsForVisibleRows].count;
                }
            }
            //NSLog(@"scrollto:%@",[[listaFiltrata objectAtIndex:indexPath.row] dateString]);
            [self.tabella beginUpdates];
            [self.tabella endUpdates];
            [self.tabella scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    };
    
    if(animated){
        [UIView animateWithDuration:0.6
                              delay:0
             usingSpringWithDamping:0.8
              initialSpringVelocity:5
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:animation_block
                         completion:completition_block];
    }
    else
    {
        animation_block();
        completition_block(YES);
        
    };
}

-(void)ripristinaLista{
    // NSLog(@"RipristinaLista");
    [listaFiltrata removeAllObjects];
    self.spiritualArray = [Nota getNoteInContext:[AppDelegate mainManagedObjectContext]];
    [listaFiltrata addObjectsFromArray:self.spiritualArray];
    [self.tabella reloadData];
}

-(void)resetMenuAndList{
    //SE c'è la searchBar
    if(!self.search_view.hidden){
        [UIView animateWithDuration:0.4
                         animations:^{
                             [self.search_view setAlpha:0];
                         } completion:^(BOOL finito){
                             self.search_view.hidden = YES;
                         }];
    };
    //SE c'è PICKER
    if(self.picker_view.frame.origin.y!=self.view.frame.size.height){
        [self hidePicker];
    }
    
    //CHIUDO MENU
    [self hideMenuWithAnimation:NO];
    
    //RIPRISTINO LISTA
    [self ripristinaLista];
}

//===============================
// NOTA MENAGE
//===============================
#pragma mark - NOTA
-(IBAction)newNota:(UIButton*)sender{
    //NSLog(@"newNota");
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self addActivityOnView:self.add_btn_img];
    }];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NotaCTRL *newNota = [[NotaCTRL alloc] initNewNotaWithDelegate:self];
        [newNota.view setAlpha:0];
        [self displayViewController:newNota inView:self.view];
        [UIView animateWithDuration:0.5
                         animations:^{
                             [newNota.view setAlpha:1];
                         }
                         completion:^(BOOL fine){
                             [self removeActivityOnView:self.add_btn_img];
                             [self resetMenuAndList];
                         }];
    }];
    }

//===========================
// ActivityOnButton
//===========================
-(void)addActivityOnView:(UIView*)sender{
    UIActivityIndicatorView *a = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [a startAnimating];
    CGPoint center = (CGPoint){sender.frame.origin.x+(sender.frame.size.width/2.0),sender.frame.origin.y+(sender.frame.size.height/2.0)};
    [a setTransform:CGAffineTransformMakeScale(1.f, 1.f)];
    [a setCenter:center];
    [a setTag:1234];
    [sender.superview addSubview:a];
    [sender setHidden:YES];
}

-(void)removeActivityOnView:(UIView*)sender{
    [[sender.superview viewWithTag:1234] removeFromSuperview];
    [sender setHidden:NO];
}

-(void)dismissNewMyNotaCtrl:(NotaCTRL*)n andNota:(Nota*)new_nota{
    
    BOOL newNota = new_nota!=nil;
    
     // STEP 0
    NSBlockOperation *step0 = [NSBlockOperation blockOperationWithBlock:^{
        //NSLog(@"step(0):%i",[NSThread isMainThread]);
        //creo semaforo
        __block dispatch_semaphore_t semo = dispatch_semaphore_create(0);
        
        dispatch_sync(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.5
                         animations:^{
                             [n.view setAlpha:0];
                         } completion:^(BOOL fine){
                             //>>>semaforo verde!
                             dispatch_semaphore_signal(semo);
                         }];
        });
        //aspetto semaforo verde <<<
        dispatch_semaphore_wait(semo, DISPATCH_TIME_FOREVER);
    }];//fine step0
    
    // STEP 1
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        //NSLog(@"step(1)");
        if(new_nota!=nil){
        self.spiritualArray = [Nota getNoteInContext:[AppDelegate mainManagedObjectContext]];
        
        //NSLog(@"Add item:%@",[self.spiritualArray firstObject]);
        
        [listaFiltrata insertObject:[self.spiritualArray firstObject] atIndex:0];
        }
    }];//fine step1
    
    // STEP 2
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        //NSLog(@"step(2)");
        [self checkNoItemLabel];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tabella beginUpdates];
        [self.tabella
         insertRowsAtIndexPaths:@[indexPath]withRowAnimation:UITableViewRowAnimationTop];
        [self.tabella endUpdates];
    }];//fine step2
    
    // STEP 3
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        //NSLog(@"step(3)");
        // STEP 4
        [self removeChildCTRL:n];
        [n removeViewController];
        [AppDelegate saveContext];
    }];//fine step3

    
    

    if(newNota){
        [step1 addDependency:step0];
        [step2 addDependency:step1];
        [step3 addDependency:step2];
        [[NSOperationQueue new] addOperation:step1];
        [[NSOperationQueue mainQueue] addOperation:step2];
    }
    else{
        [step3 addDependency:step0];
    }

    [[NSOperationQueue new] addOperation:step0];
    [[NSOperationQueue new] addOperation:step3];
}

#pragma mark - SEARCHBAR
//=========================
// SEARCHBAR
//=========================
-(IBAction)toggleSearchBar:(UIButton*)sender{
    BOOL shouldShow = self.search_view.hidden;
    if(IS_IPAD) {
        CGRect fs = self.search_view.frame;
        fs.origin.y = 122;
        [self.search_view setFrame:fs];
    }
    if(self.spiritualArray.count==0) shouldShow = NO;
    [sender setSelected:shouldShow];
    [sender setAlpha:shouldShow?0.8:0.6];
    if(shouldShow){
        [self.search_view setAlpha:0];
        [self.search_view setHidden:NO];
    }
    [UIView animateWithDuration:0.4
                     animations:^{
                         [self.search_view setAlpha:shouldShow];
                     } completion:^(BOOL finito){
                            }];
    
    if(!shouldShow){
        [self.search_view setHidden:YES];
        [_searchBar resignFirstResponder];
        [btn_closeKeyboard setHidden:YES];
    }
    
    NSIndexPath *indexPath = nil;
    for(int i=0 ; i < [self.tabella indexPathsForVisibleRows].count ; i++){
        NSIndexPath *index = [[self.tabella indexPathsForVisibleRows] objectAtIndex:i];
        CGRect cellRect = [self.tabella rectForRowAtIndexPath:index];
        BOOL completelyVisible = CGRectContainsRect(self.tabella.bounds, cellRect);
        if(completelyVisible){
            indexPath = index;
            i = (int)[self.tabella indexPathsForVisibleRows].count;
        }
        
        [self.tabella beginUpdates];
        [self.tabella endUpdates];
        
        
        if(indexPath != nil && self.listaFiltrata.count<indexPath.row){
            //NSLog(@"scrollto:%@",[[listaFiltrata objectAtIndex:indexPath.row] dateString]);
            [self.tabella scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }

}

#pragma mark delegate search
-(void)searchBar:(UISearchBar *)s textDidChange:(NSString *)searchText{
    //NSLog(@"textDidChangeQUI:%@",_searchBar.text);
    if(self.spiritualArray.count<=0) return;
    
    if([_searchBar.text isEqualToString:@""]) {
        // NSLog(@"cancel!");
        [self ripristinaLista];
        return;
    }
    [self searchForString];
}

-(void)searchForString{
    [listaFiltrata removeAllObjects];
    
    NSString* searchText;
    NSString* dateText;
    
    if(_searchBar.text.length>0) searchText = _searchBar.text;
    
    if([_date_picker selectedRowInComponent:1]!=0 && [_date_picker selectedRowInComponent:0]!=0){
        dateText = [NSString stringWithFormat:@"%@ %@",[_monthData objectAtIndex:[_date_picker selectedRowInComponent:0]],[_yearData objectAtIndex:[_date_picker selectedRowInComponent:1]]];
    }

    else if([_date_picker selectedRowInComponent:0]!=0)
            dateText = [_monthData objectAtIndex:[_date_picker selectedRowInComponent:0]];
    else if([_date_picker selectedRowInComponent:1]!=0)
            dateText = [_yearData objectAtIndex:[_date_picker selectedRowInComponent:1]];
    else dateText = nil;
    
    if(!dateText || dateText.length==0){
        [self ripristinaLista];
        [self.tabella reloadData];
        return;
    }

    for (Nota *dict in self.spiritualArray)
        {
            BOOL TextTest = NO;
            BOOL dateTest = NO;
        if([dict dateString])
            {
                NSString * d = dateText;
                if(!dateText) d = searchText;
                NSRange r = [dict.dateString rangeOfString:dateText options:NSCaseInsensitiveSearch];
                //NSLog(@"DATEsearching:%@ in:%@ r:%@",dateText,dict.dateString,NSStringFromRange(r));
                if (r.location != NSNotFound) dateTest = YES;
            }

        if(dict.text)
            {
                if(searchText.length>0){
                    NSRange r = [dict.text rangeOfString:searchText options:NSCaseInsensitiveSearch];
                    //NSLog(@"STRINGsearching:%@ in:%@ r:%@",dateText,dict.dateString,NSStringFromRange(r));
                    if (r.location != NSNotFound) TextTest = YES;
                }
            }
        
            
        if(dateTest || (TextTest && !dateText))
            [listaFiltrata addObject:dict];
    }
    [self.tabella reloadData];
}

- (IBAction)searchBarCancelButtonClicked:(UISearchBar *)s{
    // NSLog(@"searchBarCancelButtonClicked:%@",searchBar);
    _searchBar.text = @"";
    [self ripristinaLista];
    [_searchBar resignFirstResponder];
    [btn_closeKeyboard setHidden:YES];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    // NSLog(@"selectedScopeButtonIndexDidChange");
    [UIView transitionWithView: btn_closeKeyboard
                      duration: 0.7
                       options: UIViewAnimationOptionTransitionFlipFromTop
                    animations: ^{
                        [btn_closeKeyboard setHidden:NO];
                    }
                    completion:^(BOOL finished){
                        // NSLog(@"Fine");
                    }];
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)s{
    // NSLog(@"searchBarSearchButtonClicked");
    [_searchBar resignFirstResponder];
    [btn_closeKeyboard setHidden:YES];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)s{
    // NSLog(@"searchBarShouldBeginEditing");
    [UIView transitionWithView: btn_closeKeyboard
                      duration: 0.7
                       options: UIViewAnimationOptionTransitionFlipFromTop
                    animations: ^{
                        [btn_closeKeyboard setHidden:NO];
                    }
                    completion:^(BOOL finished){
                        // NSLog(@"Fine");
                    }];
    return YES;
}

-(IBAction)chiudiTastiera:(UIButton*)sender{
    [btn_closeKeyboard setHidden:YES];
    [_searchBar performSelectorOnMainThread:@selector(resignFirstResponder) withObject:nil waitUntilDone:YES];
}

#pragma mark -
#pragma mark Picker
//=========================
// Picker
//=========================
-(IBAction)togglePicker:(UIButton*)sender{
    BOOL shouldShow = self.picker_view.frame.origin.y == self.view.frame.size.height;
    [sender setSelected:shouldShow];
    [sender setAlpha:shouldShow?0.8:0.6];
    if(shouldShow) [self showPicker];
    else [self hidePicker];
}

- (void)initPicker
{
    
    //PICKER
    CGFloat margin = 30;
    CGRect picker_frame = self.picker_view.bounds;
    picker_frame.origin.x = margin;
    picker_frame.origin.y -=14;
    picker_frame.size.width -= margin*2.;
    self.date_picker = [[UIPickerView alloc] initWithFrame:picker_frame];
        //[self.date_picker setSoundsEnabled:NO];
    
    [self.date_picker setDelegate:self];
    [self.date_picker setDataSource:self];
    
    
    //hide pickerview
    CGRect frame = self.picker_view.frame;
    frame.origin.y = self.view.frame.size.height;
    frame.size.height = self.date_picker.frame.size.height - 60.f;
    frame.size.height += 20;
    [self.picker_view setFrame:frame];

    
    [self prepareMonths];
    [self prepareYear];
    
    [self.picker_view addSubview:self.date_picker];
    [self.picker_view setBackgroundColor:[[UIColor colorFromHexString:@"7B6356"] colorWithAlphaComponent:0.63]];
    [self.picker_view applyFadeTop:YES bottom:NO andPercentage:@0.55];
    [self.date_picker applyFadeTop:YES bottom:NO andPercentage:@0.55];

    //[self.date_picker selectRow:[[linguaPos objectForKey:[[[Home getCurrentDay] linguaGiorno] sigla]] intValue] inComponent:0 animated:NO];
}

-(void)prepareMonths{
    self.monthData = [NSMutableArray new];
    [self.monthData addObject:@"qualunque mese"];
    for(int monthNumber = 1; monthNumber<13;monthNumber++){
    NSString * dateString = [NSString stringWithFormat: @"%d", monthNumber];
    
    NSDateFormatter* dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"MM"];
    NSDate* myDate = [dateFormatter dateFromString:dateString];
    
    [dateFormatter setDateFormat:@"MMMM"];
    [self.monthData addObject:[dateFormatter stringFromDate:myDate]];
    }
    
    //NSLog(@"self.monthData:%@",self.monthData);
}

-(void)prepareYear{
    self.yearData = [NSMutableArray new];
    [self.yearData addObject:@"qualunque anno"];
    [self.yearData addObject:@"2015"];
    [self.yearData addObject:@"2014"];
    [self.yearData addObject:@"2013"];
}

-(void)showPicker{
    //NSLog(@"showPicker");
    animation_block = ^{
        CGRect frame = self.picker_view.frame;
        frame.origin.y = self.view.frame.size.height-(self.picker_view.frame.size.height);
        frame.origin.y -= -10; // margin bottom negativo per bounce!!
        [self.picker_view setFrame:frame];
    };
    
    completition_block = ^(BOOL finished) {
        //fineShow!!
    };
    
        [UIView animateWithDuration:0.6
                              delay:0
             usingSpringWithDamping:0.65
              initialSpringVelocity:0.9
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:animation_block
                         completion:completition_block];
}


-(void)hidePicker{
    //NSLog(@"hidePicker");
    animation_block = ^{
        CGRect frame = self.picker_view.frame;
        frame.origin.y = self.view.frame.size.height;
        [self.picker_view setFrame:frame];
    };
    
    completition_block = ^(BOOL finished) {
        //fineShow!!
    };
    
    [UIView animateWithDuration:0.6
                          delay:0
         usingSpringWithDamping:0.65
          initialSpringVelocity:1
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:animation_block
                     completion:completition_block];
}



#pragma mark - PickerDelegate
//===========================
// Picker delegate
//===========================
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    //NSLog(@"numberOfRowsInComponent:%i",(int)component);
    if(component==0) return self.monthData.count;
    if(component==1) return self.yearData.count;
    return 0;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *label  = [UILabel new];
    
    NSString *label_text;
    if(component == 0){
        label_text = [self.monthData objectAtIndex:row];
        [label setFrame:CGRectMake(0, 0, [self pickerView:pickerView widthForComponent:component], 20)];
        label.font = [UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:18];
    }
    if(component == 1){
        label_text = [self.yearData objectAtIndex:row];
        [label setFrame:CGRectMake(0, 0, [self pickerView:pickerView widthForComponent:component], 20)];
        label.font = [UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:18];
    }
    [label setText:label_text];
    [label setTextAlignment:NSTextAlignmentCenter];
    if(row==0){
        label.numberOfLines = 0;
        label.adjustsFontSizeToFitWidth = YES;
    }
    [label setTextColor:[UIColor whiteColor]];
    return label;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    switch (component){
        case 0:
            return pickerView.frame.size.width/2.0;
        case 1:
            return pickerView.frame.size.width/2.0; //50.0f;
    }
    return 0;
}



#pragma mark PickerDID Select
//===========================
// PickerDID Select
//===========================
- (void)pickerView:(UIPickerView *)pV didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //NSLog(@"didSelectRow:%i",(int)row);
    [self searchForString];
}

#pragma mark RESETALL
//===================
// RESETALL
//===================
-(IBAction)azione_resetta:(UIButton*)sender{
    
    [self alert:NSLocalizedString(@"My Spiritual Book", nil)
         andMex:NSLocalizedString(@"delete_allNote_confirm", nil)
           icon:nil
       okAction:^(UIAlertAction*a){
           [self confirmReset];
       }
     withCancel:YES];
}

- (void)confirmReset{
    // NSLog(@"Conferma Reset");
    // STEP 1
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"Conferma Reset: step(1)");
        [Nota deleteAll];
    }];//fine step1
    
    // STEP 2
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"Conferma Reset: step(2)");
        [self fineResetta];
    }];//fine step2
    
    [step2 addDependency:step1];
    [[NSOperationQueue new] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
}

-(void)fineResetta{
    self.spiritualArray = [NSMutableArray arrayWithArray:[Nota getNoteInContext:[AppDelegate mainManagedObjectContext]]];
    listaFiltrata = [NSMutableArray arrayWithArray:self.spiritualArray];
    [self.tabella reloadData];
    [self checkNoItemLabel];
}

//=========================
// END
//=========================
@end
