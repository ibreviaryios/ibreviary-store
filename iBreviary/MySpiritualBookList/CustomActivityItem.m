//
//  CustomActivityItem.m
//  iBreviary
//
//  Created by Leonardo Parenti on 09/05/15.
//  Copyright (c) 2015 leyo. All rights reserved.
//

#import "CustomActivityItem.h"
@interface CustomActivityItem()

@property(nonatomic,strong) Nota *nota;

@end

@implementation CustomActivityItem


- (id)initWithNota:(Nota*)n{
    if (self = [super init]) {
        self.nota = n;
        }
    return self;
}


- (void)prepareWithActivityItems:(NSArray *)activityItems {
    for (NSObject *item in activityItems) {
        NSLog(@"prepareWithActivityItems:%@",item);
        
    }
}

- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return @"MySpiritualBook - powered by iBreviary";
}

- (NSString *)activityViewController:(UIActivityViewController *)activityViewController subjectForActivityType:(NSString *)activityType
{
    if ([activityType isEqualToString:UIActivityTypeMail])
    {
        return @"MySpiritualBook - powered by iBreviary";
    }
    
    return nil;
}

/*
-(UIImage *)activityViewController:(UIActivityViewController *)activityViewController thumbnailImageForActivityType:(NSString *)activityType suggestedSize:(CGSize)size{
    
}
*/

-(NSURL*)url{
    return [NSURL URLWithString:@"https://itunes.apple.com/it/app/ibreviary-ts-plus/id422601705?mt=8"];
}

-(NSString*)poweredText{
    return [self.nota.text stringByAppendingString:@" - powered by iBreviary"];
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    NSLog(@"canPerformAction:%@ withSender:%@",NSStringFromSelector(action),sender);
    return YES;
}

- (id)activityViewController:(UIActivityViewController *)activityViewController
         itemForActivityType:(NSString *)activityType
{
    NSLog(@"activityViewControllerForActivityType:%@",activityType);
    if ([activityType isEqualToString:UIActivityTypeMessage]) {
        //>>>>iMESSAGE
        //NSArray *items = @[self.nota.text,[self.nota getImageUrl]];
        return self.nota.text;
    }
    
    else if ([activityType isEqualToString:UIActivityTypePostToFacebook]) {
        //>>>>FACEBOOK
        NSDictionary *d;
        CGFloat version = [[UIDevice currentDevice].systemVersion floatValue];
        if(version<9.f){
            d = [[NSDictionary alloc] initWithObjects:@[self.url] forKeys:@[@"url"]];
        }
        else{
            d = [[NSDictionary alloc] initWithObjects:@[self.nota.text] forKeys:@[@"text"]];
        }
        return d;
    }
    else if ([activityType isEqualToString:UIActivityTypePostToTwitter]) {
        //>>>>TWITTER
        return @"#iBreviary";
    }
    else if ([activityType isEqualToString:UIActivityTypeMail]) {
        //>>>>MAIL
        NSDictionary *d = [[NSDictionary alloc] initWithObjects:@[self.nota.text] forKeys:@[@"body"]];

        return d;
    }
    else if ([activityType isEqualToString:UIActivityTypeCopyToPasteboard]) {
        //>>>>COPY
        return self.nota.text;
    }
    
    
//    else if ([activityType isEqualToString:UIActivityTypeSaveToCameraRoll]) {
//        //>>>>SAVE TO CAMERA
//        return @[self.nota.getImageUrl];
//    }
    
    
    return [NSString stringWithFormat:@"%@ non intercettato",activityType];
}

-(void)didFinishActivityAction:(BOOL)completed
                      withType:(NSString *)activityType
                   returnItems:(NSArray*)returnedItems
                           err:(NSError *)activityError{
    NSLog(@"Activity Type selected: %@", activityType);
    if (completed) {
        NSLog(@"didFinishActivityAction:%@ withType:%@ returnItems:%@ err:%@",completed?@"YES":@"NO",activityType, returnedItems, activityError);
        [self.nota removeImageWithError:nil];
    } else {
        if (activityType == NULL) {
            NSLog(@"User dismissed the view controller without making a selection.");
        } else {
            NSLog(@"Activity was not performed.");
        }
    }
}

@end

