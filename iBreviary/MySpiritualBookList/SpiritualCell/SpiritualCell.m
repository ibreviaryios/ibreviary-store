//
//  SpiritualCell.m
//  Connect
//
//  Created by Leonardo Parenti on 20/03/15.
//  Copyright (c) 2015 Piksel. All rights reserved.
//

#import "SpiritualCell.h"
#import "Nota.h"
#import "MainWebView.h"

//====================
// BLOCK define
//====================
void (^animation_block)(void);
void (^completition_block)(BOOL);

@interface SpiritualCell ()

@property (strong, nonatomic) IBOutlet UIImageView *quote;
@property (strong, nonatomic) IBOutlet UILabel *text_label;
@property (strong, nonatomic) IBOutlet UILabel *date_label;
@property (strong, nonatomic) IBOutlet UIButton *mod_show_btn;
@property (strong, nonatomic) IBOutlet UIButton *delete_btn;
@property (strong, nonatomic) IBOutlet UIButton *source_btn;
@property (strong, nonatomic) IBOutlet UIButton *menu_btn;
@property (strong, nonatomic) IBOutlet UIView *menu_view;

@property (strong, nonatomic) IBOutlet MainWebView *NotaDetail;

@property (strong, nonatomic) id<SpiritualCellDelegate> delegate;

@property (strong, nonatomic) NSIndexPath *indexPathPosition;
@property (strong, nonatomic) Nota *nota;



//-(void)calculateCellQuotePositionForText:(NSString*)text;
-(void)calculateCellQuotePositionForText:(NSString*)text;

@end

@implementation SpiritualCell

- (void)awakeFromNib {
    // Initialization code
    [self inizializeView];
    //QUOTE
    
//    UIEdgeInsets edgeInsets = self.isIpad?UIEdgeInsetsMake(72, 100, 72, 100):UIEdgeInsetsMake(31, 43, 31, 43);
//    [self.quote setImage:[[UIImage imageNamed: self.isIpad?@"QuoteBackgroundADD.png":@"QuoteBackgroundADD_iphone.png"] resizableImageWithCapInsets:edgeInsets  resizingMode:UIImageResizingModeStretch]];

    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(31, 43, 31, 43);
    [self.quote setImage:[[UIImage imageNamed:@"QuoteBackgroundADD_iphone.png"] resizableImageWithCapInsets:edgeInsets  resizingMode:UIImageResizingModeStretch]];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse{
    // _shadow_view;
    // _cornerRadius_view

    [self inizializeView];
}

-(void)inizializeView{
    
    [_quote setTranslatesAutoresizingMaskIntoConstraints:YES];
    [_text_label setTranslatesAutoresizingMaskIntoConstraints:YES];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    [self.contentView setBackgroundColor:[UIColor clearColor]];
    [self setBackgroundColor:[UIColor clearColor]];
    _text_label.text = @"";
    _date_label.text = @"";
    _text_label.attributedText = [[NSAttributedString alloc] initWithString:@""];
    _date_label.attributedText = [[NSAttributedString alloc] initWithString:@""];
    _indexPathPosition = nil;
    [self.text_label setFrame:[self textFrame]];
    [self.quote setFrame:[self textFrame]];
    self.delegate = nil;
    /*
    [self.share_btn setImage:[UIImage imageNamed:@"minimal_share.png"] withTintColor:[UIColor colorFromHexString:@"#6c5b43"]];
    [self.mod_show_btn setImage:[UIImage imageNamed:@"minimal_mod.png"] withTintColor:[UIColor colorFromHexString:@"#6c5b43"]];
    [self.delete_btn setImage:[UIImage imageNamed:@"minimal_trash.png"] withTintColor:[UIColor colorFromHexString:@"#6c5b43"]];
     // */
    self.menu_btn.transform = CGAffineTransformIdentity;
    [self hideMenu:NO];
    self.hidden = NO;
}



-(BOOL)isIpad{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}

-(void)setAttributeDate:(NSAttributedString*)attributeDate
       andAttributeText:(NSAttributedString*)attributeText
                forNota:(Nota*)n
          withIndexPath:(NSIndexPath*)i
               delegate:(id<SpiritualCellDelegate>)d{
    self.delegate = d;
    [self setIndexPathPosition:i];
    [self setNota:n];
    [self calculateCellQuotePositionForText:n.text];
    [self.date_label setText:@""];
    [self.date_label setAttributedText:attributeDate];
    [self.text_label setText:@""];
    [self.text_label setAttributedText:attributeText];
    [self checkSource];
}

-(void)checkSource{
    
}

#define marginText 15.f

#pragma mark -
#pragma mark btnAnimation
//=================================
// btnAnimation
//=================================
-(IBAction)toggleMenu:(UIButton*)sender{
    BOOL shoudShowMenu = self.menu_view.frame.size.width == self.menu_view.frame.size.height;
    if(shoudShowMenu) [self showMenu];
    else [self hideMenu:YES];
}

-(CGFloat)menuOrigin{
    CGFloat origin =  IS_IPAD?252.0:94.0;
    CGFloat menu = self.menu_view.frame.size.height;
    if(IS_IPHONE_6 || IS_IPHONE_6_PLUS) origin = self.frame.size.width - (211.f + marginText);
    NSLog(@"origin:%f",origin);
    return origin;
}

-(CGFloat)menuWidth{
    return IS_IPAD?219.0:219.0;
}

-(CGRect)textFrame{
    CGFloat fixedWidth = IS_IPAD?438:290;
    CGFloat origin = marginText;
    if(IS_IPHONE_6 || IS_IPHONE_6_PLUS) fixedWidth = self.frame.size.width - (origin*2);
    CGRect tf = CGRectMake(origin, 37, fixedWidth, 160);
    return tf;
}

-(void)showMenu{
    // NSLog(@"showMenu");
    animation_block = ^{
        CGRect frame = self.menu_view.frame;
        frame.origin.x = [self menuOrigin];
        frame.size.width = [self menuWidth];
        if(![self.nota hasSourceText]){
            //NOTA SENZA CITAZIONE SOURCE SENDER
            frame.size.width -= 42.0;
            frame.origin.x += 42.0;
            [self.source_btn setHidden:YES];
        }
        else [self.source_btn setHidden:NO];
        [self.menu_view setFrame:frame];
        CGFloat radians = atan2f(self.menu_btn.transform.b, self.menu_btn.transform.a);
        CGFloat degrees = radians * (180 / M_PI);
        CGAffineTransform transform = CGAffineTransformMakeRotation((180 + degrees) * M_PI/180);
        self.menu_btn.transform = transform;
        };
    
    completition_block = ^(BOOL finished) {
        //fineShow!!
    };
    [UIView animateWithDuration:0.6
                          delay:0
         usingSpringWithDamping:0.65
          initialSpringVelocity:0.9
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:animation_block
                     completion:completition_block];
}

-(void)hideMenu:(BOOL)animated{
    // NSLog(@"hideMenu");
    animation_block = ^{
        CGRect frame = self.menu_view.frame;
        frame.origin.x = [self menuOrigin]+[self menuWidth]-self.menu_view.frame.size.height;
        frame.size.width = self.menu_view.frame.size.height;
        [self.menu_view setFrame:frame];
        if(animated){
            CGFloat radians = atan2f(self.menu_btn.transform.b, self.menu_btn.transform.a);
            CGFloat degrees = radians * (180 / M_PI);
            CGAffineTransform transform = CGAffineTransformMakeRotation((180 + degrees) * M_PI/180);
            self.menu_btn.transform = transform;
        }
    };
    
    completition_block = ^(BOOL finished) {
        //fineHide Menu!!
        self.menu_btn.transform = CGAffineTransformIdentity;
    };
    
    
    if(animated){
    [UIView animateWithDuration:0.6
                          delay:0
         usingSpringWithDamping:1
          initialSpringVelocity:0.9
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:animation_block
                     completion:completition_block];
    }
    else{
        animation_block();
        completition_block(YES);
    }

}

#pragma mark -
#pragma mark ACTION
//=================================
// ACTION
//=================================
-(IBAction)showSourceNota_action:(UIButton*)sender{
    if([self.delegate respondsToSelector:@selector(showSourceNota:)])
        [self.delegate showSourceNota:self.nota];
}
-(IBAction)deleteNota_action:(UIButton*)sender{
    if([self.delegate respondsToSelector:@selector(deleteNota:)])
        [self.delegate deleteNota:self.nota];
}

-(IBAction)modNota_action:(UIButton*)sender{
    if([self.delegate respondsToSelector:@selector(modNota:)])
        [self.delegate modNota:self.nota];
}

-(IBAction)shareNota_action:(UIButton*)sender{
    if([self.delegate respondsToSelector:@selector(shareNota:sender:)])
        [self.delegate shareNota:self.nota sender:self];
}



#pragma mark -
#pragma mark calculateCellQuotePosition
//=================================
// calculateQUOTEPosition
//=================================
-(void)calculateCellQuotePositionForText:(NSString*)text{
    [self.text_label setText:text];
    CGFloat fixedWidth = [self textFrame].size.width;
    CGRect b  = [text boundingRectWithSize:CGSizeMake(fixedWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:20]} context:nil];
    
    CGSize newSize = b.size;
    CGFloat minH = (31)*2;
    CGFloat maxH = 160;
    CGFloat minW = 43*2;
    if(newSize.height<minH) newSize.height=minH;
    if(newSize.height>maxH) newSize.height=maxH;
    if(newSize.width<minW) newSize.width=minW;
    
    newSize.height +=10;
    newSize.width +=25;
    
    [self.quote setFrame:CGRectMake(0, 0, newSize.width,newSize.height)];
    [self.quote setCenter:self.text_label.center];
    [self.quote setHidden:(!self.nota.hasSourceText)];
}


- (void)layoutSubviews
{
    [super layoutSubviews];
}


@end
