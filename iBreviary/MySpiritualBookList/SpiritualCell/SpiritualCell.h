//
//  SpiritualCell.h
//  Connect
//
//  Created by Leonardo Parenti on 20/03/15.
//  Copyright (c) 2015 Piksel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Nota,SpiritualCell;

@protocol SpiritualCellDelegate <NSObject>
-(void)showSourceNota:(Nota*)nota;
-(void)deleteNota:(Nota*)nota;
-(void)modNota:(Nota*)nota;
-(void)shareNota:(Nota*)nota sender:(SpiritualCell*)sender;
@end

@interface SpiritualCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *share_btn;


//-(void)calculateCellQuotePositionForText:(NSString*)text;
-(void)setAttributeDate:(NSAttributedString*)attributeDate
       andAttributeText:(NSAttributedString*)attributeText
                forNota:(Nota*)n
          withIndexPath:(NSIndexPath*)i
               delegate:(id<SpiritualCellDelegate>)d;

@end
