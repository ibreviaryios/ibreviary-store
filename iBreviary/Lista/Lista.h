//
//  Lista.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "CPPickerView.h"
#import "Lingua.h"
#import "Tutorial.h"
#import "FXBlurView.h"
#import "AdvancedSettings.h"


@protocol ListaDelegate <NSObject>
-(void)caricaGiornoWithIndex:(NSIndexPath*)i;
@end


@interface Lista : BaseController<CPPickerViewDataSource, CPPickerViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,LinguaDelegate,AdvancedSettingsDelegate,TutorialDelegate>{
}

//DELEGATE
@property (nonatomic, strong) BaseController<ListaDelegate> *delegate;

//SFONDO
@property (strong, nonatomic) IBOutlet UIImageView *logoVelina_lingua;

//STRUCTURE
@property (nonatomic, strong) NSMutableArray *list_day;
@property (nonatomic, strong) NSMutableArray *list_down;
@property (nonatomic, strong) NSMutableArray *list_lingua_img;
@property (nonatomic, strong) NSMutableDictionary *linguaPos;
@property (strong, nonatomic) IBOutlet UIImageView *cornice;

//CLOSE
@property (strong, nonatomic) IBOutlet UIButton *close;

//ROTELLA
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *rotella;

//TABELLA - GIORNI DISPONIBILI
@property (strong, nonatomic) IBOutlet UILabel *title_tabella;
@property (strong, nonatomic) IBOutlet UITableView *tabella;

//DOWNLOAD
@property (strong, nonatomic) IBOutlet UILabel *title_download;
@property (strong, nonatomic) IBOutlet UIButton *button_download;
@property (strong, nonatomic) IBOutlet UIButton *button_settimana;
@property (strong, nonatomic) CPPickerView *picker_ctrl_day;
@property (strong, nonatomic) UIPickerView *picker_ctrl_lingua;
@property (strong, nonatomic) IBOutlet UIView *backPicker;
@property (strong, nonatomic) IBOutlet UIView *day_ctrl_view;
@property (strong, nonatomic) IBOutlet UIButton *up_btn;
@property (strong, nonatomic) IBOutlet UIButton *down_btn;
@property (strong, nonatomic) IBOutlet UIButton *sx_btn;
@property (strong, nonatomic) IBOutlet UIButton *dx_btn;
@property (strong, nonatomic) IBOutlet UILabel *week_label_download;
@property (strong, nonatomic) IBOutlet UIView *backPicker_lingua;

@property (strong, nonatomic) IBOutlet UIButton *reset_btn;
@property (strong, nonatomic) IBOutlet UILabel *reset_label;

//->LINGUA
@property (strong, nonatomic) FXBlurView *blurView;
@property (strong, nonatomic) Lingua *linguaCTRL;
@property (strong, nonatomic) IBOutlet UIImageView *info_img;
@property (strong, nonatomic) IBOutlet UIButton *btn_lingua;

//ADVANCED SETTINGS
@property (strong, nonatomic) AdvancedSettings *advancedSettingsCTRL;
@property (strong, nonatomic) IBOutlet UIImageView *advancedSettings_img;
@property (strong, nonatomic) IBOutlet UIButton *advancedSettings_btn;


//TUTORIAL
@property (strong, nonatomic) Tutorial *tutorialCTRL;
@property (strong, nonatomic) IBOutlet UIImageView *tutorial_img;
@property (strong, nonatomic) IBOutlet UIButton *tutorial_btn;

//INIT
- (id)initWithDelegate:(BaseController<ListaDelegate> *)d;

@end
