//
//  Lista.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "Lista.h"
#import "SystemData.h"
#import "Home.h"
#import "LinguaObject.h"
#import "TestoProprio.h"
#import "Messa.h"

#define EVENT_PATH @"http://www.ibreviary.com/get_event_days.php"


@interface Lista ()<BNHtmlPdfKitDelegate, UIDocumentInteractionControllerDelegate>

@property (nonatomic, assign) CGPoint scrollVelocity;

@property (nonatomic, strong) IBOutlet UIScrollView *scroller_delegate;
@property (nonatomic, strong) IBOutlet UILabel *messa_label;
@property (strong, nonatomic) IBOutlet UITableView *tabellaMessa;
@property (strong, nonatomic) IBOutlet UIView *area_tabelle;

//FOR MESSA on IPHONE
@property (strong, nonatomic) IBOutlet UIPageControl *pager;
@property (strong, nonatomic) NSArray *messe_data;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *rotellaShare;
@property (strong, nonatomic) BNHtmlPdfKit *htmlPdfKit;
@property (strong) UIDocumentInteractionController *documentInteractionCtrl;

@end


@implementation Lista

//DELEGATE
@synthesize delegate;
//STRUCTURE
@synthesize list_day, list_down, list_lingua_img, linguaPos, cornice;
//CLOSE
@synthesize close;
//ROTELLA
@synthesize rotella;
//TABELLA - GIORNI DISPONIBILI
@synthesize title_tabella, tabella;
//DOWNLOAD
@synthesize title_download,button_download, button_settimana, week_label_download;
@synthesize picker_ctrl_day,picker_ctrl_lingua, backPicker,backPicker_lingua;
@synthesize logoVelina_lingua;
@synthesize day_ctrl_view,sx_btn,dx_btn;
@synthesize up_btn,down_btn;
@synthesize reset_btn,reset_label;
//->LINGUA
@synthesize linguaCTRL,btn_lingua,blurView,info_img;
//->ADVANCED SETTINGS
@synthesize advancedSettingsCTRL,advancedSettings_img,advancedSettings_btn;
//->TUTORIAL
@synthesize tutorialCTRL,tutorial_img,tutorial_btn;

//COLORI
UIColor *selectedCell;
UIColor *notSelectedCell;
UIColor *defaulf_color,*default_disabilita,*default_mese,*pickerColor,*reset_color;
UIColor *hightlight_color;

NSMutableDictionary *colorWeek;

NSArray *temp_color;


int processoLista = -1;

int cellaSelezionata = -1;


#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithDelegate:(BaseController<ListaDelegate> *)d
{
    //iPhone4
    NSString *nibNameOrNil = @"Lista";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"Lista_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"Lista5";
    if(IS_IPHONE_6) nibNameOrNil = @"Lista6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"Lista6Plus";
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        list_day = [NSMutableArray new];
    }
    return self;
}


#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad
{
    [super viewDidLoad];
    // NSLog(@"Lista DIDload");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aggiornaListaInDownloadStep) name:AGGIORNA_LISTA_DOWNLOAD object:nil];
    
    [self buildMessaLista];

    [self initView];
    [self localizza];
    
}

// Add this method
- (BOOL)prefersStatusBarHidden {
    return !self.isIpad;
}

BOOL firstAppear;

- (void)initView{
    // NSLog(@"Lista initView");
    firstAppear = YES;
    [self.tabella setHidden:YES];
    if(!self.isIpad){
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    //COLORI
    selectedCell = [UIColor colorWithRed:171/255.f green:0 blue:0 alpha:0.9];
    notSelectedCell= [UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:150/255.f];
    defaulf_color = [UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:150/255.f];
    default_mese = [UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:255/255.f];
    hightlight_color = [UIColor colorWithRed:171/255.f green:0 blue:0 alpha:0.9];
    default_disabilita = [UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:100/255.f];
    pickerColor = [UIColor colorWithRed:122/255.f green:97/255.f blue:79/255.f alpha:1];
    reset_color = [UIColor colorWithRed:122/255.f green:97/255.f blue:79/255.f alpha:1];
    colorWeek = [[NSMutableDictionary alloc] init];
    [colorWeek setObject:[UIColor colorWithRed:190/255.f green:14/255.f blue:3/255.f alpha:0.85] forKey:@"7"];  //SABATO
    [colorWeek setObject:[UIColor colorWithRed:227/255.f green:23/255.f blue:10/255.f alpha:0.80] forKey:@"1"];   //DOMENICA
    [colorWeek setObject:[UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79] forKey:@"2"]; //LUNEDI'
    [colorWeek setObject:[UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79] forKey:@"3"]; //MARTEDI'
    [colorWeek setObject:[UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79] forKey:@"4"]; //MERCOLEDI'
    [colorWeek setObject:[UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79] forKey:@"5"]; //GIOVEDI'
    [colorWeek setObject:[UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79] forKey:@"6"]; //VENERDI'
 
    //COLOR
    [week_label_download setTextColor:pickerColor];
    
    tabella.backgroundView.hidden = YES;
    [tabella setBackgroundColor:[UIColor clearColor]];
    
    CGRect f = tabella.frame;
    f.size.width = self.view.frame.size.width;
    [tabella setFrame:f];
    
    
    //PICKER LINGUA
    list_lingua_img = [LinguaObject getLingue];
    linguaPos = [[NSMutableDictionary alloc] init];
    for(int j=0; j< [list_lingua_img count];j++){
        LinguaObject *l = [[LinguaObject getLingue] objectAtIndex:j];
        [linguaPos setObject:[NSNumber numberWithInt:j] forKey:l.sigla];
    }
    // NSLog(@"list_lingua_img %@",list_lingua_img);
}

-(void)localizza{
    //LOCALIZZAZIONE:
    [week_label_download setText:[NSLocalizedString(@"Lista_Scarica_settimana", nil) uppercaseString]];
    [title_tabella setText:[NSLocalizedString(@"Lista_Giorni_disponibili", nil) uppercaseString]];
    [self.messa_label  setText:[NSLocalizedString(@"ListaMesse", nil) uppercaseString]];
    [title_download setText:[@"Download" uppercaseString]];
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
}


-(void)configureView{
    // NSLog(@"Lista Will configureView");
    
    [self initListPicker];
    
    [picker_ctrl_day selectItemAtIndex:1 animated:NO];
    
    
	UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
	v.backgroundColor = [UIColor clearColor];
	[self.tabella setTableFooterView:v];
	[self.tabella setTableHeaderView:v];
    [self AttivaRotella];
    
}

-(void)buildMessaLista{
    if(self.isIpad) return;
    self.messe_data = [Messa getMesseIn:[AppDelegate mainManagedObjectContext]];
    BOOL shoudShowMessa = (self.messe_data.count>0);
    
    [self.pager setHidden:!shoudShowMessa];
    [self.scroller_delegate setScrollEnabled:shoudShowMessa];
    [self.pager setNumberOfPages:2];
    self.scroller_delegate.contentSize = CGSizeMake(self.scroller_delegate.frame.size.width*2.f, self.scroller_delegate.frame.size.height);
}


- (IBAction)changePage:(UIPageControl*)sender
{
    // NSLog(@"changePage:%li",(long)self.pager.currentPage);
    // update the scroll view to the appropriate page
    CGRect frame;
    frame.origin.x = self.scroller_delegate.frame.size.width * self.pager.currentPage;
    frame.origin.y = 0;
    frame.size = self.scroller_delegate.frame.size;

    
    [self.scroller_delegate scrollRectToVisible:frame animated:YES];
    [UIView animateWithDuration:0.4
                     animations:^{
                         CGRect frame_list = self.area_tabelle.frame;
                         frame_list.origin.x = self.pager.currentPage==0?0:-self.view.frame.size.width;
                         [self.area_tabelle setFrame:frame_list];
                     }
                     completion:^(BOOL finished) {
                         ;
                     }];
}

//=========================
// Appear
//=========================
#pragma mark - Appear
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureView];
}


- (void)viewDidAppear:(BOOL)animated {
    // NSLog(@"Lista DIDAppear");
    [super viewDidAppear:animated];
    if(![self.defaults boolForKey:TUTORIAL_LISTA_VISTO]){
        [self.defaults setBool:YES forKey:TUTORIAL_LISTA_VISTO];
        [self.defaults synchronize];
        [self showTutorial:nil];
    }
    [self aggiornaListaInDownload];
    
}

//=========================
// Disappear
//=========================
#pragma mark - Disappear
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

//=========================
// LINGUA DELEGATE
//=========================
#pragma mark - LINGUA DELEGATE
-(IBAction)showLingua:(UIButton*)sender
{
    // NSLog(@"showLingua");
	linguaCTRL = [[Lingua alloc] initWithDelegate:self andIsLista:YES];
	linguaCTRL.view.alpha = 0;
    
    blurView = [[FXBlurView alloc] initWithFrame:self.view.frame];
    [self.blurView setDynamic:NO];
    [self.blurView setTintColor:[UIColor brownColor]];
    self.blurView.alpha = 0;
    
    [self.view addSubview:self.blurView];
    
	[self.view addSubview:linguaCTRL.view];
	
	[UIView beginAnimations:@"AppareLingua" context:NULL];
    [UIView setAnimationDuration:0.5];
	linguaCTRL.view.alpha = 0.95;
    self.blurView.alpha = 1;
    [UIView commitAnimations];
}

-(void)removeLingua{
    // NSLog(@"FineInfo");
    linguaCTRL.view.alpha = 1;
    [self rotateImage:info_img withDuration:0.6 onY_axis:YES];
    [UIView beginAnimations:@"Lingua_disappear" context:NULL];
    [UIView setAnimationDuration:0.4];
    linguaCTRL.view.alpha = 0;
    self.blurView.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineLingua)];
    [UIView commitAnimations];
}

-(void)fineLingua{
    [self.blurView removeFromSuperview];
    [linguaCTRL.view removeFromSuperview];
    self.blurView =  nil;
    linguaCTRL =  nil;

}

#pragma mark -
#pragma mark ROTELLA
//=========================
// ROTELLA
//=========================
-(void)AttivaRotella{
    if([NSThread isMainThread]) [self AttivaRotellaMain];
    else [self performSelectorOnMainThread:@selector(AttivaRotellaMain) withObject:nil waitUntilDone:NO];
}

-(void)AttivaRotellaMain{
    rotella.transform = CGAffineTransformMakeScale(1.2f, 1.2f);
    [rotella startAnimating];
    [rotella setHidden:NO];
    [self activityNetwork:YES];
}

-(void)rimuoviRotella{
    if(processoLista>0) return;
    if([NSThread isMainThread]) [self rimuoviRotellaMM];
    else [self performSelectorOnMainThread:@selector(rimuoviRotellaMM) withObject:nil waitUntilDone:NO];
}
-(void)rimuoviRotellaMM{
    // NSLog(@"rimuoviRotellaMM");
    [rotella setHidden:YES];
    [rotella stopAnimating];
    [self activityNetwork:NO];
}

#pragma mark -
#pragma mark AZIONI
//=========================
// AZIONI
//=========================
- (IBAction)azioneCTRL_Day:(UIButton*)sender
{
    int pos = [picker_ctrl_day rigaSelezionata];
    // NSLog(@"azioneCTRL Day pos:%i",pos);
    if (sender == dx_btn) {
        //CASO DX
        if (pos<(list_down.count - 1)) {
            pos +=1;
            [picker_ctrl_day selectItemAtIndex:pos animated:YES];
        }
        
        [sx_btn setAlpha:1];
        [dx_btn setAlpha:(pos==(list_down.count - 1))?0.3:1];
        // NSLog(@"DX selected:%i",pos);
        return;
    }
    //CASO SX
    if (pos!=0) {
        pos-=1;
        [picker_ctrl_day selectItemAtIndex:pos animated:YES];
    }
    // NSLog(@"SX selected:%i",pos);
    [sx_btn setAlpha:(pos==0)?0.3:1];
    [dx_btn setAlpha:1];

}

- (IBAction)azioneCTRL_Lang:(UIButton*)sender
{
    NSInteger pos = [picker_ctrl_lingua selectedRowInComponent:0];
    // NSLog(@"azioneCTRL Day pos:%i",pos);
    if (sender == down_btn) {
        //CASO UP
        if (pos<(list_down.count - 1)) {
            [picker_ctrl_lingua selectRow:pos+1 inComponent:0 animated:YES];
        }
        [down_btn setAlpha:([picker_ctrl_lingua selectedRowInComponent:0]==[list_lingua_img count]-1)?0.3:1];
        [up_btn setAlpha:([picker_ctrl_lingua selectedRowInComponent:0]==0)?0.3:1];
        return;
    }
    //CASO DOWN
    if (pos!=0) {
        [picker_ctrl_lingua selectRow:pos-1 inComponent:0 animated:YES];
        [down_btn setAlpha:([picker_ctrl_lingua selectedRowInComponent:0]==[list_lingua_img count]-1)?0.3:1];
        [up_btn setAlpha:([picker_ctrl_lingua selectedRowInComponent:0]==0)?0.3:1];
    }
}

- (IBAction)azioneDOWNLOAD_DAY:(UIButton*)sender
{
    // NSLog(@"azioneDOWNLOAD_DAY:%i",self.connesso);
    if(!self.connesso) { [self noNet]; return; }
    [sender setSelected:YES];
    
    processoLista = 1;
    [self AttivaRotella];
    [button_download setSelected:YES];
    
    [self scaricaDay];

}

- (IBAction)azioneDOWNLOAD_WEEK:(UIButton*)sender
{
    // NSLog(@"azioneDOWNLOAD_WEEK:%i",self.connesso);
    if(!self.connesso) { [self noNet]; return; }
    [sender setSelected:YES];
    processoLista = 1;
    [self AttivaRotella];
    [button_settimana setSelected:YES];
    [self aggiornaListaInDownload];
    [[NSOperationQueue new] addOperationWithBlock:^{
        [self scaricaSettimana];
    }];
}

- (IBAction)azioneDismiss:(UIButton*)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                        // NSLog(@"azioneDismiss completata");
                                        [Home dismiss:self];
                                        }];
}


-(IBAction)downSelettore:(UIButton*)sender{
    [reset_label setTextColor:hightlight_color];
  }

-(IBAction)outSelettore:(UIButton*)sender{
    [reset_label setTextColor:reset_color];
}

#pragma mark RESET
//=========================
// RESET
//=========================
-(IBAction)azione_resetta:(UIButton*)sender{
//    NSString *path = [[NSString alloc] initWithString:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"erro@2x.png"]];
//    UIImage *bkgImg = [[UIImage alloc] initWithContentsOfFile:path];
    
    [self alert:@"RESET"
         andMex:NSLocalizedString(@"reset_day_confirm", nil)
           icon:[UIImage imageNamed:@"erro@2x.png"]
       okAction:^(UIAlertAction *action) {
           [self resettaLista];
       }
     withCancel:YES];

    
    /*
    UIAlertView *resetAlert = [[UIAlertView alloc] initWithTitle: message:delegate:self cancelButtonTitle:[NSLocalizedString(@"risposta_annulla", nil) uppercaseString] otherButtonTitles:[NSLocalizedString(@"risposta_si", nil) uppercaseString] ,nil];
    
    if(self.isIos7orUpper)
    {
        UIView *mask = [[UIView alloc] initWithFrame:CGRectMake(-120, -100, 240, 230)];
        [mask setBackgroundColor:[UIColor clearColor]];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(72, 25, 17, 17)];
        [imageView setAlpha:0.8];
         [imageView setImage:bkgImg];
    
    
        [mask addSubview:imageView];
        UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        [view2 setBackgroundColor:[UIColor clearColor]];
        [view2 addSubview:mask];
        [resetAlert setValue:view2 forKey:@"accessoryView"];
    }

    [resetAlert setTag:1];
    [resetAlert show];
     */
}

-(void)resettaLista{
    [self AttivaRotella];
    processoLista = 1;
    [reset_label setTextColor:hightlight_color];
    
    NSArray *contents = [[NSArray alloc] initWithArray:[Day getListDays]];
    for (NSString *filename in contents) {
        NSError *errore = nil;
        [self.fileManager removeItemAtPath:[self.cacheDir stringByAppendingFormat:@"/%@/%@",PATH_DAY,filename] error:&errore];
        if(errore){
            // NSLog(@"ResetError:%@",[errore description]);
            }
        }
    NSArray *contentsIMG = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[self.cacheDir stringByAppendingPathComponent:PATH_IMG] error:nil];
    for (NSString *imagename in contentsIMG) {
        NSError *errore = nil;
        [self.fileManager removeItemAtPath:[self.cacheDir stringByAppendingFormat:@"/%@/%@",PATH_IMG,imagename] error:&errore];
        if(errore){
            // NSLog(@"Reset_IMG_Error:%@",[errore description]);
        }
    }
    
    [self fineResetta];
}

-(void)fineResetta{
    [rotella setHidden:YES];
    [reset_label setTextColor:reset_color];
    processoLista = 0;
    [self rimuoviRotella];
    list_day = [NSMutableArray arrayWithArray:[Day getListDays]];
    // NSLog(@"fineResetta:%@",list_day);
    [self aggiornaListaInDownload];
}

#pragma mark DOWNLOAD DAY
//=========================
// DOWNLOAD DAY
//=========================
-(void)scaricaDay{
    if(!self.connesso) { [self noNet]; return; }
    [self.view setUserInteractionEnabled:NO];
    LinguaObject *lingua = [[LinguaObject getLingue] objectAtIndex:[picker_ctrl_lingua selectedRowInComponent:0]];
    [Day scaricaGiornoconData:[list_down objectAtIndex:[picker_ctrl_day rigaSelezionata]]
                       withLang:lingua.sigla
                      eventFlag:[TestoProprio getTestoProprioWithKey:[self.defaults objectForKey:EVENT_FLAG]]
               completionBlock:^(Day *ddd){
                                NSString *risposta = ddd.filename;
                                [self.view setUserInteractionEnabled:YES];
                                [[NSOperationQueue new] addOperationWithBlock:^{ [self fineScarica:risposta];}];
                                return ddd;
                                }];
    //*/
// remove ** for debug date_day fixed 2014-02-10 lang=it
    /* 
    LinguaObject *lingua = [LinguaObject getLinguaBySigla:@"it"];
    NSString *risposta = [[Day scaricaGiornoconData:@"2014-02-10" withLang:lingua.sigla eventFlag:[TestoProprio getTestoProprioWithKey:[self.defaults objectForKey:EVENT_FLAG]]] filename];
    //*/
}

-(void)fineScarica:(NSString*)risposta{
    // NSLog(@"fineScarica:%@",risposta);
    
    NSDictionary *d = [self parseDaySaved:risposta];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *data_notifica = [dateFormatter dateFromString:[d objectForKey:@"date_day"]];
    [dateFormatter setDateFormat:@"d/M/yy"];
    
    if([[d objectForKey:@"complete"] isEqualToNumber:[NSNumber numberWithBool:NO]])[self mostraNotificaDayERROR_INCOMPLETE:[dateFormatter stringFromDate:data_notifica]];
    else
        //OK!
        [self mostraNotificaDayDownloaded:[dateFormatter stringFromDate:data_notifica]];
    [self performSelectorOnMainThread:@selector(aggiornaBottoneDownload) withObject:nil waitUntilDone:NO];
    list_day = [NSMutableArray arrayWithArray:[Day getListDays]];
    while(list_day.count == 0)
    {
        list_day = [NSMutableArray arrayWithArray:[Day getListDays]];
        // NSLog(@"ERORRE-WEEK:%@",list_day);
    }
    // NSLog(@"!Lista AGGIORNATA!:%@",list_day);
    list_day = [NSMutableArray arrayWithArray:[Day getListDays]];
    processoLista = 0;
    if(list_day.count > 0) [self aggiornaListaInDownload];
    [button_download setSelected:NO];
    [self rimuoviRotella];
    
}

-(void)aggiornaBottoneDownload{
    [self.view setUserInteractionEnabled:YES];
    [button_download setSelected:NO];
    // NSLog(@"bottone Download aggiornato");
}

#pragma mark DOWNLOAD WEEK
//=========================
// DOWNLOAD SETTIMANA
//=========================
-(void)scaricaSettimana{
    if(!self.connesso) { [self noNet]; return; }
    [self.view setUserInteractionEnabled:NO];
    processoLista = 1;
    [self AttivaRotella];
    LinguaObject *lingua = [[LinguaObject getLingue] objectAtIndex:[picker_ctrl_lingua selectedRowInComponent:0]];
    [Day scaricaSettimana:lingua.sigla eventFlag:[TestoProprio getTestoProprioWithKey:[self.defaults objectForKey:EVENT_FLAG]] completionBlock:^(NSString* risposta){
        // NSLog(@"Completed:%@",risposta);
        processoLista = 0;
        [tabella setAlpha:1];
        [self fineScaricaSett:risposta];
    }];
}

-(void)fineScaricaSett:(NSString*)risposta{
    // NSLog(@"fineScaricaSett:%@",risposta);
    [self performSelectorOnMainThread:@selector(aggiornaBottoneWeekDownload) withObject:nil waitUntilDone:NO];
    // NSLog(@"button_settimana[%i]:%@",button_settimana.selected,button_settimana);
    // NSLog(@"risposta:%@",risposta);
    if([risposta uguale:@"1111111"])
        [self mostraNotificaWeekDownloaded];
    else
        [self mostraNotificaWeekERROR_INCOMPLETE:risposta];
    processoLista = 0;
    list_day = [NSMutableArray arrayWithArray:[Day getListDays]];
    [self aggiornaListaInDownload];
}

-(void)aggiornaBottoneWeekDownload{
    [self.view setUserInteractionEnabled:YES];
    [button_settimana setSelected:NO];
    //[button_settimana setImage:[UIImage imageNamed:@"download2014.png"] forState:UIControlStateNormal];
    //[button_settimana setImage:[UIImage imageNamed:@"download2014H.png"] forState:UIControlStateSelected];
    // NSLog(@"bottone Week aggiornato");
}

-(void)aggiornaListaInDownloadStep{
    // NSLog(@"aggiornaListaInDownloadStep");
    list_day = [NSMutableArray arrayWithArray:[Day getListDays]];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        processoLista = 1;
        [tabella reloadData];
    }];
}

-(void)aggiornaListaInDownload{
    // NSLog(@"aggiornaListaInDownload<-Fine");
    list_day = [NSMutableArray arrayWithArray:[Day getListDays]];

    // STEP 1
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
            // NSLog(@"MainReload_listaView: step(1)");
            processoLista = 0;
            [self rimuoviRotellaMM];
        }];//fine step1
    
    
    // STEP 2
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
            // NSLog(@"MainReload_listaView: step(2)");
            processoLista = 0;
            if(firstAppear) [self.tabella setHidden:YES];
            [tabella reloadData];
            if(firstAppear){
                [self animateTableAppear];
                firstAppear = NO;
            }
    }];//fine step2
    
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];

    [self performSelectorOnMainThread:@selector(MainReloadMM) withObject:nil waitUntilDone:YES];
    [self rimuoviRotella];
}

-(void)MainReloadMM{
    // NSLog(@"MainReload");
    [tabella reloadData];
}


#pragma mark -
#pragma mark ->> Table view
#pragma mark -
#pragma mark --------> Table view
#pragma mark Header/Footer
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// HEADER SESSION
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

// FOOTER SESSION
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

#pragma mark ROW

//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView==self.tabellaMessa){
        return self.messe_data.count;
    }
    [tabella setAlpha:(processoLista>0)?0.6:1];
    NSInteger i = [list_day count];
    [tableView setScrollEnabled:self.isIphone5?(i>6):(i>4)];
    // NSLog(@"LISTA:numberOfRows-> list_day[%lu] processoLista:%i tabella:%@",(unsigned long)list_day.count,processoLista,[tabella description]);
    return i;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 60.f;
}

- (UITableViewCell *)messaCellaForIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"CellMessa";
    // NSLog(@"cellForRowAtIndexPath:%@",indexPath);
    
    UITableViewCell *cell = [self.tabellaMessa dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        UIView* backgroundView = [[UIView alloc ] initWithFrame:CGRectZero ];
        backgroundView.backgroundColor = [UIColor clearColor];
        cell.backgroundView.alpha = 0;
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.alpha = 0;
        cell.contentView.backgroundColor = [UIColor clearColor];
        [cell setBackgroundView: backgroundView];
        cell.textLabel.backgroundColor =[UIColor clearColor];
        cell.textLabel.text = @"";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    //CUSTOM LABEL
    UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, self.tabellaMessa.frame.size.width, 59)];
    [Label setBackgroundColor:[UIColor colorWithRed:215/255.f green:170/255.f blue:105/255.f alpha:0.4]];
    [Label.layer setMasksToBounds:YES];
    [cell.contentView addSubview:Label];
    
    
    Messa *m = [self.messe_data objectAtIndex:indexPath.row];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *data_giorno = [dateFormatter dateFromString:m.day.id_date];
    
    //-->NUMERO DELLA SETTIMANA
    UILabel *numeroGiorno = [[UILabel alloc] initWithFrame:CGRectMake(0, -7, 50, 50)];
    [numeroGiorno setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:25]];
    [numeroGiorno setTextAlignment:NSTextAlignmentCenter];
    [numeroGiorno setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.9]];
    [numeroGiorno setBackgroundColor:[UIColor clearColor]];
    [dateFormatter setDateFormat:@"dd"];
    [numeroGiorno setText:[dateFormatter stringFromDate:data_giorno]];
    
    //-->GIORNO DELLA SETTIMANA
    UILabel *giornoSett = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, 50, 20)];
    [giornoSett setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14]];
    [giornoSett setTextAlignment:NSTextAlignmentCenter];
    [giornoSett setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7]];
    [dateFormatter setDateFormat:@"EEE"];
    [giornoSett setBackgroundColor:[UIColor clearColor]];
    [giornoSett setText:[[dateFormatter stringFromDate:data_giorno] uppercaseString]];
    
    //-> CERCHIO
    UIView *cerchioUno = [[UIView alloc] initWithFrame:CGRectMake(12, 5, 50, 50)];
    [cerchioUno setBackgroundColor:[[m.day messaColorWeek] colorWithAlphaComponent:0.7]];
    [cerchioUno.layer setCornerRadius:25];
    [cerchioUno.layer setMasksToBounds:YES];
    [cerchioUno setTag:999];
    [cerchioUno addSubview:numeroGiorno];
    [cerchioUno addSubview:giornoSett];
    
    
    //-> MESE STRINGA
    UILabel *giorno = [[UILabel alloc] initWithFrame:CGRectMake(65, 0, 120, 59)];
    [giorno setBackgroundColor:[UIColor clearColor]];
    [giorno setFont:[UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size:22]];
    [giorno setTextColor:defaulf_color];
    [giorno setTextAlignment:NSTextAlignmentCenter];
    [giorno setAdjustsFontSizeToFitWidth:YES];
    [giorno setMinimumScaleFactor:0.5];
    [dateFormatter setDateFormat:@"MMMM"];
    [giorno setText:[[dateFormatter stringFromDate:data_giorno] capitalizedString]];
    giorno.numberOfLines = 1;
    
    //-> ANNO STRINGA
    UILabel *anno = [[UILabel alloc] initWithFrame:CGRectMake(102, 5, 100, 55)];
    [anno setBackgroundColor:[UIColor clearColor]];
    [anno setFont:[UIFont fontWithName:FONT_HELVETICANEUE_CONDENSEDBLACK size:45]];
    [anno setTextColor:[UIColorFromRGB(0Xddc29b) colorWithAlphaComponent:0.4]];
    [anno setTextAlignment:NSTextAlignmentLeft];
    [dateFormatter setDateFormat:@"YYYY"];
    [anno setText:[[dateFormatter stringFromDate:data_giorno] capitalizedString]];
    anno.numberOfLines = 1;
    
    //-->LINGUA
    LinguaObject *lingua = m.day.linguaGiorno;
    
    
    //LOGO Lingua
    UIView *logo1 = lingua.view;
    CGRect  logo_frame =  logo1.frame;
    logo_frame.origin.x = Label.frame.size.width-(logo_frame.size.width);
    logo_frame.origin.y = (Label.frame.size.height-logo_frame.size.height)/2.0;
    [logo1 setFrame:logo_frame];
    
    UIView* schiarire = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, Label.frame.size.width-(logo_frame.size.width), 70)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = schiarire.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColorFromRGB(0Xded6c9) colorWithAlphaComponent:0.9] CGColor], (id)[[[UIColor whiteColor] colorWithAlphaComponent:0.2] CGColor],(id)[[[UIColor whiteColor] colorWithAlphaComponent:0.2] CGColor], nil];
    
    [gradient setStartPoint:CGPointMake(0.0, 0.5)];
    [gradient setEndPoint:CGPointMake(1.0, 0.5)];
    
    [schiarire.layer insertSublayer:gradient atIndex:0];
    
    
    //-> TITOLO_MESSA
    // NSLog(@"titolo messa:%@",m.messa_name);
    if(m.messa_name==nil) m.messa_name = m.day.title;
    UILabel *titoloMessa = [[UILabel alloc] initWithFrame:CGRectMake(180, 0, Label.frame.size.width-(logo_frame.size.width+180), 59)];
    [titoloMessa setBackgroundColor:[UIColor clearColor]];
    [titoloMessa setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:12]];
    [titoloMessa setTextColor:UIColorFromRGB(0X745f4d)];
    NSMutableParagraphStyle *style  = [NSMutableParagraphStyle new];
    style.minimumLineHeight = titoloMessa.font.pointSize;
    style.maximumLineHeight = titoloMessa.font.pointSize;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style};
    //set text with attribute
    titoloMessa.attributedText = [[NSAttributedString alloc] initWithString:[m.messa_name uppercaseString]
                                                                 attributes:attributtes];
    
    [titoloMessa setTextAlignment:NSTextAlignmentCenter];
    [titoloMessa setAdjustsFontSizeToFitWidth:YES];
    [titoloMessa setMinimumScaleFactor:0.5];
    titoloMessa.numberOfLines = 2;
    
    [Label addSubview:schiarire];
    [Label addSubview:cerchioUno];
    if(m.isComune)
    {
        //-> CERCHIO COMUNE
        //UILabel *cerchioComune = [[UILabel alloc] initWithFrame:CGRectMake(48, 4, 18, 18)];
        UILabel *cerchioComune = [[UILabel alloc] initWithFrame:CGRectMake(49, 4, 18, 18)];
        [cerchioComune setBackgroundColor:[UIColorFromRGB(0X744414) colorWithAlphaComponent:0.7]];
        [cerchioComune setBackgroundColor:[UIColorFromRGB(0XFFFFFF) colorWithAlphaComponent:0.85]];
        [cerchioComune.layer setCornerRadius:cerchioComune.frame.size.width/2.0];
        [cerchioComune.layer setBorderColor:[[UIColorFromRGB(0XFF0000) colorWithAlphaComponent:0.8] CGColor]];
        [cerchioComune.layer setBorderWidth:1.5];
        [cerchioComune.layer setCornerRadius:cerchioComune.frame.size.width/2.0];
        
        [cerchioComune setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7]];
        [cerchioComune setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:16]];
        [cerchioComune.layer setMasksToBounds:YES];
        NSMutableParagraphStyle *style  = [NSMutableParagraphStyle new];
        style.minimumLineHeight = cerchioComune.frame.size.height-2;
        style.maximumLineHeight = cerchioComune.frame.size.height-2;
        style.alignment = NSTextAlignmentCenter;
        //NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
        //set text with attribute
        //cerchioComune.attributedText = [[NSAttributedString alloc] initWithString:@"c" attributes:attributtes];
        
        [Label addSubview:cerchioComune];
    }
    [Label addSubview:anno];
    [Label addSubview:giorno];
    [Label addSubview:logo1];
    [Label addSubview:titoloMessa];
    
    
    BOOL isNotCompleted = (m.sel_prefazio.intValue == -1 || m.sel_preghieraEucaristica.intValue == -1);
    
    if(isNotCompleted)
    {
        UIImageView *incompleto = [[UIImageView alloc] initWithFrame:CGRectMake(10, 4, 12, 12)];
        [incompleto setBackgroundColor:[UIColor clearColor]];
        [incompleto setContentMode:UIViewContentModeScaleAspectFit];
        [incompleto setAlpha:0.9];
        [incompleto setImage:[UIImage imageNamed:@"erro.png"]];
        [Label addSubview:incompleto];
        
        UILabel *incompleto_label = [[UILabel alloc] initWithFrame:CGRectMake(49, 4, 18, 18)];
        [incompleto_label setBackgroundColor:[UIColor clearColor]];
        [Label addSubview:incompleto_label];
        
    }
    
    
    //Label on cell
    [cell.contentView addSubview:Label];
    return cell;
}

#pragma mark CELL
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView==self.tabellaMessa){
        return [self messaCellaForIndexPath:indexPath];
    }

    static NSString *CellIdentifier = @"Cell";
    // NSLog(@"cellForRowAtIndexPath:%@",indexPath);
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		UIView* backgroundView = [[UIView alloc ] initWithFrame:CGRectZero ];
		backgroundView.backgroundColor = [UIColor clearColor];
		cell.backgroundView.alpha = 0;
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.alpha = 0;
		cell.contentView.backgroundColor = [UIColor clearColor];
		[cell setBackgroundView: backgroundView];
        cell.textLabel.backgroundColor =[UIColor clearColor];
        cell.textLabel.text = @"";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [tabella setAlpha:(processoLista==1)?0.6:1];
    
    //CUSTOM LABEL
    UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 59)];
    [Label setBackgroundColor:[UIColor colorWithRed:215/255.f green:170/255.f blue:105/255.f alpha:0.4]];
    [Label.layer setMasksToBounds:YES];
    [cell.contentView addSubview:Label];
    
    
    NSMutableDictionary *dettagli = [self parseDaySaved:[list_day objectAtIndex:indexPath.row]];
    
    
    UIView* schiarire = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, Label.frame.size.width-126, 70)];
    [schiarire setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.2]];
    /*
     CAGradientLayer *gradient = [CAGradientLayer layer];
     gradient.frame = schiarire.bounds;
     gradient.colors = [NSArray arrayWithObjects:(id)[[[UIColor whiteColor] colorWithAlphaComponent:0.2] CGColor],(id)[[UIColorFromRGB(0X603e11) colorWithAlphaComponent:0.2] CGColor], nil];
     [gradient setStartPoint:CGPointMake(0.0, 0.5)];
     [gradient setEndPoint:CGPointMake(1.0, 0.5)];
     [schiarire.layer insertSublayer:gradient atIndex:0];
     */

    

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];        
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *data_giorno = [dateFormatter dateFromString:[dettagli objectForKey:@"date_day"]];
    
    //-->NUMERO DELLA SETTIMANA
    UILabel *numeroGiorno = [[UILabel alloc] initWithFrame:CGRectMake(0, -7, 50, 50)];
    [numeroGiorno setFont:[UIFont fontWithName:FONT_HELVETICANEUE_BOLD size:25]];
    [numeroGiorno setTextAlignment:NSTextAlignmentCenter];
    [numeroGiorno setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.9]];
    [numeroGiorno setBackgroundColor:[UIColor clearColor]];
    [dateFormatter setDateFormat:@"dd"];
    [numeroGiorno setText:[dateFormatter stringFromDate:data_giorno]];

    //-->GIORNO DELLA SETTIMANA
    UILabel *giornoSett = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, 50, 20)];
    [giornoSett setFont:[UIFont fontWithName:FONT_HELVETICANEUE_BOLD size:14]];
    [giornoSett setTextAlignment:NSTextAlignmentCenter];
    [giornoSett setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7]];
    [dateFormatter setDateFormat:@"EEE"];
    [giornoSett setBackgroundColor:[UIColor clearColor]];
    [giornoSett setText:[[dateFormatter stringFromDate:data_giorno] uppercaseString]];
    
    //-> CERCHIO
    UIView *cerchioUno = [[UIView alloc] initWithFrame:CGRectMake(12, 5, 50, 50)];
    [cerchioUno setBackgroundColor:[[colorWeek objectForKey:[dettagli objectForKey:@"weekday"]] colorWithAlphaComponent:0.7]];
    [cerchioUno.layer setCornerRadius:25];
    [cerchioUno.layer setMasksToBounds:YES];
    [cerchioUno setTag:999];
    [cerchioUno addSubview:numeroGiorno];
    [cerchioUno addSubview:giornoSett];


    //-> MESE STRINGA
	UILabel *giorno = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, 200, 59)];
	[giorno setBackgroundColor:[UIColor clearColor]];
    [giorno setFont:[UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size:22]];
    [giorno setTextColor:defaulf_color];
	[giorno setTextAlignment:NSTextAlignmentLeft];
    //[dateFormatter setDateFormat:@"MMMM ''YY"];
    [dateFormatter setDateFormat:@"MMMM"];
	[giorno setText:[[dateFormatter stringFromDate:data_giorno] capitalizedString]];
	giorno.numberOfLines = 1;
    
    //-> ANNO STRINGA
	UILabel *anno = [[UILabel alloc] initWithFrame:CGRectMake(102, 5, 100, 55)];
	[anno setBackgroundColor:[UIColor clearColor]];
    [anno setFont:[UIFont fontWithName:FONT_HELVETICANEUE_CONDENSEDBLACK size:45]];
    [anno setTextColor:[UIColorFromRGB(0Xddc29b) colorWithAlphaComponent:0.4]];
	[anno setTextAlignment:NSTextAlignmentLeft];
    [dateFormatter setDateFormat:@"YYYY"];
	[anno setText:[[dateFormatter stringFromDate:data_giorno] capitalizedString]];
	anno.numberOfLines = 1;

    
    //-->LINGUA
    LinguaObject *lingua = [LinguaObject getLinguaBySigla:[dettagli objectForKey:@"lang"]];
    
    
    //AreaLoghi
    UIView *areaLoghi = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-126.f, 0, 126, 60)];
    [areaLoghi setBackgroundColor:[UIColor clearColor]];
    
    //LOGO 1
    UIView *logo1 = lingua.view;
    [logo1 setFrame:CGRectMake(0, 0,  logo1.frame.size.width,  logo1.frame.size.height)];
    CGPoint centroArea = CGPointMake(areaLoghi.frame.size.width/2.0, areaLoghi.frame.size.height/2.0);
    CGPoint centro = centroArea;
    centro.x = (centro.x/2.0) + 10.0;
    [logo1 setCenter:centro];
    
    //LOGO 2
    UIImageView *logo2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [logo2 setBackgroundColor:[UIColor clearColor]];
    centro = centroArea;
    centro.x = (centro.x/2.0 )+ centroArea.x - 10.0;
    [logo2 setCenter:centro];
    [logo2 setContentMode:UIViewContentModeScaleAspectFit];
    [logo2 setAlpha:0.8];
    
    //LOGO 3
    UIImageView *logo3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [logo3 setBackgroundColor:[UIColor clearColor]];
    [logo3 setContentMode:UIViewContentModeScaleAspectFit];
    [logo3 setAlpha:0.8];
    centro = centroArea;
    centro.x = 3.0 * (centro.x/2.0 )+3.0;
    [logo3 setCenter:centro];

    //Controllo SANTO ARRAY
    //=====================
    NSMutableArray *santoPreview = [Day getSantPreviewForIndex:indexPath];
    BOOL controlloSanto = (santoPreview.count>0);

    if(controlloSanto)
    {
        [logo2 setImage:[santoPreview objectAtIndex:0]];
    }
    if(santoPreview.count>1)
    {
        [logo3 setImage:[santoPreview objectAtIndex:1]];
    }

    //Controllo TP
    //============
    BOOL controlloTP = [dettagli objectForKey:@"type"] && (![[dettagli objectForKey:@"type"] isEqualToString:@"generic"] && ![[dettagli objectForKey:@"type"] isEqualToString:@""]);
    if(controlloTP){
            UIImage *tp_logo = [[TestoProprio getTestoProprioWithKey:[dettagli objectForKey:@"type"]] logo];
            if(!tp_logo) tp_logo = [[TestoProprio getTestoProprioWithKey:[dettagli objectForKey:@"type"]] image_official];
            if(!logo2.image)[logo2 setImage:tp_logo];
            else [logo3 setImage:tp_logo];
    }

    //Controllo isHome TP Element
    //===========================
    // NSLog(@"th:%@ t:%@",[dettagli objectForKey:@"type_home"],[dettagli objectForKey:@"type"]);
    if([dettagli objectForKey:@"type_home"] && (![[dettagli objectForKey:@"type_home"] isEqualToString:@"(null)"] && ![[dettagli objectForKey:@"type_home"] isEqualToString:@""]) && ![[dettagli objectForKey:@"type"] isEqualToString:[dettagli objectForKey:@"type_home"]])
        {
        UIImage *tp_logo = [[TestoProprio getTestoProprioWithKey:[dettagli objectForKey:@"type_home"]] logo];
        if(!tp_logo) tp_logo = [[TestoProprio getTestoProprioWithKey:[dettagli objectForKey:@"type_home"]] image_official];
        if(!logo2.image)
            {
                [logo2 setImage:tp_logo];
            }
        else
            //C'è già un  logo/santo in  logo2
        {
                if(controlloTP && !controlloSanto)[logo3 setImage:tp_logo];
                else [logo2 setImage:tp_logo];
            }

        }
    
    //[logo3 setImage:logo2.image];

    //=================
    //POSIZIONO I // NSLogHI
    //=================
    if(logo3.image)
    {
    CGPoint centro = centroArea;
    centro.x = (centro.x/2.0)-3.0;
    [logo1 setCenter:centro];
    
    centro = centroArea;
    [logo2 setCenter:centro];
    }
    else if(!logo2.image && !logo3.image)
    {
        centro = centroArea;
        [logo1 setCenter:centro];
    }
    // NSLog(@"l1:%f,l2:%fi,l3:%f",logo1.center.x,logo2.center.x,logo3.center.x);
    
    
    //===========
    //COMPLETE
    //===========
    BOOL isNotComplete = [dettagli objectForKey:@"complete"] && [[dettagli objectForKey:@"complete"] isEqualToNumber:[NSNumber numberWithBool:NO]];
    UIImageView *incompleto;
    UIButton *buttonIncomplete;
    if(isNotComplete)
    {
    incompleto = [[UIImageView alloc] initWithFrame:CGRectMake(176, 3, 16, 16)];
    [incompleto setBackgroundColor:[UIColor clearColor]];
    [incompleto setContentMode:UIViewContentModeScaleAspectFit];
    [incompleto setAlpha:0.9];
    [incompleto setImage:[UIImage imageNamed:@"erro.png"]];
        buttonIncomplete = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonIncomplete setTag:indexPath.row];
        [buttonIncomplete setBackgroundColor:[UIColor clearColor]];
        [buttonIncomplete setFrame:CGRectMake(0, 0, 44, 44)];
        [buttonIncomplete addTarget:self action:@selector(incompletePressed:) forControlEvents:UIControlEventTouchUpInside];
        [buttonIncomplete setCenter:incompleto.center];
    }

    
    //===========
    //LAST UPDATE
    //===========
	UILabel *last_update = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-135.f, 49, 125, 11)];
	[last_update setBackgroundColor:[UIColor clearColor]];
    [last_update setFont:[UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size:8]];
    [last_update setTextColor:[defaulf_color colorWithAlphaComponent:0.3]];
	[last_update setTextAlignment:NSTextAlignmentCenter];
	[last_update setText:[dettagli objectForKey:@"last_update"]];
	last_update.numberOfLines = 1;
    
    //===========
    //CURRENT DAY
    //===========
    if([Day isCurrentDay:indexPath])
    {
        [cerchioUno.layer setBorderWidth:3.5];
        [cerchioUno.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:0.86] CGColor]];
        CGFloat offset = 3.5;
        CGRect split = cerchioUno.frame;
        split.origin.x -=offset;
        split.origin.y -=offset;
        split.size.width += offset*2;
        split.size.height += offset*2;
        
        //cerchioUno
        [cerchioUno setFrame:split];
        [cerchioUno.layer setCornerRadius:split.size.height/2];
        
        //numeroGiorno
        split = numeroGiorno.frame;
        split.origin.x+=offset;
        split.origin.y+=offset;
        [numeroGiorno setFrame:split];
        
        //giornoSett
        split = giornoSett.frame;
        split.origin.x+=offset;
        split.origin.y+=offset;

        [giornoSett setFrame:split];
    }

    
    [Label addSubview:schiarire];
    [Label addSubview:last_update];
    [Label addSubview:cerchioUno];
    [Label addSubview:anno];
    [Label addSubview:giorno];
    
    
    
    //AGGIUNGO // NSLogHI
    if(logo2.image) [areaLoghi addSubview:logo2];
    if(logo3.image) [areaLoghi addSubview:logo3];
    [areaLoghi addSubview:logo1];
    
    [Label addSubview:areaLoghi];

    
    
    //SEGNALO se non COMPLETO
    if(isNotComplete){ [Label addSubview:incompleto];[Label addSubview:buttonIncomplete];}
    
    if(cellaSelezionata==indexPath.row){
        // NSLog(@"cellaInDownload=%i",cellaSelezionata);
        
        /*
        UIActivityIndicatorView *rotellaDownload = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [rotellaDownload setColor:[[UIColor whiteColor] colorWithAlphaComponent:0.9]];
        [rotellaDownload setTransform:CGAffineTransformMakeScale(1.3f, 1.3f)];
        [rotellaDownload startAnimating];
        [rotellaDownload setCenter:cerchioUno.center];
        [Label addSubview:rotellaDownload];
         */
    }

    
    
    //Label on cell
    [cell.contentView addSubview:Label];
    return cell;
}

-(NSMutableDictionary *)parseDaySaved:(NSString*)dayName{
    // NSLog(@"parseDaySaved:%@",dayName);
    NSMutableDictionary *day_info = [[NSMutableDictionary alloc] init];
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    NSArray *components = [dayName componentsSeparatedByString:@"_"];
    if(components.count==6)
    {
    [day_info setObject:[components objectAtIndex:0] forKey:@"date_day"];
    [day_info setObject:[components objectAtIndex:1] forKey:@"lang"];
    [day_info setObject:[components objectAtIndex:2] forKey:@"type"];
    [day_info setObject:[components objectAtIndex:3] forKey:@"weekday"];
    [day_info setObject:[components objectAtIndex:4] forKey:@"type_home"];
    [day_info setObject:[NSNumber numberWithBool:[(NSString*)[components objectAtIndex:5] uguale:@"1"]] forKey:@"complete"];
    }
    else{
        NSError *err = nil;
        [self.fileManager removeItemAtPath:[self.cacheDir stringByAppendingFormat:@"/DAY/%@",dayName] error:&err];
        // NSLog(@"errore day file inatteso[%@]",err?[err description]:dayName);
        [tabella reloadData];
        return nil;
        
    }
    NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[self.cacheDir stringByAppendingFormat:@"/%@/%@",PATH_DAY,dayName] error:nil];
    [f setDateFormat:@"dd/MM hh:mm:ss"];
    [day_info setObject:[f stringFromDate:[attributes fileModificationDate]] forKey:@"last_update"];
    // NSLog(@"ParsedDayName(%@):%@",dayName,day_info);
    return day_info;
}

#pragma mark -
#pragma mark DELETE Table row and item
#ifdef __IPHONE_8_0

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView==self.tabellaMessa){
        // NSLog(@"MessaList editor");
        UITableViewRowAction *shareAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"      " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            // maybe show an action sheet with more options
            // NSLog(@"Share pressed!");
            [self creaPDFConMessa:indexPath];
            //[tableView setEditing:NO];
            // NSLog(@"Fine Share pressed!");
        }];
        //shareAction.backgroundColor = UIColorFromRGB(0X007aff);
        shareAction.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"share_icon_cell.png"]];
        
        return @[shareAction];

    }

    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"      " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self doDelete:indexPath];
    }];
    deleteAction.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"delete_icon_cell.png"]];
    
    return @[deleteAction];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
#endif

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView==self.tabellaMessa){
        NSLog(@"iPhone: MessaList no edit!!");
        return;
    }
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self doDelete:indexPath];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

-(void)doDelete:(NSIndexPath *)indexPath{
    // NSLog(@"delete AVVIATO ->%i",[Day isCurrentDay:indexPath]);
    if([Day isCurrentDay:indexPath])
    {
        [tabella reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                       withRowAnimation: UITableViewRowAnimationBottom];
        return;
    }
    NSString *delete = [list_day objectAtIndex:indexPath.row];
    NSError *err = nil;
    if([Day deleteDayWithName:delete andError:&err]){
        [list_day removeObjectAtIndex:indexPath.row];
        [tabella deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else {
        // NSLog(@"errore cancellando %@:%@",delete,[err description]);
    }
}

-(void)creaPDFConMessa:(NSIndexPath*)indexPath
{
    //========
    // STEP 1
    //========
    __block Messa *m = [list_day objectAtIndex:indexPath.row];

    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        //BLoccare la vista
        [self.view setUserInteractionEnabled:NO];
        self.rotellaShare = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self.rotellaShare setColor:self.rotella.color];
        [self.rotellaShare setCenter:(CGPoint){self.view.frame.size.width/2.,self.view.frame.size.height/2.}];
        [self.rotellaShare startAnimating];
        [self.view addSubview:self.rotellaShare];
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        [self alert:m.titolo_custom
             andMex:NSLocalizedString(@"MessaSharePdfText", nil)
     optionOneTitle:NSLocalizedString(@"MessaSharePdfOmeliaSI", nil)
          optionOne:^(UIAlertAction *action) {
              NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
                  [self creaPDFConMessaStep1:m conOmelia:YES];
              }];//fine step3
              [[NSOperationQueue mainQueue] addOperation:step3];
          }
     optionTwoTitle:NSLocalizedString(@"MessaSharePdfOmeliaNO", nil)
          optionTwo:^(UIAlertAction *action) {
              NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
                  [self creaPDFConMessaStep1:m conOmelia:NO];
              }];//fine step3
              [[NSOperationQueue mainQueue] addOperation:step3];
          }];
    }];//fine step2
    
    
    
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    
}

-(void)creaPDFConMessaStep1:(Messa*)m conOmelia:(BOOL)visualizzaOmelia{
    // NSLog(@"creaPDFConMessa:%@",m);
    self.htmlPdfKit = [[BNHtmlPdfKit alloc] initWithPageSize:BNPageSizeA4];
    self.htmlPdfKit.delegate = self;
    NSString *messa_name = m.messa_name;
    if(messa_name==nil) messa_name = m.day.title;

    [self.htmlPdfKit saveHtmlAsPdf:[m htmlPdfStringWithOmelia:visualizzaOmelia]
                            toFile:[self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"iBreviary - %@.pdf",messa_name]]];
}

//==================================
// PDF DELEGATE
//==================================
#pragma mark - PDF DELEGATE -
- (void)htmlPdfKit:(BNHtmlPdfKit *)h didSavePdfFile:(NSString *)file{
    // NSLog(@"didSavePdfFile[%i]:%@", h.pageSize,file);
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        self.documentInteractionCtrl = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:file]];
        [self.documentInteractionCtrl setUTI:@"com.adobe.pdf"];
        
        self.documentInteractionCtrl.delegate = self;

        // NSLog(@"presenting documentCTRL:%@",self.documentInteractionCtrl);
        [self.documentInteractionCtrl presentOptionsMenuFromRect:CGRectZero inView:self.view animated:YES];
        
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        [self.view setUserInteractionEnabled:YES];
        [self.rotellaShare removeFromSuperview];
        self.rotellaShare = nil;
    }];//fine step2
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    
}
- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didFailWithError:(NSError *)error{
    // NSLog(@"didFailWithError:%@",[error description]);
    
}

#pragma mark - document delegate
-(void)documentInteractionController:controller willBeginSendingToApplication:(NSString *)application{
    // NSLog(@"documentInteractionController:%@",application);
    [self.tabellaMessa setEditing:NO];
}

-(void)documentInteractionControllerWillPresentOptionsMenu:(UIDocumentInteractionController *)controller{
    // NSLog(@"documentInteractionControllerWillPresentOptionsMenu");
}
-(void)documentInteractionControllerDidDismissOptionsMenu:(UIDocumentInteractionController *)controller{
    [self.tabellaMessa setEditing:NO];
    // NSLog(@"documentInteractionControllerDidDismissOptionsMenu");
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller
{
    return self.view;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller
{
    return self.view.frame;
}


#pragma mark -
#pragma mark Table view DIDSELECT

// COMANDA DOWNLOAD TESTI PROPRI DAY
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView==self.tabellaMessa){
        // NSLog(@"MessaList didSelectRowAtIndexPath:%@",indexPath);
        [self showMessaIphone:indexPath];
        return;
    }
    // NSLog(@"didSelectRowAtIndexPath:%i",(int)indexPath.row);
    cellaSelezionata = (int)indexPath.row;
    if(self.isIpad)
    {
        [Home attivaRotellaHome];
        [delegate performSelectorInBackground:@selector(caricaGiornoWithIndex:) withObject:indexPath];
    }
    [self performSelectorOnMainThread:@selector(ringLine:) withObject:indexPath waitUntilDone:YES];
    if(self.isIpad)
    {
            [[[Home sharedInstance] lista_popover] dismissPopoverAnimated:YES];
        }
    else [delegate performSelectorInBackground:@selector(caricaGiornoWithIndex:) withObject:indexPath];
    [self performSelector:@selector(fine:) withObject:indexPath afterDelay:0.4];
}

-(void)showMessaIphone:(NSIndexPath*)indexPath{
    
    //========
    // STEP 1
    //========
    [self performSelectorOnMainThread:@selector(ringLine:) withObject:indexPath waitUntilDone:YES];
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"ListaMessa: did select row step 2");
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     // NSLog(@"ListaMessa dismissed");
                                     [Home mostraMessa:[self.messe_data objectAtIndex:indexPath.row] editMode:NO];
        }];
    }];//fine step2
    
    /*
     performSelectorInBackground
     */
    [[NSOperationQueue currentQueue] addOperation:step2];
}


-(void)pulseLine:(NSIndexPath *)indexPath{
    UIView *anello = [[UIView alloc] initWithFrame:CGRectMake(12, 5, 50, 50)];
    [anello setBackgroundColor:[UIColor clearColor]];
    [anello.layer setBorderWidth:4];
    [anello.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:1] CGColor]];
    CGFloat offset = 3.5;
    CGRect split = anello.frame;
    split.origin.x -=offset;
    split.origin.y -=offset;
    split.size.width += offset*2;
    split.size.height += offset*2;
    
    [anello setFrame:split];
    [anello.layer setCornerRadius:split.size.height/2];
    
    [[tabella cellForRowAtIndexPath:indexPath].contentView addSubview:anello];
    [self aggiungiRotellaAllaLinea:indexPath];
    
    
    CABasicAnimation *theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    theAnimation.duration=0.35;
    theAnimation.repeatCount=3;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:0.0];
    theAnimation.toValue=[NSNumber numberWithFloat:1.0];
    [anello.layer addAnimation:theAnimation forKey:@"animateOpacity"];

}

-(void)aggiungiRotellaAllaLinea:(NSIndexPath *)indexPath{
    UIView *anello = [[UIView alloc] initWithFrame:CGRectMake(12, 5, 50, 50)];
    UIActivityIndicatorView *rotellaDownload = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [rotellaDownload setColor:[[UIColor whiteColor] colorWithAlphaComponent:0.2]];
    [rotellaDownload setTransform:CGAffineTransformMakeScale(1.1f, 1.1f)];
    [rotellaDownload startAnimating];
    [rotellaDownload setCenter:anello.center];
    [rotellaDownload setAlpha:0];
    [[tabella cellForRowAtIndexPath:indexPath].contentView addSubview:rotellaDownload];
    [UIView animateWithDuration:0.2 animations:^{ [rotellaDownload setAlpha:1];}];

}

-(void)ringLine:(NSIndexPath *)indexPath
{
    UIView *anello = [[UIView alloc] initWithFrame:CGRectMake(12, 5, 50, 50)];
    [anello setBackgroundColor:[UIColor clearColor]];
    //[anello.layer setBorderWidth:4];
    //[anello.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:1] CGColor]];
    CGFloat offset = 3.5;
    CGRect split = anello.frame;
    split.origin.x -=offset;
    split.origin.y -=offset;
    split.size.width += offset*2;
    split.size.height += offset*2;
    
    [anello setFrame:split];
    [anello.layer setCornerRadius:split.size.height/2];
    
    
    
    [[tabella cellForRowAtIndexPath:indexPath].contentView addSubview:anello];
    
    UIColor *stroke = [UIColor whiteColor];
    
    CABasicAnimation *borderAnimation = [CABasicAnimation animationWithKeyPath:@"borderColor"];
    borderAnimation.fromValue = (id)[UIColor clearColor].CGColor;
    borderAnimation.toValue = (id)stroke.CGColor;
    borderAnimation.duration = 0.5f;
    [anello.layer addAnimation:borderAnimation forKey:nil];
    
    CGRect pathFrame = CGRectMake(-CGRectGetMidX(anello.bounds), -CGRectGetMidY(anello.bounds), anello.bounds.size.width, anello.bounds.size.height);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:pathFrame cornerRadius:anello.layer.cornerRadius];
    
    UIView *dove;
    if(self.isIpad){
        dove = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    }
    else dove = self.view;
    CGPoint shapePosition = [dove convertPoint:anello.center fromView:anello.superview];

    
    CAShapeLayer *circleShape = [CAShapeLayer layer];
    circleShape.path = path.CGPath;
    circleShape.position = shapePosition;
    circleShape.fillColor = [UIColor clearColor].CGColor;
    circleShape.opacity = 0;
    circleShape.strokeColor = stroke.CGColor;
    circleShape.lineWidth = 4;
    
    [dove.layer addSublayer:circleShape];
    
    
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    scaleAnimation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(2.5, 2.5, 1)];
    
    CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    alphaAnimation.fromValue = @1;
    alphaAnimation.toValue = @0;
    
    CAAnimationGroup *animation = [CAAnimationGroup animation];
    animation.animations = @[scaleAnimation, alphaAnimation];
    animation.duration = 0.5f;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    [circleShape addAnimation:animation forKey:nil];
    [self aggiungiRotellaAllaLinea:indexPath];
}


-(void)fine:(NSIndexPath*)indexPath{
    // NSLog(@"Dismissing Lista...");
    [self performSelectorOnMainThread:@selector(mainDidSelect:) withObject:indexPath waitUntilDone:NO];
}

-(void)mainDidSelect:(NSIndexPath*)indexPath{
    [self dismissViewControllerAnimated:YES completion:^{
                                            // NSLog(@"DIsmissed");
                                            cellaSelezionata = -1;
                                            [Home dismiss:self];
                                                        }];
}

#pragma mark - Animate Table
//==========================
// ANIMATE TABLE
//==========================
-(void)animateTableAppear{
    [self.tabella setHidden:NO];
    NSArray *visibleCell = [self.tabella visibleCells];
    for(__block NSUInteger idx = 0; idx < visibleCell.count; idx++){
        __block UITableViewCell *cell = [visibleCell objectAtIndex:idx];
        CGPoint origin = cell.frame.origin;
        [cell setFrame:CGRectMake(cell.frame.origin.x, self.tabella.frame.size.height+self.tabella.contentOffset.y, cell.frame.size.width, cell.frame.size.height)];
        [UIView animateWithDuration:0.7
                              delay:(0.1*(float)idx)
             usingSpringWithDamping:0.99
              initialSpringVelocity:15
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [cell setFrame:CGRectMake(origin.x, origin.y, cell.frame.size.width, cell.frame.size.height)];
                         } completion:^(BOOL fine){
                             
                         }];
        
    }
    [self.tabella setHidden:NO];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(processoLista) return;
    CATransform3D translation;
    if(goingUP){
        translation = CATransform3DMakeTranslation(0, MIN(fabs(self.scrollVelocity.y), cell.frame.size.height), 0);
    } else {
        translation = CATransform3DMakeTranslation(0, -MIN(fabs(self.scrollVelocity.y), cell.frame.size.height), 0);
    }
    
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    cell.layer.transform = CATransform3DIdentity;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}

CGFloat lastContentOffset;
BOOL goingUP;

int main_page = 0;

#pragma mark - ScrollView delegate (for top animation)
//===================
//ScrollView delegate
//===================
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(scrollView != self.scroller_delegate){
            if (lastContentOffset > scrollView.contentOffset.y)
                    goingUP = NO;
            else if (lastContentOffset <= scrollView.contentOffset.y)
                    goingUP = YES;
    
            lastContentOffset = scrollView.contentOffset.y;
            return;
    }
    
    
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scroller_delegate.frame.size.width;
    
    int page = floor((self.scroller_delegate.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    // NSLog(@"scrollViewDidScroll:%i",page);
    if(page!= self.pager.currentPage){
        self.pager.currentPage = page;
        
        [self changePage:self.pager];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.scrollVelocity = [scrollView.panGestureRecognizer velocityInView:scrollView];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    self.scrollVelocity = [scrollView.panGestureRecognizer velocityInView:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    self.scrollVelocity = [scrollView.panGestureRecognizer velocityInView:scrollView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    
}

#pragma mark -
#pragma mark Picker
//=========================
// Picker
//=========================
- (void)initListPicker
{
    //PICKER DAY
	NSLog(@"INIT DAY PICK");
    
    if (!picker_ctrl_day) picker_ctrl_day = [[CPPickerView alloc] initWithFrame:CGRectMake(0, 0, 190, 60)];
    picker_ctrl_day.backgroundColor = [UIColor clearColor];
    picker_ctrl_day.dataSource = self;
    picker_ctrl_day.delegate = self;
    [picker_ctrl_day setItemFont:[UIFont fontWithName:FONT_HELVETICANEUE_THIN size:20]];
    [picker_ctrl_day setItemColor:pickerColor];
	FullDateFormatter *dateFormatter = [[FullDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSMutableArray *lista_giorni = [[NSMutableArray alloc] init];
	
	for (int i=-1; i<16; i++)
    {
        NSString *test = [dateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*i]];
        [lista_giorni addObject:test];
    }
    
	list_down = [[NSMutableArray alloc] initWithArray:lista_giorni];
    // NSLog(@"list_down:%i",(int)list_down.count);

    [picker_ctrl_day reloadData];
    
    if(!picker_ctrl_day.superview){[backPicker addSubview:picker_ctrl_day];}

    //PICKER LINGUA
    // NSLog(@"INIT LINGUA PICK");
    if (!picker_ctrl_lingua)
    {
        picker_ctrl_lingua = [[UIPickerView alloc] initWithFrame:CGRectMake(-4, -57, 60, 70)];
        //[picker_ctrl_lingua setSoundsEnabled:NO];
        [picker_ctrl_lingua setBackgroundColor:[UIColor clearColor]];
        [backPicker_lingua.layer setMasksToBounds:YES];
        
        if(!self.isIos7orUpper){
            
            // NSLog(@"picker_ctrl_lingua:%@\n%@",picker_ctrl_lingua,picker_ctrl_lingua.subviews);
            
        }

        
    }

    [picker_ctrl_lingua setDelegate:self];
    [picker_ctrl_lingua setDataSource:self];
    
    
    if(!picker_ctrl_lingua.superview) [backPicker_lingua addSubview:picker_ctrl_lingua];
    [picker_ctrl_lingua setCenter:CGPointMake(backPicker_lingua.frame.size.width/2, backPicker_lingua.frame.size.height/2)];
    

    [picker_ctrl_lingua selectRow:[[linguaPos objectForKey:[[[Home getCurrentDay] linguaGiorno] sigla]] intValue] inComponent:0 animated:NO];
    
    [up_btn setAlpha:([picker_ctrl_lingua selectedRowInComponent:0]==0)?0.3:1];
    
    [down_btn setAlpha:([picker_ctrl_lingua selectedRowInComponent:0]==[list_lingua_img count]-1)?0.3:1];
    
    // NSLog(@"Fine Picker");

}


#pragma mark - CPPickerViewDataSource
- (NSInteger)numberOfItemsInPickerView:(UIPickerView *)pickerView
{
    // NSLog(@"Picker[%@]:numberOfItemsInPickerView %i",[pickerView class],(int)list_down.count);
    return list_down.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    NSInteger i = ([pickerView isKindOfClass:[CPPickerView class]])?list_down.count:list_lingua_img.count;
    // NSLog(@"Picker[%@]:numberOfRowsInComponent %i",list_down,(int)i);
    return i;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForItem:(NSInteger)index
{
    if(!([pickerView isKindOfClass:[CPPickerView class]]))
        return nil;
    if((index > list_down.count)) return @"errore";
    if(([pickerView isKindOfClass:[CPPickerView class]]))
    {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setLocale:[NSLocale currentLocale]];
    
	// get NSDate from old string format
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
	NSDate *date = [dateFormatter dateFromString:[list_down objectAtIndex:index]];
	
	// get string in new date format
    [dateFormatter setDateFormat:@"EEEE d MMMM"];
    
    return [[dateFormatter stringFromDate:date] capitalizedString];
    }
    
    
    return nil;
}


#pragma mark - CPPickerViewDelegate
//=========================
// Picker delegate
//=========================
- (void)pickerView:(CPPickerView *)pickerView didSelectItem:(NSInteger)item{
    // NSLog(@"CPPickerView: didSelectItem:%i",(int)item);
    [dx_btn setAlpha:(item==(list_down.count - 1))?0.3:1];
    [sx_btn setAlpha:(item==0)?0.3:1];
}


- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    // NSLog(@"pickerView rowHeightForComponent:%@",[pickerView class]);
    if(pickerView==picker_ctrl_lingua) return 60.0;
    if([pickerView isKindOfClass:[CPPickerView class]]) return 60.0;
    return 60.0;
}

#pragma mark - PickerViewDelegate
//=========================
// Picker delegate
//=========================
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(thePickerView==picker_ctrl_lingua)
    {
    // NSLog(@"DayPicker item selected:%i",(int)row);
    [up_btn setAlpha:(row==0)?0.3:1];
    
    [down_btn setAlpha:(row==[list_lingua_img count]-1)?0.3:1];
    }

}


-(UIView *)pickerView:(UIPickerView *)pickerView
           viewForRow:(NSInteger)row
         forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    LinguaObject *l = [list_lingua_img objectAtIndex:row];
    // NSLog(@"view4lingua:%@",l);
    for(int s = 0; s < [[picker_ctrl_lingua subviews] count];s++)
    {
        if(self.isIos7orUpper)
        {
        UIView *sub = [[picker_ctrl_lingua subviews] objectAtIndex:s];
        [sub setHidden:(s!=0)];
        }
        else{
            // NSLog(@"ISO6->%@",[picker_ctrl_lingua subviews]);
            [(UIView*)[[picker_ctrl_lingua subviews] objectAtIndex:0] setHidden:YES];
            [(UIView*)[[picker_ctrl_lingua subviews] objectAtIndex:4] setHidden:YES];
            [(UIView*)[[picker_ctrl_lingua subviews] objectAtIndex:1] setHidden:YES];
            [(UIView*)[[picker_ctrl_lingua subviews] objectAtIndex:3] setHidden:YES];

        }
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.isIos7orUpper?60:50, self.isIos7orUpper?60:50)];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    [imageView setImage:l.logo];
    return imageView;
}


Day *dayIncompleto = nil;

-(IBAction)incompletePressed:(UIButton*)sender{
    NSInteger index = sender.tag;
    NSArray *listaDay = [Day getListDays];
    if(index>=listaDay.count)
    {
        return;
    }
    NSString *dayPath = [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_DAY,[[Day getListDays] objectAtIndex:index]]];
    if(![self fileExistsAtPath:dayPath])
    {
        return;
    }
    NSData *data = [[NSData alloc] initWithContentsOfFile:dayPath];
    dayIncompleto = [Day new];
    dayIncompleto = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *tit = self.isIos7orUpper?@"":@"DOWNLOAD";
    NSString *mex = [NSString stringWithFormat:NSLocalizedString(@"AGGIORNA_GIORNO_MEX", nil),dayIncompleto.date];
    
    UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:tit message:mex delegate:self cancelButtonTitle:NSLocalizedString(@"AGGIORNA_GIORNO_PIU_TARDI", nil) otherButtonTitles:NSLocalizedString(@"AGGIORNA_GIORNO_OK", nil),nil];
    [successAlert setTag:2];
    
    if(self.isIos7orUpper)
    {
    UIView *mask = [[UIView alloc] initWithFrame:CGRectMake(-120, -108, 240, 230)];
    [mask setBackgroundColor:[UIColor clearColor]];

    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(53, 8, 18, 18)];
    [imageView setAlpha:0.9];
    NSString *path = [[NSString alloc] initWithString:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"erro@2x.png"]];
    UIImage *bkgImg = [[UIImage alloc] initWithContentsOfFile:path];
    [imageView setImage:bkgImg];
    
    UIImageView *imageLogo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 25)];
    [imageLogo setContentMode:UIViewContentModeScaleAspectFit];
    [imageLogo setCenter:CGPointMake(mask.frame.size.width/2, 21)];
    NSString *pathLogo = [[NSString alloc] initWithString:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"iBreviaryLOGO.png"]];
    UIImage *imglogo = [[UIImage alloc] initWithContentsOfFile:pathLogo];
    [imageLogo setImage:imglogo];
    [mask addSubview:imageLogo];

    
    [mask addSubview:imageView];
    UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    [view2 setBackgroundColor:[UIColor clearColor]];
    [view2 addSubview:mask];
    [successAlert setValue:view2 forKey:@"accessoryView"];
    }
    [successAlert show];
}


#pragma mark ALERTVIEW delegate
//=========================
// ALERTVIEW delegate
//=========================
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
    switch (buttonIndex) {
        case 0:
            // NSLog(@"NO premuto");
            [reset_label setTextColor:reset_color];
            break;
        case 1:
        {
            // NSLog(@"Conferma Reset");
            // STEP 1
            NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
                // NSLog(@"Conferma Reset: step(1)");
                [self resettaLista];
            }];//fine step1
            
            
            // STEP 2
            NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
                // NSLog(@"Conferma Reset: step(2)");
                [self fineResetta];
            }];//fine step2
            
            [step2 addDependency:step1];
            [[NSOperationQueue new] addOperation:step1];
            [[NSOperationQueue mainQueue] addOperation:step2];
            break;
        }
        default:
            return;
            break;
            }
    }
    
    if(alertView.tag == 2)
    {
        switch (buttonIndex) {
            case 0:
                // NSLog(@"AlertAggiorna:PIU'TARDI");
                dayIncompleto = nil;
                break;
            case 1:
            {
                // NSLog(@"AlertAggiorna:Conferma riscarica");
                NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
                    // NSLog(@"AlertAggiorna:Conferma riscarica");
                    [self riscarica];
                }];//fine block
                [[NSOperationQueue mainQueue] addOperation:step1];
                break;
            }
            default:
                return;
                break;
                }
    }
    
    [super alertView:alertView clickedButtonAtIndex:buttonIndex];

}

#pragma mark aggiorna INCOMPLETE
//=========================
// RISCARICA
//=========================
-(void)riscarica{
    // NSLog(@"Lista: riscarica started");
    if(!self.connesso) { [self noNet]; return; }
      
    [Day scaricaGiornoconData:dayIncompleto.id_date withLang:dayIncompleto.lingua eventFlag:[TestoProprio getTestoProprioWithKey:[dayIncompleto.type isEqualToString:@"generic"]?@"":dayIncompleto.type] completionBlock:^(Day *new){
            if(!new)
            {
            [self noNet];
            return new;
            }
        
            [self AttivaRotella];
        
            NSDateFormatter *f = [NSDateFormatter new];
            [f setDateFormat:@"yyyy-MM-dd"];
            NSDate *data_aggiornata = [f dateFromString:dayIncompleto.id_date];
            [f setDateFormat:@"d/M/yy"];
            if(new.complete)
                //OK!
                [self mostraNotificaDayDownloaded:[f stringFromDate:data_aggiornata]];
            else
                [self mostraNotificaDayERROR_INCOMPLETE:[f stringFromDate:data_aggiornata]];
            [self rimuoviRotella];
        
            //deallocco oggetti utilizzati
            new = nil;
            dayIncompleto = nil;
        
            [self aggiornaListaInDownload];
            return new;
        }];
}

//===========================
// ADVANCED SETTINGS DELEGATE
//===========================
#pragma mark - ADVANCED SETTINGS DELEGATE
- (IBAction)showAdvancedSettings:(UIButton*)sender
{
    // NSLog(@"showInfo_AdvancedSettingsCTRL");

    advancedSettingsCTRL = nil;
	advancedSettingsCTRL = [[AdvancedSettings alloc] initWithDelegate:self];
	advancedSettingsCTRL.view.alpha = 0;
    [self cardRotation:advancedSettings_btn destination:[UIImage imageNamed:@"autoOggi2014H.png"]];

	[self.view addSubview:advancedSettingsCTRL.view];

    
	[UIView beginAnimations:@"AppareadvancedSettingsCTRL" context:NULL];
    [UIView setAnimationDuration:0.5];
	advancedSettingsCTRL.view.alpha = 1;
    [UIView commitAnimations];
}

-(void)removeAdvancedSettings{
    [self performSelectorOnMainThread:@selector(cambiaBtn) withObject:nil waitUntilDone:YES];
    // NSLog(@"FineadvancedSettingsCTRL");
    advancedSettingsCTRL.view.alpha = 1;
    [UIView beginAnimations:@"advancedSettingsCTRL_disappear" context:NULL];
    [UIView setAnimationDuration:0.4];
    advancedSettingsCTRL.view.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineAdvancedSettingsCTRL)];
    [UIView commitAnimations];
}

-(void)cambiaBtn{
    [self cardRotation:advancedSettings_btn destination:[UIImage imageNamed:@"autoOggi2014.png"]];
}

-(void)fineAdvancedSettingsCTRL{
    [advancedSettingsCTRL.view removeFromSuperview];
    advancedSettingsCTRL =  nil;
}

//===========================
// TUTORIAL DELEGATE
//===========================
#pragma mark - TUTORIAL DELEGATE
- (IBAction)showTutorial:(UIButton*)sender{
        // NSLog(@"showTutorial");
        [self rotateImage:tutorial_img withDuration:0.6 onY_axis:YES];

        tutorialCTRL = [[Tutorial alloc] initWithDelegate:self];
        CGRect frame = self.view.frame;
        if(!self.isIos7orUpper && !self.isIphone5)
        {
            frame.origin.y -= 20;
            frame.size.height +=20;
            [tutorialCTRL.view setFrame:frame];
            frame.origin.y += 20;

        }

        blurView = [[FXBlurView alloc] initWithFrame:frame];
    // NSLog(@"FRAME:%@",NSStringFromCGRect(frame));

        [self.blurView setDynamic:NO];
        [self.blurView setTintColor:[UIColor brownColor]];
        [self.blurView addSubview:tutorialCTRL.view];
        self.blurView.layer.transform = CATransform3DMakeScale(0.02, 0.02, 1.0);
        if(self.isIos7orUpper)[self.blurView setCenter:CGPointMake(sender.center.x-7, sender.center.y+2)];
        [self.blurView setAlpha:0];

        [self.view addSubview:self.blurView];

        [UIView beginAnimations:@"AppareTutorialCTRL" context:NULL];
        [UIView setAnimationDuration:0.1];
        [self.blurView setAlpha:1];
        [UIView commitAnimations];
    
    
    [UIView animateWithDuration:0.4 animations:^{
                                                self.blurView.layer.transform = CATransform3DMakeScale(1, 1, 1.0);
                                                if(self.isIos7orUpper)[self.blurView setCenter:self.view.center];

                                                //[self.blurView setFrame:to];
                                                }
                         completion:^(BOOL finished)    {
                                                        // NSLog(@"Fine Animation");
                                                        }];

}


-(void)removeTutorial{
    // NSLog(@"removeTutorial");
    [self rotateImage:tutorial_img withDuration:0.6 onY_axis:YES];

    [UIView animateWithDuration:0.4 animations:^{
                                                self.blurView.layer.transform = CATransform3DMakeScale(0.01, 0.01, 1.0);
                                                [self.blurView setCenter:CGPointMake(tutorial_btn.center.x-7, tutorial_btn.center.y+2)];

                                                }
                     completion:^(BOOL finished)    {
                         [UIView beginAnimations:@"SconpareTutorialCTRL" context:NULL];
                         [UIView setAnimationDuration:0.1];
                         [UIView setAnimationDelegate:self.blurView];
                         [UIView setAnimationDidStopSelector:@selector(removeFromSuperview)];
                         [self.blurView setAlpha:0];
                         [UIView commitAnimations];
                     }];


}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return [[DDMenuController sharedInstance] supportedInterfaceOrientations];
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    // NSLog(@"will Rotate");
    [[DDMenuController sharedInstance] willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    // NSLog(@"did Rotate");
    [[DDMenuController sharedInstance] didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}



#pragma mark -
#pragma mark ERROR CLASS
//=========================
// ERROR CLASS
//=========================
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTitle:nil];
    [self setLogoVelina_lingua:nil];
    [self setBackPicker:nil];
    [self setDay_ctrl_view:nil];
    [self setWeek_label_download:nil];
    [self setBackPicker_lingua:nil];
    [self setReset_btn:nil];
    [self setReset_label:nil];
     [super viewDidUnload];
}


@end
