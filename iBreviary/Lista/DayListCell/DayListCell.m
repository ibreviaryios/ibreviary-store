//
//  DayListCell.m
//  DayListCell
//
//  Created by Leonardo Parenti on 14/02/16.
//  Copyright © 2016 iBreviary. All rights reserved.
//

#import "DayListCell.h"
#import "TestoProprio.h"

#define MARGIN_LABEL 5.f

void (^animation_block)(void);
void (^completition_block)(BOOL);


@interface DayListCell()

//Label property
@property (nonatomic,retain) IBOutlet UIView *area_header;
@property (nonatomic,retain) IBOutlet UILabel *label_titolo;
@property (nonatomic,retain) IBOutlet UILabel *label_value;
@property (nonatomic,retain) IBOutlet UILabel *label_description;


//Progress property
@property (nonatomic) CGFloat progress;

@property(nonatomic,retain) IBOutlet UIView *under_progress_view;
@property(nonatomic,retain) IBOutlet UIView *border;
@property(nonatomic,retain) IBOutlet UIView *progress_view;
@property(nonatomic,retain) IBOutlet UIImageView *icon;
@property(nonatomic,retain) IBOutlet UILabel *label_progress_description;

@property(nonatomic,retain) UIColor *underProgressColor;
@property(nonatomic,retain) UIColor *progressColor;

@property (nonatomic,retain) IBOutlet UIView *area_infinite;
@property (nonatomic,retain) IBOutlet UIImageView *icon_infinite;
@property (nonatomic,retain) IBOutlet UIImageView *icon_infinite2;


@end


@implementation DayListCell

#pragma mark -
#pragma mark init
//=========================
// INIT
//=========================
-(id)init{
    self = [super init];
    if (self) {
    }
    return self;
}

+(CGFloat)height{
    return 60.f;
}

#pragma mark -
#pragma mark AWAKE
//=========================
// AWAKE
//=========================
- (void)awakeFromNib {
    //DDLogDebug(@"awakeFromNib");
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    [self inizializeView];
    
}

#pragma mark -
#pragma mark PREPARE REUSE
//=========================
// PREPARE REUSE
//=========================
-(void)prepareForReuse{
    [self inizializeView];
}


-(void)inizializeView{
    //LOGLEO(@"inizializing %@",self);
    self.progress = 0;
    
//    self.border;
//    self.progress_view;
//    self.icon;
    
    [self.label_progress_description setText:@""];
    [self.label_progress_description setHidden:YES];
    
    [self.area_infinite setHidden:YES];
    [self.progress_view setHidden:NO];

    self.underProgressColor = [UIColor clearColor];
    self.progressColor = [UIColor clearColor];

}

#pragma mark -
#pragma mark SET&GET
//=========================
// SET&GET
//=========================

//-(void)setOpzione:(id)opt{
//    [_opzione release];
//    _opzione = [opt retain];
//    _offerta = nil;
//    [self populate];
//}


#pragma mark -
#pragma mark POPULATE
//=========================
// POPULATE
//=========================
-(void)populate{
    
    [self initialize];
    //populate view
    
    //Populate TITLE TYPE

}

#pragma mark - Private

- (void)initialize {
    [self.border.layer setBorderWidth:2];
    [self.border.layer setMasksToBounds:YES];
    [self.border setBackgroundColor:[UIColor clearColor]];
    [self.border.layer setCornerRadius:self.border.frame.size.height/2.f];
    [self.border setClipsToBounds:YES];
    CGRect f = self.progress_view.frame;
    f.size.width = f.size.height;
    [self.progress_view setFrame:f];
    [self.icon setFrame:self.progress_view.bounds];
    [self.progress_view setBackgroundColor:[UIColor greenColor]];
    [self.progress_view.layer setCornerRadius:self.progress_view.frame.size.height/2.f];
    [self.progress_view setClipsToBounds:YES];

    [self.under_progress_view.layer setCornerRadius:self.progress_view.layer.cornerRadius];
    [self.under_progress_view setClipsToBounds:YES];

    UIFont *font = self.label_progress_description.font;
    CGFloat size = self.frame.size.height/2.f;
    if(size>15) size = 15;
    [self.label_progress_description setFont:[UIFont fontWithName:font.familyName size:size]];
}


#pragma mark -
#pragma mark DEALLOC
//=========================
// DEALLOC
//=========================
-(void)dealloc{
    
    //obj
    
    //Label property
    self.area_header = nil;
    self.label_titolo = nil;
    self.label_value = nil;
    self.label_description = nil;

    //Progress property
    self.under_progress_view = nil;
    self.border = nil;
    self.progress_view = nil;
    self.icon  = nil;
    self.label_progress_description = nil;
    self.underProgressColor = nil;
    self.progressColor = nil;
    
    //infinite
    self.icon_infinite = nil;
    self.icon_infinite2 = nil;
    self.area_infinite = nil;
}

@end


