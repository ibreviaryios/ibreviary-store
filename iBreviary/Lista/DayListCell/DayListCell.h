//
//  DayListCell.h
//  DayListCell
//
//  Created by Leonardo Parenti on 14/02/16.
//  Copyright © 2016 iBreviary. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Progress bar similar to the one in iOS's launching screen.
 */
@interface DayListCell : UITableViewCell



+(CGFloat)height;

-(id)initWithFrame:(CGRect)aFrame
          progress:(CGFloat)p
              text:(NSString*)t
       borderColor:(UIColor*)bc
         textColor:(UIColor*)tc
        gressColor:(UIColor*)pc
             image:(UIImage*)img;

-(void)startProgressAnimated;
-(void)reset;

@end
