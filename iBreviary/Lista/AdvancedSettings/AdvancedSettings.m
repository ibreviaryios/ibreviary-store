//
//  AdvancedSettings.m
//  iBreviary_PRO
//
//  Created by ileyo on 07/12/10.
//  Copyright 2010 leyo.breviario. All rights reserved.
//

#import "AdvancedSettings.h"
#import "WebBrowser.h"
#import "Home.h"
#import "Define.h"

@implementation AdvancedSettings

@synthesize button_feature_1, delegate;
@synthesize button_feature_2;
@synthesize button_feature_3;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<AdvancedSettingsDelegate>*)d
{
    NSString *nibNameOrNil = @"AdvancedSettings5";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
    }
    return self;
}


#pragma mark -
#pragma mark AZIONI
//=========================
// AZIONI
//=========================
-(IBAction)switchFeature:(UIButton*)sender{
    // NSLog(@"switchFeature");
    if(sender.tag == 1)
    {
        // NSLog(@"AUTODOWNLOAD_OGGI");
        [self.defaults setBool:![self.defaults boolForKey:AUTODOWNLOAD_OGGI] forKey:AUTODOWNLOAD_OGGI];
        NSDateFormatter *f = [NSDateFormatter new];
        [f setDateFormat:@"yyyy-MM-dd"];
        NSString *date_string = [f stringFromDate:[NSDate date]];
        [self.defaults setObject:date_string forKey:AUTODOWNLOAD_OGGI_DATE];
        [self.defaults synchronize];
        [sender setSelected:[self.defaults boolForKey:AUTODOWNLOAD_OGGI]];
    }
    else if(sender.tag == 2)
    {
        // NSLog(@"COMPIETA IERI");
        [self.defaults setBool:![self.defaults boolForKey:COMPLINE_YESTERDAY] forKey:COMPLINE_YESTERDAY];
        [self.defaults synchronize];
        [sender setSelected:[self.defaults boolForKey:COMPLINE_YESTERDAY]];
        [(UINavigationController*)[[DDMenuController sharedInstance] leftViewController] popToRootViewControllerAnimated:NO];
    }
    else if(sender.tag == 3)
    {
        // NSLog(@"SOLO FULLSCREEN");
        [self.defaults setBool:![self.defaults boolForKey:SOLO_FULLSCREEN] forKey:SOLO_FULLSCREEN];
        [self.defaults synchronize];
        [sender setSelected:[self.defaults boolForKey:SOLO_FULLSCREEN]];
        if(self.isIpad) [[Home sharedInstance] checkFull];
    }
    else if(sender.tag == 4)
    {
        // NSLog(@"CHANGE DEVICE LANGUAGE");
        //[[NSUserDefaults standardUserDefaults] setObject: [NSArray arrayWithObjects:@"it", nil] forKey:@"AppleLanguages"];
    }
    
    //[delegate performSelector:@selector(removeAdvancedSettings) withObject:nil afterDelay:0.2];
}

#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
-(IBAction)FineAdvancedSettings:(UIButton*)sender{
    // NSLog(@"FineAdvanced");
    [delegate removeAdvancedSettings];
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    // NSLog(@"AdvancedSettings DidLoad");
    [self gui];
    [button_feature_1 setSelected:[self.defaults boolForKey:AUTODOWNLOAD_OGGI]];
    [button_feature_2 setSelected:[self.defaults boolForKey:COMPLINE_YESTERDAY]];
    [button_feature_3 setSelected:[self.defaults boolForKey:SOLO_FULLSCREEN]];


}

-(void)gui{
    [button_feature_1 setTitle:NSLocalizedString(@"auto_download_day", nil) forState:UIControlStateNormal];
    [button_feature_2 setTitle:NSLocalizedString(@"Compline_yesterday", nil) forState:UIControlStateNormal];
    [button_feature_3 setTitle:NSLocalizedString(@"Solo_fullScreen", nil) forState:UIControlStateNormal];
}



#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}



@end
