//
//  AdvancedSettings.h
//  iBreviary_PRO
//
//  Created by ileyo on 07/12/10.
//  Copyright 2010 leyo.breviario. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "BaseController.h"

@protocol AdvancedSettingsDelegate <NSObject>
- (void)removeAdvancedSettings;
- (IBAction)showAdvancedSettings:(UIButton*)sender;

@end


@interface AdvancedSettings : BaseController

    
@property (nonatomic, strong) BaseController<AdvancedSettingsDelegate> *delegate;
@property (nonatomic, strong) IBOutlet UIButton *button_feature_1;
@property (nonatomic, strong) IBOutlet UIButton *button_feature_2;
@property (nonatomic, strong) IBOutlet UIButton *button_feature_3;


- (id)initWithDelegate:(BaseController<AdvancedSettingsDelegate>*)d;

@end
