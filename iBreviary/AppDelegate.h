//
//  AppDelegate.h
//  iBreviary
//
//  Created by leyo on 22/01/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersistentStack.h"

@class Home, DDMenuController, Settings;
;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) PersistentStack* persistentStack;


//=========================
// METODI DI CLASSE
//=========================
+ (AppDelegate *)sharedAppDelegate;
+ (void)saveContext;
+ (void)savePrivateContext;
+ (NSManagedObjectContext *)mainManagedObjectContext;
+ (NSManagedObjectContext *)privateQueueContext;

//debug iCloud destroy
+(void)debugDeleteiCLoud:(UIButton*)sender;

//=========================
// VARIABILI DI SISTEMA
//=========================
@property (nonatomic, readonly) BOOL isIphone;
@property (nonatomic, readonly) BOOL isIphone5;
@property (nonatomic, readonly) NSUserDefaults *defaults;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) Home *viewController;
@property (strong, nonatomic) DDMenuController *menuController;
@property (strong, nonatomic) Settings *rightMenu;


@end
