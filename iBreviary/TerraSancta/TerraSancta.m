//
//  TerraSancta.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "TerraSancta.h"
#import "WebBrowser.h"

@implementation TerraSancta

@synthesize label_dona,labelV, textV,delegate,close;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)init
{
    //iPhone4
    NSString *nibNameOrNil = @"TerraSancta";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"TerraSancta_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"TerraSancta5";
    if(IS_IPHONE_6) nibNameOrNil = @"TerraSancta6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"TerraSancta6Plus";

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark -
#pragma mark AZIONI
//=========================
// AZIONI
//=========================
- (IBAction)azioneDona:(UIButton*)sender
{
    [delegate showTSWebSite];
}


#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
-(IBAction)FineCustodia:(UIButton*)sender{
    [delegate removeTS];
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.isIpad)
    {
        [self.view.layer setCornerRadius:10];
        [self.view.layer setMasksToBounds:YES];
    }
    //[labelV setFont:[UIFont fontWithName:FONT_HOEFLER_ITALIC size:17]];
	//[textV setFont:[UIFont fontWithName:FONT_HOEFLER_ITALIC size:17]];
	
	//SET PULSANTE
	[label_dona setText:NSLocalizedString(@"PulsanteDona", @"DONATE")];
    
	// SET DEL TESTO
	labelV.text = NSLocalizedString(@"ProTerraSanctaMEX", nil);
	textV.text = NSLocalizedString(@"ProTerraSanctaMEX", nil);
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     if(!self.isIos7orUpper){
         CGRect fr = self.view.frame;
         fr.origin.y =-20;
         [self.view setFrame:fr];
         fr = [[self.view.subviews objectAtIndex:0] frame];
         fr.origin.y = -20;[[self.view.subviews objectAtIndex:0] setFrame:fr];
     }
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}



#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}



@end
