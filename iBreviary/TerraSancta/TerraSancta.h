//
//  TerraSancta.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"

@protocol TerraSanctaDelegate <NSObject>
- (void)removeTS;
- (void)showTSWebSite;

@end


@interface TerraSancta : BaseController

    
@property (nonatomic, strong) BaseController<TerraSanctaDelegate> *delegate;
@property (nonatomic, strong) IBOutlet UILabel *label_dona;
@property (nonatomic, strong) IBOutlet UITextView *textV;
@property (nonatomic, strong) IBOutlet UILabel *labelV;
@property (nonatomic, strong) IBOutlet UIButton *close;


@end
