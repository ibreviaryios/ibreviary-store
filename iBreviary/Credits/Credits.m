//
//  Credits.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "Credits.h"
#import "WebBrowser.h"

@implementation Credits

@synthesize textV,delegate,webContent,close;

#define TIMER_CREDITS 0.3
NSInteger h = 0;
NSInteger maxH = 0;
BOOL StopTimer = NO;
BOOL PausaTimer = NO;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<CreditsDelegate>*)d
{
    //iPhone4
    NSString *nibNameOrNil = @"Credits";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"Credits_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"Credits5";
    if(IS_IPHONE_6) nibNameOrNil = @"Credits6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"Lista6Plus";
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        if(self.isIpad) [self.view setCenter:d.view.center];
    }
    return self;
}



#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
-(IBAction)FineCredits:(UIButton*)sender{
    StopTimer = YES;
    [delegate removeCredits];
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    //LOG(@"Credits DidLoad");
    if(self.isIpad)
    {
        [self.view.layer setCornerRadius:10];
        [self.view.layer setMasksToBounds:YES];
    }
    [webContent setDelegate:self];
    
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
    
    [self caricaTesto];
}

#pragma mark - WEB-DELEGATE
//=========================
// WEB-DELEGATE
//=========================
- (BOOL)webView:(UIWebView *)w shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    maxH = [[webView stringByEvaluatingJavaScriptFromString:@"$(document).height();"] integerValue];
    //LOG(@"webViewDidFinishLoad H:%i",(int)maxH);
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    //LOG(@"scrollViewWillBeginDragging");
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //LOG(@"scrollViewDidScroll");
}




-(void)caricaTesto{
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    
    NSString *credits_html=[NSString stringWithContentsOfFile:[NSString stringWithFormat:self.isIpad?@"%@/credits_ipad.html":@"%@/credits.html",path] encoding:NSUTF8StringEncoding error:nil];
    
    
    [webContent loadHTMLString:credits_html baseURL:baseURL];
}


- (void)startScrolling{
    
    if(StopTimer) {StopTimer = NO; h = 0; return; }
    if(PausaTimer) {return;}
    // Updates the variable h, adding 100 (put your own value here!)
    h = webContent.scrollView.contentOffset.y;

    h += 7;
    // NSLog(@"webContent.scrollView.contentOffset.y:%li",h);
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    //This makes the scrollView scroll to the desired position
    if (webContent.scrollView.contentOffset.y>=maxH)
        {
         h = -(screenSize.height);
        [webContent.scrollView setContentOffset:CGPointMake(0, h) animated:NO];
        [self startScrolling];
        return;
        }
    [UIView beginAnimations:@"Scrolla" context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(startScrolling)];
    [UIView setAnimationDuration:TIMER_CREDITS];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    
    [webContent.scrollView setContentOffset:CGPointMake(0, h)];
    
    [UIView commitAnimations];
    
}


#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}



@end
