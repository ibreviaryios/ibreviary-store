//
//  MenuItem.h
//
//  Created by Leyo 4/01/2016.
//  Copyright (c) 2016 Leonardo Parenti.
//

#import <UIKit/UIKit.h>


@protocol MenuListDelegate

@required
-(void)didSelectIndex:(MenuButtonType)type;

@end

@interface MenuItem : UIView

@property (nonatomic, assign) MenuButtonType type;


@end

@interface MenuList : NSObject

@property (nonatomic, readonly) BOOL isShowed;


-(void)addMenuWithDelegate:(UIViewController<MenuListDelegate>*)d
              andItemArray:(NSArray*)dataArray;

-(void)toggleMenu:(BOOL)show;

-(void)removeRotella;
-(void)attivaRotella;

@end
