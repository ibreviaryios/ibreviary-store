//
//  MenuItem.h
//
//  Created by Leyo 4/01/2016.
//  Copyright (c) 2016 Leonardo Parenti.
//

#define DID_PRESS_BUTTON @"DID_PRESS_BUTTON"
#import "MenuItem.h"
@interface Button : UIButton

@end
@implementation Button

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    self.titleLabel.textColor = highlighted?[UIColor whiteColor]:[UIColor grayColor];
    
}

@end
@interface MenuItem ()

@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) Button *select;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) UIImage *iconImage;
@property (nonatomic, strong) id object;
@property (nonatomic, copy) NSString *text;


@property (nonatomic, assign) CGFloat paddingLeft;

//Menu Configurator
@end

@implementation MenuItem

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    _paddingLeft = 5;
    [self initView];
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    [self.bgView setFrame:self.bounds];
    
    [self updateLayout];
}

-(void)didMoveToSuperview{
    [super didMoveToSuperview];
    //NSLog(@"Item did Add:%@",self);
}

- (void)initView
{
    self.bgView = [[UIView alloc] init];
    self.bgView.userInteractionEnabled = NO;
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.layer.shadowColor = [UIColor grayColor].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0, 0);
    self.bgView.layer.shadowOpacity = 0.2;
    self.bgView.layer.shouldRasterize = YES;
    [self.bgView setFrame:self.bounds];
    [self addSubview:self.bgView];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.contentMode = UIViewContentModeCenter;
    [self addSubview:self.iconImageView];
    
    
    self.select = [[Button alloc] initWithFrame:self.bgView.bounds];
    self.select.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.select setBackgroundColor:[UIColor clearColor]];
    [self.select addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.select setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.select setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self addSubview:self.select];

    
    [self updateLayout];
    //NSLog(@"initView:%@",self);
}

- (void)setIconImage:(UIImage *)iconImage
{
    _iconImage = iconImage;
    [self.iconImageView setImage:self.iconImage];
    
    [self updateLayout];
}

- (IBAction)buttonPressed:(Button*)sender{
    sender.highlighted = YES;
    NSDictionary *d = @{@"index":[NSNumber numberWithInteger:self.index]};
    [[NSNotificationCenter defaultCenter] postNotificationName:DID_PRESS_BUTTON
                                                        object:nil
                                                      userInfo:d];
    sender.highlighted = NO;
}

- (void)updateLayout
{
    
    CGFloat selfWidth = CGRectGetWidth(self.bounds);
    CGFloat selfHeight = CGRectGetHeight(self.bounds);
    
    [self.iconImageView setFrame:CGRectMake(self.paddingLeft, 0, selfHeight, selfHeight)];
    if (self.iconImage) {
        self.select.titleEdgeInsets = UIEdgeInsetsMake(0, CGRectGetMaxX(self.iconImageView.frame), 0, 0);
//        [self.select setFrame:CGRectMake(CGRectGetMaxX(self.iconImageView.frame), 0, selfWidth - CGRectGetMaxX(self.iconImageView.frame), selfHeight)];
    } else {
        self.select.titleEdgeInsets = UIEdgeInsetsMake(0, self.paddingLeft, 0, 0);
    }
    
   // NSLog(@"updateLayout:%@",self);
}

- (void)setPaddingLeft:(CGFloat)paddingLeft
{
    _paddingLeft = paddingLeft;
    
    [self updateLayout];
}

- (void)setObject:(id)object
{
    _object = object;
}

- (void)setText:(NSString *)text
{
    _text = text;
    [self.select setTitle:self.text forState:UIControlStateNormal];
}

- (id)copyWithZone:(NSZone *)zone
{
    MenuItem *itemCopy = [[MenuItem alloc] init];
    
    itemCopy.index = _index;
    itemCopy.iconImage = _iconImage;
    itemCopy.object = _object;
    itemCopy.text = _text;
    itemCopy.paddingLeft = _paddingLeft;
    
    return itemCopy;
}

@end

@interface MenuList()

@property(nonatomic, strong) NSMutableArray *dropdownItems;
@property (nonatomic, assign) CGFloat offsetX;
@property (nonatomic, assign) CGFloat offsetY;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, assign) CGRect oldFrame;
@property (nonatomic, assign) CGRect originalFrame;
@property (nonatomic, assign) CGSize itemSize;
@property (nonatomic, assign) CGFloat slidingInOffset;
@property (nonatomic, assign) CGFloat gutterY;
@property (nonatomic, copy) void (^selectedItemChangeBlock)(NSInteger selectedIndex);

@property(nonatomic, assign) UIViewController<MenuListDelegate> *delegate;
@property(nonatomic, strong) UIView *view;
@property(nonatomic, strong) UIView *blurView;
@property(nonatomic, strong) UIActivityIndicatorView *rotella;



@end

@implementation MenuList

-(void)didSelectItem:(NSNotification*)n{
    //NSLog(@"%@",[n userInfo]);
    NSNumber *index = [[n userInfo] objectForKey:@"index"];
    MenuItem* i = [self.dropdownItems objectAtIndex:index.integerValue];
    if(i.type==MenuType_FontA || i.type==MenuType_FontB || i.type==MenuType_FontC ){
        [self attivaRotella];
    }
    if(self.delegate){
        if([self.delegate respondsToSelector:@selector(didSelectIndex:)])
                                            [self.delegate didSelectIndex:i.type];
    }
    else{
        [self removeRotella];
    }
    
}

-(void)removeRotella{
    // NSLog(@"removeRotellaSHARE!");
    [self.rotella stopAnimating];
    [self.rotella setHidden:YES];
}

-(void)attivaRotella{
    // NSLog(@"attivaRotellaSHARE!");
    [self.rotella startAnimating];
    [self.rotella setHidden:NO];
}

-(void)addMenuWithDelegate:(UIViewController<MenuListDelegate>*)d
              andItemArray:(NSArray*)dataArray{
    
    if(!dataArray){
        dataArray = @[@{@"image":@"font_a.png",@"title":@"Normale",@"type":[NSNumber numberWithInt:MenuType_FontA]},
                      @{@"image":@"font_b.png",@"title":@"Medio",@"type":[NSNumber numberWithInt:MenuType_FontB]},
                      @{@"image":@"font_c.png",@"title":@"Grande",@"type":[NSNumber numberWithInt:MenuType_FontC]},
                      @{@"image":@"menu_close.png",@"title":@"Chiudi Menu",@"type":[NSNumber numberWithInt:MenuType_CloseMenu]},
                      @{@"title":@"Chiudi Libro",@"type":[NSNumber numberWithInt:MenuType_CloseFeature]}];
        
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectItem:) name:DID_PRESS_BUTTON object:nil];

    
    self.slidingInOffset = -1;
    self.gutterY = 0;
    self.selectedIndex = -1;
    self.itemSize = CGSizeMake(200, 45);
    CGRect frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    if(IS_IPAD) frame.size.height = 1024;
    if(IS_IPAD_PRO){
        NSLog(@"iPAD_PRO !!");
        frame.size.height = 1366;
        if(frame.size.width!=1366){
            CGFloat xmargin = 20.f;
            frame.origin.x = - (((1336 - frame.size.width)/2.f) + xmargin);
            frame.size.width = 1336+(2*xmargin);
        }
    }
    self.offsetX = (frame.size.width - self.itemSize.width) / 2.f;
    self.offsetY = (frame.size.height - (self.itemSize.height*dataArray.count))/2.0;
    self.delegate = d;
    
    //NSLog(@"offsetX:%f",self.offsetX);

   
    self.view = [[UIView alloc] initWithFrame:frame];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;

    [self.view setBackgroundColor:[UIColorFromRGB(0X31251b) colorWithAlphaComponent:IS_IPAD?0.89:0.92]];

//    self.blurView = [[UIView alloc] initWithFrame:self.delegate.view.bounds];
//    [self.blurView addSubview:self.view];
//    [self blurView:self.blurView withEffectStyle:UIBlurEffectStyleLight];
//    [self.blurView setAlpha:0.95];

    
    UIButton *b = [[UIButton alloc] initWithFrame:self.view.bounds];
    [b setBackgroundColor:[UIColor clearColor]];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;

    [b addTarget:self action:@selector(hideMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:b];

    
    self.dropdownItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < dataArray.count; i++) {
        NSDictionary *dict = dataArray[i];
        
        MenuItem *item = [[MenuItem alloc] initWithFrame:(CGRect){self.offsetX,self.offsetY+(i*self.itemSize.height), self.itemSize}];
        item.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;

        [item setIndex:i];
        if(dict[@"image"]){
            [item setIconImage:[UIImage imageNamed:dict[@"image"]]];
        }
        [item setText:dict[@"title"]];
        if(dict[@"type"]){
            [item setType:(MenuButtonType)[dict[@"type"] intValue]];
        }
        [self.dropdownItems addObject:item];
        //NSLog(@"item:%@",NSStringFromCGRect(item.frame));
    }
    
    for(MenuItem *row in self.dropdownItems){
        row.alpha = 0;
        [row setFrame:[self frameOnFoldForItem:row]];
        [self.view addSubview:row];
    }
    
    self.rotella = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:IS_IPAD?UIActivityIndicatorViewStyleWhiteLarge:UIActivityIndicatorViewStyleWhite];
    [self.rotella setColor:[UIColor whiteColor]];
    //[rotellaDownload setTransform:CGAffineTransformMakeScale(1.6f, 1.6f)];
    [self.rotella setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)];
    [self.view addSubview:self.rotella];
    [self removeRotella];
    
    [self.view setHidden:YES];
    [d.view addSubview:self.view];
}

#pragma mark -
#pragma mark addBLUR
//===============================
// addBLUR
//===============================
-(void)blurView:(UIView*)view withEffectStyle:(UIBlurEffectStyle)style
{
    if(self.supportBlur){
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:style];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
        effectView.frame = view.bounds;
        
        [view addSubview:effectView];
    }
    else{
        //NO BLUR SUPPORT
    }
}

-(void)toggleMenu:(BOOL)show{
    if(show){
        [self showMenu];
    }
    else{
        [self hideMenu];
    }
}
 
- (void)showMenu
{
    for (int i = (int)self.dropdownItems.count - 1; i >= 0; i--) {
        MenuItem *item = self.dropdownItems[i];
        // first item move first
        CGFloat delay = 0.1;
        delay += 0.1 * (i%2?i:i-1);

        [UIView animateWithDuration:IS_IPAD?0.5:0.4
                              delay:delay
             usingSpringWithDamping:0.5
              initialSpringVelocity:2.0
                            options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             if(self.view.hidden){
                                 [self.view setHidden:NO];
                                }
                             [self setUpExpandItem:item];
                         }
                         completion:^(BOOL finished) {
                             //FIN
                         }];

    }
    
    if(self.view.hidden){
        [self.view setAlpha:0.0];
        [self.view setHidden:NO];
        [UIView animateWithDuration:IS_IPAD?0.3:0.3
                              delay:0.0
             usingSpringWithDamping:0.5
              initialSpringVelocity:2.0
                            options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             [self.view setAlpha:1.0];
                         }
                         completion:^(BOOL finished) {
                             
                         }];
        
    }
    
}


-(BOOL)isShowed{
    return !self.view.hidden;
}

- (void)hideMenu
{
    // fold the view
    if(self.view.hidden){
        return;
    }
    
    CGFloat duration = IS_IPAD?0.3:0.4;

    __block CGFloat hideMenuDuration = 0.0;

    for (int i = (int)self.dropdownItems.count - 1; i >= 0; i--) {
        MenuItem *item = self.dropdownItems[i];
        CGFloat delay = 0.1;
        delay += 0.1 * (i%2?i:i-1);

        
        [UIView animateWithDuration:duration
                                  delay:delay
                 usingSpringWithDamping:1.0
                  initialSpringVelocity:2.0
                                options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 [self setUpFoldItem:item];
            } completion:^(BOOL finished) {
                
            }];
        
        if(i==(int)self.dropdownItems.count - 1){
            //NSLog(@"duration(%f)+delay(%f)+0.1",duration,delay);
            hideMenuDuration = duration+delay+0.1;
        }
    }
    [UIView animateWithDuration:hideMenuDuration
                     animations:^{
                         [self.view setAlpha:0];
                     }
                     completion:^(BOOL finished) {
                         [self.view setHidden:YES];
                         [self.view setAlpha:1];
                     }];

}

- (void)setUpExpandItem:(MenuItem*)item
{
    // set alpha for slidingIn
    
    // set frame (MUST before rotation reset)
    [item setFrame:[self frameOnExpandForItem:item]];
    
    // set rotate
    item.transform = CGAffineTransformMakeRotation(0); //[self transformOnExpandForItem:item];
    item.alpha = 1.0;
}

- (void)setUpFoldItem:(MenuItem*)item
{
    // reset rotate
    item.transform = CGAffineTransformMakeRotation(0);
    
    // set frame (MUST after rotation reset)
    [item setFrame:[self frameOnFoldForItem:item]];
    //NSLog(@"Fold item.f:%@",NSStringFromCGRect(item.frame));

    // set alpha for slidingIn
    item.alpha = 0.0;
}

- (CGRect)frameOnFoldForItem:(MenuItem*)item
{
    CGFloat x = 0;
    CGFloat y = item.frame.origin.y;
    CGFloat width = self.itemSize.width;
    CGFloat height = self.itemSize.height;
    
    x = (item.index % 2 != 0)?0:self.view.frame.size.width-self.itemSize.width;
    
    if(IS_IPAD)
        x = (item.index % 2 != 0)?self.offsetX-self.itemSize.width:self.offsetX+self.itemSize.width;
    
    y = (item.index + 1) * (height + self.gutterY);
    
    
    //    CGFloat buttonHeight = CGRectGetHeight(self.menuButton.frame);
    //    y = self.frame.size.height - buttonHeight - y;
    //    NSLog(@"bHeight: %f, height: %f", buttonHeight, self.frame.size.height);
    
    
    return CGRectMake(x, y, width, height);
}

- (CGRect)frameOnExpandForItem:(MenuItem*)item
{
    CGFloat x = self.offsetX;
    CGFloat y = item.frame.origin.y;
    CGFloat width = self.itemSize.width;
    CGFloat height = self.itemSize.height;
    
    
    return CGRectMake(x, y, width, height);
}

- (CGAffineTransform)transformOnExpandForItem:(MenuItem*)item
{
    CGFloat angle = 0;
    return CGAffineTransformMakeRotation(angle);
}

@end
