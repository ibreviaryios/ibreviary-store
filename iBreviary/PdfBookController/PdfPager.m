//
//  PdfPager.m
//  ZoomingPDFViewer
//
//  Created by Leonardo Parenti on 29/12/15.
//  Copyright © 2015 Apple DTS. All rights reserved.
//

#import "PdfPager.h"
#import "ModelController.h"
#import "DataViewController.h"
#import "DDMenuController.h"
#import "Home.h"
#import "MenuItem.h"
#import "TNCheckBoxGroup.h"




#define TUTORIAL_MODALITA_BOOK @"TUTORIAL_MODALITA_BOOKCustom"


//====================================================================================================
// PAGEVIEWCONTROLLER
//====================================================================================================
@interface PVCtrl()


@end

@implementation PVCtrl

-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(BOOL)shouldAutorotate{
    return IS_IPAD;
}

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    if(IS_IPAD){
        return UIInterfaceOrientationMaskAll;
    }
    
    return UIInterfaceOrientationMaskPortrait;
}

//-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
//    
//}
//
//-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
//
//}

-(void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    NSLog(@"PdfPager %@/n%@/",self,[self viewControllers]);
    for(id r in self.viewControllers)
        NSLog(@"r:%@",r);
}


-(void)viewDidLoad{
    [super viewDidLoad];
}

@end

//====================================================================================================
// PDF PAGER
//====================================================================================================
@interface PdfPager()<MenuListDelegate,UIDocumentInteractionControllerDelegate,CustomIOSAlertViewDelegate>

@property (strong, nonatomic) PVCtrl *pageViewController;
@property (readonly, strong, nonatomic) ModelController *modelController;
@property (strong, nonatomic) NSString *url;

@property(nonatomic,strong) IBOutlet UIButton *exit_btn;
@property(nonatomic,strong) IBOutlet UIView *pagerView;
@property(nonatomic,strong) IBOutlet UIView *navbar;

@property(nonatomic,strong) MenuList *context_menu;
@property(nonatomic,strong) UIDocumentInteractionController *share_ctrl;

@end

@implementation PdfPager

@synthesize modelController = _modelController;

#pragma mark -
#pragma mark init
//=========================
// INIT
//=========================

- (instancetype)initWithPdfUrl:(NSString*)u
{
    NSString *nibNameOrNil = @"PdfPager";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if(self){
        self.url = u;
    }
    return self;
    
}
#pragma mark -
#pragma mark SHOW/HIDE
//=========================
// SHOW/HIDE
//=========================
+(PdfPager*)presentPDFPagerWithUrlFile:(NSString*)u{
    PdfPager *p = [[PdfPager alloc] initWithPdfUrl:u];
    UIViewController *root = [[(AppDelegate *)[[UIApplication sharedApplication] delegate] window] rootViewController];
    [root presentViewController:p
                       animated:YES
                     completion:^{
                         NSLog(@"PdfPager:%s",__PRETTY_FUNCTION__);
                     }];
    return p;
}

#pragma mark -
#pragma mark STATUSBAR
//=========================
// STATUSBAR
//=========================
-(BOOL)prefersStatusBarHidden{
    return YES;
}

#pragma mark -
#pragma mark ROTATION
//=========================
// ROTATION
//=========================
-(BOOL)shouldAutorotate{
    return IS_IPAD;
}

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    if(IS_IPAD){
        return UIInterfaceOrientationMaskAll;
    }
    
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark -
#pragma mark viewDidLoad
//=========================
// DIDLOAD
//=========================
-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self setUp];

    [self addTabsGesture];
}

-(void)setUp{
    if(self.pageViewController){
        [self.pageViewController.view removeFromSuperview];
        [self removeChildCTRL:self.pageViewController];
        _modelController = nil;
        self.pageViewController = nil;
    }
    self.pageViewController = [[PVCtrl alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageViewController.delegate = self;
    [self.pageViewController setNeedsStatusBarAppearanceUpdate];
    
    DataViewController *startingViewController = [self.modelController viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:NULL];
    
    self.pageViewController.dataSource = self.modelController;
    
    [self addChildViewController:self.pageViewController];
    [self.pagerView addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
    CGRect pageViewRect = self.view.bounds;
    //    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
    //        pageViewRect = CGRectInset(pageViewRect, 40.0, 40.0);
    //    }
    
    self.pageViewController.view.frame = pageViewRect;
    [self.context_menu removeRotella];
}

//=========================
// Appear
//=========================
#pragma mark - Appear
- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"PdfPager viewWillAppear");
    [super viewWillAppear:animated];

}


-(NSArray*)arrayForMenu{
    return @[@{@"image":@"font_a.png",@"title":NSLocalizedString(@"book_fontA", nil),@"type":[NSNumber numberWithInt:MenuType_FontA]},
             @{@"image":@"font_b.png",@"title":NSLocalizedString(@"book_fontB", nil),@"type":[NSNumber numberWithInt:MenuType_FontB]},
             @{@"image":@"font_c.png",@"title":NSLocalizedString(@"book_fontC", nil),@"type":[NSNumber numberWithInt:MenuType_FontC]},
             @{@"image":@"menu_share.png",@"title":NSLocalizedString(@"book_condividi", nil),@"type":[NSNumber numberWithInt:MenuType_Share]},
             @{@"image":@"feature_close@2x.png",@"title":NSLocalizedString(@"book_chiudi", nil),@"type":[NSNumber numberWithInt:MenuType_CloseFeature]}];
    
    //@{@"image":@"menu_close.png",@"title":@"Chiudi Menu",@"type":[NSNumber numberWithInt:MenuType_CloseMenu]},
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"PdfPager DIDAppear");
    [super viewDidAppear:animated];
    if(![self.defaults boolForKey:TUTORIAL_MODALITA_BOOK]){
        [self customTutorialAlert];
    }
    
    self.context_menu = [[MenuList alloc] init];
    [self.context_menu addMenuWithDelegate:self
                              andItemArray:self.arrayForMenu];
    NSLog(@"PdfPager %@/n%@/",self.pageViewController,[self.pageViewController viewControllers]);
    for(id r in self.pageViewController.viewControllers)
    NSLog(@"r:%@",r);
}

#pragma mark -
#pragma mark DIDSelect delegate
//=========================================
// DIDSelect delegate
//=========================================
-(void)didSelectIndex:(MenuButtonType)index{
    switch (index) {
        case MenuType_FontA:
            NSLog(@"FontA");
            [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:MenuType_FontA] forKey:PDF_FONT_TYPE];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [Home reloadPdf];
            break;
        case MenuType_FontB:
            NSLog(@"FontB");
            [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:MenuType_FontB] forKey:PDF_FONT_TYPE];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [Home reloadPdf];
            break;
        case MenuType_FontC:
            NSLog(@"FontC");
            [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:MenuType_FontC] forKey:PDF_FONT_TYPE];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [Home reloadPdf];
            break;
        case MenuType_Share:
            NSLog(@"Share!");
            [self shareAction];
            break;
        case MenuType_CloseMenu:
            NSLog(@"Chiudi Menu");
            [self toggleNavBar:NO];
            break;
        case MenuType_CloseFeature:
            NSLog(@"Chiudi pdfPager");
            [Home dismissPdfPager];
            break;
            
        default:
            break;
    }
}

#pragma mark -
#pragma mark SHARE action
//=========================================
// SHARE action
//=========================================
-(void)shareAction{
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        self.share_ctrl = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:self.url]];
        
        self.share_ctrl.delegate = self;
        [self.share_ctrl presentOptionsMenuFromRect:CGRectMake(0, -24, self.view.frame.size.width, 10) inView:self.view animated:YES];
        
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        //[self removeRotellaSHARE];
    }];//fine step2
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];

}



-(void)documentInteractionController:controller willBeginSendingToApplication:(NSString *)application{
    // NSLog(@"documentInteractionController:%@",application);
    if([DDMenuController isMenuOpen])[DDMenuController closeMenuWithCompletion:^{}];
}

#pragma mark -
#pragma mark TAB delegate
//=========================================
// TAB delegate
//=========================================
-(void)addTabsGesture{
    UITapGestureRecognizer* dTap = [[UITapGestureRecognizer alloc] initWithTarget : self  action : @selector (doubleTap:)];
    
    [dTap setDelaysTouchesBegan:NO];
    [dTap setDelaysTouchesEnded:NO];
    dTap.numberOfTapsRequired = 2;
    dTap.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer : dTap];
    
    UITapGestureRecognizer* sTap = [[UITapGestureRecognizer alloc] initWithTarget : self  action : @selector (singleTap:)];
    
    [sTap setDelaysTouchesBegan:NO];
    [sTap setDelaysTouchesEnded:NO];
    
    sTap.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer : sTap];
}

- (void)doubleTap:(UIGestureRecognizer*) sender{
    [self toggleNavBar:YES];
}

- (void)singleTap:(UIGestureRecognizer*) sender{
    if(![self.context_menu isShowed]){
        [self toggleNavBar:NO];
    }
}

-(void)toggleNavBar:(BOOL)attiva{
    [self.context_menu toggleMenu:attiva];
}


- (ModelController *)modelController
{
    // Return the model controller object, creating it if necessary.
    // In more complex implementations, the model controller may be passed to the view controller.
    if (!_modelController) {
        _modelController = [[ModelController alloc] initWithPdfUrl:self.url];
    }
    return _modelController;
}

-(void)reloadPdfUrl:(NSString*)u{
    self.url = u;
    [self setUp];
}

#pragma mark -
#pragma mark UIPageViewController delegate
//=========================================
// UIPageViewController delegate
//=========================================

 - (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
 {
     //[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    
 }


- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    if (UIInterfaceOrientationIsPortrait(orientation) || ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)) {
        // In portrait orientation or on iPhone: Set the spine position to "min" and the page view controller's view controllers array to contain just one view controller. Setting the spine position to 'UIPageViewControllerSpineLocationMid' in landscape orientation sets the doubleSided property to YES, so set it to NO here.
        
        UIViewController *currentViewController = self.pageViewController.viewControllers[0];
        NSArray *viewControllers = @[currentViewController];
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:NULL];
        
        self.pageViewController.doubleSided = NO;
        return UIPageViewControllerSpineLocationMin;
    }
    
    // In landscape orientation: Set set the spine location to "mid" and the page view controller's view controllers array to contain two view controllers. If the current page is even, set it to contain the current and next view controllers; if it is odd, set the array to contain the previous and current view controllers.
    DataViewController *currentViewController = self.pageViewController.viewControllers[0];
    NSArray *viewControllers = nil;
    
    NSUInteger indexOfCurrentViewController = [self.modelController indexOfViewController:currentViewController];
    if (indexOfCurrentViewController == 0 || indexOfCurrentViewController % 2 == 0) {
        UIViewController *nextViewController = [self.modelController pageViewController:self.pageViewController viewControllerAfterViewController:currentViewController];
        viewControllers = @[currentViewController, nextViewController];
    } else {
        UIViewController *previousViewController = [self.modelController pageViewController:self.pageViewController viewControllerBeforeViewController:currentViewController];
        viewControllers = @[previousViewController, currentViewController];
    }
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:NULL];
    
    
    return UIPageViewControllerSpineLocationMid;
}

#pragma mark -
#pragma mark CUSTOM ALERTVIEW
//=========================================
// CUSTOM ALERTVIEW
//=========================================
- (void)customTutorialAlert
{
    // Here we need to pass a full frame
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self createTutorialContainer]];
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK", nil]];
    [alertView setDelegate:self];
    
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    [alertView close];
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d. CHECK:%@", (int)buttonIndex, (int)[alertView tag],self.tutorialAlertGroup.checkedCheckBoxes);
    [self.defaults setBool:[self.tutorialAlertGroup.checkedCheckBoxes firstObject] forKey:TUTORIAL_MODALITA_BOOK];
    [self.defaults synchronize];
}

- (UIView *)createTutorialContainer
{
    
    UILabel *titolo = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 290, 25)];
    [titolo setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:20]];
    [titolo setTextAlignment:NSTextAlignmentCenter];
    [titolo setText:NSLocalizedString(@"book_titolo", nil)];
    
    UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(titolo.frame)+5, 270, 125)];
    [text setText:NSLocalizedString(@"book_mex", nil)];
    [text setFont:[UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size:16]];
    [text setNumberOfLines:0];
    [text sizeToFit];
    
    TNCircularCheckBoxData *bananaData = [[TNCircularCheckBoxData alloc] init];
    bananaData.identifier = NSLocalizedString(@"book_checkbox", nil);
    bananaData.labelText = NSLocalizedString(@"book_checkbox", nil);
    bananaData.checked = NO;
    bananaData.labelMarginLeft = 5.f;
    bananaData.borderColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    bananaData.circleColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    bananaData.borderRadius = 20;
    bananaData.circleRadius = 15;
    
    self.tutorialAlertGroup = [[TNCheckBoxGroup alloc] initWithCheckBoxData:@[bananaData] style:TNCheckBoxLayoutHorizontal];
    self.tutorialAlertGroup.labelColor = [[UIColor blackColor] colorWithAlphaComponent:0.55];
    self.tutorialAlertGroup.rowItemCount = 1;
    [self.tutorialAlertGroup create];
    self.tutorialAlertGroup.position = CGPointMake(25, CGRectGetMaxY(text.frame)+10);
    [self.tutorialAlertGroup setBackgroundColor:[UIColor clearColor]];

    NSLog(@"titolo:%@",titolo);
    NSLog(@"text:%@",text);
    NSLog(@"createTutorialContainer%@",self.tutorialAlertGroup);
    
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, CGRectGetMaxY(text.frame)+40)];
    [demoView addSubview:titolo];
    [demoView addSubview:text];
    [demoView addSubview:self.tutorialAlertGroup];

    return demoView;
}


@end
