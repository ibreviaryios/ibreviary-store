//
//  PdfPager.h
//  ZoomingPDFViewer
//
//  Created by Leonardo Parenti on 29/12/15.
//  Copyright © 2015 Apple DTS. All rights reserved.
//
/*
 DemoCall:
 
 @property (strong, nonatomic) PdfPager *p_test;

 
 NSString *path = [[NSBundle mainBundle] pathForResource:@"test_ibreviary.pdf" ofType:nil];
 
 self.p_test = [[PdfPager alloc] initWithPdfUrl:path
                                     controller:self
                                           view:self.view];

 
 
 */

#import <Foundation/Foundation.h>
#import "BaseController.h"

@interface PVCtrl : UIPageViewController

@end

@interface PdfPager : BaseController <UIPageViewControllerDelegate>


+(PdfPager*)presentPDFPagerWithUrlFile:(NSString*)u;
-(void)reloadPdfUrl:(NSString*)url;
@end
