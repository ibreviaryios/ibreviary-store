//
//  MenuViewController.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "Breviario.h"
#import "Messale.h"
#import "TabellaMaster.h"
#import "TPHome.h"
#import "MessaMenuCtrl.h"
#import "Home.h"




@interface MenuViewController : BaseController <UITableViewDataSource,UITableViewDelegate,MessaMenuDelegate,UINavigationControllerDelegate>{
    NSMutableArray *_objects;
}

@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (strong, nonatomic) IBOutlet UIImageView *sfondo_master;
@property (strong, nonatomic) IBOutlet UILabel *day_label;
@property (strong, nonatomic) IBOutlet UIButton *btn_home;
@property (strong, nonatomic) IBOutlet UIView *black_mask;

//---Messale SCROLLER
@property (strong, nonatomic) UIView *switcherView;
@property (strong, nonatomic) UIScrollView *scroller;
@property (strong, nonatomic) UIPageControl *pager;
@property (strong, nonatomic) IBOutlet UIImageView *tp_img;
@property (strong, nonatomic) NSMutableArray *listaMessaliMenu;
@property (strong, nonatomic) MissalObject *messaleSelected;

//SUBController
@property (strong, nonatomic) Breviario *breviario;
@property (strong, nonatomic) Messale *messale;
@property (strong, nonatomic) TabellaMaster *controllerLista;
@property (nonatomic) MessaMenuCtrl *messaMenuCtrl;

@property (strong, nonatomic) TPHome *tph_ctrl;




+ (MenuViewController *)sharedInstance;

+(void)aggiornaVista;
+(void)checkCSS;
+(void)fineMessaCaricata;

+(void)setCurrentMissalPos:(NSNumber*)pos;
+(void)setCurrentBreviaryPos:(NSNumber*)pos;

@end
