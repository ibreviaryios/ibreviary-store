//
//  LettureCTRL.m
//  iBreviary HD 2.0
//
//  Created by leyo on 17/12/12.
//  Copyright (c) 2012 leyo. All rights reserved.
//

#import "LettureCTRL.h"
#import "Day.h"
#import "Fix.h"
#import "Home.h"

@implementation LettureCTRL
    
//Home istance
@synthesize home, lista, missal;
//LettureCTRL GUI
@synthesize tabella, titolo, sfondo_master;
@synthesize black_mask, day_label;



#pragma mark - INIT method
//=========================
// INIT
//=========================
- (id)initWithMissal:(MissalObject*)m
{
    //iPhone4
    NSString *nibNameOrNil = @"LettureCTRL";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"LettureCTRL_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"LettureCTRL_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) nibNameOrNil = @"LettureCTRL5";
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        home = [Home sharedInstance];
        missal = m;
    }
    
    return self;
}


#pragma mark - DIDLOAD method
//=========================
// DIDLOAD method
//=========================
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initLista];
    [self gui];
    if(self.isIpad)[[NSNotificationCenter defaultCenter] addObserver:self
                                                            selector:@selector(controllaLettOrientationIPAD)
                                                                name:DID_ROTATE object:nil];
    
}

-(void)controllaLettOrientationIPAD{
    [sfondo_master setImage:[UIImage imageNamed:self.isLand?@"back_menu_002L.png":@"back_menu_002P.png"]];
    
    int i = (int)[lista count];
    CGRect frame;
    frame = CGRectMake(-5, self.view.frame.size.height-(50*i)-100, 270, 50*i);
    [tabella setFrame:frame];
}

-(void)initLista{
    lista = [[Home getCurrentDay] listaLetture:missal];
}

- (void)gui
{
    NSString *nav_titolo = [[Fix getFixForLang:[[Home getCurrentDay] lingua]] readings_title];
    [self titleOnNavBar:nav_titolo];
    
    [day_label setFont:[UIFont fontWithName:FONT_ROMAN_ANTIQUE size:15]];
    [titolo setFont:[UIFont fontWithName:FONT_ROMAN_ANTIQUE size:15]];
    
    [titolo setText:NSLocalizedString([[self.defaults objectForKey:ROOT_TS_INDEX] objectAtIndex:0], nil)];
}


-(IBAction)goHome:(UIButton*)sender{
    // NSLog(@"goHome!");
    [self.navigationController popToRootViewControllerAnimated:YES];
    [DDMenuController closeMenuWithCompletion:
                                                    ^{  // NSLog(@"Settare Home in Home!!");
                                                        [Home mostraHome];
                                                    }];
}

-(IBAction)goBack:(UIButton*)sender{
    // NSLog(@"goBack!");
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - LifeCyrcle
//=========================
// LIFECYRCLE
//=========================
#pragma mark Appear

- (void)viewWillAppear:(BOOL)animated{
    // NSLog(@"viewWillAppear!");
	[super viewWillAppear:animated];
    //Sfondo
    BOOL reverse = ([self.defaults integerForKey:CSS]==2) && [Home isContentVisible];
    [day_label setTextColor:reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.4]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9]];
    [day_label setText:missal.title];
    [black_mask setHidden:!reverse];
    if(self.isIpad) [self controllaLettOrientationIPAD];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //Sfondo
    BOOL reverse = ([self.defaults integerForKey:CSS]==2) && [Home isContentVisible];
    [day_label setTextColor:reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.4]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9]];
    [day_label setText:missal.title];
    [black_mask setHidden:!reverse];
    [tabella reloadData];

}

#pragma mark Disappear

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

#pragma mark -
#pragma mark --------> Table view
#pragma mark Header/Footer
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// HEADER SESSION
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

// FOOTER SESSION
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

#pragma mark ROW SIZE

//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int i = (int)[lista count];
    CGRect frame = CGRectMake(-5, self.view.frame.size.height-(50*i)-90, 260, 50*i);
    [tabella setFrame:frame];
    return i;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50.0;
}

#pragma mark CELL FOR ROW
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellLetture e Preghiere";
    BOOL reverse = ([self.defaults integerForKey:CSS]==2 && [Home isContentVisible]);

    UIColor *coloreCapoLettera = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.48]:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.9];
    UIColor *colore = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.45]:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.9];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		UIView* backgroundView = [[UIView alloc ] initWithFrame:CGRectZero ];
		backgroundView.backgroundColor = [UIColor clearColor];
		cell.backgroundView.alpha = 0;
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.alpha = 0;
		cell.contentView.backgroundColor = [UIColor clearColor];
		[cell setBackgroundView: backgroundView];
        cell.textLabel.backgroundColor =[UIColor clearColor];
        cell.textLabel.text = @"";
    }
	//Colore Label RGB a=alpha;
	UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, 260, 49)];
	Label.backgroundColor = [UIColor clearColor];
    

    
    
    NSString *testo = @"";
    Item *row = [lista objectAtIndex:indexPath.row];
    // NSLog(@"cellForRowAtIndexPath:%@",row);
    if(row.titolo_home==nil)
        {
            testo = [row titolo];
            //CAPO
            UILabel *capo = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 50, 50)];
            [capo setBackgroundColor:[UIColor clearColor]];
            [capo setFont: [UIFont fontWithName:FONT_EILEENCAPS size: 35]];
            [capo setTextColor:coloreCapoLettera];
            [capo setShadowColor:reverse?[UIColor clearColor]:[UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:0.2]];
            capo.shadowOffset = CGSizeMake(1,2);
            [capo setTextAlignment:NSTextAlignmentLeft];
            [capo setText:[testo substringWithRange:NSMakeRange(0,1)]];
            
            //RESTO
            UILabel *resto = [[UILabel alloc] initWithFrame:CGRectMake(42, 14, 218, 35)];
            resto.backgroundColor = [UIColor clearColor];
            [resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size:25]];
            resto.textAlignment = NSTextAlignmentLeft;
            [resto setTextColor:colore];
            resto.numberOfLines = 2;
            [resto setAdjustsFontSizeToFitWidth:YES];
            [resto setMinimumScaleFactor:0.6];
            [resto setShadowColor:capo.shadowColor];
            resto.shadowOffset = CGSizeMake(0,2);
            resto.text = [testo substringFromIndex:(NSUInteger)1];
	
            [Label addSubview:capo];
            [Label addSubview:resto];
            
            // NSLog(@"lingua giorno: %@",[[Home getCurrentDay] linguaGiorno]);
            if([[[Home getCurrentDay] linguaGiorno] alignment_right])
                {
                CGFloat marginLeft = 5;
                [resto setText:testo];
                [capo setText:@""];
                [resto setNumberOfLines:1];
                [resto setAdjustsFontSizeToFitWidth:YES];
                [resto setTextAlignment:NSTextAlignmentRight];
                [resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size: 26]];
                [resto setFrame:CGRectMake(marginLeft, Label.frame.origin.y, 255-marginLeft, Label.frame.size.height)];
                }

    }
    else{
        // NSLog(@"SONO QUI:%@",row);
        testo = [row titolo_home];
        UILabel *titolo_home = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, tabella.frame.size.width-160, 20)];
        [titolo_home setBackgroundColor:[UIColor clearColor]];
        CAGradientLayer *gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = titolo_home.bounds;
        gradientLayer.colors = [NSArray arrayWithObjects:(id)[[[UIColor whiteColor] colorWithAlphaComponent:0.1]CGColor], (id)[[UIColorFromRGB(0Xffffff) colorWithAlphaComponent:0.2]CGColor], nil];
        [titolo_home.layer insertSublayer:gradientLayer atIndex:0];
        
        [titolo_home setFont: [UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:12]];
        [titolo_home setTextColor:[UIColorFromRGB(0XFF0000) colorWithAlphaComponent:0.9]];
        [titolo_home setTextAlignment:NSTextAlignmentCenter];
        [titolo_home setAdjustsFontSizeToFitWidth:YES];
        [titolo_home setMinimumScaleFactor:0.5];
        [titolo_home setText:[testo uppercaseString]];
        [cell.contentView addSubview:titolo_home];
        
        UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 50)];
        [selectedBackgroundView setBackgroundColor:[UIColor clearColor]];
        [cell setSelectedBackgroundView:selectedBackgroundView];

        
        return cell;
    }
    
    
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 50)];
    [selectedBackgroundView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.1]];
	[cell setSelectedBackgroundView:selectedBackgroundView];
    
	[cell.contentView addSubview:Label];
	
	return cell;

}

#pragma mark DELEGATE (tableView)

// COMANDA DETAILVIEW
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self performSelector:@selector(deselectRow:) withObject:indexPath afterDelay:0.5];
    
    Item *i = [lista objectAtIndex:indexPath.row];
    if(i.titolo_home) return;
    
    [DDMenuController closeMenuWithCompletion:
     ^{
         [Home mostraItem:i];
     }];
}


-(void)deselectRow:(NSIndexPath*)indexPath{
    [tabella deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark --------> FINE - Table view
    
    


#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    // NSLog(@"will Rotate");
     [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    // NSLog(@"did Rotate");
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}



#pragma mark - Error CLass
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [super viewDidUnload];
}




//=========================
// END CLASS
//=========================

@end
