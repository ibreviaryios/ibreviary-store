//
//  LettureCTRL.h
//  iBreviary 2014
//
//  Created by leyo on 26/01/13.
//  Copyright (c) 2013 leyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "TabellaMaster.h"
#import "MissalObject.h"


@class Home;

@interface LettureCTRL : BaseController<UIScrollViewDelegate>

//Home istance
@property (strong, readonly) Home *home;
@property (strong, nonatomic) NSMutableArray *lista;
@property (strong, nonatomic) MissalObject *missal;


//LettureCTRL GUI
@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (strong, nonatomic) IBOutlet UILabel *titolo;
@property (strong, nonatomic) IBOutlet UIImageView *sfondo_master;
@property (strong, nonatomic) IBOutlet UIView *black_mask;
@property (strong, nonatomic) IBOutlet UILabel *day_label;
 


- (id)initWithMissal:(MissalObject*)m;

@end
