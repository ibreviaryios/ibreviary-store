//
//  Messale.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "Messale.h"
#import "Day.h"
#import "Fix.h"
#import "Home.h"
#import "MenuViewController.h"

@implementation Messale
    
//Home istance
@synthesize home, lista;
//Messale GUI
@synthesize tabella, titolo, sfondo_master;
@synthesize black_mask, day_label,ghirigoro;

//Comuni Controller
@synthesize controllerLista,controllerOrdinario;
//---Messale SCROLLER
@synthesize currentMissalPos, switcherView, scroller, pager, listaMessali,messaleSelected;
@synthesize controllerLetture;

//ROTELLA INVIEW
@synthesize rotella_inview, subRotella_inview;

BOOL tornatoDaLetturePreghiere;
BOOL missal_did_load;

#pragma mark - INIT method
//=========================
// INIT
//=========================
- (id)initWithMissalPos:(int)pos
{
    //iPhone4
    NSString *nibNameOrNil = @"Messale";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"Messale_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) nibNameOrNil = @"Messale5";
    

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        home = [Home sharedInstance];
        currentMissalPos = pos;
        
    }
    return self;
}


#pragma mark - DIDLOAD method
//=========================
// DIDLOAD method
//=========================
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self gui];
    tornatoDaLetturePreghiere = NO;
    missal_did_load = YES;
    if(self.isIpad)[[NSNotificationCenter defaultCenter] addObserver:self
                                                            selector:@selector(controllaMessaleOrientationIPAD)
                                                                name:DID_ROTATE object:nil];
    
}

-(void)controllaMessaleOrientationIPAD{
    // NSLog(@"controllaMessaleOrientationIPAD");
    [sfondo_master setImage:[UIImage imageNamed:self.isLand?@"back_menu_002L.png":@"back_menu_002P.png"]];
}

-(void)initLista{
    lista = [[NSMutableArray alloc] initWithArray:[self.defaults objectForKey:MESSALE_INDEX]];
    // NSLog(@"day:%@ lista%@",[Home getCurrentDay],lista);
}

- (void)gui
{
    [pager setHidden:YES];
    [ghirigoro setHidden:YES];

    //Rotella
    [rotella_inview setHidden:NO];
    
    [self titleOnNavBar:NSLocalizedString([[self.defaults objectForKey:ROOT_TS_INDEX] objectAtIndex:1], nil)];


    [day_label setFont:[UIFont fontWithName:FONT_CALSON_ANTIQUE size:15]];
    [day_label setAdjustsFontSizeToFitWidth:YES];
    [day_label setText:@""];

    [titolo setFont:[UIFont fontWithName:FONT_CALSON_ANTIQUE size:15]];
    
    [titolo setText:NSLocalizedString([[self.defaults objectForKey:ROOT_TS_INDEX] objectAtIndex:1], nil)];
    
    if(IS_IPHONE_6 || IS_IPHONE_6_PLUS){
        UIImage *image = [UIImage imageNamed:@"back_menu_002P.png"];
        [self.sfondo_master setImage:image];
        [self.sfondo_master setContentMode:UIViewContentModeTopLeft];
    }
    
    }

-(void)updateTablePosition{
    if(IS_IPHONE_6 || IS_IPHONE_6_PLUS){
        NSLog(@"1:t:%@ g:%@ d:%@",tabella,ghirigoro,day_label);
        
        //set day label frame
        CGRect d = day_label.frame;
        d.origin.y = self.view.frame.size.height - day_label.frame.size.height;
        [day_label setFrame:d];
        
        //set ghirigoro frame
        CGRect g = ghirigoro.frame;
        g.origin.y = d.origin.y - ghirigoro.frame.size.height;
        [ghirigoro setFrame:g];
        
        //set tabella frame
        CGRect f = tabella.frame;
        f.size.height = 50*lista.count;
        f.origin.y = g.origin.y - f.size.height;
        [tabella setFrame:f];
        NSLog(@"2:t:%@ g:%@ d:%@",tabella,ghirigoro,day_label);
    }

}


-(IBAction)goHome:(UIButton*)sender{
    // NSLog(@"goHome!");
    [self.navigationController popToRootViewControllerAnimated:YES];
    [MenuViewController setCurrentMissalPos:[NSNumber numberWithInteger:self.pager.currentPage]];
    [DDMenuController closeMenuWithCompletion:
                                                    ^{
                                                        [Home mostraHome];
                                                    }];
}

-(IBAction)goBack:(UIButton*)sender{
    // NSLog(@"goBack!");
    [MenuViewController setCurrentMissalPos:[NSNumber numberWithInteger:self.pager.currentPage]];

    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - LifeCyrcle
//=========================
// LIFECYRCLE
//=========================
#pragma mark Appear

- (void)viewWillAppear:(BOOL)animated{
    // NSLog(@"viewWillAppear!");
	[super viewWillAppear:animated];
    if(self.isIpad)[self controllaMessaleOrientationIPAD];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(![self.defaults boolForKey:TUTORIAL_MENU_LIBRO]){
        [self customTutorialBookMenuAlert];
    }
    
    BOOL reverse = ([self.defaults integerForKey:CSS]==2) && [Home isContentVisible];
    [day_label setTextColor:reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.4]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9]];
    [day_label setText:[[[Home sharedInstance] day] day_label]];
    [black_mask setHidden:!reverse];
    
    if(missal_did_load || tornatoDaLetturePreghiere)
    {
        // NSLog(@"missal did load!");
        missal_did_load = NO;
        tornatoDaLetturePreghiere = NO;
        [self initLista];
        listaMessali = [[NSMutableArray alloc] initWithArray:[[Home getCurrentDay] listaMissals]];
        // NSLog(@"initLista: %@",[[Home getCurrentDay] listaMissals]);
        [self buildMessaleScroller];
        [ghirigoro setHidden:NO];
        [self updateTablePosition];
        [tabella reloadData];
    }
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
    {
        [alertView close];
        NSLog(@"Delegate: Button at position %d is clicked on alertView %d. CHECK:%@", (int)buttonIndex, (int)[alertView tag],self.tutorialAlertGroup.checkedCheckBoxes);
        [self.defaults setBool:[self.tutorialAlertGroup.checkedCheckBoxes firstObject] forKey:TUTORIAL_MENU_LIBRO];
        [self.defaults synchronize];
    }

#pragma mark Disappear

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

#pragma mark -
#pragma mark --------> Table view
#pragma mark Header/Footer
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

// HEADER SESSION
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

// FOOTER SESSION
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

#pragma mark ROW SIZE

//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // NSLog(@"Lsta count:%lui",(unsigned long)[lista count]);
    if([lista count]>0) [self rimuoviRotellaInView];
    return [lista count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50.0;
}

#pragma mark CELL FOR ROW
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellMessale";
    // NSLog(@"cellForRowAtIndexPath:%@",indexPath);
    BOOL reverse = ([self.defaults integerForKey:CSS]==2 && [Home isContentVisible]);

    UIColor *coloreCapoLettera = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.48]:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.9];
    UIColor *colore = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.45]:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.9];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		UIView* backgroundView = [[UIView alloc ] initWithFrame:CGRectZero ];
		backgroundView.backgroundColor = [UIColor clearColor];
		cell.backgroundView.alpha = 0;
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.alpha = 0;
		cell.contentView.backgroundColor = [UIColor clearColor];
		[cell setBackgroundView: backgroundView];
        cell.textLabel.backgroundColor =[UIColor clearColor];
        cell.textLabel.text = @"";
    }
	//Colore Label RGB a=alpha;
	UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, 220, 50)];
	Label.backgroundColor = [UIColor clearColor];
    
    UILabel *capo = [[UILabel alloc] initWithFrame:CGRectMake(1, 0, 50, 50)];
	[capo setBackgroundColor:[UIColor clearColor]];
	[capo setFont: [UIFont fontWithName:FONT_EILEENCAPS size: 28]];
    [capo setTextColor:coloreCapoLettera];
    [capo setShadowColor:reverse?[UIColor clearColor]:[UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:0.2]];
    capo.shadowOffset = CGSizeMake(1,2);
	[capo setTextAlignment:NSTextAlignmentLeft];
    
    NSString *testo = NSLocalizedString([lista objectAtIndex:indexPath.row], nil);
	[capo setText:[testo substringWithRange:NSMakeRange(0,1)]];
    
	UILabel *resto = [[UILabel alloc] initWithFrame:CGRectMake(27, 12, 230, 35)];
	resto.backgroundColor = [UIColor clearColor];
	[resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size:26]];
	[resto setAdjustsFontSizeToFitWidth:YES];
    [resto setMinimumScaleFactor:0.8];
	resto.textAlignment = NSTextAlignmentLeft;
	[resto setTextColor:colore];
	
	resto.lineBreakMode = NSLineBreakByClipping;
	resto.numberOfLines = 1;
    [resto setShadowColor:capo.shadowColor];
	resto.shadowOffset = CGSizeMake(0,2);
	
	resto.text = [testo substringFromIndex:(NSUInteger)1];
    
    if([NSLocalizedString(@"checklingua",nil) isEqualToString:@"ar"])
    {
        CGFloat marginLeft = 5;
        [resto setText:testo];
        [capo setText:@""];
        [resto setBackgroundColor:[UIColor clearColor]];
        [resto setTextAlignment:NSTextAlignmentRight];
        [resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size: 26]];
        [resto setFrame:CGRectMake(marginLeft, Label.frame.origin.y, 255-marginLeft, Label.frame.size.height)];
    }
	
	UIImageView *selectedBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270, 50)];
	[selectedBackgroundView setImage:[UIImage imageNamed:@"cellSelect80.png"]];
	[cell setSelectedBackgroundView:selectedBackgroundView];
	[Label addSubview:capo];
	[Label addSubview:resto];
	[cell.contentView addSubview:Label];
    Fix *fix = [Fix getFixForLang:[self.defaults objectForKey:LINGUA]];

    if((indexPath.row == 2 && fix.prefazio_array.count==0) || (indexPath.row == 3 && fix.liturgiaeucaristica_array.count==0)|| (indexPath.row == 4 && fix.preghieredeifedeli_array.count==0) || (indexPath.row == 5 && fix.missal_comuni_array.count==0))
    {
        UIImageView *selectedBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 260, 50)];
        [selectedBackgroundView setImage:[UIImage imageNamed:@"x@2x.png"]];
        [selectedBackgroundView setContentMode:UIViewContentModeCenter];
        [selectedBackgroundView setAlpha:0.1];
        [cell setSelectedBackgroundView:selectedBackgroundView];
    }
	
	return cell;

}

#pragma mark DELEGATE (tableView)

// COMANDA DETAILVIEW
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self performSelector:@selector(deselectRow:) withObject:indexPath afterDelay:0.5];
    Fix *fix = [Fix getFixForLang:[[[Home getCurrentDay] linguaGiorno] sigla]];
    switch (indexPath.row) {
        case 0:
        {
            controllerOrdinario = nil;
            controllerOrdinario = [[MessaleOrdinario alloc] initWithItem:[fix ordinarioDellaMessa]];
            [self.navigationController pushViewController:controllerOrdinario animated:YES];
            // NSLog(@"Ordinario della Messa");
            Item *ordinarioMessa = [fix ordinarioDellaMessa];
            [Home mostraItem:ordinarioMessa];
            break;
        }
        case 1:
        {
            // NSLog(@"Letture & preghiere");
            controllerLetture = nil;
            tornatoDaLetturePreghiere = YES;
            MissalObject * m;
            if (listaMessali.count>1)
                m = [listaMessali objectAtIndex:self.pager.currentPage];
            else if(listaMessali.count==1)
                m = [listaMessali objectAtIndex:0];
            else return;
            // NSLog(@"a:%@,b:%@",m.antiphon_and_opening_prayer,m.prayer_of_the_faithful);
            controllerLetture = [[LettureCTRL alloc] initWithMissal:m];
            
            [self.navigationController pushViewController:controllerLetture animated:YES];

            
            
            break;
        }
        case 2:
        {
            // NSLog(@"Prefazio");
            if(fix.prefazio_array.count==0) return;
            controllerLista = nil;
            controllerLista = [[TabellaMaster alloc] initWithList:fix.prefazio_array andTitle:NSLocalizedString(@"Preface", nil)];
            [self.navigationController pushViewController:controllerLista animated:YES];
            break;
        }
        case 3:
        {
            // NSLog(@"Preghiera Eucaristica");
            if(fix.liturgiaeucaristica_array.count==0) return;
            controllerLista = nil;
            controllerLista = [[TabellaMaster alloc] initWithList:fix.liturgiaeucaristica_array andTitle:NSLocalizedString(@"Eucharistic Prayer", nil)];
            [self.navigationController pushViewController:controllerLista animated:YES];
            break;
       }
        case 4:
        {
            // NSLog(@"Preghiera dei Fedeli");
            if(fix.preghieredeifedeli_array.count==0) return;
            controllerLista = nil;
            controllerLista = [[TabellaMaster alloc] initWithList:fix.preghieredeifedeli_array andTitle:NSLocalizedString(@"Faithfuls' Prayer", nil)];
            [self.navigationController pushViewController:controllerLista animated:YES];
            break;
       }
        case 5:
        {
            // NSLog(@"Comuni");
            if(fix.missal_comuni_array.count==0) return;
            controllerLista = nil;
            controllerLista = [[TabellaMaster alloc] initWithList:fix.missal_comuni_array andTitle:NSLocalizedString(@"Common", nil)];
            [self.navigationController pushViewController:controllerLista animated:YES];

            break;
        }
        default:
            break;
    }
}


-(void)deselectRow:(NSIndexPath*)indexPath{
    [tabella deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark --------> FINE - Table view
    
    
#pragma mark -
#pragma mark SCROLLER
//=========================
// SCROLLER MISSAL
//=========================
-(void)buildMessaleScroller
    {
        int count = (int)[listaMessali count];
        // NSLog(@"buildMessaleScroller[%i]",count);
        
        CGRect puntoDiPartenza = CGRectMake(0, 0, 250, 100);
        
        for(UIView* subview in [self.scroller subviews]) //per cancellare tutte le superviste (reset)
        [subview removeFromSuperview];
        
        if(count==1) return;
        
        for (int i = 0; i < count; i++) {
            
            MissalObject *row = [listaMessali objectAtIndex:i];
            
            CGRect frame = self.scroller.frame;
            frame.origin.x = self.scroller.frame.size.width*i;
            frame.origin.x += 2;
            frame.size = CGSizeMake(self.scroller.frame.size.width-4, self.scroller.frame.size.height);
           
            // NSLog(@"subTitolo_tex:%@",NSStringFromCGRect(frame));
            UIView *subTitolo_tex = [[UIView alloc] initWithFrame:frame];
            CAGradientLayer *gradientLayer = [CAGradientLayer layer];
            gradientLayer.frame = subTitolo_tex.bounds;
            gradientLayer.colors = [NSArray arrayWithObjects:(id)[[[UIColor whiteColor] colorWithAlphaComponent:0.2]CGColor], (id)[[UIColorFromRGB(0Xffffff) colorWithAlphaComponent:0.5]CGColor], nil];
            [subTitolo_tex.layer insertSublayer:gradientLayer atIndex:0];
            [subTitolo_tex.layer setBorderColor:[[UIColorFromRGB(0Xd89c57) colorWithAlphaComponent:0.5]CGColor]];
            [subTitolo_tex.layer setBorderWidth:2.0];
            [subTitolo_tex setAlpha:0.8];
            
            [self.scroller addSubview:subTitolo_tex];
            
            
            NSString *titolo_messale = row.title;
            if(row.variant && [row.variant isKindOfClass:[NSString class]] && ![row.variant isEqualToString:@""])
                titolo_messale = row.variant;
            NSString *titolo_text = row.subtitle?[NSString stringWithFormat:@"%@ - %@",titolo_messale,row.subtitle]:(row.type?[NSString stringWithFormat:@"%@ - %@",titolo_messale,row.type]:titolo_messale);

            frame.origin.y = -4;
            UILabel *label_titolo = [[UILabel alloc] initWithFrame:frame];
            [label_titolo setBackgroundColor:[UIColor clearColor]];
            [label_titolo setTextColor:UIColorFromRGB(0X483627)];
            [label_titolo setNumberOfLines:2];
            [label_titolo setMinimumScaleFactor:0.6];
            [label_titolo setLineBreakMode:NSLineBreakByTruncatingTail];
            
            [label_titolo setTextAlignment:NSTextAlignmentCenter];
            [label_titolo setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:14]];
            [label_titolo setText:[titolo_text uppercaseString]];
            [label_titolo setAdjustsFontSizeToFitWidth:YES];
            
            [self.scroller addSubview:label_titolo];
        }
        
        [pager setNumberOfPages:count];
        [pager setHidden:(count==1)];
        [switcherView setHidden:(count==1)];

        if(currentMissalPos!=-1)
        {
            puntoDiPartenza.origin.x = self.scroller.frame.size.width * currentMissalPos;
            [pager setCurrentPage:currentMissalPos];
            puntoDiPartenza.origin.y = 0;
            puntoDiPartenza.size = self.scroller.frame.size;
        }


        self.scroller.pagingEnabled = YES;
        self.scroller.contentSize = CGSizeMake(self.scroller.frame.size.width * count, self.scroller.frame.size.height);
        // NSLog(@"self.scroller.contentSize:%@",NSStringFromCGSize(self.scroller.contentSize));
        [self.scroller setContentOffset:CGPointZero animated:NO];
        
        [self.scroller scrollRectToVisible:puntoDiPartenza animated:NO];
        [self.scroller setScrollEnabled:(count!=1)];
}

#pragma mark - Missal SCROLLView delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView == scroller)
    {
        CGFloat pageWidth = self.scroller.frame.size.width;
        int currentPage = floor((self.scroller.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.pager.currentPage = currentPage;
        
        CGRect frame;
        frame.origin.x = self.scroller.frame.size.width * self.pager.currentPage;
        frame.origin.y = 0;
        frame.size = self.scroller.frame.size;
        [self.scroller scrollRectToVisible:frame animated:YES];
        
        }
}
    
-(IBAction)changePage:(UIPageControl *)aPageControl
    {
        // NSLog(@"changePage:%li",(long)self.pager.currentPage);
        CGRect frame;
        frame.origin.x = self.scroller.frame.size.width * self.pager.currentPage;
        frame.origin.y = 0;
        frame.size = self.scroller.frame.size;
        [self.scroller scrollRectToVisible:frame animated:YES];
    }
    

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    // NSLog(@"will Rotate");
     [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    // NSLog(@"did Rotate");
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}


#pragma mark -
#pragma mark ROTELLA INVIEW
//=========================
//ROTELLA INVIEW
//=========================
-(void)AttivaRotellaMain{
    // NSLog(@"AttivaRotellaMain");
    [self.btn_home setHidden:YES];
    [rotella_inview startAnimating];
    [rotella_inview setHidden:NO];
}

BOOL rimuovendoRotellaMissal = NO;

-(void)rimuoviRotellaInView{
    if(rimuovendoRotellaMissal) return;
    rimuovendoRotellaMissal = YES;
    [self performSelectorOnMainThread:@selector(rimuoviRotellaMM) withObject:nil waitUntilDone:NO];
}
-(void)rimuoviRotellaMM{
    [rotella_inview startAnimating];
    [rotella_inview setHidden:NO];
    [self performSelector:@selector(rimuoviRotellaDelay) withObject:nil afterDelay:0.2];
}

-(void)rimuoviRotellaSenzaDelay{
    [self performSelectorOnMainThread:@selector(rimuoviRotellaDelay) withObject:nil waitUntilDone:NO];
}

-(void)rimuoviRotellaDelay{
    
    [rotella_inview setHidden:YES];
    [rotella_inview stopAnimating];
    [self.btn_home setHidden:NO];
    rimuovendoRotellaMissal = NO;
}





#pragma mark - Error CLass
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [super viewDidUnload];
}




//=========================
// END CLASS
//=========================

@end
