//
//  MessaleOrdinario.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "Item.h"


@interface MessaleOrdinario : BaseController<UIScrollViewDelegate>

@property (strong, nonatomic) NSMutableArray *lista;

//Messale GUI
@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (strong, nonatomic) IBOutlet UILabel *titolo;
@property (strong, nonatomic) IBOutlet UIImageView *sfondo_master;
@property (strong, nonatomic) IBOutlet UIView *black_mask;
@property (strong, nonatomic) IBOutlet UILabel *day_label;
 
    
@property (strong, nonatomic) Item *item_ordinario;

- (id)initWithItem:(Item*)i;


@end
