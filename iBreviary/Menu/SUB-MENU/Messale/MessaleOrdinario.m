//
//  MessaleOrdinario.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "MessaleOrdinario.h"
#import "Day.h"
#import "Fix.h"
#import "Home.h"

@implementation MessaleOrdinario
    
//Home istance
@synthesize item_ordinario, lista;
//Messale GUI
@synthesize tabella, titolo, sfondo_master;
@synthesize black_mask, day_label;


BOOL init_Ordinario_done = NO;

#pragma mark - INIT method
//=========================
// INIT
//=========================
- (id)initWithItem:(Item*)i{
    
    //iPhone4
    NSString *nibNameOrNil = @"MessaleOrdinario";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"MessaleOrdinario_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) nibNameOrNil = @"MessaleOrdinario5";
    

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        item_ordinario = i;
        
    }
    return self;
}


#pragma mark - DIDLOAD method
//=========================
// DIDLOAD method
//=========================
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self gui];
    init_Ordinario_done = YES;
    if(self.isIpad)[[NSNotificationCenter defaultCenter] addObserver:self
                                                            selector:@selector(controllaMessOrdOrientationIPAD)
                                                                name:DID_ROTATE object:nil];
    
}

-(void)controllaMessOrdOrientationIPAD{
    [sfondo_master setImage:[UIImage imageNamed:self.isLand?@"back_menu_002L.png":@"back_menu_002P.png"]];
}

    
-(void)initLista{
    Fix *f = [Fix getFixForLang:[self.defaults objectForKey:LINGUA]];
    lista = [[NSMutableArray alloc] initWithArray:f.ancore_array];
    // NSLog(@"day:%@ lista%@",[Home getCurrentDay],lista);
}

- (void)gui
{
    
    [self titleOnNavBar:NSLocalizedString(@"Mass' Order", nil)];
    
    [day_label setFont:[UIFont fontWithName:FONT_ROMAN_ANTIQUE size:15]];
    [titolo setFont:[UIFont fontWithName:FONT_ROMAN_ANTIQUE size:15]];
    
    [titolo setText:NSLocalizedString([[self.defaults objectForKey:ROOT_TS_INDEX] objectAtIndex:1], nil)];
}


-(IBAction)goHome:(UIButton*)sender{
    // NSLog(@"goHome!");
    [self.navigationController popToRootViewControllerAnimated:YES];
    [DDMenuController closeMenuWithCompletion:
                                                    ^{
                                                        [Home mostraHome];
                                                    }];
}

-(IBAction)goBack:(UIButton*)sender{
    // NSLog(@"goBack!");
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - LifeCyrcle
//=========================
// LIFECYRCLE
//=========================
#pragma mark Appear

- (void)viewWillAppear:(BOOL)animated{
    // NSLog(@"viewWillAppear!");
	[super viewWillAppear:animated];
    if(self.isIpad) [self controllaMessOrdOrientationIPAD];

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //Sfondo
    BOOL reverse = ([self.defaults integerForKey:CSS]==2) && [Home isContentVisible];
    [day_label setTextColor:reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.4]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9]];
    [day_label setText:[[[Home sharedInstance] day] day_label]];
    [black_mask setHidden:!reverse];
    if(init_Ordinario_done) {
        init_Ordinario_done = NO;
        [self initLista];
        [tabella reloadData];
    }
}

#pragma mark Disappear

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

#pragma mark -
#pragma mark --------> Table view
#pragma mark Header/Footer
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// HEADER SESSION
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

// FOOTER SESSION
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

#pragma mark ROW SIZE

//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [lista count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50.0;
}

#pragma mark CELL FOR ROW
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellMessaleOrdinario";
    // NSLog(@"cellForRowAtIndexPath:%@",indexPath);
    BOOL reverse = ([self.defaults integerForKey:CSS]==2 && [Home isContentVisible]);

    UIColor *coloreCapoLettera = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.48]:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.9];
    UIColor *colore = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.45]:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.9];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		UIView* backgroundView = [[UIView alloc ] initWithFrame:CGRectZero ];
		backgroundView.backgroundColor = [UIColor clearColor];
		cell.backgroundView.alpha = 0;
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.alpha = 0;
		cell.contentView.backgroundColor = [UIColor clearColor];
		[cell setBackgroundView: backgroundView];
        cell.textLabel.backgroundColor =[UIColor clearColor];
        cell.textLabel.text = @"";
    }
	//Colore Label RGB a=alpha;
	UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, 320, 50)];
	Label.backgroundColor = [UIColor clearColor];
    
    UILabel *capo = [[UILabel alloc] initWithFrame:CGRectMake(10, 1, 50, 50)];
	[capo setBackgroundColor:[UIColor clearColor]];
	[capo setFont: [UIFont fontWithName:FONT_EILEENCAPS size: 28]];
    [capo setTextColor:coloreCapoLettera];
    [capo setShadowColor:reverse?[UIColor clearColor]:[UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:0.2]];
    capo.shadowOffset = CGSizeMake(1,2);
	[capo setTextAlignment:NSTextAlignmentLeft];
    
    //->TESTO ANCORA
    NSString *testo = NSLocalizedString([lista objectAtIndex:indexPath.row], nil);
    if(indexPath.row == (lista.count-2)){
        Fix *f = [Fix getFixForLang:[self.defaults objectForKey:LINGUA]];
        testo = [f.celebration_conclusion_rites titolo];
    }
    
	[capo setText:[testo substringWithRange:NSMakeRange(0,1)]];
    
	UILabel *resto = [[UILabel alloc] initWithFrame:CGRectMake(36, 12, 215, 35)];
	resto.backgroundColor = [UIColor clearColor];
	[resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size:22]];
	[resto setAdjustsFontSizeToFitWidth:YES];
    [resto setMinimumScaleFactor:0.8];
	resto.textAlignment = NSTextAlignmentLeft;
	[resto setTextColor:colore];
	
	resto.lineBreakMode = NSLineBreakByClipping;
	resto.numberOfLines = 1;
    [resto setShadowColor:capo.shadowColor];
	resto.shadowOffset = CGSizeMake(0,2);
	
	resto.text = [[testo substringFromIndex:(NSUInteger)1] lowercaseString];
        
    if([[[Home getCurrentDay] linguaGiorno] alignment_right])
    {
        CGFloat marginLeft = 5;
        [resto setText:testo];
        [capo setText:@""];
        [resto setNumberOfLines:1];
        [resto setAdjustsFontSizeToFitWidth:YES];
        [resto setTextAlignment:NSTextAlignmentRight];
        [resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size: 26]];
        [resto setFrame:CGRectMake(marginLeft, Label.frame.origin.y, 255-marginLeft, Label.frame.size.height)];
    }


	
	UIImageView *selectedBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
	[selectedBackgroundView setImage:[UIImage imageNamed:@"cellSelect80.png"]];
	[cell setSelectedBackgroundView:selectedBackgroundView];
	[Label addSubview:capo];
	[Label addSubview:resto];
	[cell.contentView addSubview:Label];
	
	return cell;

}

#pragma mark DELEGATE (tableView)

// COMANDA DETAILVIEW
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self performSelector:@selector(deselectRow:) withObject:indexPath afterDelay:0.5];
    NSString *ancora = [lista objectAtIndex:indexPath.row];
    UIWebView *webView = [Home sharedInstance].webContent;
    // NSLog(@"ancora(%@)",ancora);
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"scrollToAnchor('%@');",ancora]];
    [DDMenuController closeMenuWithCompletion:nil];
}


-(void)deselectRow:(NSIndexPath*)indexPath{
    [tabella deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark --------> FINE - Table view
    
    
#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    // NSLog(@"will Rotate");
     [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    // NSLog(@"did Rotate");
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}



#pragma mark - Error CLass
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [super viewDidUnload];
}




//=========================
// END CLASS
//=========================

@end
