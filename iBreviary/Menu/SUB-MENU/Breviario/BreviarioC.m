//
//  BreviarioC.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "BreviarioC.h"
#import "Day.h"
#import "Fix.h"
#import "Home.h"

@implementation BreviarioC
@synthesize tabella,sfondo_master,day_label,lista;
@synthesize titolo, black_mask,breviario_item;

#pragma mark - INIT method
//=========================
// INIT
//=========================
- (id)initWithBreviario:(BreviarioObject*)b
{
    NSString *nibNameOrNil;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) nibNameOrNil = @"BreviarioC_ipad";
    else
        nibNameOrNil = ([[UIScreen mainScreen] bounds].size.height == 568)?@"BreviarioC5":@"BreviarioC";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        breviario_item = b;
        
    }
    return self;
}


#pragma mark - DIDLOAD method
//=========================
// DIDLOAD method
//=========================
- (void)viewDidLoad
{
    // NSLog(@"BreviarioC:viewDidLoad %@",self.breviario_item);
    
    if(self.isIpad)[[NSNotificationCenter defaultCenter] addObserver:self
                                                            selector:@selector(controllaBreviarioCOrientationIPAD)
                                                                name:DID_ROTATE object:nil];

    
    [super viewDidLoad];
    
    [self titleOnNavBar:NSLocalizedString([[self.defaults objectForKey:ROOT_TS_INDEX] objectAtIndex:0], nil)];


    
    lista = [[NSMutableArray alloc] initWithArray:[self.defaults objectForKey:BREVIARIO_COMUNE_INDEX]];
    // NSLog(@"day:%@ lista%@",[Home getCurrentDay],lista);
    

    [self configureView];
}


-(void)controllaBreviarioCOrientationIPAD{
    [sfondo_master setImage:[UIImage imageNamed:self.isLand?@"back_menu_002L.png":@"back_menu_002P.png"]];
}


- (void)configureView
{
    [day_label setFont:[UIFont fontWithName:FONT_ROMAN_ANTIQUE size:15]];
    [titolo setFont:[UIFont fontWithName:FONT_ROMAN_ANTIQUE size:15]];
    
    [titolo setText:NSLocalizedString([[self.defaults objectForKey:ROOT_TS_INDEX] objectAtIndex:0], nil)];
    
}


-(IBAction)goHome:(UIButton*)sender{
    // NSLog(@"goHome!");
    [self.navigationController popToRootViewControllerAnimated:YES];
    [DDMenuController closeMenuWithCompletion:
                                                    ^{  // NSLog(@"Settare Home in Home!!");
                                                        [Home mostraHome];
                                                    }];
}

-(IBAction)goBack:(UIButton*)sender{
    // NSLog(@"goBack!");
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - LifeCyrcle
//=========================
// LIFECYRCLE
//=========================
#pragma mark Appear

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    //Sfondo
    // NSLog(@"BreviarioC:viewWillAppear!");
    BOOL reverse = ([self.defaults integerForKey:CSS]==2) && [Home isContentVisible];
    [day_label setTextColor:reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.4]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9]];
    [day_label setText:breviario_item.title];
    [black_mask setHidden:!reverse];
    }

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //Sfondo
    // NSLog(@"BreviarioC:viewDidAppear!");
    BOOL reverse = ([self.defaults integerForKey:CSS]==2) && [Home isContentVisible];
    [day_label setTextColor:reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.4]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9]];
    [day_label setText:breviario_item.title];
    [black_mask setHidden:!reverse];
    [tabella reloadData];

}

#pragma mark Disappear

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

#pragma mark -
#pragma mark --------> Table view
#pragma mark Header/Footer
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// HEADER SESSION
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

// FOOTER SESSION
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

#pragma mark ROW

//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50.0;
}

#pragma mark CELL
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellBreviarioC";
    // NSLog(@"cellForRowAtIndexPath:%@",indexPath);
    BOOL reverse = ([self.defaults integerForKey:CSS]==2 && [Home isContentVisible]);
    
    UIColor *coloreCapoLettera = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.48]:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.9];
    UIColor *colore = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.45]:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.9];

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		UIView* backgroundView = [[UIView alloc ] initWithFrame:CGRectZero ];
		backgroundView.backgroundColor = [UIColor clearColor];
		cell.backgroundView.alpha = 0;
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.alpha = 0;
		cell.contentView.backgroundColor = [UIColor clearColor];
		[cell setBackgroundView: backgroundView];
        cell.textLabel.backgroundColor =[UIColor clearColor];
        cell.textLabel.text = @"";
    }
	//Colore Label RGB a=alpha;
	UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, 320, 50)];
	Label.backgroundColor = [UIColor clearColor];
    
    UILabel *capo = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 50, 50)];
	[capo setBackgroundColor:[UIColor clearColor]];
	[capo setFont: [UIFont fontWithName:FONT_EILEENCAPS size: 35]];
    [capo setTextColor:coloreCapoLettera];
    [capo setShadowColor:reverse?[UIColor clearColor]:[UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:0.2]];
    capo.shadowOffset = CGSizeMake(1,2);
	[capo setTextAlignment:NSTextAlignmentLeft];
    
    NSString *testo = NSLocalizedString([lista objectAtIndex:indexPath.row], nil);
	[capo setText:[testo substringWithRange:NSMakeRange(0,1)]];
    
    UILabel *resto = [[UILabel alloc] initWithFrame:CGRectMake(42, self.isIos7orUpper?14:12, 200, 35)];
	resto.backgroundColor = [UIColor clearColor];
	[resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size:25]];
	resto.textAlignment = NSTextAlignmentLeft;
	[resto setTextColor:colore];
	
	resto.lineBreakMode = NSLineBreakByClipping;
	resto.numberOfLines = 1;
    [resto setShadowColor:capo.shadowColor];
	resto.shadowOffset = CGSizeMake(0,2);
	
	resto.text = [testo substringFromIndex:(NSUInteger)1];
    
    if([NSLocalizedString(@"checklingua",nil) isEqualToString:@"ar"])
    {
        CGFloat marginLeft = 5;
        [resto setText:testo];
        [capo setText:@""];
        [resto setBackgroundColor:[UIColor clearColor]];
        [resto setTextAlignment:NSTextAlignmentRight];
        [resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size: 26]];
        [resto setFrame:CGRectMake(marginLeft, Label.frame.origin.y, 255-marginLeft, Label.frame.size.height)];
    }

	
	UIImageView *selectedBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
	[selectedBackgroundView setImage:[UIImage imageNamed:@"cellSelect80.png"]];
	[cell setSelectedBackgroundView:selectedBackgroundView];
	[Label addSubview:capo];
	[Label addSubview:resto];
	[cell.contentView addSubview:Label];
	
	return cell;

}

#pragma mark DELEGATE (tableView)

// COMANDA DETAILVIEW
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self performSelector:@selector(deselectRow:) withObject:indexPath afterDelay:0.5];
    
    switch (indexPath.row) {
        case 0:
        {
            // NSLog(@"Primi Vespri");
            [DDMenuController closeMenuWithCompletion:
             ^{
                 [Home mostraItem:[breviario_item first_vespers]];
             }];
            break;
        }
        case 1:
        {
            // NSLog(@"Ufficio Letture");
            [DDMenuController closeMenuWithCompletion:
             ^{
                 [Home mostraItem:[breviario_item officeofreading]];
             }];
            break;
        }
        case 2:
        {
            // NSLog(@"Lodi");
            [DDMenuController closeMenuWithCompletion:
             ^{
                 [Home mostraItem:[breviario_item lauds]];
             }];
            break;
        }
        case 3:
        {
            // NSLog(@"Ora Media");
            [DDMenuController closeMenuWithCompletion:
             ^{
                 [Home mostraItem:[breviario_item daytimeprayers]];

             }];
            break;
        }
        case 4:
        {
            // NSLog(@"Vespri");
            [DDMenuController closeMenuWithCompletion:
             ^{
                 [Home mostraItem:[breviario_item vespers]];
             }];
            break;
        }
        default:
            break;
    }

    //if(reverse)[tabella performSelector:@selector(reloadData) withObject:nil afterDelay:0.8];
}


-(void)deselectRow:(NSIndexPath*)indexPath{
    [tabella deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark --------> FINE - Table view

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    // NSLog(@"will Rotate");
     [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    // NSLog(@"did Rotate");
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}



#pragma mark - Error CLass
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [super viewDidUnload];
}




//=========================
// END CLASS
//=========================

@end
