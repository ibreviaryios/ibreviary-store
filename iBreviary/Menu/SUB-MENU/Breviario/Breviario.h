//
//  Breviario.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "TabellaMaster.h"
#import "BreviarioObject.h"


@class Home;

@interface Breviario : BaseController<UIScrollViewDelegate>

//Home istance
@property (strong, readonly) Home *home;
@property (strong, nonatomic) NSMutableArray *lista;
@property (strong, nonatomic) NSMutableArray *listaBreviary;
@property (strong, nonatomic) BreviarioObject *breviarioSelected;

//Breviario GUI
@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (strong, nonatomic) IBOutlet UILabel *titolo;
@property (strong, nonatomic) IBOutlet UIImageView *sfondo_master;
@property (strong, nonatomic) IBOutlet UIView *black_mask;
@property (strong, nonatomic) IBOutlet UILabel *day_label;
@property (strong, nonatomic) IBOutlet UIImageView *ghirigoro;


    
//Comuni Controller
@property (strong, nonatomic) TabellaMaster *controllerLista;
    

//---Breviario SCROLLER
@property (strong, nonatomic) IBOutlet UIView *switcherView;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UIPageControl *pager;
@property (nonatomic) int currentMissalPos;

//ROTELLA INVIEW
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *rotella_inview;


- (id)initWithMissalPos:(int)pos;


@end
