//
//  Breviario.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "Breviario.h"
#import "Day.h"
#import "Fix.h"
#import "Home.h"
#import "MenuViewController.h"

@implementation Breviario
    
//Home istance
@synthesize home, lista, listaBreviary, breviarioSelected,currentMissalPos;
//Breviario GUI
@synthesize tabella, titolo, sfondo_master;
@synthesize black_mask, day_label,ghirigoro;

//Comuni Controller
@synthesize controllerLista;
//---Breviario SCROLLER
@synthesize switcherView, scroller, pager;

//ROTELLA INVIEW
@synthesize rotella_inview;

BOOL init_brev_done = NO;

#pragma mark - INIT method
//=========================
// INIT
//=========================
- (id)initWithMissalPos:(int)pos
{
    //iPhone4
    NSString *nibNameOrNil = @"Breviario";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"Breviario_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"Breviario5";
    if(IS_IPHONE_6) nibNameOrNil = @"Breviario5";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"Breviario5";

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        home = [Home sharedInstance];
        currentMissalPos = pos;
        
    }
    return self;
}


#pragma mark - DIDLOAD method
//=========================
// DIDLOAD method
//=========================
- (void)viewDidLoad
{
    [super viewDidLoad];
    // NSLog(@"Breviaro:didLOAD:%@",self.navigationController);
    if(self.isIpad)[[NSNotificationCenter defaultCenter] addObserver:self
                                                            selector:@selector(controllaBreviarioOrientationIPAD)
                                                                name:DID_ROTATE object:nil];
    
    [self gui];
    init_brev_done = YES;
}

-(void)controllaBreviarioOrientationIPAD{
    [sfondo_master setImage:[UIImage imageNamed:self.isLand?@"back_menu_002L.png":@"back_menu_002P.png"]];

    int i = (int)[lista count];
    CGRect frame;
    frame = CGRectMake(-5, self.view.frame.size.height-(50*i)-100, 270, 50*i);
    [tabella setFrame:frame];
}

-(void)initLista{
    [self attivaRotellaInView];

    lista = [[NSMutableArray alloc] initWithArray:[self.defaults objectForKey:BREVIARIO_INDEX]];
    if([self.defaults boolForKey:COMPLINE_YESTERDAY])
    {
        [lista insertObject:NSLocalizedString(@"Compline_yesterday", nil) atIndex:5];
     }
    
    listaBreviary = [[NSMutableArray alloc] initWithArray:[[Home getCurrentDay] listaBreviari]];
    // NSLog(@"initLista: %@",[[Home getCurrentDay] listaBreviari]);
    
    [self buildBreviarioScroller];
    [ghirigoro setHidden:NO];
    [tabella reloadData];
}


- (void)gui
{
    [pager setHidden:YES];
    
    //Rotella
    [rotella_inview setHidden:NO];
    
    [self titleOnNavBar:NSLocalizedString([[self.defaults objectForKey:ROOT_TS_INDEX] objectAtIndex:0], nil)];
    
    [self.day_label setText:@""];
}


-(IBAction)goHome:(UIButton*)sender{
    // NSLog(@"goHome!");
    [self.navigationController popToRootViewControllerAnimated:YES];
    [MenuViewController setCurrentBreviaryPos:[NSNumber numberWithInteger:self.pager.currentPage]];
    [DDMenuController closeMenuWithCompletion:
                                                    ^{  // NSLog(@"Settare Home in Home!!");
                                                        [Home mostraHome];
                                                    }];
}

-(IBAction)goBack:(UIButton*)sender{
    // NSLog(@"goBack!");
    [self.navigationController popViewControllerAnimated:YES];
    [MenuViewController setCurrentBreviaryPos:[NSNumber numberWithInteger:self.pager.currentPage]];

}


#pragma mark - LifeCyrcle
//=========================
// LIFECYRCLE
//=========================
#pragma mark Appear

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    // NSLog(@"Breviario:WillAppear");
 }

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // NSLog(@"Breviario:viewDidAppear");
    if(![self.defaults boolForKey:TUTORIAL_MENU_LIBRO]){
        [self customTutorialBookMenuAlert];
    }
    if(init_brev_done) {
        init_brev_done = NO;
        [self initLista];
        [tabella reloadData];
        [self rimuoviRotellaInView];
    }

    //Sfondo
    BOOL reverse = ([self.defaults integerForKey:CSS]==2) && [Home isContentVisible];
    [day_label setTextColor:reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.4]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9]];
    [day_label setText:[[[Home sharedInstance] day] day_label]];
    [black_mask setHidden:!reverse];
    // NSLog(@"BREVIARIO-viewDidAppear! css:%i home:%i blackHidden:%i",([self.defaults integerForKey:CSS]==2), [Home isContentVisible], black_mask.hidden);
    
    if(self.isIpad) [self controllaBreviarioOrientationIPAD];
}

#pragma mark Disappear

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

#pragma mark -
#pragma mark --------> Table view
#pragma mark Header/Footer
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// HEADER SESSION
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

// FOOTER SESSION
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

#pragma mark ROW SIZE

//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int i = (int)[lista count];
    if(self.isIpad)
    {
        [self controllaBreviarioOrientationIPAD];
        return i;
    }
    CGRect frame;
    if(!self.isIphone5) {
        frame = CGRectMake(-5, self.view.frame.size.height-(50*i)-40, 270, 50*i);
        if(lista.count>6 && listaBreviary.count>1)
        {
            [ghirigoro setHidden:YES];
            [day_label setHidden:YES];
            frame = CGRectMake(-5, self.view.frame.size.height-(50*i)-5, 270, 50*i);
        }
    }
    else frame = CGRectMake(-5, self.view.frame.size.height-(50*i)-57, 270, 50*i);

    if(!self.isIos7orUpper && !self.isIphone5)
    {
        [ghirigoro setHidden:YES];
        [day_label setHidden:YES];
        [switcherView setCenter:CGPointMake(switcherView.center.x, switcherView.center.y+1)];
        frame = CGRectMake(-5, self.view.frame.size.height-(50*i), 270, 50*i);
    }

    [tabella setFrame:frame];
    return [lista count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50.0;
}


#pragma mark CELL FOR ROW
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellBreviario";
    // NSLog(@"cellForRowAtIndexPath:%@",indexPath);
    BOOL reverse = ([self.defaults integerForKey:CSS]==2 && [Home isContentVisible]);

    UIColor *coloreCapoLettera = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.48]:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.9];
    UIColor *colore = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.45]:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.9];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		UIView* backgroundView = [[UIView alloc ] initWithFrame:CGRectZero ];
		backgroundView.backgroundColor = [UIColor clearColor];
		cell.backgroundView.alpha = 0;
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.alpha = 0;
		cell.contentView.backgroundColor = [UIColor clearColor];
		[cell setBackgroundView: backgroundView];
        cell.textLabel.backgroundColor =[UIColor clearColor];
        cell.textLabel.text = @"";
    }
	//Colore Label RGB a=alpha;
	UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, 270, 50)];
	Label.backgroundColor = [UIColor clearColor];
    
    UILabel *capo = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 50, 50)];
	[capo setBackgroundColor:[UIColor clearColor]];
	[capo setFont: [UIFont fontWithName:FONT_EILEENCAPS size: 35]];
    [capo setTextColor:coloreCapoLettera];
    [capo setShadowColor:reverse?[UIColor clearColor]:[UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:0.2]];
    capo.shadowOffset = CGSizeMake(1,2);
	[capo setTextAlignment:NSTextAlignmentLeft];
    
    NSString *testo = NSLocalizedString([lista objectAtIndex:indexPath.row], nil);
	[capo setText:[testo substringWithRange:NSMakeRange(0,1)]];
    
	UILabel *resto = [[UILabel alloc] initWithFrame:CGRectMake(42, 14, 220, 35)];
	resto.backgroundColor = [UIColor clearColor];
	[resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size:25]];
	resto.textAlignment = NSTextAlignmentLeft;
	[resto setTextColor:colore];
	
	resto.lineBreakMode = NSLineBreakByClipping;
	resto.numberOfLines = 1;
    [resto setShadowColor:capo.shadowColor];
	resto.shadowOffset = CGSizeMake(0,2);
	
	resto.text = [testo substringFromIndex:(NSUInteger)1];
    
    if([NSLocalizedString(@"checklingua",nil) isEqualToString:@"ar"])
    {
        CGFloat marginLeft = 5;
        [resto setText:testo];
        [capo setText:@""];
        [resto setBackgroundColor:[UIColor clearColor]];
        [resto setTextAlignment:NSTextAlignmentRight];
        [resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size: 26]];
        [resto setFrame:CGRectMake(marginLeft, Label.frame.origin.y, 255-marginLeft, Label.frame.size.height)];
    }
	
	UIImageView *selectedBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
	[selectedBackgroundView setImage:[UIImage imageNamed:@"cellSelect80.png"]];
	[cell setSelectedBackgroundView:selectedBackgroundView];
	[Label addSubview:capo];
	[Label addSubview:resto];
	[cell.contentView addSubview:Label];
    
    Fix *fix = [Fix getFixForLang:[self.defaults objectForKey:LINGUA]];
    
    if((indexPath.row == 0 && [[[[listaBreviary objectAtIndex:self.pager.currentPage] officeofreading] contenuto] isEqualToString:@""]) ||
       (indexPath.row == 1 && [[[[listaBreviary objectAtIndex:self.pager.currentPage] lauds] contenuto] isEqualToString:@""]) ||
       (indexPath.row == 2 && [[[[listaBreviary objectAtIndex:self.pager.currentPage] daytimeprayers] contenuto] isEqualToString:@""]) ||
       (indexPath.row == 3 && [[[[listaBreviary objectAtIndex:self.pager.currentPage] vespers] contenuto] isEqualToString:@""]) ||
       (indexPath.row == 4 && [[[[listaBreviary objectAtIndex:self.pager.currentPage] compline] contenuto] isEqualToString:@""]) ||
       (indexPath.row == 5 && fix.breviary_comuni_array.count==0 && ![self.defaults boolForKey:COMPLINE_YESTERDAY]) ||
       (indexPath.row == 6 && fix.breviary_comuni_array.count==0 && [self.defaults boolForKey:COMPLINE_YESTERDAY]))
    {
        BOOL leftToRight = !([NSLocalizedString(@"checklingua",nil) isEqualToString:@"ar"]);
        UIView *selectView = [[UIView alloc] initWithFrame:Label.frame];
        //[selectedBackgroundView setBackgroundColor:[[UIColor blueColor] colorWithAlphaComponent:0.1]];
        CGRect frame = leftToRight?CGRectMake(0, 5, 50, 40):CGRectMake(200, 5, 50, 40);
        UIImageView *selectedBackgroundView = [[UIImageView alloc] initWithFrame:frame];
        [selectedBackgroundView setImage:[UIImage imageNamed:@"x@2x.png"]];
        //[selectedBackgroundView setBackgroundColor:[UIColor greenColor]];
        [selectedBackgroundView setContentMode:UIViewContentModeScaleAspectFit];
        [selectedBackgroundView setAlpha:0.6];
        [selectView addSubview:selectedBackgroundView];
        [cell setSelectedBackgroundView:selectView];
    }

	
	return cell;

}

#pragma mark DELEGATE (tableView)

// COMANDA DETAILVIEW
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self performSelector:@selector(deselectRow:) withObject:indexPath afterDelay:0.5];
    
    switch (indexPath.row) {
        case 0:
        {
            // NSLog(@"Ufficio Letture");
            Item *item = [[listaBreviary objectAtIndex:self.pager.currentPage] officeofreading];
            if([item.contenuto isEqualToString:@""]) return;
            [Home mostraItem:item];
            [DDMenuController closeMenuWithCompletion:^{}];
            break;
        }
        case 1:
        {
            // NSLog(@"Lodi");
            Item *item = [[listaBreviary objectAtIndex:self.pager.currentPage] lauds];
            if([item.contenuto isEqualToString:@""]) return;
            [Home mostraItem:item];
            [DDMenuController closeMenuWithCompletion:^{}];
            break;
        }
        case 2:
        {
            // NSLog(@"Ora Media");
            Item *item = [[listaBreviary objectAtIndex:self.pager.currentPage] daytimeprayers];
            if([item.contenuto isEqualToString:@""]) return;
            [Home mostraItem:item];
            [DDMenuController closeMenuWithCompletion:^{}];
           break;
        }
        case 3:
        {
            // NSLog(@"Vespri");
            Item *item = [[listaBreviary objectAtIndex:self.pager.currentPage] vespers];
            if([item.contenuto isEqualToString:@""]) return;
            [Home mostraItem:item];
            [DDMenuController closeMenuWithCompletion:^{}];
           break;
        }
        case 4:
        {
            // NSLog(@"Compieta");
            Item *item = [[listaBreviary objectAtIndex:self.pager.currentPage] compline];
            if([item.contenuto isEqualToString:@""]) return;
            [Home mostraItem:item];
            [DDMenuController closeMenuWithCompletion:^{}];
           break;
        }
        case 5:
        {
            if([self.defaults boolForKey:COMPLINE_YESTERDAY])
            {
             // NSLog(@"CompietaYesterday");
            Item *i= [[Home getCurrentDay] compline_yesterday];
            [i setContenuto:[NSString stringWithFormat:@"<div class=\"sezioneCompietaIeri\">%@</div>%@",i.time,i.contenuto]];
            [Home mostraItem:i];
            [DDMenuController closeMenuWithCompletion:^{}];
            }
            else
            {
            // NSLog(@"Comuni");
            Fix *fix = [Fix getFixForLang:[[[Home getCurrentDay] linguaGiorno] sigla]];
            if(fix.breviary_comuni_array.count > 0)
                {
                controllerLista = nil;
                controllerLista = [[TabellaMaster alloc] initWithList:fix.breviary_comuni_array andTitle:NSLocalizedString(@"Common", nil)];
                [self.navigationController pushViewController:controllerLista animated:YES];
                }
            }
            break;
        }

        case 6:
        {
            // NSLog(@"Comuni");
            Fix *fix = [Fix getFixForLang:[[[Home getCurrentDay] linguaGiorno] sigla]];
            if(fix.breviary_comuni_array.count > 0)
                {
                controllerLista = nil;
                controllerLista = [[TabellaMaster alloc] initWithList:fix.breviary_comuni_array andTitle:   NSLocalizedString(@"Common", nil)];
                [self.navigationController pushViewController:controllerLista animated:YES];
                }
            break;
        }
        default:
            break;
    }

    //if(reverse)[tabella performSelector:@selector(reloadData) withObject:nil afterDelay:0.8];
}


-(void)deselectRow:(NSIndexPath*)indexPath{
    [tabella deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark --------> FINE - Table view
    
    
#pragma mark -
#pragma mark SCROLLER
//=========================
// SCROLLER BREVIARIO
//=========================
-(void)buildBreviarioScroller
    {
        int count = (int)[listaBreviary count];
        // NSLog(@"buildBreviarioScroller[%i]",count);
        
        
        for(UIView* subview in [self.scroller subviews]) //per cancellare tutte le superviste (reset)
        [subview removeFromSuperview];
        
        [switcherView setHidden:(count==1)];
        if(count==1) return;
        
        UIView *viewPrepered = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.scroller.frame.size.width * count, self.scroller.frame.size.height)];
        // NSLog(@"viewPrepered:%@",NSStringFromCGSize(viewPrepered.frame.size));
        [viewPrepered.layer setMasksToBounds:YES];
        
        for (int i = 0; i < count; i++) {
            BreviarioObject *row = [listaBreviary objectAtIndex:i];
            
            CGRect frame;
            frame.origin.x = self.scroller.frame.size.width*i;
            frame.origin.y = 0;
            frame.origin.x += 2;
            frame.size = CGSizeMake(self.scroller.frame.size.width-4, self.scroller.frame.size.height);
            
            
            UIView *subTitolo_tex = [[UIView alloc] initWithFrame:frame];
            CAGradientLayer *gradientLayer = [CAGradientLayer layer];
            gradientLayer.frame = subTitolo_tex.bounds;
            gradientLayer.colors = [NSArray arrayWithObjects:(id)[[[UIColor whiteColor] colorWithAlphaComponent:0.2]CGColor], (id)[[UIColorFromRGB(0Xffffff) colorWithAlphaComponent:0.5]CGColor], nil];
            [subTitolo_tex.layer insertSublayer:gradientLayer atIndex:0];
            [subTitolo_tex.layer setBorderColor:[[UIColorFromRGB(0Xd89c57) colorWithAlphaComponent:0.5]CGColor]];
            [subTitolo_tex.layer setBorderWidth:2.0];
            [subTitolo_tex setAlpha:0.8];

            [viewPrepered addSubview:subTitolo_tex];
                        

            
            NSString *titolo_text = row.subtitle?[NSString stringWithFormat:@"%@ - %@",row.title,row.subtitle]:(row.type?[NSString stringWithFormat:@"%@ - %@",row.title,row.type]:row.title);
            // NSLog(@"title:%@",titolo_text);
            UILabel *label_titolo = [[UILabel alloc] initWithFrame:frame];
            [label_titolo setBackgroundColor:[UIColor clearColor]];
            [label_titolo setTextColor:UIColorFromRGB(0X483627)];
            [label_titolo setNumberOfLines:2];
            [label_titolo setMinimumScaleFactor:0.6];
            [label_titolo setLineBreakMode:NSLineBreakByTruncatingTail];
            [label_titolo setTextAlignment:NSTextAlignmentCenter];
            [label_titolo setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:14]];
            [label_titolo setText:[titolo_text uppercaseString]];
            [label_titolo setAdjustsFontSizeToFitWidth:YES];
            
            [viewPrepered addSubview:label_titolo];
        }
        
        [pager setNumberOfPages:count];
        [pager setHidden:(count==1)];
        [self.scroller addSubview:viewPrepered];

        
        CGRect puntoDiPartenza = CGRectMake(0, 0, 250, 100);

        if(currentMissalPos !=-1)
        {
            Day *d = [Home getCurrentDay];
            int breviaryPos = [d intForBreviaryFromMissalPos:currentMissalPos];
            d = nil;
            puntoDiPartenza.origin.x = self.scroller.frame.size.width * breviaryPos;
            [pager setCurrentPage:breviaryPos];
            puntoDiPartenza.origin.y = 0;
            puntoDiPartenza.size = self.scroller.frame.size;
        }

        [day_label setText:[[[Home sharedInstance] day] day_label]];


        self.scroller.pagingEnabled = YES;
        self.scroller.contentSize = CGSizeMake(self.scroller.frame.size.width * count, self.scroller.frame.size.height);
        // NSLog(@"self.scroller.contentSize:%@",NSStringFromCGSize(self.scroller.contentSize));
        [self.scroller setContentOffset:CGPointZero animated:NO];
        
        [self.scroller scrollRectToVisible:puntoDiPartenza animated:NO];
        [self.scroller setScrollEnabled:(count!=1)];
        
}

#pragma mark - BREVIARIO SCROLLView delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView == scroller)
    {
        CGFloat pageWidth = self.scroller.frame.size.width;
        int currentPage = floor((self.scroller.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.pager.currentPage = currentPage;
        
        CGRect frame;
        frame.origin.x = self.scroller.frame.size.width * self.pager.currentPage;
        frame.origin.y = 0;
        frame.size = self.scroller.frame.size;
        [self.scroller scrollRectToVisible:frame animated:YES];
        
        // NSLog(@"Breviario Scroller[%i]: scrollViewDidEndDecelerating:%@",(int)self.pager.currentPage,NSStringFromCGRect(frame));
    }
}
    
-(IBAction)changePage:(UIPageControl *)aPageControl
    {
        // NSLog(@"changePage:%li",(long)self.pager.currentPage);
        CGRect frame;
        frame.origin.x = self.scroller.frame.size.width * self.pager.currentPage;
        frame.origin.y = 0;
        frame.size = self.scroller.frame.size;
        [self.scroller scrollRectToVisible:frame animated:YES];
    }


#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    // NSLog(@"will Rotate");
     [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    // NSLog(@"did Rotate");
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

#pragma mark -
#pragma mark ROTELLA INVIEW
//=========================
//ROTELLA INVIEW
//=========================
-(void)attivaRotellaInView{
    // NSLog(@"attivaRotellaInView");
    [self performSelectorOnMainThread:@selector(AttivaRotellaMain) withObject:nil waitUntilDone:NO];
}
//@synthesize rotella_inview, subRotella_inview;
-(void)AttivaRotellaMain{
    // NSLog(@"AttivaRotellaMain");
    [self.btn_home setHidden:YES];
    [rotella_inview startAnimating];
    [rotella_inview setHidden:NO];
}

BOOL rimuovendo = NO;

-(void)rimuoviRotellaInView{
    if(rimuovendo) return;
    rimuovendo = YES;
    [self performSelectorOnMainThread:@selector(rimuoviRotellaMM) withObject:nil waitUntilDone:NO];
}
-(void)rimuoviRotellaMM{
    [rotella_inview startAnimating];
    [rotella_inview setHidden:NO];
    [self performSelector:@selector(rimuoviRotellaDelay) withObject:nil afterDelay:0.2];
}

-(void)rimuoviRotellaSenzaDelay{
    [self performSelectorOnMainThread:@selector(rimuoviRotellaDelay) withObject:nil waitUntilDone:NO];
}

-(void)rimuoviRotellaDelay{

    [rotella_inview setHidden:YES];
    [rotella_inview stopAnimating];
    [self.btn_home setHidden:NO];
    rimuovendo = NO;
}



#pragma mark - Error CLass
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [super viewDidUnload];
}



- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    [alertView close];
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d. CHECK:%@", (int)buttonIndex, (int)[alertView tag],self.tutorialAlertGroup.checkedCheckBoxes);
    [self.defaults setBool:[self.tutorialAlertGroup.checkedCheckBoxes firstObject] forKey:TUTORIAL_MENU_LIBRO];
    [self.defaults synchronize];
}


//=========================
// END CLASS
//=========================

@end
