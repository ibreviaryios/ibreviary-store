//
//  BreviarioC.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "BreviarioObject.h"
@class Home;

@interface BreviarioC : BaseController

@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (strong, nonatomic) IBOutlet UILabel *titolo;
@property (strong, nonatomic) IBOutlet UILabel *day_label;
@property (strong, nonatomic) IBOutlet UIImageView *sfondo_master;
@property (strong, nonatomic) NSMutableArray *lista;
@property (strong, readonly) Home *home;
@property (strong, nonatomic) IBOutlet UIView *black_mask;
@property (strong, nonatomic) BreviarioObject *breviario_item;


- (id)initWithBreviario:(BreviarioObject*)b;

@end
