//
//  TabellaMaster.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"


@class Day, BreviarioC;

@interface TabellaMaster : BaseController<UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (strong, nonatomic) IBOutlet UIImageView *sfondo_master;
@property (strong, nonatomic) IBOutlet UILabel *day_label;
@property (strong, nonatomic) NSMutableArray *lista;
@property (strong, nonatomic) NSMutableArray *listaFiltrata;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIView *sfondo_nav;
@property (strong, nonatomic) NSString *titolo;
@property (strong, nonatomic) IBOutlet UIButton *btn_closeKeyboard;
@property (strong, nonatomic) BreviarioC *controllerSubBreviario;

@property (strong, nonatomic) IBOutlet UIView *black_mask;


- (id)initWithList:(NSArray*)a andTitle:(NSString*)t;



@end
