//
//  TabellaMaster.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "TabellaMaster.h"
#import "BreviarioC.h"
#import "Home.h"
#import "Day.h"
#import "Item.h"

@implementation TabellaMaster
@synthesize tabella,sfondo_master,titolo,sfondo_nav,day_label,lista,listaFiltrata,black_mask;
@synthesize btn_closeKeyboard ,searchBar,controllerSubBreviario;

BOOL barraMostrata = NO;

#pragma mark - INIT
- (id)initWithList:(NSArray*)a andTitle:(NSString*)t
{
    //iPhone4
    NSString *nibNameOrNil = @"TabellaMaster";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"TabellaMaster_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) nibNameOrNil = @"TabellaMaster5";
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        self.lista = [[NSMutableArray alloc] initWithArray:a];
        self.listaFiltrata = [[NSMutableArray alloc] initWithArray:a];
        self.titolo = NSLocalizedString(t, nil);
    }
    return self;
}



#pragma mark - DIDLOAD method
//=========================
// DIDLOAD method
//=========================
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [day_label setFont:[UIFont fontWithName:FONT_ROMAN_ANTIQUE size:15]];
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    [searchBar setBackgroundColor:[UIColor clearColor]];
    if(self.isIos7orUpper)searchBar.barTintColor = [UIColor clearColor];
    searchBar.backgroundImage = [UIImage new];
    // NSLog(@"searchBar.subviews:%@",searchBar.subviews);


    barraMostrata = NO;
    
    [self configureView];
    if(self.isIpad)[[NSNotificationCenter defaultCenter] addObserver:self
                                                            selector:@selector(controllaOrientationIPAD)
                                                                name:DID_ROTATE object:nil];
    
    if(self.isIpad)[self controllaOrientationIPAD];

    
}

-(void)controllaOrientationIPAD{
    [sfondo_master setImage:[UIImage imageNamed:self.isLand?@"back_menu_003L.png":@"back_menu_003P.png"]];
}

- (void)configureView
{
    //viene eseguita ad ogni aggiornamento giorno/CSS
    //NAVBAR
    [self titleOnNavBar2:titolo];
        
    [tabella reloadData];
    
    BOOL reverse = ([self.defaults integerForKey:CSS]==2) && [Home sharedInstance].HOME.hidden;
    [day_label setTextColor:reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.4]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9]];
    [day_label setText:[Home sharedInstance].day.date];
    
    if(IS_IPHONE_6 || IS_IPHONE_6_PLUS){
        [sfondo_master setImage:[UIImage imageNamed:@"back_menu_003L.png"]];
    }
}

-(IBAction)checktable:(UIButton*)sender{
    // NSLog(@"checktable:%@",sender);
#ifdef __IPHONE_8_0
    NSMutableString *msg = [NSMutableString stringWithFormat:@"table frame:%@\n",NSStringFromCGRect(tabella.frame)];
    [msg appendFormat:@"view:%@\n",self.view];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"test" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [self dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok]; // add action to uialertcontroller
    

    [self presentViewController:alert animated:YES completion:nil];
#endif

}


-(void)creaTitolo{
    //Titolo
    UILabel *capo = [[UILabel alloc] initWithFrame:CGRectMake(0, -2, 44, 44)];
	[capo setBackgroundColor:[UIColor clearColor]];
	[capo setFont: [UIFont fontWithName:FONT_EILEENCAPS size: 30]];
    [capo setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.6]];
    capo.shadowOffset = CGSizeMake(1,2);
	[capo setTextAlignment:NSTextAlignmentLeft];
	[capo setText:[self.titolo substringWithRange:NSMakeRange(0,1)]];
    
	UILabel *resto = [[UILabel alloc] initWithFrame:CGRectMake(26, 12, 320, 24)];
	resto.backgroundColor = [UIColor clearColor];
	[resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size: 20]];
	resto.textAlignment = NSTextAlignmentLeft;
	[resto setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.65]];
	
	resto.lineBreakMode = NSLineBreakByWordWrapping;
	resto.numberOfLines = 1;
    [resto setShadowColor:capo.shadowColor];
	resto.shadowOffset = CGSizeMake(0,2);
	
	resto.text = [self.titolo substringFromIndex:(NSUInteger)1];
    
    CGSize expectedLabelSize;
    
        CGRect b  = [resto.text boundingRectWithSize:resto.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:resto.font} context:nil];
        
        expectedLabelSize = b.size;
        
    UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, expectedLabelSize.width+resto.frame.origin.x, 44)];
    Label.layer.transform = CATransform3DMakeScale(0.85, 0.85, 1.0);
    [Label setClipsToBounds:NO];
    UIView* back = [[UIView alloc ] initWithFrame:CGRectMake(-15, 7, expectedLabelSize.width+resto.frame.origin.x+30, 30)];
    [back setBackgroundColor:[UIColorFromRGB(0X422c20) colorWithAlphaComponent:0.1]];
    UIButton *toTop = [UIButton buttonWithType:UIButtonTypeCustom];
    [toTop setBackgroundColor:[UIColor clearColor]];
    [toTop setFrame:Label.frame];
    [toTop addTarget:self action:@selector(tabellaAllaRicerca:) forControlEvents:UIControlEventTouchUpInside];
    [Label addSubview:toTop];
    
    [Label setCenter:CGPointMake(130,43)];
    //[Label addSubview:back];
    [Label addSubview:capo];
    [Label addSubview:resto];
    
    if([NSLocalizedString(@"checklingua",nil) isEqualToString:@"ar"])
    {
        [resto setText:self.titolo];
        [capo setText:@""];
        [Label setFrame:CGRectMake(0, 0, 200, Label.frame.size.height)];
        [Label setCenter:CGPointMake(130,43)];
        [resto setTextAlignment:NSTextAlignmentCenter];
        [resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size: 25]];
        [resto setFrame:CGRectMake(0, 0, Label.frame.size.width, Label.frame.size.height)];
        
    }

    
    [sfondo_nav addSubview:Label];
}



-(IBAction)tabellaAllaRicerca:(UIButton*)sender{
    [tabella scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}

-(IBAction)goHome:(UIButton*)sender{
    // NSLog(@"goHome!");
    [searchBar performSelectorOnMainThread:@selector(resignFirstResponder) withObject:nil waitUntilDone:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [DDMenuController closeMenuWithCompletion:
     ^{  // NSLog(@"Settare Home in Home!!");
         [Home mostraHome];
     }];
}

-(IBAction)goBack:(UIButton*)sender{
    // NSLog(@"goBack!");
    [searchBar performSelectorOnMainThread:@selector(resignFirstResponder) withObject:nil waitUntilDone:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - LifeCyrcle
//=========================
// LIFECYRCLE
//=========================
#pragma mark Appear

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    // NSLog(@"TabellaMaster: viewWillAppear");
    if(self.isIpad)[self controllaOrientationIPAD];

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //Sfondo
    BOOL reverse = ([self.defaults integerForKey:CSS]==2) && [Home isContentVisible];
    [day_label setTextColor:reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.4]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9]];
    [day_label setText:[[[Home sharedInstance] day] day_label]];
    [black_mask setHidden:!reverse];

    //nascondo searchBar
    if(!barraMostrata && lista.count>0)
    {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [tabella scrollToRowAtIndexPath:indexPath
                   atScrollPosition:UITableViewScrollPositionTop
                           animated:YES];
    barraMostrata = YES;
    }
    
    //if(tabella.frame.origin.y!=64)
    {
        [tabella setFrame:CGRectMake(0, 64, 260, self.view.frame.size.height-64)];
    }
}

#pragma mark Disappear

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    // NSLog(@"TabellaMaster: viewDidDisappear");
    [self chiudiTastiera:btn_closeKeyboard];

}

#pragma mark -
#pragma mark --------> Table view
#pragma mark Header/Footer
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// HEADER SESSION
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

// FOOTER SESSION
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

#pragma mark ROW

//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listaFiltrata.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self heighAtIndexPath:indexPath];
}

-(float)heighAtIndexPath:(NSIndexPath *)indexPath{
    CGSize labelSize =CGSizeMake(0, 0);
    if([[lista objectAtIndex:indexPath.row] isKindOfClass:[Item class]])
    {
        // NSLog(@"string:%@",[(Item*)[lista objectAtIndex:indexPath.row] titolo]);
        
        CGRect b  = [[(Item*)[lista objectAtIndex:indexPath.row] titolo] boundingRectWithSize:CGSizeMake(250, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_HOEFLER size: 20]} context:nil];
        // NSLog(@"textRect.size:%@",NSStringFromCGRect(b));
        labelSize = b.size;
        // NSLog(@"labelSize:%@",NSStringFromCGSize(labelSize));
     }
    if([[lista objectAtIndex:indexPath.row] isKindOfClass:[BreviarioObject class]])
    {
        CGRect b  = [[(BreviarioObject*)[lista objectAtIndex:indexPath.row] title] boundingRectWithSize:CGSizeMake(250, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_HOEFLER size: 20]} context:nil];
        
        labelSize = b.size;
    }
    if([[lista objectAtIndex:indexPath.row] isKindOfClass:[MissalObject class]])
    {
        CGRect b  = [[(MissalObject*)[lista objectAtIndex:indexPath.row] title] boundingRectWithSize:CGSizeMake(250, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_HOEFLER size: 20]} context:nil];
        
        labelSize = b.size;
    }
    
    labelSize.height +=30.f;
    // NSLog(@"labelSize:%@",NSStringFromCGSize(labelSize));

    if(labelSize.height<60.f){
        // NSLog(@"");
        return 60.0;
    }
	return (float)labelSize.height;
}
#pragma mark CELL
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell_tabella";
    float height = [self heighAtIndexPath:indexPath];
    BOOL reverse = ([self.defaults integerForKey:CSS]==2) && ([Home sharedInstance].HOME.hidden);
    UIColor *colore = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.55]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, 260, height)];
		cell.backgroundColor = [UIColor clearColor];
		cell.contentView.backgroundColor = [UIColor clearColor];
		[cell setBackgroundView:nil];
    }

	UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, 260, height)];
	Label.backgroundColor = [UIColor clearColor];
    
    
	UILabel *resto = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 250, height-10)];
	resto.backgroundColor = [UIColor clearColor];
	[resto setFont: [UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size: 20]];
	resto.textAlignment = NSTextAlignmentCenter;
	[resto setTextColor:colore];
	
	resto.lineBreakMode = NSLineBreakByTruncatingTail;
	resto.numberOfLines = 4;
    
    if([[lista objectAtIndex:indexPath.row] isKindOfClass:[Item class]])
    {
        // NSLog(@"IndexPath:%i",(int)indexPath.row);
        Item *itemRow = [listaFiltrata objectAtIndex:indexPath.row];
        resto.text = [itemRow.titolo stringByRemovingHTMLtag];
        if(itemRow.sottotitolo && ![itemRow.sottotitolo isEqualToString:@""])
        {
            CGRect frame = Label.frame;
            frame.origin.y = height-23;
            frame.origin.x += 4;
            frame.size.height = 30;
            frame.size.width -= 8;
            UILabel *subtitle = [[UILabel alloc] initWithFrame:frame];
            subtitle.backgroundColor = [UIColor clearColor];
            [subtitle setFont: [UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size: 12]];
            subtitle.textAlignment = NSTextAlignmentCenter;
            [subtitle setTextColor:[colore colorWithAlphaComponent:0.8]];
            [subtitle setMinimumScaleFactor:0.8];
            [subtitle setAdjustsFontSizeToFitWidth:YES];
            subtitle.numberOfLines = 1;
            [subtitle setText:itemRow.sottotitolo];
            [Label addSubview:subtitle];
            
        }
    }
    
    if([[lista objectAtIndex:indexPath.row] isKindOfClass:[BreviarioObject class]])
    {
        resto.text = [[(BreviarioObject*)[listaFiltrata objectAtIndex:indexPath.row] title] stringByRemovingHTMLtag];

    }
    
    if([[lista objectAtIndex:indexPath.row] isKindOfClass:[MissalObject class]])
    {
        MissalObject *m = (MissalObject*)[listaFiltrata objectAtIndex:indexPath.row];
        resto.text = [[m title] stringByRemovingHTMLtag];
        
    }

    [Label addSubview:resto];
    
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 50)];
    [selectedBackgroundView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.1]];
	[cell setSelectedBackgroundView:selectedBackgroundView];

    
    
    [cell.contentView addSubview:Label];
	return cell;
}

#pragma mark DELEGATE (tableView)

// COMANDA DETAILVIEW
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tabella deselectRowAtIndexPath:indexPath animated:YES];
    BOOL reverse = [self.defaults integerForKey:CSS]==2;
    
    if(indexPath.row>=listaFiltrata.count) {
        // NSLog(@"indexPath.row>=listaFiltrata.count\n%@",listaFiltrata);
        return;
    }
    
    [self chiudiTastiera:btn_closeKeyboard];
    
    //->ITEM
    if([[lista objectAtIndex:indexPath.row] isKindOfClass:[Item class]])
    {
        [DDMenuController closeMenuWithCompletion:
         ^{
             Item *i = [listaFiltrata objectAtIndex:indexPath.row];
             if(i==nil || i.contenuto == nil) return;
             if(i.sottotitolo && ![i.sottotitolo isEqualToString:@""])
             [i setContenuto:[NSString stringWithFormat:@"<div class=\"sezioneSubPrefazio\">%@</div>%@",i.sottotitolo,i.contenuto]];
             [Home mostraItem:i];
         }];
    }
    
    //->ITEM-BREVIARIO(COMUNE)
    if([[lista objectAtIndex:indexPath.row] isKindOfClass:[BreviarioObject class]])
    {
        BreviarioObject *b = (BreviarioObject*)[listaFiltrata objectAtIndex:indexPath.row];
        // NSLog(@"selezionato:%@",b);

        if(b==nil) return;
        controllerSubBreviario = nil;
        controllerSubBreviario = [[BreviarioC alloc] initWithBreviario:b];
        [self.navigationController pushViewController:controllerSubBreviario animated:YES];
    }
    
    //->ITEM-MESSALE(COMUNE)
    if([[lista objectAtIndex:indexPath.row] isKindOfClass:[MissalObject class]])
    {
        // NSLog(@"ITEM-MESSALE(COMUNE)");
        [DDMenuController closeMenuWithCompletion:
                ^{
                    MissalObject *m = (MissalObject*)[listaFiltrata objectAtIndex:indexPath.row];
                    if(m==nil) return;
                    Item *mc = [m messaleComune];
                    [mc setTitoloClass:@"sezioneComuni"];
                    [Home mostraItem:mc];
                }];
     }

    
    
    if(reverse)[tabella performSelector:@selector(reloadData) withObject:nil afterDelay:0.8];
}


#pragma mark --------> FINE - Table view


#pragma mark - Error CLass
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [self setSfondo_master:nil];
    [super viewDidUnload];
}

#pragma mark - SEARCHBAR
//=========================
// SEARCHBAR
//=========================
-(void)searchBar:(UISearchBar *)s textDidChange:(NSString *)searchText{
    // NSLog(@"textDidChangeQUI:%@",searchBar.text);
    if(lista.count<=0) return;
    
    if([searchBar.text isEqualToString:@""]) {
        // NSLog(@"cancel!");
        [self ripristinaLista];
        return;
    }
    
    [listaFiltrata removeAllObjects];
    
    if([[lista objectAtIndex:0] isKindOfClass:[Item class]]){

    for (Item *dict in lista)
    {
        if(dict.titolo)
        {
        NSRange r = [dict.titolo rangeOfString:searchText options:NSCaseInsensitiveSearch];
        if (r.location != NSNotFound) [listaFiltrata addObject:dict];
        }
    }
    
    }
    
    //->ITEM-MESSALE(COMUNE)
    if([[lista objectAtIndex:0] isKindOfClass:[MissalObject class]])
    {
        for (MissalObject *dict in lista)
        {
            if(dict.title)
            {
                NSRange r = [dict.title rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (r.location != NSNotFound) [listaFiltrata addObject:dict];
            }
        }
    
    }
    
    //->ITEM-BREVIARIO(COMUNE)
    if([[lista objectAtIndex:0] isKindOfClass:[BreviarioObject class]])
    {
        for (BreviarioObject *dict in lista)
        {
            if(dict.title)
            {
                NSRange r = [dict.title rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (r.location != NSNotFound) [listaFiltrata addObject:dict];
            }
        }
        
    }

    
    [self ricaricaTabella];
}

-(void)ripristinaLista{
    // NSLog(@"RipristinaLista");
    [listaFiltrata removeAllObjects];
    [listaFiltrata addObjectsFromArray:lista];
    [self ricaricaTabella];
}

-(void)ricaricaTabella{
    [self performSelectorOnMainThread:@selector(mainRicaricaTabella) withObject:nil waitUntilDone:NO];
}

-(void)mainRicaricaTabella{
    [tabella reloadData];
}

#pragma mark delegate search
- (IBAction)searchBarCancelButtonClicked:(UISearchBar *)s{
    // NSLog(@"searchBarCancelButtonClicked:%@",searchBar);
    searchBar.text = @"";
    [self ripristinaLista];
    [searchBar resignFirstResponder];
    [btn_closeKeyboard setHidden:YES];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    // NSLog(@"selectedScopeButtonIndexDidChange");
    [UIView transitionWithView: btn_closeKeyboard
                      duration: 0.7
                       options: UIViewAnimationOptionTransitionFlipFromTop
                    animations: ^{
                        [btn_closeKeyboard setHidden:NO];
                        }
                    completion:^(BOOL finished){
                        // NSLog(@"Fine");
                    }];


}

- (void)searchBarSearchButtonClicked:(UISearchBar *)s{
    // NSLog(@"searchBarSearchButtonClicked");
    [searchBar resignFirstResponder];
    [btn_closeKeyboard setHidden:YES];

}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)s{
    // NSLog(@"searchBarShouldBeginEditing");
    [UIView transitionWithView: btn_closeKeyboard
                      duration: 0.7
                       options: UIViewAnimationOptionTransitionFlipFromTop
                    animations: ^{
                        [btn_closeKeyboard setHidden:NO];
                    }
                    completion:^(BOOL finished){
                        // NSLog(@"Fine");
                    }];
    return YES;
}

-(IBAction)chiudiTastiera:(UIButton*)sender{
    [btn_closeKeyboard setHidden:YES];
    [searchBar performSelectorOnMainThread:@selector(resignFirstResponder) withObject:nil waitUntilDone:YES];
}


//=========================
// END CLASS
//=========================

@end
