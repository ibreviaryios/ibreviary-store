//
//  MessaMenuCtrl.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "BNHtmlPdfKit.h"

@class Home,Messa;

@interface MessaMenuCtrl : BaseController <UITableViewDataSource,UITableViewDelegate,BNHtmlPdfKitDelegate,UIDocumentInteractionControllerDelegate>

//SHARE
@property (strong, nonatomic) BNHtmlPdfKit *htmlPdfKit;
@property (strong, nonatomic) UIDocumentInteractionController *dtest;
@property (strong, nonatomic) IBOutlet UIButton *share_btn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *rotella_share;

@property (strong, nonatomic) NSMutableArray *indice;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *rotella_HOME;


@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (strong, nonatomic) IBOutlet UIImageView *sfondo_master;
@property (strong, nonatomic) IBOutlet UILabel *day_label;
@property (strong, nonatomic) IBOutlet UIButton *btn_home;
@property (strong, nonatomic) IBOutlet UIView *black_mask;
@property (strong, nonatomic) IBOutlet UIButton *edit_button;

- (id)initWithEditMode:(BOOL)edit;
-(void)forzaEditor;
-(void)aggiornaTabella;
-(void)removeRotellaHOME;
-(void)creaPDFConMessa:(Messa*)m;

@end
