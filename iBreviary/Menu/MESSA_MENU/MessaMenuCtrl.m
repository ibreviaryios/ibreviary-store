//
//  MessaMenuCtrl.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "MessaMenuCtrl.h"
#import "Home.h"
#import "SystemData.h"

#define ALTEZZA_CELLA_IPHONE 60.f
@interface MessaMenuCtrl()

@property(weak, nonatomic) IBOutlet UIButton* book_btn;
@property(weak, nonatomic) IBOutlet UIActivityIndicatorView* book_rotella;


@end

@implementation MessaMenuCtrl
@synthesize tabella, sfondo_master, day_label, btn_home, black_mask;
@synthesize indice, edit_button;
@synthesize rotella_HOME;
//SHARE
@synthesize htmlPdfKit,dtest,share_btn,rotella_share;

BOOL creata_in_land = NO;

#pragma mark - INIT method
//=========================
// INIT
//=========================
- (id)initWithEditMode:(BOOL)edit
{
    NSString *nibNameOrNil = IS_IPAD?@"MessaMenuCtrl_ipad":@"MessaMenuCtrl_iphone";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        self.editing = IS_IPAD?edit:NO;
    }
    return self;
}



//=========================
// StatusBar
//=========================
#pragma mark - StatusBar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

//cambio colore statusbar in bianco
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - DIDLOAD method
//=========================
// DIDLOAD method
//=========================
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self inizializeInterface];
}

#pragma mark Tabella Reload
-(void)aggiornaTabella{
    // NSLog(@"aggiornaTabella -");
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{ [tabella reloadData];}];
}

#pragma mark - LifeCyrcle
//=========================
// LIFECYRCLE
//=========================

#pragma mark Appear
- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"MessaLista:viewWillAppear(LAND:%i)->(%@)",self.isLand,NSStringFromCGRect(self.view.frame));
	[super viewWillAppear:animated];
    if(creata_in_land!=self.isLand){
        // NSLog(@"MessaMenu è orientato sbagliato!");
        [tabella reloadData];
    }
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
    
    if(self.isIpad){
        BOOL reverse = ([self.defaults integerForKey:CSS]==2) && [Home isContentVisible];
        [black_mask setHidden:!reverse];
    }
    
    [self.book_rotella setHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    // NSLog(@"MessaLista:viewDidAppear editing:%i",self.editing);
    [super viewDidAppear:animated];
    if(![self.defaults boolForKey:TUTORIAL_MENU_LIBRO_MESSA]){
        [self customTutorialBookMenuAlert];
    }
    
    [self initIndice];
    [tabella reloadData];
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    [alertView close];
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d. CHECK:%@", (int)buttonIndex, (int)[alertView tag],self.tutorialAlertGroup.checkedCheckBoxes);
    [self.defaults setBool:[self.tutorialAlertGroup.checkedCheckBoxes firstObject] forKey:TUTORIAL_MENU_LIBRO_MESSA];
    [self.defaults synchronize];
}

#pragma mark Disappear
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    // NSLog(@"MessaLista:viewDidDisappear");
    [super viewDidDisappear:animated];
}

#pragma mark - Action
//=========================
// Action
//=========================
-(void)inizializeInterface{
    // NSLog(@"MessaLista inizializeInteface");
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate)
                                                 name:DID_ROTATE object:nil];

    [edit_button setTitle:NSLocalizedString(@"Messa_edit", nil) forState:UIControlStateNormal];
    [edit_button setTitle:NSLocalizedString(@"Messa_editok", nil) forState:UIControlStateSelected];
    [edit_button.layer setCornerRadius:5];
    [edit_button.layer setBorderWidth:1.2];
    [edit_button.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:0.7] CGColor]];
    [edit_button.layer setMasksToBounds:YES];

    [rotella_share setHidden:YES];
    [share_btn setHidden:NO];
}

-(void)initIndice{
    indice = [NSMutableArray new];
    for (int i=0; i<14; i++) {
        NSString *text = [NSString stringWithFormat:@"indexMessa_%i",i];
        [indice addObject:NSLocalizedString(text, nil)];
    }
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 10)];
	v.backgroundColor = [UIColor clearColor];
	[tabella setTableFooterView:v];
    [tabella reloadData];
}

-(IBAction)editAction:(UIButton*)sender{
    // NSLog(@"editAction");
    self.editing = !self.editing;
    [tabella setSeparatorStyle:!self.editing?UITableViewCellSeparatorStyleNone:UITableViewCellSeparatorStyleSingleLine];
    [edit_button setSelected:self.editing];
    [edit_button setTitle:NSLocalizedString(@"Messa_edit", nil) forState:UIControlStateNormal];
    [edit_button setTitle:NSLocalizedString(@"Messa_editok", nil) forState:UIControlStateSelected];
    [edit_button setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.5] forState:UIControlStateNormal];
    [edit_button setTitleColor:[[UIColor redColor] colorWithAlphaComponent:0.7] forState:UIControlStateSelected];
    [tabella reloadData];
    // NSLog(@"EditButton:selected:%i",edit_button.selected);
}

-(void)forzaEditor{
    self.editing = YES;
    [edit_button setSelected:YES];
    edit_button.selected = YES;
    [tabella reloadData];
}

-(void)removeRotellaHOME{
    // NSLog(@"removeRotellaHOME!");
    [rotella_HOME stopAnimating];
    [rotella_HOME setHidden:YES];
    [btn_home setHidden:NO];
}


-(IBAction)goHome:(UIButton*)sender{
    // NSLog(@"goHome from Messa!");
    [DDMenuController closeMenuWithCompletion:
     ^{  // NSLog(@"Settare Home in Home!!");
         [[NSOperationQueue mainQueue] addOperationWithBlock:^{ [Home resetMessa]; }];
         
         [[NSOperationQueue mainQueue] addOperationWithBlock:^{ [self.navigationController popToRootViewControllerAnimated:YES]; }];
         [[NSOperationQueue mainQueue] addOperationWithBlock:^{ [Home mostraHome]; }];
     }];
    
}

#pragma mark -
#pragma mark --------> Table view
#pragma mark Header/Footer
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // NSLog(@"messa indice reloading:%i",self.editing);
    [edit_button setSelected:self.editing];
    [edit_button setTitle:NSLocalizedString(@"Messa_edit", nil) forState:UIControlStateNormal];
    [edit_button setTitle:NSLocalizedString(@"Messa_editok", nil) forState:UIControlStateSelected];
    [edit_button setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.5] forState:UIControlStateNormal];
    [edit_button setTitleColor:[[UIColor redColor] colorWithAlphaComponent:0.7] forState:UIControlStateSelected];
    [edit_button.titleLabel setFont:[UIFont fontWithName:self.editing?FONT_HELVETICANEUE_BOLD:FONT_HELVETICANEUE_REGULAR size:edit_button.titleLabel.font.pointSize]];

    return 1;
}

// HEADER SESSION
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 5;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

// FOOTER SESSION
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

#pragma mark ROW

//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    [tabella setScrollEnabled:!IS_IPAD];
    // NSLog(@"indice:%lu",(unsigned long)indice.count);
    return indice.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!IS_IPAD){
        return ALTEZZA_CELLA_IPHONE;
    }
    CGRect home_frame = [[[Home sharedInstance] view] frame];
    creata_in_land = self.isLand;
    // NSLog(@"Altezza impostata:%f",(home_frame.size.height-64.0)/(float)indice.count);
    return (home_frame.size.height-64.0)/(float)indice.count;
}

-(int)typeAtIndexPath:(NSIndexPath *)indexPath{
    //caso 0 niente
    //caso 1 editor icon
    //caso 2 chooser icon
    if (self.editing) {
        switch (indexPath.row) {
            case 2:
                return 2;
                break;
            case 3:
                return 1;
                break;
            case 13:
                return 1;
            case 4:
                return 2;
            case 5:
                return 2;
            case 8:
                return 2;
            case 9:
                return 2;
            case 12:
                return 2;
            default:
                return 0;
                break;
        }
        return 0;
    }
    
    return 0;
}
#pragma mark CELL
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell_indice_messa";
    BOOL reverse = ([self.defaults integerForKey:CSS]==2);
    CGFloat altezza_cella = tabella.frame.size.height/(float)indice.count;
    if(!IS_IPAD){
        altezza_cella = ALTEZZA_CELLA_IPHONE;
    }
    [tabella setSeparatorColor:reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.02]:[UIColor colorWithRed:137/255.f green:109/255.f blue:62/255.f alpha:0.09]];
    UIColor *coloreCapoLettera = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.67]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9];
    UIColor *colore = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.55]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tabella.frame.size.width, altezza_cella)];
		cell.backgroundColor = [UIColor clearColor];
		cell.contentView.backgroundColor = [UIColor clearColor];
		[cell setBackgroundView:nil];
    }
    
    UILabel *capo = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, self.isLand?35:40)];
    // NSLog(@"capo:%@",NSStringFromCGRect(capo.frame));

	[capo setBackgroundColor:[UIColor clearColor]];
	[capo setFont: [UIFont fontWithName:FONT_EILEENCAPS size:(self.isLand?35:40)]];
    [capo setTextColor:coloreCapoLettera];
    [capo setShadowColor:reverse?[UIColor clearColor]:[UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:0.2]];
    capo.shadowOffset = CGSizeMake(1,2);
	[capo setTextAlignment:NSTextAlignmentLeft];
	[capo setText:[[indice objectAtIndex:indexPath.row] substringToIndex:1]];
    
	UILabel *resto = [[UILabel alloc] initWithFrame:CGRectMake((self.isLand?30:37), (self.isLand?-3:0), 250, 55)];
	resto.backgroundColor = [UIColor clearColor];
	[resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size: (self.isLand?20:23)]];
	resto.textAlignment = NSTextAlignmentLeft;
	[resto setTextColor:colore];
	
	resto.lineBreakMode = NSLineBreakByClipping;
	resto.numberOfLines = 1;
    [resto setShadowColor:capo.shadowColor];
	resto.shadowOffset = CGSizeMake(0,2);
	
	resto.text = [[indice objectAtIndex:indexPath.row] substringFromIndex:1];
    
    if(!self.editing)
    {
    UIImageView *selectedBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 260, altezza_cella)];
    [selectedBackgroundView setBackgroundColor:[UIColor colorWithRed:230/255.f green:175/255.f blue:96/255.f alpha:0.6]];
    [cell setSelectedBackgroundView:selectedBackgroundView];
    }
    else
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    CGSize expectedLabelSize;
    CGRect b  = [resto.text boundingRectWithSize:resto.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:resto.font} context:nil];
    
    expectedLabelSize = b.size;

    
    UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, expectedLabelSize.width+resto.frame.origin.x, (self.isLand?35:40))];
	Label.backgroundColor = [UIColor clearColor];
    
    Messa *messa = [Home getCurrentMessa];
    // NSLog(@"MESSA-PREF:%@ MESSA-PE:%@",messa.sel_prefazio, messa.sel_preghieraEucaristica);
    if(indexPath.row==8 && messa.sel_prefazio.intValue==-1)
    {
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,19,18)];
        [img setContentMode:UIViewContentModeScaleAspectFit];
        [img setCenter:CGPointMake(Label.frame.size.width+5, 17)];
        [img setAlpha:0.62];
        [img setImage:[UIImage imageNamed:@"alert_icon.png"]];
        [Label addSubview:img];
    }
    
    if(indexPath.row==9 && messa.sel_preghieraEucaristica.intValue==-1)
    {
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,19,18)];
        [img setContentMode:UIViewContentModeScaleAspectFit];
        [img setCenter:CGPointMake(Label.frame.size.width+5, 17)];
        [img setAlpha:0.62];
        [img setImage:[UIImage imageNamed:@"alert_icon.png"]];
        [Label addSubview:img];
    }

    
    [Label addSubview:capo];
	[Label addSubview:resto];
    
    
	[cell.contentView addSubview:Label];
    
    if(self.editing)
    {
        int check = [self typeAtIndexPath:indexPath];
        if(check==0){
            [Label setFrame:CGRectMake(5, Label.frame.origin.y, Label.frame.size.width, Label.frame.size.height)];
            [Label setAlpha:0.4];
        }
        else{
            UIImageView *img;
            img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
            [img setContentMode:UIViewContentModeScaleAspectFit];
            [img setImage:[UIImage imageNamed:(check==1)?@"edit_icon2.png":@"scegliLogo.png"]];
            [img setAlpha:0.82];
            [img setBackgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:img];
            CGRect label_frame = CGRectMake(28, Label.frame.origin.y, Label.frame.size.width, Label.frame.size.height);
            [Label setFrame:label_frame];
            
            if((28+label_frame.size.width) > 260){
                //aggiustamento stringa se è più lunga di 260 px
                CGRect resto_frame = resto.frame;
                resto_frame.size.width = 250.f - label_frame.origin.x - resto_frame.origin.x;
                [resto setAdjustsFontSizeToFitWidth:YES];
                [resto setMinimumScaleFactor:0.8];
                [resto setFrame:resto_frame];
                
            }
            
            [img setCenter:CGPointMake(14, Label.center.y+2)];
            
            //if(check!=1)
            {
                UIView *sub_img = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
                [sub_img.layer setCornerRadius:sub_img.frame.size.width/2.f];
                [sub_img.layer setBorderColor:[[UIColorFromRGB(0x543725) colorWithAlphaComponent:0.5] CGColor]];
                [sub_img.layer setBorderWidth:1.2];
                [sub_img.layer setCornerRadius:sub_img.frame.size.width/2.f];
                [sub_img setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.1]];
                [sub_img setCenter:img.center];
                [cell.contentView insertSubview:sub_img belowSubview:img];
            }
        }
    }
    
    if(!self.editing)[Label setCenter:CGPointMake(125, altezza_cella/2.0)];
    // NSLog(@"Label:%@",NSStringFromCGRect(Label.frame));
	return cell;
    
}

#pragma mark DELEGATE (tableView)

// COMANDA DETAILVIEW
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Messa *currentMessa = [Home getCurrentMessa];
    
    if(!self.editing){
        switch (indexPath.row) {
            case 2:
                // NSLog(@"Letture goTo");
                if(currentMessa.sel_letture.intValue == -1) { [self showNessunaSelezione:indexPath]; return;}
                break;
            case 3:
                // NSLog(@"Omelia goTo");
                if([currentMessa.omelia isEqualToString:@""]) { [self showNessunaSelezione:indexPath]; return;}
                break;
            case 4:
                // NSLog(@"Credo goTo");
                if(currentMessa.sel_credo.intValue == -1) { [self showNessunaSelezione:indexPath]; return;}
                break;
            case 5:
                // NSLog(@"PreghieraFedeli goTo");
                if(currentMessa.sel_preghieraDeiFedeli.intValue == -1) { [self showNessunaSelezione:indexPath]; return;}
                break;
            case 8:
                // NSLog(@"Prefazio goTo");
                if(currentMessa.sel_prefazio.intValue == -1)  { [self showNessunaSelezione:indexPath]; return;}
                break;
            case 9:
                // NSLog(@"Preghiere Eucaristiche goTo");
                if(currentMessa.sel_preghieraEucaristica.intValue == -1)  { [self showNessunaSelezione:indexPath]; return;}
                break;
            case 12:
                // NSLog(@"BenedizSolenne goTo");
                if(currentMessa.sel_benedizioneSolenne.intValue == -1)  { [self showNessunaSelezione:indexPath]; return;}
                break;
            case 13:
                // NSLog(@"Avvisi goTo");
                if([currentMessa.avvisi isEqualToString:@""])  { [self showNessunaSelezione:indexPath]; return;}
                break;
            default:
                break;
        }

        [DDMenuController closeMenuWithCompletion:
         ^{
             [Home messaGoToIndex:indexPath];
         }];
    }//fine !editing
    
    if ([self typeAtIndexPath:indexPath]<=0) return;
    
    if(self.editing)
    {
        switch (indexPath.row) {
            case 2:
                // NSLog(@"Letture editing");
                [Home showMessaSelettoreWithType:tipo_lista_letture];
                break;
            case 3:
                // NSLog(@"Omelia editing");
                [Home showMessaEditor:@"omelia"];
                break;
            case 4:
                // NSLog(@"Credo chooser:%i",tipo_lista_credo);
                [Home showMessaSelettoreWithType:tipo_lista_credo];
                break;
            case 5:
                // NSLog(@"PreghieraFedeli chooser");
                [Home showMessaSelettoreWithType:tipo_lista_preghiera_fedeli];
                break;
            case 8:
                // NSLog(@"Prefazio chooser");
                [Home showMessaSelettoreWithType:tipo_lista_prefazio];
                break;
            case 9:
                // NSLog(@"Preghiere Eucaristiche chooser");
                [Home showMessaSelettoreWithType:tipo_lista_preghiera_eucaristica];
                break;
            case 12:
                // NSLog(@"BenedizSolenne chooser");
                [Home showMessaSelettoreWithType:tipo_lista_benedizione_solenne];
                break;
            case 13:
                // NSLog(@"Avvisi editing");
                [Home showMessaEditor:@"avvisi"];
                break;
            default:
                break;
        }//fine editing
        
        [Home messaGoToIndex:indexPath];
    }
}

-(void)showNessunaSelezione:(NSIndexPath *)indexPath
{
    CGFloat altezza_cella = tabella.frame.size.height/(float)indice.count;
    UIImageView *nessunaSelezione = [[UIImageView alloc] initWithFrame:CGRectMake(tabella.frame.size.width - 20 - altezza_cella, 10, altezza_cella-20, altezza_cella-20)];
    [nessunaSelezione setImage:[UIImage imageNamed:@"no_item.png"]];
    [nessunaSelezione setAlpha:0.9];
    
    [[tabella cellForRowAtIndexPath:indexPath].contentView addSubview:nessunaSelezione];
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ nessunaSelezione.alpha = 0;}
                     completion:^(BOOL finito){[nessunaSelezione removeFromSuperview];}];
}

-(void)deselectRow:(NSIndexPath*)indexPath{
    [tabella deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark --------> FINE - Table view

//==================================
// CONDIVIDI MESSA
//==================================
#pragma mark - CONDIVIDI MESSA -
-(IBAction)condividiMessa:(UIButton*)sender{
    // NSLog(@"self.isLand?%i frame:%@",self.isLand,NSStringFromCGRect(self.view.frame));
    [self creaPDFConMessa:[Home getCurrentMessa]];
}

-(void)removeRotellaSHARE{
    // NSLog(@"removeRotellaSHARE!");
    [rotella_share stopAnimating];
    [rotella_share setHidden:YES];
    [share_btn setHidden:NO];
}

-(void)attivaRotellaSHARE{
    // NSLog(@"attivaRotellaSHARE!");
    [rotella_share startAnimating];
    [rotella_share setHidden:NO];
    [share_btn setHidden:NO];
}

-(IBAction)showMessaBook:(UIButton*)sender{
    if(!self.book_rotella.hidden){
        return;
    }
    [self.book_rotella startAnimating];
    [self.book_rotella setHidden:NO];
    
    Messa *m = [Home getCurrentMessa];
    NSString *messa_name = m.messa_name;
    if(messa_name==nil) messa_name = m.day.title;
    NSLog(@"PdfBook presente:%@?%@",[m pdfBookNameFile],[self fileExistsAtPath:[m pdfBookNameFile]]?@"Y":@"N");

    if([self fileExistsAtPath:[m pdfBookNameFile]]){
        NSLog(@"PdfBook presente");
        [self.book_rotella stopAnimating];
        [self.book_rotella setHidden:YES];
        [Home mostraPdfBookwithUrl:[m pdfBookNameFile]];
        return;
    }
    
    // NSLog(@"self.isLand?%i frame:%@",self.isLand,NSStringFromCGRect(self.view.frame));
    self.htmlPdfKit = [[BNHtmlPdfKit alloc] initWithPageSize:IS_IPAD?BNPageSizeA4:BNPageSizeIphone];
    self.htmlPdfKit.delegate = self;
       
    [self.htmlPdfKit saveHtmlAsPdf:[m htmlPdfBookWithOmelia:YES]
                            toFile:[m pdfBookNameFile]];
}

-(void)creaPDFConMessa:(Messa*)m
{
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        [self attivaRotellaSHARE];
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        [self alert:m.titolo_custom
             andMex:NSLocalizedString(@"MessaSharePdfText", nil)
     optionOneTitle:NSLocalizedString(@"MessaSharePdfOmeliaSI", nil)
          optionOne:^(UIAlertAction *action) {
              NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
                  [self creaPDFConMessaStep1:m conOmelia:YES];
              }];//fine step3
              [[NSOperationQueue mainQueue] addOperation:step3];
          }
     optionTwoTitle:NSLocalizedString(@"MessaSharePdfOmeliaNO", nil)
          optionTwo:^(UIAlertAction *action) {
              NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
                  [self creaPDFConMessaStep1:m conOmelia:NO];
              }];//fine step3
              [[NSOperationQueue mainQueue] addOperation:step3];
          }];
    }];//fine step2
    
    
    
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    
}

-(void)creaPDFConMessaStep1:(Messa*)m conOmelia:(BOOL)visualizzaOmelia{
    // NSLog(@"creaPDFConMessa:%@",m);
    htmlPdfKit = [[BNHtmlPdfKit alloc] initWithPageSize:BNPageSizeA4];
    htmlPdfKit.delegate = self;
    NSString *messa_name = m.messa_name;
    if(messa_name==nil) messa_name = m.day.title;
    [htmlPdfKit saveHtmlAsPdf:[m htmlPdfStringWithOmelia:visualizzaOmelia]
                       toFile:[self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"iBreviary - %@.pdf",messa_name]]];
}

//==================================
// PDF DELEGATE
//==================================
#pragma mark - PDF DELEGATE -
- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didSavePdfFile:(NSString *)file{
    // NSLog(@"didSavePdfFile:%@",file);
    if(!self.book_rotella.hidden){
        NSLog(@"salvato book:%@",file);
        [Home mostraPdfBookwithUrl:file];
        [self.book_rotella stopAnimating];
        [self.book_rotella setHidden:YES];
        return;
    }
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        self.dtest = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:file]];
        
        self.dtest.delegate = self;
        [self.dtest presentOptionsMenuFromRect:share_btn.superview.frame inView:self.view.superview animated:YES];

    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        [self removeRotellaSHARE];
    }];//fine step2
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];

}
- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didFailWithError:(NSError *)error{
    // NSLog(@"didFailWithError:%@",[error description]);
    
}

-(void)documentInteractionController:controller willBeginSendingToApplication:(NSString *)application{
    // NSLog(@"documentInteractionController:%@",application);
    if([DDMenuController isMenuOpen])[DDMenuController closeMenuWithCompletion:^{}];
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================
-(void)didRotate{
    // NSLog(@"self.isLand?%i frame:%@",self.isLand,NSStringFromCGRect(self.view.frame));
    [tabella reloadData];
}


//=========================
// Error CLass
//=========================
#pragma mark - Error CLass
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [super viewDidUnload];
}

//=========================
// END CLASS
//=========================
@end
