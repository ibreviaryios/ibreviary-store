//
//  MenuViewController.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "MenuViewController.h"
#import "Home.h"
#import "Fix.h"

@interface MenuViewController()

@property (strong, nonatomic) IBOutlet UIImageView *ghirigoro;


@end

@implementation MenuViewController
@synthesize tabella,sfondo_master,day_label,ghirigoro,btn_home,black_mask;
//SUBController
@synthesize breviario,messale,controllerLista,tph_ctrl,messaMenuCtrl;
//---Messale SCROLLER
@synthesize switcherView, scroller, pager, listaMessaliMenu,messaleSelected;
//TP HOME IMG
@synthesize tp_img;

BOOL didLOAD = NO;
BOOL primoAvvio = YES;
BOOL editMode = NO;

static MenuViewController *sharedMyManager = nil;


#pragma mark - SINGLETON
//=========================
// INIT & SINGLETON
//=========================
+ (MenuViewController *)sharedInstance {
    
    if(!sharedMyManager){
        sharedMyManager = [MenuViewController new];
    }
    return sharedMyManager;
    
}

/*
+ (MenuViewController *)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
*/


#pragma mark - INIT method
//=========================
// INIT
//=========================
- (id)init
{
    if(sharedMyManager) return sharedMyManager;

    //iPhone4
    NSString *nibNameOrNil = @"MenuViewController";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"MenuViewController_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"MenuViewController5";
    if(IS_IPHONE_6) nibNameOrNil = @"MenuViewController6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"MenuViewController6Plus";

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        sharedMyManager = self;

    }
    return self;
}


//=========================
// StatusBar
//=========================
#pragma mark - StatusBar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

//cambio colore statusbar in bianco
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - DIDLOAD method
//=========================
// DIDLOAD method
//=========================
- (void)viewDidLoad
{
    // NSLog(@"Menu didLoad");
    [super viewDidLoad];
    [self.navigationController setDelegate:self];
    [self messaDelegate];
    
    if(self.isIpad)[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(controllaOrientamentoTabellaIpad)
                                                 name:DID_ROTATE object:nil];

    [day_label setFont:[UIFont fontWithName:FONT_CALSON_ANTIQUE size:15]];
    
    // NSLog(@"check_lingua:%@",NSLocalizedString(@"checklingua", nil));
    primoAvvio = NO;

    if(!self.isIos7orUpper && !self.isIpad)
    {
        CGRect frame = self.view.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.height = 460;
        [self.view setFrame:frame];
        // NSLog(@"day_label:%@",NSStringFromCGRect(day_label.frame));
        // NSLog(@"self.view:%@",NSStringFromCGRect([(UIView*)[[self.view subviews] objectAtIndex:0] frame]));
        
    }
    if(self.isIos7orUpper && self.isIpad)
    {
        CGRect f = sfondo_master.frame;
        f.origin.y -= 20;
        f.size.height += 20;
        [sfondo_master setFrame:f];
    }
        
    [self checkCSSmenu];
    [self aggiornaVista];

}

-(void)createScroller{
    if(!self.switcherView.superview){
    self.switcherView = [[UIView alloc] initWithFrame:CGRectMake(0, self.isIpad?325:66, 260, 67)];
    [self.switcherView setBackgroundColor:[UIColor clearColor]];
    self.scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 7, self.switcherView.frame.size.width, 60)];
    [self.scroller setBackgroundColor:[UIColor clearColor]];
    [self.scroller setShowsHorizontalScrollIndicator:NO];
    [self.scroller setShowsVerticalScrollIndicator:NO];
    [self.scroller setDelegate:self];
    self.pager = [[UIPageControl alloc] initWithFrame:CGRectMake(106, 40, 37, 37)];
    [self.pager setBackgroundColor:[UIColor clearColor]];
    [self.pager setCurrentPageIndicatorTintColor:UIColorFromRGB(0X8a6e54)];
    [self.pager setPageIndicatorTintColor:UIColorFromRGB(0Xb99e7c)];
    [self.pager addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    [self.switcherView addSubview:scroller];
    [self.switcherView addSubview:pager];
    [self.view addSubview:switcherView];
    }
}

+(void)setCurrentMissalPos:(NSNumber*)pos{
    [[MenuViewController sharedInstance] currentMissalChangeWithPos:pos];
}

-(void)currentMissalChangeWithPos:(NSNumber*)pos{
    CGRect puntoDiPartenza = CGRectMake(0, 0, 250, 60);
    [self.pager setCurrentPage:pos.integerValue];
    
    
    puntoDiPartenza.origin.x = self.scroller.frame.size.width * pos.floatValue;
    puntoDiPartenza.origin.y = 0;
    puntoDiPartenza.size = self.scroller.frame.size;
    
    [self.scroller scrollRectToVisible:puntoDiPartenza animated:NO];

}

+(void)setCurrentBreviaryPos:(NSNumber*)pos{
    [[MenuViewController sharedInstance] currentBreviaryChangeWithPos:pos];
}

-(void)currentBreviaryChangeWithPos:(NSNumber*)pos{
    CGRect puntoDiPartenza = CGRectMake(0, 0, 250, 60);

    Day *d = [Home getCurrentDay];
    int missalPos = [d intForMissalFromBreviaryPos:pos.intValue];
    d = nil;
    [pager setCurrentPage:missalPos];
    puntoDiPartenza.origin.x = self.scroller.frame.size.width * self.pager.currentPage;
    puntoDiPartenza.origin.y = 0;
    puntoDiPartenza.size = self.scroller.frame.size;
    
    [self.scroller scrollRectToVisible:puntoDiPartenza animated:NO];
    
}

#pragma mark - LifeCyrcle
//=========================
// LIFECYRCLE
//=========================

#pragma mark Appear
- (void)viewWillAppear:(BOOL)animated{
    // NSLog(@"Menu:viewWillAppear");
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    // NSLog(@"Menu:viewDidAppear");
    [super viewDidAppear:animated];
    [btn_home setHidden:[Home isHomeVisible]];
}

+(void)aggiornaVista{
    // NSLog(@"AggiornaVista class");
    [[MenuViewController sharedInstance] performSelectorOnMainThread:@selector(aggiornaVista) withObject:nil waitUntilDone:NO];
}

-(void)aggiornaVista{
    listaMessaliMenu = [[NSMutableArray alloc] initWithArray:[[Home getCurrentDay] listaMissals]];
    [self.switcherView setHidden:YES];
    [self buildMessaleScrollerMenu];
    [day_label setText:[[[Home sharedInstance] day] day_label]];
    // NSLog(@"aggiornaVista:rivelatoCambioGiorno:%@",listaMessaliMenu);
    [self updateTablePosition];
    [self.tabella reloadData];
}

-(void)updateTablePosition{
        //set day label frame
        CGRect d = day_label.frame;
        d.origin.y = self.view.frame.size.height - day_label.frame.size.height;
        [day_label setFrame:d];
        
        //set ghirigoro frame
        CGRect g = ghirigoro.frame;
        g.origin.y = d.origin.y - ghirigoro.frame.size.height;
        [ghirigoro setFrame:g];
        
        BOOL tphome = [[Home getCurrentDay] hasHomeTP];
        NSInteger count = tphome?6:5;

        
        //set tabella frame
        CGRect f = tabella.frame;
        f.size.height = 50*count;
        f.origin.y = g.origin.y - f.size.height;
        [tabella setFrame:f];
    
        [tp_img setHidden:!tphome];
}


+(void)checkCSS{
    [[MenuViewController sharedInstance] checkCSSmenu];
}

-(void)checkCSSmenu{
    // NSLog(@"Menu checkCSSmenu");
    
    BOOL reverse = ([self.defaults integerForKey:CSS]==2) && [Home isContentVisible];
    [day_label setTextColor:reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.4]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9]];
    [day_label setText:[[[Home sharedInstance] day] day_label]];
    [black_mask setHidden:!reverse];
    
    
    if(self.isIpad)
    {
        [self controllaOrientamentoTabellaIpad];
    }
    else
    {
        [self updateTablePosition];
    }
    
    [day_label setText:[[[Home sharedInstance] day] day_label]];
    [tabella reloadData];
}

#pragma mark Disappear
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    // NSLog(@"Menu:viewDidDisappear:%@",[Home isMessaINUSO]?@"Messa IN USO":@"Giorno in uso");
    [super viewDidDisappear:animated];
}

#pragma mark - MESSA DELEGATE
//===========================
// MESSA DELEGATE
//===========================
-(void)messaDelegate{
    Home *h = [Home sharedInstance];
    [h setDelegate_messamenu:self];
}

-(void)caricaMessaMenuEdit:(BOOL)edit{
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    messaMenuCtrl = nil;
    // NSLog(@"caricaMessaMenuEditRimosso:%@",self.navigationController.viewControllers);
    messaMenuCtrl = [[MessaMenuCtrl alloc] initWithEditMode:edit];
    // NSLog(@"caricaMessaMenuEdit:%@ - %i",messaMenuCtrl,[NSThread isMainThread]);
    [self addChildCTRL:messaMenuCtrl];
    [self.navigationController pushViewController:messaMenuCtrl animated:NO];
    // NSLog(@"Pushed done->%@",self.navigationController.viewControllers);

}

-(void)rimuoviMessaMenu{
    // NSLog(@"MenuViewController: rimuoviMessaMenu");
    [self.navigationController popToRootViewControllerAnimated:YES];
    messaMenuCtrl = nil;
}

-(void)forzaMessaInEditor{
        [messaMenuCtrl forzaEditor];
}

+(void)fineMessaCaricata{
    MenuViewController * m = [MenuViewController sharedInstance];
    [m.messaMenuCtrl removeRotellaHOME];
}


-(void)resettaScroller{
    if(self.scroller==nil) [self createScroller];
    for(UIView* subview in [self.scroller subviews]) //per cancellare tutte le superviste (reset)
        [subview removeFromSuperview];
    int count = (int)[listaMessaliMenu count];
    [self.scroller setHidden:count<=1];
    [pager setHidden:scroller.hidden];
}

-(void)verificaVisibiltaScroller{
    // NSLog(@"verificaVisibiltaScroller scroller:%@",self.scroller);
}


#pragma mark -
#pragma mark SCROLLER
//=========================
// SCROLLER MISSAL
//=========================
-(void)buildMessaleScrollerMenu
{
    [self resettaScroller];
    int count = (int)[listaMessaliMenu count];
    // NSLog(@"Menu:buildMessaleScroller[%i]",count);
    
    if(count<=1) return;
    
    UIView *viewPrepered = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.scroller.frame.size.width * count, self.scroller.frame.size.height)];
    // NSLog(@"viewPrepered:%@",NSStringFromCGSize(viewPrepered.frame.size));
    [viewPrepered.layer setMasksToBounds:YES];
    
    for (int i = 0; i < count; i++) {
       
        MissalObject *row = [listaMessaliMenu objectAtIndex:i];
        
        CGRect frame;
        frame.origin.x = self.scroller.frame.size.width*i;
        frame.origin.y = 0;
        frame.origin.x += 2;
        frame.size = CGSizeMake(self.scroller.frame.size.width-4, self.scroller.frame.size.height);
        
        
        UIView *subTitolo_tex = [[UIView alloc] initWithFrame:frame];
        CAGradientLayer *gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = subTitolo_tex.bounds;
        gradientLayer.colors = [NSArray arrayWithObjects:(id)[[[UIColor whiteColor] colorWithAlphaComponent:0.2]CGColor], (id)[[UIColorFromRGB(0Xffffff) colorWithAlphaComponent:0.5]CGColor], nil];
        [subTitolo_tex.layer insertSublayer:gradientLayer atIndex:0];
        [subTitolo_tex.layer setBorderColor:[[UIColorFromRGB(0Xd89c57) colorWithAlphaComponent:0.5]CGColor]];
        [subTitolo_tex.layer setBorderWidth:2.0];
        [subTitolo_tex.layer setMasksToBounds:YES];
        [subTitolo_tex setAlpha:0.8];
        
        [viewPrepered addSubview:subTitolo_tex];
        
        // NSLog(@"variant:%@. type:%@. title:%@.",row.variant,row.type,row.title);
        NSString *titolo_messale = row.title;
        if(row.variant && [row.variant isKindOfClass:[NSString class]] && ![row.variant isEqualToString:@""])
            titolo_messale = row.variant;
        
        
        NSString *titolo_text = row.subtitle?[NSString stringWithFormat:@"%@ - %@",titolo_messale,row.subtitle]:(row.type?[NSString stringWithFormat:@"%@ - %@",titolo_messale,row.type]:titolo_messale);
        
        // NSLog(@"titolo_text:%@",titolo_text);
        frame.origin.y = -4;
        UILabel *label_titolo = [[UILabel alloc] initWithFrame:frame];
        [label_titolo setBackgroundColor:[UIColor clearColor]];
        [label_titolo setTextColor:UIColorFromRGB(0X483627)];
        [label_titolo setNumberOfLines:2];
        [label_titolo setMinimumScaleFactor:0.6];
        [label_titolo setLineBreakMode:NSLineBreakByTruncatingTail];
        //[label_titolo.layer setBorderColor:[[UIColor redColor] CGColor]];
        //[label_titolo.layer setBorderWidth:1];
        [label_titolo setTextAlignment:NSTextAlignmentCenter];
        [label_titolo setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:14]];
        [label_titolo setText:[titolo_text uppercaseString]];
        [label_titolo setClipsToBounds:YES];
        [label_titolo setAdjustsFontSizeToFitWidth:YES];
        
        [viewPrepered addSubview:label_titolo];
    }
    if(count>0)
    [self performSelectorOnMainThread:@selector(mainBuildInterface:) withObject:viewPrepered waitUntilDone:YES];
    
}

-(void)mainBuildInterface:(UIView*)new{
    // NSLog(@"Menu:mainBuildInterface");
    [self.scroller addSubview:new];
    
    self.scroller.pagingEnabled = YES;
    self.scroller.contentSize = new.frame.size;
    // NSLog(@"self.scroller.contentSize:%@",NSStringFromCGSize(self.scroller.contentSize));
    [self.scroller setContentOffset:CGPointZero animated:NO];
    
    
    int count = (int)[listaMessaliMenu count];

    [pager setNumberOfPages:count];
    
    [self verificaVisibiltaScroller];
    
    [self.pager setCurrentPage:0];
    
    [day_label setText:[(MissalObject*)[listaMessaliMenu objectAtIndex:self.pager.currentPage] title]];
    [self.scroller setScrollEnabled:YES];
    [self.switcherView setHidden:listaMessaliMenu.count<=1];

    // NSLog(@"switcher:%i sub:%@",self.switcherView.hidden,[self.scroller subviews]);
}

#pragma mark - Missal SCROLLView delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView == scroller)
    {
        CGFloat pageWidth = self.scroller.frame.size.width;
        int currentPage = floor((self.scroller.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.pager.currentPage = currentPage;
        
        CGRect frame;
        frame.origin.x = self.scroller.frame.size.width * self.pager.currentPage;
        frame.origin.y = 0;
        frame.size = self.scroller.frame.size;
        [self.scroller scrollRectToVisible:frame animated:YES];
        
        // NSLog(@"Messale Scroller[%i]: scrollViewDidEndDecelerating:%@",(int)self.pager.currentPage,NSStringFromCGRect(frame));
    }
}

-(IBAction)changePage:(UIPageControl *)aPageControl
{
    // NSLog(@"Root:changePage:%li",(long)self.pager.currentPage);
    CGRect frame;
    frame.origin.x = self.scroller.frame.size.width * self.pager.currentPage;
    frame.origin.y = 0;
    frame.size = self.scroller.frame.size;
    [self.scroller scrollRectToVisible:frame animated:YES];
}



#pragma mark - Action
//=========================
// Action
//=========================
-(IBAction)goHome:(UIButton*)sender{
    // NSLog(@"goHome!");

    [DDMenuController closeMenuWithCompletion:
     ^{  // NSLog(@"Settare Home in Home!!");
         [Home mostraHome];
         [btn_home setHidden:YES];
     }];
    
}


#pragma mark -
#pragma mark --------> Table view
#pragma mark Header/Footer
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// HEADER SESSION
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

// FOOTER SESSION
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

#pragma mark ROW
//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(![self.defaults boolForKey:AGGIORNAMENTO_2015]) return 0;
    BOOL tphome = [[Home getCurrentDay] hasHomeTP];
    if(self.isIpad)
    {
        //>>IPAD
        [self controllaOrientamentoTabellaIpad];
    }
    return tphome?6:5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50.0;
}

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)

#pragma mark CELL
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // NSLog(@"Menu:cellForRowAtIndexPath->%@",indexPath);
    static NSString *CellIdentifier = @"Cell";
    Fix *fix = [Fix getFixForLang:[[[Home getCurrentDay] linguaGiorno] sigla]];

    // NSLog(@"cellForRowAtIndexPath:%@",indexPath);
    BOOL reverse = ([self.defaults integerForKey:CSS]==2 && [Home isContentVisible]);
    
    UIColor *coloreCapoLettera = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.48]:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.9];
    UIColor *colore = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.45]:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.9];

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		UIView* backgroundView = [[UIView alloc ] initWithFrame:CGRectZero ];
		backgroundView.backgroundColor = [UIColor clearColor];
		cell.backgroundView.alpha = 0;
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.alpha = 0;
		cell.contentView.backgroundColor = [UIColor clearColor];
		[cell setBackgroundView: backgroundView];
        cell.textLabel.backgroundColor =[UIColor clearColor];
        cell.textLabel.text = @"";
    }
	//Colore Label RGB a=alpha;
	UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, 270, 50)];
	Label.backgroundColor = [UIColor clearColor];
    
    UILabel *capo = [[UILabel alloc] initWithFrame:CGRectMake(17, 0, 50, 50)];
	[capo setBackgroundColor:[UIColor clearColor]];
	[capo setFont: [UIFont fontWithName:FONT_EILEENCAPS size: 40]];
    [capo setTextColor:coloreCapoLettera];
    [capo setShadowColor:reverse?[UIColor clearColor]:[UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:0.2]];
    capo.shadowOffset = CGSizeMake(1,2);
	[capo setTextAlignment:NSTextAlignmentLeft];
    
    UILabel *resto = [[UILabel alloc] initWithFrame:CGRectMake(51, 14, 219, 35)];
    [resto setBackgroundColor:[UIColor clearColor]];
	[resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size: 30]];
	[resto setTextAlignment:NSTextAlignmentLeft];
	[resto setTextColor:colore];
	
	resto.lineBreakMode = NSLineBreakByClipping;
	resto.numberOfLines = 1;
    [resto setShadowColor:capo.shadowColor];
	resto.shadowOffset = CGSizeMake(0,2);
    
    NSString *testo;
    if (indexPath.row<5)
    {
        testo = NSLocalizedString([[self.defaults objectForKey:ROOT_TS_INDEX] objectAtIndex:indexPath.row], nil);
    }
    else
    {
        TestoProprio *tp_home = [TestoProprio getTestoProprioWithKey:[[Home getCurrentDay] type_home]];
        testo = tp_home.titolo;
       //tp_img.transform = CGAffineTransformMakeRotation(RADIANS(-20));
        [tp_img setImage:tp_home.logo];
    }
    
    if([NSLocalizedString(@"checklingua",nil) isEqualToString:@"ar"])
    {
        CGFloat marginLeft = 5;
        [resto setText:testo];
        [resto setBackgroundColor:[UIColor clearColor]];
        [resto setTextAlignment:NSTextAlignmentRight];
        [resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size: 35]];
        [resto setFrame:CGRectMake(marginLeft, Label.frame.origin.y, 255-marginLeft, Label.frame.size.height)];
    }

    //Qualunque Altra lingua
    else{
    [capo setText:[testo substringWithRange:NSMakeRange(0,1)]];
	
	[resto setText:[testo substringFromIndex:(NSUInteger)1]];
    }
	
	UIImageView *selectedBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 260, 50)];
	[selectedBackgroundView setImage:[UIImage imageNamed:@"croceSelect.png"]];
    [selectedBackgroundView setContentMode:UIViewContentModeCenter];
    [selectedBackgroundView setAlpha:0.1];
	[cell setSelectedBackgroundView:selectedBackgroundView];
    
    Day *currentDay = [Home getCurrentDay];
    // NSLog(@"currentDay:%@ missal listaDay:%@ lista messali:%@",currentDay, [currentDay listaMissals],listaMessaliMenu);
    BOOL test = NO;
    if(indexPath.row == 0 && [currentDay breviarioIsEmpty]) test = YES;
    
    if(indexPath.row == 1 && listaMessaliMenu.count==0)  test = YES;
    
    if(indexPath.row == 2 && listaMessaliMenu.count==0)  test = YES;
    
    if(indexPath.row == 2 && listaMessaliMenu.count > 1 && [[Home getCurrentDay] listaLettureAggregate:[listaMessaliMenu objectAtIndex:self.pager.currentPage]] == nil)  test = YES;
    
    if(indexPath.row == 3 && fix.prayers.count==0)  test = YES;
    
    if(indexPath.row == 4 && fix.riti.count==0)  test = YES;
    
    if(test){
        UIView *selectView = [[UIView alloc] initWithFrame:Label.frame];
        //[selectedBackgroundView setBackgroundColor:[[UIColor blueColor] colorWithAlphaComponent:0.1]];
        UIImageView *selectedBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 220, 50)];
        [selectedBackgroundView setImage:[UIImage imageNamed:@"x@2x.png"]];
        //[selectedBackgroundView setBackgroundColor:[UIColor greenColor]];
        [selectedBackgroundView setContentMode:UIViewContentModeScaleAspectFit];
        [selectedBackgroundView setAlpha:0.6];
        [selectView addSubview:selectedBackgroundView];
        [cell setSelectedBackgroundView:selectView];
    }

    currentDay = nil;
    
	[Label addSubview:capo];
	[Label addSubview:resto];
	[cell.contentView addSubview:Label];
	
	return cell;
}

#pragma mark DELEGATE (tableView) DIDSELECT
// COMANDA DETAILVIEW
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Fix *fix = [Fix getFixForLang:[[[Home getCurrentDay] linguaGiorno] sigla]];
    NSArray * listaMessali = [[Home getCurrentDay] listaMissals];
    switch (indexPath.row) {
        case 0:
        {
            // NSLog(@"Breviario");
            Day *currentDay = [Home getCurrentDay];
            if([currentDay breviarioIsEmpty]) return;
            currentDay = nil;
            breviario = nil;
            breviario = [[Breviario alloc] initWithMissalPos:(listaMessali.count>1)?(int)self.pager.currentPage:-1];
            // NSLog(@"self.nav:%@ brevario:%@",self.navigationController,breviario);
            [self.navigationController pushViewController:breviario animated:YES];
            break;
        }
        case 1:
        {
            // NSLog(@"Messale");
            if(listaMessali.count==0) return;
            messale = nil;
            messale = [[Messale alloc] initWithMissalPos:(listaMessali.count>1)?(int)self.pager.currentPage:-1];
            [self.navigationController pushViewController:messale animated:YES];
            break;
        }
        case 2:
        {
             // NSLog(@"Letture");
            if(listaMessali.count==0) return;
            Item *lettureAggreGate = [[Home getCurrentDay] listaLettureAggregate:[listaMessali objectAtIndex:self.pager.currentPage>=0?self.pager.currentPage:0]];
            if(lettureAggreGate == nil) return;
            [Home mostraItem:lettureAggreGate];
            [DDMenuController closeMenuWithCompletion:^{}];
            break;
        }
        case 3:
            // NSLog(@"Preghiere:%@",fix.prayers);
            if(fix.prayers.count == 0) return;
            controllerLista = nil;
            controllerLista = [[TabellaMaster alloc] initWithList:fix.prayers andTitle:NSLocalizedString([[self.defaults objectForKey:ROOT_TS_INDEX] objectAtIndex:indexPath.row], nil)];
            [self.navigationController pushViewController:controllerLista animated:YES];
            break;
        case 4:
            // NSLog(@"Riti");
            if(fix.riti.count == 0) return;
            controllerLista = nil;
            controllerLista = [[TabellaMaster alloc] initWithList:fix.riti andTitle:NSLocalizedString([[self.defaults objectForKey:ROOT_TS_INDEX] objectAtIndex:indexPath.row], nil)];
            [self.navigationController pushViewController:controllerLista animated:YES];
            break;
        
        case 5:
            {
            // NSLog(@"TPHome");
            tph_ctrl = nil;
            TestoProprio *tp_home = [TestoProprio getTestoProprioWithKey:[[Home getCurrentDay] type_home]];
            tph_ctrl = [[TPHome alloc] initWithTitoloTP:tp_home.titolo];
            [self.navigationController pushViewController:tph_ctrl animated:YES];
            break;
            }
        default:
            break;
    }
    
    fix = nil;
    
}


#pragma mark --------> FINE - Table view

#pragma mark - NAV DELEGATE
//=========================
// NAV DELEGATE
//=========================
- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    // NSLog(@"NAV:willShowViewController:%@",NSStringFromClass([viewController class]));
}

- (void)navigationController:(UINavigationController *)navigationController
       didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    // NSLog(@"NAV:didShowViewController:%@",NSStringFromClass([viewController class]));
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================
-(void)controllaOrientamentoTabellaIpad{
    BOOL tphome = [[Home getCurrentDay] hasHomeTP];
    int i = tphome?6:5;
    CGRect frame;
    CGFloat margin_bottom = 65;
    frame = CGRectMake(-5, self.view.frame.size.height-(50*i)-margin_bottom, 270, 50*i);
    [tabella setFrame:frame];
}

//=========================
// Error CLass
//=========================
#pragma mark - Error CLass
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [super viewDidUnload];
}

//=========================
// END CLASS
//=========================
@end
