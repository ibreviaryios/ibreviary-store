//
//  NotificaCTRL.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "NotificaCTRL.h"
#import "Home.h"
#import "WebBrowser.h"


@implementation NotificaCTRL

@synthesize text_view,text_label,area_text,titolo_notifica;
@synthesize notifica;
@synthesize close, btn_DX, btn_SX, btn_CENTER;
@synthesize  logo_paypal,  logo;
@synthesize delegate;
@synthesize webContent;



#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
-(id)initWithDelegate:(BaseController<NotificaCTRLDelegate>*)d  andNotifica:(NotificaObject*)n
{

    //iPhone4
    NSString *nibNameOrNil = @"NotificaCTRL";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"NotificaCTRL_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"NotificaCTRL5";
    if(IS_IPHONE_6) nibNameOrNil = @"NotificaCTRL6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"NotificaCTRL6Plus";

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        self.delegate = d;
        self.notifica = n;
        if(self.isIpad) [self.view setCenter:d.view.center];

    }
    return self;
}

#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
-(IBAction)FineNotificaCTRL:(UIButton*)sender{
    // NSLog(@"FineNotificaCTRL");
    [delegate removeNotificaCTRL];
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
-(void)viewDidLoad {
    [super viewDidLoad];
    if(self.isIpad)
    {
        [self.view.layer setCornerRadius:10];
        [self.view.layer setMasksToBounds:YES];
    }
    // NSLog(@"NotificaCTRL DidLoad");
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
    [self gui];
    [self caricaTesto];

    [self aggiornaNotifica];
    
}

-(void)aggiornaNotifica{
    //Cancellare l'eventuale notifica nel Notification Center
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    // NSLog(@"aggiornaNotifica:%@",eventArray);
    for (int i=0; i<[eventArray count]; i++)
    {
//        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
//        // NSLog(@"oneEvent:%@",oneEvent);
//        NSDictionary *userInfoCurrent = oneEvent.userInfo;
//        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"id"]];
//        // NSLog(@"%i == %i",uid.intValue , notifica.id_notifica.intValue);
//        if (uid.intValue == notifica.id_notifica.intValue)
//        {
//            //Cancelling local notification
//            [app cancelLocalNotification:oneEvent];
//        }
    }
    //imposto notifica come letta

    
    //setta settings notifiche e save on Private
    [notifica impostaNotificaLetta];
}



-(void)viewWillAppear:(BOOL)animated{
    // NSLog(@"NotificaCTRL:viewWillAppear");
    [super viewWillAppear:animated];
    if(!self.isIos7orUpper){
        CGRect fr = self.view.frame;
        fr.origin.y =-20;
        fr.size.height -=20;
        [self.view setFrame:fr];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    // NSLog(@"NotificaCTRL:viewDidAppear");
    [super viewDidAppear:animated];
}


-(void)gui{
    //AREA TEXT
    [area_text.layer setCornerRadius:10];
    [area_text.layer setMasksToBounds:YES];
        
    [btn_DX.layer setCornerRadius:10];
    [btn_DX.layer setBorderColor:[[UIColorFromRGB(0Xe9e2d2) colorWithAlphaComponent:0.9] CGColor]];
    [btn_DX.layer setBorderWidth:1.6];
    [btn_DX.layer setMasksToBounds:YES];
    //[btn_DX setTitleColor:[UIColorFromRGB(0X7a6357) colorWithAlphaComponent:0.35] forState:UIControlStateNormal];
    
    [btn_CENTER.layer setCornerRadius:10];
    [btn_CENTER.layer setBorderColor:[[UIColorFromRGB(0X7b592d) colorWithAlphaComponent:0.8] CGColor]];
    [btn_CENTER.layer setBorderWidth:2.5];
    [btn_CENTER.layer setMasksToBounds:YES];
    [btn_CENTER setTitleColor:[UIColorFromRGB(0X7a6357) colorWithAlphaComponent:0.25] forState:UIControlStateNormal];
    
    
    
    if([self isIos7orUpper])
        text_view.tintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9];
}

-(void)caricaTesto{
    
    [titolo_notifica setText:notifica.titolo];
    [text_label setText:notifica.contenuto];
    [text_view setText:notifica.contenuto];
    
        
    CGSize stringsize;
    CGSize linkStringSize;
    CGSize constraindeSize = CGSizeMake(text_view.frame.size.width, 5000);
    CGSize constraindeSizeLink = CGSizeMake(self.view.frame.size.width,btn_DX.frame.size.height);
    CGRect b  = [notifica.contenuto boundingRectWithSize:constraindeSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:text_label.font} context:nil];
        
    stringsize = b.size;
    CGRect b_title  = [notifica.link_titolo boundingRectWithSize:constraindeSizeLink options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:btn_DX.titleLabel.font} context:nil];
        
    linkStringSize = b_title.size;
    if(linkStringSize.width+40<=self.view.frame.size.width){
        linkStringSize.width += 40.0;
    }
    BOOL tantoTesto = (stringsize.height>(text_label.frame.size.height));
    // NSLog(@"String SIZE:%@ > %f",NSStringFromCGSize(stringsize),(float)text_label.frame.size.height);

    [text_label setHidden:tantoTesto];
    [text_view setHidden:!tantoTesto];
    
    //or whatever font you're using
    CGPoint center = btn_DX.center;
    [btn_DX setFrame:CGRectMake(0,0,linkStringSize.width, btn_DX.frame.size.height)];
    [btn_DX setTitle:notifica.link_titolo forState:UIControlStateNormal];
    [btn_DX setCenter:center];

    //NOTIFICA DONAZIONI
    [logo_paypal setHidden:(notifica.type.intValue!=0)];
    
    
    if(notifica.type.intValue==1) {
             //NOTIFICA NEWSLETTER
    }
    
    //NOTIFICA CON LINK
    [btn_DX setHidden:!(notifica.link && ![notifica.link isEqualToString:@""])];
    
}

#pragma mark - ACTION
//=========================
// ACTION
//=========================
-(IBAction)premutoLink:(UIButton*)sender{
    NSString *link_string = notifica.link;
    if ([link_string rangeOfString:@"http://"].location == NSNotFound)
        link_string = [NSString stringWithFormat:@"http://%@",notifica.link];
    //[NotificaObject controlloLocalNotifichePushWithID:[NSNumber numberWithInt:notifica.id_notifica]];
    WebBrowser *browser = [WebBrowser new];
    [browser setUrl:[NSURL URLWithString:link_string]];
    [delegate presentViewController:browser animated:YES completion:^{[delegate removeNotificaCTRL];}];
    
}


-(IBAction)premutoPiuTardi:(UIButton*)sender{
    // NSLog(@"premutoPiuTardi");
}


#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}



@end
