//
//  NotificaCTRL.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "NotificaObject.h"

@protocol NotificaCTRLDelegate <NSObject>
- (void)removeNotificaCTRL;
@end


@interface NotificaCTRL : BaseController<UIWebViewDelegate>

    
@property (nonatomic, strong) BaseController<NotificaCTRLDelegate> *delegate;
@property (nonatomic, strong) IBOutlet UIView *area_text;
@property (nonatomic, strong) IBOutlet UILabel *titolo_notifica;
@property (nonatomic, strong) IBOutlet UITextView *text_view;
@property (nonatomic, strong) IBOutlet UILabel *text_label;
@property (nonatomic, strong) NotificaObject *notifica;
@property (nonatomic, strong) IBOutlet UIButton *close;
@property (nonatomic, strong) IBOutlet UIWebView *webContent;
@property (nonatomic, strong) IBOutlet UIButton *btn_DX;
@property (nonatomic, strong) IBOutlet UIButton *btn_SX;
@property (nonatomic, strong) IBOutlet UIButton *btn_CENTER;
@property (nonatomic, strong) IBOutlet UIImageView *logo_paypal;
@property (nonatomic, strong) IBOutlet UIImageView *logo;


-(id)initWithDelegate:(BaseController<NotificaCTRLDelegate>*)d  andNotifica:(NotificaObject*)n;

@end
