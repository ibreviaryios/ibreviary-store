//
//  Messa.m
//  iBreviary
//
//  Created by leyo on 27/08/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import "Messa.h"
#import "Fix.h"
#import "DataObject.h"

@implementation Messa

@dynamic messa_name;
@dynamic isComune;
@dynamic date_string;
@dynamic lingua_string;
@dynamic titolo_custom;
@dynamic day;
@dynamic riti_introduzione;
@dynamic antifonaEcolletta;
@dynamic letture;
@dynamic omelia;
@dynamic credo;
@dynamic preghieraDeiFedeli;
@dynamic liturgia_eucaristica_da_ordinario;
@dynamic sel_missal;
@dynamic prefazio;
@dynamic preghieraEucaristica;
@dynamic riti_comuni_da_ordinario;
@dynamic riti_conclusione_da_ordinario;
@dynamic antifonaDopo;
@dynamic benedizioneSolenne;
@dynamic avvisi;
@dynamic extra;
@dynamic sel_credo;
@dynamic sel_letture;
@dynamic sel_preghieraDeiFedeli;
@dynamic sel_prefazio;
@dynamic sel_preghieraEucaristica;
@dynamic sel_benedizioneSolenne;
@dynamic sulleofferte;

//=========================
// SET METHOD & INT
//=========================
#pragma mark - INIT
+ (void)initialize {
	if (self == [Messa class]) {
		DataObject *transformer = [[DataObject alloc] init];
		[NSValueTransformer setValueTransformer:transformer forName:@"DataObject"];
	}
}

-(NSUserDefaults*)defaults{
    NSUserDefaults *de = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    return de;
}

//=========================
// EREDITATE DAL DAY
//=========================
#pragma mark - FROM DAY obj
-(NSString *)lingua_string{
    return self.day.lingua;
}

-(NSString *)date_string{
    return self.day.id_date;
}

+(Messa*)addMessaWithDay:(Day*)d forContext:(NSManagedObjectContext*)context{
    // NSLog(@"addMessaWithDay %@",d);
    
    //Creiamo un'istanza di NSManagedObject per l'Entità che ci interessa
    Messa *new_messa = [NSEntityDescription
                         insertNewObjectForEntityForName:@"Messa"
                         inManagedObjectContext:context];
    
    //Usando il Key-Value Coding inseriamo i dati presi dall'interfaccia nell'istanza dell'Entità appena creata
    [new_messa setDay:d];
    [new_messa createFromDayIn:context];    
    return new_messa;
}

- (void)createFromDayIn:(NSManagedObjectContext*)context{
    Fix *fix = [Fix getFixForLang:self.day.lingua];
    if(self.day.missal.count<=0)
    {
        // NSLog(@"no_missal_found!!!");
        return;
    }
    self.sel_missal = [NSNumber numberWithInt:0];
;
    MissalObject *missal = [self.day.missal objectAtIndex:[self.sel_missal integerValue]];
    
    
    //messa
    self.riti_introduzione = fix.introductory_rites;
    self.antifonaEcolletta = [missal antiphon_and_opening_prayer];
    self.letture = [NSMutableArray new];
    if([self.day listaLettureAggregate:missal]!=nil){
        [self.letture addObject:[self.day listaLettureAggregate:missal]];
        self.sel_letture = [NSNumber numberWithInt:0];
    }
    self.credo = [fix getCredoArray];
    self.preghieraDeiFedeli = [[NSMutableArray alloc] initWithArray:fix.preghieredeifedeli_array];
    Fix *deviceFix = [Fix getFixForLang:NSLocalizedString(@"checklingua", nil)];
    self.riti_comuni_da_ordinario = fix.celebration_communion_rites;
    Item *riti_conclusion = [Item new];
    [riti_conclusion setTitolo:deviceFix.celebration_conclusion_rites.titolo];
    [riti_conclusion setContenuto:fix.celebration_conclusion_rites.contenuto];
    self.riti_conclusione_da_ordinario = riti_conclusion;
    // NSLog(@"MESSA:\nletture:%@\nsel_letture:%@",self.letture,self.sel_letture);
    self.sulleofferte = [missal prayer_over_the_gifts];
    self.prefazio = [[NSMutableArray alloc] initWithArray:fix.prefazio_array];
    self.preghieraEucaristica = [[NSMutableArray alloc] initWithArray:fix.liturgiaeucaristica_array];
    self.liturgia_eucaristica_da_ordinario = fix.eucharistic_liturgy;
    self.antifonaDopo = [missal antiphon_and_prayer_after_communion];
    self.benedizioneSolenne = [[NSMutableArray alloc] initWithArray:fix.benedizionisolenni_array];
    //init editable attribute
    self.omelia = @"";
    self.avvisi = @"";
    
    //init choosed attribute
    self.sel_credo = [NSNumber numberWithInt:-1];
    self.sel_preghieraDeiFedeli = [NSNumber numberWithInt:-1];
    self.sel_prefazio = [NSNumber numberWithInt:-1];
    self.sel_preghieraEucaristica = [NSNumber numberWithInt:-1];
    self.sel_benedizioneSolenne = [NSNumber numberWithInt:-1];
    [AppDelegate saveContext];
    
}


-(void)cambiaMessale:(NSInteger)numero{
    NSMutableArray *missals = [self.day listaMissals];
    if(numero>=missals.count) // NSLog(@"Errore: cambioMessale %i non esiste",(int)numero);
    self.sel_missal = [NSNumber numberWithInteger:numero];
    MissalObject *missal = [missals objectAtIndex:numero];
    // NSLog(@"Missal LIST:%@",missals);
    // NSLog(@"NewMissal:%@",missal);
    // NSLog(@"OLD ANTIFONA:%@",self.antifonaEcolletta);
    self.antifonaEcolletta = [missal antiphon_and_opening_prayer];
    // NSLog(@"NEW ANTIFONA:%@",self.antifonaEcolletta);
    self.sulleofferte = [missal prayer_over_the_gifts];
    self.antifonaDopo = [missal antiphon_and_opening_prayer];
    self.letture = [NSMutableArray new];
    if([self.day listaLettureAggregate:missal]!=nil){
        [self.letture addObject:[self.day listaLettureAggregate:missal]];
        self.sel_letture = [NSNumber numberWithInt:0];
    }
    [AppDelegate saveContext];
}

- (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}

-(NSString*)pdf_book_url{
    return [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"messa%@%@.pdf",self.date_string,[self.messa_name stringByReplacingOccurrencesOfString:@" " withString:@""]]];
}


//=========================
// CLASS METHOD
//=========================
#pragma mark - GET METHOD
+ (NSArray*)getMesseIn:(NSManagedObjectContext*)context {
    //istanzio la classe NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //istanzio l'Entità da passare alla Fetch Request
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Messa" inManagedObjectContext:context];
    
    //Setto la proprietà Entity della Fetch Request ****IMPORTANTE!!
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey: @"date_string" ascending: YES];


    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey: @"messa_name" ascending: YES];
    
    //Eseguo la Fetch Request e salviamo il risultato in un array, per visualizzarlo nella tabella
    NSError *error;
    NSMutableArray *risposta = [NSMutableArray arrayWithArray:[context executeFetchRequest:fetchRequest error:&error]];
    //NSLog(@"getMesse:%@",risposta);

    [risposta sortUsingDescriptors:@[sortDescriptor1,sortDescriptor2]];
    NSLog(@"getMessa:%@",risposta);
    return risposta;
}

- (void)awakeFromInsert{
    NSLog(@"CoreData_Messa:awakeFromInsert");
}

- (void)awakeFromFetch {
    NSLog(@"CoreData_Messa:awakeFromFetch");
}



+(Messa*)getMessaWithDateString:(NSString*)date_search in:(NSManagedObjectContext*)context{
    // NSLog(@"getMessaWithDateString:%@",date_search);
    NSArray *array = nil;
    NSError *error = nil;
    NSFetchRequest *request = [NSFetchRequest new];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Messa" inManagedObjectContext:context];
    
    [request setEntity:entity];
    NSPredicate *p = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"date_string == %@",date_search]];
    [request setPredicate:p];
    [request setIncludesPendingChanges:NO];
    array = [NSArray arrayWithArray:[context executeFetchRequest:request error:&error]];
    if(error || array==nil){
        // NSLog(@"Messa Errore request[%@] %@", request, [error description]);
    }
    
    
    if(array!=nil && array.count>0 && [[array objectAtIndex:0] isKindOfClass:[Messa class]]){
        return [array objectAtIndex:0];
    }
    
    else // NSLog(@"Messa Date with string:%@ non trovato",p);
    
    return nil;
}

- (NSString*)htmlString{
//  NSLog(@"htmlString:%@",self);
//  #warning debugging PDF string
//  return [self htmlPdfString];
    NSString *html;
    NSString *path = [[NSBundle mainBundle] bundlePath];
    
    NSString *header=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/header_%li.html",path,(long)[self.defaults integerForKey:CSS]] encoding:NSUTF8StringEncoding error:nil];
    
    NSString *footer=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/footer.html",path] encoding:NSUTF8StringEncoding error: nil];
    
    
    NSString *zoom=[NSString stringWithFormat:@"<div ID=\"ciccio\" class=\"zoomClass\" >%@</div>",[self.defaults objectForKey:ZOOM]];
    
    NSString *messaCorpo = [NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/corpoMessa.html",path] encoding:NSUTF8StringEncoding error:nil];
    
    NSString *credo_string = (self.sel_credo.intValue!=-1)?[[self.credo objectAtIndex:self.sel_credo.intValue] contenuto]:@"";
    
    NSString *preghieraFedeli = (self.sel_preghieraDeiFedeli.intValue!=-1)?[[self.preghieraDeiFedeli objectAtIndex:self.sel_preghieraDeiFedeli.intValue] contenuto]:@"";
    
    NSString *prefazio_string = (self.sel_prefazio.intValue!=-1)?[[self.prefazio objectAtIndex:self.sel_prefazio.intValue] contenuto]:@"";
    
    NSString *preghieraEucaristica_string = (self.sel_preghieraEucaristica.intValue!=-1)?[[self.preghieraEucaristica objectAtIndex:self.sel_preghieraEucaristica.intValue] contenuto]:@"";
    
    NSString *benedizioneSolenne = (self.sel_benedizioneSolenne.intValue!=-1)?[[self.benedizioneSolenne objectAtIndex:self.sel_benedizioneSolenne.intValue] contenuto]:@"";
    
    NSString *messaTextCarico = [NSString stringWithFormat:messaCorpo,
                                 [NSLocalizedString(@"indexMessa_0", nil) gtm_stringByEscapingForHTML],self.riti_introduzione.contenuto,
                                 [NSLocalizedString(@"indexMessa_1", nil) gtm_stringByEscapingForHTML],self.antifonaEcolletta.contenuto,
                                 [NSLocalizedString(@"indexMessa_2", nil) gtm_stringByEscapingForHTML],(self.sel_letture.integerValue < self.letture.count)?[[self.letture objectAtIndex:self.sel_letture.integerValue] contenuto]:@"",
                                 [NSLocalizedString(@"indexMessa_3", nil) gtm_stringByEscapingForHTML],self.omelia,
                                 (self.sel_credo.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_4", nil) gtm_stringByEscapingForHTML]:@"",credo_string,
                                 (self.sel_preghieraDeiFedeli.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_5", nil) gtm_stringByEscapingForHTML ]:@"",preghieraFedeli,
                                 [NSLocalizedString(@"indexMessa_6", nil) gtm_stringByEscapingForHTML],self.liturgia_eucaristica_da_ordinario.contenuto,
                                 [NSLocalizedString(@"indexMessa_7", nil) gtm_stringByEscapingForHTML],self.sulleofferte.contenuto,
                                 (self.sel_prefazio.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_8", nil) gtm_stringByEscapingForHTML]:@"",prefazio_string,
                                 (self.sel_preghieraEucaristica.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_9", nil) gtm_stringByEscapingForHTML]:@"",preghieraEucaristica_string,
                                 [NSLocalizedString(@"indexMessa_10", nil) gtm_stringByEscapingForHTML],self.riti_comuni_da_ordinario.contenuto,
                                 [NSLocalizedString(@"titoloMessa_11", nil) gtm_stringByEscapingForHTML],self.antifonaDopo.contenuto,
                                 self.riti_conclusione_da_ordinario.titolo,self.riti_conclusione_da_ordinario.contenuto,
                                 (self.sel_benedizioneSolenne.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_12", nil) gtm_stringByEscapingForHTML]:@"",benedizioneSolenne,
                                 ([self.avvisi isEqualToString:@""])?@"":[NSLocalizedString(@"indexMessa_13", nil) gtm_stringByEscapingForHTML],self.avvisi];
    
    html = [NSString stringWithFormat:@"%@%@%@%@",header,zoom,messaTextCarico,footer];
    // NSLog(@"HTML-LEtture[%@]: %@",self.sel_letture,[self.letture objectAtIndex:self.sel_letture.integerValue]);
    // NSLog(@"HTML: %@",html);
    return html;
}

- (NSString*)htmlPdfStringWithOmelia:(BOOL)visualizzare_omelia{
    // NSLog(@"htmlPdfString:%@",self);
    NSString *html;
    NSString *path = [[NSBundle mainBundle] bundlePath];
    
    NSString *header=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/header_pdf.html",path] encoding:NSUTF8StringEncoding error:nil];
    
    NSString *footer=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/footer.html",path] encoding:NSUTF8StringEncoding error: nil];
    
    
    NSString *messaCorpo = [NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/corpoMessaShare.html",path] encoding:NSUTF8StringEncoding error:nil];
    
    NSString *credo_string = (self.sel_credo.intValue!=-1)?[[self.credo objectAtIndex:self.sel_credo.intValue] contenuto]:@"";
    
    NSString *preghieraFedeli = (self.sel_preghieraDeiFedeli.intValue!=-1)?[[self.preghieraDeiFedeli objectAtIndex:self.sel_preghieraDeiFedeli.intValue] contenuto]:@"";
    
    NSString *prefazio_string = (self.sel_prefazio.intValue!=-1)?[[self.prefazio objectAtIndex:self.sel_prefazio.intValue] contenuto]:@"";
    
    NSString *preghieraEucaristica_string = (self.sel_preghieraEucaristica.intValue!=-1)?[[self.preghieraEucaristica objectAtIndex:self.sel_preghieraEucaristica.intValue] contenuto]:@"";
    
    NSString *benedizioneSolenne = (self.sel_benedizioneSolenne.intValue!=-1)?[[self.benedizioneSolenne objectAtIndex:self.sel_benedizioneSolenne.intValue] contenuto]:@"";
    
    
    NSString *titolo_omelia = visualizzare_omelia?[NSLocalizedString(@"indexMessa_3", nil) gtm_stringByEscapingForHTML]:@"";
    NSString *contenutoOmelia = visualizzare_omelia?self.omelia:@"";
    
    NSString *messaTextCarico = [NSString stringWithFormat:messaCorpo,
                                 [NSLocalizedString(@"indexMessa_0", nil) gtm_stringByEscapingForHTML],self.riti_introduzione.contenuto,
                                 [NSLocalizedString(@"indexMessa_1", nil) gtm_stringByEscapingForHTML],self.antifonaEcolletta.contenuto,
                                 [NSLocalizedString(@"indexMessa_2", nil) gtm_stringByEscapingForHTML],(self.sel_letture.intValue < self.letture.count)?[[self.letture objectAtIndex:self.sel_letture.integerValue] contenuto]:@"",
                                 titolo_omelia,contenutoOmelia,
                                 (self.sel_credo.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_4", nil) gtm_stringByEscapingForHTML]:@"",credo_string,
                                 (self.sel_preghieraDeiFedeli.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_5", nil) gtm_stringByEscapingForHTML ]:@"",preghieraFedeli,
                                 [NSLocalizedString(@"indexMessa_6", nil) gtm_stringByEscapingForHTML],self.liturgia_eucaristica_da_ordinario.contenuto,
                                 [NSLocalizedString(@"indexMessa_7", nil) gtm_stringByEscapingForHTML],self.sulleofferte.contenuto,
                                 (self.sel_prefazio.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_8", nil) gtm_stringByEscapingForHTML]:@"",prefazio_string,
                                 (self.sel_preghieraEucaristica.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_9", nil) gtm_stringByEscapingForHTML]:@"",preghieraEucaristica_string,
                                 [NSLocalizedString(@"indexMessa_10", nil) gtm_stringByEscapingForHTML],self.riti_comuni_da_ordinario.contenuto,
                                 [NSLocalizedString(@"titoloMessa_11", nil) gtm_stringByEscapingForHTML],self.antifonaDopo.contenuto,
                                 self.riti_conclusione_da_ordinario.titolo,self.riti_conclusione_da_ordinario.contenuto,
                                 (self.sel_benedizioneSolenne.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_12", nil) gtm_stringByEscapingForHTML]:@"",benedizioneSolenne,
                                 ([self.avvisi isEqualToString:@""])?@"":[NSLocalizedString(@"indexMessa_13", nil) gtm_stringByEscapingForHTML],self.avvisi];
    
    NSString *primaPagina = [NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/share_titolo.html",path] encoding:NSUTF8StringEncoding error:nil];
    ;
    
    NSMutableArray *missals = [self.day listaMissals];
    NSString *titoloUfficiale = self.day.title;
    if(missals.count>1 && self.sel_missal.integerValue<missals.count) titoloUfficiale = [(MissalObject*)[missals objectAtIndex:self.sel_missal.integerValue] title];
    NSString *sottotitolo = self.messa_name;
    if([sottotitolo isEqualToString:titoloUfficiale]) sottotitolo = @"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    NSString *share_messa_header = [NSString stringWithFormat:primaPagina,titoloUfficiale,sottotitolo,self.day.day_label];
    
    

    html = [NSString stringWithFormat:@"%@%@%@%@",header,share_messa_header,messaTextCarico,footer];
    // NSLog(@"HTML-LEtture[%@]: %@",self.sel_letture,[self.letture objectAtIndex:self.sel_letture.integerValue]);
    NSLog(@"HTML-PDF: %@",html);
    return html;
}

-(NSString*)pdfBookNameFile{
    NSString*fontType= [[NSUserDefaults standardUserDefaults] objectForKey:PDF_FONT_TYPE]?[[NSUserDefaults standardUserDefaults] objectForKey:PDF_FONT_TYPE]:@"1";
    return [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"iBreviary_%@_%@.pdf",[self.messa_name stringByReplacingOccurrencesOfString:@" " withString:@""],fontType]];
}

- (NSString*)htmlPdfBookWithOmelia:(BOOL)visualizzare_omelia{
    // NSLog(@"htmlPdfString:%@",self);
    NSString *html;
    NSString *path = [[NSBundle mainBundle] bundlePath];
    
    
    
    NSString *header=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/header_pdf_book_messa.html",path] encoding:NSUTF8StringEncoding error:nil];
    NSString*fontType= [[NSUserDefaults standardUserDefaults] objectForKey:PDF_FONT_TYPE]?[[NSUserDefaults standardUserDefaults] objectForKey:PDF_FONT_TYPE]:@"1";

    header = [NSString stringWithFormat:header,fontType];
    
    NSString *footer=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/footer_pdf_book.html",path] encoding:NSUTF8StringEncoding error: nil];
    
    
    NSString *messaCorpo = [NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/corpoMessaShare.html",path] encoding:NSUTF8StringEncoding error:nil];
    
    NSString *credo_string = (self.sel_credo.intValue!=-1)?[[self.credo objectAtIndex:self.sel_credo.intValue] contenuto]:@"";
    
    NSString *preghieraFedeli = (self.sel_preghieraDeiFedeli.intValue!=-1)?[[self.preghieraDeiFedeli objectAtIndex:self.sel_preghieraDeiFedeli.intValue] contenuto]:@"";
    
    NSString *prefazio_string = (self.sel_prefazio.intValue!=-1)?[[self.prefazio objectAtIndex:self.sel_prefazio.intValue] contenuto]:@"";
    
    NSString *preghieraEucaristica_string = (self.sel_preghieraEucaristica.intValue!=-1)?[[self.preghieraEucaristica objectAtIndex:self.sel_preghieraEucaristica.intValue] contenuto]:@"";
    
    NSString *benedizioneSolenne = (self.sel_benedizioneSolenne.intValue!=-1)?[[self.benedizioneSolenne objectAtIndex:self.sel_benedizioneSolenne.intValue] contenuto]:@"";
    
    
    NSString *titolo_omelia = visualizzare_omelia?[NSLocalizedString(@"indexMessa_3", nil) gtm_stringByEscapingForHTML]:@"";
    NSString *contenutoOmelia = visualizzare_omelia?self.omelia:@"";
    
    NSString *messaTextCarico = [NSString stringWithFormat:messaCorpo,
                                 [NSLocalizedString(@"indexMessa_0", nil) gtm_stringByEscapingForHTML],self.riti_introduzione.contenuto,
                                 [NSLocalizedString(@"indexMessa_1", nil) gtm_stringByEscapingForHTML],self.antifonaEcolletta.contenuto,
                                 [NSLocalizedString(@"indexMessa_2", nil) gtm_stringByEscapingForHTML],(self.sel_letture.intValue < self.letture.count)?[[self.letture objectAtIndex:self.sel_letture.integerValue] contenuto]:@"",
                                 titolo_omelia,contenutoOmelia,
                                 (self.sel_credo.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_4", nil) gtm_stringByEscapingForHTML]:@"",credo_string,
                                 (self.sel_preghieraDeiFedeli.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_5", nil) gtm_stringByEscapingForHTML ]:@"",preghieraFedeli,
                                 [NSLocalizedString(@"indexMessa_6", nil) gtm_stringByEscapingForHTML],self.liturgia_eucaristica_da_ordinario.contenuto,
                                 [NSLocalizedString(@"indexMessa_7", nil) gtm_stringByEscapingForHTML],self.sulleofferte.contenuto,
                                 (self.sel_prefazio.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_8", nil) gtm_stringByEscapingForHTML]:@"",prefazio_string,
                                 (self.sel_preghieraEucaristica.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_9", nil) gtm_stringByEscapingForHTML]:@"",preghieraEucaristica_string,
                                 [NSLocalizedString(@"indexMessa_10", nil) gtm_stringByEscapingForHTML],self.riti_comuni_da_ordinario.contenuto,
                                 [NSLocalizedString(@"titoloMessa_11", nil) gtm_stringByEscapingForHTML],self.antifonaDopo.contenuto,
                                 self.riti_conclusione_da_ordinario.titolo,self.riti_conclusione_da_ordinario.contenuto,
                                 (self.sel_benedizioneSolenne.intValue!=-1)?
                                 [NSLocalizedString(@"indexMessa_12", nil) gtm_stringByEscapingForHTML]:@"",benedizioneSolenne,
                                 ([self.avvisi isEqualToString:@""])?@"":[NSLocalizedString(@"indexMessa_13", nil) gtm_stringByEscapingForHTML],self.avvisi];
    
    NSString *primaPagina = [NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/share_titolo.html",path] encoding:NSUTF8StringEncoding error:nil];
    ;
    
    NSMutableArray *missals = [self.day listaMissals];
    NSString *titoloUfficiale = self.day.title;
    if(missals.count>1 && self.sel_missal.integerValue<missals.count) titoloUfficiale = [(MissalObject*)[missals objectAtIndex:self.sel_missal.integerValue] title];
    NSString *sottotitolo = self.messa_name;
    if([sottotitolo isEqualToString:titoloUfficiale]) sottotitolo = @"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    NSString *share_messa_header = [NSString stringWithFormat:primaPagina,titoloUfficiale,sottotitolo,[self.day.day_label gtm_stringByEscapingForHTML]];
    
    
    
    html = [NSString stringWithFormat:@"%@%@%@%@",header,share_messa_header,messaTextCarico,footer];
    // NSLog(@"HTML-LEtture[%@]: %@",self.sel_letture,[self.letture objectAtIndex:self.sel_letture.integerValue]);
    //NSLog(@"HTML-PDF-BOOK: %@",html);
    return html;
}

#pragma mark - DELETE METHOD
+(BOOL)deleteMessa:(Messa*)messaDaCancellare andError:(NSError**)e forContext:(NSManagedObjectContext*)context{
    if([[NSFileManager defaultManager] fileExistsAtPath:[messaDaCancellare pdf_book_url]]){
        [[NSFileManager defaultManager] removeItemAtPath:[messaDaCancellare pdf_book_url]
                                                          error:nil];
    }
        [context deleteObject:messaDaCancellare];
        *e = nil;
        [AppDelegate saveContext];
        [NSThread sleepForTimeInterval:0.5];
        return YES;
}


+(void)deleteAll{
    NSError *e = nil;
    for(Messa *m in [Messa getMesseIn:[AppDelegate mainManagedObjectContext]]){
        [Messa deleteMessa:m andError:&e forContext:[AppDelegate mainManagedObjectContext]];
        e = nil;
    }
    [NSThread sleepForTimeInterval:0.1];
    [AppDelegate saveContext];
    if(e){
        // NSLog(@"deleteAll Errore:%@",e);
        }
}



//==================
//// NSLog %@
//==================
#pragma mark - // NSLog
-(NSString*)description{
    return [NSString stringWithFormat:@"<messa:%@ name:%@ l:%@ date:%@>",self.day.id_date,self.messa_name,self.lingua_string,self.date_string];
    return [NSString stringWithFormat:@"<messa:%@ credo:%@ fedeli:%@ prefazio:%@ eucari:%@ ben:%@ missal:%@ letture:%@>",self.day.id_date,self.sel_credo,self.sel_preghieraDeiFedeli,self.sel_prefazio,self.sel_preghieraEucaristica,self.sel_benedizioneSolenne,self.sel_missal,self.sel_letture];
}


//=================
//END
//=================
@end
