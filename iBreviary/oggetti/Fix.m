//
//  Fix.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import "Fix.h"
#import "BreviarioObject.h"
#import "MissalObject.h"
#import "SystemData.h"
#import "GlobalFunction.h"
#include <sys/types.h>
#include <sys/sysctl.h>

@implementation Fix

//FIX_STRUCTURE
@synthesize last_update, lingua,linguaFix,ancore_array,readings_title;

//ORDER OF THE MASS
@synthesize ordinario_title;
@synthesize introductory_rites, eucharistic_liturgy, communion_rites;

//PRO MESSA
@synthesize celebration_communion_rites, celebration_conclusion_rites;

//PREFAZIO
@synthesize prefazio_title, prefazio_array;

//LITURGIA EUCARISTICA
@synthesize liturgiaeucaristica_title, liturgiaeucaristica_array;

//PREGHIERA DEI FEDELI
@synthesize preghieredeifedeli_title, preghieredeifedeli_array;

//BENEDIZIONI SOLENNI
@synthesize benedizionisolenni_title, benedizionisolenni_array;

//PRAYERS
@synthesize prayers;

//RITI
@synthesize riti;

//TESTI COMUNI BREVIARIO
@synthesize last_update_common_breviary, breviary_comuni_array;

//TESTI COMUNI MISSAL
@synthesize last_update_common_missal, missal_comuni_array;

#define URL_FIX @"http://www.ibreviary.com/service/services.php?s=get_fixed_text"


//=================
// INIT
//=================
#pragma mark - INIT
- (id)init {
    // NSLog(@"Fix INIT");
    if ((self = [super init])){
    }
    return self;
}

-(id)initWithDictionary:(NSDictionary*)json_fix{
    if ((self = [super init])){
        
        NSMutableDictionary *missal = [NSMutableDictionary dictionaryWithDictionary:[json_fix objectForKey:@"missal"]];

        
        //=============
        //FIX_STRUCTURE
        //=============
        self.last_update = [json_fix objectForKey:@"last_update"];
        self.lingua = [json_fix objectForKey:@"lingua"];
        self.linguaFix = [LinguaObject getLinguaBySigla:[json_fix objectForKey:@"lingua"]
                          ];

        self.ancore_array = [[json_fix objectForKey:@"languages"] objectForKey:@"missal_ancor_text"];
        if(ancore_array.count>0) [self.ancore_array insertObject:@"riti conclusivi" atIndex:ancore_array.count-1];
        self.readings_title = [[json_fix objectForKey:@"languages"] objectForKey:@"readings_title"];
        
        
        //=================
        //ORDER OF THE MASS
        //=================
        NSMutableDictionary *order = [NSMutableDictionary dictionaryWithDictionary:[missal objectForKey:@"order_of_mass"]];
        self.ordinario_title = [order objectForKey:@"title"];
        
        self.introductory_rites = [[Item alloc] initWithDictionary:[order objectForKey:@"introductory_rites"]];
        
        self.eucharistic_liturgy = [[Item alloc] initWithDictionary:[order objectForKey:@"eucharistic_liturgy"]];
        
        self.communion_rites = [[Item alloc] initWithDictionary:[order objectForKey:@"communion_rites"]];
        // NSLog(@"order:%@",order);
        self.celebration_communion_rites = [[Item alloc] initWithDictionary:[order objectForKey:@"communion_rites_celebration"]];
        // NSLog(@"celebration_communion_rites:%@",self.celebration_communion_rites);
        self.celebration_conclusion_rites = [[Item alloc] initWithDictionary:[order objectForKey:@"concluding_rites"]];
        // NSLog(@"celebration_conclusion_rites:%@\norder_of_mass:%@",self.celebration_conclusion_rites,[order objectForKey:@"concluding_rites"]);


        //========
        //PREFAZIO
        //========
        self.prefazio_title = [[missal objectForKey:@"preface"] objectForKey:@"title"];
        self.prefazio_array = [NSMutableArray new];
        
        for(NSMutableDictionary *row in [[missal objectForKey:@"preface"] objectForKey:@"list"])
        {
            Item *itemFromRow = [[Item alloc] initWithDictionary:row];
            // NSLog(@"row=%@",itemFromRow);
            if(itemFromRow)
            [self.prefazio_array addObject:itemFromRow];
        }
            

        
        
        // NSLog(@"PrefazioOK:%@",prefazio_array);
        
        //====================
        //LITURGIA EUCARISTICA
        //====================
        self.liturgiaeucaristica_title = [[missal objectForKey:@"eucharistic_liturgy"] objectForKey:@"title"];
        self.liturgiaeucaristica_array = [NSMutableArray new];
        
        for(NSMutableDictionary *row in [[missal objectForKey:@"eucharistic_liturgy"] objectForKey:@"list"])
            [self.liturgiaeucaristica_array addObject:[[Item alloc] initWithDictionary:row]];
        // NSLog(@"Liturgia OK:%@",liturgiaeucaristica_array);

  
        //====================
        //PREGHIERA DEI FEDELI
        //====================
        self.preghieredeifedeli_title = [[missal objectForKey:@"prayers_of_the_faithful"] objectForKey:@"title"];
        self.preghieredeifedeli_array = [NSMutableArray new];
        
        for(NSMutableDictionary *row in [NSArray arrayWithArray:[[missal objectForKey:@"prayers_of_the_faithful"] objectForKey:@"list"]])
            [self.preghieredeifedeli_array addObject:[[Item alloc] initWithDictionary:row]];
        
        //===================
        //BENEDIZIONI SOLENNI
        //===================
        self.benedizionisolenni_title = [[missal objectForKey:@"blessing"] objectForKey:@"title"];
        self.benedizionisolenni_array = [NSMutableArray new];
        
        for(NSMutableDictionary *row in [NSArray arrayWithArray:[[missal objectForKey:@"blessing"] objectForKey:@"list"]])
            [self.benedizionisolenni_array addObject:[[Item alloc] initWithDictionary:row]];

        // NSLog(@"benedizionisolenni_titleOK:%@",benedizionisolenni_title);

        
        //==============
        //PRAYERS
        //==============
        self.prayers = [NSMutableArray new];
        int i = 1;
        // NSLog(@"PRAYER COUNT:%i",(int)[[json_fix objectForKey:@"prayers"] count]);
        for(NSMutableDictionary *row in [json_fix objectForKey:@"prayers"]){
            // NSLog(@"%i",i);
            [self.prayers addObject:[[Item alloc] initWithDictionary:row]];
            i++;
        }

        // NSLog(@"prayers:%i",(int)[self.prayers count]);
        
        //==============
        //RITI
        //==============
        self.riti = [NSMutableArray new];
        for(NSMutableDictionary *row in [json_fix objectForKey:@"rites"])
            [self.riti addObject:[[Item alloc] initWithDictionary:row]];


        //======================
        //TESTI COMUNI BREVIARIO
        //======================
        self.breviary_comuni_array = [NSMutableArray new];
        for(NSMutableDictionary *row in [json_fix objectForKey:@"breviary_special"])
            [self.breviary_comuni_array addObject:[[BreviarioObject alloc] initWithDictionary:row]];

        
        //===================
        //TESTI COMUNI MISSAL
        //===================
        self.missal_comuni_array = [NSMutableArray new];
        for(NSMutableDictionary *row in [json_fix objectForKey:@"missal_special"])
            [self.missal_comuni_array addObject:[[MissalObject alloc] initWithDictionary:row]];

        // NSLog(@"FIX_NEW_FINE:%@",self);
    }
    return self;
}

//=========================
// ISTANCE METHOD
//=========================
#pragma mark - ISTANCE METHOD
- (BOOL)save{
    NSString *fixName = [self.lingua stringByAppendingString:@".fix"];
    NSString *fixPath = [self.documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_TESTIFIX,fixName]];
    NSFileManager *f = [NSFileManager defaultManager];
    if(![f fileExistsAtPath:[self.documentDir stringByAppendingPathComponent:PATH_TESTIFIX]])
    {
        NSError *error = nil;
        [f createDirectoryAtPath:[self.documentDir stringByAppendingPathComponent:PATH_TESTIFIX] withIntermediateDirectories:NO attributes:nil error:&error];
        // NSLog(@"CreateFixFolder:%@",(error!=nil)?[error description]:@"ok");
    }
    // NSLog(@"FIXPath..saving...:%@",fixName);
    NSData *dataDaSalvare = [NSKeyedArchiver archivedDataWithRootObject:self];
    BOOL success = [dataDaSalvare writeToFile:fixPath atomically:TRUE];
    if(success){
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:fixPath]];
    }
    return success;
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    NSError *error = nil;
    BOOL success = [URL setResourceValue:[NSNumber numberWithBool:YES]
                                  forKey:NSURLIsExcludedFromBackupKey
                                   error: &error];
    if(!success){
        NSLog(@"Fix: addSkipBackupAttributeToItemAtURL: Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}


- (BOOL)saveWithError:(NSError**)e{
    NSString *fixName = [self.lingua stringByAppendingString:@".fix"];
    NSFileManager *f = [NSFileManager defaultManager];
    if(![f fileExistsAtPath:[self.documentDir stringByAppendingPathComponent:PATH_TESTIFIX]])
    {
        [f createDirectoryAtPath:[self.documentDir stringByAppendingPathComponent:PATH_TESTIFIX] withIntermediateDirectories:NO attributes:nil error:e];
        // NSLog(@"CreateFixFolder:%@",(e!=nil)?[*e description]:@"ok");
    }
    NSString *fixPath = [self.documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_TESTIFIX,fixName]];
    // NSLog(@"FIXPath..saving...:%@",fixPath);
    NSData *dataDaSalvare = [NSKeyedArchiver archivedDataWithRootObject:self];
    BOOL success = [dataDaSalvare writeToFile:fixPath options:NSDataWritingAtomic error:e];
    if(success){
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:fixPath]];
    }
    return success;
}



-(Item*)ordinarioDellaMessa{
    NSString *titolo = [NSString stringWithFormat:@"%@",NSLocalizedString(@"Mass' Order", nil)];
    NSString *contenuto = @"";
    
    //ANCORE
    //INTRODUCTION
    if(self.introductory_rites && self.introductory_rites.contenuto && ![self.introductory_rites.contenuto isEqualToString:@""] && self.ancore_array.count>0)
    {
        
        contenuto = [contenuto stringByAppendingFormat:@"<a name=\"%@\"></a><div class=\"sezione\">%@</div>%@", [self.ancore_array objectAtIndex:0], [self.ancore_array objectAtIndex:0], self.introductory_rites.contenuto];
        // NSLog(@"contenuto:%@",contenuto);
    }
    
    //CREDO
    if (self.prayers.count>0 && self.ancore_array.count>1) {
        contenuto = [contenuto stringByAppendingFormat:@"<a name=\"%@\"></a><br /><div class=\"sezione\">%@</div>", [self.ancore_array objectAtIndex:1], [self.ancore_array objectAtIndex:1]];
    }
    for (Item *credo in self.prayers) {
        if(credo.use && [credo.use isKindOfClass:[NSString class] ] && [credo.use isEqualToString:@"Credo"])
            contenuto = [contenuto stringByAppendingString:credo.contenuto];
    }
    
    //eucharistic_liturgy
    if(self.eucharistic_liturgy && self.eucharistic_liturgy.contenuto && ![self.eucharistic_liturgy.contenuto isEqualToString:@""]&& self.ancore_array.count>2)
    {
        contenuto = [contenuto stringByAppendingFormat:@"<a name=\"%@\"></a><br /><div class=\"sezione\">%@</div>%@", [self.ancore_array objectAtIndex:2],self.eucharistic_liturgy.titolo,self.eucharistic_liturgy.contenuto];
    }
    
    //communion_rites
    /*
    if(self.communion_rites && self.communion_rites.contenuto && ![self.communion_rites.contenuto isEqualToString:@""])
    {
        contenuto = [contenuto stringByAppendingFormat:@"<a name=\"%@\"></a><br /><div class=\"sezione\">%@</div>", [self.ancore_array objectAtIndex:3], self.communion_rites.titolo];
        contenuto = [contenuto stringByAppendingString:self.communion_rites.contenuto];;
    }
    */
    
    if(self.celebration_communion_rites && self.celebration_communion_rites.contenuto && ![self.celebration_communion_rites.contenuto isEqualToString:@""] && self.ancore_array.count>3)
    {
        contenuto = [contenuto stringByAppendingFormat:@"<a name=\"%@\"></a><br /><div class=\"sezione\">%@</div>", [self.ancore_array objectAtIndex:3], self.celebration_communion_rites.titolo];
        contenuto = [contenuto stringByAppendingString:self.celebration_communion_rites.contenuto];;
    }
    
    if(self.celebration_conclusion_rites && self.celebration_conclusion_rites.contenuto && ![self.celebration_conclusion_rites.contenuto isEqualToString:@""]&& self.ancore_array.count>4)
    {
        contenuto = [contenuto stringByAppendingFormat:@"<a name=\"%@\"></a><br /><div class=\"sezione\">%@</div>",[self.ancore_array objectAtIndex:4], self.celebration_conclusion_rites.titolo];
        contenuto = [contenuto stringByAppendingString:self.celebration_conclusion_rites.contenuto];;
    }
    
    
    //BLESSING
    if (self.ancore_array.count>5 && self.benedizionisolenni_array.count>0) {
        contenuto = [contenuto stringByAppendingFormat:@"<a name=\"%@\"></a><br /><div class=\"sezione\">%@</div>", [self.ancore_array objectAtIndex:5], self.benedizionisolenni_title];
    
        for (Item *blessing in self.benedizionisolenni_array) {
        contenuto = [contenuto stringByAppendingString:blessing.contenuto];
        }
    }
    
    
    
    //Creo item
    Item *i = [Item new];
    [i setTitoloClass:@"sezioneOrdinario"];
    [i setTitolo:titolo];
    [i setContenuto:contenuto];
    // NSLog(@"sezioneOrdinario:%@",i.contenuto);
    return i;
}

-(NSMutableArray*)getCredoArray{
    NSMutableArray *credoArray = [NSMutableArray new];
    for (Item *credo in self.prayers) {
        if(credo.use && [credo.use isKindOfClass:[NSString class] ] && [credo.use isEqualToString:@"Credo"])
            [credoArray addObject:credo];
    }
    
    return credoArray;
}

//=========================
// CLASS METHOD
//=========================
#pragma mark - CLASS METHOD
//GET FIX WITH LANG (ritorna nil se non trovato)
+(Fix*)getFixForLang:(NSString *)lang{
    // NSLog(@"Fix getFixForLang:%@",lang);
    Fix *fix = nil;
    NSString *fixPath = [[Fix documentDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.fix",PATH_TESTIFIX, lang]];
    if([Fix fileExistsAtPath:fixPath])
            {
                NSData *d = [[NSData alloc] initWithContentsOfFile:fixPath];
                fix = [NSKeyedUnarchiver unarchiveObjectWithData:d];
                // NSLog(@"fix_breviary:%@",fix.breviary_comuni_array);
                return fix;
            }
    
    return nil;
}

+(BOOL)checkComuniForLang:(NSString *)lang{
    Fix *fix = nil;
    NSString *fixPath = [[Fix documentDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.fix",PATH_TESTIFIX, lang]];
    if([Fix fileExistsAtPath:fixPath]){
        NSData *d = [[NSData alloc] initWithContentsOfFile:fixPath];
        fix = [NSKeyedUnarchiver unarchiveObjectWithData:d];
        BOOL risposta = fix.missal_comuni_array.count>0;
        fix = nil;
        return risposta;
    }
    return NO;
}

+(NSString*)checkLASTUPDATEWithLingua:(NSString*)lingua{
    // NSLog(@"FIX: checkLASTUPDATE:%@",lingua);
    if(![GlobalFunction checkNetworkStatus])
    {
        // NSLog(@"FIX:checkLASTUPDATEWithLingua Nessuna CONNESSIONE");
        return @"";
    }
    
    //DOWNLOAD
    NSURL *url = [NSURL URLWithString:[URL_LASTUPDATE stringByAppendingFormat:@"&r=%@&v=%@&l=%@",[SystemData platformString],[SystemData versionApp],lingua]];
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_REQUEST];
    NSError *err = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err];
    if(err!=nil)
    {
        // NSLog(@"FIX:checkLASTUPDATEWithLingua[ERR]: %@",[err description]);
        return @"";

    }
    
    NSMutableDictionary *risposta = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err]];
        // NSLog(@"checkLASTUPDATE-FIX:%@",[[risposta objectForKey:@"fixed_text"] objectForKey:LAST_UPDATE]);
    if(err)
        {
            // NSLog(@"FIX:checkLASTUPDATEWithLingua[Json]:%@",[err description]);
            return @"";
        }
        
    return [[risposta objectForKey:@"fixed_text"] objectForKey:LAST_UPDATE];
    
}

+(void)forzaAggiornamentoTotale{
    NSArray *lingue = [LinguaObject getLingue];
    // NSLog(@"Lingue:%@",lingue);
    
    for (int i=0; i < lingue.count;i++ ) {
        // NSLog(@"iterFixTest:%i",i);
        LinguaObject *row = [lingue objectAtIndex:i];
        // NSLog(@"row:%@",row.sigla);
        [Fix scaricaFixWithLang:[row sigla] andLastUpdate:@"0"];
        
    }

}


//DOWNLOAD FIX
+(Fix*)scaricaFixWithLang:(NSString *)lang andLastUpdate:(NSString*)lu{
    // NSLog(@"Fix: scaricaFixWithLang:%@ - lu:%@",lang,lu);

    if([lu isEqualToString:@""]){NSLog(@"Nessuna connessione per scaricare Fix Update"); return nil;}
    if(!lang){NSLog(@"LANG PROBLEM:%@",lang); return nil;}
    //DOWNLOAD
    NSString *urlString = [NSString stringWithFormat:@"%@&r=%@&l=%@",URL_FIX,[SystemData platformString], lang];
    // NSLog(@"Fix: linkRequest:%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_REQUEST];
    NSError *err = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err];
    if(err!=nil)
    {
        //detail o download dovranno controllare la presenza del file!
        // NSLog(@"Fix: ERR scaricando %@: %@",lang, [err description]);
        return nil;
    }
    
    //FINE DOWNLOAD notifiche switchcase settate prima della chiamata!
    // NSLog(@"Fix: linkRequest:%@",url);
    err = nil;
    NSMutableDictionary *risposta_fix = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err]];
    if(err) { // NSLog(@"Errore parsing Json Day"); return  nil;
    }
    
    
    //[risposta_fix setObject:lu forKey:@"last_update"];
    [risposta_fix setObject:lang forKey:@"lingua"];
    
    Fix *fix = [[Fix alloc] initWithDictionary:risposta_fix];
    // NSLog(@"Fix after init:%@",fix);
    
    //salvo FIX
    err = nil;
    [fix saveWithError:&err];
    if(err) {
        // NSLog(@"Errore saving Fix:%@",[err description]); return  nil;
    }
    return fix;
}

//CACHE DIRECTORY
+ (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}

//DOCUMENT DIRECTORY
+ (NSString *)documentDir {
    NSArray* documentPathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* docPath = [documentPathArray lastObject];
    // NSLog(@"DocDir:%@",docPath);
    return docPath;
}


+(BOOL)fileExistsAtPath:(NSString*)path{
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

+(NSString*)versionApp{
    return (NSString *)[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
}


//=======================================
//Encoding (FOR SAVING OBJECT)
//=======================================
#pragma mark - ENCODING
-(void)encodeWithCoder:(NSCoder*)decoder{
    
    //FIX_STRUCTURE
    [decoder encodeObject:self.last_update forKey:@"last_update"];
    [decoder encodeObject:self.lingua forKey:@"lingua"];
    [decoder encodeObject:self.linguaFix forKey:@"linguaFix"];
    [decoder encodeObject:self.ancore_array forKey:@"ancore_array"];
    [decoder encodeObject:self.readings_title forKey:@"readings_title"];
    
    
    //ORDER OF THE MASS
    [decoder encodeObject:self.ordinario_title forKey:@"ordinario_title"];
    [decoder encodeObject:self.introductory_rites forKey:@"introductory_rites"];
    [decoder encodeObject:self.eucharistic_liturgy forKey:@"eucharistic_liturgy"];
    [decoder encodeObject:self.communion_rites forKey:@"communion_rites"];
    
    //PRO MESSA
    [decoder encodeObject:self.celebration_communion_rites forKey:@"celebration_communion_rites"];
    [decoder encodeObject:self.celebration_conclusion_rites forKey:@"celebration_conclusion_rites"];
    
    //PREFAZIO
    [decoder encodeObject:self.prefazio_title forKey:@"prefazio_title"];
    [decoder encodeObject:self.prefazio_array forKey:@"prefazio_array"];

    
    //LITURGIA EUCARISTICA
    [decoder encodeObject:self.liturgiaeucaristica_title forKey:@"liturgiaeucaristica_title"];
    [decoder encodeObject:self.liturgiaeucaristica_array forKey:@"liturgiaeucaristica_array"];

    
    //PREGHIERA DEI FEDELI
    [decoder encodeObject:self.preghieredeifedeli_title forKey:@"preghieredeifedeli_title"];
    [decoder encodeObject:self.preghieredeifedeli_array forKey:@"preghieredeifedeli_array"];

    
    //BENEDIZIONI SOLENNI
    [decoder encodeObject:self.benedizionisolenni_title forKey:@"benedizionisolenni_title"];
    [decoder encodeObject:self.benedizionisolenni_array forKey:@"benedizionisolenni_array"];

    
    //PRAYERS
    [decoder encodeObject:self.prayers forKey:@"prayers"];

    
    //RITI
    [decoder encodeObject:self.riti forKey:@"riti"];

    
    //TESTI COMUNI BREVIARIO
    [decoder encodeObject:self.last_update_common_breviary forKey:@"last_update_common_breviary"];
    [decoder encodeObject:self.breviary_comuni_array forKey:@"breviary_comuni_array"];

    
    //TESTI COMUNI MISSAL
    [decoder encodeObject:self.last_update_common_missal forKey:@"last_update_common_missal"];
    [decoder encodeObject:self.missal_comuni_array forKey:@"missal_comuni_array"];


}

//======================================
//Decoding (FOR READ SAVED OBJECT)
//======================================
#pragma mark - DECODING
- (id)initWithCoder: (NSCoder *)aCoder {
    self = [super init];
    if (self) {

        //FIX_STRUCTURE
        self.last_update = [aCoder decodeObjectForKey:@"last_update"];
        self.lingua = [aCoder decodeObjectForKey:@"lingua"];
        self.linguaFix = [aCoder decodeObjectForKey:@"linguaFix"];
        self.ancore_array = [aCoder decodeObjectForKey:@"ancore_array"];
        self.readings_title = [aCoder decodeObjectForKey:@"readings_title"];

        
        //ORDER OF THE MASS
        self.ordinario_title = [aCoder decodeObjectForKey:@"ordinario_title"];
        self.introductory_rites = [aCoder decodeObjectForKey:@"introductory_rites"];
        self.eucharistic_liturgy = [aCoder decodeObjectForKey:@"eucharistic_liturgy"];
        self.communion_rites = [aCoder decodeObjectForKey:@"communion_rites"];
        
        //PRO MESSA
        self.celebration_communion_rites = [aCoder decodeObjectForKey:@"celebration_communion_rites"];
        self.celebration_conclusion_rites = [aCoder decodeObjectForKey:@"celebration_conclusion_rites"];
        
        //PREFAZIO
        self.prefazio_title = [aCoder decodeObjectForKey:@"prefazio_title"];
        self.prefazio_array = [aCoder decodeObjectForKey:@"prefazio_array"];

        
        //LITURGIA EUCARISTICA
        self.liturgiaeucaristica_title = [aCoder decodeObjectForKey:@"liturgiaeucaristica_title"];
        self.liturgiaeucaristica_array = [aCoder decodeObjectForKey:@"liturgiaeucaristica_array"];

        
        //PREGHIERA DEI FEDELI
        self.preghieredeifedeli_title = [aCoder decodeObjectForKey:@"preghieredeifedeli_title"];
        self.preghieredeifedeli_array = [aCoder decodeObjectForKey:@"preghieredeifedeli_array"];

        
        //BENEDIZIONI SOLENNI
        self.benedizionisolenni_title = [aCoder decodeObjectForKey:@"benedizionisolenni_title"];
        self.benedizionisolenni_array = [aCoder decodeObjectForKey:@"benedizionisolenni_array"];

        
        //PRAYERS
        self.prayers = [aCoder decodeObjectForKey:@"prayers"];

        
        //RITI
        self.riti = [aCoder decodeObjectForKey:@"riti"];

        
        //TESTI COMUNI BREVIARIO
        self.last_update_common_breviary = [aCoder decodeObjectForKey:@"last_update_common_breviary"];
        self.breviary_comuni_array = [aCoder decodeObjectForKey:@"breviary_comuni_array"];

        
        //TESTI COMUNI MISSAL
        self.last_update_common_missal = [aCoder decodeObjectForKey:@"last_update_common_missal"];
        self.missal_comuni_array = [aCoder decodeObjectForKey:@"missal_comuni_array"];
    }
    
    return self;
}


#pragma mark cacheDir
//==================
//CACHE DIRECTORY istance
//==================
- (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}

#pragma mark documentDir
//==================
//DOCUMENT DIRECTORY istance
//==================
- (NSString *)documentDir {
    NSArray* documentPathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* docPath = [documentPathArray lastObject];
    // NSLog(@"DocDir:%@",docPath);
    return docPath;
}

//==================
// description
//==================
#pragma mark - description
-(NSString*)description{
    return [NSString stringWithFormat:@"<fix lingua:%@ last:%@>",self.lingua,self.last_update];
}




//=================
//END
//=================
@end
