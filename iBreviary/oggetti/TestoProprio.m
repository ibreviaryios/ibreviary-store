//
//  TestoProprio.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import "TestoProprio.h"

@implementation TestoProprio
/*
 JSON sample: 
            {
            image = "http://www.ibreviary.com/img_db.php?t=e&amp;id=1&amp;n=1";
             logo = "http://www.ibreviary.com/img_db.php?t=e&amp;id=1&amp;n=1";
            title =	{
                    en = "Holy land";
                    es = "Tierra Santa";
                    fr = "Terre Sainte";
                    it = "Terra Santa";
                    la = "Terra Sancta";
                    ra = "Terra Santa";
                    ro = "Holy Land";
                    vt = "Terra Sancta";
                    };
            type = "TerraSanta";
            }
 */

//SYNTHESIZE (readonly non sono sintetizzate)


//=================
// INIT
//=================
#pragma mark - INIT

- (id)init{
    if ((self = [super init])){
    }
    return self;
}


- (id)initWithDictionary:(NSDictionary*)d{
    if ((self = [super init])){
        ;
        self.titolo = [[d objectForKey:@"title"] objectForKey:NSLocalizedString(@"checklingua", nil)]?[[d objectForKey:@"title"] objectForKey:NSLocalizedString(@"checklingua", nil)]:[[d objectForKey:@"title"] objectForKey:@"en"];

        self.key = [d objectForKey:@"type"]?[d objectForKey:@"type"]:@"";
        self.image_official = nil;
        self.logo = nil;
        self.url_image_official = [d objectForKey:@"image"]?[d objectForKey:@"image"]:@"";
        self.url_image_logo = [d objectForKey:@"logo"]?[d objectForKey:@"logo"]:@"";
        self.isForHome = ([d objectForKey:@"home"] && [[d objectForKey:@"home"] integerValue]);

     }
    // NSLog(@"New TESTO PROPRIO:%@",[self description]);
    return self;
}
//=========================
// CLASS METHOD
//=========================
#pragma mark - get METHOD



//=========================
// CLASS METHOD
//=========================
#pragma mark - CLASS METHOD
//CACHE DIRECTORY
+ (NSString *)cacheDir {
    NSArray* documentPathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* docPath = [documentPathArray lastObject];
    // NSLog(@"DocDir:%@",docPath);
    return docPath;
}


+(BOOL)fileExistsAtPath:(NSString*)path{
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}


//=======================
// GET TP
//=======================
#pragma mark - GET TP
+(NSMutableArray*)getTestiPropri
{
    NSMutableArray * lista = [NSMutableArray new];
    NSString *path = [[TestoProprio cacheDir] stringByAppendingPathComponent:PATH_TESTIPROPRI];
    if(![TestoProprio fileExistsAtPath:path])
    {
        // NSLog(@"LISTA TP non esiste");
        return lista;
    }
    
    NSData *d = [[NSData alloc] initWithContentsOfFile:path];
    lista = [NSKeyedUnarchiver unarchiveObjectWithData:d];
    // NSLog(@"\nLISTA TP:\n%@\n\n",lista);
    return lista;
}

+(int)getPosTestoProprioWithKey:(NSString*)k{
    // NSLog(@"getTestoProprioWithKey:%@",k);
    if(!k || [k isEqualToString:@""] || [k isEqualToString:@"generic"]) return 0;
    
    NSMutableArray * array = [TestoProprio getTestiPropri];
    for (int i = 0;i < array.count; i++)
        {
            TestoProprio *t = [array objectAtIndex:i];
            if ([t.key isEqualToString:k]) return i;
        }
    return 0;
}

+(TestoProprio*)getTestoProprioWithKey:(NSString*)k{
    // NSLog(@"getTestoProprioWithKey:%@",k);
    
    if(k!=nil && ![k isEqualToString:@""] && ![k isEqualToString:@"generic"])
    {
    NSMutableArray * array = [TestoProprio getTestiPropri];
    for (TestoProprio *t in array)
            {
            if ([t.key isEqualToString:k])
                {
                    // NSLog(@"Trovato:%@",t);
                    return t;
                }
            }
    }
    
    // NSLog(@"TP Non Selezionato");
    //elemento no selezione
    TestoProprio *no_testiPropri = [TestoProprio new];
    [no_testiPropri setKey:@""];
    [no_testiPropri setLogo:[UIImage imageNamed:@"logo_.png"]];
    [no_testiPropri setImage_official:[UIImage imageNamed:@"logo_.png"]];
    return no_testiPropri;
}

+(NSMutableArray*)getTestiPropriForHome{
    // NSLog(@"getTestiPropriForHome");
    NSMutableArray * array = [NSMutableArray new];
    for (TestoProprio *t in [TestoProprio getTestiPropri])
    {
        if (t.isForHome) [array addObject:t.key];
    }
    
    return array;
}


//ordinare lista oggetti secondo un attributo
- (NSComparisonResult)compare:(TestoProprio *)otherObject {
    return [self.titolo compare:otherObject.titolo];
}

+(BOOL)isForHomeTPwithKey:(NSString*)k{
    TestoProprio *r = [TestoProprio getTestoProprioWithKey:k];
    return r.isForHome;
}
//=======================
//SET TP
//=======================
#pragma mark - SET TP
+(void)setTestiPropri:(NSArray*)arrayTP{
    // NSLog(@"setTestiPropri:%@",arrayTP);

    NSMutableArray *array_oggetti = [NSMutableArray new];
    
    for (int i=0; i<arrayTP.count; i++) {
        TestoProprio *row = [[TestoProprio alloc] initWithDictionary:[arrayTP objectAtIndex:i]];
        // NSLog(@"array_tpROW:%@",row);
        [row scaricaImmagini];
        [array_oggetti addObject:row];
        }
    
    NSArray *sortedArray = [array_oggetti sortedArrayUsingSelector:@selector(compare:)];
    
    NSMutableArray *array_finale = [NSMutableArray new];

    
    //elemento no selezione
    TestoProprio *no_testiPropri = [TestoProprio new];
    [no_testiPropri setKey:@""];
    [no_testiPropri setLogo:[UIImage imageNamed:@"logo_.png"]];
    [no_testiPropri setImage_official:[UIImage imageNamed:@"logo_no.png"]];
    [array_finale addObject:no_testiPropri];

    [array_finale addObjectsFromArray:sortedArray];


    NSData *d = [NSKeyedArchiver archivedDataWithRootObject:array_finale];

    if(![TestoProprio fileExistsAtPath:[[TestoProprio cacheDir] stringByAppendingPathComponent:PATH_TESTIPROPRI]])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:[[TestoProprio cacheDir] stringByAppendingPathComponent:@"/DATA"] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    // NSLog(@"Saving...");
    NSString *tpPath = [self.cacheDir stringByAppendingPathComponent:PATH_TESTIPROPRI];
    
    BOOL success = [d writeToFile:tpPath atomically:YES];
    if(success){
        NSError *error = nil;
        BOOL successExclude = [[NSURL fileURLWithPath:tpPath] setResourceValue:[NSNumber numberWithBool:YES]
                                                                          forKey:NSURLIsExcludedFromBackupKey
                                                                           error: &error];
        if(!successExclude){
            NSLog(@"TP: addSkipBackupAttributeToItemAtURL: Error excluding %@ from backup %@", [[NSURL URLWithString:tpPath] lastPathComponent], error);
        }
    }
}


-(void)scaricaImmagini{
    //DOWNLOAD IMAGE
    NSURL *url = [NSURL URLWithString:self.url_image_official];
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0+TIME_OUT_REQUEST];
    NSError *err = nil;
    // NSLog(@"request:%@",theRequest);
    NSData *dataI = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err];
    if(err!=nil)
    {
        //detail o download dovranno controllare la presenza del file!
        // NSLog(@"ERR[%@] o_immagine %@",self.key,[err description]);
    }
    
    [self setImage_official:[UIImage imageWithData:dataI]];
    
    //DOWNLOAD // NSLogO
    if(![self.url_image_logo isEqualToString:@""])
    {
    NSURL *url2 = [NSURL URLWithString:self.url_image_logo];
    NSURLRequest *theRequest2=[NSURLRequest requestWithURL:url2 cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0+TIME_OUT_REQUEST];
    NSError *err2 = nil;
    NSData *dataI2 = [NSURLConnection sendSynchronousRequest:theRequest2 returningResponse:nil error:&err2];
    if(err2!=nil)
    {
        //detail o download dovranno controllare la presenza del file!
        // NSLog(@"ERR[%@]  logo_immagine %@",self.key,[err2 description]);
    }
    
        [self setLogo:[UIImage imageWithData:dataI2]?[UIImage imageWithData:dataI2]:self.image_official];
    }
    
    else [self setLogo:self.image_official];

    // NSLog(@"Fine Download Immagini ->%@",self.key);
}


//=======================================
//Encoding (FOR SAVING OBJECT)
//=======================================
#pragma mark - ENCODING
-(void)encodeWithCoder:(NSCoder*)decoder{

    [decoder encodeObject:self.titolo forKey:@"titolo"];
    [decoder encodeObject:self.key forKey:@"key"];
    [decoder encodeObject:self.image_official forKey:@"image_official"];
    [decoder encodeObject:self.logo forKey:@"logo"];
    [decoder encodeObject:self.url_image_official forKey:@"url_image_official"];
    [decoder encodeObject:self.url_image_logo forKey:@"url_image_logo"];
    
    [decoder encodeBool:self.isForHome forKey:@"isForHome"];

}


//======================================
//Decoding (FOR READ SAVED OBJECT)
//======================================
#pragma mark - DECODING
- (id)initWithCoder: (NSCoder *)aCoder {
    self = [super init];
    if (self) {
        self.titolo = [aCoder decodeObjectForKey:@"titolo"];
        self.key = [aCoder decodeObjectForKey:@"key"];
        self.image_official = [aCoder decodeObjectForKey:@"image_official"];
        self.logo = [aCoder decodeObjectForKey:@"logo"];
        self.url_image_official = [aCoder decodeObjectForKey:@"url_image_official"];
        self.url_image_logo = [aCoder decodeObjectForKey:@"url_image_logo"];
        
        self.isForHome = [aCoder decodeBoolForKey:@"isForHome"];
        }
    
    return self;
}


//==================
//// NSLog %@
//==================
#pragma mark - // NSLog
-(NSString*)description{
    return [NSString stringWithFormat:@"<tp:%@ forHome[%i] imageOfficial:%@>", self.key,self.isForHome,self.image_official];
}




//=================
//END
//=================
@end
