//
//  NotificaObject.h
//  iBreviary
//
//  Created by leyo on 30/09/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NotificaObject : NSObject

@property (nonatomic, retain) NSNumber * id_notifica;
@property (nonatomic, retain) NSString * titolo;
@property (nonatomic, retain) NSString * lingua;
@property (nonatomic, retain) NSString * contenuto;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSString * link_titolo;
@property (nonatomic, retain) NSString * last_update;
@property (nonatomic, retain) NSDate * data_inizio;
@property (nonatomic, retain) NSDate * fired;
@property (nonatomic, retain) NSNumber * giorni_frequenza;
@property (nonatomic, retain) NSNumber * soloUnaVolta;
@property (nonatomic, retain) NSNumber * sempre_presente_in_home;
@property (nonatomic, retain) NSNumber * attivo;
@property (nonatomic, retain) NSNumber * letto;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, readonly) BOOL giaLetta;

//Istance methods
- (NSMutableDictionary*)dictionary_userInfo;
- (void)ScheduleLocalNotification;
- (void)notificaDaMostrare;
- (void)impostaNotificaLetta;
- (void)impostaDaLeggereEbadge;
- (BOOL)notificaScaduta;
- (BOOL)save;

//+(BOOL)deleteAllNotifiche;

//GET NOTIFICHE
+(NotificaObject*)notificaConId:(NSNumber*)i_n;
+(NSArray*)getNotifiche;
+(NSInteger)notificheDaleggere;
+(NSSet*)notificheLette;
//HOME
+(NSArray*)getNotifichePerHome;


//CHECK NOTIFICHE
+(void)checkNotifiche;
+(void)updateHome;

//RESET NOTIFICHE
+(void)resetNotifiche;

+(NSString*)debugString;

// NOTIFICATION TYPE
#ifdef __IPHONE_8_0
+ (BOOL)checkNotificationType:(UIUserNotificationType)type;
#endif

// BADGE NUMBER
+ (void)setApplicationBadgeNumber:(NSInteger)badgeNumber;

//send notifica
+ (void)sendLocalNotification:(NotificaObject*)n;
+ (void)sendLocalNotificationTestAfterSecond:(NSInteger)sec;

@end
