//
//  MissalObject.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "Item.h"

@interface MissalObject :  NSObject<NSCoding>{
    
}

//STRUCTURE
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subtitle;
@property (strong, nonatomic) NSString *type;
@property (nonatomic) BOOL obbligatorio;
@property (nonatomic) int id_santo;
@property (nonatomic) NSString *id_event;

//MISSAL
@property (strong, nonatomic) NSString *variant;
@property (strong, nonatomic) Item *introduction;
@property (strong, nonatomic) Item *introductory_rites;
@property (strong, nonatomic) Item *eucharistic_liturgy;
@property (strong, nonatomic) Item *communion_rites;
@property (strong, nonatomic) Item *antiphon_and_opening_prayer;
@property (strong, nonatomic) Item *first_reading;
@property (strong, nonatomic) Item *second_reading;
@property (strong, nonatomic) Item *responsorial_psalm;
@property (strong, nonatomic) Item *gospel_acclamation;
@property (strong, nonatomic) Item *gospel;
@property (strong, nonatomic) Item *prayer_of_the_faithful;
@property (strong, nonatomic) Item *prayer_over_the_gifts;
@property (strong, nonatomic) Item *preface;
@property (strong, nonatomic) Item *antiphon_and_prayer_after_communion;
@property (strong, nonatomic) Item *custom;

//INIT
-(id)initWithDictionary:(NSDictionary*)json_missal;

// ISTANCE METHOD
-(Item*)messaleComune;

// CLASS METHOD


@end
