//
//  Nota.m
//  iBreviary
//
//  Created by Leonardo Parenti on 15/03/15.
//  Copyright (c) 2015 leyo. All rights reserved.
//

#import "Nota.h"
#import "DataObject.h"
#import "GlobalFunction.h"
#import "AppDelegate.h"
#import "Home.h"
#import "Item.h"

@interface ContentViewForScroller ()

@end

@implementation ContentViewForScroller

//=========================
// SET METHOD & INT
//=========================
#pragma mark - INIT
-(void)incrementFontSize:(NSInteger)newFontSize forScrollView:(UIScrollView*)scroll{
    int fontSize = IS_IPAD?29:24;
    


    CGFloat fix_width = scroll.frame.size.width;
    
    //LABEL FRAME
    CGFloat margin_left = IS_IPAD?12:10;
    CGFloat margin_top = IS_IPAD?30:20.0;
    CGFloat margin_bottom = IS_IPAD?30:20.0;
    CGRect label_frame = CGRectMake(margin_left, 0, fix_width-(2*margin_left), MAXFLOAT);

    
    // Set current font size
    [self.label setFont:[UIFont fontWithName:FONT_PALATINO_ROMAN size:fontSize]];
    
    CGRect b  = [self.label.text boundingRectWithSize:CGSizeMake(self.label.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:fontSize]} context:nil];
    //int line_number = (int)b.size.height/fontSize;
    
    int maxFontSize = IS_IPAD?34:26;
    
    while(b.size.height < scroll.frame.size.height/2.0 && fontSize < maxFontSize){
        fontSize += 2;
        if(fontSize>maxFontSize) fontSize = maxFontSize;
        [self.label setFont:[UIFont fontWithName:FONT_PALATINO_ROMAN size:fontSize]];
        
        b  = [self.label.text boundingRectWithSize:CGSizeMake(self.label.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:fontSize]} context:nil];
    }
    
    fontSize += newFontSize;
    
    // Set current font size
    [self.label setFont:[UIFont fontWithName:FONT_PALATINO_ROMAN size:fontSize]];
    
    b  = [self.label.text boundingRectWithSize:CGSizeMake(self.label.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:fontSize]} context:nil];

    
    label_frame.size.height = b.size.height;
    
    
    
    
    [self.label setFrame:label_frame];
    [self.label sizeToFit];
    b = self.label.bounds;
    [self.label setAdjustsFontSizeToFitWidth:YES];
    [self.label setCenter:(CGPoint){scroll.frame.size.width/2.0,self.label.center.y}];
    
    
    
    
    CGSize newSize = b.size;
    CGFloat minH = (31)*2;
    CGFloat minW = 43*2;
    if(newSize.height<minH) newSize.height=minH;
    if(newSize.width<minW) newSize.width=minW;
    
    newSize.height +=10;
    newSize.width +=25;
    
    
    [self.quote setFrame:(CGRect){0,0,newSize.width,newSize.height}];

    [self.quote setCenter:self.label.center];
    
    [scroll setContentSize:(CGSize){scroll.frame.size.width,margin_top+self.label.frame.size.height+margin_bottom}];
}


@end


@implementation Nota

@dynamic title;
@dynamic html_text;
@dynamic timestamp;
@dynamic creation_date;
@dynamic text;
@dynamic item_sender_text;

//=========================
// SET METHOD & INT
//=========================
#pragma mark - INIT
+ (void)initialize {
    if (self == [Nota class]) {
        DataObject *transformer = [[DataObject alloc] init];
        [NSValueTransformer setValueTransformer:transformer forName:@"DataObject"];
    }
}

//=========================
// CREA/MODIFICA
//=========================
#pragma mark - CREA/MODIFICA
+(Nota*)addNotaFromCopyAction:(NSString*)clip_string andItem:(Item*)i forContext:(NSManagedObjectContext*)context{
    // NSLog(@"Nota:addNotificaWithDict:%@",clip_string);
    if(clip_string.length<=0) return nil;
    //Creiamo un'istanza di NSManagedObject per l'Entità che ci interessa
    Nota *new_nota = [NSEntityDescription insertNewObjectForEntityForName:@"Nota" inManagedObjectContext:context];
    
    [new_nota createFromString:clip_string  andItem:(Item*)i  withContext:context];
    return new_nota;
}

#pragma mark CREA NOTA
//=========================
// CREA NOTA
//=========================
- (Nota*)createFromString:(NSString*)s  andItem:(Item*)i  withContext:(NSManagedObjectContext*)context{
    
    //SETTO TEXT
    self.text = s;
    //SETTO DATA
    self.creation_date = [NSDate date];
    self.timestamp = [[NSDate alloc] initWithTimeInterval:0 sinceDate:self.creation_date];
    if(i){
        //SETTO SENDER STRING
        self.item_sender_text = [i defaultWebContentString];
    }
    
    [self saveInContext:context];
    return self;
}

+(void)info{
    // NSLog(@"main:%@",[AppDelegate mainManagedObjectContext]);
    // NSLog(@"private:%@",[AppDelegate privateQueueContext]);
    
    // NSLog(@"notMain:%@",[Nota getNoteInContext:[AppDelegate mainManagedObjectContext]]);
    
    //NSLog(@"notPriv:%@",[Nota getNoteInContext:[AppDelegate privateQueueContext]]);
}

-(BOOL)saveInContext:(NSManagedObjectContext*)context{
    //SETTO DATA
    self.timestamp = [NSDate date];

    __block BOOL risp = NO;
    runOnMainQueueWithoutDeadlocking(^{
            [AppDelegate saveContext];
    });
   
    //[Nota info];
    return risp;
}

-(BOOL)hasSourceText{
    return (self.item_sender_text!=nil && self.item_sender_text.length>0);
}


-(NSString*)source_Htmlstring{
    NSString *path = [[NSBundle mainBundle] bundlePath];
    
    NSString *header=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/header_spiritual.html",path] encoding:NSUTF8StringEncoding error:nil];
    
    NSString *footer=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/footer.html",path] encoding:NSUTF8StringEncoding error: nil];
    
    NSString *zoom=[NSString stringWithFormat:@"<div ID=\"ciccio\" class=\"zoomClass\" >100</div>"];
    
    NSString *width = IS_IPAD?@"448":@"300";
    NSString *padding = IS_IPAD?@"10":@"5";
    header = [NSString stringWithFormat:header,width,padding];
    return [NSString stringWithFormat:@"%@%@%@%@",header,zoom,self.item_sender_text,footer];
    
}

#pragma mark CARTOLINA
//=====================
// CARTOLINA
//=====================
- (UIImage*)cartolina{
    if([[NSFileManager defaultManager] fileExistsAtPath:self.pathSave isDirectory:nil]){
        return [UIImage imageWithContentsOfFile:self.pathSave];
    }
    CGRect frame = CGRectZero;
    CGFloat fix_width = 1200;
    CGFloat label_minimalHeight = fix_width*254/484;
    CGFloat heigh_scale = label_minimalHeight/254;
    CGFloat width_scale = fix_width/484;
    
    //LABEL FRAME
    CGFloat margin_left = 30.0*width_scale;
    
    CGFloat margin_top = 37.0*heigh_scale;
    CGFloat margin_bottom = 50.0*heigh_scale;
    CGRect label_frame = CGRectMake(margin_left, margin_top, fix_width-(2*margin_left), label_minimalHeight);
    
    //LABEL
    UILabel *text_label = [[UILabel alloc] initWithFrame:label_frame];
    text_label.attributedText = [[NSAttributedString alloc] initWithString:@""];
    [text_label setText:self.text];
    [text_label setTextColor:UIColorFromRGB(0X814220)];
    [text_label setTextAlignment:NSTextAlignmentCenter];
    [text_label setNumberOfLines:0];
    int fontSize = 30*heigh_scale;
    

    // Set current font size
    [text_label setFont:[UIFont fontWithName:FONT_PALATINO_ROMAN size:fontSize+(fontSize*0.2)]];
    

    CGFloat fixedWidth = label_frame.size.width;
    CGRect b  = [self.text boundingRectWithSize:CGSizeMake(fixedWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:fontSize]} context:nil];
    //int line_number = (int)b.size.height/fontSize;

    if(b.size.height<label_minimalHeight){
        b.size.height = label_minimalHeight;
    }
    
    label_frame.size.height = b.size.height;
        

    
    [text_label setFrame:label_frame];
    [text_label sizeToFit];
    b = text_label.bounds;
    [text_label setMinimumScaleFactor:0.8];
    [text_label setAdjustsFontSizeToFitWidth:YES];
    
    //QUOTE size
    CGSize quoteSize = b.size;
    CGFloat minH = (31)*2;
    CGFloat minW = 43*2;
    if(quoteSize.height<minH) quoteSize.height=minH;
    if(quoteSize.width<minW) quoteSize.width=minW;
    quoteSize.height +=10;
    quoteSize.width +=25;


    //SET MAIN FRAME size
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size.height = margin_top + margin_bottom + text_label.frame.size.height;
    frame.size.width =  text_label.frame.size.width+(2*margin_left);
    
    
    UIView *cartolina_view = [[UIView alloc] initWithFrame:frame];
    [cartolina_view setBackgroundColor:[UIColor whiteColor]];
    
    //Background
    UIImageView *background = [[UIImageView alloc] initWithFrame:frame];
    [background setBackgroundColor:[UIColor clearColor]];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    [background setImage:[UIImage imageNamed:@"background_cartolinaL.png"]];
    
    //Logo SX
    UIImageView *logoSX = [[UIImageView alloc] initWithFrame:CGRectMake((10*width_scale), -(36*heigh_scale)+frame.size.height, 26*width_scale, 26*heigh_scale)];
    [logoSX setBackgroundColor:[UIColor clearColor]];
    [logoSX setContentMode:UIViewContentModeScaleAspectFit];
    [logoSX setImage:[UIImage imageNamed:@"Icon-120.png"]];
    [logoSX.layer setCornerRadius:4*heigh_scale];
    [logoSX.layer setMasksToBounds:YES];
    
    //Logo DX
    UIImageView *logoDX = [[UIImageView alloc] initWithFrame:CGRectMake(-(145*width_scale)+frame.size.width, -(30*heigh_scale)+frame.size.height, 150*width_scale, 20*heigh_scale)];
    [logoDX setBackgroundColor:[UIColor clearColor]];
    [logoDX setContentMode:UIViewContentModeScaleAspectFit];
    [logoDX setImage:[UIImage imageNamed:@"powered_iBrev.png"]];

 
    //Quote
    UIImageView *quote = [[UIImageView alloc] initWithFrame:(CGRect){0,0,quoteSize.width,quoteSize.height}];
    [quote setBackgroundColor:[UIColor clearColor]];
    [quote setAlpha:1];
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(31*heigh_scale, 43*width_scale, 31*heigh_scale, 43*width_scale);
    //immagine deve avere la widht del frame
    [quote setImage:[[UIImage imageNamed:@"QuoteBackgroundShare.png"] resizableImageWithCapInsets:edgeInsets  resizingMode:UIImageResizingModeStretch]];
    
 
    
    
    //ADD SUBVIEWs
    [cartolina_view addSubview:background];
    [cartolina_view addSubview:logoSX];
    [cartolina_view addSubview:logoDX];
    if(self.hasSourceText)[cartolina_view addSubview:quote];
    [cartolina_view addSubview:text_label];
    
    [text_label setCenter:(CGPoint){cartolina_view.frame.size.width/2.0,text_label.center.y}];
    [quote setCenter:text_label.center];



    UIGraphicsBeginImageContextWithOptions(cartolina_view.frame.size,YES,0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [cartolina_view.layer renderInContext:context];
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    

    if([UIImagePNGRepresentation(capturedImage) writeToFile:self.pathSave atomically:YES]){
        NSLog(@"cartolina:saved");
    }
    
    
    
    return capturedImage;

    }

#pragma mark - FULL SCROLL CONTENT
//==============================
// FULL SCROLL CONTENT
//==============================
- (ContentViewForScroller*)fullContentForScrollView:(UIScrollView*)scroll{
    ContentViewForScroller *content = [ContentViewForScroller new];
    CGRect frame = CGRectZero;
    CGFloat fix_width = scroll.frame.size.width;
    
    //LABEL FRAME
    CGFloat margin_left = IS_IPAD?12:10;
    CGFloat margin_top = IS_IPAD?30:20.0;
    CGFloat margin_bottom = IS_IPAD?30:20.0;
    CGRect label_frame = CGRectMake(margin_left, 0, fix_width-(2*margin_left), MAXFLOAT);
    
    //LABEL
    content.label = [UILabel new];
    [content.label setFrame:label_frame];
    content.label.attributedText = [[NSAttributedString alloc] initWithString:@""];
    [content.label setText:self.text];
    [content.label setTextColor:UIColorFromRGB(0X814220)];
    [content.label setTextAlignment:NSTextAlignmentCenter];
    [content.label setNumberOfLines:0];
    [content.label setBackgroundColor:[UIColor clearColor]];
    int fontSize = IS_IPAD?29:24;
    
    
    // Set current font size
    [content.label setFont:[UIFont fontWithName:FONT_PALATINO_ROMAN size:fontSize]];
    
    CGRect b  = [self.text boundingRectWithSize:CGSizeMake(label_frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:fontSize]} context:nil];
    //int line_number = (int)b.size.height/fontSize;
    
    int maxFontSize = IS_IPAD?34:26;
    
    while(b.size.height < scroll.frame.size.height/2.0 && fontSize < maxFontSize){
        fontSize += 2;
        if(fontSize>maxFontSize) fontSize = maxFontSize;
        [content.label setFont:[UIFont fontWithName:FONT_PALATINO_ROMAN size:fontSize]];
        
        b  = [self.text boundingRectWithSize:CGSizeMake(label_frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:fontSize]} context:nil];
    }
    
    
    label_frame.size.height = b.size.height;
    
    
    
    
    [content.label setFrame:label_frame];
    [content.label sizeToFit];
    b = content.label.bounds;
    [content.label setAdjustsFontSizeToFitWidth:YES];
    [content.label setCenter:(CGPoint){scroll.frame.size.width/2.0,content.label.center.y}];
    
    
    //SET MAIN FRAME size
    frame.origin.x = 0;
    frame.origin.y = margin_top;
    frame.size.height =  margin_bottom + content.label.frame.size.height;
    frame.size.width =  scroll.frame.size.width;
    
    
    content.contentView = [[UIView alloc] initWithFrame:frame];
    [content.contentView setBackgroundColor:[UIColor clearColor]];
    
    
    CGSize newSize = b.size;
    CGFloat minH = (31)*2;
    CGFloat minW = 43*2;
    if(newSize.height<minH) newSize.height=minH;
    if(newSize.width<minW) newSize.width=minW;
    
    newSize.height +=10;
    newSize.width +=25;

    
    //Quote
    content.quote = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, newSize.width,newSize.height)];
    
    [content.quote setBackgroundColor:[UIColor clearColor]];
    [content.quote setAlpha:0.25];
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(31, 43, 31, 43);
    [content.quote setImage:[[UIImage imageNamed:@"QuoteBackgroundADD_iphone.png"] resizableImageWithCapInsets:edgeInsets  resizingMode:UIImageResizingModeStretch]];

    //ADD SUBVIEWs
    if(self.hasSourceText)[content.contentView addSubview:content.quote];
    [content.contentView addSubview:content.label];
    
    [content.quote setCenter:content.label.center];
    
    [scroll addSubview:content.contentView];
    [scroll setContentSize:(CGSize){scroll.frame.size.width,margin_top+content.contentView.frame.size.height}];
    return content;
}

-(NSString*)pathSave{
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    
    return [cachePath stringByAppendingFormat:@"/nota%lu.png",(unsigned long)self.hash];
}

-(NSURL*)getImageUrl{
    if(![[NSFileManager defaultManager] fileExistsAtPath:self.pathSave]){
        [self cartolina];
    }
    return [NSURL fileURLWithPath:self.pathSave];
}

-(BOOL)removeImageWithError:(NSError**)err{
    return [[NSFileManager defaultManager] removeItemAtPath:self.pathSave error:err];
}


#pragma mark - OPERAZIONI SU LISTA NOTE
//==============================
// GET NOTE
//==============================
+(NSMutableArray*)getNoteInContext:(NSManagedObjectContext*)context
{
    if(context==nil) context = [AppDelegate mainManagedObjectContext];
    
    //istanzio la classe NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    //istanzio l'Entità da passare alla Fetch Request
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Nota" inManagedObjectContext:context];
    
    //Setto la proprietà Entity della Fetch Request ****IMPORTANTE!!
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:NO];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
    //Eseguo la Fetch Request e salviamo il risultato in un array, per visualizzarlo nella tabella
    NSError *error;
    NSArray *risposta = [context executeFetchRequest:fetchRequest error:&error];
    //NSLog(@"getNote:%@",risposta);
    return [NSMutableArray arrayWithArray:risposta];
    
}


-(NSString*)dateString{
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"dd MMMM yyyy, HH:mm"];
    if([NSLocalizedString(@"checklingua", nil) isEqualToString:@"en"]){
        [f setDateFormat:@"dd MMMM yyyy, hh:mm a"];
    }
    NSString *date_string = [f stringFromDate:self.timestamp];
    return  date_string;

}



//==============================
// NOTA CON TITLE
//==============================
+(NotificaObject*)getNotaWithTitle:(NSString*)tit inContext:(NSManagedObjectContext*)context{
    //NSLog(@"getNotaWithTitle:%@",tit);
    if(context==nil) context = [AppDelegate mainManagedObjectContext];
    NSArray *array = nil;
    NSError *error = nil;
    NSFetchRequest *request = [NSFetchRequest new];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Nota" inManagedObjectContext:context];
    
    [request setEntity:entity];
    NSPredicate *p = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"title == %@",tit]];
    [request setPredicate:p];
    [request setIncludesPendingChanges:NO];
    array = [NSArray arrayWithArray:[context executeFetchRequest:request error:&error]];
    if(error || array==nil){
       NSLog(@"Nota Errore request[%@] %@", request, [error description]);
    }
    
    
    if(array!=nil && array.count>0 && [[array objectAtIndex:0] isKindOfClass:[Nota class]]){
        return [array objectAtIndex:0];
    }
    
    else NSLog(@"Nota con %@ non trovata",p);
    
    return nil;
}

#pragma mark - DELETE METHOD
+(BOOL)deleteNota:(Nota*)n andError:(NSError**)e forContext:(NSManagedObjectContext*)context{
    @synchronized(context) {
    [context deleteObject:n];
    [AppDelegate saveContext];
    [NSThread sleepForTimeInterval:1];
    return YES;
    }
}

+(void)deleteAll{
    NSError *e = nil;
    for(Nota *n in [Nota getNoteInContext:[AppDelegate mainManagedObjectContext]]){
        [Nota deleteNota:n andError:&e forContext:[AppDelegate mainManagedObjectContext]];
        e = nil;
    }
    [AppDelegate saveContext];
    }


//=========================
// PDF FROM ARRAY
//=========================
#pragma mark - PDF FROM ARRAY
-(NSString *)htmlPdfString{
    NSString *html;
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *header=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/notaShareSingola.html",path] encoding:NSUTF8StringEncoding error:nil];
    html = [NSString stringWithFormat:header,[self dateString],[self text]];
    return html;
}

+(NSString*)htmlPdfStringFromArray:(NSArray*)note_list{
    // NSLog(@"htmlPdfString:%@",self);
    NSString *html;
    NSString *path = [[NSBundle mainBundle] bundlePath];
    
    NSString *header=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/header_pdf.html",path] encoding:NSUTF8StringEncoding error:nil];
    
    NSString *footer=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/footer.html",path] encoding:NSUTF8StringEncoding error: nil];
    
    
    NSMutableString *noteCorpo = [NSMutableString new];
    
    for(Nota*n in note_list){
        [noteCorpo appendString:[n htmlPdfString]];
        [noteCorpo appendString:@"<br />"];
    }
    
    
    // Prima Pagina
    NSString *primaPagina = [NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/share_titolo_nota.html",path] encoding:NSUTF8StringEncoding error:nil];
    
    NSString *titoloUfficiale = NSLocalizedString(@"pdf_mySpiritual_tit", nil);
    NSString *sottotitolo = NSLocalizedString(@"pdf_mySpiritual_subtit", nil);
    NSString *pre_data_text = NSLocalizedString(@"pdf_mySpiritual_data", nil);


    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"dd MMMM yyyy, hh:mm"];
    NSString *date_string = [f stringFromDate:[NSDate date]];
    NSString *dataCreazione = [NSString stringWithFormat:@"%@ %@",pre_data_text,date_string];
    if([sottotitolo isEqualToString:titoloUfficiale]) sottotitolo = @"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    NSString *share_messa_header = [NSString stringWithFormat:primaPagina,titoloUfficiale,sottotitolo,dataCreazione];
    
    
    
    html = [NSString stringWithFormat:@"%@%@%@%@",header,share_messa_header,noteCorpo,footer];
    // NSLog(@"HTML-LEtture[%@]: %@",self.sel_letture,[self.letture objectAtIndex:self.sel_letture.integerValue]);
    // NSLog(@"HTML-PDF: %@",html);
    return html;
}


@end
