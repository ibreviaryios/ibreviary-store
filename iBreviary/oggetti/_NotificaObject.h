//
//  NotificaObject.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NotificaObject : NSObject<NSCoding>{
    
}

//NotificaObject
@property (nonatomic) int id_notifica;
@property (strong, nonatomic) NSString *titolo;
@property (strong, nonatomic) NSString *lingua;
@property (strong, nonatomic) NSString *contenuto;
@property (nonatomic) int type;
@property (strong, nonatomic) NSString *link;
@property (strong, nonatomic) NSString *link_titolo;
@property (strong, nonatomic) NSDate *data_inizio;
@property (strong, nonatomic) NSDate *fired;
@property (nonatomic) int giorni_frequenza;
@property (nonatomic) BOOL soloUnaVolta;
@property (nonatomic) BOOL sempre_presente_in_home;
@property (nonatomic) BOOL attivo;
@property (nonatomic) BOOL letto;


-(id)initWithDictionary:(NSDictionary*)d;


//GET NOTIFICHE
+(NotificaObject*)notificaConId:(NSNumber*)id_notifica;
+(NSMutableArray*)getNotifiche;
+(void)ScheduleLocalNotification;
+(void)controllaNotificheDuplicate;
//HOME
+(NSArray*)getNotifichePerHome;
//RICORRENTI
+(NSMutableArray*)getNotificheRicorrenti;
+(void)deleteNotificheRicorrenti;
//ISTANTANEE
+(NSMutableArray*)getNotificheIstantanee;
+(void)deleteNotificheIstantanee;

//CHECK NOTIFICHE
+(void)checkNotifiche;
+(NSMutableArray*)getCurrentNotifiche;
+(void)resetCurrentNotifiche;
+(BOOL)addCurrentNotifica:(int)i;

+(void)controlloLocalNotifichePush:(UILocalNotification *)notification;
+(void)controlloLocalNotifichePushWithID:(NSNumber *)number_id;
+(void)controllanotificheincoda;


//NOTIFICHE DELEGATE
//=REMOTE
+(void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo;
+(void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken;
//=LOCAL
+(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification;
// POST TOKEN
+(void)postToken;

//BADGE NUMBER
+(void)incrementOneBadge;
+(void)decrementOneBadge;


@end
