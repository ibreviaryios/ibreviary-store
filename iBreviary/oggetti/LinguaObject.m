//
//  LinguaObject.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import "LinguaObject.h"
#import "SystemData.h"

@implementation LinguaObject
/*
 JSON sample: 
                {
                "code": "it",
                "title": "Italiano",
                "direction" : "ltr"; //or "rtl"
                "color": "#FFFFFF"
                }
 */

//SYNTHESIZE (readonly non sono sintetizzate)
@synthesize sigla,nome,colore,alignment_right;


//=================
// INIT
//=================
#pragma mark - INIT

- (id)init{
    if ((self = [super init])){
    }
    return self;
}


- (id)initWithDictionary:(NSDictionary*)d{
    if ((self = [super init])){
        self.nome = [d objectForKey:@"title"]?[d objectForKey:@"title"]:@"";
        // NSLog(@"D:%@",d);
        self.colore = [d objectForKey:@"color"]?[UIColor colorFromHexString:[d objectForKey:@"color"]]:[UIColor clearColor];
        self.sigla = [d objectForKey:@"code"]?[d objectForKey:@"code"]:@"";
        // NSLog(@"alignment:%@->%i",[d objectForKey:@"direction"],[[d objectForKey:@"direction"] isEqualToString:@"rtl"]);
        
        self.alignment_right = [[d objectForKey:@"direction"] isEqualToString:@"rtl"];
     }
    // NSLog(@"New LINGUA:%@",[self description]);
    return self;
}


//=========================
// CLASS METHOD
//=========================
#pragma mark - CLASS METHOD
//CACHE DIRECTORY
+ (NSString *)cacheDir {
    NSArray* documentPathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* docPath = [documentPathArray lastObject];
    // NSLog(@"DocDir:%@",docPath);
    return docPath;
}


+(BOOL)fileExistsAtPath:(NSString*)path{
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

-(UIView*)view{
    // NSLog(@"getLinguaList:ritorna->UIImageView[logo] NSString[title]");
    UIView *i = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    [i setBackgroundColor:[UIColor clearColor]];
    //[i setContentMode:UIViewContentModeCenter];
    //[i setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",l]]];
    [i setAlpha:0.65];
    UILabel *label = [[UILabel alloc] initWithFrame:i.frame];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:UIColorFromRGB(0XFFFFFF)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setText:self.sigla];
    [label setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:18]];
    UIView *pallino = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [pallino.layer setCornerRadius:15];
    [pallino setCenter:i.center];
    [pallino setBackgroundColor:self.colore];
    CGRect frame = i.frame;
    frame.origin.y -= 2.2;
    [label setFrame:frame];
    
    [i addSubview:pallino];
    [i addSubview:label];
    
    return i;
}

-(UIImage*)logo{
    UIImageView *i = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [i setBackgroundColor:[UIColor clearColor]];
    [i setAlpha:0.95];
    CGRect frame = i.frame;
    frame.size.height -=2.5;
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:UIColorFromRGB(0XFFFFFF)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setText:self.sigla];
    [label setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:36]];
    UIView *pallino = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 66, 66)];
    [pallino.layer setCornerRadius:pallino.frame.size.width/2.0];
    [pallino setCenter:CGPointMake(i.frame.size.width/2.0, i.frame.size.height/2.0)];
    [pallino setBackgroundColor:self.colore];
    
    [i addSubview:pallino];
    [i addSubview:label];
    
    //Trasformo la vista i in UIImage
    UIGraphicsBeginImageContextWithOptions((i.bounds.size), NO, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [i.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

//=======================
// GET LINGUE
//=======================
#pragma mark - GET LINGUE
+(NSMutableArray*)getLingue
{
    NSMutableArray * lista = [NSMutableArray new];
    NSString *path = [[LinguaObject cacheDir] stringByAppendingPathComponent:PATH_LINGUE];
    if(![LinguaObject fileExistsAtPath:path])
    {
        // NSLog(@"LISTA LINGUE non esiste");
        NSUserDefaults *defaults = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
        if([defaults boolForKey:AGGIORNAMENTO_2015]){
            [defaults removeObjectForKey:AGGIORNAMENTO_2015];
            [defaults synchronize];
            [SystemData aggiornamento2015];
        }
        return lista;
    }
    
    NSData *d = [[NSData alloc] initWithContentsOfFile:path];
    lista = [NSKeyedUnarchiver unarchiveObjectWithData:d];
    // NSLog(@"\nLISTA LINGUE:\n%@\n\n",lista);
    return lista;
}

+(LinguaObject*)getLinguaBySigla:(NSString*)s{
    NSMutableArray * array = [LinguaObject getLingue];
    for (LinguaObject *l in array)
            {
            if ([[l sigla] isEqualToString:s]) return l;
            }
    // NSLog(@"sigla[%@] Lingua Non Trovata",s);
    return [LinguaObject new];
}

+(LinguaObject*)getCurrentLingua{
    NSUserDefaults *defaults = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    NSString *s = [defaults objectForKey:LINGUA];
    NSMutableArray * array = [LinguaObject getLingue];
    for (LinguaObject *l in array)
    {
        if ([[l sigla] isEqualToString:s]) return l;
    }
    // NSLog(@"ERRORE Lingua[%@] impossibile!",s);
    return [LinguaObject new];
}

//ordinare lista oggetti secondo un attributo
- (NSComparisonResult)compare:(LinguaObject *)otherObject {
    return [self.sigla compare:otherObject.sigla];
}

//=======================
//SET LINGUE
//=======================
#pragma mark - SET LINGUE
+(void)setLingueFromArray:(NSArray*)array_lingue{
    // NSLog(@"setLingueFromArray:%@",array_lingue);

    NSMutableArray *array_oggetti_l = [NSMutableArray new];

    for (int i=0; i<array_lingue.count; i++) {
        LinguaObject *row = [[LinguaObject alloc] initWithDictionary:[array_lingue objectAtIndex:i]];
        // NSLog(@"array_lingueROW:%@",row);
        [array_oggetti_l addObject:row];
        }
    
    NSArray *sortedArray;
    sortedArray = [array_oggetti_l sortedArrayUsingSelector:@selector(compare:)];

    NSData *d = [NSKeyedArchiver archivedDataWithRootObject:sortedArray];

    if(![LinguaObject fileExistsAtPath:[[LinguaObject cacheDir] stringByAppendingPathComponent:PATH_LINGUE]])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:[[LinguaObject cacheDir] stringByAppendingPathComponent:@"/DATA"] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    // NSLog(@"Saving...");
    NSString *linguaPath = [[LinguaObject cacheDir] stringByAppendingPathComponent:PATH_LINGUE];
    
    BOOL success = [d writeToFile:linguaPath atomically:YES];
    if(success){
        NSError *error = nil;
        BOOL successExclude = [[NSURL fileURLWithPath:linguaPath] setResourceValue:[NSNumber numberWithBool:YES]
                                      forKey:NSURLIsExcludedFromBackupKey
                                       error: &error];
        if(!successExclude){
            NSLog(@"Lingua: addSkipBackupAttributeToItemAtURL: Error excluding %@ from backup %@", [[NSURL URLWithString:linguaPath] lastPathComponent], error);
        }
    }
    // NSLog(@"Salvata Lista lingue:%@",sortedArray);
}

//=======================================
//Encoding (FOR SAVING OBJECT)
//=======================================
#pragma mark - ENCODING
-(void)encodeWithCoder:(NSCoder*)decoder{
    
    [decoder encodeObject:self.nome forKey:@"nome"];
    [decoder encodeObject:self.colore forKey:@"colore"];
    [decoder encodeObject:self.sigla forKey:@"sigla"];
    [decoder encodeBool:self.alignment_right forKey:@"alignment_right"];

}


//======================================
//Decoding (FOR READ SAVED OBJECT)
//======================================
#pragma mark - DECODING
- (id)initWithCoder: (NSCoder *)aCoder {
    self = [super init];
    if (self) {
        self.nome = [aCoder decodeObjectForKey:@"nome"];
        self.colore = [aCoder decodeObjectForKey:@"colore"];
        self.sigla = [aCoder decodeObjectForKey:@"sigla"];
        self.alignment_right = [aCoder decodeBoolForKey:@"alignment_right"];

        }
    
    return self;
}


//==================
//// NSLog %@
//==================
#pragma mark - // NSLog
-(NSString*)description{
    return [NSString stringWithFormat:@"<lingua sigla:%@ nome:%@ dir:%@ colore:%@>",self.sigla,self.nome,self.alignment_right?@"right":@"left",self.colore];
}




//=================
//END
//=================
@end
