//
//  NotificaObject.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "NotificaObject.h"
#import "GlobalFunction.h"
#import "Home.h"

#define PATH_NOT @"/DATA/notifiche.data"
#define URL_NOTIFICHE @"http://www.ibreviary.org/iOS/services.php?type=getNot"

#define  NOTIFICA_DONAZIONI @"NOTIFICA_DONAZIONI"
#define  NOTIFICA_NEWSLETTER @"NOTIFICA_NEWSLETTER"
#define  NOTIFICA_TEMPO_TRASCORSO_DA_OGGI @"NOTIFICA_TEMPO_TRASCORSO_DA_OGGI"
#define  NOTIFICA_LOOP_CUSTOM @"NOTIFICA_LOOP_CUSTOM"
#define  NOTIFICA_LOOP_CUSTOM_DICT @"NOTIFICA_LOOP_CUSTOM_DICT"
#define  NOTIFICA_DATA_ORA @"NOTIFICA_DATA_ORA"
#define  NOTIFICA_ISTANTANEA @"NOTIFICA_ISTANTANEA"

@interface NotificaObject ()

@property (nonatomic) BOOL _letto;

@end

@implementation NotificaObject
/*
  sample:
                {
                "title": string,
                "content": htmlString
                }
 */


//SYNTHESIZE
@synthesize id_notifica;
@synthesize _letto = letto;
@synthesize titolo,lingua,contenuto,type,link,link_titolo;
@synthesize data_inizio,fired, giorni_frequenza;
@synthesize soloUnaVolta,attivo,sempre_presente_in_home;


//=================
// INIT
//=================
#pragma mark - INIT

- (id)init{
    if ((self = [super init])){
    }
    return self;
}


- (id)initWithDictionary:(NSDictionary*)d{
    if ((self = [super init])){
        NSLog(@"NotificaObj init:%@",d);
        self.id_notifica = [d objectForKey:@"id"]?[[d objectForKey:@"id"] intValue]:0;
        self.type = [[d objectForKey:@"type"] intValue];
        self.giorni_frequenza = [[d objectForKey:@"giorni_frequenza"] intValue];
        self.soloUnaVolta = [[d objectForKey:@"solo_una_volta"] isEqualToString:@"YES"];
        self.sempre_presente_in_home = [[d objectForKey:@"sempre_presente_in_home"] isEqualToString:@"YES"];
        NSDictionary *contenuto_localizzato = [d objectForKey:@"contenuto_localizzato"];
        NSLog(@"contenuto_localizzato(%i):%@",[[contenuto_localizzato objectForKey:@"attivo"] intValue]==1,contenuto_localizzato);
        
        self.titolo = [contenuto_localizzato objectForKey:@"titolo"]?[contenuto_localizzato objectForKey:@"titolo"]:nil;
        self.contenuto = [contenuto_localizzato objectForKey:@"contenuto"]?[contenuto_localizzato objectForKey:@"contenuto"]:nil;
        self.link = [contenuto_localizzato objectForKey:@"link"]?[contenuto_localizzato objectForKey:@"link"]:nil;
        self.link_titolo = [contenuto_localizzato objectForKey:@"link_titolo"]?[contenuto_localizzato objectForKey:@"link_titolo"]:nil;
        self.lingua = [contenuto_localizzato objectForKey:@"lingua"]?[contenuto_localizzato objectForKey:@"lingua"]:nil;
        self.attivo = [[contenuto_localizzato objectForKey:@"attivo"] intValue]==1?YES:NO;
        self.letto = NO;
        self.fired = nil;
        
        //SETTO DATA INIZIO
        self.data_inizio = nil;
        if([d objectForKey:@"data_inizio"])
        {
            NSDateFormatter *f = [NSDateFormatter new];
            NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
            [f setTimeZone:gmt];
            [f setDateFormat:@"yyyy-MM-dd"];
            NSString *date_string = [d objectForKey:@"data_inizio"];
            if([d objectForKey:@"ora_inizio"])
                {
                    [f setDateFormat:@"yyyy-MM-dd HH:mm"];
                    date_string = [date_string stringByAppendingFormat:@" %@",[d objectForKey:@"ora_inizio"]];
                }
            self.data_inizio = [f dateFromString:date_string];
        }
        
        /*
        if(self.type==4)
        {
        NSLog(@"self.data_inizio = %@",self.data_inizio);
            
        }
         */
     }
    return self;
}

//=========================
// ISTANCE METHOD
//=========================
#pragma mark - ISTANCE METHOD
#pragma mark -
//ordinare lista oggetti secondo un attributo
- (NSComparisonResult)compare:(NotificaObject *)otherObject {
    return self.type <= otherObject.type;
}

- (NSMutableDictionary*)dictionary_userInfo{
    NSMutableDictionary * d = [NSMutableDictionary new];
    [d setObject:[NSNumber numberWithInt:self.type] forKey:@"type"];
    [d setObject:self.titolo forKey:@"titolo"];
    [d setObject:[NSNumber numberWithBool:self.letto] forKey:@"letto"];
    [d setObject:[NSNumber numberWithInt:self.id_notifica] forKey:@"id"];
    return d;
}

-(BOOL)save{
    NSMutableArray * lista = [NotificaObject getNotifiche];
    BOOL trovato = NO;
    for(int i=0; i<lista.count ;i++)
    {
        NotificaObject *row = [lista objectAtIndex:i];
        if(row.id_notifica == self.id_notifica && !trovato)
        {
        [lista replaceObjectAtIndex:i withObject:self];
        trovato = YES;
        }
    }
    NSData *d = [NSKeyedArchiver archivedDataWithRootObject:lista];

    return [d writeToFile:[[NotificaObject cacheDir] stringByAppendingPathComponent:PATH_NOT] atomically:YES];
}

-(void)deleteNotifica{
    NSLog(@"deleteNotifica:%@->%@",self,[NotificaObject getNotifiche]);
    NSMutableArray *listaDaCancellare = [NSMutableArray arrayWithArray:[NotificaObject getNotifiche]];
    NSMutableArray *nuovaLista = [NSMutableArray new];
    for(NotificaObject *row in listaDaCancellare)
    {
        NSLog(@"ROW:%i ->self:%i",row.id_notifica,self.id_notifica);
        if (row.id_notifica != self.id_notifica)
            {
                [nuovaLista addObject:row];
                NSLog(@"nuovaLista:%@",nuovaLista);
                
            }
    }    
    //salvo nuova lista
    NSData *d = [NSKeyedArchiver archivedDataWithRootObject:nuovaLista];
    [d writeToFile:[[NotificaObject cacheDir] stringByAppendingPathComponent:PATH_NOT] atomically:YES];
    NSLog(@"dopo delete\n%@",[NotificaObject getNotifiche]);
}

+(int)getMaxID{
        NSMutableArray * lista = [NotificaObject getNotifiche];
        int max = 0;
        for(NotificaObject *row in lista)
            if(row.id_notifica > max) max = row.id_notifica;
        return max;
}


-(void)setLetto:(BOOL)l{
    NSLog(@"setLetto:%i",l);
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setBool:l forKey:[NSString stringWithFormat:@"istant_%i",self.id_notifica]];
    [def synchronize];
}

-(BOOL)letto{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    return [def boolForKey:[NSString stringWithFormat:@"istant_%i",self.id_notifica]];
}

+(NSMutableArray*)getCurrentNotifiche{
    NSMutableArray *a = [[NSUserDefaults standardUserDefaults] objectForKey:NOTIFICHE_INCODA]?[[NSUserDefaults standardUserDefaults] objectForKey:NOTIFICHE_INCODA]:[NSMutableArray new];
    return a;
}

+(void)resetCurrentNotifiche{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NOTIFICHE_INCODA];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL)addCurrentNotifica:(int)i{
    NSMutableArray *a = [[NSUserDefaults standardUserDefaults] objectForKey:NOTIFICHE_INCODA]?[[NSUserDefaults standardUserDefaults] objectForKey:NOTIFICHE_INCODA]:[NSMutableArray new];
    [a addObject:[NSNumber numberWithInt:i]];
    
    [[NSUserDefaults standardUserDefaults] setObject:a forKey:NOTIFICHE_INCODA];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return YES;
}

#pragma mark OPERAZIONI SU LISTA NOTIFICHE
//==============================
// OPERAZIONI SU LISTA NOTIFICHE
//==============================
+(NSMutableArray*)getNotifiche
{
    NSMutableArray * lista = [NSMutableArray new];
    NSString *path = [[NotificaObject cacheDir] stringByAppendingPathComponent:PATH_NOT];
    if(![NotificaObject fileExistsAtPath:path])
    {
        NSLog(@"ERR: LISTA NOTIFICHE non esiste");
        return lista;
    }
    
    NSData *d = [[NSData alloc] initWithContentsOfFile:path];
    lista = [NSKeyedUnarchiver unarchiveObjectWithData:d];

    //NSLog(@"LISTA NOTIFICHE:\n%@\n\n",lista);
    return lista;
}

//NOTIFICA CON ID
+(NotificaObject*)notificaConId:(NSNumber*)id_notifica{
    NSMutableArray * lista = [NotificaObject getNotifiche];
    for(NotificaObject *row in lista)
        if(row.id_notifica == [id_notifica intValue]) return row;
    return nil;
}

+(void)controllaNotificheDuplicate{
    NSLog(@"controllaNotificheDuplicate%@",[NotificaObject getNotifiche]);
    NSArray * notifiche = [NSArray arrayWithArray:[NotificaObject getNotifiche]];
// controllo aggiuntivo Notifiche Duplicate
    NSMutableArray *itemsWithUniqueElements = [NSMutableArray arrayWithCapacity:[notifiche count]];
    NSMutableDictionary *id_list = [NSMutableDictionary new];
    
    for (NotificaObject *a in notifiche) {
        if(![id_list objectForKey:[NSNumber numberWithInt:a.id_notifica]])
        {
            [itemsWithUniqueElements addObject:a];
            [id_list setObject:[NSNumber numberWithBool:YES] forKey:[NSNumber numberWithInt:a.id_notifica]];
        }
    }
    
    if(itemsWithUniqueElements.count < notifiche.count){
        
        NSData *d = [NSKeyedArchiver archivedDataWithRootObject:itemsWithUniqueElements];
        [d writeToFile:[[NotificaObject cacheDir] stringByAppendingPathComponent:PATH_NOT] atomically:YES];
    }
    NSLog(@"controllaNotificheDuplicateFINE:%@",[NotificaObject getNotifiche]);
}


//NOTIFICA PER HOME
+(NSArray*)getNotifichePerHome
{
    NSMutableArray * lista = [NSMutableArray new];
    NSArray * notifiche = [NSArray arrayWithArray:[NotificaObject getNotifiche]];
    for(NotificaObject *row in notifiche)
        if(row.sempre_presente_in_home) [lista addObject:row];
    NSLog(@"getNotifichePerHome:%@",lista);
    return (lista.count>0)?[NSArray arrayWithArray:lista]:[NSArray new];
}


//===================
// RICORRENTI
//===================
+(NSMutableArray*)getNotificheRicorrenti{
    NSMutableArray * lista = [NSMutableArray new];
    NSArray * notifiche = [NSArray arrayWithArray:[NotificaObject getNotifiche]];
    for(NotificaObject *row in notifiche)
        if(row.type != 4 && row.type != 6 && row.type != 5)  [lista addObject:row];
    return lista;
}

+(void)deleteNotificheRicorrenti{
    NSMutableArray * lista = [NSMutableArray new];
    NSArray * notifiche = [NSArray arrayWithArray:[NotificaObject getNotifiche]];
    for(NotificaObject *row in notifiche)
        if(row.type == 4)  [lista addObject:row];
    
    NSData *d = [NSKeyedArchiver archivedDataWithRootObject:lista];
    [d writeToFile:[[NotificaObject cacheDir] stringByAppendingPathComponent:PATH_NOT] atomically:YES];
    NSLog(@"LISTA NOTIFICHE:\n%@\n\n",lista);

}

//===================
// ISTANTANEE
//===================
+(NSMutableArray*)getNotificheIstantanee{
    NSMutableArray * lista = [NSMutableArray new];
    NSArray * notifiche = [NSArray arrayWithArray:[NotificaObject getNotifiche]];
    for(NotificaObject *row in notifiche)
        if(row.type == 4)  [lista addObject:row];
    return lista;
}

+(int)getIstant_LastID{
    int last_id = -1;
    for(NotificaObject *item in [NotificaObject getNotificheIstantanee])
    {
        if(item.id_notifica > last_id) last_id = item.id_notifica;
    }
    return last_id;
}

+(void)deleteNotificheIstantanee{
    NSMutableArray * lista = [NSMutableArray new];
    NSArray * notifiche = [NSArray arrayWithArray:[NotificaObject getNotifiche]];
    for(NotificaObject *row in notifiche)
        if(row.type != 4)  [lista addObject:row];
    
    NSData *d = [NSKeyedArchiver archivedDataWithRootObject:lista];
    [d writeToFile:[[NotificaObject cacheDir] stringByAppendingPathComponent:PATH_NOT] atomically:YES];
    NSLog(@"LISTA NOTIFICHE:\n%@\n\n",lista);

}

#pragma mark LASTUPDATE
//=========================
// LASTUPDATE
//=========================
+(void)checkNotifiche{
    NSLog(@"checkNotifiche");
    if(![GlobalFunction checkNetworkStatus]) NSLog(@"noNet");
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    //DOWNLOAD
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&lang=%@",URL_NOTIFICHE,NSLocalizedString(@"checklingua", nil)]];
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_REQUEST];
    NSError *err = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err];
    if(err!=nil)
    {
        NSLog(@"checkNotifiche: ERR lastUpdate: %@",[err description]);
        return;
    }
    else{
        NSMutableDictionary *risposta = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err]];
        NSLog(@"checkNotifiche:[RISP]->%@ \nurl:%@",risposta,url);
        //NOTIFICHE_LAST_UPDATE_RICORRENZA NOTIFICHE_LAST_ID_ISTANT
        if(err)
        {
            NSLog(@"checkNotifiche: Errore parsing Json lastupdate");
            return;
        }
        
        NSLog(@"lista_notificaJSON:%@",[risposta objectForKey:@"lista_notifiche"]);
        
        NSLog(@"NOTIFICHE_LAST_UPDATE_RICORRENZA:%@\nRISPOSTA:%@",[def objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA], [risposta objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA]);

        if(![def objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA] || ![[def objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA] isEqualToString:[risposta objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA]])
        {
            //CancelloListaNotificheRicorrenti
            [NotificaObject deleteNotificheRicorrenti];
            
            NSMutableArray *notificheAttuali = [NSMutableArray arrayWithArray:[NotificaObject getNotifiche]];
            NSArray *lista_notifiche = [risposta objectForKey:@"lista_notifiche"];

            for (int i=0; i<lista_notifiche.count; i++) {
                NSDictionary *row_json = [lista_notifiche objectAtIndex:i];
                if([[row_json objectForKey:@"type"] intValue] != 4 && [[row_json objectForKey:@"type"] intValue] != 6 && [[row_json objectForKey:@"type"] intValue] != 5 && [row_json objectForKey:@"contenuto_localizzato"]!=nil)
                    {
                        NotificaObject *row = [[NotificaObject alloc] initWithDictionary:row_json];
                        [notificheAttuali addObject:row];
                    }
            }
            
            NSLog(@"Saving Notifiche Ricorrenti List...%@",notificheAttuali);
            
            NSData *d = [NSKeyedArchiver archivedDataWithRootObject:notificheAttuali];
            if(![[NSFileManager defaultManager] fileExistsAtPath:[[NotificaObject cacheDir] stringByAppendingPathComponent:@"/DATA"]])
                [[NSFileManager defaultManager] createDirectoryAtPath:[[NotificaObject cacheDir] stringByAppendingPathComponent:@"/DATA"] withIntermediateDirectories:YES attributes:nil error:nil];
            [d writeToFile:[[NotificaObject cacheDir] stringByAppendingPathComponent:PATH_NOT] atomically:YES];
            [def setObject:[risposta objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA] forKey:NOTIFICHE_LAST_UPDATE_RICORRENZA];
            [def synchronize];

            //INIZIALIZZO NOTIFICHE
            [NotificaObject initNotificDate];
            
            UIApplication* app = [UIApplication sharedApplication];
            [app cancelAllLocalNotifications];
            
         }
        
        int last_id = [NotificaObject getIstant_LastID];
        NSLog(@"NOTIFICHE INSTANTANEE CHECK:%i\nRISPOSTA:%i",last_id, [[risposta objectForKey:NOTIFICHE_LAST_ID_ISTANT] intValue]);
        
        
        ///NOTIFICHE INSTANTANEE CHECK
        if([[risposta objectForKey:NOTIFICHE_LAST_ID_ISTANT] intValue] != last_id)
        {
            //CancelloListaNotificheRicorrenti
            [NotificaObject deleteNotificheIstantanee];
            NSMutableArray *notificheAttuali = [NSMutableArray arrayWithArray:[NotificaObject getNotifiche]];
            
            NSArray *lista_notifiche = [risposta objectForKey:@"lista_notifiche"];
            for (int i=0; i<lista_notifiche.count; i++) {
               
                NSDictionary *row_json = [lista_notifiche objectAtIndex:i];
                NSLog(@"lista_notificaCHECK_ISTANT:%@-%@",[row_json objectForKey:@"type"],[row_json objectForKey:@"contenuto_localizzato"]);
                if([[row_json objectForKey:@"type"] intValue] == 4 && [row_json objectForKey:@"contenuto_localizzato"]!=nil)
                {
                    NotificaObject *row = [[NotificaObject alloc] initWithDictionary:row_json];
                    int giorno = (60*60*24);
                    NSTimeInterval secondi = [[NSDate date] timeIntervalSinceDate:row.data_inizio];
                    int giorni_trascorsi = (int)secondi/giorno;
                    
                    NSLog(@"Differenza[DATA e ORA LOCALE] da [%@] a Ora in giorni:%i sec:%f",row.data_inizio, giorni_trascorsi,secondi);
                    //range: 30 minuti prima o 10 ore DOPO
                    if(giorni_trascorsi < 0 || (giorni_trascorsi>=0 && giorni_trascorsi <= row.giorni_frequenza))
                            [notificheAttuali addObject:row];
                    NSLog(@"Aggiungendo Notifica Istant...%@",row);
                }
            }
            
            

  
            NSArray *sortedArray  = [notificheAttuali sortedArrayUsingSelector:@selector(compare:)];
            NSData *d = [NSKeyedArchiver archivedDataWithRootObject:[NSMutableArray arrayWithArray:sortedArray]];
            NSLog(@"Saving Notifiche con Istantanee List...%@",notificheAttuali);

            
            
            
            [d writeToFile:[[NotificaObject cacheDir] stringByAppendingPathComponent:PATH_NOT] atomically:YES];
            

            
            //INIZIALIZZO NOTIFICHE
            UIApplication* app = [UIApplication sharedApplication];
            [app cancelAllLocalNotifications];
            
          }
    }
    
    [NotificaObject controllaNotificheDuplicate];
    NSLog(@"Notifiche DOPO CHECK:%@",[NotificaObject getNotifiche]);

    Home *h = [Home sharedInstance];
    //Nascondo Icona se non ci sono notifiche x home
    [h.notifica_btn setHidden:([[NotificaObject getNotifichePerHome] count] <= 0)];
    
}



//=========================
// CLASS METHOD
//=========================
#pragma mark - CLASS METHOD
//CACHE DIRECTORY
+ (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    //NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}

+(NSUserDefaults*)userDefault{
    return [NSUserDefaults standardUserDefaults];
}


+(BOOL)fileExistsAtPath:(NSString*)path{
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}


//=========================
// BADGE NUMBER
//=========================
#pragma mark - BADGE NUMBER
+(void)incrementOneBadge{
    NSInteger numberOfBadges = [UIApplication sharedApplication].applicationIconBadgeNumber;
    numberOfBadges +=1;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:numberOfBadges];
}

+(void)decrementOneBadge{
    NSInteger numberOfBadges = [UIApplication sharedApplication].applicationIconBadgeNumber;
    numberOfBadges -=1;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:numberOfBadges];
}

+(void)initNotificDate{
    NSLog(@"initNotificDate");
    if(![[NotificaObject userDefault] objectForKey:NOTIFICA_DONAZIONI])
            [[NotificaObject userDefault] setObject:[NSDate date] forKey:NOTIFICA_DONAZIONI];
    if(![[NotificaObject userDefault] objectForKey:NOTIFICA_NEWSLETTER])
            [[NotificaObject userDefault] setObject:[NSDate date] forKey:NOTIFICA_NEWSLETTER];
    
    NSArray * a = [NSArray arrayWithArray:[NotificaObject getNotifiche]];
    NSMutableDictionary *d = [[NotificaObject userDefault] objectForKey:NOTIFICA_LOOP_CUSTOM_DICT]?[[NotificaObject userDefault] objectForKey:NOTIFICA_LOOP_CUSTOM_DICT]:[NSMutableDictionary new];
    for (NotificaObject *row in a) {
        if(row.type == 3 && ![d objectForKey:[NSString stringWithFormat:@"%i",row.id_notifica]])
        [d setObject:[NSDate date] forKey:[NSString stringWithFormat:@"%i",row.id_notifica]];
    }
    
    [[NotificaObject userDefault] setObject:d forKey:NOTIFICA_LOOP_CUSTOM_DICT];
    [[NotificaObject userDefault] synchronize];
}

//=========================
// NOTIFICHE LOCALI
//=========================
#pragma mark - NOTIFICHE LOCALI
+(void)ScheduleLocalNotification
{
    [self controllaNotificheDuplicate];
    NSLog(@"ScheduleLocalNotification:%@",[NotificaObject getNotifiche]);
    NSMutableArray *lista = [NotificaObject getNotifiche];
    [NotificaObject resetCurrentNotifiche];
    //Resetto Badge
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    for(NotificaObject *notifica in lista)
    {
        NSLog(@"notifica:%@",notifica);
    int giorno = (60*60*24);
        
    switch (notifica.type) {
            
        case 0:
            {
            //NOTIFICA DONAZIONI
            NSDate *notifyDate = [[NotificaObject userDefault] objectForKey:NOTIFICA_DONAZIONI];
            NSTimeInterval secondi = [[NSDate date] timeIntervalSinceDate:notifyDate];
            int giorni_trascorsi = (int)secondi/giorno;
            
            NSLog(@"Differenza[DONAZIONI] da [%@] a Ora in giorni:%i sec:%f",notifyDate, giorni_trascorsi,secondi);
// SCHEDULE NOTIFICHE PER DEBUG DONA
            if(giorni_trascorsi >= notifica.giorni_frequenza)
            {
                [[NotificaObject userDefault] setObject:[NSDate date] forKey:NOTIFICA_DONAZIONI];
                [[NotificaObject userDefault] synchronize];
                [NotificaObject creaNotificaDONAZIONI:notifica];
            }

            break;
            }
        case 1:
             {
             //NOTIFICA NEWSLETTER
             NSDate *notifyDate = [[NotificaObject userDefault] objectForKey:NOTIFICA_NEWSLETTER];
             NSTimeInterval secondi = [[NSDate date] timeIntervalSinceDate:notifyDate];
             int giorni_trascorsi = (int)secondi/giorno;
                 
             NSLog(@"Differenza[NEWSLETTER] da [%@] a Ora in giorni:%i sec:%f",notifyDate, giorni_trascorsi,secondi);
// SCHEDULE NOTIFICHE PER DEBUG NEWSLETTER
            if(giorni_trascorsi >= notifica.giorni_frequenza)
                 {
                     [[NotificaObject userDefault] setObject:[NSDate date] forKey:NOTIFICA_NEWSLETTER];
                     [[NotificaObject userDefault] synchronize];
                     [NotificaObject creaNotificaNEWS_LETTER:notifica];
                 }

             break;
             }
        case 2:
             {
             //TEMPO TRASCORSO SENZA UTILIZZARE APP
            [NotificaObject creaNotificaNONUSATA:notifica];
             break;
             }
             case 3:
             {
             //NOTIFICA LOOP
                 NSDate *notifyDate = [[[NotificaObject userDefault] objectForKey:NOTIFICA_LOOP_CUSTOM_DICT] objectForKey:[NSString stringWithFormat:@"%i",notifica.id_notifica]];
                 NSTimeInterval secondi = [[NSDate date] timeIntervalSinceDate:notifyDate];
                 int giorni_trascorsi = (int)secondi/giorno;
                 
                 NSLog(@"Differenza[LOOP-CUSTOM] da [%@] a Ora in giorni:%i sec:%f",notifyDate, giorni_trascorsi,secondi);
                 
                 if(giorni_trascorsi >= notifica.giorni_frequenza)
                 {
                     NSLog(@"giorni_trascorsi >= notifica.giorni_frequenza");
                     [[NotificaObject userDefault] setObject:[NSDate date] forKey:NOTIFICA_LOOP_CUSTOM];
                     [[NotificaObject userDefault] synchronize];
                     NSMutableDictionary *d = [[NotificaObject userDefault] objectForKey:NOTIFICA_LOOP_CUSTOM_DICT]?[[NotificaObject userDefault] objectForKey:NOTIFICA_LOOP_CUSTOM_DICT]:[NSMutableDictionary new];
                     [d setObject:[NSDate date] forKey:[NSString stringWithFormat:@"%i",notifica.id_notifica]];
                     [[NSUserDefaults standardUserDefaults] setObject:d forKey:NOTIFICA_LOOP_CUSTOM_DICT];
                     [[NSUserDefaults standardUserDefaults] synchronize];

                     [NotificaObject creaNotificaLOOP:notifica];
                 }

             break;
             }
             case 4:
             {
             //NOTIFICA DATA e ORA LOCALE
                 NSTimeInterval secondi = [[NSDate date] timeIntervalSinceDate:notifica.data_inizio];
                 int giorni_trascorsi = (int)secondi/giorno;
                 
                 NSLog(@"Differenza[DATA e ORA LOCALE] da [%@] a Ora in giorni:%i sec:%f",notifica.data_inizio, giorni_trascorsi,secondi);
                 //range: 30 minuti prima o 10 ore DOPO
                 if(!notifica.letto && giorni_trascorsi >= 0 && giorni_trascorsi <= notifica.giorni_frequenza)

                 {
                     NSLog(@"Crea NOTIFICA type4!!!");
                     [NotificaObject creaNotificaDATAeORA:notifica];
                 }
                 
                 else if(notifica.letto || giorni_trascorsi > notifica.giorni_frequenza)
                     [notifica deleteNotifica];


            
            break;
             }
        case 5:
        {
            //NOTIFICA ISTANTANEA range in giorni frequenza
            NSLog(@"Nothing");
            break;
        }
        case 6:
        {
            //FORZA AGGIORNAMENTO creare notifica ad HOC
            NSLog(@"NON E' NOTIFICA: %@",notifica);
            //[Fix forzaAggiornamentoTotale];
            break;
        }
        case 7:
        {
            //NOTIFICA SOLO PER HOME
            NSLog(@"NOTIFICA %@ SOLO PER HOME",notifica);
            break;
        }

        default:
            break;
    }
    
    }
}

+(void)creaNotificaDONAZIONI:(NotificaObject*)notifica{
    if(!notifica.attivo) return;
    
    [NotificaObject addCurrentNotifica:notifica.id_notifica];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    //[localNotification setTimeZone:[NSTimeZone localTimeZone]];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [localNotification setAlertBody:notifica.titolo];
    [localNotification setSoundName:UILocalNotificationDefaultSoundName];
    [localNotification setFireDate:[[NSDate date] dateByAddingTimeInterval:(arc4random()%15)+40]];
    [localNotification setUserInfo:[NSDictionary dictionaryWithDictionary:[notifica dictionary_userInfo]]];
    [localNotification setApplicationIconBadgeNumber:1];
    [localNotification setRepeatInterval:0];
    [localNotification setAlertAction:NSLocalizedString(@"View Details", nil)];
    NSLog(@"creaNotificaDONAZIONI:%@",localNotification);
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

+(void)creaNotificaNEWS_LETTER:(NotificaObject*)notifica{
    if(!notifica.attivo) return;
    
    [NotificaObject addCurrentNotifica:notifica.id_notifica];

    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    //[localNotification setTimeZone:[NSTimeZone localTimeZone]];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [localNotification setAlertBody:notifica.titolo];
    [localNotification setSoundName:UILocalNotificationDefaultSoundName];
    [localNotification setFireDate:[[NSDate date] dateByAddingTimeInterval:(arc4random()%20)+45]];
    [localNotification setUserInfo:[NSDictionary dictionaryWithDictionary:[notifica dictionary_userInfo]]];
    [localNotification setApplicationIconBadgeNumber:1];
    [localNotification setRepeatInterval:0];
    [localNotification setAlertAction:NSLocalizedString(@"View Details", nil)];
    NSLog(@"creaNotificaNEWS_LETTER:%@",localNotification);
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

+(void)creaNotificaNONUSATA:(NotificaObject*)notifica{
    
    [NotificaObject addCurrentNotifica:notifica.id_notifica];

    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    //[localNotification setTimeZone:[NSTimeZone localTimeZone]];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [localNotification setAlertBody:notifica.titolo];
    [localNotification setSoundName:UILocalNotificationDefaultSoundName];
    [localNotification setFireDate:[[NSDate date] dateByAddingTimeInterval:(60*60*24)*notifica.giorni_frequenza]];
    [localNotification setUserInfo:[NSDictionary dictionaryWithDictionary:[notifica dictionary_userInfo]]];
    NSLog(@"creaNotificaNONUSATA:%@",localNotification);
    [localNotification setApplicationIconBadgeNumber:1];
    [localNotification setRepeatInterval:0];
    [localNotification setAlertAction:NSLocalizedString(@"View Details", nil)];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

+(void)creaNotificaLOOP:(NotificaObject*)notifica{
    if(!notifica.attivo) return;
    
    [NotificaObject addCurrentNotifica:notifica.id_notifica];

    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    //[localNotification setTimeZone:[NSTimeZone localTimeZone]];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [localNotification setAlertBody:notifica.titolo];
    [localNotification setRepeatInterval:0];
    //[localNotification setTimeZone:[NSTimeZone defaultTimeZone]];
    [localNotification setSoundName:UILocalNotificationDefaultSoundName];
    [localNotification setFireDate:[[NSDate date] dateByAddingTimeInterval:(arc4random()%10)+60]];
    [localNotification setUserInfo:[NSDictionary dictionaryWithDictionary:[notifica dictionary_userInfo]]];
    [localNotification setApplicationIconBadgeNumber:1];
    [localNotification setAlertAction:NSLocalizedString(@"View Details", nil)];
    NSLog(@"creaNotificaLOOP:%@",localNotification);
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

+(void)creaNotificaISTANTANEA:(NotificaObject*)notifica{
    if(notifica.letto) return;
    
    [NotificaObject addCurrentNotifica:notifica.id_notifica];

    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    //[localNotification setTimeZone:[NSTimeZone localTimeZone]];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [localNotification setAlertBody:notifica.titolo];
    [localNotification setSoundName:UILocalNotificationDefaultSoundName];
    [localNotification setFireDate:[[NSDate date] dateByAddingTimeInterval:5]];
    [localNotification setUserInfo:[NSDictionary dictionaryWithDictionary:[notifica dictionary_userInfo]]];
    NSLog(@"creaNotificaISTANTANEA:%@",localNotification);
    [localNotification setApplicationIconBadgeNumber:1];
    [localNotification setAlertAction:NSLocalizedString(@"View Details", nil)];

    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

+(void)creaNotificaDATAeORA:(NotificaObject*)notifica{
    if(notifica.letto) return;
    
    [NotificaObject addCurrentNotifica:notifica.id_notifica];

    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    //[localNotification setTimeZone:[NSTimeZone localTimeZone]];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [localNotification setAlertBody:notifica.titolo];
    [localNotification setSoundName:UILocalNotificationDefaultSoundName];
    [localNotification setFireDate:[[NSDate date] dateByAddingTimeInterval:61]];
    [localNotification setUserInfo:[NSDictionary dictionaryWithDictionary:[notifica dictionary_userInfo]]];
    NSLog(@"creaNotificaDATAeORA:%@",localNotification);
    [localNotification setApplicationIconBadgeNumber:1];
    [localNotification setAlertAction:NSLocalizedString(@"View Details", nil)];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}


+(void)resettoRemoteNotifichePush{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }


+(void)controlloLocalNotifichePush:(UILocalNotification *)notification
{
    [NotificaObject controlloLocalNotifichePushWithID:[NSNumber numberWithInt:[[notification.userInfo objectForKey:@"id"] intValue]]];
}

+(void)controlloLocalNotifichePushWithID:(NSNumber *)number_id{
    NotificaObject *notifica_avvenuta = [NotificaObject notificaConId:number_id];
    NSLog(@"controlloLocalNotifichePushType:%i",notifica_avvenuta.type);
    
    if(notifica_avvenuta.type == 4 || notifica_avvenuta.type == 5)
    {
        notifica_avvenuta.letto = YES;
        [notifica_avvenuta save];
        NSLog(@"Notifiche:%@",[NotificaObject getNotifiche]);
        
    }

    switch (notifica_avvenuta.type) {
        case 0:
             {
             //NOTIFICA DONAZIONI
             NSLog(@"Fine notifica DONAZIONI");
             break;
             }
        case 1:
             {
             //NOTIFICA NEWSLETTER
             NSLog(@"Fine notifica NEWSLETTER");
             break;
             }
        case 2:
             {
             //TEMPO TRASCORSO SENZA USARE APP
             NSLog(@"Fine notifica TEMPO TRASCORSO SENZA USARE APP");
             break;
             
             }
        case 3:
             {
             //NOTIFICA LOOP CUSTOM
             NSLog(@"Fine notifica NOTIFICA LOOP CUSTOM");
             break;
             }
        case 4:
             {
             //NOTIFICA DATA e ORA
             NSLog(@"Fine notifica NOTIFICA DATA e ORA");
             break;
             }
        case 5:
        {
            //NOTIFICA ISTANTANEA
            NSLog(@"Fine notifica NOTIFICA DATA e ORA");
            break;
        }
        default:
            return;
            break;
    }
    
}

//=========================
// NOTIFICHE DELEGATE
//=========================
#pragma mark - push notification delegate
//=REMOTE
+ (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"NOTiFICAobj:didReceiveRemoteNotification");
    
    [NotificaObject resettoRemoteNotifichePush];
}

+(void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
    NSString* deviceTokenString = [[NSString alloc]
                                   initWithString: [[[[deviceToken description]
                                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                                    stringByReplacingOccurrencesOfString: @" " withString: @""]];
    [def setObject:deviceTokenString forKey:TOKEN];
    [def synchronize];
    NSLog(@"NOTiFICAobj:My token is: %@", deviceTokenString);
    [NotificaObject postToken];
    
}

//=LOCAL
+ (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    
    
    NSLog(@"didReceiveLocalNotification!!");
    //AudioServicesPlaySystemSound(1002);
    //[Home mostraNotifca:notification];
}


+(void)controllanotificheincoda{
    /*
    NSLog(@"controllanotificheincoda:%@",currentNotifications);
    for (UILocalNotification *loc in currentNotifications) {
        NSLog(@"loc.fireDate:%@ date:%@ compare:%li",loc.fireDate,[NSDate date] ,[loc.fireDate compare:[NSDate date]]);
        if([loc.fireDate compare:[NSDate date]] > 0)
        {
            [Home mostraNotifca:loc];
            AudioServicesPlaySystemSound(1002);
            
        }
    }
     */
}

#pragma mark -
#pragma mark POST TOKEN
//=========================
// POST TOKEN
//=========================
+(void)postToken
{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    if([def boolForKey:TOKEN_INVIATO]) return;
    if(![GlobalFunction checkNetworkStatus])
    {
        NSLog(@"NOTiFICAobj:ERR postToken no net");
        return;
    }
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&token=%@&lingua=%@",URL_POST_TOKEN,[def objectForKey:TOKEN],NSLocalizedString(@"checklingua", nil)]];
    NSMutableURLRequest *richiesta = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20];
    NSError* err = nil;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:richiesta returningResponse:nil error:&err];
    if(err != nil)//controllo errore da sendSynchronousRequest
    {
        NSLog(@"NOTiFICAobj:ERR da postToken: %@", [err description]);
    }
    
    NSDictionary *risposta = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data
                                                                                                      options:NSJSONReadingMutableContainers
                                                                                                        error:&err]];
    if(err != nil)//controllo errore da JSON
    {
        NSLog(@"NOTiFICAobj:ERR da postToken: %@", [err description]);
    }
    
    if([[risposta objectForKey:@"risposta" ] isEqualToString: @"OK"])//controllo risposta web server
    {
        [def setBool:YES forKey:TOKEN_INVIATO];
        [def synchronize];
    }
    
}



//=======================================
//Encoding (FOR SAVING OBJECT)
//=======================================
#pragma mark - ENCODING
-(void)encodeWithCoder:(NSCoder*)decoder{

    [decoder encodeInt:self.id_notifica forKey:@"id_notifica"];
    [decoder encodeObject:self.titolo forKey:@"titolo"];
    [decoder encodeObject:self.lingua forKey:@"lingua"];
    [decoder encodeObject:self.contenuto forKey:@"contenuto"];
    [decoder encodeInt:self.type forKey:@"type"];
    [decoder encodeObject:self.link forKey:@"link"];
    [decoder encodeObject:self.link_titolo forKey:@"link_titolo"];
    [decoder encodeObject:self.data_inizio forKey:@"data_inizio"];
    [decoder encodeObject:self.fired forKey:@"fired"];
    [decoder encodeInt:self.giorni_frequenza forKey:@"giorni_frequenza"];
    [decoder encodeBool:self.soloUnaVolta forKey:@"soloUnaVolta"];
    [decoder encodeBool:self.attivo forKey:@"attivo"];
    //[decoder encodeBool:self.letto forKey:@"letto"];
    [decoder encodeBool:self.sempre_presente_in_home forKey:@"sempre_presente_in_home"];

}


//======================================
//Decoding (FOR READ SAVED OBJECT)
//======================================
#pragma mark - DECODING
- (id)initWithCoder: (NSCoder *)aCoder {
    self = [super init];
    if (self) {
        
        self.id_notifica = [aCoder decodeIntForKey:@"id_notifica"];
        self.titolo = [aCoder decodeObjectForKey:@"titolo"];
        self.lingua = [aCoder decodeObjectForKey:@"lingua"];
        self.contenuto = [aCoder decodeObjectForKey:@"contenuto"];
        self.type = [aCoder decodeIntForKey:@"type"];
        self.link = [aCoder decodeObjectForKey:@"link"];
        self.link_titolo = [aCoder decodeObjectForKey:@"link_titolo"];
        self.data_inizio = [aCoder decodeObjectForKey:@"data_inizio"];
        self.fired = [aCoder decodeObjectForKey:@"fired"];
        self.giorni_frequenza = [aCoder decodeIntForKey:@"giorni_frequenza"];
        self.soloUnaVolta = [aCoder decodeBoolForKey:@"soloUnaVolta"];
        self.attivo = [aCoder decodeBoolForKey:@"attivo"];
        //self.letto = [aCoder decodeBoolForKey:@"letto"];
        self.sempre_presente_in_home = [aCoder decodeBoolForKey:@"sempre_presente_in_home"];
        }
    
    return self;
}

//==================
//NSLog %@
//==================
#pragma mark - NSLog
-(NSString*)description{
    return [NSString stringWithFormat:@"<notifica[%i] type:%i inizio:%@ giorni_frequenza:%i letta:%i >",self.id_notifica,self.type,self.data_inizio,self.giorni_frequenza,self.letto];
}




//=================
//END
//=================
@end
