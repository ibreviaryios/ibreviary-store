//
//  BreviarioObject.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "Item.h"

@interface BreviarioObject :  NSObject<NSCoding>{
    
}

//BREVIARIO
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subtitle;
@property (strong, nonatomic) NSString *type;
@property (nonatomic) BOOL complete;
@property (nonatomic) BOOL obbligatorio;
@property (nonatomic) int id_santo;
@property (nonatomic) NSString *id_event;


@property (strong, nonatomic) Item *first_vespers;
@property (strong, nonatomic) Item *officeofreading;
@property (strong, nonatomic) Item *lauds;
@property (strong, nonatomic) Item *daytimeprayers;
@property (strong, nonatomic) Item *vespers;
@property (strong, nonatomic) Item *compline;


//INIT
-(id)initWithDictionary:(NSDictionary*)breviary_json;

// ISTANCE METHOD


// CLASS METHOD


@end
