//
//  SettingOnCloud.h
//  iBreviary
//
//  Created by Leonardo Parenti on 07/05/15.
//  Copyright (c) 2015 leyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface SettingOnCloud : NSManagedObject

+(NSSet*)getSetIdNotificheLette;
+(BOOL)isDebug;

+(SettingOnCloud*)getSettings;


+(void)updateSet:(NSSet*)changeSet;
+(void)changeDebug:(BOOL)d;


+(void)commitChanges;

@end
