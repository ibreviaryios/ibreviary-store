//
//  Nota.h
//  iBreviary
//
//  Created by Leonardo Parenti on 15/03/15.
//  Copyright (c) 2015 leyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item;

@interface ContentViewForScroller : UIView

@property(nonatomic,strong) UIView *contentView;
@property(nonatomic,strong) UILabel *label;
@property(nonatomic,strong) UIImageView *quote;


-(void)incrementFontSize:(NSInteger)newFontSize forScrollView:(UIScrollView*)scroller;

@end

@interface Nota : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * html_text;
@property (nonatomic, retain) NSDate * timestamp;
@property (nonatomic, retain) NSDate * creation_date;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * item_sender_text;
@property (nonatomic, readonly) BOOL hasSourceText;

//SHARE
@property (nonatomic, readonly) UIImage *cartolina;
-(NSURL*)getImageUrl;
-(BOOL)removeImageWithError:(NSError**)err;

-(BOOL)saveInContext:(NSManagedObjectContext*)context;

-(NSString*)dateString;
-(NSString *)htmlPdfString;
-(NSString*)source_Htmlstring;

+(Nota*)addNotaFromCopyAction:(NSString*)clip_string andItem:(Item*)i forContext:(NSManagedObjectContext*)context;

+(NSMutableArray*)getNoteInContext:(NSManagedObjectContext*)context;
// NOTA CON TITLE
+(Nota*)getNotaWithTitle:(NSString*)tit inContext:(NSManagedObjectContext*)context;

//DELETE PROTOCOL
+(BOOL)deleteNota:(Nota*)n andError:(NSError**)e forContext:(NSManagedObjectContext*)context;
+(void)deleteAll;

-(ContentViewForScroller*)fullContentForScrollView:(UIScrollView*)scroll;

+(NSString*)htmlPdfStringFromArray:(NSArray*)note_list;

@end
