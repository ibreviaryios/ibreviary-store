//
//  NormalizeMesseModel2.m
//  iBreviary
//
//  Created by Leonardo Parenti on 01/09/15.
//  Copyright (c) 2015 leyo. All rights reserved.
//

#import "NormalizeMesseModel2.h"

@implementation NormalizeMesseModel2

// called once, before the start of the migration
-(BOOL)beginEntityMapping:(NSEntityMapping *)mapping
                  manager:(NSMigrationManager *)manager
                    error:(NSError *__autoreleasing *)error
{
    // create a dictionary to store State entities
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
    
    // give the dictionary to the migration manager
    [manager setUserInfo:userInfo];
    
    return YES;
}

-(BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance
                                     entityMapping:(NSEntityMapping *)mapping
                                           manager:(NSMigrationManager *)manager
                                             error:(NSError *__autoreleasing *)error
{
    BOOL created = [super createDestinationInstancesForSourceInstance:sInstance
                                                        entityMapping:mapping
                                                              manager:manager
                                                                error:error];
    
    NSLog(@"ERROR Merging:%@",*error);
    
    return created;

    /*
    // destination managed object context and entity name
    NSManagedObjectContext *destinationManagedObjectContext = [manager destinationContext];
    NSString *destinationEntityName = [mapping destinationEntityName];
    
    // create the Address entity in the destination model
    NSManagedObject *dInstance = [NSEntityDescription insertNewObjectForEntityForName:destinationEntityName
                                    inManagedObjectContext:destinationManagedObjectContext];
    
    // set the non-normalized attributes
    [dInstance setValue:[sInstance valueForKey:@"street"] forKeyPath:@"street"];
    [dInstance setValue:[sInstance valueForKey:@"city"] forKeyPath:@"city"];
    
    // lookup its state
    NSString *stateName = [sInstance valueForKey:@"state"];
    NSMutableDictionary *states = [manager userInfo][@"states"];
    NSManagedObject *state = states[stateName];
    
    // create and store a new State entity if not found
    if (!state) {
        state
        = [NSEntityDescription insertNewObjectForEntityForName:@"State"
                                        inManagedObjectContext:destinationManagedObjectContext];
        [state setValue:stateName forKeyPath:@"name"];
        states[stateName] = state;
    }
    
    // set the State relationship on the Address entity
    [dInstance setValue:state forKeyPath:@"state"];
    
    // associate the source and destination Address entities
    [manager associateSourceInstance:sInstance
             withDestinationInstance:dInstance
                    forEntityMapping:mapping];
    */
    return YES;
}

@end
