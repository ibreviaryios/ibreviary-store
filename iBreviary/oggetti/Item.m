//
//  Item.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import "Item.h"
#import "Home.h"

@implementation Item
/*
  sample:
                {
                "title": string,
                "content": htmlString
                }
 */


//SYNTHESIZE
@synthesize titolo,titoloClass,sottotitolo,time,use,contenuto,titolo_home;



//=================
// INIT
//=================
#pragma mark - INIT

- (id)init{
    if ((self = [super init])){
    }
    return self;
}


- (id)initWithDictionary:(NSDictionary*)d{
    if ((self = [super init])){
        self.titolo = [d objectForKey:@"title"] && ![[d objectForKey:@"title"] isEqualToString:@""]?[d objectForKey:@"title"]:nil;
        self.sottotitolo = [d objectForKey:@"subtitle"] && ![[d objectForKey:@"subtitle"] isEqualToString:@""]?[d objectForKey:@"subtitle"]:nil;
        self.use = [d objectForKey:@"use"]?[d objectForKey:@"use"]:nil;
        self.time = [d objectForKey:@"time"]?[d objectForKey:@"time"]:nil;
        self.contenuto = [[d objectForKey:@"content"] isKindOfClass:[NSString class]] && ![[d objectForKey:@"content"] isEqualToString:@""]?[d objectForKey:@"content"]:nil;
     }
    return self;
}

- (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}

-(NSString*)pdf_book_url{
    return [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"iBreviary_%lu%@.pdf",(unsigned long)[self hash],self.font_type]];
}

-(NSNumber*)font_type{
    if(![[NSUserDefaults standardUserDefaults] objectForKey:PDF_FONT_TYPE]){
        return [NSNumber numberWithInt:MenuType_FontA];
    }
    return [[NSUserDefaults standardUserDefaults] objectForKey:PDF_FONT_TYPE];
}

//=========================
// ISTANCE METHOD
//=========================
#pragma mark - ISTANCE METHOD
-(void)mettiInCodaContenuto:(Item*)i{
    self.contenuto = [NSString stringWithFormat:@"%@%@", self.contenuto, i.contenuto];
}


-(NSString*)defaultWebContentString{
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *header=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/header_0.html",path] encoding:NSUTF8StringEncoding error:nil];
    NSString *footer=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/footer.html",path] encoding:NSUTF8StringEncoding error: nil];
    
    NSString *titleHtml;
    
    if(self.titoloClass!=nil)
    {
        titleHtml =[NSString stringWithFormat:@"<div class='%@'>%@</div>",self.titoloClass , self.titolo];
        
    }
    else titleHtml =[NSString stringWithFormat:@"<div class='sezione'>%@</div>", self.titolo];

    NSString *stringaWebContent = [NSString stringWithFormat:@"%@%@%@%@%@",header,titleHtml,@"",[[NSString stringWithFormat:@"<div class=\"data_SpriritualSender\">%@</div>",[[[Home sharedInstance] day] title]] stringByAppendingString:self.contenuto],footer];
    
    return stringaWebContent;
}


//=========================
// CLASS METHOD
//=========================
#pragma mark - CLASS METHOD
//CACHE DIRECTORY
+ (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}


+(BOOL)fileExistsAtPath:(NSString*)path{
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

+(NSMutableArray*)initListOfItem:(NSArray *)a{
    NSMutableArray *lista = [NSMutableArray new];
    for(NSDictionary *row in a)
    {
        Item *i = [[Item alloc] initWithDictionary:row];
        [lista addObject:i];
    }
    return lista;
}


//=======================================
//Encoding (FOR SAVING OBJECT)
//=======================================
#pragma mark - ENCODING
-(void)encodeWithCoder:(NSCoder*)decoder{
    
    [decoder encodeObject:self.titolo forKey:@"titolo"];
    [decoder encodeObject:self.titoloClass forKey:@"titoloClass"];
    [decoder encodeObject:self.sottotitolo forKey:@"sottotitolo"];
    [decoder encodeObject:self.use forKey:@"use"];
    [decoder encodeObject:self.time forKey:@"time"];
    [decoder encodeObject:self.contenuto forKey:@"contenuto"];
    [decoder encodeObject:self.titolo_home forKey:@"titolo_home"];

}


//======================================
//Decoding (FOR READ SAVED OBJECT)
//======================================
#pragma mark - DECODING
- (id)initWithCoder: (NSCoder *)aCoder {
    self = [super init];
    if (self) {
        self.titolo = [aCoder decodeObjectForKey:@"titolo"];
        self.titoloClass = [aCoder decodeObjectForKey:@"titoloClass"];
        self.sottotitolo = [aCoder decodeObjectForKey:@"sottotitolo"];
        self.use = [aCoder decodeObjectForKey:@"use"];
        self.time = [aCoder decodeObjectForKey:@"time"];
        self.contenuto = [aCoder decodeObjectForKey:@"contenuto"];
        self.titolo_home = [aCoder decodeObjectForKey:@"titolo_home"];
        
        }
    
    return self;
}


//==================
//// NSLog
//==================
#pragma mark - // NSLog
-(NSString*)description{
    return [NSString stringWithFormat:@"<item titolo:%@ contenuto:%i.. >",self.titolo,(int)self.contenuto.length];
}




//=================
//END
//=================
@end
