//
//  SantoObject.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "Item.h"
#import "BreviarioObject.h"
#import "MissalObject.h"

@interface SantoObject : NSObject<NSCoding>{
    
}

//SantoObject
@property (nonatomic) int id_santo;
//Detail
@property (readonly, nonatomic) NSString *titolo;
@property (strong, nonatomic) NSString *url_image;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *subtitle;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *text_info;

//Breviario
@property (strong, nonatomic) BreviarioObject *breviario;

//Missal
@property (strong, nonatomic) MissalObject *missal;


-(NSString *)htmlPdfString;


- (id)initWithDictionary:(NSDictionary*)d andBreviarioMemory:(NSDictionary*)jsonBreviarioMemory andMissalMemory:(NSDictionary*)jsonMissalMemory;

+(UIImage*)scaricaImmagini:(NSString*)url_string;

@end
