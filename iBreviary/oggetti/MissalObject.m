//
//  MissalObject.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import "MissalObject.h"


@implementation MissalObject

//MISSAL
@synthesize title,subtitle,type,obbligatorio,variant;
@synthesize id_event,id_santo;
@synthesize introduction,introductory_rites,eucharistic_liturgy, communion_rites;
@synthesize antiphon_and_opening_prayer, first_reading,second_reading, responsorial_psalm;
@synthesize gospel_acclamation, gospel, prayer_of_the_faithful;
@synthesize prayer_over_the_gifts, preface, antiphon_and_prayer_after_communion,custom;

//=================
// INIT
//=================
#pragma mark - INIT
- (id)init {
    // NSLog(@"MissalObject INIT");
    if ((self = [super init])){
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary*)missal_json{
    if ((self = [super init])){
        // NSLog(@"MissalObject inizialize");
        
        //STRUCTURE
        self.title = [missal_json objectForKey:@"title"]?[missal_json objectForKey:@"title"]:nil;
        //Tipo testo proprio
        self.type = [missal_json objectForKey:@"type"]?[missal_json objectForKey:@"type"]:nil;
        self.subtitle = [missal_json objectForKey:@"subtitle"]?[missal_json objectForKey:@"subtitle"]:nil;
        self.variant = [missal_json objectForKey:@"variant"]?[missal_json objectForKey:@"variant"]:@"";
        self.obbligatorio = [missal_json objectForKey:@"mandatory"]?([[missal_json objectForKey:@"mandatory"] intValue]==1):NO;
        if([missal_json objectForKey:@"saint_id"] == [NSNull null]) self.id_santo = 0;
        else self.id_santo = [missal_json objectForKey:@"saint_id"]?[[missal_json objectForKey:@"saint_id"] intValue]:-1;
        self.id_event = [missal_json objectForKey:@"id_event"]?[missal_json objectForKey:@"id_event"]:nil;
        
        //MISSAL
        NSDictionary *order = [missal_json objectForKey:@"order_of_mass"];
        
        self.introduction = [missal_json objectForKey:@"introduction"]?[[Item alloc] initWithDictionary:[missal_json objectForKey:@"introduction"]]:nil;
        
        self.introductory_rites = [order objectForKey:@"introductory_rites"]?[[Item alloc] initWithDictionary:[order objectForKey:@"introductory_rites"]]:nil;

        self.eucharistic_liturgy = [order objectForKey:@"eucharistic_liturgy"]?[[Item alloc] initWithDictionary:[order objectForKey:@"eucharistic_liturgy"]]:nil;
        
        self.communion_rites = [order objectForKey:@"communion_rites"]?[[Item alloc] initWithDictionary:[order objectForKey:@"communion_rites"]]:nil;
        
        self.antiphon_and_opening_prayer = [missal_json objectForKey:@"antiphon_and_opening_prayer"]?[[Item alloc] initWithDictionary:[missal_json objectForKey:@"antiphon_and_opening_prayer"]]:nil;
        self.first_reading = [missal_json objectForKey:@"first_reading"]?[[Item alloc] initWithDictionary:[missal_json objectForKey:@"first_reading"]]:nil;
        
        self.second_reading = [missal_json objectForKey:@"second_reading"]?[[Item alloc] initWithDictionary:[missal_json objectForKey:@"second_reading"]]:nil;
        self.responsorial_psalm = [missal_json objectForKey:@"responsorial_psalm"]?[[Item alloc] initWithDictionary:[missal_json objectForKey:@"responsorial_psalm"]]:nil;
        self.gospel_acclamation = [missal_json objectForKey:@"gospel_acclamation"]?[[Item alloc] initWithDictionary:[missal_json objectForKey:@"gospel_acclamation"]]:nil;
        self.gospel = [missal_json objectForKey:@"gospel"]?[[Item alloc] initWithDictionary:[missal_json objectForKey:@"gospel"]]:nil;
        self.prayer_of_the_faithful = [missal_json objectForKey:@"prayer_of_the_faithful"]?[[Item alloc] initWithDictionary:[missal_json objectForKey:@"prayer_of_the_faithful"]]:nil;
        self.prayer_over_the_gifts = [missal_json objectForKey:@"prayer_over_the_gifts"]?[[Item alloc] initWithDictionary:[missal_json objectForKey:@"prayer_over_the_gifts"]]:nil;
        self.preface = [missal_json objectForKey:@"preface"]?[[Item alloc] initWithDictionary:[missal_json objectForKey:@"preface"]]:nil;
        self.antiphon_and_prayer_after_communion = [missal_json objectForKey:@"antiphon_and_prayer_after_communion"]?[[Item alloc] initWithDictionary:[missal_json objectForKey:@"antiphon_and_prayer_after_communion"]]:nil;
        self.custom = [missal_json objectForKey:@"custom"] && [[missal_json objectForKey:@"custom"] objectForKey:@"title"] && ![[[missal_json objectForKey:@"custom"] objectForKey:@"title"] isEqualToString:@""] && [[missal_json objectForKey:@"custom"] objectForKey:@"content"] && ![[[missal_json objectForKey:@"custom"] objectForKey:@"content"] isEqualToString:@""]?[[Item alloc] initWithDictionary:[missal_json objectForKey:@"custom"]]:nil;


        // NSLog(@"MissalObject:%@",self);

    }

    return self;
}

//=========================
// ISTANCE METHOD
//=========================
#pragma mark - ISTANCE METHOD
-(Item*)messaleComune{
    NSString *titolo = self.title;
    NSString *contenuto = @"";
    
    //INTRODUCTION
    if(self.introduction && self.introduction.contenuto && ![self.introduction.contenuto isEqualToString:@""])
    {
        contenuto = [contenuto stringByAppendingFormat:@"<div class=\"sezione\">%@</div>", self.introduction.titolo];
        contenuto = [contenuto stringByAppendingString:self.introduction.contenuto];;
    }
    
    //antiphon_and_opening_prayer
    if(self.antiphon_and_opening_prayer && self.antiphon_and_opening_prayer.contenuto && ![self.antiphon_and_opening_prayer.contenuto isEqualToString:@""])
    {
        contenuto = [contenuto stringByAppendingFormat:@"<div class=\"sezione\">%@</div>", self.antiphon_and_opening_prayer.titolo];
        contenuto = [contenuto stringByAppendingFormat:@"<br />%@", self.antiphon_and_opening_prayer.contenuto];;
    }
    
    //prayer_of_the_faithful
    if(self.prayer_of_the_faithful && self.prayer_of_the_faithful.contenuto && ![self.prayer_of_the_faithful.contenuto isEqualToString:@""])
    {
        contenuto = [contenuto stringByAppendingFormat:@"<div class=\"sezione\">%@</div>", self.prayer_of_the_faithful.titolo];
        contenuto = [contenuto stringByAppendingString:self.prayer_of_the_faithful.contenuto];;
    }
    
    //prayer_over_the_gifts
    if(self.prayer_over_the_gifts && self.prayer_over_the_gifts.contenuto && ![self.prayer_over_the_gifts.contenuto isEqualToString:@""])
    {
        contenuto = [contenuto stringByAppendingFormat:@"<div class=\"sezione\">%@</div>", self.prayer_over_the_gifts.titolo];
        contenuto = [contenuto stringByAppendingString:self.prayer_over_the_gifts.contenuto];;
    }
    
    //PREFACE
    if(self.preface && self.preface.contenuto && ![self.preface.contenuto isEqualToString:@""])
    {
        contenuto = [contenuto stringByAppendingFormat:@"<div class=\"sezione\">%@</div>", self.preface.titolo];
        contenuto = [contenuto stringByAppendingString:self.preface.contenuto];;
    }
    
    //antiphon_and_prayer_after_communion
    if(self.antiphon_and_prayer_after_communion && self.antiphon_and_prayer_after_communion.contenuto && ![self.antiphon_and_prayer_after_communion.contenuto isEqualToString:@""])
    {
        contenuto = [contenuto stringByAppendingFormat:@"<div class=\"sezione\">%@</div>", self.antiphon_and_prayer_after_communion.titolo];
        contenuto = [contenuto stringByAppendingString:self.antiphon_and_prayer_after_communion.contenuto];;
    }
    
    //Creo item
    Item *i = [Item new];
    [i setTitolo:titolo];
    [i setContenuto:contenuto];
    
    return i;
}

//=========================
// CLASS METHOD
//=========================
#pragma mark - CLASS METHOD
//CACHE DIRECTORY


//=======================================
//Encoding (FOR SAVING OBJECT)
//=======================================
#pragma mark - ENCODING
-(void)encodeWithCoder:(NSCoder*)decoder{
    
    //STRUCTURE
    [decoder encodeObject:self.title forKey:@"title"];
    [decoder encodeObject:self.subtitle forKey:@"subtitle"];
    [decoder encodeObject:self.type forKey:@"type"];
    [decoder encodeBool:self.obbligatorio forKey:@"obbligatorio"];
    [decoder encodeObject:self.id_event forKey:@"id_event"];
    [decoder encodeInt:self.id_santo forKey:@"id_santo"];

    
    //MISSAL
    [decoder encodeObject:self.variant forKey:@"variant"];
    [decoder encodeObject:self.introduction forKey:@"introduction"];
    [decoder encodeObject:self.introductory_rites forKey:@"introductory_rites"];
    [decoder encodeObject:self.eucharistic_liturgy forKey:@"eucharistic_liturgy"];
    [decoder encodeObject:self.communion_rites forKey:@"communion_rites"];
    [decoder encodeObject:self.antiphon_and_opening_prayer forKey:@"antiphon_and_opening_prayer"];
    [decoder encodeObject:self.first_reading forKey:@"first_reading"];
    [decoder encodeObject:self.second_reading forKey:@"second_reading"];
    [decoder encodeObject:self.responsorial_psalm forKey:@"responsorial_psalm"];
    [decoder encodeObject:self.gospel_acclamation forKey:@"gospel_acclamation"];
    [decoder encodeObject:self.gospel forKey:@"gospel"];
    [decoder encodeObject:self.prayer_of_the_faithful forKey:@"prayer_of_the_faithful"];
    [decoder encodeObject:self.prayer_over_the_gifts forKey:@"prayer_over_the_gifts"];
    [decoder encodeObject:self.preface forKey:@"preface"];
    [decoder encodeObject:self.antiphon_and_prayer_after_communion forKey:@"antiphon_and_prayer_after_communion"];
    [decoder encodeObject:self.custom forKey:@"custom"];

}

//======================================
//Decoding (FOR READ SAVED OBJECT)
//======================================
#pragma mark - DECODING
- (id)initWithCoder: (NSCoder *)aCoder {
    self = [super init];
    if (self) {

        //STRUCTURE
        self.title = [aCoder decodeObjectForKey:@"title"];
        self.subtitle = [aCoder decodeObjectForKey:@"subtitle"];
        self.type = [aCoder decodeObjectForKey:@"type"];
        self.obbligatorio = [aCoder decodeBoolForKey:@"obbligatorio"];
        self.id_santo = [aCoder decodeIntForKey:@"id_santo"];
        self.id_event = [aCoder decodeObjectForKey:@"id_event"];

 
        //MISSAL
        self.introduction = [aCoder decodeObjectForKey:@"introduction"];
        self.introductory_rites = [aCoder decodeObjectForKey:@"introductory_rites"];
        self.eucharistic_liturgy = [aCoder decodeObjectForKey:@"eucharistic_liturgy"];
        self.communion_rites = [aCoder decodeObjectForKey:@"communion_rites"];
        ;
        self.variant = [aCoder decodeObjectForKey:@"variant"];
        self.antiphon_and_opening_prayer = [aCoder decodeObjectForKey:@"antiphon_and_opening_prayer"];
        self.first_reading = [aCoder decodeObjectForKey:@"first_reading"];
        self.second_reading = [aCoder decodeObjectForKey:@"second_reading"];
        self.responsorial_psalm = [aCoder decodeObjectForKey:@"responsorial_psalm"];
        self.gospel_acclamation = [aCoder decodeObjectForKey:@"gospel_acclamation"];
        self.gospel = [aCoder decodeObjectForKey:@"gospel"];
        self.prayer_of_the_faithful = [aCoder decodeObjectForKey:@"prayer_of_the_faithful"];
        self.prayer_over_the_gifts = [aCoder decodeObjectForKey:@"prayer_over_the_gifts"];
        self.preface = [aCoder decodeObjectForKey:@"preface"];
        self.antiphon_and_prayer_after_communion = [aCoder decodeObjectForKey:@"antiphon_and_prayer_after_communion"];
        self.custom = [aCoder decodeObjectForKey:@"custom"];
        
    }
    return self;
}

//==================
//// NSLog %@
//==================
#pragma mark - // NSLog
-(NSString*)description{
    return [NSString stringWithFormat:@"<MissalObject type:%@ title:%@ >",self.type,self.title];
}




//=================
//END
//=================
@end

