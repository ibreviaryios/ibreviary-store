//
//  SantoObject.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import "SantoObject.h"
#import "Day.h"

@implementation SantoObject
/*
  sample:
        {
 
        id      : "43",
        image   : "http://www.ibreviary.com/img_db.php?t=r&id=43&n=2",
        name    : "Santa Scolastica",
        subtitle: "Vergine",
        text_info   : "<p>Scolastica ci &egrave;..."
        
        breviary_memory:
                        {
                        title: "Santa Scolastica, Vergine",
                        type : "Memoria",
                        saint_id: "43",
                        officeofreadings: { … },
                        lauds: { … },
                        daytimeprayers: { … },
                        vespers: { … },
                        compline: { … }
                        }
 
        missal_memory: {
                        title: "Santa Scolastica",
                        type: "Memoria",
                        saint_id: "43",
                        antiphon_and_opening_prayer: { … },
                        first_reading: { … },
                        responsorial_psalm: { … },
                        gospel_acclamation: { … },
                        gospel: { … },
                        prayer_of_the_faithful: { … },
                        prayer_over_the_gifts: { … },
                        preface: { … },
                        antiphon_and_prayer_after_communion": { … }
                        }
        }
 */

//ID
@synthesize id_santo;
//Detail
@synthesize url_image, image, name, subtitle, text_info, type;
//Breviario
@synthesize breviario;
//Missal
@synthesize missal;


//=================
// INIT
//=================
#pragma mark - INIT

- (id)init{
    if ((self = [super init])){
    }
    return self;
}


- (id)initWithDictionary:(NSDictionary*)d
      andBreviarioMemory:(NSDictionary*)jsonBreviarioMemory
         andMissalMemory:(NSDictionary*)jsonMissalMemory{
    
    // NSLog(@"SANTO INIT:%@",jsonMissalMemory);
    if ((self = [super init])){
        if([d objectForKey:@"id"] == [NSNull null])
            self.id_santo = 0;
        else self.id_santo = [d objectForKey:@"id"]?[[d objectForKey:@"id"] intValue]:0;
        //Detail
        self.url_image = [d objectForKey:@"image"]?[d objectForKey:@"image"]:@"";
        self.image = [SantoObject scaricaImmagini:self.url_image];
        
        self.name = [d objectForKey:@"name"]?[d objectForKey:@"name"]:@"";
        self.subtitle = [d objectForKey:@"subtitle"]?[d objectForKey:@"subtitle"]:@"";
        self.text_info = [d objectForKey:@"text"]?[d objectForKey:@"text"]:@"";
        self.type = [d objectForKey:@"type"]?[d objectForKey:@"type"]:nil;

        //Breviario
        if(jsonBreviarioMemory!=nil)
        {
        NSMutableDictionary *jbm = [NSMutableDictionary dictionaryWithDictionary:jsonBreviarioMemory];
        [jbm setObject:self.name forKey:@"title"];
        [jbm setObject:self.type forKey:@"type"];
        self.breviario = [[BreviarioObject alloc] initWithDictionary:jbm];
        }
        else
        self.breviario = nil;
        
        //Missal
        if(jsonMissalMemory!=nil)
        {
        NSMutableDictionary *jmm = [NSMutableDictionary dictionaryWithDictionary:jsonMissalMemory];
        [jmm setObject:self.name forKey:@"title"];
        [jmm setObject:self.type forKey:@"type"];
        // NSLog(@"jmMemory:%@",jmm);
        
        self.missal = [[MissalObject alloc] initWithDictionary:jmm];
        }
        else self.missal = nil;
        }
    return self;
}
//=========================
// SET AND GET
//=========================
#pragma mark - SET AND GET
-(NSString *)titolo {
    return [NSString stringWithFormat:@"%@ - %@",self.name, self.type];
 }

-(NSString *)htmlPdfString{
    NSString *html;
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *imageData = [UIImagePNGRepresentation(self.image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    NSString *santo_img =[NSString stringWithFormat:@"<div id=\"headerSanto\"><img src=\"data:image/png;base64,%@\" /></div>", imageData];
    NSString *titleHtml =[NSString stringWithFormat:@"<div class='sezione'>%@", self.name];
    //aggiungo sottotitolo se c'è
    if(![self.subtitle isEqualToString:@""]) titleHtml = [titleHtml stringByAppendingString:[NSString stringWithFormat:@"<br />%@</div>",self.subtitle]];
    else
        titleHtml = [titleHtml stringByAppendingString:@"</div>"];
    
    NSString *header=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/headerSantoShare.html",path] encoding:NSUTF8StringEncoding error:nil];
    
    NSString *share_footer=[NSString stringWithFormat:@"<div id=\"footer\"><div id=\"footer_conteiner\"><img class=\"img_footer\" src=\"iBreviaryLOGOFOOTER.png\"/><div class=\"footerText\">- %@</div></div></div>",[[Day caricaGiornoInUso] title]];
    

    
    html = [NSString stringWithFormat:@"%@%@<div id=\"textSanto\">%@%@</div>%@</div></body></html>",header,santo_img,titleHtml,self.text_info,share_footer];
    // NSLog(@"PREE!!");

    // NSLog(@"PDFsanto:%@",html);
    return html;
}

//=========================
// CLASS METHOD
//=========================
#pragma mark - CLASS METHOD
//CACHE DIRECTORY
+ (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}


+(BOOL)fileExistsAtPath:(NSString*)path{
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

+(UIImage*)scaricaImmagini:(NSString*)url_string{
    //DOWNLOAD SANTO IMAGE
    UIImage *img = nil;
    NSURL *url = [NSURL URLWithString:url_string];
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0+TIME_OUT_REQUEST];
    NSError *err = nil;
    // NSLog(@"request_santo_img:%@",theRequest);
    NSData *dataI = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err];
    
    //if(err!=nil)
    {
        //detail o download dovranno controllare la presenza del file!
        // NSLog(@"ERR santo_img %@",[err description]);
    }
    //else
    img = dataI?[UIImage imageWithData:dataI]:nil;
    
    // NSLog(@"Fine Download Santo Immagine");
    return img;
}


//=======================================
//Encoding (FOR SAVING OBJECT)
//=======================================
#pragma mark - ENCODING
-(void)encodeWithCoder:(NSCoder*)decoder{
    
    //ID
    [decoder encodeInt:self.id_santo forKey:@"id_santo"];
    //Detail
    [decoder encodeObject:self.url_image forKey:@"url_image"];
    [decoder encodeObject:self.image forKey:@"image"];
    [decoder encodeObject:self.name forKey:@"name"];
    [decoder encodeObject:self.subtitle forKey:@"subtitle"];
    [decoder encodeObject:self.text_info forKey:@"text_info"];
    [decoder encodeObject:self.type forKey:@"type"];
    
    //Breviario
    [decoder encodeObject:self.breviario forKey:@"breviario"];
    
    //Missal
    [decoder encodeObject:self.missal forKey:@"missal"];
 

}


//======================================
//Decoding (FOR READ SAVED OBJECT)
//======================================
#pragma mark - DECODING
- (id)initWithCoder: (NSCoder *)aCoder {
    self = [super init];
    if (self) {

        //ID
        self.id_santo = [aCoder decodeIntForKey:@"id_santo"];
        //Detail
        self.url_image = [aCoder decodeObjectForKey:@"url_image"];
        self.image = [aCoder decodeObjectForKey:@"image"];
        self.name = [aCoder decodeObjectForKey:@"name"];
        self.subtitle = [aCoder decodeObjectForKey:@"subtitle"];
        self.text_info = [aCoder decodeObjectForKey:@"text_info"];
        self.type = [aCoder decodeObjectForKey:@"type"];
        
        //Breviario
        self.breviario = [aCoder decodeObjectForKey:@"breviario"];
        
        //Missal
        self.missal = [aCoder decodeObjectForKey:@"missal"];
 
        }
    
    return self;
}


//==================
//// NSLog %@
//==================
#pragma mark - // NSLog
-(NSString*)description{
    return [NSString stringWithFormat:@"<SantoObject id:%i type:%@ bm:%@  mm:%@ >",self.id_santo ,self.type, self.breviario, self.missal];
}




//=================
//END
//=================
@end
