//
//  Item.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Item : NSObject<NSCoding>{
    
}

//ITEM
@property (strong, nonatomic) NSString *titolo;
@property (strong, nonatomic) NSString *titoloClass;
@property (strong, nonatomic) NSString *sottotitolo;
@property (strong, nonatomic) NSString *time;
@property (strong, nonatomic) NSString *contenuto;
@property (strong, nonatomic) NSString *use;
@property (strong, nonatomic) NSString *titolo_home;
@property (readonly, nonatomic) NSString *pdf_book_url;


-(id)initWithDictionary:(NSDictionary*)d;
-(void)mettiInCodaContenuto:(Item*)i;
-(NSString*)defaultWebContentString;

+(NSMutableArray*)initListOfItem:(NSArray *)lista;

@end
