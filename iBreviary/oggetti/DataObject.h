
/*
     File: DataObject
 Abstract: A value transformer, which transforms an object into an NSData object.
 Copyright (C) 2014 leyo. All Rights Reserved.
 
 */

@interface DataObject : NSValueTransformer

@end
