//
//  NotificaObject.m
//  iBreviary
//
//  Created by leyo on 30/09/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import "NotificaObject.h"
#import "DataObject.h"
#import "GlobalFunction.h"
#import "AppDelegate.h"
#import "Home.h"


//URL DEVICE
#define PATH_NOT @"/NOTIFICHE"
#define filenameformat @"id%@.notdata"
#define URL_NOTIFICHE @"http://www.ibreviary.org/iOS/services.php?type=getNot"
#define URL_NOTIFICHE_DEBUG @"http://www.ibreviary.com/iOS/debug/services.php?type=getNot"

#define  NOTIFICA_DONAZIONI @"NOTIFICA_DONAZIONI"
#define  NOTIFICA_NEWSLETTER @"NOTIFICA_NEWSLETTER"
#define  NOTIFICA_TEMPO_TRASCORSO_DA_OGGI @"NOTIFICA_TEMPO_TRASCORSO_DA_OGGI"
#define  NOTIFICA_LOOP_CUSTOM @"NOTIFICA_LOOP_CUSTOM"
#define  NOTIFICA_LOOP_CUSTOM_DICT @"NOTIFICA_LOOP_CUSTOM_DICT"
#define  NOTIFICA_DATA_ORA @"NOTIFICA_DATA_ORA"
#define  NOTIFICA_ISTANTANEA @"NOTIFICA_ISTANTANEA"

@interface NotificaObject ()

@property(readonly,nonatomic) NSString *filename;
@property(readonly,nonatomic) NSString *path;


@end

@implementation NotificaObject



NSTimer *icloudTimer;


//=========================
// SET METHOD & INT
//=========================
#pragma mark - INIT
- (id)init {
    // NSLog(@"Day INIT");
    if ((self = [super init])){
    }
    return self;
}

//=========================
// CREA/MODIFICA
//=========================
#pragma mark - CREA/MODIFICA
- (id)initWithDictionary:(NSDictionary*)d{
    NSDictionary *contenuto_localizzato = [d objectForKey:@"contenuto_localizzato"];
    // NSLog(@"contenuto_localizzato:%@",contenuto_localizzato);
    if(contenuto_localizzato){
    if ((self = [super init])){
        // NSLog(@"NotificaObj init:%@",d);
            self.id_notifica = [d objectForKey:@"id"]?[NSNumber numberWithInt:[[d objectForKey:@"id"] intValue]]:[NSNumber numberWithInt:0];
            self.type = [NSNumber numberWithInt:[[d objectForKey:@"type"] intValue]];
            self.giorni_frequenza = [NSNumber numberWithInt:[[d objectForKey:@"giorni_frequenza"] intValue]];
            self.soloUnaVolta = [NSNumber numberWithBool:[[d objectForKey:@"solo_una_volta"] isEqualToString:@"YES"]];
            self.sempre_presente_in_home = [NSNumber numberWithBool:[[d objectForKey:@"sempre_presente_in_home"] isEqualToString:@"YES"]];
            self.titolo = [contenuto_localizzato objectForKey:@"titolo"]?[contenuto_localizzato objectForKey:@"titolo"]:nil;
            
            self.contenuto = [contenuto_localizzato objectForKey:@"contenuto"]?[contenuto_localizzato objectForKey:@"contenuto"]:nil;
            self.link = [contenuto_localizzato objectForKey:@"link"]?[contenuto_localizzato objectForKey:@"link"]:nil;
            self.link_titolo = [contenuto_localizzato objectForKey:@"link_titolo"]?[contenuto_localizzato objectForKey:@"link_titolo"]:nil;
            self.last_update = [contenuto_localizzato objectForKey:@"last_update"]?[contenuto_localizzato objectForKey:@"last_update"]:nil;
            self.lingua = [contenuto_localizzato objectForKey:@"lingua"]?[contenuto_localizzato objectForKey:@"lingua"]:nil;
            self.attivo = [NSNumber numberWithBool:([[contenuto_localizzato objectForKey:@"attivo"] intValue]==1)];
            self.letto = [NSNumber numberWithInt:-1];
            // NSLog(@"self.letto=%@",self.letto);
            self.fired = nil;
            
            //SETTO DATA INIZIO
            self.data_inizio = nil;
            if([d objectForKey:@"data_inizio"])
            {
                // NSLog(@"data inizio");
                NSDateFormatter *f = [NSDateFormatter new];
                NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
                [f setTimeZone:gmt];
                [f setDateFormat:@"yyyy-MM-dd"];
                NSString *date_string = [d objectForKey:@"data_inizio"];
                if([d objectForKey:@"ora_inizio"])
                {
                    [f setDateFormat:@"yyyy-MM-dd HH:mm"];
                    date_string = [date_string stringByAppendingFormat:@" %@",[d objectForKey:@"ora_inizio"]];
                }
                self.data_inizio = [f dateFromString:date_string];
                // NSLog(@"self.data_inizio:%@",self.data_inizio);
            }
            else self.data_inizio = [NSDate date];
            
            
            if([self notificaScaduta]){
                NSLog(@"NOTIFICA:%@ scaduta!",self);
                return nil;
            }
            
            [self ScheduleLocalNotification];
            
            [self save];
        }
        return self;
    }
    return nil;
}

-(NSString *)filename{
    return [NSString stringWithFormat:filenameformat,self.id_notifica];
}

-(NSString *)path{
    return [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_NOT,self.filename]];
}

//=========================
// ISTANCE METHOD
//=========================
#pragma mark - ISTANCE METHOD
- (BOOL)save{
    if(![[NSFileManager defaultManager] fileExistsAtPath:[self.cacheDir stringByAppendingPathComponent:PATH_NOT]])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:[self.cacheDir stringByAppendingPathComponent:PATH_NOT] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *notPath = self.path;
    NSData *dataDaSalvare = [NSKeyedArchiver archivedDataWithRootObject:self];
    
    BOOL success = [dataDaSalvare writeToFile:notPath atomically:YES];
    if(success){
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:notPath]];
    }
    NSLog(@"saving[%@]...:%@",success?@"OK":@"ERR",self);
    return success;
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    NSError *error = nil;
    BOOL success = [URL setResourceValue:[NSNumber numberWithBool:YES]
                                  forKey:NSURLIsExcludedFromBackupKey
                                   error: &error];
    if(!success){
        NSLog(@"Day: addSkipBackupAttributeToItemAtURL: Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

#pragma mark MODIFICA NOTIFICA
//=========================
// MODIFICA NOTIFICA
//=========================
-(NotificaObject*)modificaNotifica:(NSDictionary*)d{
    
    // NSLog(@"NotificaObj init:%@",d);
    if([[d objectForKey:@"type"] intValue]==4){
        NSLog(@"%@ non può essere modificata",self);
        return self;
            }
    self.type = [NSNumber numberWithInt:[[d objectForKey:@"type"] intValue]];
    self.giorni_frequenza = [NSNumber numberWithInt:[[d objectForKey:@"giorni_frequenza"] intValue]];
    self.soloUnaVolta = [NSNumber numberWithBool:[[d objectForKey:@"solo_una_volta"] isEqualToString:@"YES"]];
    self.sempre_presente_in_home = [NSNumber numberWithBool:[[d objectForKey:@"sempre_presente_in_home"] isEqualToString:@"YES"]];
    NSDictionary *contenuto_localizzato = [d objectForKey:@"contenuto_localizzato"];
    // NSLog(@"contenuto_localizzato(%i):%@",[[contenuto_localizzato objectForKey:@"attivo"] intValue]==1,contenuto_localizzato);
    
    self.titolo = [contenuto_localizzato objectForKey:@"titolo"]?[contenuto_localizzato objectForKey:@"titolo"]:nil;
    self.contenuto = [contenuto_localizzato objectForKey:@"contenuto"]?[contenuto_localizzato objectForKey:@"contenuto"]:nil;
    self.link = [contenuto_localizzato objectForKey:@"link"]?[contenuto_localizzato objectForKey:@"link"]:nil;
    self.link_titolo = [contenuto_localizzato objectForKey:@"link_titolo"]?[contenuto_localizzato objectForKey:@"link_titolo"]:nil;
    self.lingua = [contenuto_localizzato objectForKey:@"lingua"]?[contenuto_localizzato objectForKey:@"lingua"]:nil;
    self.last_update = [contenuto_localizzato objectForKey:@"last_update"]?[contenuto_localizzato objectForKey:@"last_update"]:@"";
    self.attivo = [NSNumber numberWithBool:([[contenuto_localizzato objectForKey:@"attivo"] intValue]==1)];
    
    //SETTO DATA INIZIO
    self.data_inizio = nil;
    if([d objectForKey:@"data_inizio"] && self.type.intValue == 4)
    {
        NSDateFormatter *f = [NSDateFormatter new];
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [f setTimeZone:gmt];
        [f setDateFormat:@"yyyy-MM-dd"];
        NSString *date_string = [d objectForKey:@"data_inizio"];
        if([d objectForKey:@"ora_inizio"])
        {
            [f setDateFormat:@"yyyy-MM-dd HH:mm"];
            date_string = [date_string stringByAppendingFormat:@" %@",[d objectForKey:@"ora_inizio"]];
        }
        self.data_inizio = [f dateFromString:date_string];
    }
    
    else self.data_inizio = [NSDate date];
    
    [self save];
    
    [self ScheduleLocalNotification];
    return self;
}

//CACHE DIRECTORY
- (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}

//=========================
// CLASS METHOD
//=========================
#pragma mark - GET CLASS METHOD
+(NSArray*)getNotificheFileName{
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    return [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[[cachePathArray lastObject] stringByAppendingPathComponent:PATH_NOT] error:nil];
}


+(NSArray*)getNotifiche{
    NSMutableArray* lista = [NSMutableArray new];
    NSArray *listaName = [NotificaObject getNotificheFileName];
    for(NSString* fname in listaName){
        NSString *notPath = [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_NOT,fname]];
        NSData *d = [[NSData alloc] initWithContentsOfFile:notPath];
        NotificaObject *not = [NSKeyedUnarchiver unarchiveObjectWithData:d];
        [lista addObject:not];
    }
    return [NSArray arrayWithArray:lista];
}


+(NotificaObject*)notificaConId:(NSNumber*)i_n{
    NSArray *list = [NotificaObject getNotificheFileName];
    NSString *stringTosearch = [NSString stringWithFormat:filenameformat,i_n];
    
    for(NSString *fileName in list)
    {
        if ([fileName rangeOfString:stringTosearch].location != NSNotFound)
        {
            // NSLog(@"trovato");
            NSString *notPath = [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_NOT,fileName]];
            NSData *d = [[NSData alloc] initWithContentsOfFile:notPath];
            NotificaObject *not = [NSKeyedUnarchiver unarchiveObjectWithData:d];
            return not;
        }
    }
    return nil;
}

//=========================
// ScheduleLocalNotification
//=========================
#pragma mark Schedule
-(void)ScheduleLocalNotification
{
    NSLog(@"-ScheduleLocalNotification:%@",self);
    int type = self.type.intValue;
    
    if((type > 4 && type!=7)){
        [self deleteNotifica];
        return;
    }
    
    else if(type==4){
        if([self notificaScaduta]){
            //se la notifica è scaduta
            [self deleteNotifica];
            return;
            }
    }
    
        //se la data di notifica è trascorsa
    if(!self.fired || [self.fired timeIntervalSinceDate:[NSDate date]] > 0 ){
        [NotificaObject sendLocalNotification:self];
    }
}

/*
 0	notifica donazione
 1	notifica iscrizione newsletter
 2	tempo trascorso senza utilizzare app
 3	notifica loop custom (come donazione,newsletter)
 4	notifica DATA e ORA LOCALE (range giorni validità in giorni frequenza)
 5	notifica rimossa
 6	forza aggiornamento tutti testi fissi (è una notifica operativa, nessuna visualizzazione utente)
 7	messaggio presente SOLO per la home (non sarà creata nessuna notifica)
 */
+ (void)sendLocalNotification:(NotificaObject*)n
{
    NSLog(@"+sendLocalNotification:%@",n);
    int type = n.type.intValue;
    int giorno_insecondi = (60*60*24);
    if(type > 4) return; // 5=non usata 6=forza aggiornamento tutti testi fissi 7=solo home
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    // Set the category to the category that contains our action

    if([[UIDevice currentDevice].systemVersion floatValue] >= 8){
        // NSLog(@"category Settings > 8");
        localNotif.category = @"testCategory";
    }

    localNotif.alertBody = n.titolo;//n.titolo;
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    localNotif.userInfo = [n dictionary_userInfo];
    if(type==2){
        NSLog(@"type2 da implementare");
    }
    if(type==0 || type == 1 || type == 3){
        //notifiche ricorrenti
        int secondiDagiorni = n.giorni_frequenza.intValue * giorno_insecondi;
        secondiDagiorni += type*60;
        BOOL inizializzazioneNotificaPeriodica = NO;
        if(!n.fired){
            inizializzazioneNotificaPeriodica = YES;
            localNotif.fireDate = [[NSDate date] dateByAddingTimeInterval:-5000];
            n.fired = localNotif.fireDate;
            [n save];
        }
        NSLog(@"<<<<<<ciclo notifica ricorrente[%@]\n%@ : creala:%@",inizializzazioneNotificaPeriodica?@"YES":@"NO",n,([n.fired timeIntervalSinceDate:[NSDate date]] <= 0)?@"YES":@"NO");
        if((n.fired && [n.fired timeIntervalSinceDate:[NSDate date]] <= 0)){
            localNotif.fireDate = [n.fired dateByAddingTimeInterval:secondiDagiorni];
            n.fired = localNotif.fireDate;
            while([localNotif.fireDate timeIntervalSinceDate:[NSDate date]] <= 0 ){
                localNotif.fireDate = [localNotif.fireDate dateByAddingTimeInterval:secondiDagiorni];
                n.fired = localNotif.fireDate;
                NSLog(@"ciclo: ln:%@ n.fired:%@",localNotif.fireDate,n.fired);
            }
            //[n setLetto:inizializzazioneNotificaPeriodica?@0:@1];
            if(![n save]){
                //salvo in main non è andato
                NSLog(@"salvo n è andato:%@",n);

                }
            
            if(!inizializzazioneNotificaPeriodica){
                //mostro la notifica scattata senza notifiche
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [Home showNotifica:n];
                }];
            }
        }
        else{
            NSLog(@"Notifica %@ già con sveglia(%@)",n,[n notificaPresenteNelDevice]?@"YES":@"NO");
            return;
        }
    }
    
    if(type==4){
        //NOTIFICA DATA e ORA LOCALE
        NSTimeInterval secondi = [[NSDate date] timeIntervalSinceDate:n.data_inizio];
        int giorni_trascorsi = (int)secondi/giorno_insecondi;
        // NSLog(@"Differenza[DATA e ORA LOCALE] da [%@] a Ora in giorni:%i sec:%f",n.data_inizio, giorni_trascorsi,secondi);
        if(secondi>=0 && giorni_trascorsi >= 0 && giorni_trascorsi <= n.giorni_frequenza.intValue){
            // NSLog(@"Crea NOTIFICA type4!!!");
            localNotif.fireDate = [NSDate dateWithTimeIntervalSinceNow:60*2];//dopo 10minuti 60*10
        }
        else if(secondi<0){
            localNotif.fireDate = [n.data_inizio dateByAddingTimeInterval:11*60*60];//ore 11 del giorno data e ora
        }
    }
    
    n.fired = [[NSDate alloc] initWithTimeInterval:0 sinceDate:localNotif.fireDate];
    
    if(![n notificaPresenteNelDevice]){
        //attivo se non già presente
        NSLog(@">>>>>>>>>>>>>>>>attivo notifica\n>>>>>>>>>>>>>>>>\n%@\n>>>>>>>>>>>>>>>>\n",n);
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    }
    else{
        NSLog(@"Notifica %@ Fired Ma già presente nel dispositivo",n);
    }
    
    //salvo dopo le modifiche
    [n save];
    NSLog(@">>>>>>>>>>>>>>>>FINE +sendLocalNotification");
}

-(BOOL)notificaPresenteNelDevice{

    NSArray *list = [NotificaObject getNotificheFileName];
    NSString *stringTosearch = self.filename;
    BOOL presente = NO;

    for(NSString *fileName in list)
    {
        if ([fileName rangeOfString:stringTosearch].location != NSNotFound)
        {
            // NSLog(@"trovato");
             presente = YES;
        }
    }
    NSLog(@"notifica(%@)PresenteNelDevice:%@",self.id_notifica,presente?@"YES":@"NO");
    return presente;
}

+ (void)sendLocalNotificationTestAfterSecond:(NSInteger)sec
{
    NSArray * a = [NotificaObject getNotifiche];
    if(a.count==0) return;
    NotificaObject *n = [a objectAtIndex: arc4random() % [a count]];
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
#ifdef __IPHONE_8_0
    if([[UIDevice currentDevice].systemVersion floatValue] >= 8){
        // NSLog(@"category Settings > 8");
        localNotif.category = @"testCategory";
    }
#endif
    localNotif.alertBody = n.titolo;//n.titolo;
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    localNotif.userInfo = [n dictionary_userInfo];
    localNotif.fireDate = [NSDate dateWithTimeIntervalSinceNow:sec];
    n.fired = [[NSDate alloc] initWithTimeInterval:0 sinceDate:localNotif.fireDate];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    // NSLog(@"Sending TEST Local notification:%@",localNotif);
    
}



#pragma mark - ISTANCE METHOD
//=========================
// ISTANCE METHOD
//=========================
- (NSMutableDictionary*)dictionary_userInfo{
    NSMutableDictionary * d = [NSMutableDictionary new];
    [d setObject:[NSNumber numberWithInt:self.type.intValue] forKey:@"type"];
    [d setObject:self.titolo forKey:@"titolo"];
    [d setObject:[NSNumber numberWithInt:self.id_notifica.intValue] forKey:@"id"];
    return d;
}


-(BOOL)notificaScaduta{
    if(self.type.intValue<4 || self.type.intValue == 7) return NO;
    if(self.type.intValue == 5 || self.type.intValue == 6) return YES; //notifiche type disabilitato
    
    //caso notifica 4 !!
    int giorno_insecondi = (60*60*24);//86400
    NSTimeInterval secondi = [[NSDate date] timeIntervalSinceDate:self.data_inizio];
    int giorni_trascorsi = (int)secondi/giorno_insecondi;
    // NSLog(@"DATA ORA:%@ DATA INZIO:%@",[NSDate date],self.data_inizio);
    // NSLog(@"notificascaduta? sec:%i giorni trasc:%i freq:%i",(int)secondi,giorni_trascorsi,self.giorni_frequenza.intValue);
    if(giorni_trascorsi >= 0 && giorni_trascorsi >= self.giorni_frequenza.intValue){
        return YES;
    }
    self.sempre_presente_in_home = [NSNumber numberWithBool:YES];
    return NO;
}


- (void)notificaDaMostrare{
    self.letto = [NSNumber numberWithInt:-1];
    [self save];
    [Home aggiornaBadge];
}

- (void)impostaNotificaLetta{
    self.letto = [NSNumber numberWithInt:1];
    [self save];
    [Home aggiornaBadge];
}

- (void)impostaDaLeggereEbadge{
    self.letto = [NSNumber numberWithInt:-1];
    [self save];
    [Home aggiornaBadge];
}


-(BOOL)giaLetta{
    return self.letto.intValue > 0;
}

-(NSUserDefaults*)defaults{
    NSUserDefaults *de = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    return de;
}

#pragma mark - OPERAZIONI SU LISTA NOTIFICHE
//==============================
// GET NOTIFICHE
//==============================



//==============================
// N° NOTIFICHE DA LEGGERE
//==============================
+(NSInteger)notificheDaleggere{
    NSMutableArray *array = [NSMutableArray new];
    for(NotificaObject *row in [NotificaObject getNotifichePerHome]){
        // NSLog(@"");
        if(row.letto.integerValue<=0){
            [array addObject:row];
            NSLog(@"%@ da leggere",row);
        }
    }
    return array.count;
}

+(NSSet*)notificheLette{
    NSMutableSet *array = [NSMutableSet new];
    for(NotificaObject *row in [NotificaObject getNotifichePerHome]){
        // NSLog(@"");
        if(row.letto.integerValue>0){
            [array addObject:row.id_notifica];
            NSLog(@"%@ da leggere",row);
        }
    }
    // NSLog(@"notificheDaleggere:%i",(int)array.count);
    return array;
}

//==============================
// GET NOTIFICHE X HOME
//==============================
+(NSArray*)getNotifichePerHome{
    
    NSArray *list = [NotificaObject getNotifiche];
    
    //search
    NSPredicate *p = [NSPredicate predicateWithFormat:@"sempre_presente_in_home > 0"];
    NSArray *homeList = [list filteredArrayUsingPredicate:p];
    
    NSMutableArray *risposta = [NSMutableArray new];
    //check
    for(NotificaObject*row in homeList){
        if(row.type.intValue==4 && [row notificaScaduta]){
            [row deleteNotifica];
        }
        else [risposta addObject:row];
    }
    
    // NSLog(@"getNotifiche:%@",risposta);
    return [NSArray arrayWithArray:risposta];
    
}

+(void)info{
    NSLog(@"not:%@",[NotificaObject getNotifiche]);
}

#pragma mark DELETE NOTIFICA
//==============================
// DELETE NOTIFICA
//==============================
-(BOOL)deleteNotifica{
    NSString *deletepath = self.path;
    NSFileManager *fm = [NSFileManager defaultManager];
    
    if(![fm fileExistsAtPath:deletepath]){
        
        return NO;
    }
    BOOL risposta = [fm removeItemAtPath:deletepath error:nil];
    return risposta;
}

+(void)resetNotifiche{
    for(NotificaObject* n in [NotificaObject getNotifiche]){
        [n deleteNotifica];
    }
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [NotificaObject checkNotifiche];
}

#pragma mark CHECK NOTIFICHE
//=========================
// CHECK NOTIFICHE
//=========================
+(void)checkNotifiche{
    
    //forzo iCloudCheck
    [self notificheDaleggere];
    
    NSLog(@" called Notifica");

        NSLog(@"checkNotifiche starting..");
        NSError *err = nil;
         if(![GlobalFunction checkNetworkStatus]){
            // NSLog(@"checkNotifiche: noNet");
            [self checkNotificheRicorrenti];
            return;
        }
        //DOWNLOAD
        NSURL *url = nil;
    
        if([[NSUserDefaults standardUserDefaults] boolForKey:IBREVIARY_DEBUG_MODE]){
        
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&lang=%@",URL_NOTIFICHE_DEBUG,NSLocalizedString(@"checklingua", nil)]];
        }
        else{
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&lang=%@",URL_NOTIFICHE,NSLocalizedString(@"checklingua", nil)]];
        
        }
 
//<<<<<<<<<<<<<<<<<<<
// TEST SOLO ARABO
// url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&lang=%@",URL_NOTIFICHE,@"ar"]];
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    
        NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_REQUEST];
        NSLog(@"checkNotifiche:%@",theRequest);
        NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err];
        if(err!=nil){
            NSLog(@"checkNotifiche: ERR lastUpdate: %@",[err description]);
            return;
        }
        else{
            NSMutableDictionary *risposta = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err]];
            
            
            //NSLog(@"checkNotifiche:[URL]->%@",url);
            if(err){
                NSLog(@"checkNotifiche: Errore parsing Json lastupdate:%@",err);
                return;
            }
            
            
            NSLog(@"lista_notificaJSON:%@",[risposta objectForKey:@"lista_notifiche"]);
            
            NSUserDefaults *def = [NotificaObject userDefault];
            
            NSLog(@"NOTIFICHE_LAST_UPDATE_RICORRENZA:%@\nRISPOSTA:%@",[def objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA], [risposta objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA]);
            NSLog(@"NOTIFICHE_ID_ISTANT:%@\nRISPOSTA:%@",[def objectForKey:NOTIFICHE_LAST_ID_ISTANT], [risposta objectForKey:NOTIFICHE_LAST_ID_ISTANT]);
            
            
            if(![def objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA] || ![[def objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA] isEqualToString:[risposta objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA]] || [[risposta objectForKey:NOTIFICHE_LAST_ID_ISTANT] intValue]!=[[def objectForKey:NOTIFICHE_LAST_ID_ISTANT] intValue]){
                
            
             //NSLog(@"lista_notificaJSON:%@",[risposta objectForKey:@"lista_notifiche"]);
            
             //NSLog(@"NOTIFICHE_LAST_UPDATE_RICORRENZA:%@\nRISPOSTA:%@",[def objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA], [risposta objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA]);
            
             //NSLog(@"NOTIFICHE_ID_ISTANT:%@\nRISPOSTA:%@",[def objectForKey:NOTIFICHE_LAST_ID_ISTANT], [risposta objectForKey:NOTIFICHE_LAST_ID_ISTANT]);
            
            //NSInteger countList = [[risposta objectForKey:@"lista_notifiche"] count];
                ;
            
            NSArray *a = [NotificaObject getNotifiche];
            NSMutableDictionary *notificheDaCancellare = [NSMutableDictionary new];

                
                NSLog(@"committing notifiche protocol");
                for (NotificaObject *n in a) {
                    [notificheDaCancellare setObject:[NSNumber numberWithBool:YES] forKey:n.id_notifica];
                }
                //Analizzo ogni singola notifica
                NSArray *lista_notifiche = [risposta objectForKey:@"lista_notifiche"];
                
                for (int i=0; i<lista_notifiche.count; i++) {
                    NSDictionary *row_json = [lista_notifiche objectAtIndex:i];
                    [notificheDaCancellare removeObjectForKey:[NSNumber numberWithInt:[[row_json objectForKey:@"id"] intValue]]];
                    //NSLog(@"row_json:%@",row_json);
                    if([[row_json objectForKey:@"type"] intValue] != 6 && [[row_json objectForKey:@"type"] intValue] != 5 && [row_json objectForKey:@"contenuto_localizzato"]!=nil)
                    {
                        NotificaObject *row = [NotificaObject notificaConId:[NSNumber numberWithInt:[[row_json objectForKey:@"id"] intValue]]];
                        if(row!=nil){
                            NSString *server_lastUpdate = [[row_json objectForKey:@"contenuto_localizzato"]  objectForKey:@"last_update"];
                            
                            if(![row.last_update isEqualToString:server_lastUpdate] || (row.type.integerValue==3 && ![row.titolo isEqualToString:[row_json objectForKey:@"titolo"]]) || (row.type.integerValue == 7 && ![row.titolo isEqualToString:[row_json objectForKey:@"titolo"]]))
                                [row modificaNotifica:row_json];
                        }
                        else{
                            NotificaObject *n = [[NotificaObject alloc] initWithDictionary:row_json];
                            [n save];
                        }
                    }
                }
                
                // NSLog(@"Notifiche da cancellare %@",notificheDaCancellare.allKeys);
                for (NSNumber *key in notificheDaCancellare.allKeys) {
                    NotificaObject *notifica_del = [NotificaObject notificaConId:key];
                    if(notifica_del) [notifica_del deleteNotifica];
                }
                
                
                
                [def setObject:[risposta objectForKey:NOTIFICHE_LAST_UPDATE_RICORRENZA]
                        forKey:NOTIFICHE_LAST_UPDATE_RICORRENZA];
                
                [def setObject:[risposta objectForKey:NOTIFICHE_LAST_ID_ISTANT]
                        forKey:NOTIFICHE_LAST_ID_ISTANT];
                
                [def synchronize];
                
                
                
            }//END SHOULD UPDATE
            

            [NotificaObject updateHome];
            
        }//END RISPOSTA DAL SERVER NOTIFICHE
    
    
}

+(NSString*)debugString{
    NSMutableString *debugMex = [NSMutableString new];
    NSDictionary *options = nil;
    [debugMex appendString:@"NOTIFICHE on iCLoud\n============\n"];
    [debugMex appendFormat:@" - url:%@\n",[options objectForKey:NSPersistentStoreUbiquitousContentURLKey]];
    [debugMex appendFormat:@" - options:%@\n",options];
    [debugMex appendString:@"\nda fare se serve\n=================\n"];

    return debugMex;
}

+(NSArray*)listaNotificheAttive{
    UIApplication *objApp = [UIApplication sharedApplication];
    NSArray *oldNotifications = [objApp scheduledLocalNotifications];
    return oldNotifications;
}
    
    
+(void)updateHome{
    
    // STEP 0
    NSBlockOperation *step0 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"checkNotifiche: fine step(1) aggiorno notifica_view");
        [NotificaObject notificheDaleggere];
    }];//fine step0

    // STEP 1
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"checkNotifiche: fine step(1) aggiorno notifica_view");
        [self checkNotificheRicorrenti];
    }];//fine step1
    
    // STEP 2
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"checkNotifiche: fine step(1) aggiorno notifica_view");
        [Home aggiornaBadge];
    }];//fine step2

    [step1 addDependency:step0];
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step0];
    [[NSOperationQueue new] addOperation:step1];
    [[NSOperationQueue new] addOperation:step2];
}

+(void)checkNotificheRicorrenti{
    NSLog(@"checkNotificheRicorrenti");
    NSArray *a = [NSArray arrayWithArray:[NotificaObject getNotifiche]];
    
    for(NotificaObject *n in a){
        
        if(n.type.intValue<4){
            //notifiche ricorrenti
            [NotificaObject sendLocalNotification:n];
        }
    }
}


+(void)resettoRemoteNotifichePush{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    //[[UIApplication sharedApplication] cancelAllLocalNotifications];
}


#pragma mark -
#pragma mark POST TOKEN
//=========================
// POST TOKEN
//=========================
+(void)postToken
{
    NSUserDefaults *def = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    if([def boolForKey:TOKEN_INVIATO]) return;
    if(![GlobalFunction checkNetworkStatus])
    {
        // NSLog(@"NOTiFICAobj:ERR postToken no net");
        return;
    }
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&token=%@&lingua=%@",URL_POST_TOKEN,[def objectForKey:TOKEN],NSLocalizedString(@"checklingua", nil)]];
    NSMutableURLRequest *richiesta = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20];
    NSError* err = nil;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:richiesta returningResponse:nil error:&err];
    if(err != nil)//controllo errore da sendSynchronousRequest
    {
        // NSLog(@"NOTiFICAobj:ERR da postToken: %@", [err description]);
    }
    
    NSDictionary *risposta = [[NSDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data
                                                                                                      options:NSJSONReadingMutableContainers
                                                                                                        error:&err]];
    if(err != nil)//controllo errore da JSON
    {
        // NSLog(@"NOTiFICAobj:ERR da postToken: %@", [err description]);
    }
    
    if([[risposta objectForKey:@"risposta" ] isEqualToString: @"OK"])//controllo risposta web server
    {
        [def setBool:YES forKey:TOKEN_INVIATO];
        [def synchronize];
    }
    
}

//=========================
// NOTIFICATION TYPE
//=========================
#pragma mark - BADGE NUMBER

#ifdef __IPHONE_8_0

+ (BOOL)checkNotificationType:(UIUserNotificationType)type
{
    UIUserNotificationSettings *currentSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
    
    return (currentSettings.types & type);
}

#endif

//=========================
// BADGE NUMBER
//=========================
#pragma mark - BADGE NUMBER
+ (void)setApplicationBadgeNumber:(NSInteger)badgeNumber
{
    UIApplication *application = [UIApplication sharedApplication];
    
#ifdef __IPHONE_8_0
   
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        application.applicationIconBadgeNumber = badgeNumber;
        }
    else
        {
        if ([NotificaObject checkNotificationType:UIUserNotificationTypeBadge]){
                // NSLog(@"badge number changed to %i", (int)badgeNumber);
                application.applicationIconBadgeNumber = badgeNumber;
                }
        else{
            // NSLog(@"access denied for UIUserNotificationTypeBadge");
        }
    }
    
#else
    // compile with Xcode 5 (iOS SDK < 8.0)
    application.applicationIconBadgeNumber = badgeNumber;
    
#endif
}


//==================
//// NSLog %@
//==================
#pragma mark - // NSLog
-(NSString*)description{
    //sourceString();
    if(self.type.intValue==7)
        return [NSString stringWithFormat:@"<notifica[%@Honly] type:%i tit:%@ letto:%@>",self.id_notifica,self.type.intValue,self.titolo,self.letto.integerValue>0?@"YES":@"NO"];
    return [NSString stringWithFormat:@"<notifica[%@] type:%i inizio:%@ giorni_frequenza:%i fired:%@ home:%@ letto:%@>",self.id_notifica,self.type.intValue,self.data_inizio,self.giorni_frequenza.intValue,self.fired,self.sempre_presente_in_home.integerValue>0?@"YES":@"NO",self.letto.integerValue>0?@"YES":@"NO"];
}

//=======================================
//Encoding (FOR SAVING OBJECT)
//=======================================
#pragma mark - ENCODING
-(void)encodeWithCoder:(NSCoder*)decoder{
    
    //NOT_STRUCTURE
    [decoder encodeObject:self.id_notifica forKey:@"id_notifica"];
    [decoder encodeObject:self.titolo forKey:@"titolo"];
    [decoder encodeObject:self.lingua forKey:@"lingua"];
    [decoder encodeObject:self.contenuto forKey:@"contenuto"];
    [decoder encodeObject:self.link forKey:@"link"];
    [decoder encodeObject:self.link_titolo forKey:@"link_titolo"];
    [decoder encodeObject:self.last_update forKey:@"last_update"];
    [decoder encodeObject:self.data_inizio forKey:@"data_inizio"];
    [decoder encodeObject:self.fired forKey:@"fired"];
    [decoder encodeObject:self.giorni_frequenza forKey:@"giorni_frequenza"];
    [decoder encodeObject:self.soloUnaVolta forKey:@"soloUnaVolta"];
    [decoder encodeObject:self.sempre_presente_in_home forKey:@"sempre_presente_in_home"];
    [decoder encodeObject:self.attivo forKey:@"attivo"];
    [decoder encodeObject:self.letto forKey:@"letto"];
    [decoder encodeObject:self.type forKey:@"type"];
    
}

//======================================
//Decoding (FOR READ SAVED OBJECT)
//======================================
#pragma mark - DECODING
- (id)initWithCoder: (NSCoder *)aCoder {
    self = [super init];
    if (self) {
        
        //NOT_STRUCTURE
        self.id_notifica =      [aCoder decodeObjectForKey:@"id_notifica"];
        self.titolo =           [aCoder decodeObjectForKey:@"titolo"];
        self.lingua =           [aCoder decodeObjectForKey:@"lingua"];
        self.contenuto =        [aCoder decodeObjectForKey:@"contenuto"];
        self.link =             [aCoder decodeObjectForKey:@"link"];
        self.link_titolo =      [aCoder decodeObjectForKey:@"link_titolo"];
        self.last_update =      [aCoder decodeObjectForKey:@"last_update"];
        self.data_inizio =      [aCoder decodeObjectForKey:@"data_inizio"];
        self.type =             [aCoder decodeObjectForKey:@"type"];
        self.fired =            [aCoder decodeObjectForKey:@"fired"];
        self.giorni_frequenza = [aCoder decodeObjectForKey:@"giorni_frequenza"];
        self.soloUnaVolta =     [aCoder decodeObjectForKey:@"soloUnaVolta"];
        self.sempre_presente_in_home = [aCoder decodeObjectForKey:@"sempre_presente_in_home"];
        self.attivo =           [aCoder decodeObjectForKey:@"attivo"];
        self.letto =            [aCoder decodeObjectForKey:@"letto"];
        self.type =             [aCoder decodeObjectForKey:@"type"];
        
    }
    return self;
    
}


//=========================
// CLASS METHOD
//=========================
#pragma mark - CLASS METHOD
//CACHE DIRECTORY
+ (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}

+(NSUserDefaults*)userDefault{
    NSUserDefaults *de = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    return de;
}


+(BOOL)fileExistsAtPath:(NSString*)path{
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

//=================
//END
//=================
@end