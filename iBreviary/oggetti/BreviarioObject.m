//
//  BreviarioObject.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import "BreviarioObject.h"


@implementation BreviarioObject

//BREVIARIO
@synthesize title, subtitle, type, obbligatorio, complete, first_vespers, officeofreading, lauds, daytimeprayers, vespers, compline,id_santo,id_event;


//=================
// INIT
//=================
#pragma mark - INIT
- (id)init {
    // NSLog(@"BreviarioObject INIT");
    if ((self = [super init])){
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary*)breviary_json{
    if ((self = [super init])){
        // NSLog(@"BreviarioObject inizialize:%@",breviary_json);
        
        self.complete = [[breviary_json objectForKey:@"complete"] boolValue];
        self.title = [breviary_json objectForKey:@"title"]?[breviary_json objectForKey:@"title"]:nil;
        self.subtitle = [breviary_json objectForKey:@"subtitle"]?[breviary_json objectForKey:@"subtitle"]:nil;
        self.type = [breviary_json objectForKey:@"type"]?[breviary_json objectForKey:@"type"]:nil;
        self.obbligatorio = [breviary_json objectForKey:@"mandatory"]?([[breviary_json objectForKey:@"mandatory"] intValue]==1):NO;
        if([breviary_json objectForKey:@"saint_id"] == [NSNull null]) self.id_santo = 0;
        else self.id_santo = [breviary_json objectForKey:@"saint_id"]?[[breviary_json objectForKey:@"saint_id"] intValue]:-1;
        self.id_event = [breviary_json objectForKey:@"id_event"]?[breviary_json objectForKey:@"id_event"]:nil;

        self.lauds = [breviary_json objectForKey:@"lauds"]?[[Item alloc] initWithDictionary:[breviary_json objectForKey:@"lauds"]]:nil;
        
        //Breviary
        self.first_vespers = [breviary_json objectForKey:@"first_vespers"]?[[Item alloc] initWithDictionary:[breviary_json objectForKey:@"first_vespers"]]:nil;
        self.officeofreading = [breviary_json objectForKey:@"officeofreadings"]?[[Item alloc] initWithDictionary:[breviary_json objectForKey:@"officeofreadings"]]:nil;
        self.lauds = [breviary_json objectForKey:@"lauds"]?[[Item alloc] initWithDictionary:[breviary_json objectForKey:@"lauds"]]:nil;
        self.daytimeprayers = [breviary_json objectForKey:@"daytimeprayers"]?[[Item alloc] initWithDictionary:[breviary_json objectForKey:@"daytimeprayers"]]:nil;
        self.vespers = [breviary_json objectForKey:@"vespers"]?[[Item alloc] initWithDictionary:[breviary_json objectForKey:@"vespers"]]:nil;
        self.compline = [breviary_json objectForKey:@"compline"]?[[Item alloc] initWithDictionary:[breviary_json objectForKey:@"compline"]]:nil;

        // NSLog(@"BreviarioObject:%@",self);

    }
    return self;
}

//=========================
// ISTANCE METHOD
//=========================
#pragma mark - ISTANCE METHOD


//=========================
// CLASS METHOD
//=========================
#pragma mark - CLASS METHOD
//CACHE DIRECTORY


//=======================================
//Encoding (FOR SAVING OBJECT)
//=======================================
#pragma mark - ENCODING
-(void)encodeWithCoder:(NSCoder*)decoder{
    
    //TITLE
    [decoder encodeObject:self.title forKey:@"title"];
    [decoder encodeObject:self.subtitle forKey:@"subtitle"];
    [decoder encodeObject:self.type forKey:@"type"];
    [decoder encodeBool:self.complete forKey:@"complete"];
    [decoder encodeBool:self.obbligatorio forKey:@"obbligatorio"];
    [decoder encodeObject:self.id_event forKey:@"id_event"];
    [decoder encodeInt:self.id_santo forKey:@"id_santo"];

    
    //BREVIARIO
    [decoder encodeObject:self.first_vespers forKey:@"first_vespers"];
    [decoder encodeObject:self.officeofreading forKey:@"officeofreading"];
    [decoder encodeObject:self.lauds forKey:@"lauds"];
    [decoder encodeObject:self.daytimeprayers forKey:@"daytimeprayers"];
    [decoder encodeObject:self.vespers forKey:@"vespers"];
    [decoder encodeObject:self.compline forKey:@"compline"];
    
}

//======================================
//Decoding (FOR READ SAVED OBJECT)
//======================================
#pragma mark - DECODING
- (id)initWithCoder: (NSCoder *)aCoder {
    self = [super init];
    if (self) {

        self.title = [aCoder decodeObjectForKey:@"title"];
        self.subtitle = [aCoder decodeObjectForKey:@"subtitle"];
        self.type = [aCoder decodeObjectForKey:@"type"];
        self.complete = [aCoder decodeBoolForKey:@"complete"];
        self.obbligatorio = [aCoder decodeBoolForKey:@"obbligatorio"];
        self.id_santo = [aCoder decodeIntForKey:@"id_santo"];
        self.id_event = [aCoder decodeObjectForKey:@"id_event"];

        //BREVIARIO
        self.first_vespers = [aCoder decodeObjectForKey:@"first_vespers"];
        self.officeofreading = [aCoder decodeObjectForKey:@"officeofreading"];
        self.lauds = [aCoder decodeObjectForKey:@"lauds"];
        self.daytimeprayers = [aCoder decodeObjectForKey:@"daytimeprayers"];
        self.vespers = [aCoder decodeObjectForKey:@"vespers"];
        self.compline = [aCoder decodeObjectForKey:@"compline"];
    }
    return self;
}


//==================
//// NSLog
//==================
#pragma mark - // NSLog
-(NSString*)description{
    return [NSString stringWithFormat:@"<BreviarioObject>\n type:%@\ntitle:%@ \nsubtitle:%@\n",self.type,self.title,self.subtitle];
}


//=================
//END
//=================
@end

