//
//  LinguaObject.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface LinguaObject : NSObject<NSCoding>{
    
}

//DETAIL
@property (strong, nonatomic) UIColor *colore;
@property (strong, nonatomic) NSString *sigla;
@property (strong, nonatomic) NSString *nome;
@property (nonatomic) BOOL alignment_right;

@property (readonly, nonatomic) UIImage *logo;
@property (readonly, nonatomic) UIView *view;


- (id)initWithDictionary:(NSDictionary*)d;

//GET METHOD
+(NSMutableArray*)getLingue;
+(LinguaObject*)getLinguaBySigla:(NSString*)s;
+(LinguaObject*)getCurrentLingua;

//SET METHOD
+(void)setLingueFromArray:(NSArray*)array_lingue;

@end
