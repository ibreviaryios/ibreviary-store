//
//  Day.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "Item.h"
#import "LinguaObject.h"
#import "SantoObject.h"
#import "TestoProprio.h"
#import "BreviarioObject.h"
#import "MissalObject.h"

@class Day;

@protocol SystemGetterProtocol <NSObject>
- (void)didFinishDownloadingDay:(Day*)d;
@end

@interface Day :  NSObject<NSCoding>{
    
}

//DAY_STRUCTURE
@property (strong, nonatomic) NSString *id_date; //YYYY-MM-DD
@property (strong, nonatomic) NSString *filename;
@property (strong, nonatomic) NSString *lingua;
@property (strong, nonatomic) LinguaObject *linguaGiorno;
@property (nonatomic) BOOL complete;
@property (nonatomic) NSInteger week;

//LABEL_LIST
@property (strong, nonatomic) NSString *day_label;

//DAY_INFO
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *type_home;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subtitle;
@property (strong, nonatomic) NSArray *event;
@property (strong, nonatomic) NSString *day_type;
@property (strong, nonatomic) NSString *day_time;

    
//BREVIARIO
@property (strong, nonatomic) BreviarioObject *breviario;

//MISSAL
@property (strong, nonatomic) NSMutableArray *missal;


//SANTO
@property (strong, nonatomic) NSMutableArray *santo_array;
@property (nonatomic) BOOL santo_bool;
    
//EVENTO TP
@property (strong, nonatomic) BreviarioObject *tp_breviario;
@property (strong, nonatomic) MissalObject *tp_missal;
    
//EVENTO HOME TP
@property (strong, nonatomic) BreviarioObject *tpHome_breviario;
@property (strong, nonatomic) MissalObject *tpHome_missal;

//SPEACH VOICE
@property (strong, nonatomic) NSString *voice;

//YESTERDAY COMPLINE
@property (strong, nonatomic) Item *compline_yesterday;


//INIT
-(id)initWithDictionary:(NSDictionary*)json_day;

// ISTANCE METHOD
-(UIColor*)colorWeek;
-(UIColor*)messaColorWeek;
-(BOOL)save;
-(BOOL)saveWithError:(NSError**)e;
-(NSMutableArray*)listaBreviari;
-(BOOL)breviarioIsEmpty;
-(NSMutableArray*)listaMissals;
-(BOOL)hasHomeTP;
-(NSMutableArray*)lista4HomeTP;
-(Item*)listaLettureAggregate:(MissalObject*)m;
-(NSMutableArray*)listaLetture:(MissalObject*)m;
-(int)intForBreviaryFromMissalPos:(int)missal_pos;
-(int)intForMissalFromBreviaryPos:(int)breviary_pos;
// WIDGET QUERY
-(Item*)getPray:(NSString*)pray;

// CLASS METHOD
+(NSArray*)getListDays;
+(BOOL)deleteDayWithName:(NSString*)dayName andError:(NSError**)e;
+(BOOL)isCurrentDay:(NSIndexPath*)indexPath;
+(Day*)searchForDay:(NSString*)date_day withLang:(NSString *)lang;

// CHECK AUTO-OGGI
+(void)checkAutoOggi:(App_type)t;

//DOWNLOAD DAY
+(void)scaricaGiornoconData:(NSString*)date_day
                   withLang:(NSString *)lang
                  eventFlag:(TestoProprio*)tp
                andDelegate:(NSObject<SystemGetterProtocol> *)delegate;
+(void)scaricaGiornoconData:(NSString*)date_day
                   withLang:(NSString *)lang
                  eventFlag:(TestoProprio*)tp
            completionBlock:(Day*(^)(Day *day))completion;
+(void)scaricaGiornoconData:(NSString*)date_day
                   withLang:(NSString *)lang
                  eventFlag:(TestoProprio*)tp
                andDelegate:(NSObject<SystemGetterProtocol> *)delegate
                   andError:(NSError**)errore;
+(Day*)scaricaGiornoFirstconData:(NSString*)date_day
                        withLang:(NSString *)lang
                       eventFlag:(TestoProprio*)tp;
+(Day*)scaricaSYNCGiornoconData:(NSString*)date_day
                       withLang:(NSString *)lang
                      eventFlag:(TestoProprio*)tp;

//DOWNLOAD WEEK
+(void)scaricaSettimana:(NSString*)lang
                   eventFlag:(TestoProprio*)tp
             completionBlock:(void(^)(NSString *risposta))completion;

//GET SANTO IMG ARRAY
+(NSMutableArray*)getSantPreviewForIndex:(NSIndexPath *)indexPath;

//GIORNO_IN_USO
+(Day*)caricaGiornoInUso;
-(BOOL)salvaGiornoInUso:(NSError**)e;

@end
