//
//  Messa.h
//  iBreviary
//
//  Created by leyo on 27/08/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "Day.h"
#import "Item.h"


@interface Messa : NSManagedObject

@property (nonatomic, retain) NSString * messa_name;
@property (nonatomic) BOOL isComune;
@property (nonatomic, retain) NSString * date_string;
@property (nonatomic, readonly) NSString * lingua_string;
@property (nonatomic, retain) NSString * titolo_custom;
@property (nonatomic, retain) Day *day;
@property (nonatomic, retain) Item *riti_introduzione;
@property (nonatomic, retain) Item *antifonaEcolletta;
@property (nonatomic, retain) NSMutableArray * letture;
@property (nonatomic, retain) NSString * omelia;
@property (nonatomic, retain) NSMutableArray *credo;
@property (nonatomic, retain) NSMutableArray *preghieraDeiFedeli;
@property (nonatomic, retain) Item *liturgia_eucaristica_da_ordinario;
@property (nonatomic, retain) NSMutableArray *prefazio;
@property (nonatomic, retain) NSMutableArray *preghieraEucaristica;
@property (nonatomic, retain) Item *riti_comuni_da_ordinario;
@property (nonatomic, retain) Item *antifonaDopo;
@property (nonatomic, retain) Item *riti_conclusione_da_ordinario;
@property (nonatomic, retain) NSMutableArray *benedizioneSolenne;
@property (nonatomic, retain) NSString * avvisi;
@property (nonatomic, retain) NSString * extra;
@property (nonatomic, retain) Item *sulleofferte;
@property (nonatomic, retain) NSNumber * sel_credo;
@property (nonatomic, retain) NSNumber * sel_preghieraDeiFedeli;
@property (nonatomic, retain) NSNumber * sel_prefazio;
@property (nonatomic, retain) NSNumber * sel_preghieraEucaristica;
@property (nonatomic, retain) NSNumber * sel_benedizioneSolenne;
@property (nonatomic, retain) NSNumber * sel_missal;
@property (nonatomic, retain) NSNumber * sel_letture;
@property (nonatomic, readonly) NSUserDefaults *defaults;
@property (nonatomic, readonly) NSString * pdf_book_url;

-(NSString*)pdfBookNameFile;

//INIT METHOD
+(Messa*)addMessaWithDay:(Day*)d forContext:(NSManagedObjectContext*)context;

//GET METHOD
+(NSArray*)getMesseIn:(NSManagedObjectContext*)context;
+(Messa*)getMessaWithDateString:(NSString*)date_search in:(NSManagedObjectContext*)context;


//BUILD HTML
-(NSString*)htmlString;
-(NSString*)htmlPdfStringWithOmelia:(BOOL)visualizzare_omelia;
- (NSString*)htmlPdfBookWithOmelia:(BOOL)visualizzare_omelia;

//CAMBIA LETTURE
-(void)cambiaMessale:(NSInteger)numero;

//DELETE
+(BOOL)deleteMessa:(Messa*)messaDaCancellare andError:(NSError**)e forContext:(NSManagedObjectContext*)context;
+(void)deleteAll;

@end
