/*
 File: DataObject
 Abstract: A value transformer, which transforms an object into an NSData object.
 Copyright (C) 2014 leyo. All Rights Reserved.
 
 */

#import "DataObject.h"


@implementation DataObject


+ (BOOL)allowsReverseTransformation {
	return YES;
}


+ (Class)transformedValueClass {
	return [NSData class];
}


- (id)transformedValue:(id)value {
	return [NSKeyedArchiver archivedDataWithRootObject:value];
}


- (id)reverseTransformedValue:(id)value {
	return [NSKeyedUnarchiver unarchiveObjectWithData:value];
}


@end

