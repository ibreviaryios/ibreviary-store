//
//  TestoProprio.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface TestoProprio : NSObject<NSCoding>{
    
}

@property (strong, nonatomic) NSString *titolo;
@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) UIImage *image_official;
@property (strong, nonatomic) UIImage *logo;
@property (strong, nonatomic) NSString *url_image_official;
@property (strong, nonatomic) NSString *url_image_logo;
@property (nonatomic) BOOL isForHome;

- (id)initWithDictionary:(NSDictionary*)d;


//GET METHOD
+(NSMutableArray*)getTestiPropri;
+(TestoProprio*)getTestoProprioWithKey:(NSString*)k;
+(NSMutableArray*)getTestiPropriForHome;
+(BOOL)isForHomeTPwithKey:(NSString*)k;
+(int)getPosTestoProprioWithKey:(NSString*)k;
//SET METHOD
+(void)setTestiPropri:(NSArray*)arrayTP;

@end
