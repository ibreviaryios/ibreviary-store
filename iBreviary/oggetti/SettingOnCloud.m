//
//  SettingOnCloud.m
//  iBreviary
//
//  Created by Leonardo Parenti on 07/05/15.
//  Copyright (c) 2015 leyo. All rights reserved.
//

#import "SettingOnCloud.h"
#import "AppDelegate.h"
#import "DataObject.h"
#import "NotificaObject.h"
#import "Home.h"

@interface SettingOnCloud()

@property (nonatomic, retain) NSDate   * creation_date;
@property (nonatomic, retain) NSNumber * debug;
@property (nonatomic, retain) NSSet *notifiche_lette;

@end

@implementation SettingOnCloud

@dynamic debug;
@dynamic creation_date;
@dynamic notifiche_lette;

//=========================
// INIT
//=========================
#pragma mark - INIT
+ (void)initialize {
    if (self == [SettingOnCloud class]) {
        DataObject *transformer = [[DataObject alloc] init];
        [NSValueTransformer setValueTransformer:transformer forName:@"DataObject"];
    }
}

//==================
// CREA
//==================
#pragma mark - CREA
+(SettingOnCloud*)createSettings{
    // NSLog(@"createSettings");
    NSManagedObjectContext *context = [AppDelegate mainManagedObjectContext];

    //Creiamo un'istanza di NSManagedObject per l'Entità che ci interessa
    SettingOnCloud *new_settings = [NSEntityDescription insertNewObjectForEntityForName:@"SettingOnCloud" inManagedObjectContext:context];
    
    //Usando il Key-Value Coding Parsing inseriamo i dati presi dal servizio Web
    [new_settings setCreation_date:[NSDate date]];
    [new_settings setNotifiche_lette:[NSSet new]];
    [new_settings changeDebug:[[NSUserDefaults standardUserDefaults] boolForKey:IBREVIARY_DEBUG_MODE]];
    
    [AppDelegate saveContext];
    return new_settings;
}



//==================
// SAVE
//==================
#pragma mark - SAVE
-(void)saveSettings{
    // NSLog(@"savingsettings...%@",self);
    [AppDelegate saveContext];
}

//===============================
// runOnMainQueueWithoutDeadlocking
//===============================
void runOnMainQueueWithoutDeadlockingSettings(void (^block)(void))
{
    if ([NSThread isMainThread])
    {
        block();
    }
    else
    {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
};

-(void)changeDebug:(BOOL)d{
    self.debug = d?@1:@0;
}

+(NSSet*)getNotificheLette{
    SettingOnCloud *s = [SettingOnCloud getSettings];
    NSSet *set = [s notifiche_lette];
    return set;
}

-(void)updateSet:(NSSet*)changeSet{
    if(changeSet.count>0){
        NSMutableSet *merge_set = [NSMutableSet setWithSet:self.notifiche_lette];
        [merge_set addObjectsFromArray:[changeSet allObjects]];
        [self setNotifiche_lette:[NSSet setWithSet:merge_set]];
        NSLog(@"SettingOnCLoud:/n- notifiche set:%@",self.notifiche_lette);
        [self saveSettings];
    }
}

+(void)setNotificaLetta:(NotificaObject*)n{
    SettingOnCloud *s = [SettingOnCloud getSettings];
    NSMutableSet *d = [NSMutableSet new];
    if(s.notifiche_lette.count>0){
        d = [[NSMutableSet alloc] initWithSet:s.notifiche_lette copyItems:YES];
        [d addObject:n.id_notifica];
    }
    s.notifiche_lette = [[NSSet alloc] initWithSet:d];
    [Home aggiornaBadge];
}


+(void)resetSettings{
//    [SettingOnCloud updateNotificheRICORRENTI:@"reset_ricorrenti"];
//    [SettingOnCloud updateNotificheISTANTANEE:[NSNumber numberWithInt:-1]];
//    [SettingOnCloud changeDebug:[[NSUserDefaults standardUserDefaults] boolForKey:IBREVIARY_DEBUG_MODE]];
}


NSTimer *icloudSettingsTimer;

//DOCUMENT DIRECTORY
+ (NSString *)documentDir {
    NSArray* documentPathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* docPath = [documentPathArray lastObject];
    // NSLog(@"DocDir:%@",docPath);
    return docPath;
}

//CACHE DIRECTORY
+ (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}


//==============================
// GET SETTINGs
//==============================
+(SettingOnCloud*)getSettings{
    NSManagedObjectContext *context = [AppDelegate mainManagedObjectContext];
    NSArray *array = nil;
    NSError *error = nil;
    NSFetchRequest *request = [NSFetchRequest new];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"SettingOnCloud" inManagedObjectContext:context];
    
    [request setEntity:entity];
    [request setIncludesPendingChanges:NO];
    [request setReturnsObjectsAsFaults:NO];
    array = [NSArray arrayWithArray:[context executeFetchRequest:request error:&error]];
    if(error || array==nil){
        NSLog(@"SettingOnCloud Errore request[%@] %@", request, [error description]);
    }
    
    SettingOnCloud *settings = nil;
    if(array.count == 1){
        settings = [array objectAtIndex:0];
    }
    
    if(array.count==0){
        settings = [SettingOnCloud createSettings];
    }
    
    else if(array.count>1){
        NSMutableSet *merge = [NSMutableSet new];
        settings = [array objectAtIndex:0];
        for (NSInteger i=1;i<array.count;i++) {
                SettingOnCloud*row = [array objectAtIndex:i];
                [merge addObjectsFromArray:row.notifiche_lette.allObjects];
                [context deleteObject:row];
                row = nil;
                }
        [settings updateSet:merge];
    }

    
    return settings;
}

+(void)commitChanges{
    SettingOnCloud *settings = [SettingOnCloud getSettings];
    BOOL trovateDifferenza = NO;
    for(NSNumber* not_id in settings.notifiche_lette) {
        NotificaObject *n = [NotificaObject notificaConId:not_id];
        if(n && n.letto.integerValue<=0){
            n.letto = @1;
            [n save];
            trovateDifferenza = YES;
        }
    }
    
    if(trovateDifferenza){
        [Home aggiornaBadge];
    }
}

+(NSArray*)getSettingsPrivateList{
    // NSLog(@"getSettingsPrivateList");
    NSManagedObjectContext *context = [AppDelegate mainManagedObjectContext];
    NSArray *array = nil;
    NSError *error = nil;
    NSFetchRequest *request = [NSFetchRequest new];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"SettingOnCloud" inManagedObjectContext:context];
    
    [request setEntity:entity];
    [request setIncludesPendingChanges:YES];
    array = [NSArray arrayWithArray:[context executeFetchRequest:request error:&error]];
    if(error || array==nil){
        NSLog(@"SettingOnCloud Errore request[%@] %@", request, [error description]);
    }

    
    return array;
}

+(NSArray*)getSettingsDuringInsetContext:(NSManagedObjectContext *)context{
    // NSLog(@"getSettingsDuringInsetContext");
    NSArray *array = nil;
    NSError *error = nil;
    NSFetchRequest *request = [NSFetchRequest new];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"SettingOnCloud" inManagedObjectContext:context];
    
    [request setEntity:entity];
    [request setIncludesPendingChanges:YES];
    array = [NSArray arrayWithArray:[context executeFetchRequest:request error:&error]];
    if(error || array==nil){
        NSLog(@"SettingOnCloud Errore request[%@] %@", request, [error description]);
    }
    return array;
}


//==========================
// PUBLIC METHOD
//==========================
#pragma mark - PUBLIC METHOD
+(NSSet*)getSetIdNotificheLette{
    NSLog(@"**Setting On cloud da controllare!!**");
    SettingOnCloud *s = [SettingOnCloud getSettings];
    return s.notifiche_lette;
}

+(BOOL)isDebug{
    SettingOnCloud *s = [SettingOnCloud getSettings];
    return s.debug.integerValue > 0;
}

+(void)changeDebug:(BOOL)d{
    SettingOnCloud *s = [SettingOnCloud getSettings];
    [s changeDebug:d];
}

+(void)updateSet:(NSSet*)changeSet{
    SettingOnCloud *s = [SettingOnCloud getSettings];
    [s updateSet:changeSet];
};


@end
