//
//  Day.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import "Day.h"
#import "Fix.h"
#import "Home.h"
#import "MissalObject.h"
#import "BreviarioObject.h"
#import "TestoProprio.h"
#import "SystemData.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
#import <AVFoundation/AVFoundation.h>
#endif

//#define URL_DAY @"http://www.ibreviary.com/service/services.php?s=get_daily_text"
#define URL_DAY @"http://ibreviary.com/day_with_yesterday_compieta.php?"


@implementation Day

//DAY_STRUCTURE
@synthesize id_date, filename, lingua, linguaGiorno, week;

//DAY_INFO
@synthesize date, type, type_home, title, subtitle, event, day_type, day_time, day_label,complete;
//SPEACH VOICE
@synthesize voice;

//BREVIARIO
@synthesize breviario;

//MISSAL
@synthesize missal;

//SANTO
@synthesize santo_array, santo_bool;
    
//EVENTO TP
@synthesize tp_breviario, tp_missal;
    
//EVENTO HOME TP
@synthesize tpHome_breviario, tpHome_missal;

//YESTERDAY COMPLINE
@synthesize compline_yesterday;


//=================
// INIT
//=================
#pragma mark - INIT
- (id)init {
    // NSLog(@"Day INIT");
    if ((self = [super init])){
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary*)json_day{
    if ((self = [super init])){
        // NSLog(@"day initWithDictionary");
        //default
        NSUserDefaults *defaults = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];

        //->giorno Settimana
        FullDateFormatter *dateFormatter = [FullDateFormatter new];
        
        // get NSDate from old string format
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *data_giorno = [dateFormatter dateFromString:[json_day objectForKey:@"date_day"]];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        NSDateComponents *weekdayComponents =[gregorian components:NSWeekdayCalendarUnit fromDate:data_giorno];
        NSInteger weekday_calendar = 2; //inizializzo var a generica
        weekday_calendar = [weekdayComponents weekday];
        
        //==============
        // DAY_INFO
        //==============
        NSMutableDictionary *day_info = [json_day objectForKey:@"day_info"];
        // NSLog(@"day_info = %@",day_info);

        self.date = [day_info objectForKey:@"date"];
        self.day_label = [day_info objectForKey:@"date"];
        self.complete = [day_info objectForKey:@"complete"]?[[day_info objectForKey:@"complete"] boolValue]:NO;
        // NSLog(@"complete:%i json:%@",self.complete,[day_info objectForKey:@"complete"]);
        self.title = [day_info objectForKey:@"title"];
        self.code = [day_info objectForKey:@"code"];
        self.subtitle = [day_info objectForKey:@"subtitle"];
        self.day_time = [day_info objectForKey:@"day_time"];
        self.day_type = [day_info objectForKey:@"day_type"];
        
        self.type_home = @"";
        self.type = @"generic";
        self.event = [day_info objectForKey:@"event"]?[NSArray arrayWithArray:[day_info objectForKey:@"event"]]:[NSArray new];
        // NSLog(@"DAY_INFO_ok");
        self.voice = nil;
        
        //==============
        // DAY_STRUCTURE
        //==============
        self.id_date = [json_day objectForKey:@"date_day"];
        self.lingua = [json_day objectForKey:@"lang"];
        self.linguaGiorno = [LinguaObject getLinguaBySigla:self.lingua];
        self.week = weekday_calendar;
        
        //===================
        // COMPLINE YESTERDAY
        //===================
        self.compline_yesterday = [json_day objectForKey:@"compline_yesterday"]?[[Item alloc] initWithDictionary:[json_day objectForKey:@"compline_yesterday"]]:nil;
        
        //==============
        // FILENAME
        //==============
        if(self.event.count>0)
        {
        if(![[defaults objectForKey:EVENT_FLAG] isEqualToString:@""])
                    {
                    for (NSString* item in self.event)
                            if ([item rangeOfString:[defaults objectForKey:EVENT_FLAG]].location != NSNotFound) self.type = item;
                    }
            
        NSArray *homeTP = [TestoProprio getTestiPropriForHome];
                    {
                    for(NSString* home_item in homeTP)
                        for(NSString* item in self.event)
                            if([item rangeOfString:home_item].location != NSNotFound)
                                    self.type_home = home_item;
                    }
        }//fine event count > 0

        // NSLog(@"type:%@ home_type:%@",self.type,self.type_home);

        self.filename = [NSString stringWithFormat:@"%@_%@_%@_%ld_%@_%i",self.id_date,self.lingua,type,(long)self.week,self.type_home,self.complete];
        // NSLog(@"DAY_STRUCTURE_ok");
        
        //===========================
        // DELETE INCOMPLETE IF EXIST
        //===========================
        if(self.complete)
        {
            NSString *delete = [NSString stringWithFormat:@"%@_%@_%@_%ld_%@_0",self.id_date,self.lingua,type,(long)self.week,self.type_home];
            NSError *err = nil;
            if([Day deleteDayWithName:delete andError:&err]){
                NSLog(@"Day: file incompleto:%@ cancellato.",delete);
            }
            
            if(!err){
                NSLog(@"Day: file incompleto:%@ non esistente.",delete);
            }
            else
            {
                NSLog(@"Day: errore cancellando %@:%@",delete,[err description]);
            }
        }
        
        //==============
        // SANTO
        //==============
        NSMutableArray *saint = [json_day objectForKey:@"saint"];
        // NSLog(@"SANTO:%i b:%@ m:%@",saint.count==0, ([json_day objectForKey:@"breviary_memory"]!=nil)?[json_day objectForKey:@"breviary_memory"]:@"NO", ([json_day objectForKey:@"missal_memory"]!=nil)?[json_day objectForKey:@"missal_memory"]:@"NO");
        
        // NSLog(@"JSON:%@",json_day);
        

        self.santo_bool = (saint && saint.count);
        self.santo_array = [NSMutableArray new];
        
        if(saint.count==0 && [json_day objectForKey:@"breviary_memory"] && [[json_day objectForKey:@"breviary_memory"] count]>0)
        {
            self.santo_bool = YES;
            NSMutableDictionary *jsonFakesanto = [NSMutableDictionary new];
            [jsonFakesanto setObject:@"0" forKey:@"id"];
            [jsonFakesanto setObject:[[[json_day objectForKey:@"breviary_memory"] objectAtIndex:0] objectForKey:@"type"] forKey:@"type"];
            [jsonFakesanto setObject:[[[json_day objectForKey:@"breviary_memory"] objectAtIndex:0] objectForKey:@"title"] forKey:@"name"];
            // NSLog(@"jsonFakesanto:%@",jsonFakesanto);
            saint = [NSMutableArray new];
            [saint addObject:jsonFakesanto];
            
            // NSLog(@"saint:%@",saint);
            
        }
        
        for(NSMutableDictionary *row in saint)
        {
            // NSLog(@"SANTOrow:%@ - memory:%@",row,[json_day objectForKey:@"breviary_memory"]);
            
            //BREVIARIO MEMORY SANTO
            NSDictionary *json_breviario_memory = nil;
            if([json_day objectForKey:@"breviary_memory"])
            {
                    for (NSUInteger i = 0; i<[[json_day objectForKey:@"breviary_memory"] count];i++)
                    {
                        NSDictionary *breviario_memory_row = [[json_day objectForKey:@"breviary_memory"] objectAtIndex:i];
                        if (([breviario_memory_row objectForKey:@"saint_id"] && [[json_day objectForKey:@"breviary_memory"] count]==1) ||
                            ([breviario_memory_row objectForKey:@"saint_id"]!=[NSNull null] && [[breviario_memory_row objectForKey:@"saint_id"] integerValue] == [[row objectForKey:@"id"] integerValue]))
                        {
                            //INSERISCO BREVIARIO SANTO
                            json_breviario_memory = breviario_memory_row;
                            i= [[json_day objectForKey:@"breviary_memory"] count];
                        }
                    }
            }//fine BREVIARIO SANTO
            
            // NSLog(@"Missal Santo step");
            //MISSAL MEMORY SANTO
            NSDictionary *json_missal_memory = nil;
            
            if([json_day objectForKey:@"missal_memory"])
            {
                    for (int i = 0; i<[[json_day objectForKey:@"missal_memory"] count];i++)
                    {
                        NSDictionary *missal_memory_row = [[json_day objectForKey:@"missal_memory"] objectAtIndex:i];
                
                        if (([missal_memory_row objectForKey:@"saint_id"] && [[json_day objectForKey:@"missal_memory"] count]==1) ||
                            [[missal_memory_row objectForKey:@"saint_id"] integerValue] == [[row objectForKey:@"id"] integerValue])
                        {
                            //INSERISCO MISSAL SANTO
                            json_missal_memory = missal_memory_row;
                            i= (int)[[json_day objectForKey:@"missal_memory"] count];
                        }
                    }
            }//fine MISSAL SANTO

        // NSLog(@"Json_missal_memory:%@",json_missal_memory);
         [self.santo_array addObject:[[SantoObject alloc] initWithDictionary:row andBreviarioMemory:json_breviario_memory andMissalMemory:json_missal_memory]];
        
            // NSLog(@"self.santo_array:%@",self.santo_array);
            // NSLog(@"DAY_SANTO:%@",self.santo_array);
            //==FINE SANTO
        }
        
        

        //=====================
        // BREVIARIO DEL GIORNO
        //=====================
        NSMutableDictionary *breviary_json = [json_day objectForKey:@"breviary"];
        [breviary_json setObject:@"-1" forKey:@"saint_id"];
        [breviary_json setObject:self.title forKey:@"title"];
        [breviary_json setObject:[NSNumber numberWithBool:self.complete] forKey:@"complete"];
        self.breviario = [[BreviarioObject alloc] initWithDictionary:breviary_json];
        
        //=====================
        // MESSALE DEL GIORNO
        //=====================
        self.missal = [NSMutableArray new];
        NSArray *missal_array = [json_day objectForKey:@"missal"];
        for(NSMutableDictionary *missal_json in missal_array){
            [missal_json setObject:self.title forKey:@"title"];
            [missal_json setObject:@"-1" forKey:@"saint_id"];
            [missal addObject:[[MissalObject alloc] initWithDictionary:missal_json]];
        }
        // NSLog(@"MISSAL_ARRAY:%@",self.missal);
        
        //=======
        // VOICE
        //=======
        [self checkVoice];

        
        //=====================
        // TP LIST
        //=====================
        NSMutableArray *eventList = [json_day objectForKey:@"event"];
        
        //inizializzo varibili del day
        self.tp_breviario = nil;
        self.tp_missal = nil;
        self.tpHome_breviario = nil;
        self.tpHome_missal = nil;
        
        for(NSDictionary *rowEvent in eventList){
            //ROW EVENT PRESENT
            NSString *titolo_event = [[rowEvent objectForKey:@"day_info"] objectForKey:@"title"];
            NSString *type_event = [[rowEvent objectForKey:@"day_info"] objectForKey:@"type"];
            NSString *subtitle_event = [[rowEvent objectForKey:@"day_info"] objectForKey:@"subtitle"];
            // NSLog(@"subtitle_event:%@->perHome:%i\ntype home:%@",type_event,[TestoProprio isForHomeTPwithKey:self.type],self.type_home);
            //AGGIUNGO EVENTUALE TESTO PROPRIO
            
            if([type_event isEqualToString:self.type])
                {
                //Breviary
                NSMutableDictionary *b_tp = [rowEvent objectForKey:@"breviary"];
                [b_tp setObject:titolo_event forKey:@"title"];
                [b_tp setObject:type_event forKey:@"type"];
                [b_tp setObject:type_event forKey:@"id_event"];
                [b_tp setObject:subtitle_event forKey:@"subtitle"];
                [b_tp setObject:[NSNumber numberWithBool:self.complete] forKey:@"complete"];
                self.tp_breviario = [[BreviarioObject alloc] initWithDictionary:b_tp];
                
                //Missal
                NSMutableDictionary *m_tp = [rowEvent objectForKey:@"missal"];
                [m_tp setObject:titolo_event forKey:@"title"];
                [m_tp setObject:type_event forKey:@"type"];
                [m_tp setObject:type_event forKey:@"id_event"];
                [m_tp setObject:subtitle_event forKey:@"subtitle"];

                self.tp_missal = [[MissalObject alloc] initWithDictionary:m_tp];
                }
            
            //AGGIUNGO EVENTUALE TESTO PROPRIO x HOME
                else if([type_event isEqualToString:self.type_home])
                {
                //Breviary TP 4 Home
                NSMutableDictionary *hb_tp = [rowEvent objectForKey:@"breviary"];
                [hb_tp setObject:titolo_event forKey:@"title"];
                [hb_tp setObject:type_event forKey:@"type"];
                [hb_tp setObject:type_event forKey:@"id_event"];
                [hb_tp setObject:[NSNumber numberWithBool:self.complete] forKey:@"complete"];
                self.tpHome_breviario = [[BreviarioObject alloc] initWithDictionary:hb_tp];
                
                //Missal TP 4 Home
                NSMutableDictionary *hm_tp = [rowEvent objectForKey:@"missal"];
                [hm_tp setObject:titolo_event forKey:@"title"];
                [hm_tp setObject:type_event forKey:@"type"];
                [hm_tp setObject:type_event forKey:@"id_event"];
                self.tpHome_missal = [[MissalObject alloc] initWithDictionary:hm_tp];
                
                }


        }//FINE ROWEVENT FOR
        // NSLog(@"DAY_NEW_FINE:%@",self);
    }
    return self;
}

//=========================
// ISTANCE METHOD
//=========================
#pragma mark - ISTANCE METHOD
- (BOOL)save{
    NSString *dayName = self.filename;
    NSString *dayPath = [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_DAY,dayName]];
    // NSLog(@"dayPath..saving...:%@",dayName);
    NSData *dataDaSalvare = [NSKeyedArchiver archivedDataWithRootObject:self];
    
    BOOL success = [dataDaSalvare writeToFile:dayPath atomically:YES];
    if(success){
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:dayPath]];
    }
    return success;
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    NSError *error = nil;
    BOOL success = [URL setResourceValue:[NSNumber numberWithBool:YES]
                                  forKey:NSURLIsExcludedFromBackupKey
                                   error: &error];
    if(!success){
        NSLog(@"Day: addSkipBackupAttributeToItemAtURL: Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

- (BOOL)saveWithError:(NSError**)e{
    NSString *dayName = self.filename;
    if(![[NSFileManager defaultManager] fileExistsAtPath:[self.cacheDir stringByAppendingPathComponent:PATH_DAY]])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:[self.cacheDir stringByAppendingPathComponent:PATH_DAY] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *dayPath = [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_DAY,dayName]];
    // NSLog(@"dayPath..saving...:%@",dayName);
    NSData *dataDaSalvare = [NSKeyedArchiver archivedDataWithRootObject:self];
    
    BOOL success = [dataDaSalvare writeToFile:dayPath options:NSDataWritingAtomic error:e];
    if(success){
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:dayPath]];
    }
    return success;
}

-(void)checkVoice{
    // NSLog(@"init_checkVoice:%@",self.lingua);
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000

    NSArray *casiparticolari = @[@"it",@"la",@"ra",@"vt"];
    for (NSString*row in casiparticolari) {
        if([self.lingua isEqualToString:row])
            {
            self.voice = @"it-IT";
            // NSLog(@"voiceFound:%@",self.voice);
            return;
            }
    }
    
    NSMutableArray *risposta= [NSMutableArray new];
    
    NSArray *a = [AVSpeechSynthesisVoice speechVoices];
    for (AVSpeechSynthesisVoice *v in a)
    {
        // NSLog(@"testing[%@]->%@",self.lingua,v.language);
        if ([v.language rangeOfString:self.lingua].location != NSNotFound) {
            // NSLog(@"found voice ->%@",v.language);
            [risposta addObject:v.language];
        }
    }
    
    if(risposta.count == 1)
    {
        self.voice = [risposta objectAtIndex:0];
        // NSLog(@"voiceFound:%@",self.voice);
    }
    else{
       // NSLog(@"Multivoice:%@",risposta);
    }
    
#endif
}

-(UIColor*)colorWeek{
    switch (self.week) {
        case 1:
            //DOMENICA
            return [UIColor colorWithRed:227/255.f green:23/255.f blue:10/255.f alpha:0.80];
            break;
        case 2:
            //LUNEDI'
            return [UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79];
            break;
        case 3:
            //MARTEDI'
            return [UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79];
            break;
        case 4:
            //MERCOLEDI'
            return [UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79];
            break;
        case 5:
            //GIOVEDI'
            return [UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79];
            break;
        case 6:
            //VENERDI'
            return [UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79];
            break;
        case 7:
            //SABATO
            return [UIColor colorWithRed:190/255.f green:14/255.f blue:3/255.f alpha:0.85];
            break;
            
        default:
        break;
        }
    
    return [UIColor clearColor];
}

-(UIColor*)messaColorWeek{
    switch (self.week) {
        case 1:
            //DOMENICA
            return [UIColor colorWithRed:227/255.f green:23/255.f blue:10/255.f alpha:0.80];
            break;
        case 2:
            //LUNEDI'
            return [UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79];
            break;
        case 3:
            //MARTEDI'
            return [UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79];
            break;
        case 4:
            //MERCOLEDI'
            return [UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79];
            break;
        case 5:
            //GIOVEDI'
            return [UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79];
            break;
        case 6:
            //VENERDI'
            return [UIColor colorWithRed:133/255.f green:103/255.f blue:87/255.f alpha:0.79];
            break;
        case 7:
            //SABATO
            return [UIColor colorWithRed:190/255.f green:14/255.f blue:3/255.f alpha:0.85];
            break;
            
        default:
            break;
    }
    
    return [UIColor clearColor];
}



-(void)aggiornaListaDownload{
    // NSLog(@"day:NOT->aggiornaListaDownload!");
    [self performSelectorOnMainThread:@selector(aggiornaListaDownloadMM) withObject:nil waitUntilDone:YES];
}

-(void)aggiornaListaDownloadMM{
    [[NSNotificationCenter defaultCenter] postNotificationName:AGGIORNA_LISTA_DOWNLOAD object:nil];
    
}



-(NSMutableArray*)listaBreviari{
    // NSLog(@"listaBreviari:%@",self);
    NSMutableArray *breviary_list = [NSMutableArray new];
    
    //Priorita 1 = TP
    if(self.tp_breviario) [breviary_list addObject:self.tp_breviario];

    //Priorita 2 = Memorie Obbligatorie
    for (SantoObject *s in self.santo_array) {
        // NSLog(@"santo_Breviary:%@",s.breviario);
                    if(s.breviario && s.breviario.obbligatorio)[breviary_list addObject:s.breviario];
    }
    
    //Priorita 3 = breviario del giorno
    if(self.breviario)[breviary_list addObject:self.breviario];

    //Priorita 4 = Non è Memorie Obbligatoria
    for (SantoObject *s in self.santo_array) {
        if(s.breviario && !s.breviario.obbligatorio)[breviary_list addObject:s.breviario];
    }
    
    return breviary_list;
}

-(BOOL)breviarioIsEmpty{
    
    BOOL risposta = NO;
    risposta = (breviario.officeofreading.contenuto == nil || [breviario.officeofreading.contenuto isEqualToString:@""]) && (breviario.lauds.contenuto == nil || [breviario.lauds.contenuto isEqualToString:@""]) && (breviario.daytimeprayers.contenuto == nil || [breviario.daytimeprayers.contenuto isEqualToString:@""]) && (breviario.vespers.contenuto == nil || [breviario.vespers.contenuto isEqualToString:@""]) && (breviario.compline.contenuto == nil || [breviario.compline.contenuto isEqualToString:@""]);
    
    return risposta && [[self listaBreviari] count]<=1;
}


    
-(NSMutableArray*)listaMissals{
    NSMutableArray *missal_list = [NSMutableArray new];
    
    //Priorita 1 = TP
    if(self.tp_missal) [missal_list addObject:self.tp_missal];

    //Priorita 2 = Memorie Obbligatorie
    for (SantoObject *s in self.santo_array) {
        if(s.missal && s.missal.obbligatorio)[missal_list addObject:s.missal];
    }
    
    //Priorita 3 = missal normal
    for (MissalObject *normal in self.missal) {
                        [missal_list addObject:normal];
    }

     //Priorita 4 = Non è Memorie Obbligatoria
     for (SantoObject *s in self.santo_array) {
         if(s.missal && !s.missal.obbligatorio)[missal_list addObject:s.missal];
     }
    // NSLog(@"listaMissals:%@",missal_list);
    return missal_list;
}

-(int)intForBreviaryFromMissalPos:(int)missal_pos {
    NSMutableArray *listaBreviari = [self listaBreviari];
    if(listaBreviari.count <=1) return 0;
    MissalObject *m = [[self listaMissals] objectAtIndex:missal_pos];
    for(int breviary_pos = 0; breviary_pos < listaBreviari.count;breviary_pos++)
    {
        BreviarioObject *b = [listaBreviari objectAtIndex:breviary_pos];
        if(m.id_santo != -1 && b.id_santo == m.id_santo) return breviary_pos;
        else if(m.id_event!=nil && [m.id_event isEqualToString:b.id_event]) return breviary_pos;
        else if([m.title isEqualToString:b.title]) return breviary_pos;

    }

    return 0;
}

-(int)intForMissalFromBreviaryPos:(int)breviary_pos {
    NSMutableArray *listaMissal = [self listaMissals];
    if(listaMissal.count <=1) return 0;
    BreviarioObject *b = [[self listaBreviari] objectAtIndex:breviary_pos];
    for(int missal_pos = 0; missal_pos < listaMissal.count;missal_pos++)
    {
        MissalObject *m = [listaMissal objectAtIndex:missal_pos];
        if(b.id_santo != -1 && b.id_santo == m.id_santo) return missal_pos;
        else if(b.id_event!=nil && [b.id_event isEqualToString:m.id_event]) return missal_pos;
        else if([b.title isEqualToString:m.title]) return missal_pos;
        
    }
    
    return 0;
}


-(BOOL)hasHomeTP{
    // NSLog(@"hasHomeTP:%@",self.type_home);
    return (self.type_home !=nil && ![self.type_home isEqualToString:@""]);
}


-(Item*)listaLettureAggregate:(MissalObject*)m{
    //->LETTURE AGGREGATE
    NSString *content_letture = @"";
    
    if(m.first_reading.contenuto!=nil)
        content_letture = [content_letture stringByAppendingString:m.first_reading.contenuto];
    // NSLog(@"1)content_letture:%@",content_letture);

    if(m.responsorial_psalm.contenuto!=nil)
    content_letture = [content_letture stringByAppendingString:m.responsorial_psalm.contenuto];
    // NSLog(@"2)content_letture:%@",content_letture);

    if(m.second_reading.contenuto!=nil)
    content_letture = [content_letture stringByAppendingString:m.second_reading.contenuto];
    // NSLog(@"3)content_letture:%@",content_letture);

    if(m.gospel_acclamation.contenuto!=nil)
    content_letture = [content_letture stringByAppendingString:m.gospel_acclamation.contenuto];
    // NSLog(@"4)content_letture:%@",content_letture);

    if(m.gospel.contenuto!=nil)
    content_letture = [content_letture stringByAppendingString:m.gospel.contenuto];
    // NSLog(@"5)content_letture:%@",content_letture);

    if([content_letture isEqualToString:@""]) return nil;
    
    Item *letture = [Item new];
    //titolo Letture presente in testoFisso Relativo
    [letture setTitolo:[[Fix getFixForLang:self.lingua] readings_title]];
    if(m.title != nil) [letture setTitolo:[NSString stringWithFormat:@"%@ (%@)",letture.titolo,m.title]];
    else if(m.type != nil) [letture setTitolo:[NSString stringWithFormat:@"%@ (%@)",letture.titolo,m.type]];
    [letture setContenuto:content_letture];
    
    return letture;
    
}


-(NSMutableArray*)listaLetture:(MissalObject*)m{
    // NSLog(@"listaLetture");
    NSMutableArray *lista = [NSMutableArray new];
    
    if(m.antiphon_and_opening_prayer!=nil && ![[m.antiphon_and_opening_prayer contenuto] isEqualToString:@""])
        [lista addObject:m.antiphon_and_opening_prayer];
    
    Item *lettureAgg = [self listaLettureAggregate:m];
    if(lettureAgg!=nil)[lista addObject:lettureAgg];
    
    if([m.prayer_of_the_faithful contenuto] && ![[m.prayer_of_the_faithful contenuto] isEqualToString:@""])
        [lista addObject:m.prayer_of_the_faithful];
    
    if([m.prayer_over_the_gifts contenuto] && ![[m.prayer_over_the_gifts contenuto] isEqualToString:@""])
        [lista addObject:m.prayer_over_the_gifts];
    
    if([m.preface contenuto] && ![[m.preface contenuto] isEqualToString:@""])
        [lista addObject:m.preface];
    
    if([m.antiphon_and_prayer_after_communion contenuto] && ![[m.antiphon_and_prayer_after_communion contenuto] isEqualToString:@""])
        [lista addObject:m.antiphon_and_prayer_after_communion];
    
    if(m.custom && [m.custom titolo] && ![[m.custom contenuto] isEqualToString:@""])
        [lista addObject:m.custom];

    
    return lista;

}

-(NSMutableArray*)lista4HomeTP{
    NSMutableArray *lista = [NSMutableArray new];
    if(self.tpHome_breviario)
    {
        if([self.tpHome_breviario.officeofreading contenuto] && ![[self.tpHome_breviario.officeofreading contenuto] isEqualToString:@""])
                        [lista addObject:self.tpHome_breviario.officeofreading];
        if([self.tpHome_breviario.lauds contenuto] && ![[self.tpHome_breviario.lauds contenuto] isEqualToString:@""])
        [lista addObject:self.tpHome_breviario.lauds];
        if([self.tpHome_breviario.daytimeprayers contenuto] && ![[self.tpHome_breviario.daytimeprayers contenuto] isEqualToString:@""])
        [lista addObject:self.tpHome_breviario.daytimeprayers];
        if([self.tpHome_breviario.vespers contenuto] && ![[self.tpHome_breviario.vespers contenuto] isEqualToString:@""])
        [lista addObject:self.tpHome_breviario.vespers];
        if([self.tpHome_breviario.compline contenuto] && ![[self.tpHome_breviario.compline contenuto] isEqualToString:@""])
        [lista addObject:self.tpHome_breviario.compline];

    
        if(lista.count>0)
            {
                Item * i = [Item new];
                [i setTitolo_home:NSLocalizedString(@"Breviary", nil)];
                [i setContenuto:nil];
                [lista insertObject:i atIndex:0];
            }
    }
    
    if(self.tpHome_missal)
    {
        Item * i = [Item new];
        [i setTitolo_home:NSLocalizedString(@"Lectures", nil)];
        [i setContenuto:nil];
        [lista addObject:i];
        MissalObject *m = self.tpHome_missal;
        
        if(![[m.antiphon_and_opening_prayer contenuto] isEqualToString:@""])
            [lista addObject:m.antiphon_and_opening_prayer];

        if(![[m.responsorial_psalm contenuto] isEqualToString:@""])
            [lista addObject:m.responsorial_psalm];
        
        //->LETTURE TP HOME
        NSString *content_letture = @"";
        content_letture = [content_letture stringByAppendingString:m.first_reading.contenuto];
        content_letture = [content_letture stringByAppendingString:m.responsorial_psalm.contenuto];
        content_letture = [content_letture stringByAppendingString:m.gospel_acclamation.contenuto];
        content_letture = [content_letture stringByAppendingString:m.gospel.contenuto];
        
        
        Item *letture = [Item new];
        [letture setTitolo:NSLocalizedString(@"Lectures", nil)];
        [letture setContenuto:content_letture];

        [lista addObject:letture];
        
        
        
        if([m.prayer_of_the_faithful contenuto] && ![[m.prayer_of_the_faithful contenuto] isEqualToString:@""])
            [lista addObject:m.prayer_of_the_faithful];
        
        if([m.prayer_over_the_gifts contenuto] && ![[m.prayer_over_the_gifts contenuto] isEqualToString:@""])
            [lista addObject:m.prayer_over_the_gifts];
        
        if([m.antiphon_and_prayer_after_communion contenuto] && ![[m.antiphon_and_prayer_after_communion contenuto] isEqualToString:@""])
            [lista addObject:m.antiphon_and_prayer_after_communion];
        
    }

    return lista;
}
//===========================
// Widget Richiesta Preghiere
//===========================
#pragma mark - WIDGET QUERY
-(Item*)getPray:(NSString*)pray{
    if([pray isEqualToString:@"ufficio"]) return self.breviario.officeofreading;
    if([pray isEqualToString:@"lodi"]) return self.breviario.lauds;
    if([pray isEqualToString:@"oraMedia"]) return self.breviario.daytimeprayers;
    if([pray isEqualToString:@"vespri"]) return self.breviario.vespers;
    if([pray isEqualToString:@"compieta"]) return self.breviario.compline;
    if([pray isEqualToString:@"vangelo"]){
        if(self.missal.count > 0)
        return [(MissalObject*)[self.missal objectAtIndex:0] gospel];
        }
    return nil;
}


//=========================
// CLASS METHOD
//=========================
#pragma mark - CLASS METHOD
//CACHE DIRECTORY
+ (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}

+ (NSString *)documentDir {
    NSArray* documentPathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* docPath = [documentPathArray lastObject];
    // NSLog(@"DocDir:%@",docPath);
    return docPath;
}


+(BOOL)fileExistsAtPath:(NSString*)path{
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

+(NSString*)versionApp{
    return (NSString *)[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
}

//DELETE
+(BOOL)deleteDayWithName:(NSString*)dayName
                andError:(NSError**)e{
    NSString *deletepath = [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_DAY,dayName]];
    NSFileManager *fm = [NSFileManager defaultManager];
    
    if(![fm fileExistsAtPath:deletepath]){
        
        return NO;
    }
    BOOL risposta = [fm removeItemAtPath:deletepath error:e];
    // NSLog(@"deleteDayWithName:\n%@\nexist:%i\nrisposta:%i",deletepath,[fm fileExistsAtPath:deletepath],risposta);
    
    return risposta;
}

//SEARCH
+(Day*)searchForDay:(NSString*)date_day
           withLang:(NSString *)lang{
        // NSLog(@"searchForDay:%@",date_day);
        if(lang==nil){
             NSUserDefaults *defaults = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
            lang = [defaults objectForKey:LINGUA];
        }
        NSArray *list = [Day getListDays];
        NSString *stringTosearch = [NSString stringWithFormat:@"%@_%@",date_day,lang];
    
        for(NSString *fileName in list)
        {
            if ([fileName rangeOfString:stringTosearch].location != NSNotFound)
                    {
                    // NSLog(@"trovato");
                    NSString *dayPath = [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_DAY,fileName]];
                    NSData *d = [[NSData alloc] initWithContentsOfFile:dayPath];
                    Day *day = [NSKeyedUnarchiver unarchiveObjectWithData:d];
                        
                    //verifica giorno is OK
                    if([day.title isEqualToString:@""] && !day.day_type && !day.day_time){
                        [Day deleteDayWithName:fileName andError:nil];
                        return nil;
                        }
                    return day;
                    }
        }
    
    
        return nil;
    }


+(NSMutableArray*)getSantPreviewForIndex:(NSIndexPath *)indexPath{
    NSMutableArray *risposta = [NSMutableArray new];
    // NSLog(@"getSantPreviewForIndex:%li",(long)indexPath.row);
    NSArray *listaDay = [Day getListDays];
    if(indexPath.row>=listaDay.count)
    {
        // NSLog(@"Day:ERROR: indexPath %li errato",(long)indexPath.row);
        return risposta;
    }
    NSString *dayPath = [[Day cacheDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_DAY,[[Day getListDays] objectAtIndex:indexPath.row]]];
    if(![self fileExistsAtPath:dayPath])
    {
        // NSLog(@"caricaGiornoWithIndex:ERROR: dayPath sbagliato %@",dayPath);
        return risposta;
    }
    
    NSData *data = [[NSData alloc] initWithContentsOfFile:dayPath];
    Day *day = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    // NSLog(@"test %lu\n%@",(unsigned long)day.santo_array.count,day);
    
    for (SantoObject* s in day.santo_array)
        if([s image])[risposta addObject:[s image]];
    
    return risposta;
}


+(NSArray*)getListDays{
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    return [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[[cachePathArray lastObject] stringByAppendingPathComponent:PATH_DAY] error:nil];
}

+(BOOL)isCurrentDay:(NSIndexPath*)indexPath{
    NSArray *listanomiGiorni = [Day getListDays];
    NSUserDefaults *de = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    NSString *giornoInUso = [de objectForKey:GIORNO_IN_USO];
    
    int i = (int)indexPath.row;
    if(i<0 || i > listanomiGiorni.count || !giornoInUso) return NO;
    
    NSString*giorno = [listanomiGiorni objectAtIndex:i];
    return [giorno isEqualToString:giornoInUso];
}

#pragma mark -
#pragma mark GIORNO IN USO
//=========================
// GIORNO IN USO
//=========================
+(Day*)caricaGiornoInUso{
    NSString *dayPath = [[Day documentDir] stringByAppendingPathComponent:GIORNO_IN_USO_DOCUMENT];
    Day *giorno_in_uso = [Day new];
    NSUserDefaults *defaults = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    if(![defaults boolForKey:AGGIORNAMENTO_2015]) return nil;
    if(![[NSFileManager defaultManager] fileExistsAtPath:dayPath])
    {
        FullDateFormatter *f = [FullDateFormatter new];
        [f setDateFormat:@"yyyy-MM-dd"];
        [Day scaricaGiornoconData:[f stringFromDate:[NSDate date]]
                            withLang:[defaults objectForKey:LINGUA]
                           eventFlag:[TestoProprio
            getTestoProprioWithKey:[defaults objectForKey:EVENT_FLAG]]
                    completionBlock:^(Day *day){
                                    // NSLog(@"scaricaGiornoconData Finito");
                                    [giorno_in_uso salvaGiornoInUso:nil];
                                    return giorno_in_uso;
                                    }];

    }
    else {
        NSData *data = [[NSData alloc] initWithContentsOfFile:dayPath];
        giorno_in_uso = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    return giorno_in_uso;
}

-(BOOL)salvaGiornoInUso:(NSError**)e{
    
    NSString *dayPath = [[Day documentDir] stringByAppendingPathComponent:GIORNO_IN_USO_DOCUMENT];
    NSData *dataDaSalvare = [NSKeyedArchiver archivedDataWithRootObject:self];
    BOOL salvato = [dataDaSalvare writeToFile:dayPath options:NSDataWritingAtomic error:e];
    
    if(salvato)
    {
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:dayPath]];
        NSUserDefaults *de = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
        for(int i=0; i < [[Day getListDays] count]; i++)
            if([self.filename isEqualToString:[[Day getListDays] objectAtIndex:i]])
                                            [de setInteger:i forKey:GIORNO_IN_USO_INDEX];
        [de setObject:self.lingua forKey:LINGUA];
        [de setObject:self.filename forKey:GIORNO_IN_USO];
        [de synchronize];
    }
    
    return salvato;
}

#pragma mark -
#pragma mark CHECK AUTO-OGGI
//=========================
// CHECK AUTO-OGGI
//=========================
+(void)checkAutoOggi:(App_type)t{
    NSUserDefaults *d = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    if(![d boolForKey:AUTODOWNLOAD_OGGI]){
        // NSLog(@"checkAutoOggi:NON Attivo");
        return;
    }
    switch (t) {
        case getting_background:
            // NSLog(@"checkAutoOggi:getting_background");
            break;
        case getting_foreground:
            // NSLog(@"checkAutoOggi:getting_foreground");
            break;
        case getting_active:
            // NSLog(@"checkAutoOggi:getting_active");
            break;
        default:
            break;
    }
    
    //autodownload attivo!
    FullDateFormatter *f = [FullDateFormatter new];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSString *date_string = [f stringFromDate:[NSDate date]];
    
    if(t==getting_background)
            date_string = [f stringFromDate:[[NSDate date] dateByAddingTimeInterval:(60*60*24)]];
    
    __block Day *day = [Day searchForDay:date_string withLang:[d objectForKey:LINGUA]];
    
    //lo visualizzo una Volta al giorno
    // NSLog(@"checkAutoOggi_attivo\ndate_string:%@\nAUTODOWNLOAD_OGGI_DATE:%@",date_string,[d objectForKey:AUTODOWNLOAD_OGGI_DATE]);
    if(t==getting_background || [d objectForKey:AUTODOWNLOAD_OGGI_DATE]==nil || ![[d objectForKey:AUTODOWNLOAD_OGGI_DATE] isEqualToString:date_string] || !day.complete)
        {
            
            
            //creo semaforo
            dispatch_semaphore_t sem = dispatch_semaphore_create(0);
            

            if(day==nil)
            {
                //il giorno non c'è
                [Day scaricaGiornoconData:date_string
                                   withLang:[d objectForKey:LINGUA]
                                  eventFlag:[TestoProprio getTestoProprioWithKey:[d objectForKey:EVENT_FLAG]]
                           completionBlock:^(Day *ddd){
                                                //>>>semaforo verde!
                                                day = ddd;
                                                dispatch_semaphore_signal(sem);
                                                return ddd;
                                                }];
            }
            else dispatch_semaphore_signal(sem);
            
            //aspetto semaforo verde <<<
            dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
            
            
            if(t!=getting_background && day!=nil){
                [d setObject:date_string forKey:AUTODOWNLOAD_OGGI_DATE];
                [d synchronize];
                // NSLog(@"Fine checkAutoOggi: oggi mostrato.");
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                                    [[Home sharedInstance] caricaGiornoWithDay:day.filename];
                                                    }];
            }
         }
}

#pragma mark -
#pragma mark SCARICA GIORNO
//=========================
// SCARICA GIORNO
//=========================
+(void)scaricaGiornoconData:(NSString*)date_day
                   withLang:(NSString *)lang
                  eventFlag:(TestoProprio*)tp
            completionBlock:(Day*(^)(Day *day))completion{
    NSUserDefaults *defaults = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    if(![defaults boolForKey:AGGIORNAMENTO_2015]){
        // NSLog(@"richiesta di scaricaGiornoconData errata...primo Aggiornamento");
        return;
    }
    dispatch_queue_t myQueue = dispatch_queue_create("ScaricaGiornoconData Class Method", NULL);
    dispatch_async(myQueue, ^{
        // Perform long running process
        // NSLog(@"Day: scaricaGiorno on BLOCK");
        
        //DOWNLOAD
        NSString *urlString = [NSString stringWithFormat:@"%@r=%@&v=%@&l=%@&d=%@",URL_DAY,[SystemData platformString],[Day versionApp], lang, [date_day stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_REQUEST];
        NSError *err = nil;
        NSLog(@"string:%@ url:%@",urlString ,url);
        NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err];
        if(err!=nil)
        {
            //detail o download dovranno controllare la presenza del file!
            NSLog(@"Day: ERR scaricando %@: %@",date_day,[err description]);
            completion(nil);
            return;
        }
        
        //FINE DOWNLOAD notifiche switchcase settate prima della chiamata!
        NSLog(@"Day: linkRequest:%@",url);
        err = nil;
        //NSMutableDictionary *dayPlist = [NSPropertyListSerialization propertyListFromData:data mutabilityOption:NSPropertyListImmutable format:&format errorDescription:&error];
        NSMutableDictionary *risposta_day = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err]];
        if(err){
            NSLog(@"Errore parsing Json Day");
            completion(nil);
        }
        
        NSLog(@"Day:FINE BLOCK DOWNLOAD");
        
        [risposta_day setObject:date_day forKey:@"date_day"];
        [risposta_day setObject:lang forKey:@"lang"];
        
        Day *day = [[Day alloc] initWithDictionary:risposta_day];
        // NSLog(@"day:%@",day);
        //salvo DAY nuovo
        err = nil;
        if ([day saveWithError:&err])
            if(err) {
                // NSLog(@"Errore saving Day:%@",[err description]);
                completion(nil);
                return;
            }
        
        completion(day);
    });
}

+(Day*)scaricaSYNCGiornoconData:(NSString*)date_day
                       withLang:(NSString *)lang
                      eventFlag:(TestoProprio*)tp{

        // Perform long running process
        // NSLog(@"Day: scaricaSYNCGiornoconData");
        
        //DOWNLOAD
        NSString *urlString = [NSString stringWithFormat:@"%@r=%@&v=%@&l=%@&d=%@",URL_DAY,[SystemData platformString],[Day versionApp], lang, [date_day stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_REQUEST];
        NSError *err = nil;
        // NSLog(@"string:%@ url:%@",urlString ,url);
        NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err];
        if(err!=nil)
        {
            //detail o download dovranno controllare la presenza del file!
            // NSLog(@"Day: ERR scaricando %@: %@",date_day,[err description]);
            return nil;
        }
        
        //FINE DOWNLOAD notifiche switchcase settate prima della chiamata!
        // NSLog(@"Day: linkRequest:%@",url);
        err = nil;
        //NSMutableDictionary *dayPlist = [NSPropertyListSerialization propertyListFromData:data mutabilityOption:NSPropertyListImmutable format:&format errorDescription:&error];
        NSMutableDictionary *risposta_day = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err]];
        if(err){
            // NSLog(@"Errore parsing Json Day");
            return nil;
        }
        
        // NSLog(@"Day:FINE BLOCK DOWNLOAD");
        
        [risposta_day setObject:date_day forKey:@"date_day"];
        [risposta_day setObject:lang forKey:@"lang"];
        
        Day *day = [[Day alloc] initWithDictionary:risposta_day];
        // NSLog(@"day:%@",day);
        //salvo DAY nuovo
        err = nil;
        if ([day saveWithError:&err])
            if(err) {
                // NSLog(@"Errore saving Day:%@",[err description]);
                return nil;
            }
    
        return day;
}


+(void)scaricaGiornoconData:(NSString*)date_day
                   withLang:(NSString *)lang
                  eventFlag:(TestoProprio*)tp
                andDelegate:(NSObject<SystemGetterProtocol> *)delegate{
    dispatch_queue_t myQueue = dispatch_queue_create("ScaricaGiornoconData Class Method", NULL);
    dispatch_async(myQueue, ^{
        // Perform long running process
        // NSLog(@"Day: scaricaGiorno && delegate");
        
        //DOWNLOAD
        NSString *urlString = [NSString stringWithFormat:@"%@r=%@&v=%@&l=%@&d=%@",URL_DAY,[SystemData platformString],[Day versionApp], lang, [date_day stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_REQUEST];
        NSError *err = nil;
        // NSLog(@"string:%@ url:%@",urlString ,url);
        NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err];
        if(err!=nil)
        {
            //detail o download dovranno controllare la presenza del file!
            // NSLog(@"Day: ERR scaricando %@: %@",date_day,[err description]);
            dispatch_async(dispatch_get_main_queue(),^{[delegate didFinishDownloadingDay:nil];});
            return;
        }
        
        //FINE DOWNLOAD notifiche switchcase settate prima della chiamata!
        // NSLog(@"Day: linkRequest:%@",url);
        err = nil;
        //NSMutableDictionary *dayPlist = [NSPropertyListSerialization propertyListFromData:data mutabilityOption:NSPropertyListImmutable format:&format errorDescription:&error];
        NSMutableDictionary *risposta_day = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err]];
        if(err){
            // NSLog(@"Errore parsing Json Day");
            dispatch_async(dispatch_get_main_queue(),^{[delegate didFinishDownloadingDay:nil];});
            return;
        }

        // NSLog(@"FINE DOWNLOAD");
        
        [risposta_day setObject:date_day forKey:@"date_day"];
        [risposta_day setObject:lang forKey:@"lang"];
        
        Day *day = [[Day alloc] initWithDictionary:risposta_day];
        // NSLog(@"day:%@",day);
        //salvo DAY nuovo
        err = nil;
        if ([day saveWithError:&err])
            if(err) {
                // NSLog(@"Errore saving Day:%@",[err description]);
                dispatch_async(dispatch_get_main_queue(),^{[delegate didFinishDownloadingDay:nil];});
                return;
            }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            [delegate didFinishDownloadingDay:day];
        });
    });
    
}

+(void)scaricaGiornoconData:(NSString*)date_day
                   withLang:(NSString *)lang
                  eventFlag:(TestoProprio*)tp
                andDelegate:(NSObject<SystemGetterProtocol> *)delegate
                   andError:(NSError**)errore{
    dispatch_queue_t myQueue = dispatch_queue_create("ScaricaGiornoconData Class Method", NULL);
    dispatch_async(myQueue, ^{
        // NSLog(@"Day: scaricaGiorno");
        //DOWNLOAD
        NSString *urlString = [NSString stringWithFormat:@"%@r=%@&v=%@&l=%@&d=%@",URL_DAY,[SystemData platformString],[Day versionApp], lang, [date_day stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_REQUEST];
        NSError *err = nil;
        // NSLog(@"string:%@ url:%@",urlString ,url);
        NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err];
        if(err!=nil){
            //detail o download dovranno controllare la presenza del file!
            // NSLog(@"Day: ERR scaricando %@: %@",date_day,[err description]);
            *errore = err;
            dispatch_async(dispatch_get_main_queue(),^{[delegate didFinishDownloadingDay:nil];});
            }
    
        //FINE DOWNLOAD notifiche switchcase settate prima della chiamata!
        // NSLog(@"Day: linkRequest:%@",url);
        err = nil;
        NSMutableDictionary *risposta_day = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err]];
        if(err){
            // NSLog(@"Errore parsing Json Day");
            dispatch_async(dispatch_get_main_queue(),^{[delegate didFinishDownloadingDay:nil];});
            }
    
        // NSLog(@"FINE DOWNLOAD");
    
        [risposta_day setObject:date_day forKey:@"date_day"];
        [risposta_day setObject:lang forKey:@"lang"];
    
        Day *day = [[Day alloc] initWithDictionary:risposta_day];
        // NSLog(@"day:%@",day);
        //salvo DAY nuovo
        err = nil;
        if ([day saveWithError:&err])
            if(err){
                // NSLog(@"Errore saving Day:%@",[err description]);
                dispatch_async(dispatch_get_main_queue(),^{[delegate didFinishDownloadingDay:nil];});
                }
    
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            [delegate didFinishDownloadingDay:day];
        });
    });

}

+(Day*)scaricaGiornoFirstconData:(NSString*)date_day
                   withLang:(NSString *)lang
                  eventFlag:(TestoProprio*)tp{
        NSLog(@"Day: scaricaGiorno First time!");
        //DOWNLOAD
        if(!lang) lang = NSLocalizedString(@"checklingua", nil);
        NSString *urlString = [NSString stringWithFormat:@"%@r=%@&v=%@&l=%@&d=%@",URL_DAY,[SystemData platformString],[Day versionApp], lang, [date_day stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_REQUEST];
        NSError *err = nil;
        // NSLog(@"string:%@ url:%@",urlString ,url);
        NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err];
        if(err!=nil){
            //detail o download dovranno controllare la presenza del file!
            // NSLog(@"Day: ERR scaricando %@: %@",date_day,[err description]);
            return nil;
        }
        
        //FINE DOWNLOAD notifiche switchcase settate prima della chiamata!
        // NSLog(@"Day: linkRequest:%@",url);
        err = nil;
        NSMutableDictionary *risposta_day = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err]];
        if(err){
            // NSLog(@"Errore parsing Json Day");
            return nil;
        }
        
        // NSLog(@"FINE DOWNLOAD");
        
        [risposta_day setObject:date_day forKey:@"date_day"];
        [risposta_day setObject:lang forKey:@"lang"];
        
        Day *day = [[Day alloc] initWithDictionary:risposta_day];
        // NSLog(@"day:%@",day);
        //salvo DAY nuovo
        err = nil;
        if ([day saveWithError:&err])
            if(err){
                // NSLog(@"Errore saving Day:%@",[err description]);
                return nil;
            }
        
    
        return day;
}


#pragma mark -
#pragma mark SETTIMANA
//=========================
// SETTIMANA
//=========================
+(void)scaricaSettimana:(NSString*)lang
                   eventFlag:(TestoProprio*)tp
             completionBlock:(void(^)(NSString *risposta))completion{
    //creo semaforo
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    
    
    __block NSString *risposta = @"";
    __block FullDateFormatter *dateFormatter = [FullDateFormatter new];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
    if([NSLocalizedString(@"checklingua", nil) isEqualToString:@"ar"]){
        NSArray* preferredLanguages = [NSLocale preferredLanguages];
        NSLog(@"preferredLanguages: %@", preferredLanguages);
                
    }

	NSDate *oggi = [NSDate date];
    for(int settimana_step = 1; settimana_step<8; settimana_step++){
		NSDate *giornoSettimana = [oggi dateByAddingTimeInterval:60*60*24*settimana_step];
		NSLog(@"scarico giorno[%d] lingua:%@ ->>> %@", settimana_step, lang,[dateFormatter stringFromDate:giornoSettimana]);
        [Day scaricaGiornoconData:[dateFormatter stringFromDate:giornoSettimana] withLang:lang  eventFlag:(TestoProprio*)tp completionBlock:^(Day *ddd){
                risposta = [risposta stringByAppendingFormat:@"%i",(ddd!=nil && ddd.complete)?1:0];
                [ddd aggiornaListaDownload];
                ddd = nil;
                if(risposta.length>=7){
                                    //>>>semaforo verde Fine download
                                    dispatch_semaphore_signal(sem);
                                }
                return ddd;
        }];

    }
    
    //aspetto semaforo verde <<<
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // Update the UI
        completion(risposta);
    });
}

//=======================================
//Encoding (FOR SAVING OBJECT)
//=======================================
#pragma mark - ENCODING
-(void)encodeWithCoder:(NSCoder*)decoder{
    
    //DAY_STRUCTURE
    [decoder encodeObject:self.id_date forKey:@"id_date"];
    [decoder encodeObject:self.filename forKey:@"filename"];
    [decoder encodeObject:self.lingua forKey:@"lingua"];
    [decoder encodeObject:self.linguaGiorno forKey:@"linguaGiorno"];
    [decoder encodeInt:(int)self.week forKey:@"week"];
    [decoder encodeBool:self.complete forKey:@"complete"];
    
    //LABEL_LIST
    [decoder encodeObject:self.day_label forKey:@"day_label"];

    //DAY_INFO
    [decoder encodeObject:self.title forKey:@"title"];
    [decoder encodeObject:self.date forKey:@"date"];
    [decoder encodeObject:self.type forKey:@"type"];
    [decoder encodeObject:self.type_home forKey:@"type_home"];
    [decoder encodeObject:self.subtitle forKey:@"subtitle"];
    [decoder encodeObject:self.event forKey:@"event"];
    [decoder encodeObject:self.day_type forKey:@"day_type"];
    [decoder encodeObject:self.day_time forKey:@"day_time"];
    [decoder encodeObject:self.code forKey:@"code"];
    //VOICE
    [decoder encodeObject:self.voice forKey:@"voice"];

    //BREVIARIO
    [decoder encodeObject:self.breviario forKey:@"breviario"];
  
    //MISSAL
    [decoder encodeObject:self.missal forKey:@"missal"];
    
    //SANTO
    [decoder encodeObject:self.santo_array forKey:@"santo_array"];
    [decoder encodeBool:self.santo_bool forKey:@"santo_bool"];
    
    //EVENTO TP
    [decoder encodeObject:self.tp_breviario forKey:@"tp_breviario"];
    [decoder encodeObject:self.tp_missal forKey:@"tp_missal"];
    
    //EVENTO HOME TP
    [decoder encodeObject:self.tpHome_breviario forKey:@"tpHome_breviario"];
    [decoder encodeObject:self.tpHome_missal forKey:@"tpHome_missal"];
    
    //YESTERDAY COMPLINE
    [decoder encodeObject:self.compline_yesterday forKey:@"compline_yesterday"];
    
}

//======================================
//Decoding (FOR READ SAVED OBJECT)
//======================================
#pragma mark - DECODING
- (id)initWithCoder: (NSCoder *)aCoder {
    self = [super init];
    if (self) {

        //DAY_STRUCTURE
        self.id_date = [aCoder decodeObjectForKey:@"id_date"];
        self.filename = [aCoder decodeObjectForKey:@"filename"];
        self.lingua = [aCoder decodeObjectForKey:@"lingua"];
        self.linguaGiorno = [aCoder decodeObjectForKey:@"linguaGiorno"];
        self.week = [aCoder decodeIntForKey:@"week"];
        self.complete = [aCoder decodeBoolForKey:@"complete"];
        
        //LABEL_LIST
        self.day_label = [aCoder decodeObjectForKey:@"day_label"];
        
        //DAY_INFO
        self.title = [aCoder decodeObjectForKey:@"title"];
        self.date = [aCoder decodeObjectForKey:@"date"];
        self.type = [aCoder decodeObjectForKey:@"type"];
        self.type_home = [aCoder decodeObjectForKey:@"type_home"];

        self.subtitle = [aCoder decodeObjectForKey:@"subtitle"];
        self.event = [aCoder decodeObjectForKey:@"event"];
        self.day_type = [aCoder decodeObjectForKey:@"day_type"];
        self.day_time = [aCoder decodeObjectForKey:@"day_time"];
        
        //VOICE
        self.voice = [aCoder decodeObjectForKey:@"voice"];

        //BREVIARIO
        self.breviario = [aCoder decodeObjectForKey:@"breviario"];
        
        //MISSAL
        self.missal = [aCoder decodeObjectForKey:@"missal"];
        
        //SANTO
        self.santo_array = [aCoder decodeObjectForKey:@"santo_array"];
        self.santo_bool = [aCoder decodeBoolForKey:@"santo_bool"];
        
        //EVENTO TP
        self.tp_breviario = [aCoder decodeObjectForKey:@"tp_breviario"];
        self.tp_missal = [aCoder decodeObjectForKey:@"tp_missal"];
        
        //EVENTO HOME TP
        self.tpHome_breviario = [aCoder decodeObjectForKey:@"tpHome_breviario"];
        self.tpHome_missal = [aCoder decodeObjectForKey:@"tpHome_missal"];
      
        //YESTERDAY COMPLINE
        self.compline_yesterday = [aCoder decodeObjectForKey:@"compline_yesterday"];

    }
    return self;
    
}


#pragma mark cacheDir
//CACHE DIRECTORY
- (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}

//==================
//// NSLog %@
//==================
#pragma mark - // NSLog
-(NSString*)description{
    return [NSString stringWithFormat:@"<day[%@] filename:%@ >",self.date,self.filename];
}




//=================
//END
//=================
@end

