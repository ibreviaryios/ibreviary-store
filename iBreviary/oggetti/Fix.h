//
//  Fix.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "LinguaObject.h"
#import "Item.h"

@interface Fix :  NSObject<NSCoding>{
    
}

//FIX_STRUCTURE
@property (strong, nonatomic) NSString *last_update;
@property (strong, nonatomic) NSString *lingua;
@property (strong, nonatomic) LinguaObject *linguaFix;
@property (strong, nonatomic) NSMutableArray *ancore_array;
@property (strong, nonatomic) NSString *readings_title;


//ORDER OF THE MASS
@property (strong, nonatomic) NSString *ordinario_title;
@property (readonly, nonatomic) Item *ordinario_aggregato;
@property (strong, nonatomic) Item *introductory_rites;
@property (strong, nonatomic) Item *eucharistic_liturgy;
@property (strong, nonatomic) Item *communion_rites;

//PRO MESSA
@property (strong, nonatomic) Item *celebration_communion_rites;
@property (strong, nonatomic) Item *celebration_conclusion_rites;


//PREFAZIO
@property (strong, nonatomic) NSString *prefazio_title;
@property (strong, nonatomic) NSMutableArray *prefazio_array;

//LITURGIA EUCARISTICA
@property (strong, nonatomic) NSString *liturgiaeucaristica_title;
@property (strong, nonatomic) NSMutableArray *liturgiaeucaristica_array;

//PREGHIERA DEI FEDELI
@property (strong, nonatomic) NSString *preghieredeifedeli_title;
@property (strong, nonatomic) NSMutableArray *preghieredeifedeli_array;

//BENEDIZIONI SOLENNI
@property (strong, nonatomic) NSString *benedizionisolenni_title;
@property (strong, nonatomic) NSMutableArray *benedizionisolenni_array;

//PRAYERS
@property (strong, nonatomic) NSMutableArray *prayers;

//RITI
@property (strong, nonatomic) NSMutableArray *riti;

//TESTI COMUNI BREVIARIO
@property (strong, nonatomic) NSString *last_update_common_breviary;
@property (strong, nonatomic) NSMutableArray *breviary_comuni_array;

//TESTI COMUNI MISSAL
@property (strong, nonatomic) NSString *last_update_common_missal;
@property (strong, nonatomic) NSMutableArray *missal_comuni_array;

//INIT
-(id)initWithDictionary:(NSDictionary*)json_fix;

// ISTANCE METHOD
-(BOOL)save;
//-(BOOL)saveWithError:(NSError**)e;
-(Item*)ordinarioDellaMessa;
-(NSMutableArray*)getCredoArray;

// CLASS METHOD
+(Fix*)getFixForLang:(NSString *)lang;
+(BOOL)checkComuniForLang:(NSString *)lang;

//DOWNLOAD FIX
+(Fix*)scaricaFixWithLang:(NSString *)lang andLastUpdate:(NSString*)lu;
+(void)forzaAggiornamentoTotale;
@end
