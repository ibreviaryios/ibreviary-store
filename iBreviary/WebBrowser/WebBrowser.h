//
//  WebBrowser.h
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

/*
    == FAQ ==
 
*/
//

#import "BaseController.h"

@interface WebBrowser : BaseController <UIWebViewDelegate, UIActionSheetDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, readonly) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIView *viewForIOS6;

@property (nonatomic, strong) IBOutlet UIView *titleToolbar;
@property (strong, nonatomic) IBOutlet UIView *bottomInfo_view;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *loadingIndicator;

@property (nonatomic, strong) IBOutlet UIButton *backButton;
@property (nonatomic, strong) IBOutlet UIButton *forwardButton;
@property (nonatomic, strong) IBOutlet UIButton *refreshButton; //diventa Stop!
@property (nonatomic, strong) IBOutlet UIButton *actionButton;
@property (nonatomic, strong) IBOutlet UIButton *doneButton;

@property (nonatomic, strong) IBOutlet UIButton *zoomIN;
@property (nonatomic, strong) IBOutlet UIButton *zoomOUT;


@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSString *html;
@property (nonatomic, strong) NSData *dataWeb;


+(void)showUrlWithString:(NSString*)url_string
        inViewController:(UIViewController *)ctrl;

+(void)showUrl:(NSURL*)url inViewController:(UIViewController *)ctrl;

@end
