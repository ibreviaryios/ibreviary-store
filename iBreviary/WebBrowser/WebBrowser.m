//
//  WebBrowser.h
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

/*
 == FAQ ==
 WebBrowser *browser = [WebBrowser new];
 [browser setUrl:[NSURL URLWithString:@"http:apple.it"]];
 [self presentViewController:browser animated:YES completion:nil];
 */
//


#import "WebBrowser.h"

@interface WebBrowser ()

@property (nonatomic) BOOL _firstRequest;
@property (nonatomic, strong) NSString *_dataMimeType;

@end

@implementation WebBrowser

// Public
@synthesize webView = _webView;
@synthesize titleLabel;
@synthesize loadingIndicator;
@synthesize url,viewForIOS6;
@synthesize html;
@synthesize dataWeb;
@synthesize titleToolbar,bottomInfo_view,backButton, forwardButton, refreshButton, actionButton, doneButton;
@synthesize zoomIN,zoomOUT;

// Private
@synthesize _firstRequest;
@synthesize _dataMimeType;

BOOL clicked;


#pragma mark - Utility
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

- (id)init {
    if ( (self = [super init]) ) {
        _firstRequest = YES;
    }
    return self;
}

#pragma mark - Caller
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
+(void)showUrl:(NSURL*)url inViewController:(UIViewController *)ctrl{
    WebBrowser *browser = [WebBrowser new];
    [browser setUrl:url];
    [ctrl presentViewController:browser animated:YES completion:nil];
}

+(void)showUrlWithString:(NSString*)url_string
        inViewController:(UIViewController *)ctrl{
    NSURL *url = [NSURL URLWithString:url_string];
    [WebBrowser showUrl:url inViewController:ctrl];
}

// StatusBar
//=========================
#pragma mark - StatusBar
- (BOOL)prefersStatusBarHidden
{
    //nascondi se la navBar non c'è'
    return (titleToolbar.alpha==0);
}



//cambio colore statusbar in bianco
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - View lifecycle
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//=========================


- (void)viewDidLoad
{
    // NSLog(@"WebBrowser:didLoad");

    [super viewDidLoad];
    clicked = NO;
    NSString *nibName = self.isIpad?@"WebBrowser-iPAD":(self.isIphone5?@"WebBrowser5":@"WebBrowser4");
    [self loadXibWithNibName:nibName];
    if ([[_webView subviews] count] > 0)
    {
        for (UIView* shadowView in [[[_webView subviews] objectAtIndex:0] subviews])
        {
            [shadowView setHidden:YES];
        }
        
        // unhide the last view so it is visible again because it has the content
        [[[[[_webView subviews] objectAtIndex:0] subviews] lastObject] setHidden:NO];
    }

    // Default to loading text
	titleLabel.text = @"Loading...";
	
    for (UIView *subview in _webView.subviews) {
        subview.clipsToBounds = NO;
    }
    _webView.clipsToBounds = NO;

    [_webView.scrollView setShowsHorizontalScrollIndicator:NO];
    [_webView.scrollView setShowsVerticalScrollIndicator:NO];
    [self loadContent];
    
    //Localizzo Pulsante Fine
    [doneButton setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];

}


#pragma mark - GESTIONE TOUCH TAB
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


- (void)tapAction:(UITapGestureRecognizer *)sender
{
    // NSLog(@"Web touched-> NavBar");
    if(self.isLand) return;
    // Get the specific point that was touched
    //CGPoint point = [sender locationInView:self.view];
    [self performSelector:@selector(executeTap) withObject:nil afterDelay:0.3];
}
    
-(void)executeTap{
    if(clicked) {clicked = NO; return;}
    CGFloat a = 1-titleToolbar.alpha;
    
    [UIView beginAnimations:@"Fade" context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(endFade)];
    [titleToolbar setAlpha:a];
    [bottomInfo_view setAlpha:a];
    [UIView commitAnimations];
}

-(void)endFade{
    // NSLog(@"End Fade");
    BOOL fullScreen = (titleToolbar.alpha==0);
    CGFloat height = [[UIScreen mainScreen] bounds].size.height - (fullScreen?0:70);
    [_webView setFrame:CGRectMake(0, fullScreen?-20:50, 320, height)];
}

#pragma mark - Appear

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark - Disappear

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [_webView stopLoading];
    [self activityNetwork:NO];
}


- (void)viewDidUnload {
	[super viewDidUnload];
	
	_webView.delegate = nil;
	[_webView stopLoading];
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================
-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    // NSLog(@"WebBrowser:didRotate:%i",self.isLand);
    //do your post-rotation // NSLogic here
    
    
    
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    //BOOL land = (UIInterfaceOrientationIsLandscape(toInterfaceOrientation));
    // NSLog(@"WebBrowser:WILL->Rotate:%i",land);
    
}

#pragma mark -
#pragma mark Activity Network
//=========================
// Activity Network
//=========================
-(BOOL)activityNetwork:(BOOL)b{
    [self performSelectorOnMainThread:b?@selector(attivaRotella):@selector(disattivaRotella) withObject:nil waitUntilDone:NO];
    return YES;
}

-(void)attivaRotella{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

-(void)disattivaRotella{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}


#pragma mark - Setters
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

- (void)setUrl:(NSURL *)inUrl {
    if (![[inUrl scheme] length])
    {
        inUrl = [NSURL URLWithString:[@"http://" stringByAppendingString:[inUrl absoluteString]]];
    }
    self->url = inUrl;
    self->dataWeb = nil;
    self->html = nil;
    
    if (self.view) {
        [self loadContent];
    }
}

- (void)setData:(NSData *)inData forMimeType:(NSString *)mimeType {
    self->dataWeb = inData;
    self->url = nil;
    self->html = nil;
    
    if ( ! mimeType) {
        mimeType = @"text/html";
    }
    
    self->_dataMimeType = mimeType;

    if (self.view) {
        [self loadContent];
    }
}

- (void)setHtml:(NSString *)inHtml {
    self->html = inHtml;
    self->dataWeb = nil;
    self->html = nil;

    if (self.view) {
        [self loadContent];
    }
}

#pragma mark - action

- (IBAction)doneButtonPressed:(UIButton*)sender {
	if (self.navigationController) {
		[self.navigationController popViewControllerAnimated:YES];
	} else {
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

- (IBAction)actionButtonPressed:(UIButton*)sender {
	UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:@"Apri in Safari" delegate:self cancelButtonTitle:@"Annulla" destructiveButtonTitle:nil otherButtonTitles:@"Apri", nil];
    
    if (self.isIphone)
        {
        [as showInView:self.view];
        }
    else
        {
        [as showFromRect:actionButton.frame inView:self.view animated:YES];
        }
}

- (IBAction)actionZOOM:(UIButton*)sender {
    [self zoom];
}

-(void)zoom{
    CGFloat a = 1-titleToolbar.alpha;
    BOOL fullScreen = (a==0);
    CGFloat height = self.isLand?[[UIScreen mainScreen] bounds].size.width - (fullScreen?0:70):[[UIScreen mainScreen] bounds].size.height - (fullScreen?0:70);
    CGFloat width = self.isLand?[[UIScreen mainScreen] bounds].size.height:[[UIScreen mainScreen] bounds].size.width;

    [UIView beginAnimations:@"Fade" context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(endZoom)];
    
    [titleToolbar setAlpha:a];
    [bottomInfo_view setAlpha:a];
    [zoomOUT setHidden:!fullScreen];
    if(self.isIos7orUpper)[_webView setFrame:CGRectMake(0, fullScreen?-20:50, width, height)];
    else {
        //iOS6
        [_webView setFrame:CGRectMake(0, fullScreen?0:50, width, height)];
        [zoomOUT setFrame:CGRectMake(zoomOUT.frame.origin.x, 5, zoomOUT.frame.size.width, zoomOUT.frame.size.height)];
    }
    [UIView commitAnimations];
}

-(void)endZoom{
    // NSLog(@"End Zoom");
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
}



- (IBAction)backAction:(UIButton*)sender {
	[_webView goBack];
}

- (IBAction)forwardAction:(UIButton*)sender {
	[_webView goForward];
}

- (IBAction)refreshAction:(UIButton*)sender {
    if(sender.tag>0)
	[_webView reload];
    //Reresh diventa STOP!
    else [_webView stopLoading];

}

#pragma mark - UIWebView Methods and Delegate
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

- (void)loadContent {
    if (url) {
        if ([_webView isLoading]) [_webView stopLoading];
        [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    } else if (dataWeb) {
        NSString *path = [[NSBundle mainBundle] bundlePath];
        NSURL *baseURL = [NSURL fileURLWithPath:path];
        [_webView loadData:dataWeb MIMEType:_dataMimeType textEncodingName:@"utf-8" baseURL:baseURL];
    } else if (html) {
        NSString *wrapperHTML = @"<html><head><meta name = \"viewport\" content = \"width = device-width\"><link rel=\"stylesheet\" media=\"all\" href=\"WebView.css\" /></head><body style=\"margin-top:%ipx;\">%@</body></html>";
        
        NSString *finalHtml;
        if ([html rangeOfString:@"<html"].location == NSNotFound) {
            finalHtml = [NSString stringWithFormat:wrapperHTML, html,self.isIpad?50:50];
        } else {
            finalHtml = html;
        }
        
        [_webView loadHTMLString:finalHtml baseURL:[[NSBundle mainBundle] bundleURL]];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    // NSLog(@"shouldStartLoadWithRequest:%@",request);
    clicked = YES;
	return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    // NSLog(@"webViewDidStartLoad");
	backButton.enabled = _webView.canGoBack;
	forwardButton.enabled = _webView.canGoForward;
    //cambio da refresh a x
    [refreshButton setImage:[UIImage imageNamed:@"browser_x.png"] forState:UIControlStateNormal];
    [refreshButton setTag:0];
	
	[loadingIndicator startAnimating];
    [self activityNetwork:YES];
	titleLabel.text = @"Loading...";
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	backButton.enabled = _webView.canGoBack;
	forwardButton.enabled = _webView.canGoForward;
    //cambio da x a refresh
    [refreshButton setImage:[UIImage imageNamed:@"browser_refresh.png"] forState:UIControlStateNormal];
    [refreshButton setTag:1];
	
	[loadingIndicator stopAnimating];
    [self activityNetwork:NO];
	titleLabel.text = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    
    NSString *currentURL = _webView.request.URL.absoluteString;
    
    if ([currentURL rangeOfString:@"ibreviary"].location != NSNotFound) {
        NSString *chat_div = [NSString stringWithFormat:@"var element = document.getElementById(\"comm100-float-button-2\"); element.parentNode.removeChild(element); element = document.getElementById(\"comm100-float-button-3\"); element.parentNode.removeChild(element); "];
            //NSString *removeChat =
            [_webView stringByEvaluatingJavaScriptFromString:chat_div];
            // NSLog(@"WebBrowser: webBrowser_removing chat:%@",removeChat);
    }
	_firstRequest = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
	if ([error code] != -999) {
        // NSLog(@"errore:%@",[NSString stringWithFormat:@"%@",[error description]]);
        NSString *title;
        if(self.url!=nil) title = [self.url host];
        else title = @"Error";
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:[NSString stringWithFormat:@"%@",[error localizedDescription]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
		[alert setTag:400];
		[alert show];
	}
    
	[self webViewDidFinishLoad:webView];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //400-> didFailLoadWithError alert displayed
    if(buttonIndex==0 && alertView.tag == 400)
        [self doneButtonPressed:doneButton];
}


#pragma mark - UIActionSheetDelegate
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0 && ! [[[[_webView request] URL] absoluteString] isEqualToString:@""]) {
		[[UIApplication sharedApplication] openURL:[[_webView request] URL]];
	}
}


@end
