//
//  EventoFlag.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "InfoTP.h"
#import "Lingua.h"
#import "FXBlurView.h"
#import "Tutorial.h"

@protocol EventFlagDelegate <NSObject>
-(void)passaAllaListaDownload;
-(void)dismissEvent;
-(BOOL)modificaEventFlagMain;

@end

@interface EventoFlag : BaseController<UIGestureRecognizerDelegate,InfoTPDelegate,LinguaDelegate,TutorialDelegate>{
}

//DELEGATE
@property (nonatomic, strong) BaseController<EventFlagDelegate> *delegate;

//SFONDO
@property (strong, nonatomic) IBOutlet UIImageView *logoVelina;

//STRUCTURE
@property (nonatomic, retain) NSMutableArray *list_day;
@property (strong, nonatomic) NSOperationQueue *queue;

//SELETTORI
@property (strong, nonatomic) IBOutlet UILabel *testi_propri; //Testipropri

//TABELLA - AGENDA
@property (strong, nonatomic) IBOutlet UILabel *title_tabella; //agenda
@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *rotella;
@property (strong, nonatomic) IBOutlet UILabel *no_item;

@property (strong, nonatomic) IBOutlet UIButton *close;

//INFO - CTRL
@property (strong, nonatomic) FXBlurView *blurView;
@property (strong, nonatomic) InfoTP *infoTP_ctrl;
@property (strong, nonatomic) IBOutlet UIImageView *info_img;

//LINGUA
@property (nonatomic, retain) NSString *linguaTP;
@property (strong, nonatomic) Lingua *lingua_ctrl;
@property (strong, nonatomic) IBOutlet UIButton *linguaTP_btn;

//SCROLLER
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UIPageControl *pager;
@property (strong, nonatomic) NSMutableArray *event_data;

//TUTORIAL
@property (strong, nonatomic) Tutorial *tutorialCTRL;
@property (strong, nonatomic) IBOutlet UIImageView *tutorial_img;
@property (strong, nonatomic) IBOutlet UIButton *btn_tutorial;

- (id)initWithDelegate:(BaseController<EventFlagDelegate> *)d;

-(IBAction)annullaTestoPropio:(UIButton*)sender;
-(void)switcher:(UISwitch*)sender;
-(void)changePage:(UIPageControl *)aPageControl;
@end
