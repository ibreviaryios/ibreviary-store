//
//  InfoTP.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "InfoTP.h"
#import "SystemData.h"
#import "TestoProprio.h"


@implementation InfoTP


@synthesize data_testipropri, sfondo, close, titolo,scroller,delegate;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<InfoTPDelegate>*)d andData:(NSArray*)da
{
    NSString *nibNameOrNil;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) nibNameOrNil = @"InfoTP5";
    else
        nibNameOrNil = ([[UIScreen mainScreen] bounds].size.height == 568)?@"InfoTP5":@"InfoTP";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        data_testipropri = [NSMutableArray arrayWithArray:da];
        if(self.isIpad) [self.view setFrame:d.view.frame];

    }
    return self;
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];

    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
    [self gui];
}

-(void)gui{
    
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
    [titolo setText:NSLocalizedString(@"infoTP_list", nil)];
    
    if(self.isIpad){
        CGRect titolo_rect = titolo.frame;
        titolo_rect.origin.y +=45;
        [titolo setFrame:titolo_rect];
        
        CGRect scroller_rect = scroller.frame;
        scroller_rect.origin.y +=65;
        scroller_rect.size.height -=65;
        [scroller setFrame:scroller_rect];
        [self.view setBackgroundColor:[UIColorFromRGB(0X3e3020) colorWithAlphaComponent:0.87]];
    }
    
    data_testipropri = [TestoProprio getTestiPropri];
    [data_testipropri removeObjectAtIndex:0];
    CGFloat top_padding = 35;
    for(int i = 0; i < data_testipropri.count; i++)
    {
        TestoProprio *row = [data_testipropri objectAtIndex:i];
        // NSLog(@"ROW:%@",row);
        UIView *riga = [[UIView alloc] initWithFrame:CGRectMake(0,top_padding+ (i*67), 280, 67)];
        [riga setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, 50, 50)];
        [logo setImage:row.logo?row.logo:row.image_official];
        [logo setContentMode:UIViewContentModeScaleAspectFit];
        [logo setAlpha:0.7];
        [riga addSubview:logo];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 220, 57)];
        [title setText:row.titolo];
        [title setNumberOfLines:0];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setFont:[UIFont fontWithName:FONT_HELVETICANEUE_THIN size:20]];
        [title setTextAlignment:NSTextAlignmentLeft];
        [title setTextColor:[UIColor whiteColor]];
        [riga addSubview:title];
        [scroller addSubview:riga];
    }
    
    [scroller setContentSize:CGSizeMake(280, top_padding+(data_testipropri.count*67)+10)];
    
}


- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
- (IBAction)FineInfoTP:(UIButton *)sender{
    [delegate removeInfoTP];
}


#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

//=========================
// ENDCLASS
//=========================
@end


