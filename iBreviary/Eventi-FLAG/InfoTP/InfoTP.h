//
//  InfoTP.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"


@protocol InfoTPDelegate <NSObject>
- (void)removeInfoTP;
@end


@interface InfoTP : BaseController


@property (nonatomic, strong) BaseController<InfoTPDelegate> *delegate;

@property (nonatomic, strong) NSMutableArray *data_testipropri;
@property (nonatomic, strong) IBOutlet UIImageView *sfondo;
@property (nonatomic, strong) IBOutlet UIButton *close;
@property (nonatomic, strong) IBOutlet UILabel *titolo;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;


- (id)initWithDelegate:(BaseController<InfoTPDelegate>*)d andData:(NSArray*)da;


@end

