//
//  EventoFlag.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

/*
 //================================
 //AGENDA MENU' PAROLE LOCALIZZATE
 //================================
 "TestiPropri" = "Proprio liturgico";
 "agenda_noitem" = "Nessun Testo Disponibile";
 "TestiPropriNoSelect" = "Nessun Testo Propio Selezionato";
 "testiPropri_abilita" = "Abilita";
 "testiPropri_disabilita" = "Disabilita";
*/
#import "EventoFlag.h"
#import "SystemData.h"
#import "Home.h"
#import "TestoProprio.h"

#define EVENT_URL @"http://www.ibreviary.com/service/services.php?s=get_event_days"

@interface EventoFlag ()

@property (nonatomic, assign) CGPoint scrollVelocity;

@end

@implementation EventoFlag

//INIT
@synthesize delegate;
//SFONDO
@synthesize  logoVelina,close;
//STRUCTURE
@synthesize queue,list_day;
//SELETTORE
@synthesize testi_propri;
//TABELLA - AGENDA
@synthesize title_tabella,tabella,rotella,no_item;
//infoTP
@synthesize infoTP_ctrl,blurView,info_img;
//linguaTP
@synthesize linguaTP, lingua_ctrl;
@synthesize linguaTP_btn;
//SCROLLER
@synthesize scroller,pager,event_data;
//->TUTORIAL
@synthesize tutorialCTRL, tutorial_img,btn_tutorial;

UIColor *selectedCell;
UIColor *notSelectedCell;
UIColor *defaulf_color,*default_disabilita,*default_mese;
UIColor *hightlight_color;

int processoFlag = 0;
int downloadDay = 0;
int cellaInDownload = -1;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)init
{
    //iPhone4
    NSString *nibNameOrNil = @"EventoFlag";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"EventoFlag_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"EventoFlag5";
    if(IS_IPHONE_6) nibNameOrNil = @"EventoFlag6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"EventoFlag6Plus";

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithDelegate:(BaseController<EventFlagDelegate> *)d
{
    //only iPAD call this!
    NSString *nibNameOrNil = @"EventoFlag_ipad";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
    }
    return self;
}

// Add this method
- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    list_day = [NSMutableArray new];
    
    queue = [NSOperationQueue new];
    [queue setMaxConcurrentOperationCount:1];
    
    linguaTP = [[[Home getCurrentDay] linguaGiorno] sigla];
;
    
    [self initView];
    event_data = [TestoProprio getTestiPropri];
    [self buildScroller];
    
    if(!self.isIpad)
    {
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
    }
}

- (void)initView{
    //COLORI
    selectedCell = [UIColor colorWithRed:171/255.f green:0 blue:0 alpha:0.9];
    notSelectedCell= [UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:150/255.f];
    defaulf_color = [UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.6];
    default_mese = [UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:255/255.f];
    hightlight_color = [UIColor colorWithRed:171/255.f green:0 blue:0 alpha:0.9];
    default_disabilita = [UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:100/255.f];
    
    //TABELLA
    tabella.opaque = NO;
    tabella.backgroundView = nil;
    
    //TEXT
    [testi_propri setText:[NSLocalizedString(@"TestiPropri", nil) uppercaseString]];
    [no_item setHidden:YES];
    [no_item setText:[NSLocalizedString(@"agenda_noitem", nil) uppercaseString]];
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
    [title_tabella setText:[NSLocalizedString(@"agenda_liturgica", nil) uppercaseString]];


    //ROTELLA
    rotella.transform = CGAffineTransformMakeScale(1.2f, 1.2f);
    [rotella setCenter:linguaTP_btn.center];
    
    //LINGUA-BTN
    [linguaTP_btn setContentMode:UIViewContentModeScaleAspectFit];
    [linguaTP_btn setImage:[[LinguaObject getLinguaBySigla:linguaTP]  logo] forState:UIControlStateNormal];
}

#pragma mark -
#pragma mark Appear
//=========================
// Appear
//=========================
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
	v.backgroundColor = [UIColor clearColor];
    v.alpha = 0;
	[tabella setTableFooterView:v];
	[tabella setTableHeaderView:v];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(![self.defaults boolForKey:TUTORIAL_TP_VISTO]){
        [self.defaults setBool:YES forKey:TUTORIAL_TP_VISTO];
        [self.defaults synchronize];
        [self showTutorial:nil];
    }
    
    
    if(self.connesso && ![[self.defaults objectForKey:EVENT_FLAG] isEqualToString:@""])[self preparaLista];
    else [self rimuoviTPRotella];
}

#pragma mark -
#pragma mark Disappear
//=========================
// Disappear
//=========================
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

//===========================
// TUTORIAL DELEGATE
//===========================
#pragma mark - TUTORIAL DELEGATE
- (IBAction)showTutorial:(UIButton*)sender{
    [self rotateImage:tutorial_img withDuration:0.6 onY_axis:YES];
    // NSLog(@"showTutorial");
    
    CGRect frame = self.view.frame;
    tutorialCTRL = [[Tutorial alloc] initWithDelegate:self];
    
    if(!self.isIos7orUpper && !self.isIphone5)
    {
        frame.size.height +=20;
        [tutorialCTRL.view setFrame:frame];
        frame.size.height -=20;
        
    }
    
    blurView = [[FXBlurView alloc] initWithFrame:frame];
    
    [self.blurView setDynamic:NO];
    [self.blurView setTintColor:[UIColor brownColor]];
    [self.blurView addSubview:tutorialCTRL.view];
    self.blurView.layer.transform = CATransform3DMakeScale(0.02, 0.02, 1.0);
    [self.blurView setCenter:CGPointMake(sender.center.x, sender.center.y+2)];
    [self.blurView setAlpha:0];
    
    [self.view addSubview:self.blurView];
    
    [UIView beginAnimations:@"AppareTutorialCTRL" context:NULL];
    [UIView setAnimationDuration:0.1];
    [self.blurView setAlpha:1];
    [UIView commitAnimations];
    [UIView animateWithDuration:0.4 animations:^{
                                                self.blurView.layer.transform = CATransform3DMakeScale(1, 1, 1.0);
                                                [self.blurView setCenter:self.view.center];
                                                }
                     completion:^(BOOL finished)    {
                         // NSLog(@"Fine Animation");
                     }];
    
}

-(void)removeTutorial{
    // NSLog(@"removeTutorial");
    
    [self rotateImage:tutorial_img withDuration:0.6 onY_axis:YES];
    [UIView animateWithDuration:0.4 animations:^{
                                                self.blurView.layer.transform = CATransform3DMakeScale(0.02, 0.02, 1.0);
                                                [self.blurView setCenter:CGPointMake(btn_tutorial.center.x, btn_tutorial.center.y+2)];
                                                }
                     completion:^(BOOL finished)    {
                         [UIView beginAnimations:@"SconpareTutorialCTRL" context:NULL];
                         [UIView setAnimationDuration:0.1];
                         [self.blurView setAlpha:0];
                         [UIView setAnimationDelegate:self.blurView];
                         [UIView setAnimationDidStopSelector:@selector(removeFromSuperview)];
                         [UIView commitAnimations];
                     }];
        
}


#pragma mark -
#pragma mark ROTELLA
//=========================
// ROTELLA
//=========================
-(void)attivaTPRotella{
    [rotella startAnimating];
    [rotella setHidden:NO];
}

-(void)rimuoviTPRotella{
    [rotella startAnimating];
    [rotella setHidden:NO];
    [self performSelector:@selector(rimuoviRotellaDelay) withObject:nil afterDelay:0.7];
}

-(void)rimuoviRotellaDelay{
    [rotella setHidden:YES];
}

#pragma mark -
#pragma mark SCROLLER
//=========================
// SCROLLER
//=========================
-(void)buildScroller
{
    for(UIView* subview in [self.scroller subviews]) //per cancellare tutte le superviste di una vista(usato per random del banner)
        [subview removeFromSuperview];
    
    CGRect puntoDiPartenza = CGRectZero;
    
    int count = (int)[event_data count];
    // NSLog(@"Scroller tp[%i]",count);
    for (int i = 0; i < count; i++) {
        TestoProprio *row = [event_data objectAtIndex:i];

        CGRect frame;
        frame.origin.x = self.view.frame.size.width*i;
        frame.origin.y = 0;
        frame.size = CGSizeMake(self.view.frame.size.width, 148);
                
        BOOL selezionato = [[self.defaults objectForKey:EVENT_FLAG] isEqualToString:row.key];
        
        UIView *item = [[UIView alloc] initWithFrame:frame];
        [item setTag:i+1000];
        [item setBackgroundColor:[UIColor clearColor]];
        // NSLog(@"item[%i] frame:%@",i,NSStringFromCGRect(item.frame));
        if (selezionato) {
            puntoDiPartenza = item.frame;
            [title_tabella setAlpha:1];
            if(!self.connesso)[self noNet];
        }
        
        if(i>0)
        {
        CGRect switch_frame = CGRectMake(self.view.frame.size.width-(51.f+20.f), 105.0, 51.0, 31.0);
        CGRect label_frame = CGRectMake(switch_frame.origin.x-(100+10), 104, 100, 31);
            if(!self.isIos7orUpper && !self.isIphone5) label_frame.origin.x -=20;

        UILabel *label_switcher = [[UILabel alloc] initWithFrame:label_frame];
        [label_switcher setBackgroundColor:[UIColor clearColor]];
        [label_switcher setTextColor:UIColorFromRGB(0X525252)];
        [label_switcher setNumberOfLines:1];
        [label_switcher setTag:2000];
        [label_switcher setTextAlignment:NSTextAlignmentRight];
        [label_switcher setFont:[UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size:13]];
        NSString *labelText = selezionato?NSLocalizedString(@"testiPropri_disabilita", nil):NSLocalizedString(@"testiPropri_abilita", nil);
        [label_switcher setText:[labelText uppercaseString]];
        [item addSubview:label_switcher];

        if(!self.isIos7orUpper && !self.isIphone5) switch_frame.origin.x -=20;

            
        UISwitch *ss = [[UISwitch alloc] initWithFrame: switch_frame];
        [ss setBackgroundColor:[UIColor whiteColor]];
        [ss setOnTintColor:[UIColorFromRGB(0X9d8773) colorWithAlphaComponent:0.9]];
        [ss setTintColor:[UIColorFromRGB(0Xad9988) colorWithAlphaComponent:0.7]];
        [ss.layer setCornerRadius:ss.frame.size.height/2.0];
        [ss setTag:100+i];
        [ss addTarget: self action:@selector(switcher:) forControlEvents:UIControlEventValueChanged];
        [item addSubview:ss];
        [ss setOn:selezionato];
        }
        
        if(i==0)
        {
            UIButton *ss = [[UIButton alloc] initWithFrame: CGRectMake(self.view.frame.size.width-205.0, 47.0, 195.0, 31.0)];
            [ss setBackgroundColor:[UIColor clearColor]];
            [ss setTitle:[NSLocalizedString(@"testiPropri_reset", nil) uppercaseString] forState:UIControlStateNormal];
            [ss setTitleColor:UIColorFromRGB(0X9d8773) forState:UIControlStateNormal];
            [ss.titleLabel setFont:[UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size:14]];
            ss.titleEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0);
            [ss setTag:999];
            [ss addTarget:self action:@selector(annullaTestoPropio:) forControlEvents:UIControlEventTouchUpInside];
            [ss setAlpha:([self.defaults objectForKey:EVENT_FLAG]!=nil && ![[self.defaults objectForKey:EVENT_FLAG] isEqualToString:@""])?1:0];
            [item addSubview:ss];
        }
        UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake(10, 13.0, 108.0, 108.0)];
        [logo setImage:(i!=0)?row.image_official:[UIImage imageNamed:@"logo_no.png"]];
        [logo setAlpha:0.25];
        [item addSubview:logo];
        
        UILabel *label_titolo = [[UILabel alloc] initWithFrame:CGRectMake(125, 34.0, 190, 60)];
        [label_titolo setBackgroundColor:[UIColor clearColor]];
        [label_titolo setTextColor:UIColorFromRGB(0X9d8773)];
        [label_titolo setNumberOfLines:3];
        [label_titolo setTextAlignment:NSTextAlignmentLeft];
        [label_titolo setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:16]];
        [label_titolo setText:[row.titolo uppercaseString]];
        if(i==0){
            [label_titolo setText:[NSLocalizedString(@"TestiPropriNoSelect", nil) uppercaseString]];
            [label_titolo setAlpha:(![self.defaults objectForKey:EVENT_FLAG]||[[self.defaults objectForKey:EVENT_FLAG] isEqualToString:@""])?1:0];
            [label_titolo setTag:666];
        }
        [item addSubview:label_titolo];
        
        [self.scroller addSubview:item];
    }
    [pager setNumberOfPages:count];
    [pager addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    self.scroller.contentSize = CGSizeMake(self.scroller.frame.size.width * count, self.scroller.frame.size.height);
    [self.scroller setContentOffset:CGPointZero animated:NO];
    
    [self.scroller scrollRectToVisible:puntoDiPartenza animated:NO];

}

-(IBAction)annullaTestoPropio:(UIButton*)sender{
    // NSLog(@"annullaTestoPropio:%@",sender);
    [self.defaults setObject:@"" forKey:EVENT_FLAG];
    [self.defaults synchronize];
    [title_tabella setAlpha:0];
    [no_item setHidden:YES];
    for (int i= 1;i<[event_data count];i++ ) {
        UISwitch *ss = (UISwitch*)[[self.scroller viewWithTag:1000+i] viewWithTag:100+i];
        UILabel *ll = (UILabel*)[[self.scroller viewWithTag:1000+i] viewWithTag:2000];
        [ss setOn:NO animated:NO];
        [ll setText:[NSLocalizedString(@"testiPropri_abilita", nil) uppercaseString]];
    }
    
    [[[self.scroller viewWithTag:1000] viewWithTag:999] setAlpha:0];
    [[[self.scroller viewWithTag:1000] viewWithTag:666] setAlpha:1];

    
    [Home modificaEventFlag];
    list_day = [NSMutableArray new];
    [tabella reloadData];
    
}


-(void)switcher:(UISwitch*)sender{
    // NSLog(@"switcher:%li",(long)sender.tag);
    [self attivaTPRotella];
    if(sender.on)
    {
        [self.defaults setObject:[(TestoProprio*)[event_data objectAtIndex:sender.tag-100] key] forKey:EVENT_FLAG];
        [title_tabella setAlpha:1];
        [[[self.scroller viewWithTag:1000] viewWithTag:999] setAlpha:1];
        [[[self.scroller viewWithTag:1000] viewWithTag:666] setAlpha:0];
        NSString *labelText = NSLocalizedString(@"testiPropri_disabilita", nil);
        [(UILabel*)[sender.superview viewWithTag:2000] setText:labelText];
        [self.defaults synchronize];
    }
    else{
        [self rimuoviTPRotella];
        [self annullaTestoPropio:nil];
        return;
    }
    
    // NSLog(@"Selezionato:%@",[self.defaults objectForKey:EVENT_FLAG]);
    
    for (int i= 1;i<[event_data count];i++ ) {
        TestoProprio * row = [event_data objectAtIndex:i];
        BOOL selezionato = [[self.defaults objectForKey:EVENT_FLAG] isEqualToString:row.key];
        
        UISwitch *ss = (UISwitch*)[[self.scroller viewWithTag:1000+i] viewWithTag:100+i];
        UILabel *ll = (UILabel*)[[self.scroller viewWithTag:1000+i] viewWithTag:2000];
        [ss setOn:[[self.defaults objectForKey:EVENT_FLAG] isEqualToString:[(TestoProprio*)[event_data objectAtIndex:i] key]] animated:NO];
        NSString *labelText = selezionato?NSLocalizedString(@"testiPropri_disabilita", nil):NSLocalizedString(@"testiPropri_abilita", nil);
        [ll setText:[labelText uppercaseString]];

    }
    if(self.isIpad)
    {
        Home *h = [Home sharedInstance];
        TestoProprio * t = [TestoProprio getTestoProprioWithKey:[self.defaults objectForKey:EVENT_FLAG]];
        // NSLog(@"modificaEventFlagMain->%@ - %@",t,h.eventFLAG);
        UIImage *img = [t logo]?[t logo]:[t image_official];
        [h.eventFLAG setImage:img forState:UIControlStateNormal];
    }
    
    [Home modificaEventFlag];
    
    if(!self.connesso) [self noNet];
    if(self.connesso && sender.tag-100!=0)[self preparaLista];
    else {
        list_day = [NSMutableArray new];
        [tabella reloadData];
    }
    
}


#pragma mark - ScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView*)sender
{
    // NSLog(@"scrollViewDidScroll:%@",sender);
    if(sender != scroller){
        if (lastContentOffset > sender.contentOffset.y)
            goingUP = NO;
        else if (lastContentOffset <= sender.contentOffset.y)
            goingUP = YES;
        
        lastContentOffset = sender.contentOffset.y;
        return;
    }
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scroller.frame.size.width;
    // NSLog(@"scrollViewDidScroll!");
    int page = floor((self.scroller.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pager.currentPage = page;
}

- (void)changePage:(UIPageControl *)aPageControl
{
    // NSLog(@"changePage:%li",(long)self.pager.currentPage);
    // update the scroll view to the appropriate page
    CGRect frame;
    frame.origin.x = self.scroller.frame.size.width * self.pager.currentPage;
    frame.origin.y = 0;
    frame.size = self.scroller.frame.size;
    [self.scroller scrollRectToVisible:frame animated:YES];
}

- (void)resetPos
{
    [self performSelectorOnMainThread:@selector(resetPosMain) withObject:nil waitUntilDone:YES];
    // NSLog(@"FinePos");
}
- (void)resetPosMain
    {
    CGRect frame;
    int i = [TestoProprio getPosTestoProprioWithKey:[self.defaults objectForKey:EVENT_FLAG]];
    frame.origin.x = self.scroller.frame.size.width * i;
    // NSLog(@"ResetPos:%f[%i]",frame.origin.x,i);
    frame.origin.y = 0;
    frame.size = self.scroller.frame.size;
    [scroller scrollRectToVisible:frame animated:YES];
}


//=========================
// INFOTP DELEGATE
//=========================
#pragma mark - INFOTP DELEGATE
-(IBAction)showInfoTP
{
    // NSLog(@"showInfoTP");
	infoTP_ctrl = [[InfoTP alloc] initWithDelegate:self andData:[NSArray new]];
	infoTP_ctrl.view.alpha = 0;
    CGRect frame = self.view.frame;
    if(!self.isIos7orUpper && !self.isIphone5)
    {
        [infoTP_ctrl.view setFrame:frame];
    }
    
    blurView = [[FXBlurView alloc] initWithFrame:frame];
    [self.blurView setDynamic:NO];
    [self.blurView setTintColor:[UIColor brownColor]];
    self.blurView.alpha = 0;
    
    [self.view addSubview:blurView];
	[self.view addSubview:infoTP_ctrl.view];
	
	[UIView beginAnimations:@"AppareINFO" context:NULL];
    [UIView setAnimationDuration:0.5];
	infoTP_ctrl.view.alpha = 1;
    self.blurView.alpha = 1.0;
    [UIView commitAnimations];
    
}

-(void)removeInfoTP{
    // NSLog(@"FineInfo");
    infoTP_ctrl.view.alpha = 1;
    [self rotateImage:info_img withDuration:0.6 onY_axis:YES];
    [UIView beginAnimations:@"InfoTP_disappear" context:NULL];
    [UIView setAnimationDuration:0.4];
    infoTP_ctrl.view.alpha = 0;
    self.blurView.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineInfoTP)];
    [UIView commitAnimations];
}

-(void)fineInfoTP{
    [infoTP_ctrl.view removeFromSuperview];
    infoTP_ctrl =  nil;
    self.blurView = nil;

}

//=========================
// LINGUA DELEGATE
//=========================
#pragma mark - LINGUA DELEGATE
-(IBAction)showLingua
{
    // NSLog(@"showLinguaTP");
    //if([[self.defaults objectForKey:EVENT_FLAG] isEqualToString:@""]) return;
	lingua_ctrl = [[Lingua alloc] initForTPWithDelegate:self];
	lingua_ctrl.view.alpha = 0;
    blurView = [[FXBlurView alloc] initWithFrame:self.view.frame];
    [self.blurView setDynamic:NO];
    [self.blurView setTintColor:[UIColor brownColor]];
    self.blurView.alpha = 0;
    
    [self.view addSubview:blurView];
	[self.view addSubview:lingua_ctrl.view];
	
	[UIView beginAnimations:@"AppareLinguaTP" context:NULL];
    [UIView setAnimationDuration:0.5];
	lingua_ctrl.view.alpha = 1;
    self.blurView.alpha = 1.0;
    [UIView commitAnimations];
    
}

-(void)removeLingua{
    // NSLog(@"FineLingua");
    lingua_ctrl.view.alpha = 1;
    [UIView beginAnimations:@"linguaTP_disappear" context:NULL];
    [UIView setAnimationDuration:0.4];
    lingua_ctrl.view.alpha = 0;
    self.blurView.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineLingua)];
    [UIView commitAnimations];
}

- (void)linguaPropriScelta:(NSString*)scelta_tp{
    if([[self.defaults objectForKey:EVENT_FLAG] isEqualToString:@""]) {[self removeLingua]; return;}
    linguaTP = scelta_tp;
    if(self.connesso)
    {
        [self preparaLista];
    }
    else
    {
        [self noNet];
        list_day = [NSMutableArray new];
        [tabella reloadData];
    }
    [self performSelector:@selector(removeLingua) withObject:nil afterDelay:0.3];
    [self performSelector:@selector(segnalaCambiamento) withObject:nil afterDelay:0.6];
}

-(void)segnalaCambiamento{
    [self cardRotation:linguaTP_btn destination:[[LinguaObject getLinguaBySigla:linguaTP] logo]];

}

-(void)fineLingua{
    [infoTP_ctrl.view removeFromSuperview];
    infoTP_ctrl =  nil;
    self.blurView = nil;
    [self resetPos];    
}



#pragma mark -
#pragma mark AZIONI
//=========================
// AZIONI
//=========================
- (IBAction)azioneDismiss:(UIButton*)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 // NSLog(@"azioneDismiss completata");
                                 [Home dismiss:self];
                             }];

}

#pragma mark -
#pragma mark PREPARA LISTA
//=========================
// PREPARA LISTA
//=========================
-(void)preparaLista{
    processoFlag = 1;
    [self attivaTPRotella];

    list_day = [NSMutableArray new];
    [tabella reloadData];
    if(!self.connesso)
        {
            [self noNet];
            processoFlag = 0;
            [self performSelectorOnMainThread:@selector(MainReload) withObject:nil waitUntilDone:NO];
            return;
        }

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&r=iphone&v=%@&l=%@&t=%@",EVENT_URL,self.iBreviary_version, linguaTP,[self.defaults objectForKey:EVENT_FLAG]]];
    // NSLog(@"Agenda_download url:%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_REQUEST];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *dataR, NSError *error) {
        if (dataR && !error)
          {
            error = nil;
            [self checkData:dataR];
          }
        else
            
          {
            // NSLog(@"error:%@",[error description]);
            [no_item setText:[error description]];
            [no_item setHidden:NO];
          }
        processoFlag = 0;
        [self performSelectorOnMainThread:@selector(MainReload) withObject:nil waitUntilDone:NO];
    }];
    
}

-(void)checkData:(NSData*)dlist{
    NSError *err = nil;
    NSMutableArray *risp = [[NSMutableArray alloc] initWithArray:[NSJSONSerialization JSONObjectWithData:dlist options:NSJSONReadingMutableContainers error:&err]];
    if(err) {
        // NSLog(@"Errore parsing Json eventDays:%@",[err description]);
        }
    // NSLog(@"risposta:%@",risp);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSDate *oggi = [NSDate date];
    
    
    NSMutableArray *lista_risposta = [[NSMutableArray alloc] init];
    NSMutableArray *post = [NSMutableArray new];

    for(NSMutableDictionary *row in risp)
      {
        [dateFormatter setDateFormat:@"YYYY"];
        NSString *year = [dateFormatter stringFromDate:oggi];
        NSString *day = ([[row objectForKey:@"day"] intValue]<10)?[NSString stringWithFormat:@"0%i",[[row objectForKey:@"day"] intValue]]:[row objectForKey:@"day"];
        NSString *month = ([[row objectForKey:@"month"] intValue]<10)?[NSString stringWithFormat:@"0%i",[[row objectForKey:@"month"] intValue]]:[row objectForKey:@"month"];
        
        [dateFormatter setDateFormat:@"YYYY-MM-dd"];
        if([[dateFormatter stringFromDate:oggi] compare:[NSString stringWithFormat:@"%@-%@-%@",year,month,day]]==1)
          {
            year = [NSString stringWithFormat:@"%i",[year intValue]+1];
          }
        [row setObject:year forKey:@"year"];
        NSString *nuovaStringa = [NSString stringWithFormat:@"%@-%@-%@",year,month,day];
        [row setObject:nuovaStringa forKey:@"dateFromString"];
        // NSLog(@"ParsinG_row:%@",row);
          [dateFormatter setDateFormat:@"YYYY"];
        if([[dateFormatter stringFromDate:oggi] intValue] == [year intValue])
            [lista_risposta addObject:row];
        else [post addObject:row];
      }
    [lista_risposta addObjectsFromArray:post];
    list_day = [[NSMutableArray alloc] initWithArray:lista_risposta];
    [self performSelectorOnMainThread:@selector(MainReload) withObject:nil waitUntilDone:NO];
}

-(void)MainReload{
    [self rimuoviTPRotella];
    // NSLog(@"mainReloadTabellaEventi:%@",NSStringFromCGRect(tabella.frame));
    [no_item setHidden:(list_day.count>0)||([[self.defaults objectForKey:EVENT_FLAG] isEqualToString:@""])];
    [tabella setHidden:YES];
    [tabella reloadData];
    [self animateTableAppear];
}

#pragma mark -
#pragma mark ->> Table view

// NUMERO SESSIONI IN TABELLA
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // NSLog(@"Numero elementi tabella:%lu",(unsigned long)list_day.count);
    if(list_day == nil || processoFlag) return 0;
    NSInteger i = [list_day count];
    BOOL attivaScroll = self.isIphone5?(i>5):(i>4);
    [tableView setScrollEnabled:attivaScroll];
    return i;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    // NSLog(@"heightForHeaderInSection!");
    return 0.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70.0;
}

#pragma mark row
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *row = [[NSMutableDictionary alloc] initWithDictionary:[list_day objectAtIndex:indexPath.row]];
    // NSLog(@"ROW:%@",row);
    [tabella setAlpha:(processoFlag==1)?0.2:1];

    
    static NSString *CellIdentifier = @"Cell_Agenda";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        [cell setBackgroundView:nil];
        }
    
    if(processoFlag==0 && ![rotella isHidden])[self rimuoviTPRotella];
        
    //CUSTOM LABEL
    UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 69)];
    [Label setBackgroundColor:[UIColor colorWithRed:215/255.f green:170/255.f blue:105/255.f alpha:0.4]];
    [Label.layer setMasksToBounds:YES];
    
    UIView* schiarire = [[UIView alloc ] initWithFrame:CGRectMake(100, 0, Label.frame.size.width-100, 69)];
    [schiarire setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.2]];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];        
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];    
    NSDate *date = [dateFormatter dateFromString:[row objectForKey:@"dateFromString"]];    
    
    // NSLog(@"row_day:%@ from %@", [dateFormatter stringFromDate:date],[NSString stringWithFormat:@"%@-%@-%@",[row objectForKey:@"year"],[row objectForKey:@"month"],[row objectForKey:@"day"]]);
    
    //-> DATA_day
	UILabel *giorno = [[UILabel alloc] initWithFrame:CGRectMake(0, 3, schiarire.frame.origin.x, 26)];
	[giorno setBackgroundColor:[UIColor clearColor]];
    [giorno setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:20]];
    [giorno setHighlightedTextColor:[[UIColor redColor] colorWithAlphaComponent:0.6]];
    [giorno setTextColor:defaulf_color];
	[giorno setTextAlignment:NSTextAlignmentCenter];
    [dateFormatter setDateFormat:@"dd"];
	[giorno setText:[dateFormatter stringFromDate:date]];
	giorno.numberOfLines = 1;
    //-> DATA_mese
	UILabel *mese = [[UILabel alloc] initWithFrame:CGRectMake(0, 13.5, schiarire.frame.origin.x, 40)];
	[mese setBackgroundColor:[UIColor clearColor]];
    [mese setFont:[UIFont fontWithName:FONT_HOEFLER size:16]];
    [mese setHighlightedTextColor:[[UIColor redColor] colorWithAlphaComponent:0.6]];
    [mese setTextColor:defaulf_color];
	[mese setTextAlignment:NSTextAlignmentCenter];
    [dateFormatter setDateFormat:@"MMMM"];
	[mese setText:[[dateFormatter stringFromDate:date] uppercaseString]];
	mese.numberOfLines = 1;
    //-> DATA_anno
	UILabel *anno = [[UILabel alloc] initWithFrame:CGRectMake(0, 42, schiarire.frame.origin.x, 30)];
	[anno setBackgroundColor:[UIColor clearColor]];
    [anno setFont:[UIFont fontWithName:@"AcademyEngravedLetPlain" size:25]];
    [anno setHighlightedTextColor:[[UIColor redColor] colorWithAlphaComponent:0.6]];
    [anno setTextColor:defaulf_color];
	[anno setTextAlignment:NSTextAlignmentCenter];
	[anno setText:[row objectForKey:@"year"]];
	anno.numberOfLines = 1;
    
    UITextView *text = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, schiarire.frame.size.width, 61)];
    [text setFont:[UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size:17]];
    [text setBackgroundColor:[UIColor clearColor]];
    [text setTextColor:[UIColorFromRGB(0X4a2f20) colorWithAlphaComponent:0.8]];
    [text setEditable:NO];
    [text setScrollEnabled:NO];
    [text setUserInteractionEnabled:NO];
    NSString *testo = [row objectForKey:@"title"];
    if(!IS_IPHONE_6 && !IS_IPHONE_6_PLUS && testo.length > 140)
        testo = [testo substringToIndex:140];
    [text setText:testo];
    //[text setText:[self puntini:[row objectForKey:@"title"]]];
    /*if(text.text.length<30)[text setTextAlignment:NSTextAlignmentNatural];
    else if(text.text.length<60)[text setTextAlignment:NSTextAlignmentNatural];
    else if(text.text.length<90)[text setTextAlignment:NSTextAlignmentNatural];
    else if(text.text.length<120)*/
    [text setTextAlignment:NSTextAlignmentNatural];
    UIImageView *selectedBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    [selectedBackgroundView setBackgroundColor:[UIColor colorWithRed:230/255.f green:175/255.f blue:96/255.f alpha:0.6]];
    [cell setSelectedBackgroundView:selectedBackgroundView];

    
    [Label addSubview:schiarire];
    [Label addSubview:giorno];
    [Label addSubview:mese];
    [Label addSubview:anno];
    [schiarire addSubview:text];
    
    if(cellaInDownload==indexPath.row){
        // NSLog(@"cellaInDownload=%i",cellaInDownload);
        UIActivityIndicatorView *rotellaDownload = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [rotellaDownload setColor:[UIColor brownColor]];
        //[rotellaDownload setTransform:CGAffineTransformMakeScale(1.6f, 1.6f)];
        [rotellaDownload startAnimating];
        [rotellaDownload setCenter:CGPointMake(210, 34)];
        [Label addSubview:rotellaDownload];
    }
    
    

    [cell.contentView addSubview:Label];
    return cell;
}

- (NSString *)puntini:(NSString*)result
{
    if(result.length<90) return result;
    //result = [result capitalizedString];
    NSMutableString *mu = [NSMutableString stringWithString:result];
    int i =0;
    int linee = 1;
    int numeroCaratteriLinea=32;
    while (i<result.length) {
        i =numeroCaratteriLinea*linee;
        if(i<mu.length) 
        {
            linee+=1;
            if([mu characterAtIndex:i] != ' ' && [mu characterAtIndex:i-1] != ' ' && [mu characterAtIndex:i+1] != ' ') [mu insertString:@" " atIndex:i];
        }
    }
    result = nil;
    result = [NSString stringWithString:mu];
    
    if (result.length>(numeroCaratteriLinea*linee)) {
        return [[result substringToIndex:(numeroCaratteriLinea*linee)-3] stringByAppendingString:@"..."];
    }
    return result;
}



#pragma mark -
#pragma mark Table view delegate

int riga_selezionata = -1;

// COMANDA DOWNLOAD TESTI PROPRI DAY
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // NSLog(@"didSelectRowAtIndexPath:%i",(int)indexPath.row);
    riga_selezionata = (int)indexPath.row;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(downloadDay!=0) return;
    if(!self.connesso) { [self noNet]; return; }
    //operazioni THREAD
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(download:) object:[list_day objectAtIndex:indexPath.row]];
    
    cellaInDownload = (int)indexPath.row;
    [self performSelectorOnMainThread:@selector(reloadRow) withObject:nil waitUntilDone:YES];

    downloadDay = 1;
    
    [queue addOperation:operation];
    
    // NSLog(@"AgendaTable - cella selezionata FINE");
}

-(void)reloadRow{
    if(cellaInDownload>0 && cellaInDownload < list_day.count)
    {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellaInDownload inSection:0];
    [tabella reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                   withRowAnimation: UITableViewRowAnimationFade];
    }
    else [tabella reloadData];
}

-(void)resetRow{
    cellaInDownload = -1;
    [tabella reloadData];
    [self performSelectorOnMainThread:@selector(rimuoviTPRotella) withObject:nil waitUntilDone:YES];
}

#pragma mark - Animate Table
//==========================
// ANIMATE TABLE
//==========================
-(void)animateTableAppear{
    [self.tabella setHidden:NO];
    NSArray *visibleCell = [self.tabella visibleCells];
    for(__block NSUInteger idx = 0; idx < visibleCell.count; idx++){
        __block UITableViewCell *cell = [visibleCell objectAtIndex:idx];
        CGPoint origin = cell.frame.origin;
        [cell setFrame:CGRectMake(cell.frame.origin.x, self.tabella.frame.size.height+self.tabella.contentOffset.y, cell.frame.size.width, cell.frame.size.height)];
        [UIView animateWithDuration:0.7
                              delay:(0.1*(float)idx)
             usingSpringWithDamping:0.99
              initialSpringVelocity:15
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [cell setFrame:CGRectMake(origin.x, origin.y, cell.frame.size.width, cell.frame.size.height)];
                         } completion:^(BOOL fine){
                             
                         }];
        
    }
    [self.tabella setHidden:NO];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    CATransform3D translation;
    if(goingUP){
        translation = CATransform3DMakeTranslation(0, MIN(fabs(self.scrollVelocity.y), cell.frame.size.height), 0);
    } else {
        translation = CATransform3DMakeTranslation(0, -MIN(fabs(self.scrollVelocity.y), cell.frame.size.height), 0);
    }
    
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    cell.layer.transform = CATransform3DIdentity;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}

CGFloat lastContentOffset;
BOOL goingUP;

#pragma mark - ScrollView delegate (for top animation)
//===================
//ScrollView delegate
//===================
/*
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (lastContentOffset > scrollView.contentOffset.y)
        goingUP = NO;
    else if (lastContentOffset <= scrollView.contentOffset.y)
        goingUP = YES;
    
    lastContentOffset = scrollView.contentOffset.y;
}
*/
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if(scrollView != scroller)
    self.scrollVelocity = [scrollView.panGestureRecognizer velocityInView:scrollView];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if(scrollView != scroller)
    self.scrollVelocity = [scrollView.panGestureRecognizer velocityInView:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if(scrollView != scroller)
    self.scrollVelocity = [scrollView.panGestureRecognizer velocityInView:scrollView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    
}

-(void)download:(NSDictionary*)row{
    [self.view setUserInteractionEnabled:NO];
    [self performSelectorOnMainThread:@selector(attivaTPRotella) withObject:nil waitUntilDone:YES];
    // NSLog(@"AGENDA: download");
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyy-mm-dd"];
    NSDate *date = [dateFormatter dateFromString:[row objectForKey:@"dateFromString"]];
    
    [dateFormatter setDateFormat:@"mm-dd"];
    NSString *giornoDaScaricare = [NSString stringWithFormat:@"%@-%@",[row objectForKey:@"year"],[dateFormatter stringFromDate:date]];

    [Day scaricaGiornoconData:giornoDaScaricare
                     withLang:self.linguaTP
                    eventFlag:[TestoProprio getTestoProprioWithKey:[self.defaults objectForKey:EVENT_FLAG]]
              completionBlock:^(Day *ddd){
                  if(!ddd)
                  {
                      // NSLog(@"errorre:%@ day:%@",[err description],ddd);
                      [self mostraNotificaDayDownloaded:[dateFormatter stringFromDate:date]];
                      
                  }
                  else{
                      // NSLog(@"AGENDA: scaricato giorno:%@",ddd.filename);
                      
                      if(self.isIos7orUpper){
                          [dateFormatter setDateFormat:@"d/mm/yy"];
                          // NSLog(@"date:%@ formatter:%@",ddd.date,[dateFormatter stringFromDate:date]);
                          [self mostraNotificaDayDownloaded:[dateFormatter stringFromDate:date]];
                      }
                      
                      ddd = nil;
                  }
                  
                  downloadDay = 0;
                  riga_selezionata = -1;
                  
                  
                  [self performSelectorOnMainThread:@selector(resetRow) withObject:nil waitUntilDone:YES];
                  [self.view setUserInteractionEnabled:YES];
                  return ddd;
    }];
    
}


#pragma mark -
#pragma mark OPERATION QUEUE
//=================
//OPERATION QUEUE
//=================
-(void)aggiungiOperazione:(SEL)selector{
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self selector:selector object:nil];
    [queue addOperation:operation];
}

-(void)aggiungiOperazione:(SEL)selector withObject:(id)obj{
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self selector:selector object:obj?obj:nil];
    [queue addOperation:operation];
}

-(void)cancelAllOperation{
    [queue cancelAllOperations];
}

#pragma mark -
#pragma mark ERROR CLASS
//=========================
// ERROR CLASS
//=========================
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================
-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return [[DDMenuController sharedInstance] supportedInterfaceOrientations];
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    // NSLog(@"will Rotate");
    [[DDMenuController sharedInstance] willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    // NSLog(@"did Rotate");
    [[DDMenuController sharedInstance] didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

//=========================
// END
//=========================
@end
