//
//  Tutorial.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "Tutorial.h"
#import "WebBrowser.h"
#import "Lista.h"
#import "Settings.h"
#import "EventoFlag.h"
#import "MessaLista.h"
#import "MySpiritualBookList.h"


@implementation Tutorial

@synthesize textV,delegate,webContent,close;


#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<TutorialDelegate>*)d
{

    //iPhone4
    NSString *nibNameOrNil = @"Tutorial";
    
    //iPADs
    //if(IS_IPAD) nibNameOrNil = @"Lista_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"Tutorial5";
    if(IS_IPHONE_6) nibNameOrNil = @"Tutorial6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"Tutorial6Plus";

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        if(self.isIpad) [self.view setFrame:d.view.frame];
    }
    return self;
}



#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
-(IBAction)FineTutorial:(UIButton*)sender{
    // NSLog(@"FineTutorial");
    [delegate removeTutorial];
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    // NSLog(@"Tutorial DidLoad");
    [webContent setDelegate:self];
    
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
    
    [self caricaTesto];
}

#pragma mark - WEB-DELEGATE
//=========================
// WEB-DELEGATE
//=========================
- (BOOL)webView:(UIWebView *)w shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    // NSLog(@"webViewDidFinishLoad H:%li",(long)[[webView stringByEvaluatingJavaScriptFromString:@"$(document).height();"] integerValue]);
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    // NSLog(@"scrollViewWillBeginDragging");
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // NSLog(@"scrollViewDidScroll");
}




-(void)caricaTesto{
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    NSString *tutorial_html = @"";
    if([delegate isKindOfClass:[Lista class]])
        {
        NSString *tutorial_path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"tutorial_lista_%@",NSLocalizedString(@"checklingua", nil)] ofType:@"html"];
        
        if(tutorial_path == nil){
            tutorial_path = [[NSBundle mainBundle] pathForResource:@"tutorial_lista_en" ofType:@"html"];
        }
        tutorial_html=[NSString stringWithContentsOfFile:tutorial_path encoding:NSUTF8StringEncoding error:nil];
        }
    if([delegate isKindOfClass:[EventoFlag class]])
        {
        NSString *tutorial_event_path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"tutorial_tp_%@",NSLocalizedString(@"checklingua", nil)] ofType:@"html"];
        
        if(tutorial_event_path == nil){
            tutorial_event_path = [[NSBundle mainBundle] pathForResource:@"tutorial_tp_en" ofType:@"html"];
        }

        tutorial_html=[NSString stringWithContentsOfFile:tutorial_event_path encoding:NSUTF8StringEncoding error:nil];
        }
    
    if([delegate isKindOfClass:[MessaLista class]])
     {
         // NSLog(@"CHECK-LINGUA:%@",NSLocalizedString(@"checklingua", nil));
         NSString *tutorial_messa_path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"tutorial_messa_%@",NSLocalizedString(@"checklingua", nil)] ofType:@"html"];
         
         if(tutorial_messa_path == nil){
             tutorial_messa_path = [[NSBundle mainBundle] pathForResource:@"tutorial_messa_en" ofType:@"html"];
         }
         tutorial_html=[NSString stringWithContentsOfFile:tutorial_messa_path encoding:NSUTF8StringEncoding error:nil];
     }
    
    if([delegate isKindOfClass:[MySpiritualBookList class]])
    {
        // NSLog(@"CHECK-LINGUA:%@",NSLocalizedString(@"checklingua", nil));
        NSString *tutorial_note_path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"tutorial_spiritual_%@",NSLocalizedString(@"checklingua", nil)] ofType:@"html"];
        
        if(tutorial_note_path == nil){
            tutorial_note_path = [[NSBundle mainBundle] pathForResource:@"tutorial_spiritual_en" ofType:@"html"];
        }
        tutorial_html=[NSString stringWithContentsOfFile:tutorial_note_path encoding:NSUTF8StringEncoding error:nil];
        tutorial_html = [NSString stringWithFormat:tutorial_html,IS_IPAD?@"bodyIPAD":@"body"];
    }



    
    [webContent loadHTMLString:tutorial_html baseURL:baseURL];
}


#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}



@end
