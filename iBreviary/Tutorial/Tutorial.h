//
//  Tutorial.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"

@protocol TutorialDelegate <NSObject>
- (void)removeTutorial;
@end


@interface Tutorial : BaseController<UIWebViewDelegate>

    
@property (nonatomic, strong) BaseController<TutorialDelegate> *delegate;
@property (nonatomic, strong) IBOutlet UITextView *textV;
@property (nonatomic, strong) IBOutlet UIButton *close;
@property (nonatomic, strong) IBOutlet UIWebView *webContent;

- (id)initWithDelegate:(BaseController<TutorialDelegate>*)d;

@end
