//
//  it_netguru_ibreviaryproterrasanctaAppDelegate.m
//  iBreviary
//
//  Created by leyo on 22/01/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import "AppDelegate.h"
#import "Home.h"
#import "MyNavigationCTRL.h"
#import "CheckVersion.h"
#import "MenuViewController.h"
#import "Settings.h"
#import "CheckVersion.h"
#import "Appirater.h"
#import "SystemData.h"
#import "GlobalFunction.h"
#import "NotificaObject.h"
#import "DDMenuController.h"
#import "NotificaObject.h"
#import "Nota.h"
#import "ShortCutItem.h"

@interface AppDelegate ()


@property (nonatomic, strong) NSMetadataQuery * metadataQuery;

@property (strong,nonatomic) NSTimer *iCloudUpdateTimer;



@end

@implementation AppDelegate



@synthesize menuController,rightMenu;

#pragma mark -
#pragma mark VARIABILI DI SISTEMA
//=========================
// VARIABILI DI SISTEMA
//=========================
-(BOOL)isIphone{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
}

-(BOOL)isIphone5{
    return ([[UIScreen mainScreen] bounds].size.height == 568);
}

-(NSUserDefaults*)defaults{
    
    NSUserDefaults *sharedDefaults = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    return sharedDefaults;
}

-(void)fontAvaible{
    NSArray *fontFamilies = [UIFont familyNames];
    for (int i = 0; i < [fontFamilies count]; i++)
    {
        NSString *fontFamily = [fontFamilies objectAtIndex:i];
        NSArray *fontNames = [UIFont fontNamesForFamilyName:[fontFamilies objectAtIndex:i]];
        NSLog (@"%@: %@", fontFamily, fontNames);
    }
}

//CACHE DIRECTORY
- (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}


-(BOOL)fileExistsAtPath:(NSString*)path{
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}


-(BOOL)isIos7orUpper{
    return [[[UIDevice currentDevice] systemVersion] floatValue]>=7;
}

#pragma mark -
#pragma mark METODI DI CLASSE
//=========================
// METODI DI CLASSE
//=========================
+ (AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 90000
- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    
    if([[UIDevice currentDevice].systemVersion floatValue]>=9.f){
        [ShortCutItem shortCutItemReceived:shortcutItem];
    }
}
#endif

#pragma mark -
#pragma mark didFinishLaunchingWithOptions
//=========================
// DID FINISH LANUNCHING
//=========================
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //[self fontAvaible];
    //[self showDocumentandCacheFolder];
    
    if([[UIDevice currentDevice].systemVersion floatValue]>=9.f){
        [ShortCutItem createShortcutItemsWithIcons];
    }

#warning setting DEBUG MODE
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IBREVIARY_DEBUG_MODE];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    if([self.defaults boolForKey:AGGIORNAMENTO_2015])
    {
        [[NSOperationQueue new] addOperationWithBlock:^{ [CheckVersion checkVersion]; }];
    }
    


    [self checkSettingBundle];

    // Nuova gestione remote notifications
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        // Setup each action thar will appear in the notification
        UIMutableUserNotificationAction *acceptAction = [[UIMutableUserNotificationAction alloc] init];
        acceptAction.identifier = @"ACCEPT_ACTION"; // Identifier is returned in handleActionWithIdentifier: after the user taps on an action
        acceptAction.title = @"ACCEPT_TITLE";
        acceptAction.activationMode = UIUserNotificationActivationModeForeground; //Brings the app into the foreground when action tapped
        
        UIMutableUserNotificationCategory *not_cat = [[UIMutableUserNotificationCategory alloc] init];
        not_cat.identifier = @"iBreviary_category";
        
        [not_cat setActions:@[acceptAction] forContext:UIUserNotificationActionContextDefault | UIUserNotificationActionContextMinimal];
        
        
        NSSet *categories = [NSSet setWithObjects:not_cat, nil];

        UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:categories];
        [application registerUserNotificationSettings:notificationSettings];
        [application registerForRemoteNotifications];
        
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    } else
   
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    
    //disabilita LockScreen
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];

    //[self fontAvaible];
    //[NSThread sleepForTimeInterval:0.5];
    
    CGRect frame = [[UIScreen mainScreen] bounds];
      self.window = [[UIWindow alloc] initWithFrame:frame];
    
    UIGraphicsBeginImageContext(self.window.frame.size);
    [[UIImage imageNamed:@"PaperHD.png"] drawInRect:self.window.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.window.backgroundColor = [UIColor colorWithPatternImage:image];

    // Override point for customization after application launch.
    self.viewController = [Home new];
    
    menuController = [[DDMenuController alloc] initWithRootViewController:self.viewController];
    [menuController setDelegate:self.viewController];
    
    rightMenu = [Settings new];
    // NSLog(@"righMenu:%@",rightMenu);

    
    MenuViewController *leftMenu = [MenuViewController sharedInstance];
    MyNavigationCTRL *leftNav = [[MyNavigationCTRL alloc] initWithRootViewController:leftMenu];

    
    menuController.leftViewController = leftNav;
    menuController.rightViewController = rightMenu;
    
        
    [self.window setRootViewController:menuController];
    [self.window makeKeyAndVisible];
    
    //->App started with localNotification fired
    if (launchOptions) //launched from notification
    {
        UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        //NSLog(@"->App started with localNotification(%@)fired",[localNotif.userInfo objectForKey:@"id"]);
        if(localNotif){
            [[NSOperationQueue new] addOperationWithBlock:^{ [Home mostraNotifca:localNotif];}];

        }
        NSURL *url = [launchOptions objectForKey: UIApplicationLaunchOptionsURLKey];
        if(url){
            // NSLog(@"URL-scheme query detected on lunch ?!??");
            [[NSOperationQueue new] addOperationWithBlock:^{ [Home showTodayItem:[url query]];}];
        }
        
    if([[UIDevice currentDevice].systemVersion floatValue]>=9.f){
            //SHORTCUT SUPPORT:
            UIApplicationShortcutItem *item = [launchOptions valueForKey:UIApplicationLaunchOptionsShortcutItemKey];
            if (item) {
                NSLog(@"We've launched from shortcut item: %@", item.localizedTitle);
                [ShortCutItem shortCutItemReceived:item];
                }
        }
        
    }

    return YES;
}


+(void)debugDeleteiCLoud:(UIButton*)sender{
    //iCloud aggiornamento
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"use_icloud_storage"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)showDocumentandCacheFolder{
    // where are doc and cache???
    NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    NSLog(@"Cache Directory: %@", cachePath);
}

-(void)checkSettingBundle{
    //Da fare con standardDefault NO group!!
    NSUserDefaults *d = [NSUserDefaults standardUserDefaults];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    if(![[d objectForKey:@"version"] isEqualToString:version] || ![[d objectForKey:@"build"] isEqualToString:build]){
        [[NSUserDefaults standardUserDefaults] setObject:version forKey:@"version"];
        [[NSUserDefaults standardUserDefaults] setObject:build forKey:@"build"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark -
#pragma mark OPEN_URL SCHEME
//=========================
// APP OPEN URL SCHEME
//=========================
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    // NSLog(@"Calling Application Bundle ID: %@", sourceApplication);
    // NSLog(@"URL scheme:%@", [url scheme]); //iBreviary
    // NSLog(@"URL host: %@", [url host]); //mostra
    // NSLog(@"URL query: %@", [url query]); //q=oraMedia
    
    __block NSURL *u = url;
    
    //Salvo url in WIDGET_CMD
    [self.defaults setObject:[url query] forKey:WIDGET_CMD];
    [self.defaults synchronize];

    NSBlockOperation *step1 = nil;
    NSUserDefaults *d = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    if([d boolForKey:AUTODOWNLOAD_OGGI]){
        // STEP 1
        step1 = [NSBlockOperation blockOperationWithBlock:^{
            // NSLog(@"applicationopenURL: step(1)");
            [Day checkAutoOggi:getting_foreground];
        }];//fine step1
    }
    
    
    // STEP 2
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"applicationopenURL: step(2):%@",u);
        [self waintingForHomeTopenURL:u];
    }];//fine step2
    
    if(step1!=nil){
        [step2 addDependency:step1];
        [[NSOperationQueue mainQueue] addOperation:step1];
    }
    
    // STEP 3
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"applicationopenURL: step(3) finiti processi rimuovo Network");
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];//fine step3
    
    [step3  addDependency:step2];
    [[NSOperationQueue new] addOperation:step2];
    [[NSOperationQueue mainQueue] addOperation:step3];
   
    return YES;
}


-(void)waintingForHomeTopenURL:(NSURL *)url{
    
    }

//=======================
//NOTIFICHE->NotificheObj
//=======================
#pragma mark - NOTIFICHE->NotificheObj
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    // NSLog(@">>>>didReceiveLocalNotification<<<<");
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateInactive) {
        NSLog(@"didReceiveLocalNotification[BACKGROUND]:devo mostrare la notifica direttamente e aggiornare il badge");
        [[NSOperationQueue new] addOperationWithBlock:^{ [Home mostraNotifca:notification];}];

    } else {
        NSLog(@"didReceiveLocalNotification[FOREGROUND]:devo mostrare il bannerNotifica e aggiornare il badge");
        [[NSOperationQueue new] addOperationWithBlock:^{ [Home applicationDidReciveLocalNotificationWhileForeground:notification];}];
    }
    
}

#ifdef __IPHONE_8_0
//Called for local notification action events
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler
{
    // NSLog(@"AppDelegate - handleActionWithIdentifier local: %@", identifier);
    
    //must call completion handler when finished
    completionHandler();
}
//Called for remote notification action events
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler
{
    // NSLog(@"AppDelegate - handleActionWithIdentifier from server: %@", identifier);
    
    //must call completion handler when finished
    completionHandler();
}

#endif

/*
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    [NotificaObject application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [NotificaObject application:application didReceiveRemoteNotification:userInfo];
}
*/

//====================
//APPLICATION DELEGATE
//====================
#pragma mark - UIAPPLICATION DELEGATE
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    if([[NSUserDefaults standardUserDefaults] boolForKey:AGGIORNAMENTO_2015]){
        [self.persistentStack savePrivateContext];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
    if([[NSUserDefaults standardUserDefaults] boolForKey:AGGIORNAMENTO_2015])
        [self saveContext];
     */
    // STEP 1
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"applicationWillEnterForeground: step(1)");
        [Day checkAutoOggi:getting_foreground];
    }];//fine step1
    
    // STEP 2
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"applicationWillEnterForeground: step(2)");
        [SystemData checkLASTUPDATE];
    }];//fine step2
    
    // STEP 3
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"applicationWillEnterForeground: step(4) finiti processi rimuovo Network");
        [NotificaObject checkNotifiche];
    }];//fine step4
    
    
    if(![self.defaults objectForKey:WIDGET_CMD]){
        [step2 addDependency:step1];
        [[NSOperationQueue new] addOperation:step1];
    }
    [step3 addDependency:step2];
    [[NSOperationQueue new] addOperation:step2];
    [[NSOperationQueue new] addOperation:step3];

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // NSLog(@"applicationDidBecomeActive");
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if(![self.defaults boolForKey:AGGIORNAMENTO_2015])
    {
        // NSLog(@"prima AGGIORNAMENTO_2015");
        [SystemData aggiornamento2015];
    }
    // NSLog(@"AGGIORNAMENTO_2015:%@",[self.defaults boolForKey:AGGIORNAMENTO_2015]?@"YES":@"NO");

    //RESETTO BADGE
    
    if ([GlobalFunction checkNetworkStatus])
        {
        if([self.defaults boolForKey:AGGIORNAMENTO_2015])
            {
                // STEP 1
                NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
                    // NSLog(@"DidBecomeActive: step(1)");
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                }];//fine step1
                
                // STEP 2
                NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
                    // NSLog(@"DidBecomeActive: step(2)");
                    [Day checkAutoOggi:getting_active];
                }];//fine step2

                // STEP 3
                NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
                    // NSLog(@"DidBecomeActive: step(3)");
                    [self checkGiorniSalvati];
                }];//fine step3

                // STEP 4
                NSBlockOperation *step4 = [NSBlockOperation blockOperationWithBlock:^{
                    // NSLog(@"DidBecomeActive: step(4)");
                    [SystemData checkLASTUPDATE];
                }];//fine step4

                // STEP 5
                NSBlockOperation *step5 = [NSBlockOperation blockOperationWithBlock:^{
                    // NSLog(@"DidBecomeActive: step(5)");
                    [NotificaObject checkNotifiche];
                }];//fine step5

                // STEP 6
                NSBlockOperation *step6 = [NSBlockOperation blockOperationWithBlock:^{
                    // NSLog(@"DidBecomeActive: step(6)");
                    [self checkRating];
                }];//fine step6
                
                // STEP 7 iCloudStep
                NSBlockOperation *step7 = [NSBlockOperation blockOperationWithBlock:^{
                    // NSLog(@"DidBecomeActive: step(6)");
                    [self.persistentStack saveContext];
                }];//fine step6

                // STEP 7
                NSBlockOperation *step8 = [NSBlockOperation blockOperationWithBlock:^{
                    // NSLog(@"DidBecomeActive: step(7) rimuovo NetworkAttivity");
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    
                    // Debug Multi Notifica
                    /*
                    NotificaObject *n = [NotificaObject notificaConId:@39 inContext:[AppDelegate mainManagedObjectContext]];
                    [Home showNotifica:n];
                    [Home showNotifica:n];
                    [Home showNotifica:n];
                     // */
                }];//fine step7

                [step4 addDependency:step3];
                [step5 addDependency:step4];
                [step6 addDependency:step5];
                [step7 addDependency:step6];
                [[NSOperationQueue new] addOperation:step1];
                if(![self.defaults objectForKey:WIDGET_CMD]){
                    [step2 addDependency:step1];
                    [step3 addDependency:step2];
                    [[NSOperationQueue new] addOperation:step2];
                }
                else [step3 addDependency:step1];
                [[NSOperationQueue new] addOperation:step3];
                [[NSOperationQueue new] addOperation:step4];
                [[NSOperationQueue new] addOperation:step5];
                [[NSOperationQueue new] addOperation:step6];
                [[NSOperationQueue mainQueue] addOperation:step7];
                [[NSOperationQueue mainQueue] addOperation:step8];
            }
        }
     [UIApplication sharedApplication].applicationIconBadgeNumber = [NotificaObject notificheDaleggere];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // NSLog(@"applicationDidEnterBackground");
    runOnMainQueueWithoutDeadlocking(^{
        [self.persistentStack savePrivateContext];
        [UIApplication sharedApplication].applicationIconBadgeNumber = [NotificaObject notificheDaleggere];
    });

    [DDMenuController closeMenuWithCompletion:^{ ;/*NSLog(@"Chiuso prima di entrare in background");*/ }];
    
    // BACK PROCESS entering BACKGROUND
    NSBlockOperation *back_process = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"applicationDidEnterBackground: step(1)");
        
        //RESET BOOK VIEW FLAG
        [Home resetBookViewBool];
        
        [Day checkAutoOggi:getting_background];
    }];
    
    [[NSOperationQueue new] addOperation:back_process];
    
    //Notifica dopo 2 secondi
//    [NotificaObject sendLocalNotificationTestAfterSecond:1];
//    [NotificaObject sendLocalNotificationTestAfterSecond:2];
//    [NotificaObject sendLocalNotificationTestAfterSecond:3];
//    [NotificaObject sendLocalNotificationTestAfterSecond:4];
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // NSLog(@"applicationWillTerminate");
    [self.persistentStack saveContext];
    [UIApplication sharedApplication].applicationIconBadgeNumber = [NotificaObject notificheDaleggere];
}

#pragma mark CORE-DATA STACK
//=========================
// CORE-DATA STACK
//=========================
// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
+ (NSManagedObjectContext *)mainManagedObjectContext{
    return [[AppDelegate sharedAppDelegate] managedObjectContext];
}

+ (NSManagedObjectContext *)privateQueueContext{
    //dovrebbe essere utilizzato solo da notifiche
    return [[AppDelegate sharedAppDelegate] privateQueueContext];
}

- (NSManagedObjectContext *)managedObjectContext
{
    if (!self.persistentStack) {
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"iBreviary" withExtension:@"momd"];

        self.persistentStack = [[PersistentStack alloc] initWithStoreURL:self.storeURL modelURL:modelURL];
        
    }
    return self.persistentStack.managedObjectContext;
}


- (NSManagedObjectContext *)privateQueueContext
{
    
    if(!self.persistentStack){
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"iBreviary" withExtension:@"momd"];
        self.persistentStack = [[PersistentStack alloc] initWithStoreURL:self.storeURL modelURL:modelURL];
        }
        
    return self.persistentStack.privateQueueContext;
}

+ (void)saveContext{
    [[[AppDelegate sharedAppDelegate] persistentStack] saveContext];
}

+ (void)savePrivateContext{
    [[[AppDelegate sharedAppDelegate] persistentStack] savePrivateContext];
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.


// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.


#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

//====================
//CHECK RATE
//====================
#pragma mark - CHECK RATE
-(void)checkRating{
    [Appirater setAppId:APPLE_ID];
    [Appirater setDaysUntilPrompt:-1];
    [Appirater setUsesUntilPrompt:5];
    [Appirater setTimeBeforeReminding:2];
    [Appirater setDebug:NO];
    [Appirater appLaunched:YES];
    // NSLog(@"App rater!");
}

//====================
//CHECK GIORNI SALVATI
//====================
#pragma mark - CHECK GIORNI SALVATI
-(void)checkGiorniSalvati{
    // NSLog(@"Appdelegate:checkGiorniSalvati (deleting < ieri)");
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *dayList = [fileManager contentsOfDirectoryAtPath:[self.cacheDir stringByAppendingPathComponent:PATH_DAY] error:nil];
    
    for (NSString *tString in dayList)
    {
        NSArray *components=[tString componentsSeparatedByString:@"_"];
        NSDateFormatter *f = [NSDateFormatter new];
        [f setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [f dateFromString:[components objectAtIndex:0]];
        
        // NSLog(@"COMPARE[%@ to %@]:%i",[f stringFromDate:date],[f stringFromDate:[[NSDate date] dateByAddingTimeInterval: -86400.0]],[[f stringFromDate:date] compare:[f stringFromDate:[NSDate date]]]);
        
        if([[f stringFromDate:date] compare:[f stringFromDate:[[NSDate date] dateByAddingTimeInterval: -(60*60*24)]]] <0)
        {
            // NSLog(@"cancellando:%@",tString);
            NSError *errore = nil;
            if([Day deleteDayWithName:tString andError:&errore]){
                            // NSLog(@"Appdelegate: %@ cancellato.",tString);
                            }
            if(errore){
                // NSLog(@"Appdelegate: Errore cancellando %@ :%@",tString,[errore description]);
                }
        }
    }
    
    NSArray *pdfList = [fileManager contentsOfDirectoryAtPath:self.cacheDir error:nil];
    
    for(NSString *pdf_name in pdfList){
        if([pdf_name.pathExtension isEqualToString:@"pdf"]){
            NSString *deletepath = [self.cacheDir stringByAppendingPathComponent:pdf_name];
            NSFileManager *fm = [NSFileManager defaultManager];
            [fm removeItemAtPath:deletepath error:nil];
        }
            
    }
}

- (NSURL*)storeURL
{
    NSURL* documentsDirectory = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:NULL];
    return [documentsDirectory URLByAppendingPathComponent:@"iBreviary.sqlite"];
}


#pragma mark -
#pragma mark Activity Network
//=========================
// Activity Network
//=========================
-(void)activityNetwork:(BOOL)b{
    [self performSelectorOnMainThread:b?@selector(attivaRotella):@selector(disattivaRotella) withObject:nil waitUntilDone:YES];
}

-(void)attivaRotella{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

-(void)disattivaRotella{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}


@end
