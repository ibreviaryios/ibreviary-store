//
//  NotaCTRL.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "NotaCTRL.h"
#import "Home.h"
#import "WebBrowser.h"

@interface NotaCTRL()

@property (nonatomic,assign) CGFloat keyboardOffset;

@end


@implementation NotaCTRL


#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
-(id)initWithSelection:(NSString*)s andItem:(Item *)i
{
    //iPhone4
    NSString *nibNameOrNil = @"NotaCTRL";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"NotaCTRL_ipad";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"NotaCTRL5";
    if(IS_IPHONE_6) nibNameOrNil = @"NotaCTRL6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"NotaCTRL6Plus";
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        self.textSecelction = s;
        if(i) self.sender_item = i;
        self.keyboardOffset = 0;
        self.state = newComment_spiritualState;
    }
    return self;
}



-(id)initNewNotaWithDelegate:(BaseController<NewNotaDelegate>*)d
{
    //iPhone4
    NSString *nibNameOrNil = @"NotaCTRL";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"NotaCTRL4PopOver";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"NotaCTRL5";
    if(IS_IPHONE_6) nibNameOrNil = @"NotaCTRL6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"NotaCTRL6Plus";
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        self.textSecelction = @"";
        self.sender_item = nil;
        self.delegate = d;
        self.keyboardOffset = 0;
        self.state = newNota_spiritualState;
        // register for keyboard notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
        }
    return self;
}

-(id)initWithSelection:(NSString*)s andItem:(Item *)i withDelegate:(BaseController<NewNotaDelegate>*)d{
    NSString *nibNameOrNil = @"NotaCTRL4PopOver";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        self.textSecelction = s;
        self.sender_item = i;
        self.delegate = d;
        self.keyboardOffset = 0;
        self.state = newNota_spiritualState;
        // register for keyboard notifications
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    }
    return self;
}


-(id)initWithNota:(Nota*)n andModDelegate:(BaseController<ModNotaDelegate>*)d
{
    //iPhone4
    NSString *nibNameOrNil = @"NotaCTRL";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"NotaCTRL4PopOver";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"NotaCTRL5";
    if(IS_IPHONE_6) nibNameOrNil = @"NotaCTRL6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"NotaCTRL6Plus";

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        self.nota = n;
        self.textSecelction = self.nota.text;
        self.mod_delegate = d;
        self.keyboardOffset = 0;
        self.state = modNota_spiritualState;
    }
    return self;
}


//=========================
// StatusBar
//=========================
#pragma mark - StatusBar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

//cambio colore statusbar in bianco
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
-(IBAction)FineNotaCTRL:(UIButton*)sender{
    // NSLog(@"FineNotaCTRL");
    
    [self.text_view resignFirstResponder];
    
    switch (self.state) {
        case newComment_spiritualState:
            self.nota = [Nota addNotaFromCopyAction:self.text_view.text andItem:self.sender_item forContext:[AppDelegate mainManagedObjectContext]];
            ;
            [Home dismissNotaCTRL];
            break;
        case newNota_spiritualState:
            if(self.text_view.text.length>0)
            self.nota = [Nota addNotaFromCopyAction:self.text_view.text andItem:self.sender_item forContext:[AppDelegate mainManagedObjectContext]];
            
            [self.delegate dismissNewMyNotaCtrl:self andNota:(self.text_view.text.length>0)?self.nota:nil];

            break;
        case modNota_spiritualState:
            [self.nota setText:self.text_view.text];
            [self.nota saveInContext:[AppDelegate mainManagedObjectContext]];
            
            [self.mod_delegate dismissModMyNotaCtrl:self andNota:self.nota mod:YES];
            break;
            
        default:
            break;
    }

}

//=========================
// FINE
//=========================
-(IBAction)AnnullaNotaCTRL:(UIButton*)sender{
    // NSLog(@"FineNotaCTRL");
    [self.text_view resignFirstResponder];
    switch (self.state) {
        case newComment_spiritualState:
                [Home dismissNotaCTRL];
            break;
        case newNota_spiritualState:
            [self.delegate dismissNewMyNotaCtrl:self andNota:nil];
            
            break;
        case modNota_spiritualState:
            [self.nota saveInContext:[AppDelegate mainManagedObjectContext]];
            [self.mod_delegate dismissModMyNotaCtrl:self andNota:self.nota mod:NO];
            break;
            
        default:
            break;
    }
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
-(void)viewDidLoad {
    [super viewDidLoad];

    [self gui];
    [self.text_view setText:self.textSecelction];
    [self calculateQuotePosition];

    if(![super isIpad]) {
        [self.text_mask applyFadeTop:YES bottom:YES andPercentage:@0.05];
    }
}

-(void)gui{
    //CLOSE BTN
    [self.close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
    [self.annulla setTitle:NSLocalizedString(@"Annulla", nil) forState:UIControlStateNormal];
    
    //TITLE SETUP
    [self.titolo setFont:[UIFont fontWithName:FONT_NOTA size:self.isIpad?30:18]];
    
    //TEXTVIEW SETUP
    [self.text_view setFont:[UIFont fontWithName:FONT_NOTA size:self.isIpad?45:20]];
    self.text_view.tintColor = [UIColor whiteColor];
    
    //QUOTE
    UIEdgeInsets edgeInsets = self.isIpad?UIEdgeInsetsMake(72, 100, 72, 100):UIEdgeInsetsMake(31, 43, 31, 43);
    [self.quote setImage:[[UIImage imageNamed: self.isIpad?@"QuoteBackgroundADD.png":@"QuoteBackgroundADD_iphone.png"] resizableImageWithCapInsets:edgeInsets  resizingMode:UIImageResizingModeStretch]];
}

-(BOOL)isIpad{
    BOOL risp = super.isIpad;
    if(self.state!=newComment_spiritualState) risp = NO;
    NSLog(@"custom isIpad:%i",risp);
    return risp;
}

#pragma mark -
#pragma mark calculateQUOTEPosition
//=================================
// calculateQUOTEPosition
//=================================
-(void)calculateQuotePosition{
    if(self.sender_item == nil || (self.text_view.text.length<3)){
        [self.quote setHidden:YES];
        return;
    }
    CGFloat fixedWidth = self.text_view.frame.size.width;
    CGRect b  = [self.text_view.text boundingRectWithSize:CGSizeMake(fixedWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_NOTA size: self.text_view.font.pointSize]} context:nil];

    
    CGSize newSize = b.size; //[self.text_view sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    newSize.height += self.isIpad?20:15;
    
    //NSLog(@"newSize:%@",NSStringFromCGSize(newSize));
    //HEIGHT
    CGFloat offset = self.isIpad?144.f:62.f;
    if(newSize.height < offset)
        newSize.height = offset;
    if(newSize.height > self.text_view.frame.size.height)
        newSize.height = self.text_view.frame.size.height;
    //WIDTH
    if(!self.isIpad){
        newSize.width -=10;
    }
    CGFloat offset_width = self.isIpad?200.f:86.f;
    if(newSize.width < offset_width)
        newSize.width = offset_width;

    [self.quote setFrame:CGRectMake(0, self.quote.frame.origin.y, newSize.width,newSize.height+(10))];
    [self.quote setCenter:CGPointMake(self.view.center.x, self.quote.center.y)];
    //NSLog(@"\nquote %@\view %@\ntext %@\n",NSStringFromCGPoint(self.quote.center),NSStringFromCGPoint(self.view.center),NSStringFromCGPoint(self.text_view.center));
}

-(void)viewWillAppear:(BOOL)animated{
    // NSLog(@"NotaCTRL:viewWillAppear");
    [super viewWillAppear:animated];
    switch (self.state) {
        case modNota_spiritualState:
        case newNota_spiritualState:{
                [self resize];
                //[self.view setAlpha:0];
                break;
        }
        default:
            break;
    }
    [self.text_view becomeFirstResponder];
}

- (void)keyboardWasShown:(NSNotification *)nsNotification {
    NSDictionary *userInfo = [nsNotification userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.keyboardOffset = kbSize.height;
    /*
    CGRect r = CGRectZero;
    r.origin = self.view.frame.origin;
    r.size = self.view.frame.size;
    r.size = [[[Home sharedInstance] mySpiritualBook_popover] popoverContentSize];
    r.size.height  = r.size.height - kbSize.height;
    NSLog(@"Height: %f Width: %f w:size%@", kbSize.height, kbSize.width,NSStringFromCGRect(r));
    [self.view setFrame:(CGRect){0,0,r.size.width,r.size.height}];
    CGRect t_mask_frame = self.text_mask.frame;
    t_mask_frame.size.height = self.view.frame.size.height - self.text_mask.frame.origin.y;
    [self.text_mask setFrame:(t_mask_frame)];
    CGRect t_frame = self.text_view.frame;
    t_frame.size.height = self.view.frame.size.height - self.text_view.frame.origin.y- 20;
    [self.text_view setFrame:(t_frame)];
     */
    [self resize];

    [self calculateQuotePosition];

    // Portrait:    Height: 264.000000  Width: 768.000000
    // Landscape:   Height: 352.000000  Width: 1024.000000
}


-(void)resize{
    if([super isIpad]){
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        
        CGRect r = CGRectZero;
        r.size = self.view.frame.size;
        UIPopoverController * pop = [[Home sharedInstance] mySpiritualBook_popover];
        r.size = [pop popoverContentSize];
        r.size.height  = r.size.height - self.keyboardOffset;
        // NSLog(@"setting size:%f",r.size.height);
        [self.view setFrame:(CGRect){0,0,r.size.width,r.size.height}];
        CGRect t_mask_frame = self.text_mask.frame;
        t_mask_frame.size.height = self.view.frame.size.height - self.text_mask.frame.origin.y;
        [self.text_mask setFrame:(t_mask_frame)];
        CGRect t_frame = self.text_view.frame;
        t_frame.size.height = -100 + self.view.frame.size.height - self.text_view.frame.origin.y ;
        [self.text_view setFrame:(t_frame)];
        [self calculateQuotePosition];
        
    }];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    // NSLog(@"NotaCTRL:viewDidAppear");
    [super viewDidAppear:animated];
}

#pragma mark - Disappear
//===================
//DISAPPEAR
//===================
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.text_view resignFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}


#pragma mark - UITextViewDelegate
//=========================
// UITextViewDelegate
//=========================
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    // NSLog(@"textViewShouldBeginEditing:");
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    // NSLog(@"textViewDidBeginEditing:");
    [self calculateQuotePosition];
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    // NSLog(@"textViewShouldEndEditing:");
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    // NSLog(@"textViewDidEndEditing:");
    [self calculateQuotePosition];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{

    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    // NSLog(@"textViewDidChange:");
    [self calculateQuotePosition];
}


#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
