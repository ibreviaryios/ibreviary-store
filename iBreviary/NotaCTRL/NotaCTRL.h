//
//  NotaCTRL.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "Nota.h"
#import "Item.h"

typedef enum : NSUInteger {
    newComment_spiritualState,
    newNota_spiritualState,
    modNota_spiritualState,
} MySpiritualState;

@protocol NewNotaDelegate <NSObject>

-(void)dismissNewMyNotaCtrl:(id)sender andNota:(Nota*)new_nota;

@end

@protocol ModNotaDelegate <NSObject>

-(void)dismissModMyNotaCtrl:(id)sender andNota:(Nota*)new_nota mod:(BOOL)moficitata;

@end


@interface NotaCTRL : BaseController<UITextViewDelegate>

//STRUTTURA
@property (nonatomic, strong) Nota *nota;
@property (nonatomic, strong) NSString *textSecelction;
@property (nonatomic, strong) Item *sender_item;

@property (nonatomic, assign)CGRect rect;

//GUI
@property (nonatomic, strong) IBOutlet UIImageView *quote;
@property (nonatomic, strong) IBOutlet UILabel *titolo;
@property (nonatomic, strong) IBOutlet UIView *text_mask;
@property (nonatomic, strong) IBOutlet UITextView *text_view;
@property (nonatomic, strong) IBOutlet UIButton *close;
@property (nonatomic, strong) IBOutlet UIButton *annulla;
@property (nonatomic, assign) MySpiritualState state;
@property (nonatomic, strong) BaseController<NewNotaDelegate> *delegate;
@property (nonatomic, strong) BaseController<ModNotaDelegate> *mod_delegate;



-(id)initWithSelection:(NSString*)s andItem:(Item*)i;
-(id)initNewNotaWithDelegate:(BaseController<NewNotaDelegate>*)d;
-(id)initWithNota:(Nota*)n andModDelegate:(BaseController<ModNotaDelegate>*)d;
-(id)initWithSelection:(NSString*)s andItem:(Item *)i withDelegate:(BaseController<NewNotaDelegate>*)d;

@end

