//
//  Home.h
//  iBreviary
//
//  Created by leyo on 23/01/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import "BaseController.h"
#import "CustodiaTS.h"
#import "TerraSancta.h"
#import "Credits.h"
#import "Info.h"
#import "GuidaPreghiera.h"
#import "Principi.h"
#import "Lista.h"
#import "Day.h"
#import "Messa.h"
#import "EventoFlag.h"
#import "Lingua.h"
#import "FXBlurView.h"
#import "SantoViewCTRL.h"
#import "VoiceSelect.h"
#import "NotificaCTRL.h"
#import "Tabella_notificheHome.h"
#import "MessaLista.h"
#import "TitoloMessa.h"
#import "SelettoreMessa.h"
#import "Editor.h"
#import "DDMenuController.h"
#import "MainWebView.h"
#import "BNHtmlPdfKit.h"

@protocol MessaMenuDelegate <NSObject>
-(void)caricaMessaMenuEdit:(BOOL)edit;
-(void)forzaMessaInEditor;
-(void)rimuoviMessaMenu;
@end


@interface Home : BaseController<DDMenuControllerDelegate,CustodiaTSDelegate,TerraSanctaDelegate,InfoDelegate,CreditsDelegate,PrincipiDelegate,GuidaPreghieraDelegate,LinguaDelegate,ListaDelegate,VoiceSelectDelegate,UIGestureRecognizerDelegate,AVSpeechSynthesizerDelegate,UIScrollViewDelegate,SantoViewCTRLDelegate,NotificaCTRLDelegate,Tabella_notificheHomeDelegate,UIPopoverControllerDelegate,EventFlagDelegate,TitoloMessaDelegate,TitoloEditMessaDelegate,SelettoreMessaDelegate,EditorDelegate>

// ROTELLA-HUD
@property (nonatomic) UIView *HUD;
@property (nonatomic) UILabel *testo_hud;

@property (strong, nonatomic) IBOutlet UIView *area_attribute;
@property (strong, nonatomic) IBOutlet UIView *area_santo;
@property (strong, nonatomic) IBOutlet UIView *area_centrale;
@property (strong, nonatomic) IBOutlet UIView *area_bassa;
@property (strong, nonatomic) IBOutlet UILabel *titolo_data;

//->NAVBAR
@property (strong, nonatomic) IBOutlet UIView *NAVBAR;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *rotellaFUllScreen;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *rotellaSettaHome;
@property (strong, nonatomic) IBOutlet UIView *subHomeAcitivy;


//FULL SCREEN
@property (strong, nonatomic) IBOutlet UIButton *fullOutOnText_btn;
@property (strong, nonatomic) IBOutlet UIButton *fullOutOnHome_btn;
@property (strong, nonatomic) IBOutlet UIButton *fullINOnNav_btn;
@property (strong, nonatomic) IBOutlet UIView *fullOnNavArea;


//Info CTRL
@property (strong, nonatomic) Info *infoCTRL;
@property (strong, nonatomic) CustodiaTS *custodia;
@property (strong, nonatomic) TerraSancta *terraSancta;
@property (strong, nonatomic) Credits *creditsCTRL;
@property (strong, nonatomic) GuidaPreghiera *guida;
@property (strong, nonatomic) Principi *principi;

//->LISTA
@property (strong, nonatomic) Lista *listaCTRL;
@property (strong, nonatomic) UIPopoverController *lista_popover;


//->EVENT-FLAG
@property (strong, nonatomic) EventoFlag *eventCTRL;
@property (strong, nonatomic) UIPopoverController *event_popover;
@property (strong, nonatomic) IBOutlet UIButton *eventFLAG;
@property (strong, nonatomic) IBOutlet UIButton *eventFLAG2;

//->BLUR_VIEW
@property (strong, nonatomic) UIImageView *fake_home_blur;
@property (strong, nonatomic) FXBlurView *blurView;
@property (strong, nonatomic) FXBlurView *blurView_listaNotifiche;

//->LINGUA
@property (strong, nonatomic) Lingua *linguaCTRL;
@property (strong, nonatomic) IBOutlet UIButton *btn_lingua;
//->DAY
@property (strong, nonatomic) Day *day;

//->HOME
@property (strong, nonatomic) IBOutlet UIImageView *sfondo_home;
@property (strong, nonatomic) IBOutlet UIView *HOME;
@property (strong, nonatomic) IBOutlet UIButton *custodia_btn;
@property (strong, nonatomic) IBOutlet UIButton *terrasanta_btn;
@property (strong, nonatomic) IBOutlet UIButton *info_btn;
//->NAVBAR - BTN
@property (strong, nonatomic) IBOutlet UIButton *oggi_btn;
@property (strong, nonatomic) IBOutlet UIButton *oggi2_btn;
@property (strong, nonatomic) IBOutlet UIButton *prega_btn;


//---DettaglioView (HOME)
@property (strong, nonatomic) IBOutlet UIView *dettagliHOME;
@property (strong, nonatomic) IBOutlet UIImageView *logo;
@property (strong, nonatomic) IBOutlet UIView *linea_dayInfo;
@property (strong, nonatomic) IBOutlet UILabel *titolo_ufficiale;
@property (strong, nonatomic) IBOutlet UILabel *label_day_time;
@property (strong, nonatomic) IBOutlet UILabel *label_day_time_tit;
@property (strong, nonatomic) IBOutlet UILabel *label_day_type;
@property (strong, nonatomic) IBOutlet UILabel *label_day_type_tit;
@property (strong, nonatomic) IBOutlet UILabel *label_lingua;
@property (strong, nonatomic) IBOutlet UILabel *label_lingua_tit;
@property (strong, nonatomic) IBOutlet UIImageView *day_info_TP_logo;
@property (strong, nonatomic) IBOutlet UILabel *day_info_TP_title;

//--NOTIFICHE
@property (strong, nonatomic) IBOutlet UIButton *notifica_btn;
@property (strong, nonatomic) IBOutlet UIView *notifica_view;
@property (strong, nonatomic) UILabel *badge;
@property (strong, nonatomic) UIActivityIndicatorView *activity_badge;
@property (strong, nonatomic) NotificaCTRL *notificaCTRL;
@property (strong, nonatomic) Tabella_notificheHome *tabella_notificheCTRL;

//->VOICE SELECTOR
@property (strong, nonatomic) IBOutlet UIViewController *voice_selector;

//---SantoView (HOME) SCROLLER
@property (strong, nonatomic) IBOutlet UIView *SantoView;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UIPageControl *pager;
@property (strong, nonatomic) IBOutlet UIButton *santo_btn;
@property (strong, nonatomic) SantoViewCTRL *santoCTRL;

//->CONTENT
@property (strong, nonatomic) IBOutlet UIView *CONTENT;
@property (strong, nonatomic) IBOutlet UIImageView *sfondo_content;
@property (strong, nonatomic) IBOutlet MainWebView *webContent;
@property (strong, nonatomic) NSString *web_title;
@property (strong, nonatomic) NSString *web_text;
//CSS
@property (strong, nonatomic) IBOutlet UIView *sfondo_black;

//AUTO SCROLLER
@property (strong, nonatomic) IBOutlet UIView *autoscroller_area;
@property (strong, nonatomic) IBOutlet UIButton *autoscroll_PLAY_btn;
@property (strong, nonatomic) IBOutlet UIButton *autoscroll_STOP_btn;

//MESSA
@property (nonatomic, strong) BaseController <MessaMenuDelegate> *delegate_messamenu;
@property (strong, nonatomic) IBOutlet UIPopoverController *messa_popover;
@property (strong, nonatomic) IBOutlet MessaLista *messa_ctrl;
@property (strong, nonatomic) IBOutlet TitoloMessa *messa_titolo_ctrl;
@property (strong, nonatomic) IBOutlet SelettoreMessa *messa_selettore_ctrl;
@property (strong, nonatomic) IBOutlet Editor *messa_editor_ctrl;

//AUDIO
@property (strong, nonatomic) IBOutlet UIButton *audio_btn;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
@property (strong, nonatomic) AVSpeechSynthesizer *synthesizer;
#endif

@property (strong, nonatomic) IBOutlet UIView *audio_area;

@property (strong, nonatomic) IBOutlet UIView *menu2_ios6;

//MY SPIRITUAL
@property (strong, nonatomic) IBOutlet UIPopoverController *mySpiritualBook_popover;

//BOOK
@property (assign, nonatomic) BOOL iSForBook;

//->HOME ISTANCE
+(Home *)sharedInstance;
    
+(Day*)getCurrentDay;
-(void)caricaGiornoWithDay:(NSString*)dayFileName;
//->ITEM
+(void)mostraItem:(Item*)item;
+(void)reloadItem;
+(void)mostraHome;
    
//CHECK STATUS VIEW
+(BOOL)isHomeVisible;
+(BOOL)isContentVisible;

//->EVENT FLAG
+(void)modificaEventFlag;
+(void)passaAllaListaDownload;
//+(void)dismissEventIphone;

//->LISTA CTRL
//+(void)dismissListaIphone;

+(void)dismiss:(UIViewController*)v;

//DAY MENAGE
-(void)caricaGiornoWithIndex:(NSIndexPath*)indexPath;
+(void)caricaGiornoWithDayObj:(Day*)d;//used in Lingua.m

//SETTA GUI/OBJECT
-(void)settaHOME;

//SCROLLER CMD
+(void)setScrollSpeed:(NSInteger)i;
+(void)scrollerCTRL;


//AUDIO CMD
+(void)audioCTRL;

// Mostra Oggi
-(IBAction)mostraOggi:(UIButton*)sender;

//NOTIFICHE
+(void)aggiornaBadge;
+(void)mostraNotifca:(UILocalNotification*)notifica;
+(void)showNotifica:(NotificaObject*)notifica;
+(void)applicationDidReciveLocalNotificationWhileForeground:(UILocalNotification *)notification;

//NO_NET
+(void)noNetInHome;

//HUD
+(void)changeHudText:(NSString *)t;
-(void)updateHudLabel:(NSString*)t;
+(void)errorePrimoAggiornamento:(NSError *)err;

//HUD recupero cache
+(void)recuperaCacheMex:(NSString*)t;
+(void)fineRecuperaCache;

+(void)attivaRotellaHome;

//WIDGET MOSTRA
+(void)showTodayItem:(NSString*)pray_query;
+(void)showSantoAtPos:(NSString*)santo_query;

//MESSA
+(BOOL)isMessaINUSO;
+(void)mostraMessa:(Messa*)messa editMode:(BOOL)editMode;
+(Messa*)getCurrentMessa;
+(void)reloadMessa:(NSIndexPath*)indexPath;
+(BOOL)isMessaVisible;
+(void)messaGoToIndex:(NSIndexPath*)indexPath;
+(void)showMessaTitolo:(Messa*)m thenComune:(BOOL)comune;
+(void)showMessaEditor:(NSString*)attribute;
+(void)showMessaSelettoreWithType:(Tipo_messa_lista)t;
+(void)resetMessa;

//FULLSCREEN
-(void)checkFull;

//ADDNOTA
+(void)dismissNotaCTRL;

//MY SPIRITUAL BOOK
+(void)dismissMySpiritual;

//PDFBOOK
+(void)mostraPdfBookwithUrl:(NSString*)url;
+(BOOL)iSForBook;
+(BOOL)toggleBook;
+(void)updatePdfUrl:(NSString*)url;
+(void)reloadPdf;
+(void)dismissPdfPager;

//DEBUG
+(void)performDebug:(BOOL)debugFlag;
+(void)fineHUD2015;

+(void)showEditModificaTitolo:(Messa*)m;

+(void)resetBookViewBool;

@end
