//
//  MainWebView.h
//  iBreviary
//
//  Created by Leonardo Parenti on 20/03/15.
//  Copyright (c) 2015 leyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainWebView : UIWebView

@end
