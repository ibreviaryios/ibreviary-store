//
//  MainWebView.m
//  iBreviary
//
//  Created by Leonardo Parenti on 20/03/15.
//  Copyright (c) 2015 leyo. All rights reserved.
//

#import "MainWebView.h"

@implementation MainWebView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
   // NSLog(@"MAIN PerformAction");
    if (action == @selector(copy:))
    {
        return YES;
    }
    return NO; //[super canPerformAction:action withSender:sender];
}

@end
