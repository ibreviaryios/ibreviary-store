//
//  Tabella_notificheHome.h
//  iBreviary_PRO
//
//  Created by ileyo on 07/12/10.
//  Copyright 2010 leyo.breviario. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "NotificaObject.h"
#import "FXBlurView.h"


@protocol Tabella_notificheHomeDelegate <NSObject>
-(void)removeTabella_notificheHome;
-(void)selezionataNotificaDaMenu:(NotificaObject *)notifica;
@end


@interface Tabella_notificheHome : BaseController

    
@property (nonatomic, strong) BaseController<Tabella_notificheHomeDelegate> *delegate;
@property (strong, nonatomic) FXBlurView *blurView_listaNotifiche;
@property (strong, nonatomic) IBOutlet UIView *POP_area;
@property (strong, nonatomic) IBOutlet UIImageView *sfondo_pop_area;
@property (strong, nonatomic) NSArray *lista_notifiche;
@property (nonatomic) CGPoint popPosition;


- (id)initWithDelegate:(BaseController<Tabella_notificheHomeDelegate>*)d fromButtonCenter:(CGPoint)b;

@end
