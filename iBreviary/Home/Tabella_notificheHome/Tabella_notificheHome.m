//
//  Tabella_notificheHome.m
//  iBreviary_PRO
//
//  Created by ileyo on 07/12/10.
//  Copyright 2010 leyo.breviario. All rights reserved.
//

#import "Tabella_notificheHome.h"

@implementation Tabella_notificheHome

@synthesize POP_area, sfondo_pop_area, lista_notifiche,popPosition , delegate, blurView_listaNotifiche;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<Tabella_notificheHomeDelegate>*)d fromButtonCenter:(CGPoint)b
{
    NSString *nibNameOrNil;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) nibNameOrNil = @"Tabella_notificheHome_ipad";
    else
        nibNameOrNil = @"Tabella_notificheHome5";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        popPosition = CGPointMake(b.x-16, b.y+20);
        [self.view setFrame:d.view.frame];
    }
    return self;
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    lista_notifiche = [NotificaObject getNotifichePerHome];
    // NSLog(@"Tabella_notificheHome DidLoad:%@\n%@",NSStringFromCGPoint(popPosition),lista_notifiche);
    [self gui];
}

-(void)gui{
    
    CGFloat topoffset = 5;
    CGFloat row_height = 40;
    [POP_area setFrame:CGRectMake(popPosition.x, popPosition.y, POP_area.frame.size.width, (topoffset*2)+(row_height*lista_notifiche.count))];
    
    [sfondo_pop_area setFrame:CGRectMake(sfondo_pop_area.frame.origin.x-5, sfondo_pop_area.frame.origin.y-5, sfondo_pop_area.frame.size.width+10, POP_area.frame.size.height+19)];
    //top, left, bottom, right
    [sfondo_pop_area setImage:[[UIImage imageNamed:@"base_not_list.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(30, 30, 30, 30)]];
    for(int i = 0; i < lista_notifiche.count; i++)
    {
        NotificaObject *row = [lista_notifiche objectAtIndex:i];
        UIButton *bottone = [UIButton buttonWithType:UIButtonTypeCustom];
        [bottone setBackgroundColor:[UIColor clearColor]];
        [bottone setTag:i];
        [bottone setTitle:row.titolo forState:UIControlStateNormal];
        [bottone setTitleColor:[UIColorFromRGB(0XFFFFFF) colorWithAlphaComponent:0.95] forState:UIControlStateNormal];
        [bottone setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8] forState:UIControlStateHighlighted];
        [bottone setImage:[UIImage imageWithImage:[UIImage imageNamed:@"tabella_notifiche_row1.png"] scaledToSize:CGSizeMake(15, 11)] forState:UIControlStateNormal];
        [bottone setAdjustsImageWhenHighlighted:NO];
        [bottone setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 20.0, 0.0, 0.0)];
        [bottone setImageEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)];
        [bottone.titleLabel setFont:[UIFont fontWithName:row.giaLetta?FONT_HELVETICANEUE_REGULAR:FONT_HELVETICANEUE_BOLD size:14]];
        [bottone setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [bottone setFrame:CGRectMake(0, topoffset+(i*row_height)+5, POP_area.frame.size.width, row_height-10)];
        [bottone addTarget:self action:@selector(selectRowButton:) forControlEvents:UIControlEventTouchUpInside];
        [POP_area addSubview:bottone];
        
        if(!row.giaLetta){
            UIImageView *new_message = [[UIImageView alloc] initWithFrame:CGRectMake(5, topoffset+(i*row_height)+5+9.5, 3, 11)];
            [new_message setImage:[UIImage imageNamed:@"new_message.png"]];
            [POP_area addSubview:new_message];
        }
    }
}


#pragma mark -
#pragma mark AZIONI
//=========================
// AZIONI
//=========================
-(IBAction)selectRowButton:(UIButton*)sender{
    // NSLog(@"selezionataNotificaDaMenu");
    [delegate selezionataNotificaDaMenu:[lista_notifiche objectAtIndex:sender.tag]];
}

#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
-(IBAction)FineTabella_notificheHome:(UIButton*)sender{
    // NSLog(@"FineAdvanced");
    [delegate removeTabella_notificheHome];
}


#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}



@end
