//
//  Home.m
//  iBreviary2014
//
//  Created by leyo on 23/01/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//


#warning OVERRIDEDEBUGforFLEX
//#define DEBUG 1


#import "Home.h"
#import "WebBrowser.h"
#import "AAMFeedbackViewController.h"
#import "SystemData.h"
#import "LinguaObject.h"
#import "TestoProprio.h"
#import "NotificaObject.h"
#import "MenuViewController.h"
#import "NotaCTRL.h"
#import "MySpiritualBookList.h"
#import "PdfPager.h"

#if DEBUG
#import "FLEXManager.h"
#endif



#define HOME_Y 0.0 //remove iOS6 iusses

//====================
// BLOCK define
//====================
void (^animation_block)(void);
void (^completition_block)(BOOL);

@interface Home ()<NewNotaDelegate,BNHtmlPdfKitDelegate>

@property (strong, nonatomic) IBOutlet UIButton *mySpiritual_btn;
@property (strong, nonatomic) IBOutlet UIButton *mySpiritual_btn2;

//DEBUG
@property (strong, nonatomic) IBOutlet UIButton *bugFix_btn;
@property (strong, nonatomic) IBOutlet UIButton *exitDebug;
@property (strong, nonatomic) IBOutlet UITextView *DebugTextView;
@property (strong, nonatomic) IBOutlet UIView *DebugView;

//HUD visible
@property (assign, nonatomic) BOOL hudExist;


//CODA NOTIFICHE
@property (strong, nonatomic) NSMutableArray *coda_notifiche;
@property (assign, nonatomic) BOOL showingNotifica;

@property (strong, nonatomic) PdfPager *pdf_homePager;
@property (strong, nonatomic) IBOutlet UIView *bookView;

@property (strong, nonatomic) BNHtmlPdfKit *htmlPdfKit;

@end

@implementation Home

// ROTELLA-HUD
@synthesize HUD,testo_hud;
//FULL SCREEN
@synthesize fullOnNavArea,fullOutOnText_btn, fullOutOnHome_btn, fullINOnNav_btn;

//ROTELLA
@synthesize rotellaSettaHome,subHomeAcitivy,rotellaFUllScreen;

@synthesize area_attribute, area_santo;
@synthesize area_bassa,area_centrale;
@synthesize custodia,terraSancta,infoCTRL,creditsCTRL;
@synthesize guida,principi;
//->LISTA
@synthesize listaCTRL,lista_popover;
//->EVENT-FLAG
@synthesize eventCTRL,event_popover,eventFLAG,eventFLAG2;
//->LINGUA
@synthesize linguaCTRL,btn_lingua;
//->BLUR
@synthesize fake_home_blur,blurView,blurView_listaNotifiche;
//->NAVBAR
@synthesize NAVBAR;

//->DAY&FIx
@synthesize day;
//->HOME
@synthesize HOME, dettagliHOME, sfondo_home, linea_dayInfo;
@synthesize custodia_btn, terrasanta_btn, info_btn,  logo;
@synthesize titolo_ufficiale, titolo_data;
@synthesize label_day_time, label_day_type, label_day_time_tit , label_day_type_tit ,label_lingua, label_lingua_tit,day_info_TP_logo, day_info_TP_title;
//->NAVBAR - BTN
@synthesize oggi_btn, oggi2_btn, prega_btn;
//->VOICE
@synthesize voice_selector;
//----SantoView
@synthesize SantoView, scroller, pager,santo_btn,santoCTRL;

//->CONTENT
@synthesize CONTENT,webContent,sfondo_content,web_title,web_text,sfondo_black;
//AUTO SCROLLER
@synthesize autoscroller_area, autoscroll_PLAY_btn, autoscroll_STOP_btn;
//--NOTIFICHE
@synthesize notifica_btn,notifica_view,tabella_notificheCTRL;
@synthesize activity_badge, badge;

//view for iOS6
@synthesize menu2_ios6;

//MESSA
@synthesize messa_popover, messa_ctrl;
@synthesize messa_titolo_ctrl, messa_selettore_ctrl, messa_editor_ctrl;

//mySpiritual

//===========
//AUDIO iOS7>
//===========
@synthesize audio_area,audio_btn;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
@synthesize synthesizer;
#endif

//=========================
//AUTOSCROLL CLASS VARIABLE
//=========================
#define TIMER_HOME 0.3
NSInteger altezzaStep = 0;
NSInteger altezzaWebView = 0;
BOOL StopScroll = YES;
BOOL PausaScroll = NO;
BOOL PlayScroll = NO;
BOOL CHANGEPAGE = NO;

NSInteger offsetScroller = 0;

static Home *singletonInstance;
Item  *currentItem;
Messa *currentMessa = nil;


BOOL speechPaused;
BOOL _userTapped;

//=========================
//QUEUE FOR NOTIFY
//=========================
NSOperationQueue *queue=nil;

BOOL isMessaEditorEnable;
BOOL home_did_load = NO;


#pragma mark - INIT & SINGLETON
//=========================
// INIT & SINGLETON
//=========================
+ (Home *)sharedInstance
{
	return singletonInstance;
}

- (instancetype)init{
        if(singletonInstance!=nil)
                return singletonInstance;
    
    //iPhone4
    NSString *nibNameOrNil = @"Home";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"Home_ipad";
    if(IS_IPAD_PRO) nibNameOrNil = @"Home_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"Home5";
    if(IS_IPHONE_6) nibNameOrNil = @"Home6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"Home6Plus";
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    
    if (self) {
        // Custom initialization
        singletonInstance = self;
        self.showingNotifica = NO;
        self.coda_notifiche = [NSMutableArray new];
    }
    return self;
}

//==================================
// SlideNavigationController Methods
//==================================
#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    _userTapped = NO;
    BOOL risp = YES;
    if(self.isLand && !self.isIpad) risp = NO;
    if((self.santoCTRL != nil) ||
       (self.linguaCTRL != nil) ||
       (self.custodia != nil) ||
       (self.terraSancta != nil) ||
       (self.infoCTRL != nil) ||
       (self.guida != nil) ||
       (self.principi != nil) ||
       (self.notificaCTRL!=nil) ||
       (self.messa_editor_ctrl!=nil) ||
       (self.messa_selettore_ctrl!=nil) ||
       (self.messa_titolo_ctrl != nil) ||
       [self isMySpiritualShowing] )
                                        risp = NO;
    
    // NSLog(@"slideNavigationControllerShouldDisplayLEFTMenu:%i",risp);
    
    return risp;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    // NSLog(@"slideNavigationControllerShouldDisplayRightMenu:%i",(self.santoCTRL != nil));
    _userTapped = NO;
    if(self.isLand && !self.isIpad) return NO;
    if((self.santoCTRL != nil) ||
       (self.linguaCTRL != nil) ||
       (self.custodia != nil) ||
       (self.terraSancta != nil) ||
       (self.infoCTRL != nil) ||
       (self.guida != nil) ||
       (self.principi != nil) ||
       (self.notificaCTRL!=nil) ||
       (self.messa_editor_ctrl!=nil) ||
       (self.messa_selettore_ctrl!=nil) ||
       (self.messa_titolo_ctrl != nil) ||
       [self isMySpiritualShowing] )
                                    return NO;
    return YES;
}

- (BOOL)slideNavigationControllerShouldShowRoot{
    if((self.messa_editor_ctrl!=nil) || (self.messa_selettore_ctrl!=nil) || (self.messa_titolo_ctrl != nil)) return NO;
    return YES;
}

-(void)menuController:(DDMenuController *)mc willShowViewController:(UIViewController *)c{
    // NSLog(@"home:DDMenuController willShowViewController:%@",c);
}


-(void)closeMenuIfOpen{
    if([DDMenuController isMenuOpen])
    {
        [DDMenuController closeMenuWithCompletion:nil];
    }
}

#pragma mark - GetDay
//=========================
// GET DAY
//=========================
+(Day*)getCurrentDay{
    Home *h = [Home sharedInstance];
    if(!h.day) h.day = [Day caricaGiornoInUso];
    // NSLog(@"getCurrentDay:%@",h.day);
    return h.day;
}

//=========================
// StatusBar
//=========================
#pragma mark - StatusBar
- (BOOL)prefersStatusBarHidden
{
    if(self.view.frame.origin.x!=0 || [self.defaults boolForKey:SOLO_FULLSCREEN]) return YES;
    
    
    for (UIViewController *row in self.childViewControllers){
        if([row isKindOfClass:[NotaCTRL class]] || [row isKindOfClass:[MySpiritualBookList class]]){
            return [row prefersStatusBarHidden];
        }
    }
    
    CGRect rect = self.NAVBAR.frame;
    
    //nascondi se la navBar non c'è' oppure ....
    BOOL risposta = (rect.origin.y < -25);
    
    if(!risposta && (self.isLand && !self.isIpad)){
        risposta = YES;
    }
    if(!risposta && santoCTRL !=nil){
        risposta = YES;
    }
    if(!risposta && custodia !=nil){
        risposta = YES;
    }
    if(!risposta && terraSancta !=nil){
        risposta = YES;
    }
    if(!risposta && linguaCTRL !=nil){
        risposta = YES;
    }
    if(!risposta && creditsCTRL !=nil){
        risposta = YES;
    }
    if(!risposta && self.notificaCTRL !=nil && !self.isIpad){
        risposta = YES;
    }
    if(!risposta && infoCTRL !=nil){
        risposta = YES;
    }
    if(!risposta && messa_titolo_ctrl.view.superview !=nil){
        risposta = YES;
    }
    if(!risposta && messa_editor_ctrl.view.superview !=nil){
        risposta = YES;
    }
    if(!risposta && messa_selettore_ctrl.view.superview !=nil){
        risposta = YES;
    }

    if(!risposta && (listaCTRL != nil && !self.isIpad)){
        // NSLog(@"l:%@",listaCTRL.view.superview);
        risposta = YES;
    }
    if(!risposta && (eventCTRL != nil && !self.isIpad)){
        risposta = YES;
    }
    if(!risposta && guida !=nil){
        risposta = YES;
    }
    if(!risposta && principi !=nil){
        risposta = YES;
    }
    
    //NSLog(@"Home:prefersStatusBarHidden:%@ messTIT:%i",risposta?@"YES":@"NO",messa_titolo_ctrl.view.superview!=nil);
    
    return risposta;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

//cambio colore statusbar in bianco
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


//=========================
// DIDLOAD
//=========================
#pragma mark - ==DIDLOAD==
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self gui];
    home_did_load = YES;
}

-(void)fineAggiornamento2015{
    
    runOnMainQueueWithoutDeadlocking(^{
        [self caricaGiornoInUso];
    });
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^(void){[Home fineHUD2015];}];
    
    // NSLog(@"fineAggiornamento2014");
}


-(void)gui{

    [self prefersStatusBarHidden];
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    
    //reset GUI
    [self checkFull];
    
#warning check debugMode
    [self.bugFix_btn setHidden:![[NSUserDefaults standardUserDefaults] boolForKey:IBREVIARY_DEBUG_MODE]];
    [self.bugFix_btn setHidden:YES];
    
    [self localizzazione];
    
    [titolo_ufficiale setText:@""];
    [titolo_ufficiale setMinimumScaleFactor:0.8];
    [titolo_ufficiale setAdjustsFontSizeToFitWidth:YES];

    [titolo_data setText:@""];
    [label_lingua setText:@""];
    [label_day_time setText:@""];
    [label_day_type setText:@""];
    [pager setHidden:YES];
    
    
    [day_info_TP_logo setImage:nil];
    [day_info_TP_logo.layer setBorderColor:[[UIColorFromRGB(0X583c2e) colorWithAlphaComponent:0.8] CGColor]];
    [day_info_TP_logo.layer setBorderWidth:0];
    [day_info_TP_logo.layer setCornerRadius:day_info_TP_logo.frame.size.width/2];
    [day_info_TP_title setText:@""];
 
    [label_day_type_tit setHidden:YES];
    [label_day_time_tit setHidden:YES];
    [label_lingua_tit setHidden:YES];
    
    
    [webContent.scrollView setDelegate:self];

    
    [Home modificaEventFlag];
    
    [area_centrale.layer setCornerRadius:7];
    [area_centrale setClipsToBounds:YES];
    [self.sfondo_content setAlpha:(1 - [self.defaults floatForKey:LUMINOSITA])];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    [Home audioCTRL];
#endif
    [Home scrollerCTRL];
    

    if(self.isIpad)
    {
        [subHomeAcitivy setHidden:YES];
        [subHomeAcitivy.layer setCornerRadius:subHomeAcitivy.frame.size.width/2.0];
        [subHomeAcitivy.layer setBorderColor:[[UIColorFromRGB(0X51340f) colorWithAlphaComponent:0.1] CGColor]];
        [subHomeAcitivy.layer setBorderWidth:2];
        [subHomeAcitivy.layer setMasksToBounds:YES];
    }
    
    [rotellaSettaHome setColor:[UIColorFromRGB(0xdcae63) colorWithAlphaComponent:1]];
    rotellaSettaHome.transform = self.isIpad?CGAffineTransformMakeScale(1.f, 1.f):CGAffineTransformMakeScale(1.2f, 1.2f);
    [rotellaSettaHome setCenter:CGPointMake(1+subHomeAcitivy.frame.size.width/2.0, 1+subHomeAcitivy.frame.size.width/2.0)];
}

-(IBAction)attivaRotella:(id)sender{
    [self AttivaRotella];
}


-(void)localizzazione{
    //_LOCALIZZAZIONE
    [oggi_btn setTitle:NSLocalizedString(@"Today", nil) forState:UIControlStateNormal];
    [oggi2_btn setTitle:NSLocalizedString(@"Today", nil) forState:UIControlStateNormal];
    [prega_btn setTitle:NSLocalizedString(@"Prega!", nil) forState:UIControlStateNormal];

    [label_day_type_tit setText:[NSLocalizedString(@"day_type", nil) uppercaseString]];
    [label_day_time_tit setText:[NSLocalizedString(@"day_time", nil) uppercaseString]];
    [label_lingua_tit setText:[NSLocalizedString(@"linguaTIT", nil) uppercaseString]];
}

-(void)resetTAB:(UISwipeGestureRecognizer*)s{
    // NSLog(@"resetTAB:%@",s);
    _userTapped = NO;
}

-(void)checkDeviceForIOS6{
    if([[UIScreen mainScreen] bounds].size.height >= 568){
            // NSLog(@"il dispositivo non è iphone 3.5inch");
            NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
                            NSString *mex = @"please update iOS before";
                            [Home changeHudText:mex];
                            // NSLog(@"inpausa");
                            }];//fine step1
            // STEP 2
            NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
                            // NSLog(@"step2");
                            [NSThread sleepForTimeInterval:3];
                            exit(0);
                            }];//fine step2
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue new] addOperation:step2];
    }
    
}

//=========================
// APPEAR
//=========================
#pragma mark - Appear

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    // NSLog(@"Home Will Appear");
    
    [self checkFull];
    // NOTIFICHE
    if([self.defaults boolForKey:AGGIORNAMENTO_2015]){
        [Home aggiornaBadge];
    }
    else {
        //AGGIORNAMENTO..in corso..
        [self.badge setHidden:YES];
    }
    
    
    NSString *aggiungiAlDiario = NSLocalizedString(@"aggiungi_al_diario", nil);
    UIMenuItem *customMenuItem1 = [[UIMenuItem alloc] initWithTitle:aggiungiAlDiario action:@selector(addNota:)];

    //NSString *copiaPerLettura = NSLocalizedString(@"copia_per_lettura", nil);
    //UIMenuItem *customMenuItem2 = [[UIMenuItem alloc] initWithTitle:copiaPerLettura action:@selector(salvaHtmlSelezione:)];

    [[UIMenuController sharedMenuController] setMenuItems:[NSArray arrayWithObjects:customMenuItem1, nil]];
    if(home_did_load && self.isIpad) [self controllaRotazioneIPAD];
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
#if DEBUG
    UITapGestureRecognizer* debugTap = [[UITapGestureRecognizer alloc] initWithTarget : self  action : @selector (handleSixFingerQuadrupleTap:)];
    
    [debugTap setDelaysTouchesBegan:NO];
    [debugTap setDelaysTouchesEnded:NO];
    debugTap.numberOfTapsRequired = 6;
    debugTap.numberOfTouchesRequired = 2;
    [self.view addGestureRecognizer : debugTap];
#endif
    
    if(!home_did_load && self.isIpad) [self controllaRotazioneIPAD];

    if(home_did_load){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aggiornamentoICloudAvvenuto) name:AGGIORNAMENTO_ICLOUD_NOTIFICA object:nil];
        
        if(![self.defaults boolForKey:AGGIORNAMENTO_2015]){
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fineAggiornamento2015) name:AGGIORNAMENTO2015_TERMINATO object:nil];
                [NotificaObject checkNotifiche];
                home_did_load = NO;
                }
        else{
                if([self.defaults objectForKey:WIDGET_CMD]){
                        [self.defaults removeObjectForKey:WIDGET_CMD];
                        [self.defaults synchronize];
                        }
                // NSLog(@">>>>>>>caricoGiornoInUso!!!!");
                [self caricaGiornoInUso];
                home_did_load = NO;
                [self addDebugMode];
                }
        }
}

- (void)handleSixFingerQuadrupleTap:(UITapGestureRecognizer *)tapRecognizer
{
#if DEBUG
    if (tapRecognizer.state == UIGestureRecognizerStateRecognized) {
        // This could also live in a handler for a keyboard shortcut, debug menu item, etc.
        [[FLEXManager sharedManager] showExplorer];
    }
#endif
 //   [self alert:@"non funziona!!" andMex:@"cribbio" icon:nil okAction:nil withCancel:YES];
//#endif
    

}


-(void)aggiornamentoICloudAvvenuto{
    NSLog(@"Home: aggiornamentoICloudAvvenuto");
    
    [NotificaObject updateHome];
    [self mostraICLoudUpdate];
}

//=========================
// DISAPPEAR
//=========================
#pragma mark - Disappear

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[UIMenuController sharedMenuController] setMenuItems:nil];
}


//===============================
// CAN PERFORM ACTION
//===============================
#pragma mark - CAN PERFORM ACTION
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    
    BOOL can = [super canPerformAction:action withSender:sender];
    if (action == @selector(addNota:))
    {
        can = YES;
    }
    if  (action == @selector(salvaHtmlSelezione:)){
        can = NO;
    }
    
    if (action == @selector(copy:))
    {
        can = NO;
    }
   // NSLog(@"%@ perform action %@ with sender %@.", can ? @"can" : @"cannot", NSStringFromSelector(action), sender);
    return can;
}

//===============================
// MySpiritualBook
//===============================
#pragma mark - MySpiritualBook
-(IBAction)showMySpiritualBook:(UIButton*)sender{
    // NSLog(@"addNota");
    [self closeMenuIfOpen];

    MySpiritualBookList *mySpiritualBOOK = [[MySpiritualBookList alloc] init];
    [mySpiritualBOOK.view setAlpha:0];
    [self displayViewController:mySpiritualBOOK inView:self.view];
    [UIView animateWithDuration:0.5
                     animations:^{
                         [mySpiritualBOOK.view setAlpha:1];
                         [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
                     }
                     completion:nil];
}

//=========================
// MySpiritualBook PopOver
//=========================
#pragma mark - MySpiritualBook PopOver
- (IBAction)showMySpiritualBookPopOver:(UIButton*)sender {
    CGRect qui = sender.frame;
    qui.origin.y -= 15;
    // NSLog(@"showMessaPopOverFrom:%@",NSStringFromCGRect(qui));
    [self.event_popover dismissPopoverAnimated:(self.event_popover != nil)];
    [self.lista_popover dismissPopoverAnimated:(self.lista_popover != nil)];
    [self.messa_popover dismissPopoverAnimated:(self.lista_popover != nil)];
    
    
    if (self.mySpiritualBook_popover.popoverVisible)
        [self.mySpiritualBook_popover dismissPopoverAnimated:YES];
    else
    {
        CGFloat height = (self.isLand)?695:950;
        if(sender==self.mySpiritual_btn2)height += 10;
        CGSize size = CGSizeMake(478, height);
        MySpiritualBookList *mySpiritualBOOK = [MySpiritualBookList new];
        [mySpiritualBOOK.view setFrame:CGRectMake(0, 0, size.width, size.height)];
        self.mySpiritualBook_popover = [[UIPopoverController alloc] initWithContentViewController:mySpiritualBOOK];
        [self.mySpiritualBook_popover setPopoverContentSize:size animated:NO];
        [self.mySpiritualBook_popover setBackgroundColor:[UIColorFromRGB(0X846e62) colorWithAlphaComponent:0.9]];
        //self.mySpiritualBook_popover.contentViewController.view.autoresizingMask = UIViewAutoresizingNone;
        self.mySpiritualBook_popover.delegate = self;
        [self.mySpiritualBook_popover presentPopoverFromRect:qui inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
    
    [sender setSelected:YES];
}

-(void)dismissMySpiritualPAD{
    // NSLog(@"dismissMySpiritualPAD");
    [self.mySpiritualBook_popover dismissPopoverAnimated:YES];
    [self.mySpiritual_btn setSelected:NO];
    [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
}

//=========================
// MySpiritualBook PopOver ADD
//=========================

#pragma mark - MySpiritualBook PopOver ADD
- (void)showMySpiritualBookPopOverADD{
    CGRect qui = CGRectMake(self.isLand?502:374, self.isLand?44:244, 20, 20);
    // NSLog(@"showMessaPopOverFrom:%@",NSStringFromCGRect(qui));
    [self.event_popover dismissPopoverAnimated:(self.event_popover != nil)];
    [self.lista_popover dismissPopoverAnimated:(self.lista_popover != nil)];
    [self.messa_popover dismissPopoverAnimated:(self.lista_popover != nil)];

    
    if (self.mySpiritualBook_popover.popoverVisible)
        [self.mySpiritualBook_popover dismissPopoverAnimated:YES];
    else
    {
        CGFloat height = 300;
        CGSize size = CGSizeMake(478, height);
        NotaCTRL *newNota = [[NotaCTRL alloc] initWithSelection:[self.webContent selectedText] andItem:(currentMessa)?nil:currentItem withDelegate:self];
        [newNota.view setFrame:CGRectMake(0, 0, size.width, size.height)];
        self.mySpiritualBook_popover = [[UIPopoverController alloc] initWithContentViewController:newNota];
        [self.mySpiritualBook_popover setBackgroundColor:[UIColor clearColor]];

        [self.mySpiritualBook_popover setPopoverContentSize:size animated:NO];
        //[self.mySpiritualBook_popover setBackgroundColor:[UIColorFromRGB(0X846e62) colorWithAlphaComponent:0.9]];
        //self.mySpiritualBook_popover.contentViewController.view.autoresizingMask = UIViewAutoresizingNone;
        self.mySpiritualBook_popover.delegate = self;
        [self.mySpiritualBook_popover presentPopoverFromRect:qui inView:self.view permittedArrowDirections:0 animated:YES];
    }
    
}

-(void)dismissNewMyNotaCtrl:(id)sender andNota:(Nota *)new_nota{
    // NSLog(@"dismissMySpiritualPAD");
    [self.mySpiritualBook_popover dismissPopoverAnimated:YES];
    [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
}


+(void)dismissMySpiritual{
    if(IS_IPAD) [[Home sharedInstance] dismissMySpiritualPAD];
    else [[Home sharedInstance] dismissMySpiritual];
}

-(BOOL)isMySpiritualShowing{
    if (self.isIpad) {
        ;//LOGICA IPAD
        return (self.mySpiritualBook_popover.popoverVisible);
    }
    BOOL risp = NO;
    for (UIViewController *row in self.childViewControllers){
        if([row isKindOfClass:[MySpiritualBookList class]]) risp = YES;
            }
    return risp;
}

-(void)dismissMySpiritual{
    [self.mySpiritual_btn setSelected:NO];
    for (__strong __block UIViewController *row in self.childViewControllers){
        if([row isKindOfClass:[MySpiritualBookList class]]){
            [UIView animateWithDuration:0.5
                             animations:^{
                                 [row.view setAlpha:0];
                             }
                             completion:^(BOOL fine){
                                 [self removeChildCTRL:row];
                                 row = nil;
                                 [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
                             }];
            
        }
    }
}


//===============================
// UIMenuController
//===============================
#pragma mark - UIMenuController
-(void)addNota:(id)m{
    if(IS_IPAD){
        [self showMySpiritualBookPopOverADD];
        return;
    }
   // NSLog(@"addNota");
    NotaCTRL *newNota = [[NotaCTRL alloc] initWithSelection:[self.webContent selectedText] andItem:(currentMessa)?nil:currentItem];
    [newNota.view setAlpha:0];
    [self displayViewController:newNota inView:self.view];
    [UIView animateWithDuration:0.5
                     animations:^{
                            [newNota.view setAlpha:1];
                                }
                     completion:nil];
}

+(void)dismissNotaCTRL{
    [[Home sharedInstance] dismissNotaCTRL];
}

-(void)dismissNotaCTRL{
    for (__strong __block UIViewController *row in self.childViewControllers){
        if([row isKindOfClass:[NotaCTRL class]]){
            [UIView animateWithDuration:0.5
                             animations:^{
                                 [row.view setAlpha:0];
                             }
                             completion:^(BOOL fine){
                                 [self removeChildCTRL:row];
                                 row = nil;
                                 [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
                             }];

        }
    }
}

-(void)salvaHtmlSelezione:(id)m{
   // NSLog(@"salvaHtmlSelezione:%@",m);
    NSString *selectedString = [self.webContent selectedHTMLText];
    [[[UIAlertView alloc] initWithTitle:@"+HTML" message:[NSString stringWithFormat:@"%@",selectedString] delegate:nil cancelButtonTitle:@"ok!" otherButtonTitles:nil] show];
}

-(void)copy:(id)sender{
    // NSLog(@"copy:%@",sender);
    NSString *shareString = [self.webContent selectedHTMLText];
    NSAttributedString *att = [[NSAttributedString alloc] initWithData:[shareString dataUsingEncoding:NSUTF8StringEncoding]
                                                               options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                         NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                    documentAttributes:nil
                                                                 error:nil];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:[att string]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


+(void)mostraPdfBookwithUrl:(NSString*)url{
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        if([DDMenuController isMenuOpen])
        {
            [DDMenuController closeMenuWithCompletion:^{}];
        }
    }];//fine step1
    
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        Home *h = [Home sharedInstance];
        [h presentBook:url];
    }];//fine step2
    
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
        
    }];//fine step3

    
    
    [step2 addDependency:step1];
    [step3 addDependency:step2];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    [[NSOperationQueue mainQueue] addOperation:step3];

}

-(IBAction)fineBookPdf:(UIButton*)sender{
    }

-(void)presentBook:(NSString*)url{
    if(self.pdf_homePager){
        [self updatePdfUrl:url];
        return;
    }
    self.pdf_homePager = [PdfPager presentPDFPagerWithUrlFile:url];
}

+(void)updatePdfUrl:(NSString*)url{
    [[Home sharedInstance] updatePdfUrl:url];
}

-(void)updatePdfUrl:(NSString*)url{
    [self.pdf_homePager reloadPdfUrl:url];
}

+(void)dismissPdfPager{
    [[Home sharedInstance] dismissPdfPager];
}

-(void)dismissPdfPager{
    
    [self.pdf_homePager dismissViewControllerAnimated:YES
                                           completion:^{
                                               self.pdf_homePager = nil;
                                           }];

}

//=========================
// MESSA
//=========================
#pragma mark - MESSA class
+(BOOL)isMessaVisible{
    return (currentMessa!=nil) && [Home isContentVisible];
}

+(Messa*)getCurrentMessa{
    return currentMessa;
}

+(void)mostraMessa:(Messa*)messa editMode:(BOOL)editMode{
    currentMessa = messa;

    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"Home: mostraMessa step 1 carica menu sx");
        [[MenuViewController sharedInstance] caricaMessaMenuEdit:editMode];
        // NSLog(@"Home: caricaMessaMenuEdit == FINE ==");
        
    }];//fine step1
    
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"Home: mostraMessa step 2 mostra menu sx");
        [DDMenuController showLeftController:^{
            Home *h = [Home sharedInstance];
            if(!IS_IPAD){
                h.listaCTRL = nil;
            }
            if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
            else [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
            
            NSBlockOperation *step1a = [NSBlockOperation blockOperationWithBlock:^{
                    // NSLog(@"Home: reload >>>");
                    [Home reloadMessa:nil];
            }];//fine step1a
            
                [[NSOperationQueue mainQueue] addOperation:step1a];
            NSBlockOperation *step2a = [NSBlockOperation blockOperationWithBlock:^{
                    // NSLog(@"Home: nascondi home >>>");
                    [h nascondiHome];
            }];//fine step2a
                [step2a addDependency:step1a];
                [[NSOperationQueue mainQueue] addOperation:step2a];
                
            NSBlockOperation *step3a = [NSBlockOperation blockOperationWithBlock:^{
                    // NSLog(@"Home: did select row step 4 fine carica messa");
                    [MenuViewController fineMessaCaricata];
            }];//fine step3a
                [step3a addDependency:step2a];
                [[NSOperationQueue mainQueue] addOperation:step3a];
        }];
        
    }];//fine step2
    
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
}

NSIndexPath *gotoMessa;

+(void)reloadMessa:(NSIndexPath*)indexPath{
    Home *h = [Home sharedInstance];
    [h.webContent loadHTMLString:@"" baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    [AppDelegate saveContext];
    
    [h.sfondo_black setHidden:([h.defaults integerForKey:CSS] != 2)];
    
    
    //RESET AREA AUTOSCROLL
    [h.sfondo_black setHidden:([h.defaults integerForKey:CSS] != 2)];
    
    
    //RESET AREA AUTOSCROLL
    if(!h.isIos7orUpper && !h.isIphone5)
    {
        CGRect autoframe = h.autoscroller_area.frame;
        autoframe.origin.y = ([h.defaults boolForKey:SOLO_FULLSCREEN] || h.isLand)?2:49;
        [h.autoscroller_area setFrame:autoframe];
    }
    
    
    //AUDIO RESET
    [h resetAudio];
    
    //MOSTRO OGGETTO
    NSString *messaHtml = [currentMessa htmlString];
    [h.webContent loadHTMLString:messaHtml baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];

    if(indexPath!=nil) gotoMessa = indexPath;
}

+(void)messaGoToIndex:(NSIndexPath*)indexPath{
    Home *h = [Home sharedInstance];
    NSString *s = [NSString stringWithFormat:@"javascript:goto(%li)",(long)indexPath.row];
    NSLog(@"javascript:goto:%@",[h.webContent stringByEvaluatingJavaScriptFromString:s]);
}

//=========================
// MESSA titolo
//=========================
#pragma mark - MESSA titolo
//@synthesize , messa_selettore_ctrl, messa_editor_ctrl;
+(void)showMessaTitolo:(Messa*)m thenComune:(BOOL)comune{
    
    __block Home *h = [Home sharedInstance];
    [h activityNetwork:YES];
    //========
    // STEP 0
    //========
    NSBlockOperation *step0 = [NSBlockOperation blockOperationWithBlock:^{
        currentMessa = m;
        h.messa_titolo_ctrl = [[TitoloMessa alloc] initWithDelegate:h
                                                             andTit:m.day.title
                                                               lang:m.day.linguaGiorno
                                                               date:m.day.id_date
                                                         thenComune:comune];
    }];//fine step0
    
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        [DDMenuController closeMenuWithCompletion:^{
            if([Home isContentVisible]){
                [Home mostraHome];
            }
        }];
    }];//fine step1

    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"showMessaTitolo step 3 UI MAIN");
        h.messa_titolo_ctrl.view.alpha = 0;
        [h.view addSubview:h.messa_titolo_ctrl.view];
        [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
        [UIView animateWithDuration:0.7
                         animations:^{
                             h.messa_titolo_ctrl.view.alpha = 1;
                             [h.messa_popover dismissPopoverAnimated:YES];
                         }
                         completion:^(BOOL finito){
                             // NSLog(@"fine show Titolo Messa");
                             [h activityNetwork:NO];
                         }];
    }];//fine step2
    
    
    [step1 addDependency:step0];
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step0];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
}

+(void)showEditModificaTitolo:(Messa*)m{
    __block Home *h = [Home sharedInstance];
    [h activityNetwork:YES];
    //========
    // STEP 0
    //========
    NSBlockOperation *step0 = [NSBlockOperation blockOperationWithBlock:^{
        h.messa_titolo_ctrl = [[TitoloMessa alloc] initForEditTitoloAction:h
                                                                     messa:m];
    }];//fine step0

    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        [DDMenuController closeMenuWithCompletion:nil];
        }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"showMessaTitolo step 3 UI MAIN");
        h.messa_titolo_ctrl.view.alpha = 0;
        [h.view addSubview:h.messa_titolo_ctrl.view];
        [h addChildCTRL:h.messa_titolo_ctrl];
        [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
        [UIView animateWithDuration:0.7
                         animations:^{
                             h.messa_titolo_ctrl.view.alpha = 1;
                             [h.messa_popover dismissPopoverAnimated:YES];
                         }
                         completion:^(BOOL finito){
                             // NSLog(@"fine show Titolo Messa");
                             [h activityNetwork:NO];
                         }];
    }];//fine step2
    
    
    [step1 addDependency:step0];
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step0];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];

}

-(void)fineTitoloEdit{
    [AppDelegate saveContext];

    [UIView animateWithDuration:0.4
                     animations:^{
                         self.messa_titolo_ctrl.view.alpha = 0;
                        }
                     completion:^(BOOL finito){
                         NSLog(@"fineTitoloEdit:%@",[Messa getMesseIn:[AppDelegate mainManagedObjectContext]]);
                         [super activityNetwork:NO];
                         [messa_titolo_ctrl.view removeFromSuperview];
                         [self.blurView removeFromSuperview];
                         [self.fake_home_blur removeFromSuperview];
                         [self removeChildCTRL:messa_titolo_ctrl];
                         messa_titolo_ctrl =  nil;
                         blurView = nil;
                         fake_home_blur = nil;
                         
                         [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
                     }];

}

-(void)titolosalvato:(NSString *)tit thenComune:(BOOL)thenComune{
    NSLog(@"titolosalvato thenComune:%i",thenComune);
    
    [currentMessa setMessa_name:tit];
    [AppDelegate saveContext];
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.messa_titolo_ctrl.view.alpha = 0;
                     }
                     completion:^(BOOL finito){
                         NSLog(@"fineTitoloEdit:%@",[Messa getMesseIn:[AppDelegate mainManagedObjectContext]]);
                         [super activityNetwork:NO];

                         [messa_titolo_ctrl.view removeFromSuperview];
                         [self.blurView removeFromSuperview];
                         [self.fake_home_blur removeFromSuperview];
                         [self removeChildCTRL:messa_titolo_ctrl];

                         messa_titolo_ctrl =  nil;
                         blurView = nil;
                         fake_home_blur = nil;
                         
                         [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
                         if(thenComune)[self fineTitoloMessaThenComune];
                         else [self fineTitoloMessa];
                     }];
}

-(void)fineTitoloMessa{
    [Home mostraMessa:currentMessa editMode:YES];
}

-(void)fineTitoloMessaThenComune{
    [self showMessaSelettoreWithType:tipo_lista_messa_comune];
}

//=========================
// MESSA selettore
//=========================
#pragma mark - MESSA selettore
+(void)showMessaSelettoreWithType:(Tipo_messa_lista)t{
    [[Home sharedInstance] showMessaSelettoreWithType:t];
}

- (void)showMessaSelettoreWithType:(Tipo_messa_lista)t
{
	NSLog(@"showMessaSelettoreWithType:%i (land:%i)",t,self.isLand);
    messa_selettore_ctrl = [[SelettoreMessa alloc] initWithMessa:currentMessa andDelegate:self type:t];
    
	[self.view addSubview:messa_selettore_ctrl.view];
    [self addChildCTRL:messa_selettore_ctrl];
    
    [[DDMenuController sharedInstance].view setAutoresizesSubviews:NO];
    [[DDMenuController sharedInstance].view setClipsToBounds:NO];
    
    [[DDMenuController sharedInstance].view addSubview:messa_selettore_ctrl.view];
    
    [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
}

- (void)dismissMessaSelettoreGoingTo:(NSIndexPath *)indexPath
{
	NSLog(@"dismissMessaSelettore");    
    [UIView animateWithDuration:0.4
                     animations:^{[messa_selettore_ctrl.view removeFromSuperview];}
                     completion:^(BOOL finito){
                         [self removeChildCTRL:messa_selettore_ctrl];
                         messa_selettore_ctrl = nil;
                         [Home reloadMessa:indexPath];
                     }];
    }

- (void)dismissMessaSelettoreThenComune
{
    // NSLog(@"dismissMessaSelettoreThenComune");
    [UIView animateWithDuration:0.4
                     animations:^{[messa_selettore_ctrl.view removeFromSuperview];}
                     completion:^(BOOL finito){
                         [self removeChildCTRL:messa_selettore_ctrl];
                         messa_selettore_ctrl = nil;
                         NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
                             NSLog(@"currentMessa: %@",currentMessa);
                             [Home mostraMessa:currentMessa editMode:YES];
                         }];//fine step1
                         [[NSOperationQueue currentQueue] addOperation:step1];
                         
                        }];//fine animation
}

-(void)titoloAnnullato{
    
    [UIView beginAnimations:@"SparisceMessaTitolo" context:NULL];
    [UIView setAnimationDuration:0.4];
    messa_titolo_ctrl.view.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineTitoloMessaAborted)];
    [UIView commitAnimations];
}

-(void)fineTitoloMessaAborted{
    [self removeChildCTRL:messa_selettore_ctrl];
    NSError *e = nil;
    if([Messa deleteMessa:currentMessa andError:&e forContext:[AppDelegate mainManagedObjectContext]]){
        currentMessa = nil;
        [messa_titolo_ctrl.view removeFromSuperview];
        messa_titolo_ctrl =  nil;
    }
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    else [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}


//=========================
// MESSA editor
//=========================
#pragma mark - MESSA editor
+(void)showMessaEditor:(NSString*)attribute{
    Home *h = [Home sharedInstance];
    [h showMessaEditor:attribute];
}

- (void)showMessaEditor:(NSString*)attribute
{
	NSLog(@"showMessaEditor:%@ - %@",messa_editor_ctrl,messa_editor_ctrl.view);
    if(messa_editor_ctrl != nil) return;
    messa_editor_ctrl = [[Editor alloc] initWithDelegate:self andMessa:currentMessa forAttribute:attribute];
//#define MENU_IPAD_SLIDE_OFFSET_PORT 510
//#define MENU_IPAD_SLIDE_OFFSET_LAND 766
    //if([DDMenuController isMenuOpen]) [DDMenuController closeMenuWithCompletion:nil];
    CGRect frame_messa = self.view.frame;
    frame_messa.origin.x = 0;
    [messa_editor_ctrl.view setFrame:frame_messa];
 
    [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    
	messa_editor_ctrl.view.alpha = 0;
    
    [[DDMenuController sharedInstance].view addSubview:messa_editor_ctrl.view];
    [[DDMenuController sharedInstance] addChildViewController:messa_editor_ctrl];
    
	[UIView animateWithDuration:0.7
                     animations:^{messa_editor_ctrl.view.alpha = 1;}
                     completion:^(BOOL finito){
                         // NSLog(@"fine show Editor Messa:%@",NSStringFromCGRect(messa_editor_ctrl.view.frame));
                     }];
}

-(void)dismissEditor:(NSIndexPath*)indexPath{
    // NSLog(@"dismissEditor:%@",currentMessa.omelia);
    [Home reloadMessa:indexPath];
    [[MenuViewController sharedInstance] forzaMessaInEditor];
    [UIView beginAnimations:@"SparisceMessaEditor" context:NULL];
    [UIView setAnimationDuration:0.4];
    messa_editor_ctrl.view.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineDismissEditor)];
    [UIView commitAnimations];
}

-(void)fineDismissEditor{
     [messa_editor_ctrl willMoveToParentViewController:nil];
     [messa_editor_ctrl removeFromParentViewController];
    [messa_editor_ctrl.view removeFromSuperview];
    messa_editor_ctrl = nil;
}


//=========================
// MESSA PopOver
//=========================
#pragma mark - MESSA PopOver
- (IBAction)showMessaPopOver:(UIButton*)sender {
	CGRect qui = sender.frame;
    if(qui.size.width==29)
    {
        qui.origin.y = 26;
        qui.origin.x = 127;
        
    }
    // NSLog(@"showMessaPopOverFrom:%@",NSStringFromCGRect(qui));
    [self.event_popover dismissPopoverAnimated:(self.event_popover != nil)];
    [self.lista_popover dismissPopoverAnimated:(self.lista_popover != nil)];
    [self.mySpiritualBook_popover dismissPopoverAnimated:(self.mySpiritualBook_popover != nil)];

    
	if (self.messa_popover.popoverVisible)
        [messa_popover dismissPopoverAnimated:YES];
	else
    {
        self.messa_ctrl = [MessaLista new];
        
        messa_popover = [[UIPopoverController alloc] initWithContentViewController:messa_ctrl];
        if(self.isIos7orUpper)[messa_popover setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"PaperHD.png"]]];
        CGSize size;
        size = CGSizeMake(messa_ctrl.view.frame.size.width, (self.isLand)?665:910);
        [messa_popover setPopoverContentSize:size animated:NO];
        [messa_ctrl.view setFrame:CGRectMake(0, 0, size.width, size.height)];
        messa_popover.contentViewController.view.autoresizingMask = UIViewAutoresizingNone;
        messa_popover.delegate = self;
        [messa_popover presentPopoverFromRect:qui
                                       inView:self.view
                     permittedArrowDirections:UIPopoverArrowDirectionUp
                                     animated:YES];
    }
	
    [sender setSelected:YES];
}

-(void)dismissMessa{
    // NSLog(@"dismissLista");
    [messa_popover dismissPopoverAnimated:YES];
}


#pragma mark -
#pragma mark ROTELLA-HUD
//=========================
// ROTELLA-HUD
//=========================
-(BOOL)avviaRotellaWithMex:(NSString*)text{
     NSLog(@"Main Avvia HUD:%@",text);
    if(self.hudExist){
        return NO;
    }

    [self.view setUserInteractionEnabled:NO];
    self.hudExist = YES;
        HUD = nil;
        HUD = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 180, 115)];
        [HUD setBackgroundColor:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.65]];
        [HUD.layer setCornerRadius:10];
        [HUD.layer setBorderColor:[[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9] CGColor]];
        [HUD.layer setBorderWidth:2];
        
        if([Home isHomeVisible]){
            UIBezierPath *path = [UIBezierPath bezierPath];
            [path moveToPoint:CGPointMake(CGRectGetWidth(HUD.frame), CGRectGetHeight(HUD.frame)/4.f)];
            [path addLineToPoint:CGPointMake(CGRectGetWidth(HUD.frame)+3, CGRectGetHeight(HUD.frame)+3)];
            [path addLineToPoint:CGPointMake(CGRectGetWidth(HUD.frame)*3/4.f, CGRectGetHeight(HUD.frame))];
            HUD.layer.shadowPath = path.CGPath;
            HUD.layer.shadowColor = UIColorFromRGB(0X4e3424).CGColor;
        }
        HUD.layer.shadowOpacity = 0.4f;
        HUD.layer.shadowRadius = 5.0f;
        HUD.clipsToBounds = YES;
        
        HUD.layer.masksToBounds = NO;
        //HUD.layer.shouldRasterize = YES;
        //HUD.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        UIImageView *iBreviaryW = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,180,45)];
        [iBreviaryW setContentMode:UIViewContentModeScaleAspectFit];
        [iBreviaryW setImage:[UIImage imageNamed:@"LogoCarta.png"]];
        [iBreviaryW setBackgroundColor:[UIColor clearColor]];
        [iBreviaryW setAlpha:0.95];
        [iBreviaryW setCenter:CGPointMake(90, 70)];
        [HUD addSubview:iBreviaryW];
        
        testo_hud = [[UILabel alloc] initWithFrame:CGRectMake(0,75,180,35)];
        if([self.defaults boolForKey:AGGIORNAMENTO_2015]){
            [testo_hud setFont:[UIFont fontWithName:FONT_HELVETICANEUE_BOLD size:10]];
            [testo_hud setTextColor:[UIColor whiteColor]];
        }
        else{
            [testo_hud setFont:[UIFont fontWithName:FONT_HELVETICANEUE_BOLD size:11]];
            [testo_hud setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.85]];
        }
        [testo_hud setBackgroundColor:[UIColor clearColor]];
        [testo_hud setTextAlignment:NSTextAlignmentCenter];
        [testo_hud setNumberOfLines:2];
        [testo_hud setMinimumScaleFactor:0.6];
        [testo_hud setTag:1];
        [HUD addSubview:testo_hud];
        
        
        //-> ANNO STRINGA
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        UILabel *anno = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 55)];
        [anno setBackgroundColor:[UIColor clearColor]];
        [anno setFont:[UIFont fontWithName:FONT_HELVETICANEUE_CONDENSEDBLACK size:35]];
        [anno setTextColor:[UIColorFromRGB(0XFFFFFF) colorWithAlphaComponent:0.08]];
        [anno setCenter:CGPointMake(145, 57)];
        [anno setTextAlignment:NSTextAlignmentLeft];
        [dateFormatter setDateFormat:@"YYYY"];
        [anno setText:[[dateFormatter stringFromDate:[NSDate date]] capitalizedString]];
        anno.numberOfLines = 1;
        [HUD addSubview:anno];
        
        
        UIActivityIndicatorView* indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [indicator setCenter:CGPointMake(90, 30)];
        [indicator setTag:2];
        [indicator startAnimating];
        [HUD addSubview:indicator];
    
        CGFloat x = self.logo.center.x;//self.view.frame.size.width/2.0;
        CGFloat y = (self.view.frame.size.height / 2.0) - 10;

        if(IS_IPAD){
            //x = self.isLand?512:384;
            y = self.isLand?384:512;
            //if(IS_IPAD_PRO) x = self.logo.center.x;
        }
    
        [HUD setCenter:CGPointMake(x, y)];

    
    
        HUD.alpha = 0;
        [self.view.superview addSubview:HUD];
        [UIView animateWithDuration:0.1 animations:^{HUD.alpha = 1.0;} completion:^(BOOL finished) {
            [self updateHudLabel:text];
        }];
        
        HUD.layer.transform = CATransform3DMakeScale(0.5, 0.5, 1.0);
        
        CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
        bounceAnimation.values = [NSArray arrayWithObjects:
                                  [NSNumber numberWithFloat:0.5],
                                  [NSNumber numberWithFloat:1.1],
                                  [NSNumber numberWithFloat:0.8],
                                  [NSNumber numberWithFloat:1.0], nil];
        bounceAnimation.duration = 0.3;
        bounceAnimation.removedOnCompletion = NO;
        [HUD.layer addAnimation:bounceAnimation forKey:@"bounce"];
        
        HUD.layer.transform = CATransform3DIdentity;
    return YES;
}

+(void)changeHudText:(NSString *)t{
    // NSLog(@"changeHudText:%@ ",t);
    runOnMainQueueWithoutDeadlocking(^{
        Home *h = [Home sharedInstance];
        if(!h.hudExist){
    
            if([h avviaRotellaWithMex:t]){
                NSLog(@"inizializeOK");
            }
            else{
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [h updateHudLabel:t];
                }];
            }
        }
        else{
            [h updateHudLabel:t];
        }
    });
}

-(void)updateHudLabel:(NSString*)t{
    //Do any updates to your label here
    // NSLog(@"updateHudLabel:%@",t);
    self.testo_hud.text = t;
    if([HUD alpha]!=1.f){
        [HUD setAlpha:1.f];
    }
}

+(void)fineHUD2015{
    Home *h = [Home sharedInstance];
    if(!h.hudExist){
        NSLog(@"fineHUD2015 già avvenuta");
        return;
    }
    [h fineHUD2015];
}

-(void)fineHUD2015{
    HUD.layer.transform = CATransform3DMakeScale(0.5, 0.5, 1.0);
    
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    bounceAnimation.values = [NSArray arrayWithObjects:
                              [NSNumber numberWithFloat:1.0],
                              [NSNumber numberWithFloat:1.1],
                              [NSNumber numberWithFloat:0.8],
                              [NSNumber numberWithFloat:0.2], nil];
    bounceAnimation.duration = 0.3;
    bounceAnimation.removedOnCompletion = NO;
    [HUD.layer addAnimation:bounceAnimation forKey:@"bounce_back"];

    [UIView animateWithDuration:0.3
                     animations:^{
                         [HUD setAlpha:0];
                     }
                     completion:^(BOOL finito){
                         // NSLog(@"finito: rimuoviHUD");
                         [HUD removeFromSuperview];
                         HUD = nil;
                         [self.view setUserInteractionEnabled:YES];
                         self.hudExist = NO;
                         [self performSelector:@selector(mostraNotificaAggiornamento2015_ok) withObject:nil afterDelay:0.6];
                     } ];
}

-(void)fineHUD_widget{
    // NSLog(@"fineHUD_widget");
    if(self.HUD!=nil){
    HUD.layer.transform = CATransform3DMakeScale(0.5, 0.5, 1.0);
    
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    bounceAnimation.values = [NSArray arrayWithObjects:
                              [NSNumber numberWithFloat:1.0],
                              [NSNumber numberWithFloat:1.1],
                              [NSNumber numberWithFloat:0.8],
                              [NSNumber numberWithFloat:0.2], nil];
    bounceAnimation.duration = 0.3;
    bounceAnimation.removedOnCompletion = NO;
    [HUD.layer addAnimation:bounceAnimation forKey:@"bounce_back"];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         [HUD setAlpha:0];
                     }
                     completion:^(BOOL finito){
                         // NSLog(@"rimuoviHUD widget");
                         [HUD removeFromSuperview];
                         self.HUD = nil;
                         [self.view setUserInteractionEnabled:YES];
                         [self activityNetwork:NO];
                         [self.defaults removeObjectForKey:WIDGET_CMD];
                         [self.defaults synchronize];
                     } ];
    }
}

#pragma mark -
#pragma mark ROTELLA-HUD recupera cache
//=========================
// ROTELLA-HUD recupera cache
//=========================
+(void)recuperaCacheMex:(NSString *)t{
    Home *h = [Home sharedInstance];
    if(h.HUD==nil)
    {
        runOnMainQueueWithoutDeadlocking(^{
            [self recuperaCacheMex:t];
        });
    }
}

-(void)recuperaCacheMex:(NSString*)text{
    [self.view setUserInteractionEnabled:NO];
    HUD = nil;
    HUD = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 180, 115)];
    [HUD setBackgroundColor:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.65]];
    [HUD.layer setCornerRadius:10];
    [HUD.layer setBorderColor:[[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9] CGColor]];
    [HUD.layer setBorderWidth:2];
    [HUD.layer setMasksToBounds:YES];
    UIImageView *iBreviaryW = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,180,45)];
    [iBreviaryW setContentMode:UIViewContentModeScaleAspectFit];
    [iBreviaryW setImage:[UIImage imageNamed:@"LogoCarta.png"]];
    [iBreviaryW setBackgroundColor:[UIColor clearColor]];
    [iBreviaryW setAlpha:0.95];
    [iBreviaryW setCenter:CGPointMake(90, 70)];
    [HUD addSubview:iBreviaryW];
    
    
    testo_hud = [[UILabel alloc] initWithFrame:CGRectMake(0,75,180,35)];
    [testo_hud setFont:[UIFont fontWithName:FONT_HELVETICANEUE_BOLD size:10]];
	[testo_hud setBackgroundColor:[UIColor clearColor]];
	[testo_hud setTextColor:[UIColor whiteColor]];
    [testo_hud setTextAlignment:NSTextAlignmentCenter];
    [testo_hud setText:[text uppercaseString]];
    [testo_hud setNumberOfLines:2];
    [testo_hud setMinimumScaleFactor:0.6];
    [testo_hud setTag:1];
    [HUD addSubview:testo_hud];
    
    
    //-> ANNO STRINGA
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
	UILabel *anno = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 55)];
	[anno setBackgroundColor:[UIColor clearColor]];
    [anno setFont:[UIFont fontWithName:FONT_HELVETICANEUE_CONDENSEDBLACK size:35]];
    [anno setTextColor:[UIColorFromRGB(0XFFFFFF) colorWithAlphaComponent:0.08]];
    [anno setCenter:CGPointMake(145, 57)];
	[anno setTextAlignment:NSTextAlignmentLeft];
    [dateFormatter setDateFormat:@"YYYY"];
	[anno setText:[[dateFormatter stringFromDate:[NSDate date]] capitalizedString]];
	anno.numberOfLines = 1;
    [HUD addSubview:anno];
    
    
    UIActivityIndicatorView* indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicator setCenter:CGPointMake(90, 30)];
    [indicator setTag:2];
    [indicator startAnimating];
    [HUD addSubview:indicator];
    
    [HUD setCenter:self.view.center];
    [self.view addSubview:HUD];
    
}

+(void)fineRecuperaCache{
    Home *h = [Home sharedInstance];
    double delayInSeconds = 2.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [h fineRecuperaCacheMain];
    });
}

-(void)fineRecuperaCacheMain{
    [self.view setUserInteractionEnabled:YES];
    [UIView animateWithDuration:1
                     animations:^{ [HUD setAlpha:0];}
                     completion:^(BOOL finished)
                                    {
                                        [HUD removeFromSuperview];
                                        HUD = nil;
                                    }];
}


#pragma mark - NOTIFICHE
//=========================
// NOTIFICHE
//=========================
- (void)initializeBadge
{
    //BADGE
    // NSLog(@"initializeBadge");
    static int const BBoriginX = 27;
    static int const BBoriginY = 2;
    self.badge = [[UILabel alloc] initWithFrame:CGRectMake(BBoriginX, BBoriginY, 20, 20)];
    
    /*
    self.activity_badge = [UIActivityIndicatorView new];
    [self.activity_badge setColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7]];
    [self.activity_badge setCenter:CGPointMake(10, 10)];
    [self.activity_badge setHidden:YES];
    [self.badge addSubview:self.activity_badge];
    */
    
    [self.badge setTextColor:[UIColor whiteColor]];
    [self.badge setBackgroundColor:[UIColor redColor]];
    [self.badge setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [self.badge setTextAlignment:NSTextAlignmentCenter];
    [self.badge.layer setCornerRadius:self.badge.frame.size.width/2.0];
    [self.badge.layer setMasksToBounds:YES];
    [self.badge setHidden:YES];
    [self.badge setUserInteractionEnabled:NO];
    
    [self.notifica_view addSubview:self.badge];
}

+ (void)aggiornaBadge{
    [[Home sharedInstance] aggiornaBadge];
}

- (void)aggiornaBadge{
    runOnMainQueueWithoutDeadlocking(^{
        if(!self.badge.superview){
            [self initializeBadge];
        }
        NSInteger i = [NotificaObject notificheDaleggere];
        [UIApplication sharedApplication].applicationIconBadgeNumber = i;
        if(i>0) {
            NSString *badge_string = [NSString stringWithFormat:@"%i",(int)i];
            [self.badge setText:badge_string];
            [UIApplication sharedApplication].applicationIconBadgeNumber = i;
        }
        NSInteger hc = [[NotificaObject getNotifichePerHome] count];
        [self.notifica_view setHidden:hc<1];
        [self.badge setHidden:(i<=0)];

    });
}

+(void)applicationDidReciveLocalNotificationWhileForeground:(UILocalNotification *)notification{
    // NSLog(@"application Did Recive NOtification");
    Home *h = [Home sharedInstance];
    NotificaObject *n = [NotificaObject notificaConId:[NSNumber numberWithInt:[[notification.userInfo objectForKey:@"id"] intValue]]];
    if(n!=nil){
        [n impostaDaLeggereEbadge];
        [h mostraLocalNotificaBanner:notification];
        [n ScheduleLocalNotification];
    }
}


+(void)aggiungiOperazione:(SEL)selector withObject:(id)obj{
    if(queue==nil)
    {
        queue = [NSOperationQueue new];
        [queue setMaxConcurrentOperationCount:1];
    }
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:[Home sharedInstance] selector:selector object:obj?obj:nil];
    [queue addOperation:operation];
}

-(IBAction)showTabellaNotifica:(UIButton*)sender
{
    if([DDMenuController isMenuOpen])
    {
        [DDMenuController closeMenuWithCompletion:^{[self eseguiTabellaNotifica:sender];}];
        
    }
    else [self eseguiTabellaNotifica:sender];
}


-(void)eseguiTabellaNotifica:(UIButton*)sender{
    tabella_notificheCTRL =  nil;
    CGFloat topoffset = 5;
    CGFloat row_height = 40;
    
    CGPoint origin = CGPointMake(sender.center.x-16, sender.center.y+20);
    
	self.tabella_notificheCTRL = [[Tabella_notificheHome alloc] initWithDelegate:self fromButtonCenter:[self.view convertPoint:sender.center fromView:sender.superview]];
    int i = (int)[[NotificaObject getNotifichePerHome] count];
    CGPoint shapePosition = [self.view convertPoint:origin fromView:sender.superview];
    CGRect frame = CGRectMake(shapePosition.x,shapePosition.y, 260, (topoffset*2)+(row_height*i));
    // NSLog(@"FRAME:%@",NSStringFromCGRect(frame));
    
    blurView_listaNotifiche = [[FXBlurView alloc] initWithFrame:frame];
    
    //[blurView_listaNotifiche setTintColor:[UIColor brownColor]];
    [blurView_listaNotifiche setDynamic:NO];
    [blurView_listaNotifiche.layer setCornerRadius:14];
    [blurView_listaNotifiche.layer setMasksToBounds:YES];
    //[blurView_listaNotifiche.layer setBorderWidth:2.3];
    //[blurView_listaNotifiche.layer setBorderColor:[UIColorFromRGB(0X472a0b) CGColor]];

    [blurView_listaNotifiche setAlpha:0];
    [tabella_notificheCTRL.view setAlpha:0];
    //[self.view addSubview:blurView_listaNotifiche];
    [self.view addSubview:tabella_notificheCTRL.view];

    [UIView animateWithDuration:0.7
                     animations:^{
                                [blurView_listaNotifiche setAlpha:0.95];
                                [tabella_notificheCTRL.view setAlpha:1];}
                     completion:^(BOOL finished){
                                // NSLog(@"FineAppareTabellaNotifica");
                     }];
}

-(void)removeTabella_notificheHome{
    // NSLog(@"selezionataNotificaDaMenu");
    [UIView animateWithDuration:0.7
                     animations:^{
                                [blurView_listaNotifiche setAlpha:0];
                                [tabella_notificheCTRL.view setAlpha:0];}
                     completion:^(BOOL finished){
                                // NSLog(@"FineScompareTabellaNotifica");
                                [blurView_listaNotifiche removeFromSuperview];
                                [tabella_notificheCTRL.view removeFromSuperview];
                                blurView_listaNotifiche = nil;
                                tabella_notificheCTRL = nil;
                                [Home aggiornaBadge];
                                }];
}


-(void)selezionataNotificaDaMenu:(NotificaObject *)notifica{
    // NSLog(@"selezionataNotificaDaMenu");
    [self showNotifica:notifica];
}

-(void)addDebugMode{
//    UILongPressGestureRecognizer *tapAndHold = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapAndHold)];
//    [tapAndHold setMinimumPressDuration:5.0];
//    [self.area_attribute addGestureRecognizer:tapAndHold];
//    [self.area_centrale addGestureRecognizer:tapAndHold];
//    [self.view addGestureRecognizer:tapAndHold];
}

//=========================
// BUGFIX DEBUG
//=========================
#pragma mark - BUGFIX DEBUG
//BugFixButton
-(IBAction)bugFixAction:(UIButton*)sender{
//    for(NotificaObject *notifica in [NotificaObject getNotificheInContext:nil]){
//        [notifica setLetto:[NSNumber numberWithInteger:0]];
//        if(![notifica saveInContext:[AppDelegate mainManagedObjectContext]]){
//            [notifica saveInContext:[AppDelegate privateQueueContext]];
//        }
//    }
//    
//    [Home aggiornaBadge];
//
//    return;
    NSString *risposta = [NotificaObject debugString];
    
    __block CGRect originalFrame = self.DebugTextView.superview.frame;
    CGRect frame = self.DebugTextView.superview.frame;
    frame.origin.y = self.DebugView.frame.size.height;
    [self.DebugTextView.superview setFrame:frame];
    [self.DebugTextView setText:risposta];
    [self.DebugView setHidden:NO];

    animation_block = ^{
        //calc height
        [self.DebugTextView.superview setFrame:originalFrame];
    };
    
    completition_block = ^(BOOL finished) {
        //BugView Showed!
    };
    
    [UIView animateWithDuration:0.6
                          delay:0
         usingSpringWithDamping:0.65
          initialSpringVelocity:1
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:animation_block
                     completion:completition_block];

    // NSLog(@"\n%@\n",risposta);
}

//DISMISS DEBUG
-(IBAction)dismissDebug:(UIButton*)sender{
    __block CGRect originalFrame = self.DebugTextView.superview.frame;
    animation_block = ^{
        //calc height
        CGRect frame = self.DebugTextView.superview.frame;
        frame.origin.y = self.DebugView.frame.size.height;
        [self.DebugTextView.superview setFrame:frame];
    };
    
    completition_block = ^(BOOL finished) {
        //rect a posto!!
        [self.DebugView setHidden:YES];
        [self.DebugTextView.superview setFrame:originalFrame];
        
           };
    
        [UIView animateWithDuration:0.6
                              delay:0
             usingSpringWithDamping:0.65
              initialSpringVelocity:1
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:animation_block
                         completion:completition_block];
}

//SECRET FUNCTION
-(void)handleTapAndHold{
    if([[NSUserDefaults standardUserDefaults] boolForKey:IBREVIARY_DEBUG_MODE]){
    //DISATTIVA DEBUGMODE
    [self alert:@"iBreviary debug Mode"
         andMex:@"vuoi tornare alla modalità normale?"
           icon:nil
       okAction:^(UIAlertAction *action) {
                [self performDebug:NO deletingCloud:YES];
                  }
     withCancel:YES];
    }
    else{
    //ATTIVA DEBUGMODE
    [self alert:@"iBreviary debug Mode"
         andMex:@"vuoi attivare debugMode?"
           icon:nil
       okAction:^(UIAlertAction *action) {
                [self performDebug:YES deletingCloud:YES];
                }
     withCancel:YES];
    }
}

+(void)performDebug:(BOOL)debugFlag{
    [[Home sharedInstance] performDebug:debugFlag deletingCloud:NO];
}

-(void)performDebug:(BOOL)debugFlag deletingCloud:(BOOL)deleteCloud{
    // STEP 1
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"performDebug(1)");
        if(debugFlag)
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IBREVIARY_DEBUG_MODE];
        else
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:IBREVIARY_DEBUG_MODE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if(deleteCloud)[AppDelegate debugDeleteiCLoud:nil];
    }];//fine step1
    
    
    // STEP 2
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"performDebug(2)");
        [NotificaObject resetNotifiche];
    }];//fine step2
    
    
    // STEP 3
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"performDebug(3):\n%@",[NotificaObject getNotifiche]);
        [NotificaObject updateHome];
    }];//fine step3
    
    // STEP 3
    NSBlockOperation *step4 = [NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"performDebug(4)");
        [NotificaObject checkNotifiche];
    }];//fine step3

    
    // STEP 3
    NSBlockOperation *step5 = [NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"performDebug(5)");
        [NotificaObject updateHome];
        [self.bugFix_btn setHidden:!debugFlag];
    }];//fine step3

    
    [step2 addDependency:step1];
    [step3 addDependency:step2];
    [step4 addDependency:step3];
    [step5 addDependency:step4];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    [[NSOperationQueue mainQueue] addOperation:step3];
    [[NSOperationQueue mainQueue] addOperation:step4];
    [[NSOperationQueue mainQueue] addOperation:step5];
}


//=========================
// NOTIFICHE CONTROLLER
//=========================
#pragma mark - NOTIFICHE
//from appdelegateNotification
+(void)mostraNotifca:(UILocalNotification*)notifica_di_sistema{
    NSNumber *notifica_id = [NSNumber numberWithInt:[[notifica_di_sistema.userInfo objectForKey:@"id"] intValue]];
    NotificaObject *n = [NotificaObject notificaConId:notifica_id];
    //NSLog(@"Home: mostraNotifica:%@",n);
    if(n){
        // NSLog(@"Home: Notifica Trovata:%@",n);
        [[Home sharedInstance] showNotifica:n];
    }
    else{
        NSLog(@"Home: Notifica %@ non esiste più!",notifica_id);
    }
}

+(void)showNotifica:(NotificaObject*)notifica{
    [[Home sharedInstance] showNotifica:notifica];
}

-(void)showNotifica:(NotificaObject*)notifica{
    
    [self.coda_notifiche addObject:[NSNumber numberWithInteger:notifica.id_notifica.integerValue]];

    runOnMainQueueWithoutDeadlocking(^{
        if([DDMenuController isMenuOpen]){
            [DDMenuController closeMenuWithCompletion:^{
                //NSLog(@"Chiuso menù prima di mostrare notifica");
            }];
        }
        
    [self showNotificaDaCoda];
        
    });
}

-(void)showNotificaDaCoda
{
    if(self.showingNotifica){
        return;
    }
    runOnMainQueueWithoutDeadlocking(^{
        self.showingNotifica = YES;
        NotificaObject *notifica = [NotificaObject notificaConId:[self.coda_notifiche lastObject]];
        [self.coda_notifiche removeLastObject];

        NSLog(@"showNotificaDaCoda:%@",notifica);
        if(!notifica) return;
        self.notificaCTRL = nil;
        self.notificaCTRL = [[NotificaCTRL alloc] initWithDelegate:self andNotifica:notifica];
        
        // NSLog(@"notificaCTRL:%@ %@",notificaCTRL.notifica, notificaCTRL.view);
        if(!IS_IPAD)[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
        
        
        [self.notificaCTRL.view setCenter:(CGPoint){self.view.frame.size.width/2.0,- (self.notificaCTRL.view.frame.size.height/2.0)}];
        [self.view addSubview:self.notificaCTRL.view];
        
        [self.notificaCTRL.view setBackgroundColor:[UIColor clearColor]];
        
        [self blurView:self.notificaCTRL.view
       withEffectStyle:UIBlurEffectStyleDark];
        
        
        animation_block = ^{
            if(self.tabella_notificheCTRL.view){
                [blurView_listaNotifiche setAlpha:0];
                [tabella_notificheCTRL.view setAlpha:0];
            }
            
            [self.notificaCTRL.view setCenter:(CGPoint){self.view.frame.size.width/2.0,self.view.frame.size.height/2.0}];
        };
        
        completition_block = ^(BOOL finished) {
            //NSLog(@"FineAppareNotifica");
            if(self.tabella_notificheCTRL.view){
                [blurView_listaNotifiche removeFromSuperview];
                [tabella_notificheCTRL.view removeFromSuperview];
                blurView_listaNotifiche = nil;
                tabella_notificheCTRL = nil;
                [Home aggiornaBadge];
            }
            
        };
        
        [UIView animateWithDuration:IS_IPAD?0.7:0.6
                              delay:0
             usingSpringWithDamping:0.65
              initialSpringVelocity:IS_IPAD?3:3
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:animation_block
                         completion:completition_block];
    });
    
}



//dispatch_semaphore_t sem;
//
//
//
//-(void)showNotificaBackGround:(NotificaObject*)notifica
//    {
//    // NSLog(@"showNotifica:%@",notifica);
//        if ([NSThread isMainThread]) {
//            NSLog(@"showNotificaBackGround in MAIN: erroreee!!!");
//            //exit(0);
//            return;
//            }
//        
//        BOOL aspetta = YES;
//
//        //background thread
//        
//        //creo semaforo notifiche se non esiste
//        if(!sem){
//            aspetta = NO;
//            sem = dispatch_semaphore_create(0);
//        }
//
//        if(aspetta || notificaCTRL!=nil){
//            NSLog(@"Attendereeeee c'è un altra notifica!!!");
//            //aspetto semaforo verde <<<
//            dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
//        }
//        else{
//            //>>>semaforo è verde!
//        }
//        
//        
//    dispatch_sync(dispatch_get_main_queue(), ^ {
//          
//    notificaCTRL = nil;
//	notificaCTRL = [[NotificaCTRL alloc] initWithDelegate:self andNotifica:notifica];
//        // NSLog(@"notificaCTRL:%@ %@",notificaCTRL.notifica, notificaCTRL.view);
//    [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
//
//        
//    [self.notificaCTRL.view setCenter:(CGPoint){self.view.frame.size.width/2.0,- (self.notificaCTRL.view.frame.size.height/2.0)}];
//    [self.view addSubview:self.notificaCTRL.view];
//        
//        [self.notificaCTRL.view setBackgroundColor:[UIColor clearColor]];
//    
//        [self blurView:self.notificaCTRL.view
//       withEffectStyle:UIBlurEffectStyleDark];
//
//    
//        animation_block = ^{
//            if(self.tabella_notificheCTRL.view){
//                [blurView_listaNotifiche setAlpha:0];
//                [tabella_notificheCTRL.view setAlpha:0];
//                }
//
//            [self.notificaCTRL.view setCenter:(CGPoint){self.view.frame.size.width/2.0,self.view.frame.size.height/2.0}];
//        };
//        
//        completition_block = ^(BOOL finished) {
//            //NSLog(@"FineAppareNotifica");
//            [self.view setUserInteractionEnabled:YES];
//            if(self.tabella_notificheCTRL.view){
//                [blurView_listaNotifiche removeFromSuperview];
//                [tabella_notificheCTRL.view removeFromSuperview];
//                blurView_listaNotifiche = nil;
//                tabella_notificheCTRL = nil;
//                [Home aggiornaBadge];
//            }
//            
//        };
//
//        [UIView animateWithDuration:IS_IPAD?0.7:0.6
//                              delay:0
//             usingSpringWithDamping:0.65
//              initialSpringVelocity:IS_IPAD?3:3
//                            options:UIViewAnimationOptionCurveEaseInOut
//                         animations:animation_block
//                         completion:completition_block];
//
////    [UIView transitionWithView:self.view
////                      duration:0.6
////                       options: UIViewAnimationOptionTransitionCurlDown
////                    animations: ^{
////                        if(self.isIpad){
////                            [self addBlurNotifica:notificaCTRL];
////                        }
////                        else [self.view addSubview:self.blurView];
////                        [self.view addSubview:self.notificaCTRL.view];
////                        
////                        }
////                    completion:^(BOOL finished){
////                                //NSLog(@"FineAppareNotifica");
////                                [self.view setUserInteractionEnabled:YES];
////                            }];
//    });
//    
//        }


-(void)removeNotificaCTRL{
    NSLog(@"Home: removeNotificaCTRL");
    
    
    animation_block = ^{
        [self.notificaCTRL.view setCenter:(CGPoint){self.notificaCTRL.view.center.x,self.notificaCTRL.view.center.y-(IS_IPAD?15:15)}];
    };
    
    completition_block = ^(BOOL finished) {
        //runOnMainQueueWithoutDeadlocking(^{
            //>>>>>> MAIN CODE
            NSLog(@"didFinishRemoveNotifica:%i",[NSThread isMainThread]);
            [self.notificaCTRL.view removeFromSuperview];
            if(fake_home_blur!=nil)[fake_home_blur removeFromSuperview];
            [self.blurView removeFromSuperview];
            self.blurView = nil;
            self.notificaCTRL =  nil;
            fake_home_blur = nil;
            self.showingNotifica = NO;
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if(self.coda_notifiche.count==0){
                    [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
                }
                
                else if(self.coda_notifiche.count>0){
                    NSLog(@"ancora codaaaaaaa:%@",self.coda_notifiche);
                    [self performSelector:@selector(showNotificaDaCoda) withObject:nil afterDelay:0.1];
                }
            }];
    };
    

    runOnMainQueueWithoutDeadlocking(^{
        //>>>>>> MAIN CODE
        //animation one
        [UIView animateWithDuration:0.1
                         animations:animation_block
                         completion:^(BOOL finished) {
                             //animation two
                             [UIView animateWithDuration:0.35
                                              animations:^{
                                                  [self.notificaCTRL.view setCenter:(CGPoint){self.view.frame.size.width/2.0,self.view.frame.size.height + (self.notificaCTRL.view.frame.size.height/2.0)}];
                                                  [self.notificaCTRL.view setAlpha:0.8];
                                                  [self.blurView setAlpha:0.0];
                                              }
                                              completion:completition_block];

                         }];
        
        });
}


#pragma mark -
#pragma mark settaHOME
//=========================
// SETTA-HOME
//=========================
-(void)settaHOME{
    
    [self AttivaRotella];
    
    // NSLog(@"SettaHOME!!:%@",day);
    
    //->DETTAGLIO VIEW
    [label_day_type setText:@""];
    [label_day_time setText:@""];
    [label_lingua setText:@""];
    [titolo_data setText:@""];
    [titolo_ufficiale setText:@""];
    
    
    [btn_lingua setAlpha:0.9];
    [label_day_type_tit setHidden:NO];
    [label_day_time_tit setHidden:NO];
    [label_lingua_tit setHidden:NO];
    
    
    //Animazione DATA
    [self animazioneTitolo_Data];
    
    //Animazione Type Titolo
    [label_day_type performSelector:@selector(animateText:) withObject:[day.day_type uppercaseString] afterDelay:0.8];
    
    //Animazione Bottone
    [self performSelector:@selector(animazioneBottone) withObject:nil afterDelay:1];
    
    
    //Animazione Type Day Time
    [label_day_time performSelector:@selector(animateText:) withObject:[day.day_time uppercaseString] afterDelay:1.1];

    
    //LINEA DINAMICA SECONDO TYPE!!
    [linea_dayInfo setHidden:NO];
    BOOL isType = day.type && ![day.type isEqualToString:@""] && ![day.type isEqualToString:@"generic"];
    CGRect linea_frame = linea_dayInfo.frame;
    linea_frame.size.height = isType?119:85;
    [linea_dayInfo setFrame:linea_frame];
    
    [day_info_TP_logo setHidden:!isType];
    [day_info_TP_title setHidden:!isType];
    
    //DAY WITH TP_!home
    if(isType)
    {
        BOOL is4HomeMenu = [TestoProprio isForHomeTPwithKey:day.type];
        TestoProprio *ttt = [TestoProprio getTestoProprioWithKey:is4HomeMenu?day.type_home:day.type];
        [day_info_TP_logo setImage:ttt.logo];
        NSString *tit = is4HomeMenu?(day.tpHome_missal?day.tpHome_missal.title:day.tpHome_breviario.title):(day.tp_missal?day.tp_missal.title:day.tp_breviario.title);
        tit = [tit uppercaseString];
        if(tit != nil){
        
        NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
        style.minimumLineHeight = day_info_TP_title.font.pointSize;
        style.maximumLineHeight = day_info_TP_title.font.pointSize;
        NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
        //set text with attribute
        day_info_TP_title.attributedText = [[NSAttributedString alloc] initWithString:tit
                                                                 attributes:attributtes];
            
        //[day_info_TP_title setBackgroundColor:[UIColor redColor]];
        }
        
    }
    
    //->SANTO VIEW
    [pager setHidden:YES];
    [self buildSaintScroller];

    
    if(self.isIpad && lista_popover.isPopoverVisible) [lista_popover dismissPopoverAnimated:YES];

    if(IS_IPAD && IS_IPAD_PRO)
    [self controllaRotazioneIPAD];
}

NSString *controlloImmagine_sigla;


-(void)animazioneTitolo_Data{
    [titolo_data setText:day.date];
    [titolo_ufficiale setAlpha:0];
    [titolo_data setAlpha:0];
    [titolo_ufficiale setText:[day.title uppercaseString]];
    [UIView animateWithDuration: 0.4
                          delay:0
                        options: UIViewAnimationOptionCurveLinear
                     animations: ^{
                         [titolo_data setAlpha:1];
                     }
                     completion:^(BOOL finished){
                     }];
    [UIView animateWithDuration: 0.6
                          delay:0.1
                        options: UIViewAnimationOptionCurveLinear
                     animations: ^{
                         [titolo_ufficiale setAlpha:1];
                     }
                     completion:^(BOOL finished){
                     }];
    
}


-(void)fineAnimazioneTitolo{
    }

-(void)animazioneBottone{
    
    if(!controlloImmagine_sigla || ![day.linguaGiorno.sigla isEqualToString:controlloImmagine_sigla])
            {
                controlloImmagine_sigla = day.linguaGiorno.sigla;
                [self cardRotation:btn_lingua destination:[day.linguaGiorno  logo]];
            }
    [label_lingua performSelector:@selector(animateText:) withObject:[[day.linguaGiorno nome] uppercaseString] afterDelay:0.3];
    
    [self rimuoviRotella];
}

#pragma mark - NAVBAR
//=========================
// NAVBAR
//=========================
-(IBAction)toggleNavBar:(UIButton*)sender{
    if(PlayScroll) return;
    CGRect rect = self.NAVBAR.frame;
    if(rect.origin.y != 0-self.NAVBAR.frame.size.height-4) [self SuNavBar:nil];
    else [self MostraNavBar:nil];
}

- (IBAction)SuNavBar:(id)sender
{
    if(self.isIpad)
        {
            [self SuNavBarIPAD:sender];
            return;
        }
    CGRect rect = self.NAVBAR.frame;
    if(rect.origin.y > -self.NAVBAR.frame.size.height)
    {
    // NSLog(@"SuNavBar:%@",NSStringFromCGRect(rect));
    rect.origin.y = 0-self.NAVBAR.frame.size.height-4;
        if (self.isIpad) {
            rect.origin.y -=14;
        }
    CGRect scroller_rect = self.autoscroller_area.frame;
    if(self.isIphone5 || self.isIos7orUpper) scroller_rect.origin.y = [self.defaults boolForKey:SOLO_FULLSCREEN]?10:60;
        else scroller_rect.origin.y = [self.defaults boolForKey:SOLO_FULLSCREEN]?10:49;
    
    //FULL OUT CHECK BUTTON
    [fullOutOnText_btn setHidden:[self.defaults boolForKey:SOLO_FULLSCREEN]];
    
    CGRect btn_MySpiritualplace = self.mySpiritual_btn.frame;
    btn_MySpiritualplace.origin.y = 0;
    CGRect btn_notifica = self.notifica_view.frame;
    btn_notifica.origin.y = 6;
    btn_notifica.origin.x = 48;

    
    CGRect  logo_place = self.logo.frame;
         logo_place.origin.y -= self.isIphone5?40:-10;
        
    CGRect tit_place = self.titolo_ufficiale.frame;
    tit_place.origin.y -= 20;
    
    CGRect data_place = self.titolo_data.frame;
        data_place.origin.y -= self.isIphone5?40:55;
        
    [UIView beginAnimations:@"MuoviSuNavBar" context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(SuNavBar_step2)];
    self.NAVBAR.frame = rect;
    self.autoscroller_area.frame = scroller_rect;
    //self.info_btn.frame = btn_infoplace;
    self.mySpiritual_btn.frame = btn_MySpiritualplace;
    if(!IS_IPHONE_6 && !IS_IPHONE_6_PLUS) self.titolo_data.frame = data_place;
    [UIView commitAnimations];
    
    [UIView beginAnimations:@"MuoviLogoSu" context:NULL];
    [UIView setAnimationDuration:0.8];
    if(!IS_IPHONE_6 && !IS_IPHONE_6_PLUS) self.logo.frame =  logo_place;
    self.notifica_view.frame = btn_notifica;
    [UIView commitAnimations];
        
    
    if(!IS_IPHONE_6 && !IS_IPHONE_6_PLUS){
        [UIView beginAnimations:@"MuoviLogoSu" context:NULL];
        [UIView setAnimationDuration:0.9];
        self.titolo_ufficiale.frame = tit_place;
        [UIView commitAnimations];
    
        [UIView beginAnimations:@"MuoviDataSu" context:NULL];
        [UIView setAnimationDuration:0.7];
        self.titolo_data.frame = data_place;
        [UIView commitAnimations];
        }
        
    if ([[DDMenuController sharedInstance] respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
            // >= iOS 7
            [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
        }
    }
}

-(void)SuNavBarIPAD:(id)sender{
    CGRect rect = self.NAVBAR.frame;
    if(rect.origin.y > -self.NAVBAR.frame.size.height)
    {
        // NSLog(@"SuNavBar:%@",NSStringFromCGRect(rect));
        rect.origin.y -= self.NAVBAR.frame.size.height;
        
        CGRect scroller_rect = self.autoscroller_area.frame;
        scroller_rect.origin.y = 20;
        
        CGRect btn_notifica = self.notifica_view.frame;
        btn_notifica.origin.y = 6;
        btn_notifica.origin.x = 48;
        
        [UIView beginAnimations:@"MuoviSuNavBar" context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(SuNavBarIPAD_step2)];
        self.NAVBAR.frame = rect;
        //self.autoscroller_area.frame = scroller_rect;
        [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
        [UIView commitAnimations];
        
        [UIView beginAnimations:@"MuoviLogoSu" context:NULL];
        [UIView setAnimationDuration:0.8];
        self.notifica_view.frame = btn_notifica;
        [UIView commitAnimations];
    }
}

-(void)SuNavBar_step2{
    [webContent setFrame:CGRectMake(0, 0, self.CONTENT.frame.size.width, self.CONTENT.frame.size.height)];
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
}

-(void)SuNavBarIPAD_step2{
    [webContent setFrame:CGRectMake(0, 0, self.CONTENT.frame.size.width, self.CONTENT.frame.size.height)];
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
}

- (IBAction)MostraNavBar:(id)sender
{
    if([self.defaults boolForKey:SOLO_FULLSCREEN]) return;
    if(self.isIpad){
        [self MostraNavBarIPAD:sender];
        return;
    }
    CGRect rect = self.NAVBAR.frame;
    // NSLog(@"MostraNavBar:%@[%i]",NSStringFromCGRect(rect),(rect.origin.y < -20));
    if(rect.origin.y < -20)
    {
        rect.origin.y = -20;
        if(IS_IPHONE_6 || IS_IPHONE_6_PLUS){
            rect.origin.y = 0;
        }
        
        //Contenuto web CTRL
        CGRect scroller_rect = self.autoscroller_area.frame;
        scroller_rect.origin.y = self.isIphone5?69:(self.isIos7orUpper?69:48);
        
        CGRect btn_MySpiritualplace = self.mySpiritual_btn.frame;
        btn_MySpiritualplace.origin.y = 58;
        
        CGRect btn_notifica = self.notifica_view.frame;
        btn_notifica.origin.y = 66;
        btn_notifica.origin.x = 5;
        
        
        CGRect  logo_place = self.logo.frame;
        logo_place.origin.y = self.isIphone5?126:71;
        CGRect tit_place = self.titolo_ufficiale.frame;
        tit_place.origin.y = self.isIphone5?187:80;
        
        CGRect data_place = self.titolo_data.frame;
        data_place.origin.y = 90;
        
        [UIView beginAnimations:@"MostraNavBar" context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(Step2_MostraNavBar)];
        self.NAVBAR.frame = rect;
        [menu2_ios6 setFrame:CGRectMake(0, 0, menu2_ios6.frame.size.width, menu2_ios6.frame.size.height)];
        self.autoscroller_area.frame = scroller_rect;
        self.mySpiritual_btn.frame = btn_MySpiritualplace;
        //self.info_btn.frame = btn_infoplace;
        self.notifica_view.frame = btn_notifica;
        
        [UIView commitAnimations];
        
        [UIView beginAnimations:@"MuoviLogoGiu" context:NULL];
        [UIView setAnimationDuration:0.8];
        self.logo.frame =  logo_place;
        [UIView commitAnimations];
        
        [UIView beginAnimations:@"MuoviLogoGiu" context:NULL];
        [UIView setAnimationDuration:0.8];
        self.titolo_ufficiale.frame = tit_place;
        [UIView commitAnimations];
        
        [UIView beginAnimations:@"MuoviDataGiu" context:NULL];
        [UIView setAnimationDuration:0.7];
        self.titolo_data.frame = data_place;
        [UIView commitAnimations];
    }
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
        [[DDMenuController sharedInstance] performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
}

- (IBAction)MostraNavBarIPAD:(id)sender
{
    if([self.defaults boolForKey:SOLO_FULLSCREEN]) return;
    CGRect rect = self.NAVBAR.frame;
    // NSLog(@"MostraNavBar:%@[%i]",NSStringFromCGRect(rect),(rect.origin.y < -20));
    if(rect.origin.y < 0)
    {
        rect.origin.y = 0;
        
        //Contenuto web CTRL
        CGRect scroller_rect = self.autoscroller_area.frame;
        scroller_rect.origin.y = self.isIphone5?69:(self.isIos7orUpper?69:48);
        
        CGRect btn_notifica = self.notifica_view.frame;
        btn_notifica.origin.y = 66;
        btn_notifica.origin.x = 5;
        
        [UIView beginAnimations:@"MostraNavBar" context:NULL];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(Step2_MostraNavBarIPAD)];
        self.NAVBAR.frame = rect;
        self.notifica_view.frame = btn_notifica;
        [UIView commitAnimations];
    }
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
        [[DDMenuController sharedInstance] performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
}


-(void)checkFull{
    // NSLog(@"checkFull");
    if(self.isIpad)
    {
        [self checkFullIPAD];
        return;
    }
    CGRect rect = self.NAVBAR.frame;
    
    if([self.defaults boolForKey:SOLO_FULLSCREEN] && (rect.origin.y < 30)){
        
        [self SuNavBar:self];
    }
    
    CGRect frame = oggi2_btn.frame;
    frame.origin.x = [self.defaults boolForKey:SOLO_FULLSCREEN]?145:100;
    if(IS_IPHONE_6){
        frame.origin.x = [self.defaults boolForKey:SOLO_FULLSCREEN]?196:151;
    }
    if(IS_IPHONE_6_PLUS){
        frame.origin.x = [self.defaults boolForKey:SOLO_FULLSCREEN]?196:151;
    }
    [UIView animateWithDuration:0.6
                     animations:^{
                         [oggi2_btn setFrame:frame];
                         [fullOutOnHome_btn setHidden:[self.defaults boolForKey:SOLO_FULLSCREEN]];
                         [fullOutOnText_btn setHidden:[self.defaults boolForKey:SOLO_FULLSCREEN]];
                     }
     
                     completion:^(BOOL finished){
                         if ([[DDMenuController sharedInstance] respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
                                // >= iOS 7
                                [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
                            } else {
                                // iOS 6
                                [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
                            }
                     }];

}

-(void)checkFullIPAD{
    CGRect rect = self.NAVBAR.frame;
    // NSLog(@"ios7:%i menu2_ios6:%@ rect:%@",self.isIos7orUpper,NSStringFromCGRect(menu2_ios6.frame),NSStringFromCGRect(rect));
    
    if([self.defaults boolForKey:SOLO_FULLSCREEN] && (rect.origin.y < 30)){
        
        [self SuNavBar:self];
    }
    
    
    CGRect frame = oggi2_btn.frame;
    frame.origin.x = self.isLand?([self.defaults boolForKey:SOLO_FULLSCREEN]?880:830):([self.defaults boolForKey:SOLO_FULLSCREEN]?622:574);
    
    [UIView animateWithDuration:0.6
                     animations:^{
                         [oggi2_btn setFrame:frame];
                         [fullOutOnHome_btn setHidden:[self.defaults boolForKey:SOLO_FULLSCREEN]];
                         [fullOutOnText_btn setHidden:[self.defaults boolForKey:SOLO_FULLSCREEN]];
                         CGRect autoScrollerFrame = autoscroller_area.frame;
                         autoScrollerFrame.origin.y = [self.defaults boolForKey:SOLO_FULLSCREEN]?5:66;
                         [autoscroller_area setFrame:autoScrollerFrame];
                     }
     
                     completion:^(BOOL finished){
                         [[DDMenuController sharedInstance] performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
                         
                     }];
    
}


-(void)Step2_MostraNavBarIPAD{
    // CGRect rect = self.NAVBAR.frame;
    // NSLog(@"Step2_MostraNavBar:%@",NSStringFromCGRect(rect));
    [webContent setFrame:CGRectMake(0,(self.isIphone5)?63:self.isIos7orUpper?63:43, self.CONTENT.frame.size.width, self.CONTENT.frame.size.height-63)];
}



-(void)Step2_MostraNavBar{
    // CGRect rect = self.NAVBAR.frame;
    // NSLog(@"Step2_MostraNavBar:%@",NSStringFromCGRect(rect));
    [webContent setFrame:CGRectMake(0,(self.isIphone5)?63:self.isIos7orUpper?63:43, self.CONTENT.frame.size.width, self.CONTENT.frame.size.height-63)];
}

+(void)modificaEventFlag{
    runOnMainQueueWithoutDeadlocking(^{
        Home *h = [Home sharedInstance];
        [h modificaEventFlagMain];
    });
}

-(BOOL)modificaEventFlagMain{
    TestoProprio * t = [TestoProprio getTestoProprioWithKey:[self.defaults objectForKey:EVENT_FLAG]];
    // NSLog(@"modificaEventFlagMain");
    [self.eventFLAG setImage:nil forState:UIControlStateNormal];
    UIImage *img = [t  logo]?[t  logo]:[t image_official];
    
    [self.eventFLAG setImage:img forState:UIControlStateNormal];
    [self.eventFLAG setImage:img forState:UIControlStateSelected];
    [self.eventFLAG setImage:img forState:UIControlStateHighlighted];
    
    UIImage *img2 = [UIImage imageNamed:@"logoFull.png"];
    if([t.key isEqualToString:@""])
    {
        [self.eventFLAG2 setImage:img2 forState:UIControlStateNormal];
        [self.eventFLAG2 setImage:img2 forState:UIControlStateSelected];
        [self.eventFLAG2 setImage:img2 forState:UIControlStateHighlighted];

    }
    else
    {
        CGSize targetSize = self.eventFLAG2.frame.size;
        
        UIGraphicsBeginImageContextWithOptions(targetSize, NO, 2.0f);
        
        CGRect thumbnailRect = CGRectZero;
        thumbnailRect.origin = CGPointMake( 5, 5 );
        thumbnailRect.size.width  = self.eventFLAG2.frame.size.width-10;
        thumbnailRect.size.height = self.eventFLAG2.frame.size.height-10;
        
        [[t  logo]?[t  logo]:[t image_official] drawInRect:thumbnailRect];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        [self.eventFLAG2 setImage:newImage forState:UIControlStateNormal];
        [self.eventFLAG2 setImage:newImage forState:UIControlStateSelected];
        [self.eventFLAG2 setImage:newImage forState:UIControlStateHighlighted];

    }
    
    return YES;
}

#pragma mark - WIDGET CMD
//=========================
// WIDGET CMD
//=========================
+(void)showTodayItem:(NSString*)pray_query{
    Home *h = [Home sharedInstance];
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"yyyy-MM-dd"];
    h.day = [Day searchForDay:[f stringFromDate:[NSDate date]] withLang:[h.defaults objectForKey:LINGUA]];
    BOOL presente = (h.day != nil);
    // NSLog(@"showTodayItem.%@",h.day);
    NSBlockOperation *step_download = nil;
    if(!presente && !h.connesso) { [h noNet];  return; }
    if(!presente){
            //Your main thread code goes in here
        NSBlockOperation *message_Step = [NSBlockOperation blockOperationWithBlock:^{
            NSDateFormatter *time = [NSDateFormatter new];
            [time setDateFormat:@"eee, dd MMM"];
            [[Home sharedInstance] activityNetwork:YES];
            [Home changeHudText:[NSString stringWithFormat:@"Downloading %@",[[time stringFromDate:[NSDate date]] capitalizedString]]];
                            [NSThread sleepForTimeInterval:0.5];
                            // NSLog(@"download message showed!");
            
        }];
        [[NSOperationQueue mainQueue] addOperation:message_Step];

        step_download = [NSBlockOperation blockOperationWithBlock:^{
            Day *d = [Day scaricaSYNCGiornoconData:[f stringFromDate:[NSDate date]]
                                         withLang:[h.defaults objectForKey:LINGUA]
                                        eventFlag:[TestoProprio getTestoProprioWithKey:[h.defaults objectForKey:EVENT_FLAG]]];
            // NSLog(@"showTodayItemStep_0 - oggi scaricato %@",d);
            [NSThread sleepForTimeInterval:0.5];
            if(d==nil){
                [h noNet];
                return;
                }
            h.day = d;
        }];
        [step_download addDependency:message_Step];
        [[NSOperationQueue mainQueue] addOperation:step_download];
    }//fine !presente
    

    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{ [h showTodayItemStep_1]; }];
    if(step_download!=nil)
        [step1 addDependency:step_download];
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{ [h showTodayItemStep_2:pray_query];  }];
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{ [h showTodayItemStep_3]; }];
    NSBlockOperation *step4 = [NSBlockOperation blockOperationWithBlock:^{ [h settaHomeWithNoAnimation];}];
    NSBlockOperation *step5 = [NSBlockOperation blockOperationWithBlock:^{ [h fineHUD_widget];}];
    [step2 addDependency:step1];
    [step3 addDependency:step2];
    [step4 addDependency:step3];
    [step5 addDependency:step3];
    
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    [[NSOperationQueue mainQueue] addOperation:step3];
    [[NSOperationQueue new] addOperation:step4];
    [[NSOperationQueue mainQueue] addOperation:step5];
}

-(void)showTodayItemStep_1{
    // NSLog(@"showTodayItemStep_1: selct or download");
    [self activityNetwork:NO];
    if(self.isIpad && lista_popover.isPopoverVisible) [lista_popover dismissPopoverAnimated:YES];

    [[MenuViewController sharedInstance] rimuoviMessaMenu];
    [self resetMessa];
}

-(void)showTodayItemStep_2:(NSString*)pray_query{
    // NSLog(@"showTodayItemStep_2: _ MOSTRA ITEM _ :%@",day);
    [day salvaGiornoInUso:nil];
    NSArray *stringArray = [pray_query componentsSeparatedByString:@"="];
    if(stringArray.count==2){
            [Home mostraItem:[self.day getPray:[stringArray objectAtIndex:1]]];
        }
}

-(void)showTodayItemStep_3{
    // NSLog(@"showTodayItemStep_3: _ NASCONDO HOME _");
    if([Home isContentVisible])[self nascondiHome];
}

-(void)settaHomeWithNoAnimation{
    // NSLog(@"showTodayItemStep_4: _ settaHome Background _");
    [day salvaGiornoInUso:nil];

    
    // NSLog(@"SettaHOME - background!!:%@",day);
    [MenuViewController aggiornaVista];

    //->DETTAGLIO VIEW
    [label_day_type_tit setHidden:YES];
    [label_day_time_tit setHidden:YES];
    [label_lingua_tit setHidden:YES];
    
    

    
    [titolo_ufficiale setText:[day.title uppercaseString]];
    [titolo_data setText:day.date];
    [label_day_type setText:[day.day_type uppercaseString]];
    [label_day_time setText:[day.day_time uppercaseString]];
    
    [btn_lingua setImage:[day.linguaGiorno  logo] forState:UIControlStateNormal];
    controlloImmagine_sigla = day.linguaGiorno.sigla;

    [label_lingua setText:[[day.linguaGiorno nome] uppercaseString]];
    
    [btn_lingua setAlpha:0.9];
    [label_day_type_tit setHidden:NO];
    [label_day_time_tit setHidden:NO];
    [label_lingua_tit setHidden:NO];
    
    //LINEA DINAMICA SECONDO TYPE!!
    [linea_dayInfo setHidden:NO];
    BOOL isType = day.type && ![day.type isEqualToString:@""] && ![day.type isEqualToString:@"generic"];
    CGRect linea_frame = linea_dayInfo.frame;
    linea_frame.size.height = isType?119:85;
    [linea_dayInfo setFrame:linea_frame];
    
    [day_info_TP_logo setHidden:!isType];
    [day_info_TP_title setHidden:!isType];
    
    //DAY WITH TP_!home
    if(isType)
    {
        BOOL is4HomeMenu = [TestoProprio isForHomeTPwithKey:day.type];
        TestoProprio *ttt = [TestoProprio getTestoProprioWithKey:is4HomeMenu?day.type_home:day.type];
        [day_info_TP_logo setImage:ttt.logo];
        NSString *tit = is4HomeMenu?(day.tpHome_missal?day.tpHome_missal.title:day.tpHome_breviario.title):(day.tp_missal?day.tp_missal.title:day.tp_breviario.title);
        tit = [tit uppercaseString];
        if(tit != nil){
            
            NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
            style.minimumLineHeight = day_info_TP_title.font.pointSize;
            style.maximumLineHeight = day_info_TP_title.font.pointSize;
            NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
            //set text with attribute
            day_info_TP_title.attributedText = [[NSAttributedString alloc] initWithString:tit
                                                                               attributes:attributtes];
            }
        
    }
    
    //->SANTO VIEW
    [pager setHidden:YES];
    [self buildSaintScroller];
}

//                               @"pos=INT"
+(void)showSantoAtPos:(NSString*)sq{
    __block NSString *santo_query = [NSString stringWithString:sq];
    __block Home *h = [Home sharedInstance];
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"yyyy-MM-dd"];
    h.day = [Day searchForDay:[f stringFromDate:[NSDate date]] withLang:[h.defaults objectForKey:LINGUA]];
    BOOL presente = (h.day != nil);
    // NSLog(@"showSantoAtPosWithday:%@",h.day);
    NSBlockOperation *step_download = nil;
    if(!presente && !h.connesso) {[h noNet];  return; }
    if(!presente){
        NSBlockOperation *message_Step = [NSBlockOperation blockOperationWithBlock:^{
                                                NSDateFormatter *time = [NSDateFormatter new];
                                                [time setDateFormat:@"eee, dd MMM"];
                                                [Home changeHudText:[NSString stringWithFormat:@"Downloading %@",[[time stringFromDate:[NSDate date]] capitalizedString]]];
                                                // NSLog(@"santo download message showed!");
                                        }];
        [[NSOperationQueue mainQueue] addOperation:message_Step];
        //Your main thread code goes in here
        step_download = [NSBlockOperation blockOperationWithBlock:^{
            Day *d = [Day scaricaSYNCGiornoconData:[f stringFromDate:[NSDate date]]
                                          withLang:[h.defaults objectForKey:LINGUA]
                                         eventFlag:[TestoProprio getTestoProprioWithKey:[h.defaults objectForKey:EVENT_FLAG]]];
            // NSLog(@"showTodayItemStep_0 - oggi scaricato %@",d);
            [NSThread sleepForTimeInterval:0.5];
            if(d==nil){
                [h noNet];
                return;
            }
            h.day = d;
        }];
        [step_download addDependency:message_Step];
        [[NSOperationQueue mainQueue] addOperation:step_download];
    }//fine !presente


    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{[h activityNetwork:NO];}];
    NSBlockOperation *step1b = [NSBlockOperation blockOperationWithBlock:^{[Home mostraHome];}];
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
                                                                [h GoToSanto:[santo_query stringByReplacingOccurrencesOfString:@"pos=" withString:@""]];
                                                                }];
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{ [h settaHomeWithNoAnimation]; }];
    NSBlockOperation *step4 = [NSBlockOperation blockOperationWithBlock:^{ [h fineHUD_widget]; }];
    
    if(step_download!=nil) [step1 addDependency:step_download];
    [step1b addDependency:step1];
    [step2 addDependency:step1b];
    [step3 addDependency:step2];
    [step4 addDependency:step3];
    [[NSOperationQueue new] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step1b];
    [[NSOperationQueue mainQueue] addOperation:step2];
    [[NSOperationQueue mainQueue] addOperation:step3];
    [[NSOperationQueue mainQueue] addOperation:step4];
}


+(BOOL)iSForBook{
    Home *h = [Home sharedInstance];
    return h.iSForBook;
}

+(BOOL)toggleBook{
    Home *h = [Home sharedInstance];
    h.iSForBook = !h.iSForBook;
    return h.iSForBook;
}

+(void)resetBookViewBool{
    Home *h = [Home sharedInstance];
    h.iSForBook = NO;
}



#pragma mark - MENU_NAVIGATION
//=========================
// MASTER_NAVIGATION
//=========================
+(void)mostraItem:(Item*)item{
    
    if(item==nil || item.contenuto==nil){
        // NSLog(@"Error mostraItem VUOTO");
        return;
    }
    
    __block Home *h = [Home sharedInstance];
    
    __block NSString *stringaWebContent;
    __block NSString *path = [[NSBundle mainBundle] bundlePath];
    __block NSURL *baseURL = [NSURL fileURLWithPath:path];
    
    //========
    // STEP 1
    //========
    //sourceString();
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        currentItem = item;
        currentMessa = nil;
        
        //MOSTRO OGGETTO
        h.web_title = item.titolo;
        h.web_text = item.contenuto;
        
        
        [h.sfondo_black setHidden:([h.defaults integerForKey:CSS] != 2)];
        
        //RESET AREA AUTOSCROLL
        if(!h.isIos7orUpper && !h.isIphone5)
        {
            CGRect autoframe = h.autoscroller_area.frame;
            autoframe.origin.y = ([h.defaults boolForKey:SOLO_FULLSCREEN] || h.isLand)?2:49;
            [h.autoscroller_area setFrame:autoframe];
        }
        
        //AUDIO RESET
        [h resetAudio];
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        NSString *header=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/header_%i.html",path,(int)[h.defaults integerForKey:CSS]] encoding:NSUTF8StringEncoding error:nil];
        
        NSString *footer=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/footer.html",path] encoding:NSUTF8StringEncoding error: nil];
        
        if(![[h.webContent stringByEvaluatingJavaScriptFromString:@"document.getElementById('ciccio').innerHTML;"] isEqualToString:@""])
        {
            [h.defaults setObject:[h.webContent stringByEvaluatingJavaScriptFromString:@"document.getElementById('ciccio').innerHTML;"] forKey:ZOOM];
            [h.defaults synchronize];
        }
        
        NSString *zoom=[NSString stringWithFormat:@"<div ID=\"ciccio\" class=\"zoomClass\" >%@</div>",[h.defaults objectForKey:ZOOM]];
        
       
        
        stringaWebContent = [NSString stringWithFormat:@"%@%@%@%@%@",header,[h titleForItem:item],zoom,h.web_text,footer];
        // NSLog(@"PAGINAHTML:\n%@\n=============\n",stringaWebContent);
        // NSLog(@"ITEM:\n%@\n%@\n=============\n",item.titolo,item.contenuto);
        
        if(h.iSForBook){
            [h loadPdf];
            [h performSelector:@selector(delayLoadString:)
                    withObject:stringaWebContent
                    afterDelay:1.0];
            }

    }];//fine step2
    
    //========
    // STEP 3
    //========
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        [h.webContent loadHTMLString:stringaWebContent baseURL:baseURL];
        if([Home isHomeVisible]){
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [h nascondiHome];
                // NSLog(@"nascondiHome(MAIN OPERATION)");
            }];
        }
    }];//fine step3
    
    
    
    [step2 addDependency:step1];
    if(!h.iSForBook){
        [step3 addDependency:step2];
    }
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    if(!h.iSForBook){
        [[NSOperationQueue mainQueue] addOperation:step3];
    }
    
    
    //=========
    //debug demo.htm
    //=========
    //h.web_text =[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/demo.htm",path,(int)[h.defaults integerForKey:CSS]] encoding:NSUTF8StringEncoding error:nil];
    
    
        // NSLog(@"self.isLand?%i frame:%@",self.isLand,NSStringFromCGRect(self.view.frame));
}

-(NSString*)titleForItem:(Item*)item{
    NSString *titleHtml;
    
    if(item.titoloClass!=nil)
    {
        titleHtml =[NSString stringWithFormat:@"<div class='%@'>%@</div>",item.titoloClass , self.web_title];
    }
    else titleHtml =[NSString stringWithFormat:@"<div class='sezione'>%@</div>", self.web_title];
    return titleHtml;
}

+(void)reloadPdf{
    [[Home sharedInstance] loadPdf];
}

-(void)loadPdf{
    Item *item = currentItem;
    if(!item){
        [self loadPdfMessa];
        return;
    }
    NSString *path = [[NSBundle mainBundle] bundlePath];

    NSLog(@"PdfBook presente:%@?%@",[item pdf_book_url],[self fileExistsAtPath:[item pdf_book_url]]?@"Y":@"N");
    
    if([self fileExistsAtPath:[item pdf_book_url]]){
        NSLog(@"PdfBook presente");
        //[self.book_rotella stopAnimating];
        //[self.book_rotella setHidden:YES];
        [Home mostraPdfBookwithUrl:[item pdf_book_url]];
        return;
    }
    
    self.htmlPdfKit = [[BNHtmlPdfKit alloc] initWithPageSize:IS_IPAD?BNPageSizeA4:BNPageSizeIphone];
    self.htmlPdfKit.delegate = self;
    
    NSString*fontType= [[NSUserDefaults standardUserDefaults] objectForKey:PDF_FONT_TYPE]?[[NSUserDefaults standardUserDefaults] objectForKey:PDF_FONT_TYPE]:@"1";
    NSString *headerPDF=[NSString stringWithContentsOfFile:[NSString stringWithFormat:IS_IPAD?@"%@/header_pdf_book.html":@"%@/header_pdf_book_iphone.html",path] encoding:NSUTF8StringEncoding error:nil];
    headerPDF = [NSString stringWithFormat:headerPDF,fontType];
    NSString *footerPDF=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/footer_pdf_book.html",path] encoding:NSUTF8StringEncoding error: nil];
    NSString *stringaPDFContent = [NSString stringWithFormat:@"%@%@%@%@",headerPDF,[self titleForItem:item],self.web_text,footerPDF];
    NSLog(@"stringaPDFContent:\n\n\n%@\n\n\n\n\n\n\n",stringaPDFContent);
    [self.htmlPdfKit saveHtmlAsPdf:stringaPDFContent
                         toFile:[item pdf_book_url]];
}

-(void)loadPdfMessa{
    Messa *m = currentMessa;
    NSString *messa_name = m.messa_name;
    if(messa_name==nil) messa_name = m.day.title;
    NSLog(@"PdfBook presente:%@?%@",[m pdfBookNameFile],[self fileExistsAtPath:[m pdfBookNameFile]]?@"Y":@"N");
    
    if([self fileExistsAtPath:[m pdfBookNameFile]]){
        NSLog(@"PdfBook presente");
        [Home mostraPdfBookwithUrl:[m pdfBookNameFile]];
        return;
    }
    
    // NSLog(@"self.isLand?%i frame:%@",self.isLand,NSStringFromCGRect(self.view.frame));
    self.htmlPdfKit = [[BNHtmlPdfKit alloc] initWithPageSize:IS_IPAD?BNPageSizeA4:BNPageSizeIphone];
    self.htmlPdfKit.delegate = self;
    
    [self.htmlPdfKit saveHtmlAsPdf:[m htmlPdfBookWithOmelia:YES]
                            toFile:[m pdfBookNameFile]];
}


-(void)delayLoadString:(NSString*)s{
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    [self.webContent loadHTMLString:s baseURL:baseURL];
    if([Home isHomeVisible]){
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self nascondiHome];
            // NSLog(@"nascondiHome(MAIN OPERATION)");
        }];
    }
}

//==================================
// PDF DELEGATE
//==================================
#pragma mark - PDF DELEGATE -
- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didSavePdfFile:(NSString *)file{
    NSLog(@"didSavePdfFile:%@",file);
        [Home mostraPdfBookwithUrl:file];
}

- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didFailWithError:(NSError *)error{
    NSLog(@"didFailWithError:%@",[error description]);
    
}


+(void)reloadItem{
    if(currentMessa!=nil) [Home mostraMessa:currentMessa editMode:NO];
    else [Home mostraItem:currentItem];
}

-(void)nascondiHome{
    CGRect rect = self.HOME.frame;
    rect.origin.y -= self.HOME.frame.size.height + 20;
    
    [UIView beginAnimations:@"NascondiHome" context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineNascondiHome)];
    self.HOME.frame = rect;
    [UIView commitAnimations];
}

-(void)fineNascondiHome{
    [HOME setHidden:YES];
    if(!self.isIos7orUpper) [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];

}

+(void)mostraHome{
    // NSLog(@"mostra Home");
    Home *h = [Home sharedInstance];
    if([Home isHomeVisible] || currentMessa!=nil || h.messa_selettore_ctrl != nil || h.messa_editor_ctrl) return;
    //Ricarico Menu
    [(UINavigationController*)[[DDMenuController sharedInstance] leftViewController] popToRootViewControllerAnimated:YES];
    [h resetAudio];
    if(!h.isIos7orUpper) [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [h.HOME setHidden:NO];
    CGRect rect = h.HOME.frame;
    rect.origin.y = HOME_Y;
    [UIView beginAnimations:@"MostraHome" context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineMostraHome)];
    h.HOME.frame = rect;
    [UIView commitAnimations];
}


    
-(void)fineMostraHome{
    // NSLog(@"fineMostraHome");
}
    
+(BOOL)isHomeVisible{
    Home *h = [Home sharedInstance];
    BOOL risp = (h.HOME.frame.origin.y > -40);
    // NSLog(@"isHomeVisible:%@",risp?@"YES":@"NO");
    return risp;
}
    
+(BOOL)isContentVisible{
    Home *h = [Home sharedInstance];
    BOOL risp = (h.HOME.frame.origin.y < -40);
    // NSLog(@"isContentVisible[%i]:h.HOME:%@",risp,NSStringFromCGRect(h.HOME.frame));
    return risp;
}

#pragma mark - iPAD FULLSCREEN
//============================
// iPAD FULLSCREEN
//============================
- (IBAction)homeFULLSCREEN:(UIButton *)sender
{
    if(self.NAVBAR.frame.origin.y<-20) [self MostraNavBar:self];
    else [self SuNavBar:self];
}

#pragma mark - WEB-DELEGATE
//=========================
// WEB-DELEGATE
//=========================
- (BOOL)webView:(UIWebView *)w shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    // NSString *requestString = [[[request URL] absoluteString] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    // NSLog(@"shouldStartLoadWithRequest:%@",requestString);
    
    if ( navigationType == UIWebViewNavigationTypeLinkClicked ) {
        if ([[[request URL] absoluteString] rangeOfString:@"#"].location != NSNotFound) return YES;
        if ([[[request URL] absoluteString] rangeOfString:@"donazione"].location != NSNotFound)
            {
                [[UIApplication sharedApplication] openURL:[request URL]];
                return NO;
            }
        WebBrowser *browser = [WebBrowser new];
        [browser setUrl:[request URL]];
        [self presentViewController:browser animated:YES completion:nil];
        return NO;
    }
    
    
    return YES;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if([scrollView isEqual:webContent.scrollView]) {
        // NSLog(@"scrollViewWillBeginDragging->resettoTAB->Webview");
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if([scrollView isEqual:webContent.scrollView]) {
        // NSLog(@"scrollViewDidScroll->resettoTAB->Webview");
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    // NSLog(@"Home:webViewDidFinishLoad");
    if([[day linguaGiorno] alignment_right])
    {
        [webView stringByEvaluatingJavaScriptFromString:@"	$('#body').attr('dir','rtl');"];
    }

    [webContent.scrollView setContentSize: CGSizeMake(webContent.frame.size.width-5, webContent.scrollView.contentSize.height)];
    altezzaWebView = [[webView stringByEvaluatingJavaScriptFromString:@"$(document).height();"] integerValue];
    //AUTO SCROLL
    [self inizializzaVelocitaScrolling];
    
    if(gotoMessa!=nil){
            [Home messaGoToIndex:gotoMessa];
            gotoMessa = nil;
    }

}

#pragma mark - POPOVER delegate
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    // NSLog(@"popoverControllerDidDismissPopover!");
    if(popoverController == lista_popover)
    {
        // NSLog(@"NAVBAR 119:%@",[NAVBAR viewWithTag:119]);
        [(UIButton*)[NAVBAR viewWithTag:119] setSelected:NO];
        [(UIButton*)[HOME viewWithTag:120] setSelected:NO];
        [self removeChildCTRL:listaCTRL];
        listaCTRL = nil;
    }
    if(popoverController == event_popover)
    {
        [self modificaEventFlagMain];
        [self removeChildCTRL:eventCTRL];
        eventCTRL = nil;
    }
    
    if(popoverController == messa_popover)
    {
        [self removeChildCTRL:messa_ctrl];
        messa_ctrl = nil;
    }
    
    if(popoverController == self.mySpiritualBook_popover)
    {
        [self.mySpiritual_btn setSelected:NO];
        [self.mySpiritual_btn2 setSelected:NO];
    }

    
    popoverController = nil;
}

-(void)popoverController:(UIPopoverController *)popoverController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing *)view{
    if(popoverController == self.mySpiritualBook_popover){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MOSSO" object:nil];
        }
}

//=========================
// GESTIONE VOICE
//=========================
#pragma mark - GESTIONE VOICE AUDIO SPEACH
NSMutableArray *audioArray;
int audioIndex = 0;


-(NSString*)checkVoice:(NSString*)sigla{
    // NSLog(@"checkVoice:%@",sigla);
    NSString *risposta=@"";

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000

    NSArray *casiparticolari = @[@"la",@"ra",@"vt"];
    for (NSString*row in casiparticolari) {
        if([sigla isEqualToString:row])
            sigla = @"it";
    }
    
    //if([sigla isEqualToString:@"en"]) return @"en-GB";
    //if([sigla isEqualToString:@"es"]) return @"en-GB";
    
    NSArray *a = [AVSpeechSynthesisVoice speechVoices];
    for (AVSpeechSynthesisVoice *v in a)
    {
        if ([v.language rangeOfString:sigla].location != NSNotFound) {
            // NSLog(@"found voice ->%@",v.language);
            risposta = v.language;
        }
    }
#endif

    //if([risposta isEqualToString:@""]) return [self checkVoice:NSLocalizedString(@"checklingua", nil)];
    return risposta;
}

-(void)resetAudio{
    //AUTO SCROLL E AUDIO RESET
    [self.autoscroll_PLAY_btn setSelected:NO];
    [self.audio_btn setSelected:NO];
    [self.synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    [self.synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    self.synthesizer = nil;
    self.synthesizer = [AVSpeechSynthesizer new];
    speechPaused = NO;
    self.synthesizer.delegate = self;
    audioIndex = 0;
    audioArray = nil;
}

- (IBAction)playPauseButtonPressed:(UIButton *)sender {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    
    if(!audioArray)
        {
        NSString *text = [webContent stringByEvaluatingJavaScriptFromString:@"textToSpeach();"];
        text = [text stringByReplacingOccurrencesOfString:@"," withString:@"."];
        text = [text stringByReplacingOccurrencesOfString:@"!" withString:@"."];
        text = [text stringByReplacingOccurrencesOfString:@"?" withString:@"."];
        text = [text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"†" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"(" withString:@" "];
        text = [text stringByReplacingOccurrencesOfString:@")" withString:@" "];
        //CROCI UNICODE
        text = [text stringByReplacingOccurrencesOfString:@"\\U00a0" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"\\U2020" withString:@""];
        //CROCE HTML
        text = [text stringByReplacingOccurrencesOfString:@"&dagger;" withString:@""];
        //virgolette << >>
        text = [text stringByReplacingOccurrencesOfString:@"\\U00ab" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"\\U00bb" withString:@""];

        text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
            // NSLog(@"Before:%@",(text.length>40)?[text  substringToIndex:40]:text);
            
        NSArray *arrayJQ = [text componentsSeparatedByString:@"."];
        if(arrayJQ.count>0)NSLog(@"After:%@",[arrayJQ objectAtIndex:0]);
        audioArray = [NSMutableArray new];
        audioIndex = 0;
        for (int i=0; i<arrayJQ.count; i++)
            {
            NSString *s = [arrayJQ objectAtIndex:i];
            BOOL aggiungi = ![s isEqualToString:@""] && ![s isEqualToString:@" "];
            BOOL pausaRipetuta = [audioArray lastObject] && [s isEqualToString:@"=PAUSA="] && ([[audioArray lastObject] isEqualToString:@"=PAUSA="] || [[audioArray lastObject] isEqualToString:@""]);
            // NSLog(@"s[%i-%@]:%@",i,pausaRipetuta?@"SI":@"NO",s);
            if(aggiungi && !pausaRipetuta)
                    {
                        // NSLog(@"add[%@]",s);
                        [audioArray addObject:s];
                    }
            }
        // NSLog(@"audioArray[%lu]:%@",(unsigned long)audioArray.count,day.voice);
        // NSLog(@"audioArray:%@",audioArray);
        }
    
    if(day.voice==nil){
        [self showVoiceS:nil];
        return;
    }
    
    if (speechPaused == NO) {
        //PLAY
        [self.synthesizer continueSpeaking];
        speechPaused = YES;
        [sender setSelected:YES];
        }
    else
        {
        //PAUSE
        speechPaused = NO;
        // NSLog(@"speaching:%@",[self.synthesizer isSpeaking]?@"SPEAKING":@"NO!");
        [self.synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
        [sender setSelected:NO];
        }
    if (self.synthesizer.speaking == NO) {
 
        // NSLog(@"AudioTextMINIMUM:%f default:%f",AVSpeechUtteranceMinimumSpeechRate,AVSpeechUtteranceDefaultSpeechRate);
        NSString *line =  (audioArray.count>audioIndex)?[audioArray objectAtIndex:audioIndex]:@"";
        {
        BOOL pausa = [line isEqualToString:@"=PAUSA="];
        // NSLog(@"line:%@ pausa:%@",line,pausa?@"SI":@"NO");
        AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:pausa?@"":line];
        if(pausa)
            {
            // NSLog(@"****************=================PAUSA LUNGA!!!!");
            utterance.postUtteranceDelay = 0.05;
            }
        //utterance.rate = AVSpeechUtteranceMinimumSpeechRate;
        // NSLog(@"==****== INZIO PARLATO[%f]->%@",AVSpeechUtteranceDefaultSpeechRate,day.voice);
            // NSLog(@"AUDIORATE_INDEX:%@",[self.defaults objectForKey:AUDIORATE_INDEX]);
        utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:day.voice];
        utterance.rate = [[[SystemData audioRate] objectAtIndex:[[self.defaults objectForKey:AUDIORATE_INDEX] integerValue]] floatValue]; //0.22;
        [self.synthesizer speakUtterance:utterance];
        audioIndex++;
        }
    }
    
#endif

}

+(void)audioCTRL{
    // NSLog(@"Home:audioCTRL");
    if (([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)) return;
    Home *h = [Home sharedInstance];
    if(![h.defaults boolForKey:AUDIOLETTURA_BOOL] && (audioArray != nil)) [h resetAudio];
    [h.audio_area setHidden:![h.defaults boolForKey:AUDIOLETTURA_BOOL]];
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000

//DELEGATE
- (void)speechSynthesizer:(AVSpeechSynthesizer *)s didFinishSpeechUtterance:(AVSpeechUtterance *)u{
    if(audioIndex >= audioArray.count)
    {
        [self resetAudio];
    }
    else if(audioIndex < audioArray.count && speechPaused)
    {
        NSString *line =  [audioArray objectAtIndex:audioIndex];
        BOOL pausa = [line isEqualToString:@"=PAUSA="];
        // NSLog(@"lien:%@ pausa:%@",line,pausa?@"SI":@"NO");
        AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:pausa?@"":line];
        if(pausa) {NSLog(@"****************=================PAUSA LUNGA!!!!");utterance.postUtteranceDelay = 0.05;}
        utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:day.voice];
        utterance.rate = [[[SystemData audioRate] objectAtIndex:[[self.defaults objectForKey:AUDIORATE_INDEX] integerValue]] floatValue]; //0.22;
        // NSLog(@"AUDIO_RATE:%f",utterance.rate);
        [self.synthesizer speakUtterance:utterance];
        audioIndex ++;
    }
    

}
- (void)speechSynthesizer:(AVSpeechSynthesizer *)s didPauseSpeechUtterance:(AVSpeechUtterance *)u{
    // NSLog(@"AUDIO:didPauseSpeechUtterance:%@",u);

}
- (void)speechSynthesizer:(AVSpeechSynthesizer *)s willSpeakRangeOfSpeechString:(NSRange)characterRange utterance:(AVSpeechUtterance *)u{
    // NSLog(@"AUDIO:willSpeakRangeOfSpeechString:%@",u);
    
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)s didContinueSpeechUtterance:(AVSpeechUtterance *)u{
    // NSLog(@"AUDIO:didContinueSpeechUtterance:%@",u);

}

#endif


//=========================
// VOICE SELECTOR DELEGATE
//=========================
#pragma mark - VOICE SELECTOR DELEGATE
-(IBAction)showVoiceS:(UIButton*)sender
{
    // NSLog(@"showVoice");
	voice_selector = [[VoiceSelect alloc] initWithDelegate:self andSigla:day.lingua];
	voice_selector.view.alpha = 0;
    
    //blurView = [[FXBlurView alloc] initWithFrame:self.view.frame];
    //[self.blurView setDynamic:NO];
    //[self.blurView setTintColor:[UIColor brownColor]];
    //self.blurView.alpha = 0;
    //[self.view addSubview:self.blurView];
    
    
    [self.view addSubview:voice_selector.view];
    
    
	[UIView beginAnimations:@"AppareVoice" context:NULL];
    [UIView setAnimationDuration:0.5];
	voice_selector.view.alpha = 1;
    [UIView commitAnimations];
    
}

-(void)removeVoiceSelect:(NSString*)voce_scelta{
    // NSLog(@"FineVoice_selector");
    if(![voce_scelta isEqualToString:@""])
        {
        day.voice = voce_scelta;
        [day save];
        }
    
    [UIView beginAnimations:@"Voice_disappear" context:NULL];
    [UIView setAnimationDuration:0.4];
    voice_selector.view.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineVoice)];
    [UIView commitAnimations];
}

-(void)fineVoice{
    [voice_selector.view removeFromSuperview];
    voice_selector =  nil;
    self.blurView = nil;
    if(day.voice)[self playPauseButtonPressed:audio_btn];
}


#pragma mark - AUTOSCROLL
//=========================
// AUTOSCROLL
//=========================
- (void)startScrolling{
    if(CHANGEPAGE){
        [self performSelector:@selector(resetScroll) withObject:nil afterDelay:TIMER_HOME];
        CHANGEPAGE = NO;
    }
    if(StopScroll){
        //STOP HANDLER
        [self resetScroll];
        return;
        }
    if(PausaScroll) {
        //PAUSA NON FARE NIENTE
        return;
    }
    
    //step da eseguire
    PlayScroll = YES;
    altezzaStep = webContent.scrollView.contentOffset.y;
    altezzaStep += offsetScroller;
    //NSLog(@"webContent.scrollView.contentOffset.y:%li",altezzaStep);

    if (webContent.scrollView.contentOffset.y>=altezzaWebView)
    {
        //FINITO TUTTO IL TESTO
        [self resetScroll];
        return;
    }
    
    //=====================
    //AUTOSCROLL->ANIMATION
    //=====================
    [UIView beginAnimations:@"Scrolla" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:StopScroll?@selector(resetScroll):@selector(startScrolling)];
    [UIView setAnimationDuration:TIMER_HOME];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    
    [webContent.scrollView setContentOffset:CGPointMake(0, StopScroll?0:altezzaStep)];
    if(StopScroll) StopScroll = NO;
    [UIView commitAnimations];
    
}

-(void)inizializzaVelocitaScrolling{
    NSArray *a = [NSArray arrayWithArray:[SystemData scrollValue]];
    offsetScroller = [[a objectAtIndex:[self.defaults integerForKey:AUTOSCROLL_INDEX]] integerValue];
    PlayScroll = NO;
    PausaScroll = NO;
    StopScroll = YES;
    altezzaStep = 0;
    [webContent.scrollView setContentOffset:CGPointZero animated:YES];
    [autoscroll_PLAY_btn setSelected:NO];
}

-(void)resetScroll{
    PlayScroll = NO;
    PausaScroll = NO;
    StopScroll = YES;
    altezzaStep = 0;
    [webContent.scrollView setContentOffset:CGPointZero animated:YES];
    [autoscroll_PLAY_btn setSelected:NO];
}

+(void)setScrollSpeed:(NSInteger)i{
    offsetScroller = i;
}


-(IBAction)startScroller:(UIButton*)sender{
    [sender setSelected:!PlayScroll];
    PlayScroll = !PlayScroll;
    PausaScroll = !PlayScroll;
    StopScroll = NO;
    if(PlayScroll) [self startScrolling];
}

-(IBAction)stopScroller:(UIButton*)sender{
    [self resetScroll];
}

+(void)scrollerCTRL{
    // NSLog(@"Home:scrollerCTRL");
    Home *h = [Home sharedInstance];
    [h inizializzaVelocitaScrolling];
    [h.autoscroller_area setHidden:![h.defaults boolForKey:AUTOSCROLL_BOOL]];
    [h controllaPosizioneAutoscrollArea];
    // NSLog(@"Home:fine_scrollerCTRL");
}

#pragma mark -
#pragma mark SCROLLER
//=========================
// SCROLLER SANTO
//=========================

-(void)buildSaintScroller
{
    [self.scroller setShowsHorizontalScrollIndicator:NO];
    [self.scroller setShowsVerticalScrollIndicator:NO];
    if(self.isIpad)
    {
        [self buildSaintScrollerIpad];
        return;
    }
    NSMutableArray *a = day.santo_array;
    int count = (int)[a count];
    // NSLog(@"buildSaintScroller[%i]",count);
    
    CGRect puntoDiPartenza = CGRectMake(0, 0, 250, 100);
    
    for(UIView* subview in [self.scroller subviews]) //per cancellare tutte le superviste (reset)
        [subview removeFromSuperview];
     [SantoView setHidden:(count==0)];
    
    for (SantoObject *row in a) {
        if(row.id_santo==0) [a removeObject:row];
    }
    
    count = (int)[a count];
    [SantoView setHidden:(count==0)];
    if(count==0) return;

    
    for (int i = 0; i < count; i++) {
        SantoObject *row = [a objectAtIndex:i];
        if(row.id_santo!=0)
        {
        CGRect frame;
        frame.origin.x = self.scroller.frame.size.width*i;
        frame.origin.y = 0;
        frame.size = CGSizeMake(self.scroller.frame.size.width, self.scroller.frame.size.height);
        
        UIView *item = [[UIView alloc] initWithFrame:frame];
        [item setTag:i+1000];
        //Sfondo Santo card
        //[item setBackgroundColor:[UIColor clearColor]];
        [item setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.06]];
        
        [item.layer setCornerRadius:5.f];
        [item.layer setMasksToBounds:YES];
            
//        UIView *blur = [[UIView alloc] initWithFrame:item.bounds];
//        [self blurView:blur withEffectStyle:UIBlurEffectStyleExtraLight];
//        [blur setAlpha:0.3];
//        [item addSubview:blur];
            
        //NSLog(@"item[%i] frame:%@",i,NSStringFromCGRect(item.frame));
        
        UIButton *ss = [[UIButton alloc] initWithFrame: CGRectMake(10, 8, 55, 66)];
            [ss.imageView setContentMode:UIViewContentModeScaleAspectFit];
            [ss setImage:row.image forState:UIControlStateNormal];
            [ss setBackgroundColor:[UIColor clearColor]];
            [ss setTag:i];
            [ss setTitle:@"" forState:UIControlStateNormal];
            [ss setUserInteractionEnabled:NO];
            [item addSubview:ss];
    
        UILabel *label_titolo = [[UILabel alloc] initWithFrame:CGRectMake(80, 5, 160, 60)];
        [label_titolo setBackgroundColor:[UIColor clearColor]];
        [label_titolo setTextColor:UIColorFromRGB(0X483627)];
        [label_titolo setNumberOfLines:2];
        [label_titolo setTextAlignment:NSTextAlignmentCenter];
            [label_titolo setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:(IS_IPHONE_6 || IS_IPHONE_6_PLUS)?18:17]];
        [label_titolo setText:[row.name uppercaseString]];
        [label_titolo setMinimumScaleFactor:0.65];
        [label_titolo setAdjustsFontSizeToFitWidth:YES];
        [item addSubview:label_titolo];
        
        UILabel *label_memory = [[UILabel alloc] initWithFrame:CGRectMake(80, 48, 160, 25)];
        [label_memory setBackgroundColor:[UIColor clearColor]];
        [label_memory setTextColor:UIColorFromRGB(0X483627)];
        [label_memory setNumberOfLines:1];
        [label_memory setTextAlignment:NSTextAlignmentCenter];
            UIFont *memory_font = [UIFont fontWithName:(IS_IPHONE_6 || IS_IPHONE_6_PLUS)?FONT_HELVETICANEUE_REGULAR:FONT_HELVETICANEUE_LIGHT size:(IS_IPHONE_6 || IS_IPHONE_6_PLUS)?15:14];
        [label_memory setFont:memory_font];
        [label_memory setText:row.type];
        [item addSubview:label_memory];
        
        UIView *s2 = [[UIView alloc] initWithFrame:item.bounds];
        [s2 setBackgroundColor:[UIColor clearColor]];
        [s2 setTag:i];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToSanto:)];
        [s2 addGestureRecognizer:tapGestureRecognizer];
//        if(i%2==0){
//                [s2 setBackgroundColor:[UIColor redColor]];
//            }
//        else{
//            [s2 setBackgroundColor:[UIColor greenColor]];
//        }
        [item addSubview:s2];

        
        [self.scroller addSubview:item];
        }
    }
    
    [pager setNumberOfPages:count];
    [pager setHidden:(count==1)];
    if(count==1){
        [pager setHidden:(count==1)];
    }
    self.scroller.pagingEnabled = YES;
    self.scroller.contentSize = CGSizeMake(self.scroller.frame.size.width * count, self.scroller.frame.size.height);
    // NSLog(@"self.scroller.contentSize:%@",NSStringFromCGSize(self.scroller.contentSize));
    [self.scroller setContentOffset:CGPointZero animated:NO];
    
    [self.scroller scrollRectToVisible:puntoDiPartenza animated:NO];
    [self.scroller setScrollEnabled:(count!=1)];
    
}

#pragma mark -
#pragma mark SCROLLER IPAD
//=========================
// SCROLLER SANTO IPAD
//=========================

-(void)buildSaintScrollerIpad
{
    NSMutableArray *a = day.santo_array;
    int count = (int)[a count];
    // NSLog(@"buildSaintScroller[%i]:%@",count,day.santo_array);
    
    CGRect puntoDiPartenza = CGRectMake(0, 0, 450, 190);
    
    for(UIView* subview in [self.scroller subviews]) //per cancellare tutte le superviste (reset)
        [subview removeFromSuperview];
    [SantoView setHidden:(count==0)];
    
    for (SantoObject *row in a) {
        if(row.id_santo==0) [a removeObject:row];
    }
    
    count = (int)[a count];
    [SantoView setHidden:(count==0)];
    if(count==0) return;
    
    
    for (int i = 0; i < count; i++) {
        SantoObject *row = [a objectAtIndex:i];
        if(row.id_santo!=0)
        {
            CGRect frame;
            frame.origin.x = self.scroller.frame.size.width*i;
            frame.origin.y = 0;
            frame.size = CGSizeMake(self.scroller.frame.size.width, self.scroller.frame.size.height);
            UIView *item = [[UIView alloc] initWithFrame:frame];
            [item setTag:i+1000];
            
            UIView *back_item = [[UIView alloc] initWithFrame:CGRectMake(5, 5, frame.size.width-10, frame.size.height-10)];
            [back_item setTag:i+1000];
            [back_item setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.12]];
            [back_item.layer setCornerRadius:10];
            [back_item.layer setMasksToBounds:YES];
            [back_item setTag:i];
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToSanto:)];
            [back_item addGestureRecognizer:tapGestureRecognizer];

            
            UIButton *ss = [[UIButton alloc] initWithFrame: CGRectMake(5, 0, 150, back_item.frame.size.height)];
            [ss.imageView setContentMode:UIViewContentModeScaleAspectFit];
            [ss setImage:row.image forState:UIControlStateNormal];
            [ss setBackgroundColor:[UIColor clearColor]];
            [ss setUserInteractionEnabled:NO];
            [ss setTitle:@"" forState:UIControlStateNormal];
            // NSLog(@"ss[%@] image:%@",ss,row.image);

            
            UILabel *label_titolo = [[UILabel alloc] initWithFrame:CGRectMake(160, 0, back_item.frame.size.width-165, back_item.frame.size.height-10)];
            [label_titolo setBackgroundColor:[UIColor clearColor]];
            [label_titolo setTextColor:[UIColorFromRGB(0X422e1e) colorWithAlphaComponent:0.9]];
            [label_titolo.layer setShadowColor:[[UIColorFromRGB(0XFFFFFF) colorWithAlphaComponent:1] CGColor]];
            [label_titolo.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
            [label_titolo.layer setShadowOpacity:0.4];
            [label_titolo.layer setShadowRadius:3.0];
            [label_titolo setNumberOfLines:3];
            [label_titolo setTextAlignment:NSTextAlignmentCenter];
            [label_titolo setFont:[UIFont fontWithName:FONT_HELVETICANEUE_BOLD size:23]];
            [label_titolo setText:[row.name uppercaseString]];
            [label_titolo setMinimumScaleFactor:0.4];
            [label_titolo setAdjustsFontSizeToFitWidth:YES];
            [label_titolo setUserInteractionEnabled:NO];

            // NSLog(@"label_titolo[%@] name:%@",label_titolo,row.name);
            
            UILabel *label_memory = [[UILabel alloc] initWithFrame:CGRectMake(160,back_item.frame.size.height-35 , back_item.frame.size.width-165, 25)];
            [label_memory.layer setCornerRadius:5];
            [label_memory.layer setMasksToBounds:YES];
            //[label_memory setBackgroundColor:[UIColorFromRGB(0X725945) colorWithAlphaComponent:0.45]];
            [label_memory setTextColor:[UIColorFromRGB(0XFFFFFF) colorWithAlphaComponent:0.55]];
            [label_memory setNumberOfLines:1];
            [label_memory setTextAlignment:NSTextAlignmentCenter];
            [label_memory setMinimumScaleFactor:0.5];
            [label_memory setAdjustsFontSizeToFitWidth:YES];
            [label_memory setFont:[UIFont fontWithName:FONT_HELVETICANEUE_BOLD size:16]];
            [label_memory setText:[row.type uppercaseString]];
            [label_memory setUserInteractionEnabled:NO];
            
            
            
            /*
            UIView *s2 = [[UIView alloc] initWithFrame:frame];
            [s2 setBackgroundColor:[[UIColor redColor] colorWithAlphaComponent:0.5]];
            [s2 setTag:i];
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GoToSanto:)];
            [s2 addGestureRecognizer:tapGestureRecognizer];
            // NSLog(@"ROW frame[%@]:%@",NSStringFromCGRect(s2.frame),row);
*/
            
            [back_item addSubview:label_memory];
            [back_item addSubview:ss];
            [back_item addSubview:label_titolo];
            //[back_item addSubview:s2];

            [item addSubview:back_item];

            
            [self.scroller addSubview:item];
        }
    }
    
    [self.pager setNumberOfPages:count];
    [self.pager setHidden:(count<=1)];
    // NSLog(@"pager:%i count:%i",(int)pager.numberOfPages,count);
    self.scroller.pagingEnabled = YES;
    self.scroller.contentSize = CGSizeMake(self.scroller.frame.size.width * count, self.scroller.frame.size.height);
    // NSLog(@"self.scroller.contentSize:%@",NSStringFromCGSize(self.scroller.contentSize));
    [self.scroller setContentOffset:CGPointZero animated:NO];
    
    [self.scroller scrollRectToVisible:puntoDiPartenza animated:NO];
    [self.scroller setScrollEnabled:(count!=1)];
    
}


#pragma mark - SANTO SCROLLView delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView == scroller)
        {
            CGFloat pageWidth = self.scroller.frame.size.width;
            int currentPage = floor((self.scroller.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            self.pager.currentPage = currentPage;
    
            CGRect frame;
            frame.origin.x = self.scroller.frame.size.width * self.pager.currentPage;
            frame.origin.y = 0;
            frame.size = self.scroller.frame.size;
            [self.scroller scrollRectToVisible:frame animated:YES];
    
            // NSLog(@"Home Scroller: scrollViewDidEndDecelerating:%@",NSStringFromCGRect(frame));
    }
}

- (IBAction)changePage:(UIPageControl *)aPageControl
{
    // NSLog(@"Pager->changePage:%li",(long)self.pager.currentPage);
    CGRect frame;
    frame.origin.x = self.scroller.frame.size.width * self.pager.currentPage;
    frame.origin.y = 0;
    frame.size = self.scroller.frame.size;
    [self.scroller scrollRectToVisible:frame animated:YES];
}


//=========================
// ACTION
//=========================
#pragma mark - ACTION
-(IBAction)premutoMenu:(UIButton*)sender{
    if(self.view.frame.origin.x!=0)[DDMenuController closeMenuWithCompletion:nil];
    else [DDMenuController showLeftController:^{
        if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
        else
        {
            // iOS 6
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
        }}];
}

-(IBAction)premutoSettings:(UIButton*)sender{
    // NSLog(@"premutoSettings:%f",self.view.frame.origin.x);
    if(self.view.frame.origin.x!=0){
        [DDMenuController closeMenuWithCompletion:nil];
    }
    else{
        [DDMenuController showRightController:^{
                                    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
                                    else
                                    {
                                        // iOS 6
                                        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
                                    }}];
    }
}

-(IBAction)premutoListaDownload:(UIButton*)sender{
    if(self.isIpad) {[self showLISTAPopOver:sender]; return;}

    self.listaCTRL = [[Lista alloc] initWithDelegate:self];
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    [self.listaCTRL setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self presentViewController:self.listaCTRL
                       animated:YES
                     completion:^{
    }];
}

#pragma mark lista

- (IBAction)showLISTAPopOver:(UIButton*)sender {
	CGRect qui = sender.frame;
    [self closeMenuIfOpen];
    [self.event_popover dismissPopoverAnimated:(self.event_popover != nil)];
    [self.messa_popover dismissPopoverAnimated:(self.messa_popover != nil)];
    [self.mySpiritualBook_popover dismissPopoverAnimated:(self.mySpiritualBook_popover != nil)];

    
	if (self.lista_popover.popoverVisible)
        [lista_popover dismissPopoverAnimated:YES];
	else
    {
        self.listaCTRL = [[Lista alloc] initWithDelegate:self];;
        
        lista_popover = [[UIPopoverController alloc] initWithContentViewController:listaCTRL];
        [lista_popover setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"PaperHD.png"]]];
        CGSize size = CGSizeMake(listaCTRL.view.frame.size.width, (self.isLand)?665:910);
        [lista_popover setPopoverContentSize:size animated:NO];
        [listaCTRL.view setFrame:CGRectMake(0, 0, size.width, size.height)];
        //[self addChildCTRL:listaCTRL];
        lista_popover.contentViewController.view.autoresizingMask = UIViewAutoresizingNone;
        lista_popover.delegate = self;
        [lista_popover presentPopoverFromRect:qui inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
	
    [sender setSelected:YES];
}

+(void)dismiss:(UIViewController*)v{
    // NSLog(@"Home dismiss:%@",v);
    Home *h = [Home sharedInstance];
    if(v.parentViewController == h){
        [h removeChildCTRL:v];
    }
    if([v isKindOfClass:[Lista class]]) h.listaCTRL = nil;
    if([v isKindOfClass:[EventoFlag class]]) h.eventCTRL = nil;
    if([h respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    else [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

-(void)dismissLista{
    // NSLog(@"dismissLista");
    [lista_popover dismissPopoverAnimated:YES];
}


-(IBAction)premutoEventFlag:(UIButton*)sender{
    if(self.isIpad) {[self showEventFLAG:sender]; return;}
    self.eventCTRL = [EventoFlag new];
    [self.eventCTRL setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
    
    [self presentViewController:self.eventCTRL animated:YES completion:nil];
    
}

- (IBAction)showEventFLAG:(UIButton*)sender {
    [self closeMenuIfOpen];

    BOOL isFull = self.NAVBAR.frame.origin.y<-20;
	CGRect qui = isFull?eventFLAG2.frame:eventFLAG.frame;
    qui.origin.y -= isFull?10.2:-17;
    qui.origin.x += isFull?8:59;
    if(isFull) qui.size.width +=2;
    
    // NSLog(@"qui:%@ isFull:%i",NSStringFromCGRect(qui),isFull);
	[self.lista_popover dismissPopoverAnimated:(self.lista_popover != nil)];
    [self.messa_popover dismissPopoverAnimated:(self.messa_popover != nil)];
    [self.mySpiritualBook_popover dismissPopoverAnimated:(self.mySpiritualBook_popover != nil)];

    
	if (self.event_popover.popoverVisible)
        [event_popover dismissPopoverAnimated:YES];
	else
    {
        self.eventCTRL = [[EventoFlag alloc] initWithDelegate:self];
        
        event_popover = [[UIPopoverController alloc] initWithContentViewController:eventCTRL];
        if(self.isIos7orUpper)[event_popover setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"PaperHD.png"]]];
        CGSize size;
        size = CGSizeMake(eventCTRL.view.frame.size.width, (self.isLand)?665:910);
        [event_popover setPopoverContentSize:size animated:NO];
        [eventCTRL.view setFrame:CGRectMake(0, 0, size.width, size.height)];
        //[self addChildCTRL:eventCTRL];
        event_popover.contentViewController.view.autoresizingMask = UIViewAutoresizingNone;
        event_popover.delegate = self;
        [event_popover presentPopoverFromRect:qui inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
	
    [sender setSelected:YES];
}

-(void)dismissEvent{
    [event_popover dismissPopoverAnimated:YES];
}


-(void)passaAllaListaDownload{
    [self performSelector:@selector(dismissEvent) withObject:nil afterDelay:0.9];
    CGRect frame = oggi_btn.frame;
    frame.origin.x += 65;
    frame.origin.y -= 5;
    
    [self performSelector:@selector(showLISTAPopOver:) withObject:[[UIButton alloc] initWithFrame:frame] afterDelay:1.5];
}



+(void)passaAllaListaDownload{
    Home *h = [Home sharedInstance];
    [h performSelector:@selector(premutoListaDownload:) withObject:nil afterDelay:1];
}

- (IBAction)GoToCustodia:(UIButton*)sender
{
	NSLog(@"GoToCustodia");
    if([DDMenuController isMenuOpen])
        {
            [DDMenuController closeMenuWithCompletion:^{[self gotoCustodia2];}];
        }
    else [self gotoCustodia2];
}


-(void)gotoCustodia2{
    custodia = [CustodiaTS new];
    if(self.isIpad)[custodia.view setCenter:self.view.center];
    
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    
    [custodia setDelegate:self];
    
    [self.custodia.view setBackgroundColor:[UIColor clearColor]];
    
    [self blurView:self.custodia.view
   withEffectStyle:UIBlurEffectStyleDark];

    
    [self.custodia.view setCenter:(CGPoint){self.view.frame.size.width/2.0,- (self.custodia.view.frame.size.height/2.0)}];
    [self.view addSubview:custodia.view];
    
    
    animation_block = ^{
        [self.custodia.view setCenter:(CGPoint){self.view.frame.size.width/2.0,self.view.frame.size.height/2.0}];
    };
    
    completition_block = ^(BOOL finished) {
        //NSLog(@"FineAppareCustodia");
    };
    
    [UIView animateWithDuration:IS_IPAD?0.7:0.6
                          delay:0
         usingSpringWithDamping:0.65
          initialSpringVelocity:IS_IPAD?3:3
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:animation_block
                     completion:completition_block];

}

- (IBAction)GoToTerraSancta:(UIButton*)sender
{
    if([DDMenuController isMenuOpen])
    {
        [DDMenuController closeMenuWithCompletion:^{[self gotoTerrasanta2];}];
    }
    
    else [self gotoTerrasanta2];
    
}

-(void)gotoTerrasanta2{
	NSLog(@"GoToTerraSancta");
	terraSancta = [TerraSancta new];
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    [terraSancta setDelegate:self];
    
    [self.terraSancta.view setBackgroundColor:[UIColor clearColor]];
    
    [self blurView:self.terraSancta.view
   withEffectStyle:UIBlurEffectStyleDark];
    
    
    [self.terraSancta.view setCenter:(CGPoint){self.view.frame.size.width/2.0,- (self.terraSancta.view.frame.size.height/2.0)}];
    [self.view addSubview:terraSancta.view];
    
    
    animation_block = ^{
        [self.terraSancta.view setCenter:(CGPoint){self.view.frame.size.width/2.0,self.view.frame.size.height/2.0}];
    };
    
    completition_block = ^(BOOL finished) {
        //NSLog(@"FineAppareTS");
    };
    
    [UIView animateWithDuration:IS_IPAD?0.7:0.6
                          delay:0
         usingSpringWithDamping:0.65
          initialSpringVelocity:IS_IPAD?3:3
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:animation_block
                     completion:completition_block];
}

//funzione solo per iPAD blur
-(void)addBlur:(BaseController*)sender{
    fake_home_blur = [[UIImageView alloc] initWithFrame:self.view.frame];
    [fake_home_blur setImage:[UIImage imageNamed:self.isPort?@"blur_ipad_port.png":@"blur_ipad_land.png"]];
    [fake_home_blur setAlpha:0];
    [self.view addSubview:fake_home_blur];
    
    UIButton *removeBtn = [[UIButton alloc] initWithFrame:self.fake_home_blur.frame];
    [removeBtn setBackgroundColor:[UIColor clearColor]];
    //TS
    if([sender isKindOfClass:[TerraSancta class]])
    {
        // NSLog(@"TS:%@ - %@",sender, [sender class]);
    [removeBtn addTarget:self action:@selector(removeTS) forControlEvents:UIControlEventTouchUpInside];
    }
    //INFO
    else if([sender isKindOfClass:[Info class]])
    {
        // NSLog(@"INFO:%@ - %@",sender, [sender class]);
        [removeBtn addTarget:self action:@selector(removeInfo) forControlEvents:UIControlEventTouchUpInside];
    }
    //CUSTODIA
    else if([sender isKindOfClass:[CustodiaTS class]])
    {
        // NSLog(@"CustodiaTS:%@ - %@",sender, [sender class]);
        [removeBtn addTarget:self action:@selector(removeCustodia) forControlEvents:UIControlEventTouchUpInside];
    }
    //LINGUA
    else if([sender isKindOfClass:[Lingua class]])
    {
        // NSLog(@"Lingua:%@ - %@",sender, [sender class]);
        [removeBtn addTarget:self action:@selector(removeLingua) forControlEvents:UIControlEventTouchUpInside];
    }
    //SANTO
    else if([sender isKindOfClass:[SantoViewCTRL class]])
    {
        // NSLog(@"Notifica:%@ - %@",sender, [sender class]);
        [removeBtn addTarget:self action:@selector(removeSantoViewCTRL) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // NSLog(@"button:%@",removeBtn);
    [fake_home_blur setUserInteractionEnabled:YES];
    [fake_home_blur addSubview:removeBtn];
    [self.view addSubview:fake_home_blur];

}

- (IBAction)GoToSanto:(id)sender
{
    [self closeMenuIfOpen];

    int santoIndex = 0;//inizializzo santIndex
    
    if([sender isKindOfClass:[UITapGestureRecognizer class]]){
        UITapGestureRecognizer *tap = (UITapGestureRecognizer*)sender;
        santoIndex = (int)tap.view.tag;
    }
    else if([sender isKindOfClass:[NSString class]])
        santoIndex = [(NSString*)sender intValue];

    
	NSLog(@"GoToSanto:%i",santoIndex);
    NSMutableArray *a = day.santo_array;
    if(a.count==0){
        [self mostraNotificaNoSanto];
        return;
    }
     for (SantoObject *row in a) {
        if(row.id_santo==0) [a removeObject:row];
    }
	santoCTRL = [[SantoViewCTRL alloc] initWithDelegate:self andSanto:[a objectAtIndex:santoIndex]];

    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];

    
    [self.santoCTRL.view setCenter:(CGPoint){self.view.frame.size.width/2.0,- (self.santoCTRL.view.frame.size.height/2.0)}];
    [self.view addSubview:santoCTRL.view];
    
    
    animation_block = ^{
        [self.santoCTRL.view setCenter:(CGPoint){self.view.frame.size.width/2.0,self.view.frame.size.height/2.0}];
    };
    
    completition_block = ^(BOOL finished) {
        //NSLog(@"FineAppareNotifica");
        [self.view setUserInteractionEnabled:YES];
        if(self.tabella_notificheCTRL.view){
            [blurView_listaNotifiche removeFromSuperview];
            [tabella_notificheCTRL.view removeFromSuperview];
            blurView_listaNotifiche = nil;
            tabella_notificheCTRL = nil;
            [Home aggiornaBadge];
        }
        
    };
    
    [UIView animateWithDuration:IS_IPAD?0.7:0.6
                          delay:0
         usingSpringWithDamping:0.65
          initialSpringVelocity:IS_IPAD?3:3
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:animation_block
                     completion:completition_block];
}

-(void)removeSantoViewCTRL{
    // NSLog(@"FineSanto");
    santoCTRL.view.alpha = 1;
    
    [UIView beginAnimations:@"ScompareSanto" context:NULL];
    [UIView setAnimationDuration:0.4];
    santoCTRL.view.alpha = 0;
    if(fake_home_blur!=nil)fake_home_blur.alpha = 0;
    if(blurView!=nil)blurView.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineSanto)];
    [UIView commitAnimations];
}

-(void)fineSanto{
    [santoCTRL.view removeFromSuperview];
    [fake_home_blur removeFromSuperview];
    [blurView removeFromSuperview];
    santoCTRL =  nil;
    fake_home_blur = nil;
    blurView = nil;
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    else{// iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];}
}


- (IBAction)GoToInfo:(UIButton*)sender
{
	NSLog(@"GoToInfo");
    if([DDMenuController isMenuOpen])
    {
        [DDMenuController closeMenuWithCompletion:^{[self gotoInfo2];}];
    }
    else [self gotoInfo2];
}

-(void)gotoInfo2{
    infoCTRL =  nil;
	infoCTRL = [[Info alloc] initWithDelegate:self];
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    else{// iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];}

    infoCTRL.view.alpha = 0;

    if(self.isIpad){
        [self addBlur:infoCTRL];
    }
    
    else
    {
    CGRect blur_rect = self.view.frame;
    // NSLog(@"blur:%@ ctrl view:%@",NSStringFromCGRect(self.blurView.frame),NSStringFromCGRect(infoCTRL.view.frame));
    if(!self.isIos7orUpper) { blur_rect.origin.y =-20; blur_rect.size.height +=20; }
    blurView = [[FXBlurView alloc] initWithFrame:blur_rect];
    [self.blurView setDynamic:NO];
    [self.blurView setTintColor:[UIColor brownColor]];
    self.blurView.alpha = 0;
    [self.view addSubview:blurView];

    }
    
 	[self.view addSubview:infoCTRL.view];
    
    [UIView animateWithDuration:0.7
                     animations:^{
                         infoCTRL.view.alpha = 1;
                         self.blurView.alpha = 1;
                         self.fake_home_blur.alpha = 1;
                        }
                     completion:^(BOOL finito){
                                        // NSLog(@"fine show Info");
                                        }];
}

//=========================
// CUSTODIA DELEGATE
//=========================
#pragma mark - CUSTODIA DELEGATE
-(void)showCustodiaBlog
{
    WebBrowser *browser = [WebBrowser new];
    [browser setUrl:[NSURL URLWithString:@"http://www.terrasanctablog.org"]];
    [self presentViewController:browser animated:YES completion:nil];
    [self removeCustodia];
}

- (void)showCustodiaWebSite
{
    WebBrowser *browser = [WebBrowser new];
    [browser setUrl:[NSURL URLWithString:@"http://www.custodia.org"]];
    [self presentViewController:browser animated:YES completion:nil];
    [self removeCustodia];
}

-(void)removeCustodia{
        // NSLog(@"FineCustodia");
        custodia.view.alpha = 1;
        
        [UIView beginAnimations:@"AppareCustodiaTS" context:NULL];
        [UIView setAnimationDuration:0.4];
        custodia.view.alpha = 0;
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(fineCustodia)];
        [UIView commitAnimations];
}

-(void)fineCustodia{
    [custodia.view removeFromSuperview];
    [self.blurView removeFromSuperview];
    [self.fake_home_blur removeFromSuperview];
    custodia =  nil;
    blurView = nil;
    fake_home_blur = nil;
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
 }

//=========================
// TS DELEGATE
//=========================
#pragma mark - TS DELEGATE
-(void)showTSWebSite
{
    WebBrowser *browser = [WebBrowser new];
    [browser setUrl:[NSURL URLWithString:@"http://www.proterrasancta.org/it/aiutaci/"]];
    [self presentViewController:browser animated:YES completion:nil];
    [self removeTS];
}


-(void)removeTS{
    // NSLog(@"FineTs");
    terraSancta.view.alpha = 1;
    
    [UIView beginAnimations:@"RimuoviTS" context:NULL];
    [UIView setAnimationDuration:0.4];
    terraSancta.view.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineTS)];
    [UIView commitAnimations];
}

-(void)fineTS{
    [terraSancta.view removeFromSuperview];
    [self.blurView removeFromSuperview];
    [self.fake_home_blur removeFromSuperview];
    blurView = nil;
    fake_home_blur = nil;
    terraSancta =  nil;
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
 }

//=========================
// LINGUA DELEGATE
//=========================
#pragma mark - LINGUA DELEGATE
-(IBAction)showLingua:(UIButton*)sender
{
    // NSLog(@"showLingua");
    [self closeMenuIfOpen];

	linguaCTRL = [[Lingua alloc] initWithDelegate:self andIsLista:NO];
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    else{// iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];}
    
    if(self.isIpad) [linguaCTRL.view setCenter:self.view.center];
    
    
    if(self.isIpad){
        [self addBlur:linguaCTRL];
    }
    
    else{
        CGRect frame = self.view.frame;
        if(!self.isIos7orUpper) frame.origin.y = -20;
        blurView = [[FXBlurView alloc] initWithFrame:frame];
        [self.blurView setDynamic:NO];
        [self.blurView setTintColor:[UIColor brownColor]];
        self.blurView.alpha = 0;
        [self.view addSubview:blurView];
    }
    
	linguaCTRL.view.alpha = 0;
    
    
	[self.view addSubview:linguaCTRL.view];
	
	[UIView animateWithDuration:0.7
                     animations:^{linguaCTRL.view.alpha = 1; self.blurView.alpha = 1; self.fake_home_blur.alpha = 1;}
                     completion:^(BOOL finito){
                                        // NSLog(@"fine show Custodia");
                                        }];
}

-(void)removeLingua{
    // NSLog(@"FineInfo");
    linguaCTRL.view.alpha = 1;
    
    [UIView beginAnimations:@"Lingua_disappear" context:NULL];
    [UIView setAnimationDuration:0.4];
    linguaCTRL.view.alpha = 0;
    if(blurView!=nil)blurView.alpha = 0;
    if(fake_home_blur!=nil)fake_home_blur.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineLingua)];
    [UIView commitAnimations];
}

-(void)fineLingua{
    [linguaCTRL.view removeFromSuperview];
    [self.blurView removeFromSuperview];
    [self.fake_home_blur removeFromSuperview];
    blurView = nil;
    fake_home_blur = nil;
    linguaCTRL =  nil;
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    else{// iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];}

}

//=========================
// GUIDA DELEGATE
//=========================
#pragma mark - GUIDA DELEGATE
-(void)showInfo_Guida
{
    // NSLog(@"showInfo_Guida");
	guida = [[GuidaPreghiera alloc] initWithDelegate:self];
	guida.view.alpha = 0;
	[self.view addSubview:guida.view];
	
	[UIView beginAnimations:@"Appareguida" context:NULL];
    [UIView setAnimationDuration:0.5];
	guida.view.alpha = 1;
    [UIView commitAnimations];
    
    [self removeInfo];
}

-(void)removeGuidaPreghiera{
    // NSLog(@"FineGuidaPreghiera");
    guida.view.alpha = 1;
    
    [UIView beginAnimations:@"GuidaPreghiera_disappear" context:NULL];
    [UIView setAnimationDuration:0.4];
    guida.view.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineGuidaPreghiera)];
    [UIView commitAnimations];
}

-(void)fineGuidaPreghiera{
    [guida.view removeFromSuperview];
    guida =  nil;
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    else [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

//=========================
// PRINCIPI DELEGATE
//=========================
#pragma mark - PRINCIPI DELEGATE
-(void)showInfo_Principi
{
    // NSLog(@"showInfo_Guida");
	principi = [[Principi alloc] initWithDelegate:self];
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    else [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];

	principi.view.alpha = 0;
	[self.view addSubview:principi.view];
	
	[UIView beginAnimations:@"Appareprincipi" context:NULL];
    [UIView setAnimationDuration:0.5];
	principi.view.alpha = 1;
    [UIView commitAnimations];

    [self removeInfo];
}

-(void)removePrincipi{
    // NSLog(@"FinePrincipi");
    principi.view.alpha = 1;
    
    [UIView beginAnimations:@"principi_disappear" context:NULL];
    [UIView setAnimationDuration:0.4];
    principi.view.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(finePrincipi)];
    [UIView commitAnimations];
}

-(void)finePrincipi{
    [principi.view removeFromSuperview];
    principi =  nil;
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    else [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}


-(void)showInfo_Credits
{
    // NSLog(@"showInfo_Credits");
	creditsCTRL = [[Credits alloc] initWithDelegate:self];
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    else [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
	creditsCTRL.view.alpha = 0;
	[self.view addSubview:creditsCTRL.view];
	
    [self fineInfoLasciandoBlur];
	[UIView beginAnimations:@"AppareCredits" context:NULL];
    [UIView setAnimationDuration:0.5];
	creditsCTRL.view.alpha = 1;
    self.blurView.alpha = 1.0;
    [UIView commitAnimations];

    [creditsCTRL performSelector:@selector(startScrolling) withObject:nil afterDelay:0.4];
}

-(void)fineInfoLasciandoBlur{
    [UIView animateWithDuration:1.4
                     animations:^{infoCTRL.view.alpha = 0;}
                     completion:^(BOOL finished) { [infoCTRL.view removeFromSuperview]; infoCTRL = nil; }];
}

-(void)showInfo_email
{
    AAMFeedbackViewController *vc = [[AAMFeedbackViewController alloc] init];
    vc.toRecipients = [NSArray arrayWithObject:@"ibreviary@me.com"];
    vc.ccRecipients = nil;
    vc.bccRecipients = nil;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
    [self removeInfo];
}

-(void)showInfo_iBreviary
{
    WebBrowser *browser = [WebBrowser new];
    [browser setUrl:[NSURL URLWithString:@"http://www.ibreviary.com"]];
    [self presentViewController:browser animated:YES completion:nil];
    [self removeInfo];
}


-(void)removeInfo{
    // NSLog(@"FineInfo");
    if(creditsCTRL!=nil) {[self removeCredits]; return;}
    infoCTRL.view.alpha = 1;
    [self rotateImage:info_btn withDuration:0.6 onY_axis:YES];
    [UIView animateWithDuration:0.4
                     animations:^{  infoCTRL.view.alpha = 0;
                                    if(self.blurView!=nil)self.blurView.alpha = 0;
                                    if(self.fake_home_blur!=nil)self.fake_home_blur.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                                    [infoCTRL.view removeFromSuperview];
                         if(self.blurView!=nil)[self.blurView removeFromSuperview];
                         if(self.fake_home_blur!=nil)[self.fake_home_blur removeFromSuperview];
                                    // NSLog(@"FINE: infoCTRL:%@",infoCTRL);

                                    self.blurView = nil;
                                    infoCTRL =  nil;
                                    fake_home_blur = nil;
                         if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
                         else [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
                     }];
}

//=========================
// CREDITS DELEGATE
//=========================
#pragma mark - CREDITS DELEGATE
-(void)removeCredits{
    // NSLog(@"FineCredits");
    creditsCTRL.view.alpha = 1;
    [UIView animateWithDuration:0.4
                     animations:^{  creditsCTRL.view.alpha = 0;
                                    if(self.blurView!=nil)self.blurView.alpha = 0;
                                    if(self.fake_home_blur!=nil)self.fake_home_blur.alpha = 0;
                                }
                     completion:^(BOOL finished) {[self fineCre];}];

}

-(void)fineCre{
    [creditsCTRL.view removeFromSuperview];
    if(self.blurView!=nil)[self.blurView removeFromSuperview];
    if(self.fake_home_blur!=nil)[self.fake_home_blur removeFromSuperview];
    creditsCTRL =  nil;
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])[[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
    else [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    self.blurView = nil;
    self.fake_home_blur = nil;

}

#pragma mark - CARICAMENTO OGGETTO DAY
//=========================
// CARICAMENTO OGGETTO DAY
//=========================
-(void)caricaGiornoWithIndex:(NSIndexPath*)indexPath{
    [[MenuViewController sharedInstance] rimuoviMessaMenu];
    [self resetMessa];
    int i = (int)indexPath.row;
    NSArray *listaDay = [Day getListDays];
    if(i>=listaDay.count)
    {
        // NSLog(@"Home:caricaGiornoWithIndex:ERROR: index %i errato",i);
        return;
    }
    NSString *dayPath = [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_DAY,[[Day getListDays] objectAtIndex:i]]];
    if(![self fileExistsAtPath:dayPath])
    {
        // NSLog(@"caricaGiornoWithIndex:ERROR: dayPath sbagliato %@",dayPath);
        return;
    }
    
    if(self.isIpad)
    {
        [(UIButton*)[NAVBAR viewWithTag:119] setSelected:NO];
        [(UIButton*)[HOME viewWithTag:120] setSelected:NO];
    }

    
    NSData *data = [[NSData alloc] initWithContentsOfFile:dayPath];
    day = [Day new];
    day = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    [day salvaGiornoInUso:nil];
    
    [MenuViewController aggiornaVista];

    // NSLog(@"caricaGiornoWithindex[%i]:%@",i ,day);
    
    [Home mostraHome];

    runOnMainQueueWithoutDeadlocking(^{
        [self settaHOME];
    });
}


-(void)caricaGiornoWithDay:(NSString*)dayFileName{
    if([dayFileName isEqualToString:@""]) return;
    [[MenuViewController sharedInstance] rimuoviMessaMenu];
    [self resetMessa];
    day = nil;
    day = [Day new];
    NSArray *components = [dayFileName componentsSeparatedByString:@"_"];
    day = [Day searchForDay:[components objectAtIndex:0] withLang:[self.defaults objectForKey:LINGUA]];
    
    if(!day){
        return;
    }
    
    [day salvaGiornoInUso:nil];
    [MenuViewController aggiornaVista];

    // NSLog(@"caricaGiornoWithDay:%@",day);
    
    [self settaHOME];
    [Home mostraHome];
}

+(void)caricaGiornoWithDayObj:(Day*)d{
    Home *h = [Home sharedInstance];
    [h caricaGiornoWithDayObj:d];
}

-(void)caricaGiornoWithDayObj:(Day*)d{
    if(d==nil) return;
    [[MenuViewController sharedInstance] rimuoviMessaMenu];
    [self resetMessa];
    day = nil;
    day = d;
    [day salvaGiornoInUso:nil];
    [MenuViewController aggiornaVista];
    
    // NSLog(@"caricaGiornoWithDayObj:%@",day);
    [self settaHOME];
    if([Home isContentVisible])[Home mostraHome];
}



-(void)caricaGiornoInUso{
    NSLog(@"Home:caricaGiornoInUso");
    //========
    // STEP 1
    //========
    //sourceString();
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"Home:caricaGiornoInUso(1)");
        day = [Day caricaGiornoInUso];
        if(currentMessa!=nil){
            [[MenuViewController sharedInstance] rimuoviMessaMenu];
            [self resetMessa];
        }

    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"Home:caricaGiornoInUso(2)");
        if(day == nil)
        {
            [Home recuperaCacheMex:@"CACHE PROBLEM - no net"];
            return;
        }
        if(home_did_load){
            // NSLog(@"Home:settaHomeWithNoAnimation");
            [self settaHomeWithNoAnimation];
        }
        else{
            // NSLog(@"Home:settaHome - Menu aggiornaVista");
            [MenuViewController aggiornaVista];
            [self settaHOME];
        }
    }];//fine step2
    
    //========
    // STEP 3
    //========
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"Home:caricaGiornoInUso(3)");
        [Home mostraHome];
    }];//fine step3
    
    
    
    [step2 addDependency:step1];
    [step3 addDependency:step2];
    [[NSOperationQueue new] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    [[NSOperationQueue mainQueue] addOperation:step3];
  
}

+(BOOL)isMessaINUSO{
    return currentMessa != nil;
}

+(BOOL)editorMessaAttivo{
    Home *h = [Home sharedInstance];
    return h.messa_titolo_ctrl.view.superview != nil || h.messa_selettore_ctrl.view.superview != nil || h.messa_editor_ctrl.view.superview != nil;
}

+(void)resetMessa{
    currentMessa = nil;
}

-(void)resetMessa{
    currentMessa = nil;
}

#pragma mark - Button Action
//=========================
// Button Action
//=========================
-(IBAction)mostraOggi:(UIButton*)sender{

    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"yyyy-MM-dd"];
    self.day = [Day searchForDay:[f stringFromDate:[NSDate date]] withLang:[self.defaults objectForKey:LINGUA]];
    BOOL presente = (self.day != nil);
    // NSLog(@"showTodayItem.%@",self.day);
    NSBlockOperation *step_download = nil;
    if(!presente && !self.connesso) { [self noNet];  return; }
    if(!presente){
        //Your main thread code goes in here
        NSDateFormatter *time = [NSDateFormatter new];
        [time setDateFormat:@"eee, dd MMM"];
        [Home changeHudText:[NSString stringWithFormat:@"Downloading %@",[[time stringFromDate:[NSDate date]] capitalizedString]]];
        [self activityNetwork:YES];
        // NSLog(@"download message showed!");
        step_download = [NSBlockOperation blockOperationWithBlock:^{
            Day *d = [Day scaricaSYNCGiornoconData:[f stringFromDate:[NSDate date]]
                                          withLang:[self.defaults objectForKey:LINGUA]
                                         eventFlag:[TestoProprio getTestoProprioWithKey:[self.defaults objectForKey:EVENT_FLAG]]];
            // NSLog(@"mostraOggi - oggi scaricato %@",d);
            [NSThread sleepForTimeInterval:0.5];
            if(d==nil){
                [self noNet];
                return;
            }
            self.day = d;
            [f setDateFormat:@"d/M/yy"];
            if(day.complete)
            {
                //notifico avvenuto download
                [self mostraNotificaDayDownloaded:[f stringFromDate:[NSDate date]]];
            }
            else
                //GIORNO INCOMPLETO
                [self mostraNotificaDayERROR_INCOMPLETE:[f stringFromDate:[NSDate date]]];
        }];
        [[NSOperationQueue new] addOperation:step_download];
    }//fine !presente

    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{ [self fineHUD_widget]; }];
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{ [self caricaGiornoWithDay:day.filename]; }];
    if(step_download!=nil)
        [step1 addDependency:step_download];
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
}

#pragma mark -
#pragma mark ROTELLA STATUSBAR
//=========================
// ROTELLA STATUSBAR
//=========================
-(BOOL)activityNetwork:(BOOL)b{
    if ([UIApplication sharedApplication].statusBarHidden){
        // NSLog(@"Activity HOME FULL!!");
        runOnMainQueueWithoutDeadlocking(^{
            if(b) [self attivaRotellaFull];
            else [self disattivaRotellaFull];
            if(b) [self AttivaRotellaMain];
            else [self rimuoviRotellaDelay];
        });
        }
    else{
        [super activityNetwork:b];
    }
    return YES;
}

-(void)attivaRotellaFull{
    [rotellaFUllScreen startAnimating];
    [rotellaFUllScreen setHidden:NO];
}

-(void)disattivaRotellaFull{
    [rotellaFUllScreen stopAnimating];
    [rotellaFUllScreen setHidden:YES];
}

#pragma mark -
#pragma mark ROTELLA
//=========================
// ROTELLA
//=========================
+(void)attivaRotellaHome{
    runOnMainQueueWithoutDeadlocking(^{
        Home *h = [Home sharedInstance];
       [h AttivaRotellaMain];
    });
}

-(BOOL)AttivaRotella{
    runOnMainQueueWithoutDeadlocking(^{
        [self AttivaRotellaMain];
    });
    return YES;
}

-(void)AttivaRotellaMain{
    // NSLog(@"AttivaRotellaMain");
    [subHomeAcitivy setHidden:NO];
    if(!self.isIpad)
    {
        [rotellaSettaHome startAnimating];
        [rotellaSettaHome setHidden:NO];
        return; //<<FINE IPHONE
    }
    //>> iPAD
    [rotellaSettaHome startAnimating];
    [rotellaSettaHome setHidden:NO];
    //CGRect screenBound = [[UIScreen mainScreen] bounds];
    //[subHomeAcitivy setCenter:self.isLand?CGPointMake(screenBound.size.width/2.0, 500 + (area_santo.frame.size.height/2.0)):CGPointMake(screenBound.size.width/2.0,598)];
    [subHomeAcitivy setFrame:CGRectMake(logo.frame.origin.x-5.f,  logo.frame.origin.y+17.f, 49.f, 49.f)];
    [subHomeAcitivy setAlpha:1];
    [rotellaSettaHome setAlpha:0.8];
    // NSLog(@"FINE - rotella:(%@) %@ - subHomeAcitivy:%@",NSStringFromCGRect(rotellaSettaHome.frame),NSStringFromCGPoint(rotellaSettaHome.center),NSStringFromCGRect(subHomeAcitivy.frame));

}

-(void)rimuoviRotella{
    runOnMainQueueWithoutDeadlocking(^{
        [self rimuoviRotellaMM];
    });
}
-(void)rimuoviRotellaMM{
    [rotellaSettaHome startAnimating];
    [rotellaSettaHome setHidden:NO];
    [self performSelector:@selector(rimuoviRotellaDelay) withObject:nil afterDelay:0.8];
}

-(void)rimuoviRotellaSenzaDelay{
    runOnMainQueueWithoutDeadlocking(^{
        [self rimuoviRotellaDelay];
    });
}

-(void)rimuoviRotellaDelay{
    [UIView animateWithDuration:0.4 animations:^{
                                            [rotellaSettaHome setAlpha:0];
                                            if(self.isIpad) [subHomeAcitivy setAlpha:0];
                                                }
                     completion:^(BOOL finished)    {
                         if(self.isIpad) [subHomeAcitivy setHidden:YES];
                         [rotellaSettaHome setHidden:YES];
                         [rotellaSettaHome stopAnimating];
                         [rotellaSettaHome setAlpha:0.8];
                     }];

}

+(void)noNetInHome{
    Home *h = [Home sharedInstance];
    [h noNet];
}

+(void)errorePrimoAggiornamento:(NSError *)err
{
    NSString *mex = @"ERR 1213\n..please try later";
    if(err!=nil){
        mex = [NSString stringWithFormat:@"%@\n..please try later",[err localizedDescription]];
    }
    // NSLog(@"errorePrimoAggiornamento:%@",mex);
    Home *h = [Home sharedInstance];

    // NSLog(@"errorePrimoAggiornamento_method:%@",mex);
    [h.HUD.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    UILabel *testo = [[UILabel alloc] initWithFrame:CGRectMake(5,0,h.HUD.frame.size.width-10,h.HUD.frame.size.height)];
    [testo setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:14]];
    [testo setNumberOfLines:0];
	[testo setBackgroundColor:[UIColor clearColor]];
	[testo setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.9]];
    [testo setTextAlignment:NSTextAlignmentCenter];
    [testo setText:mex];
    [testo setTag:1];
    [h.HUD addSubview:testo];
    // NSLog(@"errorePrimoAggiornamento_method");
}



#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================
-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    if(self.isIpad){
        // NSLog(@"Supported Orient:ALL");

        return UIInterfaceOrientationMaskAll;
    }
    BOOL support  = [Home isHomeVisible];
    //[NAVBAR setHidden:self.isLand];
    // NSLog(@"Supported Orient:%@",support?@" onlyPORT":@"ALL");
    
    return support?UIInterfaceOrientationMaskPortrait:UIInterfaceOrientationMaskAll;
}

-(BOOL)shouldAutorotate {
    BOOL risp = [Home isContentVisible];
    if(self.isIpad){
        if([Home isMessaINUSO]) risp = ![Home editorMessaAttivo];
        else risp = YES;
        }
    NSLog(@"shouldAutorotate:%i",risp);
    return risp;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self didRotate];
}

-(void)didRotate{
    // NSLog(@"Home:didRotate:%i",self.isLand);
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]){
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    
    
    if(!self.isIpad)
    {
        //===========
        //>> iPHONE
        //===========
        [NAVBAR setHidden:self.isLand];
        [webContent setFrame:CGRectMake(0, self.isLand?0:((NAVBAR.frame.origin.y<-20)?0:63), self.view.frame.size.width, self.view.frame.size.height)];
        [fullOutOnText_btn setHidden:(self.isLand && !self.isIpad) || [self.defaults boolForKey:SOLO_FULLSCREEN]];
        [self controllaPosizioneAutoscrollArea];
    }
    else
    {
        //===========
        //>> IPAD
        //===========
        if([DDMenuController isMenuOpen] && ![Home isMessaINUSO]){
            // NSLog(@"Menu open and not messa in uso");
            [self closeMenuIfOpen];
        }
        
        [self controllaRotazioneIPAD];
    }

}

-(void)controllaPosizioneAutoscrollArea{
    if(self.isIpad) return;
    CGRect autoframe = autoscroller_area.frame;
    if(self.isLand)
    {
        autoframe.origin.y = self.isIphone5?5:(self.isIos7orUpper?5:0);
        if(!self.isIphone5 && !self.isIos7orUpper) autoframe.origin.x = 380;
    }
    else
    {
        // >>>PORTRAIT IPHONE
        // NSLog(@"self.NAVBAR.frame.origin.y:%f",self.NAVBAR.frame.origin.y);
        BOOL withoutNav = (self.NAVBAR.frame.origin.y < -20);
        
        if(self.isIphone5 || self.isIos7orUpper) //iPhone4 con iOS7
            autoframe.origin.y = [self.defaults boolForKey:SOLO_FULLSCREEN]?10:(withoutNav?60:70);
        else //iPhone con iOS6
        {
            // NSLog(@"autoframe-iOS6_land:%@",NSStringFromCGRect(autoframe));
            autoframe.origin.x = 220;
            autoframe.origin.y = [self.defaults boolForKey:SOLO_FULLSCREEN]?2:49;
        }
        
    }
    
    
    [autoscroller_area setFrame:autoframe];
    // NSLog(@"controllaPosizioneAutoscrollArea[CHECKED]:%@",NSStringFromCGRect(autoframe));
}

-(void)controllaRotazioneIPAD{
    NSLog(@"controllaRotazioneIPAD%@:%@",IS_IPAD_PRO?@"PRO":@"",self.isLand?@"LAND":@"PORT");
    
    [webContent setFrame:CGRectMake(0, ((NAVBAR.frame.origin.y<-20)?0:63), self.view.frame.size.width, self.view.frame.size.height-((NAVBAR.frame.origin.y<-20)?0:63))];
    
    runOnMainQueueWithoutDeadlocking(^{
        [sfondo_home setImage:[UIImage imageNamed:self.isLand?@"IPAD2014_land.png":@"IPAD2014_port.png"]];
    });

    
    if(IS_IPAD_PRO){
    //<<<<<<<< iPAD PRO >>>>>>>>>>>
        CGRect santo_frame = area_santo.frame;
        santo_frame.origin.y = self.isLand?750:1049;
        [UIView animateWithDuration:0.3
                         animations:^{
                             [area_santo setFrame:santo_frame];
                         } completion:^(BOOL fine){
                             NSLog(@"area:%@\n%@",area_santo,self.HOME);
                            }];

    }
    else{
    //<<<<<<<<<< iPAD >>>>>>>>>>>>>
    CGRect attribute_frame = area_attribute.frame;
    attribute_frame.origin.y = self.isLand?300:339;
    
    //SANTO POSITION
    CGRect santo_frame = area_santo.frame;
    santo_frame.origin.y = self.isLand?500:717;
    //if(IS_IPAD_PRO) santo_frame.origin.y = self.isLand?1.049:1.049;
    
    [UIView animateWithDuration:0.3
                     animations:^{
        [area_santo setFrame:santo_frame];
        [area_attribute setFrame:attribute_frame];
                     } completion:^(BOOL fine){
                         //se è ipad pro non fare nulla
                         if(!IS_IPAD_PRO) return;
                         [UIView animateWithDuration:0.2
                                          animations:^{
                                              if(self.isPort){
                                                  CGRect titolo_frame = titolo_ufficiale.frame;
                                                  titolo_frame.origin.y = 96;
                                                  [titolo_ufficiale setFrame:titolo_frame];
                                              }
                                              else
                                              {
                                                  CGPoint newPoint = [self.view convertPoint:self.view.center toView:area_centrale];
                                                  [titolo_ufficiale setCenter:newPoint];
                                              }
                                              
                                          } completion:^(BOOL fine){
                                              //FINE TITOLO POSITION
                                          }];
                                    }];
    }
    
    [subHomeAcitivy setFrame:CGRectMake(logo.frame.origin.x-5.f,  logo.frame.origin.y+17.f, 49.f, 49.f)];

    /*
    [area_santo setBackgroundColor:[UIColor redColor]];
    [titolo_ufficiale setBackgroundColor:[UIColor greenColor]];
    NSLog(@"\n===\narea stanto:%@ \n===\titolo_ufficiale:%@",area_santo,titolo_ufficiale);
    // */
    [self rimuoviRotellaSenzaDelay];
    
    [self notifica:DID_ROTATE];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    // NSLog(@"Home: willRotateToInterfaceOrientation");
    if(!self.isIpad){
        if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation)){
            [self closeMenuIfOpen];
            [NAVBAR setHidden:YES];
            }
    }
    else{
        [self closeMenuIfOpen];
        if(self.santoCTRL != nil)
            [self removeSantoViewCTRL];
        else if(self.linguaCTRL != nil)
            [self removeLingua];
        else if(self.custodia != nil)
            [self removeCustodia];
        else if(self.terraSancta != nil)
            [self removeTS];
        else if(self.guida != nil)
            [self removeGuidaPreghiera];
        else if(self.principi != nil)
            [self removePrincipi];
        else if(self.tabella_notificheCTRL)
            [self removeTabella_notificheHome];
        else if(self.notificaCTRL != nil)
            [self removeNotificaCTRL];
        else if(self.infoCTRL != nil)
            [self removeInfo];
        else if(self.lista_popover)
            [self dismissLista];
        else if(self.event_popover)
            [self dismissEvent];
        else if(self.messa_popover)
            [self dismissMessa];
        else if(self.mySpiritualBook_popover)
            [Home dismissMySpiritual];
    }

}

-(void)willRotate{
    // NSLog(@"Home: willRotateToInterfaceOrientation");
    if(!self.isIpad){
        [NAVBAR setHidden:YES];
    }
    else{
        [self closeMenuIfOpen];
        if(self.santoCTRL != nil)
            [self removeSantoViewCTRL];
        else if(self.linguaCTRL != nil)
            [self removeLingua];
        else if(self.custodia != nil)
            [self removeCustodia];
        else if(self.terraSancta != nil)
            [self removeTS];
        else if(self.guida != nil)
            [self removeGuidaPreghiera];
        else if(self.principi != nil)
            [self removePrincipi];
        else if(self.tabella_notificheCTRL)
            [self removeTabella_notificheHome];
        else if(self.notificaCTRL != nil)
            [self removeNotificaCTRL];
        else if(self.infoCTRL != nil)
            [self removeInfo];
        else if(self.lista_popover)
            [self dismissLista];
        else if(self.event_popover)
            [self dismissEvent];
        else if(self.messa_popover)
            [self dismissMessa];
        else if(self.mySpiritualBook_popover)
            [Home dismissMySpiritual];
    }
}


-(void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    // NSLog(@"willTransitionToTraitCollection");
    [self willRotate];
}

-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    // NSLog(@"viewWillTransitionToSize");
    [self willRotate];
}


//=========================
// END CLASS
//=========================

@end
