@import Foundation;
@import CoreData;

@protocol PersistentStackDelegate<NSObject>

- (void)storeWillChange;
- (void)storeDidChange;
- (void)storeDidImport;

@end


@interface PersistentStack : NSObject

@property (nonatomic,strong,readwrite) NSManagedObjectContext* managedObjectContext;
@property (nonatomic,strong,readwrite) NSManagedObjectContext* privateQueueContext;
@property (assign, nonatomic) id<PersistentStackDelegate> delegate;

- (void)saveContext;
- (NSManagedObjectContext *)managedObjectContext;
- (void)savePrivateContext;
- (NSManagedObjectContext *)privateQueueContext;


- (id)initWithStoreURL:(NSURL *)storeURL modelURL:(NSURL *)modelURL;


//metodi accessori
+(BOOL)isCloudAvaible;
+(void)display_iCloudAlertIfNeeded;
+(NSString*)checkForiCloudString;

@end