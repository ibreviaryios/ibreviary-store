//
//  Lingua.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"

@interface FadingScroller: UIScrollView

@property (nonatomic) float percent; // 1 - 100%

@end


@protocol LinguaDelegate <NSObject>
- (void)removeLingua;
@optional
- (void)linguaPropriScelta:(NSString*)scelta_tp;
@end




@interface Lingua : BaseController


@property (nonatomic, strong) BaseController<LinguaDelegate> *delegate;

@property (nonatomic, strong) NSMutableArray *data_lingua;
@property (nonatomic, strong) IBOutlet UIImageView *sfondo;
@property (nonatomic, strong) IBOutlet UIButton *close;
@property (nonatomic, strong) IBOutlet UILabel *titolo;
@property (strong, nonatomic) IBOutlet FadingScroller *scroller;
@property (nonatomic) BOOL isLista;
@property (nonatomic) BOOL isForTP;
@property (strong, nonatomic) IBOutlet UILabel *testoBasso;
@property (strong, nonatomic) IBOutlet UILabel *line;


- (id)initWithDelegate:(BaseController<LinguaDelegate>*)d andIsLista:(BOOL)b;
- (id)initForTPWithDelegate:(BaseController<LinguaDelegate>*)d;

-(void)settaTestoPerTP;

@end

