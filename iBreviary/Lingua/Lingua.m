//
//  Lingua.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "Lingua.h"
#import "Day.h"
#import "LinguaObject.h"
#import "Home.h"



@implementation FadingScroller

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.percent = IS_IPAD?20.0f:5.0f;
    
    [self addObserver:self forKeyPath:@"bounds" options:0 context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if(object == self && [keyPath isEqualToString:@"bounds"])
    {
        [self initMask];
    }
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"bounds"];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self updateMask];
}

- (void)initMask
{
    CAGradientLayer *maskLayer = [CAGradientLayer layer];
    
    maskLayer.locations = @[@(0.0f), @(_percent / 100), @(1.0f)];
    
    maskLayer.bounds = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
    maskLayer.anchorPoint = CGPointZero;
    
    self.layer.mask = maskLayer;
    
    [self updateMask];
}

- (void)updateMask
{
    
    CGColorRef outer = [UIColor colorWithWhite:1.0 alpha:0.0].CGColor;
    CGColorRef inner = [UIColor colorWithWhite:1.0 alpha:1.0].CGColor;
    
    NSArray *colors = @[(__bridge id)outer, (__bridge id)inner, (__bridge id)inner, (__bridge id)outer];
    
    if(self.contentOffset.y <= 0) // top
    {
        colors = @[(__bridge id)inner, (__bridge id)inner, (__bridge id)inner, (__bridge id)outer];
    }
    else if((self.contentOffset.y + self.frame.size.height) >= self.contentSize.height) // bottom
    {
        colors = @[(__bridge id)outer, (__bridge id)inner, (__bridge id)inner, (__bridge id)inner];
    }
    
    ((CAGradientLayer *)self.layer.mask).colors = colors;
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    self.layer.mask.position = CGPointMake(0, self.contentOffset.y);
    [CATransaction commit];
}

@end

@interface Lingua ()

@property (strong,nonatomic) IBOutlet UIView *header;

@end


@implementation Lingua


@synthesize data_lingua, sfondo, close,delegate,scroller,titolo,isLista,isForTP,line,testoBasso;


#define  LOGO_TAG 123456



#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<LinguaDelegate>*)d andIsLista:(BOOL)b
{
    //iPhone4
    NSString *nibNameOrNil = @"Lingua";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = b?@"Lingua5":@"LinguaHome_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"Lingua5";
    if(IS_IPHONE_6) nibNameOrNil = @"Lingua6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"Lingua6Plus";
    

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        data_lingua = [LinguaObject getLingue];
        isLista = b;
        isForTP = NO;
        if(b && self.isIpad) [self.view setFrame:d.view.frame];
    }
    return self;
}

- (id)initForTPWithDelegate:(BaseController<LinguaDelegate>*)d
{
    //iPhone4
    NSString *nibNameOrNil = @"Lingua";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"Lingua5";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"Lingua5";
    if(IS_IPHONE_6) nibNameOrNil = @"Lingua6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"Lingua6Plus";
    
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        data_lingua = [LinguaObject getLingue];
        isLista = NO;
        isForTP = YES;
        if(self.isIpad) [self.view setFrame:d.view.frame];
    }
    return self;
}


#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.isIpad)
    {
        [self.view.layer setCornerRadius:10];
        [self.view.layer setMasksToBounds:YES];
    }    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    if(!self.isIos7orUpper)[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    if(!isLista && !isForTP && self.isIpad) [self homeGuiIPAD];
    else [self gui];
}

-(void)homeGuiIPAD{
    int top_margin = 0;
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
    
    int i = 0;
    if(data_lingua == nil) data_lingua = [LinguaObject getLingue];
    // NSLog(@"homeGui:%@",data_lingua);
    
    for(LinguaObject *row in data_lingua)
    {
        UIView *riga = [[UIView alloc] initWithFrame:CGRectMake((i<7)?0:280, top_margin+((i<7)?(i*60):((i-7)*60)), 300, 60)];
        [riga setBackgroundColor:[UIColor clearColor]];
        UIView *logo = row.view;
        [logo setTag:LOGO_TAG];
        CGRect  logo_frame =  logo.frame;
         logo_frame.origin.x = 40;
        [logo setFrame:logo_frame];
        [riga addSubview:logo];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 220, 56)];
        [title setText:row.nome];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setFont:[UIFont fontWithName:FONT_HELVETICANEUE_THIN size:20]];
        [title setTextAlignment:NSTextAlignmentLeft];
        [title setTextColor:[UIColor whiteColor]];
        [riga addSubview:title];
        
        // NSLog(@"aggiungoBottone:%i",i);
        CGRect frame =riga.frame;
        frame.origin.y = 10;
        frame.origin.x = 20;
        frame.size.width -=110;
        frame.size.height -=20;
        
        UIButton *b = [[UIButton alloc] initWithFrame:frame];
        [b setBackgroundColor:[UIColor clearColor]];
        //[b setImage:[UIImage imageNamed:@"linguaSelected.png"] forState:UIControlStateHighlighted];
        //[b setImage:[UIImage imageNamed:@"linguaSelected.png"] forState:UIControlStateSelected];
        [b setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 110)];
        [b setAlpha:0.85];
        [b setTag:i];
        [b addTarget:self action:@selector(linguaSelezionata:) forControlEvents:UIControlEventTouchUpInside];
        [riga addSubview:b];
        [scroller addSubview:riga];
        i++;
    }
    
    [scroller setContentSize:CGSizeMake(scroller.frame.size.width, ((data_lingua.count-7)*60)+top_margin)];
    // NSLog(@"Scroller:%@\n%@",scroller, scroller.subviews);
    
    if(isLista) {
        [line setHidden:YES];
        [testoBasso setHidden:YES];
        [scroller setFrame:CGRectMake(scroller.frame.origin.x, scroller.frame.origin.y, scroller.frame.size.width, self.view.frame.size.height-scroller.frame.origin.y)];
    }
    
    
    [scroller setShowsHorizontalScrollIndicator:NO];
    [scroller setShowsVerticalScrollIndicator:!self.isIpad];
    
    NSString *textTitile = isLista?NSLocalizedString(@"lingua_legenda", nil):NSLocalizedString(@"lingua_elenco", nil);
    
    [titolo setText:[textTitile uppercaseString]];
    
    
    //TESTO BASSO
    [testoBasso setTextColor:[UIColor whiteColor]];
    [testoBasso setTextAlignment:NSTextAlignmentCenter];
    
    NSString *textBasso = NSLocalizedString(@"lingua_seleziona", nil);
    [testoBasso setText:textBasso];
}


-(void)gui{
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
    
    int top_margin = self.isIpad?80:43;

    int i = 0;
    for(LinguaObject *row in data_lingua)
    {
        UIView *riga = [[UIView alloc] initWithFrame:CGRectMake(0, top_margin+(i*60), 320, 60)];
        [riga setBackgroundColor:[UIColor clearColor]];
        UIView *logo = row.view;
        CGRect  logo_frame =  logo.frame;
         logo_frame.origin.x = 40;
        [logo setFrame:logo_frame];
        [riga addSubview:logo];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 220, 56)];
        [title setText:row.nome];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setFont:[UIFont fontWithName:FONT_HELVETICANEUE_THIN size:20]];
        [title setTextAlignment:NSTextAlignmentLeft];
        [title setTextColor:[UIColor whiteColor]];
        [riga addSubview:title];
        if(!isLista){
            // NSLog(@"aggiungoBottone:%i",i);
            CGRect frame =riga.frame;
            frame.origin.y = 10;
            frame.origin.x = 20;
            frame.size.width -=110;
            frame.size.height -=20;

            UIButton *b = [[UIButton alloc] initWithFrame:frame];
            [b setBackgroundColor:[UIColor clearColor]];
            [b setImage:[UIImage imageNamed:@"linguaSelected.png"] forState:UIControlStateHighlighted];
            [b setImage:[UIImage imageNamed:@"linguaSelected.png"] forState:UIControlStateSelected];
            [b setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 110)];
            [b setAlpha:0.85];
            [b setTag:i];
            [b addTarget:self action:@selector(linguaSelezionata:) forControlEvents:UIControlEventTouchUpInside];
            [riga addSubview:b];
        }
        [scroller addSubview:riga];
        i++;
    }
    
    [scroller setContentSize:CGSizeMake(320, (data_lingua.count*60)+10+top_margin)];
    
    if(isLista) {
        [line setHidden:YES];
        [testoBasso setHidden:YES];
        [scroller setFrame:CGRectMake(scroller.frame.origin.x, scroller.frame.origin.y, scroller.frame.size.width, self.view.frame.size.height-scroller.frame.origin.y)];
    }

    
    [scroller setShowsHorizontalScrollIndicator:NO];
    [scroller setShowsVerticalScrollIndicator:!self.isIpad];
    
    NSString *textTitile = isLista?NSLocalizedString(@"lingua_legenda", nil):NSLocalizedString(@"lingua_elenco", nil);
    
    [titolo setText:[textTitile uppercaseString]];
    
    
    //TESTO BASSO
    [testoBasso setTextColor:[UIColor whiteColor]];
    [testoBasso setTextAlignment:NSTextAlignmentCenter];

    NSString *textBasso = NSLocalizedString(@"lingua_seleziona", nil);
    [testoBasso setText:textBasso];
    
    if(self.isIpad)[self.view setBackgroundColor:[self.view.backgroundColor colorWithAlphaComponent:0.85]];
    else [self.view setBackgroundColor:[UIColorFromRGB(0X3d2914) colorWithAlphaComponent:isLista?0.7:0.5]];

}

-(void)viewWillAppear:(BOOL)animated{
    // NSLog(@"Lingua: viewWillAppear");
    [super viewWillAppear:animated];
    if(isForTP) [self settaTestoPerTP];
}

-(void)viewDidAppear:(BOOL)animated{[super viewDidAppear:animated];
    if(!self.isIos7orUpper && !isLista && !isForTP){
        CGRect fr = self.view.frame;
        fr.origin.y =-20;
        [self.view setFrame:fr];
        // NSLog(@"Lingua did Appear:%@",self.view);
    }
}

-(void)settaTestoPerTP{
    [testoBasso setTextColor:[UIColor whiteColor]];
    [testoBasso setTextAlignment:NSTextAlignmentCenter];

    NSString *textTitile = NSLocalizedString(@"lingua_tp", nil);
    [testoBasso setText:textTitile];
    // NSLog(@"settaTestoPerTP:%@",testoBasso);
}

-(IBAction)linguaSelezionata:(UIButton*)sender{
    
    //=========================
    // SELEZIONATA lingua x TP
    //=========================
    if([delegate respondsToSelector:@selector(linguaPropriScelta:)]){
        // animazione e avvenuta scelta
        [self performSelectorOnMainThread:@selector(ringLine:) withObject:sender waitUntilDone:NO];
        [delegate linguaPropriScelta:[[data_lingua objectAtIndex:sender.tag] sigla]];
        return; //fine scelta TP
    }
    
    
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"Lingua: download step1");
        [self rotellaHome:sender];
        
    }];//fine step1
    
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"Lingua: download step2");
        [self scaricaCurrentPerHomeConLingua:[[data_lingua objectAtIndex:sender.tag] sigla] andBtn:sender];
    }];//fine step2
    
    [step2 addDependency:step1];

    [[NSOperationQueue currentQueue] addOperation:step1];
    [[NSOperationQueue currentQueue] addOperation:step2];
}

-(void)ringLine:(UIButton *)button
{
    UIView *anello = [[UIView alloc] initWithFrame:CGRectMake(37, 8, 24 , 24)];
    [anello setBackgroundColor:[UIColor clearColor]];
    //[anello.layer setBorderWidth:1];
    //[anello.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:1] CGColor]];
    CGFloat offset = 3.5;
    CGRect split = anello.frame;
    split.origin.x -=offset;
    split.origin.y -=offset;
    split.size.width += offset*2;
    split.size.height += offset*2;
    
    [anello setFrame:split];
    // NSLog(@"button:%@ anello:%@",NSStringFromCGRect(button.frame),NSStringFromCGRect(anello.frame));

    [anello.layer setCornerRadius:split.size.height/2];
    
    [button addSubview:anello];
    UIColor *stroke = [UIColor whiteColor];
    
    CABasicAnimation *borderAnimation = [CABasicAnimation animationWithKeyPath:@"borderColor"];
    borderAnimation.fromValue = (id)[UIColor clearColor].CGColor;
    borderAnimation.toValue = (id)stroke.CGColor;
    borderAnimation.duration = 0.8f;
    [anello.layer addAnimation:borderAnimation forKey:nil];
    
    CGRect pathFrame = CGRectMake(-CGRectGetMidX(anello.bounds), -CGRectGetMidY(anello.bounds), anello.bounds.size.width, anello.bounds.size.height);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:pathFrame cornerRadius:anello.layer.cornerRadius];
    
    UIView *dove;
    if(self.isIpad){
        dove = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    }
    else dove = self.view;
    CGPoint shapePosition = [dove convertPoint:anello.center fromView:anello.superview];
    
    
    CAShapeLayer *circleShape = [CAShapeLayer layer];
    circleShape.path = path.CGPath;
    circleShape.position = shapePosition;
    circleShape.fillColor = [UIColor clearColor].CGColor;
    circleShape.opacity = 0;
    circleShape.strokeColor = stroke.CGColor;
    circleShape.lineWidth = 2;
    
    [dove.layer addSublayer:circleShape];
    
    
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    scaleAnimation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(3.5, 3.5, 1)];
    
    CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    alphaAnimation.fromValue = @1;
    alphaAnimation.toValue = @0;
    
    CAAnimationGroup *animation = [CAAnimationGroup animation];
    animation.animations = @[scaleAnimation, alphaAnimation];
    animation.duration = 0.8f;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    [circleShape addAnimation:animation forKey:nil];
}

//==============================================
- (void)scaricaCurrentPerHomeConLingua:(NSString*)sigla andBtn:(UIButton*)sender{
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    
    NSString *date_string = [[Home getCurrentDay] id_date];
    __block Day *day = [Day new];
    day = [Day searchForDay:date_string withLang:sigla];
    
    if(day==nil)
    {
        // NSLog(@"=== GIORNO NON PRESENTE ===");
        if(self.connesso){
            [Day scaricaGiornoconData:date_string withLang:sigla eventFlag:[TestoProprio getTestoProprioWithKey:@""] completionBlock:^(Day *ddd){
                // NSLog(@"DAY DOWNLOADED!!:%@",day);
                dispatch_semaphore_signal(sem);
                return ddd;
            }];
        }
        else{
            [self noNet];
            dispatch_semaphore_signal(sem);
            [delegate removeLingua];
            return;
        }
    }
    else{
        // NSLog(@"=== GIORNO PRESENTE ===");
        dispatch_semaphore_signal(sem);
    }
    
    
    
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    if(day==nil)
        day = [Day searchForDay:date_string withLang:sigla];
    
    if(day!=nil){
        [self performSelectorOnMainThread:@selector(ringLine:) withObject:sender waitUntilDone:NO];
        [Home caricaGiornoWithDayObj:day];
        [delegate removeLingua];
    }
    else   [delegate removeLingua];
}

-(void)finescaricaCurrentDaHomeConLingua{
    [self performSelector:@selector(activityNetwork:) withObject:nil afterDelay:0.6];
    
}

//==============================================


-(void)rotellaHome:(UIButton *)sender{
    if(!self.isIpad)
    {
    UIActivityIndicatorView *a = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [a startAnimating];
    [a setTag:99999];
    int top_margin = self.isIpad?80:43;
    CGFloat y =  top_margin+(60*sender.tag);
    [a setCenter:CGPointMake(30, y+30)];
    [scroller addSubview:a];
    // NSLog(@"a:%@",NSStringFromCGRect([[scroller viewWithTag:99999] frame]));
    }
    else{
        //>>>> iPAD
        UIActivityIndicatorView *a = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [a startAnimating];
        [a setTag:99999];
        CGFloat x = ([sender superview].frame.origin.x==0)?30:310;
        [a setCenter:CGPointMake(x, [sender superview].center.y)];
        [scroller addSubview:a];
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 37, 37)];
        [v.layer setBorderWidth:4];
        [v.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:0.8] CGColor]];
        [v.layer setCornerRadius:v.frame.size.width/2.0];
        [v setCenter:[[sender superview] viewWithTag:LOGO_TAG].center];
        [[sender superview] addSubview:v];
        // NSLog(@"sender: %@ a:%@",[sender superview],NSStringFromCGRect([[scroller viewWithTag:99999] frame]));
    }
}

-(void)rotella:(UIButton *)sender{
        UIActivityIndicatorView *a = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [a startAnimating];
        [a setTag:99999];
        int top_margin = self.isIpad?80:43;
        CGFloat y =  top_margin+(60*sender.tag);
        [a setCenter:CGPointMake(30, y+30)];
        [scroller addSubview:a];
        // NSLog(@"a:%@",NSStringFromCGRect([[scroller viewWithTag:99999] frame]));
}


- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
- (IBAction)FineLingua:(UIButton *)sender{
    [delegate removeLingua];
}


#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}


//=========================
// ENDCLASS
//=========================
@end


