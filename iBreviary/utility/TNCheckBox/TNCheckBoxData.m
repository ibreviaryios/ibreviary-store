//
//  TNCheckBoxData.m
//  TNCheckBoxDemo
//
//  Created by Frederik Jacques on 24/04/14.
//  Copyright (c) 2014 Frederik Jacques. All rights reserved.
//

#import "TNCheckBoxData.h"

@implementation TNCheckBoxData

@synthesize labelMarginLeft = _labelMarginLeft;

#pragma mark - Debug
- (NSString *)description {
    return [NSString stringWithFormat:@"[TNCheckBoxData] Identifier: %@ - Tag: %i - Checked: %d", self.identifier, (int)self.tag, self.checked];
}

#pragma mark - Getters and setters
-(CGFloat)labelMarginLeft {

    if (_labelMarginLeft <= 0) {
        //default value
        _labelMarginLeft = 15.;
    }
    return _labelMarginLeft;
}

-(void)setLabelMarginLeft:(CGFloat)lm{
    _labelMarginLeft = lm;
}

-(UIFont *)labelFont {

    if (!_labelFont) {
        _labelFont = [UIFont systemFontOfSize:14.0f];
    }

    return _labelFont;
}

@end
