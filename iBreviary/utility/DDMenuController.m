//
//  DDMenuController.m
//  DDMenuController
//
//  Created by Devin Doty on 11/30/11.
//  Copyright (c) 2011 toaast. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "DDMenuController.h"
#import "Home.h"
#import "MenuViewController.h"

#define kMenuFullWidth 320.0f
#define kMenuDisplayedWidth 260.0f
#define kMenuOverlayWidth (self.view.bounds.size.width - kMenuDisplayedWidth)
#define kMenuBounceOffset 10.0f
#define kMenuBounceDuration .3f
#define kMenuSlideDuration .3f

#define MENU_SHADOW_RADIUS 10
#define MENU_SHADOW_OPACITY 1


@interface DDMenuController (Internal)
- (void)showShadow:(BOOL)val;
@end

@implementation DDMenuController

@synthesize delegate;

@synthesize leftViewController=_left;
@synthesize rightViewController=_right;
@synthesize rootViewController=_root;

@synthesize tap=_tap;
@synthesize pan=_pan;
@synthesize closepan=_closepan;
@synthesize back_panL = _backpanL;
@synthesize back_panR = _backpanR;
@synthesize backpanHtoL = _backpanHtoL;
@synthesize backpanHtoR = _backpanHtoR;

static DDMenuController *singletonInstance;
BOOL moving_back = NO;
BOOL LeftMenuIsopen = NO;
BOOL RightMenuIsopen = NO;
BOOL rootshowing = NO;
BOOL menushowing = NO;


#pragma mark - INIT & SINGLETON
//=========================
// CLASS METHOD
//=========================
+ (DDMenuController *)sharedInstance
{
    return singletonInstance;
}

+(void)closeMenuWithCompletion:(void (^)())completion{
    // NSLog(@"closeMenuWithCompletion");
    [[DDMenuController sharedInstance] showRootController:YES duration:0.3 withCompletion:(void (^)())completion];
}

+(void)showRightController:(void (^)())completion{
    [[DDMenuController sharedInstance] showRightController:YES withCompletion:completion];
}

+(void)showLeftController:(void (^)())completion{
    [[DDMenuController sharedInstance] showLeftController:YES withCompletion:completion];
}

+(BOOL)isMenuOpen{
    //DDMenuController *d = [DDMenuController sharedInstance];
    //..USO HOME...
    BOOL risp = [Home sharedInstance].view.frame.origin.x != 0;
    // NSLog(@"isMenuOpen :root:%f - home:%f risp:%i",d.rootViewController.view.frame.origin.x,[Home sharedInstance].view.frame.origin.x,risp);
    return risp;
}

-(UIViewController*)rootViewController{
    self.rootViewController = _root;
    return _root;
}


- (id)initWithRootViewController:(UIViewController*)controller {
    if ((self = [super init])) {
        singletonInstance = self;
        _root = [Home sharedInstance];
    }
    return self;
}

- (id)init {
    if ((self = [super init])) {
        singletonInstance = self;
        _root = [Home sharedInstance];
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)isIpad{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}
-(BOOL)isIphone{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // NSLog(@"DDMenuController viewDidLoad");
    [self setRootViewController:_root]; // reset root
    // NSLog(@"_root:%@",_root);
    // NSLog(@"root:%@",self.rootViewController);
    if (!_tap) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        tap.delegate = (id<UIGestureRecognizerDelegate>)self;
        [self.view addGestureRecognizer:tap];
        tap.cancelsTouchesInView = NO;
        _tap = tap;
    }
    
    if(!_pan){
        
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
        pan.delegate = (id<UIGestureRecognizerDelegate>)self;
        [_root.view addGestureRecognizer:pan];
        _pan = pan;
    }
    
    //prepare leftMenu
    if(!_left) [self setLeftViewController:[MenuViewController sharedInstance]];
    if(!_right.view.superview){
        [self.view insertSubview:_right.view atIndex:0];
        [_right.view removeFromSuperview];
        _menuFlags.showingRightView = NO;
    }
    
    if(!_left.view.superview){
        [self.view insertSubview:_left.view atIndex:0];
        _menuFlags.showingLeftView = YES;
    }

}

- (void)viewDidUnload {
    [super viewDidUnload];
    _tap = nil;
    _pan = nil;
}

//=========================
// StatusBar
//=========================
#pragma mark - StatusBar
- (BOOL)prefersStatusBarHidden
{
    BOOL risp = [_root prefersStatusBarHidden];
    NSLog(@"DDMenu:prefersStatusBarHidden:%@",risp?@"YES":@"NO");
//    NSLog(@"DDMenu:statusFrame:%@",NSStringFromCGRect([[UIApplication sharedApplication] statusBarFrame]));
    return risp;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return [_root preferredStatusBarUpdateAnimation];
}

//cambio colore statusbar in bianco
- (UIStatusBarStyle)preferredStatusBarStyle {
    return [_root preferredStatusBarStyle];
}


//=========================
// ROTATION
//=========================
#pragma mark - ROTATION

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    if(self.presentedViewController) [self.presentedViewController supportedInterfaceOrientations];
    return [_root supportedInterfaceOrientations];
    
}


-(BOOL)shouldAutorotate {
    return [_root shouldAutorotate];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return [_root shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    // NSLog(@"DDM->willRotateToInterfaceOrientation!");

    [[DDMenuController sharedInstance] showRootController:YES duration:0.2 withCompletion:nil];
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];

    
    if (_root) {
        
        [_root willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];

        UIView *view = _root.view;

        if (_menuFlags.showingRightView) {

            view.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
            
        } else if (_menuFlags.showingLeftView) {
           
            view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;

        } else {
            
            view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
            
        }
        
    }
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    // NSLog(@"DDM->didRotate!");
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];

    if (_root) {
        
        [_root didRotateFromInterfaceOrientation:fromInterfaceOrientation];

        CGRect frame = self.view.bounds;
        if (_menuFlags.showingLeftView) {
            frame.origin.x = frame.size.width - kMenuOverlayWidth;
        } else if (_menuFlags.showingRightView) {
            frame.origin.x = -(frame.size.width - kMenuOverlayWidth);
        }
        _root.view.frame = frame;
        _root.view.autoresizingMask = self.view.autoresizingMask;
        
        [self showShadow:(_root.view.layer.shadowOpacity!=0.0f)];
        
    }
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];

    if (_root) {
        [_root willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    }
    
}

#pragma mark - GestureRecognizers
-(void)panHomeBackFromLeftMenu:(UIPanGestureRecognizer*)gesture{
    // NSLog(@"panHomeBackFromLeftMenu");
    [self showRootController:YES  duration:0.3 withCompletion:nil];
}

-(void)panHomeBackFromRightMenu:(UIPanGestureRecognizer*)gesture{
    // NSLog(@"panHomeBackFromRightMenu");
    [self showRootController:YES  duration:0.3 withCompletion:nil];
}


- (void)pan:(UIPanGestureRecognizer*)gesture {
    if(gesture!=_pan || rootshowing)return;
    // NSLog(@"PAN-GestureRecognizer:%@",NSStringFromCGPoint([gesture locationInView:_root.view]));
    if (gesture.state == UIGestureRecognizerStateBegan) {
        if(menushowing) return;
        menushowing = YES;
        // NSLog(@"PAN-GestureRecognizerBEGAN:%f > 0 ? right -->>",[gesture velocityInView:self.view].x);
        [self showShadow:YES];
        _panOriginX = self.view.frame.origin.x;        
        _panVelocity = CGPointMake(0.0f, 0.0f);
        if([gesture velocityInView:self.view].x > 0) {
            _panDirection = DDMenuPanDirectionRight;
        } else {
            _panDirection = DDMenuPanDirectionLeft;
        }
        
    }
    
    if (gesture.state == UIGestureRecognizerStateChanged) {
        CGPoint velocity = [gesture velocityInView:self.view];
        if((velocity.x*_panVelocity.x + velocity.y*_panVelocity.y) < 0) {
            _panDirection = (_panDirection == DDMenuPanDirectionRight) ? DDMenuPanDirectionLeft : DDMenuPanDirectionRight;
        }
        
        _panVelocity = velocity;        
        CGPoint translation = [gesture translationInView:self.view];
        CGRect frame = _root.view.frame;
        frame.origin.x = _panOriginX + translation.x;
        if(frame.origin.x > kMenuDisplayedWidth) frame.origin.x = kMenuDisplayedWidth;
        if(frame.origin.x < -kMenuDisplayedWidth) frame.origin.x = -kMenuDisplayedWidth;

        
        if (frame.origin.x > 0.0f && !_menuFlags.showingLeftView) {
            
            if(_menuFlags.showingRightView) {
                _menuFlags.showingRightView = NO;
                [self.rightViewController.view removeFromSuperview];
            }
            
            if (_menuFlags.canShowLeft) {
                
                _menuFlags.showingLeftView = YES;
                CGRect frame = self.view.bounds;
				frame.size.width = kMenuFullWidth;
                self.leftViewController.view.frame = frame;
                [self.view insertSubview:self.leftViewController.view atIndex:0];
                
            } else {
                frame.origin.x = 0.0f; // ignore right view if it's not set
            }
            
        } else if (frame.origin.x < 0.0f && !_menuFlags.showingRightView) {
            
            if(_menuFlags.showingLeftView) {
                _menuFlags.showingLeftView = NO;
                [self.leftViewController.view removeFromSuperview];
            }
            
            if (_menuFlags.canShowRight) {
                
                _menuFlags.showingRightView = YES;
                CGRect frame = self.view.bounds;
				frame.origin.x += frame.size.width - kMenuFullWidth;
				frame.size.width = kMenuFullWidth;
                self.rightViewController.view.frame = frame;
                [self.view insertSubview:self.rightViewController.view atIndex:0];
     
            } else {
                frame.origin.x = 0.0f; // ignore left view if it's not set
            }
            
        }
        
        _root.view.frame = frame;

    } else if (gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateCancelled) {
            gesture.enabled = YES;
        //  Finishing moving to left, right or root view with current pan velocity
        
        DDMenuPanCompletion completion = DDMenuPanCompletionRoot; // by default animate back to the root
        
        if (_panDirection == DDMenuPanDirectionRight && _menuFlags.showingLeftView) {
            completion = DDMenuPanCompletionLeft;
        } else if (_panDirection == DDMenuPanDirectionLeft && _menuFlags.showingRightView) {
            completion = DDMenuPanCompletionRight;
        }
        
        CGPoint velocity = [gesture velocityInView:self.view];    
        if (velocity.x < 0.0f) {
            velocity.x *= -1.0f;
        }
        BOOL bounce = (velocity.x > 800);
        CGFloat originX = _root.view.frame.origin.x;
        CGFloat width = _root.view.frame.size.width;
        CGFloat span = (width - kMenuOverlayWidth);
        CGFloat duration = kMenuSlideDuration; // default duration with 0 velocity
        
        
        if (bounce) {
            duration = (span / velocity.x); // bouncing we'll use the current velocity to determine duration
        } else {
            duration = ((span - originX) / span) * duration; // user just moved a little, use the defult duration, otherwise it would be too slow
        }
        
        [CATransaction begin];
        [CATransaction setCompletionBlock:^{
            if (completion == DDMenuPanCompletionLeft) {
                [self showLeftController:NO withCompletion:^{menushowing = NO;}];
            } else if (completion == DDMenuPanCompletionRight) {
                [self showRightController:NO withCompletion:^{menushowing = NO;}];
            } else {
                [self showRootController:NO duration:0.3 withCompletion:^{menushowing = NO;}];
            }
            [_root.view.layer removeAllAnimations];
        }];
        
        CGPoint pos = _root.view.layer.position;
        CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        
        NSMutableArray *keyTimes = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        NSMutableArray *values = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        
        [values addObject:[NSValue valueWithCGPoint:pos]];
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
        [keyTimes addObject:[NSNumber numberWithFloat:0.0f]];
        if (bounce) {
            
            duration += kMenuBounceDuration;
            [keyTimes addObject:[NSNumber numberWithFloat:1.0f - ( kMenuBounceDuration / duration)]];
            if (completion == DDMenuPanCompletionLeft) {
                
                [values addObject:[NSValue valueWithCGPoint:CGPointMake(((width/2) + span) + kMenuBounceOffset, pos.y)]];
                
            } else if (completion == DDMenuPanCompletionRight) {
                
                [values addObject:[NSValue valueWithCGPoint:CGPointMake(-((width/2) - (kMenuOverlayWidth-kMenuBounceOffset)), pos.y)]];
                
            } else {
                
                // depending on which way we're panning add a bounce offset
                if (_panDirection == DDMenuPanDirectionLeft) {
                    [values addObject:[NSValue valueWithCGPoint:CGPointMake((width/2) - kMenuBounceOffset, pos.y)]];
                } else {
                    [values addObject:[NSValue valueWithCGPoint:CGPointMake((width/2) + kMenuBounceOffset, pos.y)]];
                }
                
            }
            
            [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
            
        }
        if (completion == DDMenuPanCompletionLeft) {
            [values addObject:[NSValue valueWithCGPoint:CGPointMake((width/2) + span, pos.y)]];
        } else if (completion == DDMenuPanCompletionRight) {
            [values addObject:[NSValue valueWithCGPoint:CGPointMake(-((width/2) - kMenuOverlayWidth), pos.y)]];
        } else {
            [values addObject:[NSValue valueWithCGPoint:CGPointMake(width/2, pos.y)]];
        }
        
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [keyTimes addObject:[NSNumber numberWithFloat:1.0f]];
        
        animation.timingFunctions = timingFunctions;
        animation.keyTimes = keyTimes;
        //animation.calculationMode = @"cubic";
        animation.values = values;
        animation.duration = duration;   
        animation.removedOnCompletion = NO;
        animation.fillMode = kCAFillModeForwards;
        [_root.view.layer addAnimation:animation forKey:nil];
        [CATransaction commit];   
    
    }    
    
}


- (void)closepan:(UIPanGestureRecognizer*)gesture {
    if(gesture!=_closepan || menushowing)return;
    // NSLog(@"PAN-GestureRecognizer:%@",NSStringFromCGPoint([gesture locationInView:_root.view]));
    if (gesture.state == UIGestureRecognizerStateBegan) {
        if(rootshowing) return;
        [self showShadow:NO];
        rootshowing = YES;
        _panOriginX = _root.view.frame.origin.x;
        _panVelocity = CGPointMake(0.0f, 0.0f);
        if([gesture velocityInView:self.view].x > 0) {
            _panDirection = DDMenuPanDirectionRight;
        } else {
            _panDirection = DDMenuPanDirectionLeft;
        }
        
    }
    
    if (gesture.state == UIGestureRecognizerStateChanged) {
        CGPoint velocity = [gesture velocityInView:self.view];
        // NSLog(@"if:%f velocity.x:%f, _panVelocity.x:%f",velocity.x*_panVelocity.x + velocity.y*_panVelocity.y,velocity.x, _panVelocity.x);

        if((velocity.x*_panVelocity.x + velocity.y*_panVelocity.y) < 0) {
            _panDirection = (_panDirection == DDMenuPanDirectionRight) ? DDMenuPanDirectionLeft : DDMenuPanDirectionRight;
        }
        
        _panVelocity = velocity;
        CGPoint translation = [gesture translationInView:self.view];
        CGRect frame = _root.view.frame;
        frame.origin.x = _panOriginX + translation.x;
        if(RightMenuIsopen){
            if(frame.origin.x >0) frame.origin.x = 0;
            if(frame.origin.x < -kMenuDisplayedWidth) frame.origin.x = -kMenuDisplayedWidth;
        }
        if(LeftMenuIsopen){
            if(frame.origin.x <0) frame.origin.x = 0;
            if(frame.origin.x > kMenuDisplayedWidth) frame.origin.x = kMenuDisplayedWidth;
        }
        
        // NSLog(@"frame.origin.x:%f",frame.origin.x);
        
        _root.view.frame = frame;
        
    } else if (gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateCancelled) {
        gesture.enabled = YES;
        //  Finishing moving to left, right or root view with current pan velocity

        CGRect frame = _root.view.frame;
        CGFloat offset = frame.origin.x;
        if(offset<0) offset=-offset;
        CGFloat duration = kMenuSlideDuration; // default duration with 0

        if(offset < 70) duration = 0.1;
        else if(offset <= 140) duration = 0.2;
        else duration = 0.3;
        
        [self showRootController:YES duration:duration withCompletion:^{
                    // NSLog(@"duration:%f",duration);
                    rootshowing = NO;
                    }];
        //[_root.view.layer removeAllAnimations];
        /*
        [CATransaction begin];
        [CATransaction setCompletionBlock:^{
            
        }];
        
        CGPoint pos = _root.view.layer.position;
        CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        
        NSMutableArray *keyTimes = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        NSMutableArray *values = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        
        [values addObject:[NSValue valueWithCGPoint:pos]];
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
        [keyTimes addObject:[NSNumber numberWithFloat:0.0f]];
        if (bounce) {
            
            duration += kMenuBounceDuration;
            [keyTimes addObject:[NSNumber numberWithFloat:1.0f - ( kMenuBounceDuration / duration)]];
            
                // depending on which way we're panning add a bounce offset
                if (_panDirection == DDMenuPanDirectionLeft) {
                    [values addObject:[NSValue valueWithCGPoint:CGPointMake((width/2) - kMenuBounceOffset, pos.y)]];
                } else {
                    [values addObject:[NSValue valueWithCGPoint:CGPointMake((width/2) + kMenuBounceOffset, pos.y)]];
                }
            
            // NSLog(@"pos.y");
            
            [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
            
        }

        [values addObject:[NSValue valueWithCGPoint:CGPointMake(width/2, pos.y)]];
        
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [keyTimes addObject:[NSNumber numberWithFloat:1.0f]];
        
        animation.timingFunctions = timingFunctions;
        animation.keyTimes = keyTimes;
        //animation.calculationMode = @"cubic";
        animation.values = values;
        animation.duration = duration;
        animation.removedOnCompletion = NO;
        animation.fillMode = kCAFillModeForwards;
        [_root.view.layer addAnimation:animation forKey:nil];
        [CATransaction commit];
        */
    }
}


- (void)tap:(UITapGestureRecognizer*)gesture {
    rootshowing = YES;
    [self showRootController:YES duration:0.3 withCompletion:^{rootshowing = NO;}];
}

- (void)pan_Leftback:(UIPanGestureRecognizer*)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        [self showShadow:NO];
        _panOriginX = _root.view.frame.origin.x;
        _panVelocity = CGPointMake(0.0f, 0.0f);
        // NSLog(@"velocityInView:%f",[gesture velocityInView:self.view].x);
        moving_back = YES;
    }
    if (gesture.state == UIGestureRecognizerStateChanged) {
        CGPoint velocity = [gesture velocityInView:self.view];
        moving_back = YES;
        _panVelocity = velocity;
        CGPoint translation = [gesture translationInView:self.view];
        CGRect frame = _root.view.frame;
        frame.origin.x = _panOriginX + translation.x;
        // NSLog(@"frame.origin.x:%f",frame.origin.x);
        if(!self.isIpad && frame.origin.x>self.view.frame.size.width-5) frame.origin.x = self.view.frame.size.width-5;
        if(self.isIpad && frame.origin.x>kMenuDisplayedWidth+5) frame.origin.x = kMenuDisplayedWidth+5;
        if(frame.origin.x<=0) frame.origin.x = 0;
        
        _root.view.frame = frame;
    } else if (gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateCancelled) {
        // Finishing moving to left, right or root view with current pan velocity
        
        CGPoint velocity = [gesture velocityInView:self.view];
        if (velocity.x < 0.0f) {
            velocity.x *= -1.0f;
        }
        BOOL bounce = (velocity.x > 800);
        CGFloat originX = _root.view.frame.origin.x;
        CGFloat width = _root.view.frame.size.width;
        CGFloat span = (width - kMenuOverlayWidth);
        CGFloat duration = kMenuSlideDuration; // default duration with 0 velocity
        if (bounce) {
            duration = (span / velocity.x); // bouncing we'll use the current velocity to determine duration
        } else {
            duration = ((span - originX) / span) * duration; // user just moved a little, use the defult duration, otherwise it would be too slow
        }
        [CATransaction begin];
        [CATransaction setCompletionBlock:^{
            [self showRootController:NO duration:0.3 withCompletion:nil];
            [_root.view.layer removeAllAnimations];
            moving_back = NO;
        }];
        CGPoint pos = _root.view.layer.position;
        CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        NSMutableArray *keyTimes = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        NSMutableArray *values = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        [values addObject:[NSValue valueWithCGPoint:pos]];
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
        [keyTimes addObject:[NSNumber numberWithFloat:0.0f]];
        [values addObject:[NSValue valueWithCGPoint:CGPointMake(width/2, pos.y)]];
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [keyTimes addObject:[NSNumber numberWithFloat:1.0f]];
        animation.timingFunctions = timingFunctions;
        animation.keyTimes = keyTimes;
        //animation.calculationMode = @"cubic";
        animation.values = values;
        animation.duration = duration;
        animation.removedOnCompletion = NO;
        animation.fillMode = kCAFillModeForwards;
        [_root.view.layer addAnimation:animation forKey:nil];
        [CATransaction commit];
    }
}


- (void)pan_Rightback:(UIPanGestureRecognizer*)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        [self showShadow:NO];
        _panOriginX = _root.view.frame.origin.x;
        _panVelocity = CGPointMake(0.0f, 0.0f);
        // NSLog(@"RBACK->velocityInView:%f",[gesture velocityInView:self.view].x);
        moving_back = YES;
    }
    if (gesture.state == UIGestureRecognizerStateChanged) {
        CGPoint velocity = [gesture velocityInView:self.view];
        if((velocity.x*_panVelocity.x + velocity.y*_panVelocity.y) < 0) {
            _panDirection = (_panDirection == DDMenuPanDirectionRight) ? DDMenuPanDirectionLeft : DDMenuPanDirectionRight;
        }
        moving_back = YES;
        _panVelocity = velocity;
        CGPoint translation = [gesture translationInView:self.view];
        CGRect frame = _root.view.frame;
        frame.origin.x = _panOriginX + translation.x;
        if(frame.origin.x>=0) frame.origin.x = 0;
        if(self.isIpad && frame.origin.x<-kMenuDisplayedWidth-5) frame.origin.x = -kMenuDisplayedWidth-5;
        _root.view.frame = frame;
    } else if (gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateCancelled) {
        // Finishing moving to left, right or root view with current pan velocity
        
        CGPoint velocity = [gesture velocityInView:self.view];
        if (velocity.x < 0.0f) {
            velocity.x *= -1.0f;
        }
        BOOL bounce = (velocity.x > 800);
        CGFloat originX = _root.view.frame.origin.x;
        CGFloat width = _root.view.frame.size.width;
        CGFloat span = (width - kMenuOverlayWidth);
        CGFloat duration = kMenuSlideDuration; // default duration with 0 velocity
        if (bounce) {
            duration = (span / velocity.x); // bouncing we'll use the current velocity to determine duration
        } else {
            duration = ((span - originX) / span) * duration; // user just moved a little, use the defult duration, otherwise it would be too slow
        }
        [CATransaction begin];
        [CATransaction setCompletionBlock:^{
            [self showRootController:NO duration:0.3 withCompletion:nil];
            [_root.view.layer removeAllAnimations];
            moving_back = NO;
        }];
        CGPoint pos = _root.view.layer.position;
        CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        NSMutableArray *keyTimes = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        NSMutableArray *values = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        [values addObject:[NSValue valueWithCGPoint:pos]];
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
        [keyTimes addObject:[NSNumber numberWithFloat:0.0f]];
        [values addObject:[NSValue valueWithCGPoint:CGPointMake(width/2, pos.y)]];
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [keyTimes addObject:[NSNumber numberWithFloat:1.0f]];
        animation.timingFunctions = timingFunctions;
        animation.keyTimes = keyTimes;
        //animation.calculationMode = @"cubic";
        animation.values = values;
        animation.duration = duration;
        animation.removedOnCompletion = NO;
        animation.fillMode = kCAFillModeForwards;
        [_root.view.layer addAnimation:animation forKey:nil];
        [CATransaction commit];
    }
}



#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {

    if(rootshowing || menushowing) return NO;
    if (gestureRecognizer == _backpanL) {
        UIPanGestureRecognizer *panGesture = (UIPanGestureRecognizer*)gestureRecognizer;
        CGPoint location = [panGesture locationInView:_left.view];
        CGPoint velocity = [panGesture velocityInView:_left.view];
        BOOL risp;
        if(velocity.x > 0) return NO;
        if(location.x<=(kMenuDisplayedWidth+5.0)) risp =  YES;
        else risp = NO;
        // NSLog(@"gestureRecognizerShouldBegin-LEFT[%@]\nlocation:%@ velocity:%@",risp?@"YES":@"NO",NSStringFromCGPoint(location),NSStringFromCGPoint(velocity));
        return risp;
    }
    if (gestureRecognizer == _backpanR) {
        UIPanGestureRecognizer *panGesture = (UIPanGestureRecognizer*)gestureRecognizer;
        CGPoint location = [panGesture locationInView:_right.view];
        // NSLog(@"location:%@(<%f)",NSStringFromCGPoint(location),self.view.frame.size.width+5.0-kMenuDisplayedWidth);
        CGPoint velocity = [panGesture velocityInView:_right.view];
        // NSLog(@"gestureRecognizerShouldBegin-RIGHT\nlocation:%@ velocity:%@",NSStringFromCGPoint(location),NSStringFromCGPoint(velocity));
        if(velocity.x < 0) return NO;
        if(location.x>kMenuDisplayedWidth+5) return NO;
        else return YES;
    }
    // Check for horizontal pan gesture
    if (gestureRecognizer == _pan) {
        // NSLog(@"gestureRecognizerShouldBegin-PAN:%@, l:%i, r:%i",_pan,LeftMenuIsopen,RightMenuIsopen);
        BOOL risp = YES;
        if(LeftMenuIsopen || RightMenuIsopen || moving_back) return NO;
        if(![delegate slideNavigationControllerShouldDisplayLeftMenu]){
            // NSLog(@"slideNavigationControllerShouldDisplayLeftMenu NO");
            return NO;
        }
        if(![delegate slideNavigationControllerShouldDisplayRightMenu]){
                // NSLog(@"slideNavigationControllerShouldDisplayRightMenu NO");
                return NO;
            }
        
        // NSLog(@"risp:%i",risp);
        /*
        UIPanGestureRecognizer *panGesture = (UIPanGestureRecognizer*)gestureRecognizer;
        CGPoint translation = [panGesture translationInView:self.view];
        if (sqrt(translation.x * translation.x) / sqrt(translation.y * translation.y) > 1) {
            risp =  YES;
        } 
        
        else risp = NO;
        */
        // NSLog(@"gestureRecognizerShouldBegin-ROOT[PAN]:%@",risp?@"YES":@"NO");
        return risp;
    }
    
    if(gestureRecognizer == _closepan){
        // NSLog(@"gestureRecognizerShouldBegin-CLOSEPAN:%@, l:%i, r:%i",_closepan,LeftMenuIsopen,RightMenuIsopen);
        if(!LeftMenuIsopen && !RightMenuIsopen){
            [_root.view removeGestureRecognizer:_closepan];
            return NO;
        }
        UIPanGestureRecognizer *panGesture = (UIPanGestureRecognizer*)gestureRecognizer;
        BOOL risp;
        CGPoint translation = [panGesture translationInView:self.view];
        if (sqrt(translation.x * translation.x) / sqrt(translation.y * translation.y) < 1) {
            risp =  NO;
        }
        
        else risp = YES;
        // NSLog(@"CLOSEgestureRecognizerShouldBegin-ROOT[PAN GESTURE velocity:%f]:%@",[panGesture velocityInView:self.view].x,risp?@"YES":@"NO");
        return risp;

    }
    
    if (gestureRecognizer == _tap) {
        
        if (_root && (_menuFlags.showingRightView || _menuFlags.showingLeftView)) {
            return CGRectContainsPoint(_root.view.frame, [gestureRecognizer locationInView:self.view]);
        }
        
        return NO;
        
    }

    return YES;
   
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (gestureRecognizer==_tap) {
        return YES;
    }
    
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    BOOL should = YES;
//    if([self.sectionRootViewController isKindOfClass:[TrafficDetailViewController class]]){
//        //se è una vista dettaglio traffico
//        CGPoint touchPoint = [touch locationInView:self.sectionRootViewController.view];
//        }
    
//    NSLog(@"DDMEnu:shouldReceiveTouch:%@",[touch view]);
    if(gestureRecognizer==_tap && (touch.view.tag == 123456789 || [touch.view isKindOfClass:[UITableView class]])){
        // NSLog(@"TAP on %@",touch.view);
        should = NO;
    }
        
    if ((touch.view.subviews.count>0 && [touch.view isKindOfClass:[UIScrollView class]]) || [touch.view isKindOfClass:[UIButton class]] || [touch.view isKindOfClass:[UISlider class]] || touch.view.tag == 123456789)
    {
        // NSLog(@"DDMuneCotrnoller: gestureRecognizer is UIScrollView or UIButton -> subviews:%@",touch.view.subviews);
        should = NO;
    }
    
    if(![delegate slideNavigationControllerShouldShowRoot]){
        // NSLog(@"slideNavigationControllerShouldDisplayRightMenu NO");
        should = NO;
    }
    
//    NSLog(@"ShouldTouch:%@",should?@"yes":@"no");
    return should;

}


#pragma Internal Nav Handling 

- (void)resetNavButtons {
    if (!_root) return;
    
    UIViewController *topController = nil;
    if ([_root isKindOfClass:[UINavigationController class]]) {
        
        UINavigationController *navController = (UINavigationController*)_root;
        if ([[navController viewControllers] count] > 0) {
            topController = [[navController viewControllers] objectAtIndex:0];
        }
        
    } else if ([_root isKindOfClass:[UITabBarController class]]) {
        
        UITabBarController *tabController = (UITabBarController*)_root;
        topController = [tabController selectedViewController];
        
    } else {
        
        topController = _root;
        
    }
    
	//页面菜单
    if (_menuFlags.canShowLeft) {
        UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_menu_icon.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(showLeft:)];
        topController.navigationItem.leftBarButtonItem = button;
    } else {
        topController.navigationItem.leftBarButtonItem = nil;
    }
    
    if (_menuFlags.canShowRight) {
        UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_menu_icon.png"] style:UIBarButtonItemStyleBordered  target:self action:@selector(showRight:)];
        topController.navigationItem.rightBarButtonItem = button;
    } else {
        topController.navigationItem.rightBarButtonItem = nil;
    }
    
}

- (void)showShadow:(BOOL)val {
    if (!_root) return;
    // NSLog(@"showShadow:%@",val?@"YES":@"NO");
    _root.view.layer.shadowColor = UIColorFromRGB(0X4e3424).CGColor;
    _root.view.layer.shadowRadius = MENU_SHADOW_RADIUS;
    _root.view.layer.shadowOpacity = MENU_SHADOW_OPACITY;
    _root.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.view.bounds].CGPath;
    _root.view.layer.shouldRasterize = YES;
    _root.view.layer.rasterizationScale = [UIScreen mainScreen].scale;

    

}

- (void)showRootController:(BOOL)animated duration:(float)d withCompletion:(void (^)())completion {
    // NSLog(@"showRootController");
    [_tap setEnabled:NO];
    [_closepan setEnabled:NO];

    CGRect frame = _root.view.frame;
    frame.origin.x = 0.0f;

    BOOL _enabled = [UIView areAnimationsEnabled];
    if (!animated) {
        [UIView setAnimationsEnabled:NO];
    }
    
    if(d<=0) d = 0.3;
    // NSLog(@"showRoot:%f",d);
    [UIView animateWithDuration:d animations:^{
        
        _root.view.frame = frame;
        
    } completion:^(BOOL finished) {
        
        
        if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
        {
            [self prefersStatusBarHidden];
            [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
        }
        
        moving_back = NO;
        _menuFlags.showingLeftView = NO;
        _menuFlags.showingRightView = NO;
        LeftMenuIsopen = NO;
        RightMenuIsopen = NO;
        [self showShadow:NO];
        
        for (id gest in [_root.view gestureRecognizers]){
            [_root.view removeGestureRecognizer:gest];
        }
        
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
        pan.delegate = (id<UIGestureRecognizerDelegate>)self;
        [_root.view addGestureRecognizer:pan];
        _pan = pan;
                
        _closepan = nil;
        [_backpanL setEnabled:NO];
        [_backpanR setEnabled:NO];
        
        [_root.view addGestureRecognizer:_pan];
        
        // NSLog(@"root.view gesture:%@",[_root.view gestureRecognizers]);

        
        if(completion) completion();
        if (_left && _left.view.superview) {
            [_left.view removeFromSuperview];
        }
        
        if (_right && _right.view.superview) {
            [_right.view removeFromSuperview];
        }
        
        if([[DDMenuController sharedInstance] respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]){
            [[DDMenuController sharedInstance] setNeedsStatusBarAppearanceUpdate];
        }


    }];
    
    if (!animated) {
        [UIView setAnimationsEnabled:_enabled];
    }
    
}

-(void)setDelayRoot{
    // NSLog(@"setDelayRoot");
    rootshowing = NO;
    [_closepan setEnabled:YES];
    [_backpanL setEnabled:YES];
    [_backpanR setEnabled:YES];
}

- (void)showLeftController:(BOOL)animated withCompletion:(void (^)())completion {
    if (!_menuFlags.canShowLeft) return;
    
    if (_right && _right.view.superview) {
        [_right.view removeFromSuperview];
        _menuFlags.showingRightView = NO;
        RightMenuIsopen = NO;
    }
    
    if(![self.delegate slideNavigationControllerShouldDisplayLeftMenu]) return;
    
   
    [self.delegate menuController:self willShowViewController:self.leftViewController];

    _menuFlags.showingLeftView = YES;
    [self showShadow:YES];

    UIView *view = self.leftViewController.view;
	CGRect frame = self.view.bounds;
	frame.size.width = kMenuFullWidth;
    view.frame = frame;
    
    if(_left && !_left.view.superview) [self.view insertSubview:view atIndex:0];
    
    frame = _root.view.frame;
    frame.origin.x = CGRectGetMaxX(view.frame) - (kMenuFullWidth - kMenuDisplayedWidth);
    
    BOOL _enabled = [UIView areAnimationsEnabled];
    if (!animated) {
        [UIView setAnimationsEnabled:NO];
    }
    
    [UIView animateWithDuration:.3 animations:^{
        _root.view.frame = frame;
    } completion:^(BOOL finished) {
        [_tap setEnabled:YES];
        if(completion) completion();
        LeftMenuIsopen = YES;
        for (UIGestureRecognizer *l in _root.view.gestureRecognizers)
        [_root.view removeGestureRecognizer:l];
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(closepan:)];
        pan.delegate = (id<UIGestureRecognizerDelegate>)self;
        [_root.view addGestureRecognizer:pan];
        _closepan = pan;
        [_closepan setEnabled:NO];
        [_backpanL setEnabled:NO];
        _pan = nil;

        /*
        UISwipeGestureRecognizer *panBL = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(panHomeBackFromLeftMenu:)];
        panBL.direction = (UISwipeGestureRecognizerDirectionLeft);
        panBL.delegate = self;
        _backpanHtoL = panBL;
        [_root.view addGestureRecognizer:panBL];
        */
        // NSLog(@"_backpanHtoL:%@\n root\nrootview:%@",_backpanHtoL,_root.view.gestureRecognizers);
        if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
        {
            [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
        }
        // NSLog(@"SHOWED left");
        [self performSelector:@selector(setDelayRoot) withObject:nil afterDelay:0.3];
        if([Home isMessaINUSO] && ![MenuViewController sharedInstance].messaMenuCtrl.view.superview){
            [[MenuViewController sharedInstance] caricaMessaMenuEdit:NO];
            
        }
    }];
    
    if (!animated) {
        [UIView setAnimationsEnabled:_enabled];
    }
    
    
}


-(BOOL)isLand{
    return UIInterfaceOrientationIsLandscape([self interfaceOrientation]);
}


- (void)showRightController:(BOOL)animated withCompletion:(void (^)())completion {
    
    
    
    if (!_menuFlags.canShowRight) return;
    
    
    if (_left && _left.view.superview) {
        [_left.view removeFromSuperview];
        _menuFlags.showingLeftView = NO;
        LeftMenuIsopen = NO;
    }
    
    if(![self.delegate slideNavigationControllerShouldDisplayRightMenu]) return;

    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    
    if (_menuFlags.respondsToWillShowViewController) {
        [self.delegate menuController:self willShowViewController:self.rightViewController];
    }
    
    _menuFlags.showingRightView = YES;
    [self showShadow:YES];

    UIView *view = self.rightViewController.view;
    // NSLog(@"rightViewController:%@",self.rightViewController.view);
    CGRect frame = self.view.bounds;
	frame.origin.x += frame.size.width - kMenuFullWidth;
    if(self.isIpad){
        frame.origin.x=(self.isLand?1024:768)-kMenuFullWidth;
    }
    if(IS_IPAD_PRO){
        frame.origin.x=(self.isLand?1366:1024)-kMenuFullWidth;
    }
    
	frame.size.width = kMenuFullWidth;
    view.frame = frame;
    // NSLog(@"self.rightViewController:%@",self.rightViewController);
    [self.view insertSubview:view atIndex:0];
    //[self.view addSubview:view];
    
    frame = _root.view.frame;
    frame.origin.x = -(frame.size.width - kMenuOverlayWidth);
    
    BOOL _enabled = [UIView areAnimationsEnabled];
    if (!animated) {
        [UIView setAnimationsEnabled:NO];
    }
    
    [UIView animateWithDuration:.3 animations:^{
        _root.view.frame = frame;
    } completion:^(BOOL finished) {
        [_tap setEnabled:YES];
        if(completion) completion();
        RightMenuIsopen = YES;
        for (UIGestureRecognizer *l in _root.view.gestureRecognizers)
            [_root.view removeGestureRecognizer:l];
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(closepan:)];
        pan.delegate = (id<UIGestureRecognizerDelegate>)self;
        [_root.view addGestureRecognizer:pan];
        _closepan = pan;
        [self performSelector:@selector(setDelayRoot) withObject:nil afterDelay:0.3];

        _pan = nil;
        /*
        UISwipeGestureRecognizer *panBR = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(panHomeBackFromRightMenu:)];
        panBR.direction = (UISwipeGestureRecognizerDirectionRight);
        panBR.delegate = self;
        _backpanHtoR = panBR;
        [_root.view addGestureRecognizer:_backpanHtoR];
         */
    }];
    
    if (!animated) {
        [UIView setAnimationsEnabled:_enabled];
    }
}


#pragma mark Setters

- (void)setDelegate:(id<DDMenuControllerDelegate>)val {
    delegate = val;
    _menuFlags.respondsToWillShowViewController = [(id)self.delegate respondsToSelector:@selector(menuController:willShowViewController:)];    
}

- (void)setRightViewController:(UIViewController *)rc {
    // NSLog(@"rightController:%@",rc.view);
    if(rc==nil) return;
    _menuFlags.canShowRight = YES;
    UIView *view = rc.view;
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan_Rightback:)];
    pan.delegate = (id<UIGestureRecognizerDelegate>)self;
    [view addGestureRecognizer:pan];
    [pan setEnabled:NO];
    _backpanR = pan;
    _right = rc;
    [self resetNavButtons];
}

- (void)setLeftViewController:(UIViewController *)leftController {
    _left = leftController;
    _menuFlags.canShowLeft = (_left!=nil);
    UIView *view = _left.view;
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan_Leftback:)];
    pan.delegate = (id<UIGestureRecognizerDelegate>)self;
    [view addGestureRecognizer:pan];
    [pan setEnabled:NO];
    _backpanL = pan;
    [self resetNavButtons];
}

- (void)setRootViewController:(UIViewController *)rootViewController {
    UIViewController *tempRoot = _root;
    _root = rootViewController;
    
    if (_root) {
        
        if (tempRoot) {
            [tempRoot.view removeFromSuperview];
            tempRoot = nil;
        }
        
        UIView *view = _root.view;
        view.frame = self.view.bounds;
        [self.view addSubview:view];

        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
        pan.delegate = (id<UIGestureRecognizerDelegate>)self;
        pan.cancelsTouchesInView = NO;
        [view addGestureRecognizer:pan];
        _pan = pan;
    } else {
        
        if (tempRoot) {
            [tempRoot.view removeFromSuperview];
            tempRoot = nil;
        }
        
    }
    
    [self resetNavButtons];
}

- (void)setRootController:(UIViewController *)controller animated:(BOOL)animated {
   
    if (!controller) {
        [self setRootViewController:controller];
        return;
    }
    
    if (_menuFlags.showingLeftView) {
        
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        // slide out then come back with the new root
        __block DDMenuController *selfRef = self;
        __block UIViewController *rootRef = _root;
        CGRect frame = rootRef.view.frame;
        frame.origin.x = rootRef.view.bounds.size.width;
        
        [UIView animateWithDuration:.1 animations:^{
            
            rootRef.view.frame = frame;
            
        } completion:^(BOOL finished) {
            
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];

            [selfRef setRootViewController:controller];
            _root.view.frame = frame;
            [selfRef showRootController:animated duration:0.3 withCompletion:nil];
            
        }];
        
    } else {
        
        // just add the root and move to it if it's not center
        [self setRootViewController:controller];
        [self showRootController:animated duration:0.3 withCompletion:nil];
        
    }
     
}


#pragma mark - Root Controller Navigation

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    NSAssert((_root!=nil), @"no root controller set");
    
    UINavigationController *navController = nil;
    
    if ([_root isKindOfClass:[UINavigationController class]]) {
    
        navController = (UINavigationController*)_root;
    
    } else if ([_root isKindOfClass:[UITabBarController class]]) {
        
        UIViewController *topController = [(UITabBarController*)_root selectedViewController];
        if ([topController isKindOfClass:[UINavigationController class]]) {
            navController = (UINavigationController*)topController;
        }
        
    } 
    
    if (navController == nil) {
       
        // NSLog(@"root controller is not a navigation controller.");
        return;
    }
    
   
    if (_menuFlags.showingRightView) {
        
        // if we're showing the right it works a bit different, we'll make a screen shot of the menu overlay, then push, and move everything over
        __block CALayer *layer = [CALayer layer];
        CGRect layerFrame = self.view.bounds;
        layer.frame = layerFrame;
        
        UIGraphicsBeginImageContextWithOptions(layerFrame.size, YES, 0);
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        [self.view.layer renderInContext:ctx];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        layer.contents = (id)image.CGImage;
        
        [self.view.layer addSublayer:layer];
        [navController pushViewController:viewController animated:NO];
        CGRect frame = _root.view.frame;
        frame.origin.x = frame.size.width;
        _root.view.frame = frame;
        frame.origin.x = 0.0f;
        
        CGAffineTransform currentTransform = self.view.transform;
        
        [UIView animateWithDuration:0.25f animations:^{
            
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
                
                  self.view.transform = CGAffineTransformConcat(currentTransform, CGAffineTransformMakeTranslation(0, -[[UIScreen mainScreen] applicationFrame].size.height));
                
            } else {
                
                  self.view.transform = CGAffineTransformConcat(currentTransform, CGAffineTransformMakeTranslation(-[[UIScreen mainScreen] applicationFrame].size.width, 0));
            }
          
            
        } completion:^(BOOL finished) {
            
            [self showRootController:NO duration:0.3 withCompletion:nil];
            self.view.transform = CGAffineTransformConcat(currentTransform, CGAffineTransformMakeTranslation(0.0f, 0.0f));
            [layer removeFromSuperlayer];
            
        }];
        
    } else {
        
        [navController pushViewController:viewController animated:animated];
        
    }
    
}


#pragma mark - Actions 

- (void)showLeft:(id)sender {
    
    [self showLeftController:YES withCompletion:nil];
    
}

- (void)showRight:(id)sender {
    
    [self showRightController:YES withCompletion:nil];
    
}

@end
