//
//  Define.h
//  iBreviary
//
//  Created by leyo on 09/02/14.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#pragma mark - define Action
//===============
// IOS VERSION
//===============
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

//=============
//COLORI CON HTML
//=============
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)


#pragma mark - define VALUE


#define TIME_OUT_REQUEST 30.0


//#define // NSLog // NSLog(...)
//================
// DEBUG // NSLog
//================
#if DEBUG_CUSTOM
    #define LOG// NSLog
#else
    #define LOG LOG(...)
#endif
// */

//===============
// DEVICE SIZE
//===============
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPAD_PRO (IS_IPAD && ([UIScreen mainScreen].bounds.size.height == 1366 || [UIScreen mainScreen].bounds.size.width == 1366))

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_RETINA ([[UIScreen mainScreen] scale] == 2.0)

//===============
// support orientation iOS9
//===============
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

//================
//AGGIORNAMENTO
//================
#define AGGIORNAMENTO_2015 @"aggiornamento_giugno2015"

#define AGGIORNAMENTO_ICLOUD_NOTIFICA @"aggiornamentoICLOUDavvenuto"
#define AGGIORNAMENTO_FOR_ICLOUD @"readyForIcloud"

//================
//APP_ID IPHONE TS
//================
#define APPLE_ID @"422601705"

//================
//APP_ID IPHONE TS
//================
#define IBREVIARY_DEBUG_MODE @"IBREVIARY_DEBUG_MODE"


//=============
//FONT
//=============
#define FONT_PALATINO_ROMAN @"Palatino-Roman"
#define FONT_CALSON_ANTIQUE @"CaslonAntique"
#define FONT_ROMAN_ANTIQUE @"RomanAntique"
#define FONT_ROMAN_ANTIQUE_2 @"antiqueroman"
#define FONT_ROMAN_ANTIQUE_ITALIC @"RomanAntique-Italic"
#define FONT_STAMP_ACT @"StampAct"
#define FONT_HOEFLER @"HoeflerText-Regular"
#define FONT_HOEFLER_BOLD @"HoeflerText-Black"
#define FONT_HOEFLER_ITALIC @"HoeflerText-Italic"
#define FONT_EILEENCAPS @"EileenCaps-Black"
#define FONT_LUXI @"LuxiMono-Oblique"
#define FONT_HELVETICANEUE_ULTRALIGHT @"HelveticaNeue-UltraLight"
#define FONT_HELVETICANEUE_CONDENSEDBLACK @"HelveticaNeue-CondensedBlack"
#define FONT_HELVETICANEUE_LIGHT @"HelveticaNeue-Light"
#define FONT_HELVETICANEUE_THIN @"HelveticaNeue-Thin"
#define FONT_HELVETICANEUE_REGULAR @"HelveticaNeue"
#define FONT_HELVETICANEUE_BOLD @"HelveticaNeue-Bold"
#define FONT_HELVETICANEUE_ITALIC @"HelveticaNeue-Italic"
#define FONT_NOTA @"KabarettD"

//=============
//DEFAULT OBJECT
//=============
#define DATA_PRIMA_INSTALLAZIONE @"DATA_PRIMA_INSTALLAZIONE"
#define GIORNO_IN_USO @"GIORNO_IN_USO"
#define GIORNO_IN_USO_INDEX @"GIORNO_IN_USO_INDEX"
#define LINGUA @"lingua_sigla"
#define LINGUA_LISTA_SIGLE @"lingua_sigla_lista"
#define LINGUA_DICTIONARY @"lingua_dictionary"
#define LINGUA_COLOR @"LINGUA_COLOR"
#define LUMINOSITA @"luminosità"
#define CSS @"CSS"
#define ZOOM @"ZOOM"
#define EVENT_FLAG @"EVENT_FLAG"
#define AUTODOWNLOAD_OGGI @"AUTODOWNLOAD_OGGI"
#define AUTODOWNLOAD_OGGI_DATE @"AUTODOWNLOAD_OGGI_DATE"
#define TUTORIAL_LISTA_VISTO @"TUTORIAL_LISTA_VISTO"
#define TUTORIAL_MESSA_LISTA_VISTO @"TUTORIAL_MESSA_LISTA_VISTO"
#define TUTORIAL_SETTINGS_VISTO @"TUTORIAL_SETTINGS_VISTO"
#define TUTORIAL_TP_VISTO @"TUTORIAL_TP_VISTO"
#define TUTORIAL_MYSPIRITUAL_VISTO @"TUTORIAL_MYSPIRITUAL_VISTO"
#define COMPLINE_YESTERDAY @"COMPLINE_YESTERDAY"
#define SOLO_FULLSCREEN @"SOLO_FULLSCREEN"
#define FIXs_GIORNOINUSO_SPOSTATI @"FIXs_GIORNOINUSO_SPOSTATI"
#define LANG_e_TP_SPOSTATI @"LANG_e_TP_SPOSTATI"
#define WIDGET_CMD @"WIDGET_CMD"
#define PDF_FONT_TYPE @"PDF_FONT_TYPE"


//=====
//PATH
//=====
#define PATH_EVENTI @"EVENTI"
#define PATH_TESTIFIX @"TESTIFISSI"
#define PATH_IMG @"IMG"
#define PATH_DAY @"DAY"
#define GIORNO_IN_USO_DATA @"DATA/giorno_in_uso.data"
#define GIORNO_IN_USO_DOCUMENT @"/giorno_in_uso.data"
#define PATH_LINGUE @"/DATA/lingua.data"
#define PATH_TESTIPROPRI @"/DATA/testiPropri.data"


//=========
//INDICI
//=========
#define ROOT_TS_INDEX @"root_ts_index"
#define MESSALE_INDEX @"messale_index"
#define BREVIARIO_INDEX @"breviario_index"
#define BREVIARIO_COMUNE_INDEX @"breviario_index_comune"
#define TERRASANTA_INDEX @"terrasanta_index"


//=================
//LAST_UPDATE
//=================
#define LAST_UPDATE @"last_update"
#define LAST_UPDATE_LANG @"last_update_LANG"
#define LAST_UPDATE_EVENT @"last_update_EVENT"
#define NOTIFICHE_LAST_UPDATE_RICORRENZA @"last_update_ricorrente"
#define NOTIFICHE_LAST_ID_ISTANT @"last_istant_id"


//=========
//NOTIFICHE
//=========
#define AGGIORNAMENTO2015_TERMINATO @"AGGIORNAMENTO2015_TERMINATO"
#define ERRORE_CARICAMENTO_GIORNO @"ERR_CARICAMENTO_DAY"
#define NOT_NONET @"Notifica:noNet"
#define AGGIORNA_LISTA_DOWNLOAD @"AGGIORNA_LISTA_DOWNLOAD"
#define DID_ROTATE @"DID_ROTATE"
#define AGGIORNA_LISTA_MESSE @"AGGIORNA_LISTA_MESSE"

//===============================
//NOTIFICHE LOCALIZZATE STATUSBAR
//===============================
#define NOT_ICLOUD_UPDATE_OK @"NOT_ICLOUD_UPDATE_OK"
#define NOT_STATUS_UPDATE2014_OK @"NOT_STATUS_UPDATE2014_OK"
#define NOT_STATUS_DAY_OK @"NOT_STATUS_DAY_OK"
#define NOT_STATUS_WEEK_OK @"NOT_STATUS_WEEK_OK"
#define NOT_STATUS_NO_NET @"NOT_STATUS_NO_NET"
#define NOT_STATUS_DAY_NO @"NOT_STATUS_DAY_NO"
#define NOT_STATUS_WEEK_NO @"NOT_STATUS_WEEK_NO"
#define NOT_STATUS_WEEK_NO_SINGOLO @"NOT_STATUS_WEEK_NO_SINGOLO"

//====
//ITEM
//====
#define ITEM_TITLE @"title"
#define ITEM_CONTENT @"content"


//===========
//EVENTI-FLAG
//===========
#define KEY_TS @"Terrasanta"
#define KEY_SALESIANI @"Salesiani"
#define KEY_FRANCESCANI @"Francescani"
#define KEY_PASSIONISTI @"Passionisti"
#define KEY_SANGUINARI @"PreziosissimoSangue"


//==================
//AUTOSCROLL-DEFAULT
//==================
#define AUTOSCROLL_BOOL @"AUTOSCROLL_BOOL"
#define AUTOSCROLL_INDEX @"AUTOSCROLL_INDEX"
#define AUTOSCROLL_VALUE @"AUTOSCROLL_VALUE"
#define AUTOSCROLL_MILL @"AUTOSCROLL_MILL"
#define AUTOSCROLL_SPEED @"AUTOSCROLL_SPEED"


//====================
//AUDIOLETTURA-DEFAULT
//====================
#define AUDIOLETTURA_BOOL @"AUDIOLETTURA_BOOL"
#define AUDIORATE_INDEX @"AUDIORATE_INDEX"


//====================
//NOTIFICHE PUSH
//====================
#define TOKEN @"TOKEN"
#define TOKEN_INVIATO @"TOKEN_INVIATO"
#define CAN_SHOW_NOTIFICA @"CAN_SHOW_NOTIFICA"
#define CURRENT_NOTIFICATIONS @"currentNotifications"
#define NOTIFICHE_INCODA @"NOTIFICHE_INCODA_TO_SEE"

//====================
// URL
//====================
#define URL_LASTUPDATE @"http://www.ibreviary.com/service/services.php?s=get_status"
#define URL_POST_TOKEN @"http://ibreviary.org/iOS/services.php?type=addNot"


#define TUTORIAL_MENU_LIBRO @"TUTORIAL_MENU_LIBRO"
#define TUTORIAL_MENU_LIBRO_MESSA @"TUTORIAL_MENU_LIBRO_MESSA"


//====================
// MENU TYPE
//====================
typedef enum {
    MenuType_FontA=1,
    MenuType_FontB,
    MenuType_FontC,
    MenuType_Share,
    MenuType_CloseMenu,
    MenuType_CloseFeature,
}MenuButtonType;

//====================
// MESSA SELETTORE TYPE
//====================
typedef enum {
    tipo_lista_messa_comune,
    tipo_lista_credo,
    tipo_lista_preghiera_fedeli,
    tipo_lista_prefazio,
    tipo_lista_preghiera_eucaristica,
    tipo_lista_benedizione_solenne,
    tipo_lista_letture
}Tipo_messa_lista;

//====================
// STATO APPLICAZIONE TYPE
//====================
typedef enum {
    getting_background,
    getting_foreground,
    getting_active,
}App_type;

//====================
// TIPI Preghiera
//====================
typedef enum {
    nessuna_preghiera,
    ufficio,
    lodi,
    oraMedia,
    vespri,
    compieta
}Ora_type;






