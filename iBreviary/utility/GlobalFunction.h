//GlobalFunction.h


#import <Foundation/Foundation.h>

@class Reachability;

@interface GlobalFunction  :  NSObject
{
    BOOL internetActive;
    BOOL hostActive;
}

@property (readwrite,assign) BOOL internetActive;
@property (readwrite,assign) BOOL hostActive;
@property (nonatomic,strong) Reachability * internetReachable;
@property (readwrite,assign) Reachability * hostReachable;
@property (readwrite,assign) Reachability * wifiReach;

+ (BOOL)checkNetworkStatus;

@end


