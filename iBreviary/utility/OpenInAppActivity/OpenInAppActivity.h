//
//  OpenInAppActivity.h
//  Created by Leyo 2013.
//
//  CHIAMATA (creare istanza UIPoppoverController):
/*
 
 -(IBAction)openDocumentIn:(UIButton*)sender {
 
 NSString * filePath = [self.cacheDir stringByAppendingPathComponent:pdf_name];
 NSURL *URL = [NSURL fileURLWithPath:filePath];
 
 OpenInAppActivity *openInAppActivity = [[OpenInAppActivity alloc] initWithView:self.view andRect:sender.frame];
 UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[URL] applicationActivities:@[openInAppActivity]];
 
 if(self.isIphone)
 {
    openInAppActivity.superViewController = activityViewController;
    [self presentViewController:activityViewController animated:YES completion:NULL];
 }
 else
 {
 //iPAD!
    self.activityPopoverController = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
    openInAppActivity.superViewController = self.activityPopoverController;
    [self.activityPopoverController presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
 }
 
 }

 */
#import <UIKit/UIKit.h>

@class OpenInAppActivity;

@protocol OpenInAppActivityDelegate <NSObject>
- (void)openInAppActivityWillPresentDocumentInteractionController:(OpenInAppActivity*)activity;
- (void)openInAppActivityDidDismissDocumentInteractionController:(OpenInAppActivity*)activity;
@end

@interface OpenInAppActivity : UIActivity <UIDocumentInteractionControllerDelegate>

@property (nonatomic, weak) id superViewController;
@property (nonatomic, weak) id<OpenInAppActivityDelegate> delegate;

- (id)initWithView:(UIView *)view andRect:(CGRect)rect;
- (id)initWithView:(UIView *)view andBarButtonItem:(UIBarButtonItem *)barButtonItem;

- (void)dismissDocumentInteractionControllerAnimated:(BOOL)animated;

@end
