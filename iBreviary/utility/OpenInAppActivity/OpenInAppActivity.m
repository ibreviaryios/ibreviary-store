//
//  OpenInAppActivity.m
//  Created by Leyo 2013.
//
#import "OpenInAppActivity.h"
#import <MobileCoreServices/MobileCoreServices.h> // For UTI

@interface OpenInAppActivity ()

// Private attributes
@property (nonatomic, strong) NSURL *fileURL;
@property (nonatomic) CGRect rect;
@property (nonatomic, strong) UIBarButtonItem *barButtonItem;
@property (nonatomic, strong) UIView *superView;
@property (nonatomic, strong) UIDocumentInteractionController *docController;

// Private methods
- (NSString *)UTIForURL:(NSURL *)url;
- (void)openDocumentInteractionController;

@end

@implementation OpenInAppActivity
@synthesize rect = _rect;
@synthesize superView = _superView;
@synthesize superViewController = _superViewController;

- (id)initWithView:(UIView *)view andRect:(CGRect)rect
{
    if(self =[super init]){
        self.superView = view;
        self.rect = rect;
    }
    return self;
}

- (id)initWithView:(UIView *)view andBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    if(self =[super init]){
        self.superView = view;
        self.barButtonItem = barButtonItem;
    }
    return self;
}

- (NSString *)activityType
{
	return NSStringFromClass([self class]);
}

- (NSString *)activityTitle
{
	return NSLocalizedString(@"Apri in ...", @"Apri in ...");
}

- (UIImage *)activityImage
{
	if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
        return [UIImage imageNamed:@"OpenInAppActivity7"];
    else
        return [UIImage imageNamed:@"OpenInAppActivity"];
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems
{
    NSUInteger count = 0;
    // NSLog(@"canPerformWithActivityItems:%@",activityItems);

    for (id activityItem in activityItems) {
		if ([activityItem isKindOfClass:[NSURL class]] && [(NSURL *)activityItem isFileURL]) {
			count++;
		}
	}
	
	return (count == 1);
}

- (void)prepareWithActivityItems:(NSArray *)activityItems
{
    // NSLog(@"prepareWithActivityItems:%@",activityItems);
	for (id activityItem in activityItems) {
		if ([activityItem isKindOfClass:[NSURL class]] && [(NSURL *)activityItem isFileURL]) {
			self.fileURL = activityItem;
		}
	}
}

- (void)performActivity
{
    // NSLog(@"performActivity");
    if(!self.superViewController){
        [self activityDidFinish:YES];
        return;
    }

    // Dismiss activity view
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        // iPhone dismiss UIActivityViewController
        [self.superViewController dismissViewControllerAnimated:YES completion:^(void){
            // Open UIDocumentInteractionController
            [self openDocumentInteractionController];
        }];
    } else {
        [self.superViewController dismissPopoverAnimated:YES];
        [((UIPopoverController *)self.superViewController).delegate popoverControllerDidDismissPopover:self.superViewController];
        // Open UIDocumentInteractionController
        [self openDocumentInteractionController];
    }
}

#pragma mark - Helper
- (NSString *)UTIForURL:(NSURL *)url
{
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)url.pathExtension, NULL);
    // NSLog(@"UTI:%@",UTI);
    return (NSString *)CFBridgingRelease(UTI) ;
}

- (void)openDocumentInteractionController
{
    // Open "Open in"-menu
    // NSLog(@"openDocumentInteractionController:%@",self.fileURL);
    self.docController = [UIDocumentInteractionController interactionControllerWithURL:self.fileURL];
    self.docController.delegate = self;
    self.docController.UTI = [self UTIForURL:self.fileURL];
    BOOL sucess; // Sucess is true if it was possible to open the controller and there are apps available
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        sucess = [self.docController presentOpenInMenuFromRect:CGRectZero inView:self.superView animated:YES];
    } else {
        if(self.barButtonItem)
            sucess = [self.docController presentOpenInMenuFromBarButtonItem:self.barButtonItem animated:YES];
        else
        {
            // NSLog(@"framing");
            CGRect frame = self.rect;
            frame.origin.y += self.rect.size.height/2;
            sucess = [self.docController presentOpenInMenuFromRect:frame inView:self.superView animated:YES];
        }
    }
    
    if(!sucess){
        // There is no app to handle this file
        NSString *deviceType = [UIDevice currentDevice].localizedModel;
        NSString *message = [NSString stringWithFormat:NSLocalizedString(@"%@ sembra non avere Apps installate che possono aprire questo pdf.",
                                                                         @"Your %@ doesn't seem to have any other Apps installed that can open this document."), deviceType];
        
        // Display alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERRORE", @"No suitable App installed")
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
        
        // Inform app that the activity has finished
        // Return NO because the service was canceled and did not finish because of an error.
        // http://developer.apple.com/library/ios/#documentation/uikit/reference/UIActivity_Class/Reference/Reference.html
        [self activityDidFinish:NO];
    }
}

- (void)dismissDocumentInteractionControllerAnimated:(BOOL)animated {
    // Hide menu
    [self.docController dismissMenuAnimated:animated];
    
    // Inform app that the activity has finished
    [self activityDidFinish:NO];
}

#pragma mark - UIDocumentInteractionControllerDelegate

- (void) documentInteractionControllerWillPresentOpenInMenu:(UIDocumentInteractionController *)controller
{
    // Inform delegate
    if([self.delegate respondsToSelector:@selector(openInAppActivityWillPresentDocumentInteractionController:)]) {
        [self.delegate openInAppActivityWillPresentDocumentInteractionController:self];
    }
}

- (void) documentInteractionControllerDidDismissOpenInMenu: (UIDocumentInteractionController *) controller
{
    // Inform delegate
    if([self.delegate respondsToSelector:@selector(openInAppActivityDidDismissDocumentInteractionController:)]) {
        [self.delegate openInAppActivityDidDismissDocumentInteractionController:self];
    }
    
    // Inform app that the activity has finished
    [self activityDidFinish:YES];
}

@end

