//GlobalFunction.m

#import "GlobalFunction.h"
#import "Reachability.h"
#include <netinet/in.h>
#import <SystemConfiguration/SCNetworkReachability.h>

@implementation GlobalFunction

GlobalFunction *singletone = nil;

@synthesize  internetActive,hostActive;
@synthesize internetReachable, hostReachable, wifiReach;

- (id)init
{
    if(singletone==nil)
    {
    self = [super init];
    if (self) {
        // Initialization code here.
        singletone = self;
        singletone.internetReachable = [Reachability reachabilityForInternetConnection];
        [singletone.internetReachable startNotifier];
            }
        
    }
    return singletone;

    
    
}

- (void)reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    if([curReach isKindOfClass:[Reachability class]] && self.internetReachable == curReach)
        self.internetReachable = [note object];
}

- (BOOL)connectedToNetwork
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability =                                                                           SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr*)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    if (!didRetrieveFlags)
    {
        // NSLog(@"Error. Could not recover network reachability flags");
        return 0;
    }
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    //below suggested by Ariel
    BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
    NSURL *testURL = [NSURL URLWithString:@"http://www.iBreviary.com/"];
    //comment by friendlydeveloper: maybe use www.google.com
    NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:TIME_OUT_REQUEST];
    //NSURLConnection *testConnection = [[NSURLConnection alloc]     initWithRequest:testRequest delegate:nil]; //suggested by Ariel
    NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:nil];
    //modified by friendlydeveloper
    return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
    
}



//  Checking Internet Connectivity
+ (BOOL)checkNetworkStatus//:(NSNotification *)notice
{
    
    // NSLog(@"checkNetworkStatus");
    singletone = nil;
    singletone = [GlobalFunction new];
    BOOL risposta = NO;
    
    NetworkStatus internetStatus = [singletone.internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            // NSLog(@"The internet is down.");
            //[self ShowMsg:@"The internet connection appears to be offline."];
            risposta = NO;
            break;
            
        }
        case ReachableViaWiFi:
        {
            // NSLog(@"The internet is working via WIFI.");
            risposta = YES;
            
            break;
            
        }
        case ReachableViaWWAN:
        {
            // NSLog(@"The internet is working via WWAN.");
            risposta = YES;
            
            break;
            
        }
        default :
            risposta = YES;
            break;
            
    }
    
    singletone = nil;
    
    return risposta;
}

@end

