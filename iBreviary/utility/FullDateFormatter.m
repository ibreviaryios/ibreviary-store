//
//  FullDateFormatter.m
//  iBreviary
//
//  Created by Leonardo Parenti on 07/11/15.
//  Copyright © 2015 leyo. All rights reserved.
//

#import "FullDateFormatter.h"

@implementation FullDateFormatter

-(NSString *)stringFromDate:(NSDate *)date{
    NSString *r = [super stringFromDate:date];
//    NSLog(@"stringFromDate:%@",r);
//    NSDateFormatter *en = [NSDateFormatter new];
//    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
//    [en setLocale:locale];
//    //r = [en stringFromDate:date];
//    
//    r = [self convertNumberToInternation:r];
//    NSLog(@"stringFromDate:%@ <<",r);
    return r;
}

-(NSString*)convertNumberToInternation:(NSString*)n_s{
    NSString *s = @"";
    s = [n_s stringByReplacingOccurrencesOfString:@"۰" withString:@"0"];
    s = [s stringByReplacingOccurrencesOfString:@"۱" withString:@"1"];
    s = [s stringByReplacingOccurrencesOfString:@"۲" withString:@"2"];
    s = [s stringByReplacingOccurrencesOfString:@"۴" withString:@"3"];
    s = [s stringByReplacingOccurrencesOfString:@"۴" withString:@"4"];
    s = [s stringByReplacingOccurrencesOfString:@"۵" withString:@"5"];
    s = [s stringByReplacingOccurrencesOfString:@"۶" withString:@"6"];
    s = [s stringByReplacingOccurrencesOfString:@"۷" withString:@"7"];
    s = [s stringByReplacingOccurrencesOfString:@"۸" withString:@"8"];
    s = [s stringByReplacingOccurrencesOfString:@"۹" withString:@"9"];
    return s;
}

@end
