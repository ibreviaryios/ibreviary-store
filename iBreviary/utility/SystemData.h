//
//  SystemData.h
//  Valconca
//
//  Created by leyo on 22/01/13.
//  Copyright (c) 2013 leyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "BaseController.h"


@class NumeriObj,RestauriObj,PubblicazioniObj,News,Messa;

@interface UIColor (colorHTML2)

+(UIColor *)colorFromHexString:(NSString *)hexString;

@end

//===================
// DELEGATE PROTOCOL
//===================

@protocol NewsDownloadDelegate <NSObject>

- (void)news_updateImageForItem:(News*)item_news  atIndex:(int)index;

@end

//===================
// SYSTEMDATA
//===================
@interface SystemData : NSObject<MFMailComposeViewControllerDelegate> {
}

//optional delegate
@property (nonatomic, weak) BaseController <NewsDownloadDelegate> *news_delegate;


//variabili e Funzioni di Sistema
@property (readonly, nonatomic) NSFileManager *fileManager;
@property (readonly, nonatomic) NSString *documentDir;
@property (readonly, nonatomic) NSString *cacheDir;
@property (readonly, nonatomic) NSUserDefaults *defaults;
@property (readonly, nonatomic) NSString *version;
@property (readonly, nonatomic) BOOL connesso;
@property (readonly, nonatomic) NSString *oggi;

- (id)init;

//METODI DI SISTEMA
-(void)activityNetwork:(BOOL)b;
-(BOOL)fileExistsAtPath:(NSString*)path;


//AGGIORNAMENTO 2015
+(void)aggiornamento2015;


//==================
//Scroll VALUE
//==================
+(NSArray*)scrollValue;

//==================
//Audio Rate
//==================
+(NSArray*)audioRate;

//==================
//CHECK LAST UPDATE
//==================
+(void)checkLASTUPDATE;


//=========================
// VERSION & PLATFORM
//=========================
+(NSString *)versionApp;
+(NSString *)platformString;


@end
