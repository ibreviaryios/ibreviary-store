//
//  Categorie.m
//  iBreviary
//
//  Created by leyo on 09/02/14.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "Categorie.h"

//=======================
// NSObject GENERIC value
//=======================
#pragma mark NSObject
@implementation NSObject (supportBlur)
-(CGFloat)version{
    return [[UIDevice currentDevice].systemVersion floatValue];
}

- (BOOL)supportBlur
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *deviceName = [NSString stringWithCString:systemInfo.machine
                                              encoding:NSUTF8StringEncoding];
    
    NSSet *graphicsQuality = [NSSet setWithObjects:@"iPad",
                              @"iPad1,1",
                              @"iPhone1,1",
                              @"iPhone1,2",
                              @"iPhone2,1",
                              @"iPhone3,1",
                              @"iPhone3,2",
                              @"iPhone3,3",
                              @"iPod1,1",
                              @"iPod2,1",
                              @"iPod2,2",
                              @"iPod3,1",
                              @"iPod4,1",
                              @"iPad2,1",
                              @"iPad2,2",
                              @"iPad2,3",
                              @"iPad2,4",
                              @"iPad3,1",
                              @"iPad3,2",
                              @"iPad3,3",
                              nil];
    
    return ![graphicsQuality containsObject:deviceName] && SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8");
}

@end

//==================
// UIImage
//==================
#pragma mark UIImage

@implementation UIImage (customImage)



+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)filledImageFrom:(UIImage *)source withColor:(UIColor *)color{
    
    // begin a new image context, to draw our colored image onto with the right scale
    UIGraphicsBeginImageContextWithOptions(source.size, NO, [UIScreen mainScreen].scale);
    
    // get a reference to that context we created
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // set the fill color
    [color setFill];
    
    // translate/flip the graphics context (for transforming from CG* coords to UI* coords
    CGContextTranslateCTM(context, 0, source.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextSetBlendMode(context, kCGBlendModeColorBurn);
    CGRect rect = CGRectMake(0, 0, source.size.width, source.size.height);
    CGContextDrawImage(context, rect, source.CGImage);
    
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathFill);
    
    // generate a new UIImage from the graphics context we drew onto
    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return the color-burned image
    return coloredImg;
}

@end

//==================
// UIColor
//==================
#pragma mark UIColor

@implementation UIColor (colorHTML)


+(UIColor *)colorFromHexString:(NSString *)hexString {
    if(hexString.length<3) return [UIColor clearColor];
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    //LOG(@"colorFromHexString:%@",[UIColor colorWithRed: red green: green blue: blue alpha: alpha]);
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

+(CGFloat)colorComponentFrom:(NSString *) string start:(NSUInteger)start length:(NSUInteger)length{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

@end

static void(^alertActionOkBlock)(UIAlertAction*);
static void(^cancelActionBlock)(UIAlertAction*);
//===========================
// UIViewController
//===========================
#pragma mark UIViewController
@implementation UIViewController (Parent)


- (void)setParentViewController:(UIViewController*)parent{
    [self setValue:parent forKey:@"_parentViewController"];
}

#pragma mark -
#pragma mark ALERT
//===============================
// ALERT iOS8/iOS7
//===============================
-(void)alert:(NSString*)title
      andMex:(NSString*)mex
        icon:(UIImage*)icon
    okAction:(void(^)(UIAlertAction *action))actionBlockOk
  withCancel:(BOOL)cancel{
    

#ifdef __IPHONE_8_0
    if(self.version>7.9){
    //iOS8 or >
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:mex
                                          preferredStyle:UIAlertControllerStyleAlert];
    __block UIImageView* iconImageView;
    
    if(icon){
        iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, icon.size.width, icon.size.height)];
        
        [iconImageView setContentMode:UIViewContentModeScaleAspectFit];
        [iconImageView setImage:icon];
        [iconImageView setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin];
        [iconImageView setCenter:(CGPoint){270/2.f,2.f+(icon.size.height/2.0)}];
        [alertController.view addSubview:iconImageView];
    }
    
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:cancel?NSLocalizedString(@"risposta_si", nil):@"OK"
                               style:UIAlertActionStyleDefault
                               handler:actionBlockOk];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"risposta_annulla", nil)
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"annullato alert!! iOS8");
                                   }];
    
    
    if(cancel)[alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    if(alertController){
        [self presentViewController:alertController animated:YES completion:^{
            if(icon){
            }
            
        }];
    }
    }
    else
#endif
        
    if(self.version<8.f)
    {
    NSLog(@"ios minore o uguale a 7");
    //iOS7 or <
    alertActionOkBlock = actionBlockOk;
    UIAlertView *a = [[UIAlertView alloc] initWithTitle:title
                                                message:mex
                                               delegate:self
                                      cancelButtonTitle:cancel?NSLocalizedString(@"risposta_annulla",nil):nil otherButtonTitles:NSLocalizedString(@"risposta_si", nil), nil];
    [a setTag:1234567];
    [a show];
    }
}

  -(void)alert:(NSString*)title
        andMex:(NSString*)mex
optionOneTitle:(NSString*)o1_tit
     optionOne:(void(^)(UIAlertAction *action))actionBlockOk
optionTwoTitle:(NSString*)o2_tit
     optionTwo:(void(^)(UIAlertAction *action))cancelActionBlock{
    
#ifdef __IPHONE_8_0
    if(self.version>=8.f){
    
    //iOS8 or >
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:mex
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *optionOneAction = [UIAlertAction
                               actionWithTitle:o1_tit
                               style:UIAlertActionStyleDefault
                               handler:actionBlockOk];
    
    
    UIAlertAction *optionTwoAction = [UIAlertAction
                                   actionWithTitle:o2_tit
                                   style:UIAlertActionStyleCancel
                                   handler:cancelActionBlock];
    
    [alertController addAction:optionOneAction];

    [alertController addAction:optionTwoAction];

    
    [self presentViewController:alertController animated:YES completion:^{
    // NSLog(@"%@",[alertController.view subviews]);
    }];
    
    }
    else
#endif
    if(self.version<8.f){
    NSLog(@"ios minore o uguale a 7");
    //iOS7 or <
        alertActionOkBlock = actionBlockOk;
        cancelActionBlock = cancelActionBlock;

    UIAlertView *a = [[UIAlertView alloc] initWithTitle:title
                                                message:mex
                                               delegate:self
                                      cancelButtonTitle:o1_tit otherButtonTitles:o2_tit, nil];
    [a setTag:8901234];
    [a show];
    }
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    //ALERT HANDLER iOS7
    if (alertView.tag == 1234567) {
        if (buttonIndex == 0) {// OK Button
            // NSLog(@"annullato alert!! iOS7");
        }
        else if (buttonIndex == 1) {// 2nd Other Button
            alertActionOkBlock(nil);
        }
        
    }
    
    //Alert two choice NO cancel
    if (alertView.tag == 8901234) {
        if (buttonIndex == 0) {// OK Button
            alertActionOkBlock([UIAlertAction new]);
        }
        else if (buttonIndex == 1) {// 2nd Other Button
            cancelActionBlock(nil);
        }
        
    }
}

@end

//===================
// NSString
//===================
#pragma mark NSString
@implementation NSString (html)
+ (NSString *)stringFromInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if (orientation == UIInterfaceOrientationPortrait) return @"UIInterfaceOrientationPortrait";
    if (orientation ==  UIInterfaceOrientationPortraitUpsideDown) return @"UIInterfaceOrientationPortraitUpsideDown";
    if (orientation ==  UIInterfaceOrientationLandscapeLeft) return @"UIInterfaceOrientationLandscapeLeft";
    if (orientation ==  UIInterfaceOrientationLandscapeRight) return @"UIInterfaceOrientationLandscapeRight";
    return nil;
}

- (NSString *)gtm_stringByEscapingHTMLUsingTable:(HTMLEscapeMap*)table
                                          ofSize:(NSUInteger)size
                                 escapingUnicode:(BOOL)escapeUnicode {
    NSUInteger length = [self length];
    if (!length) {
        return self;
    }
    
    NSMutableString *finalString = [NSMutableString string];
    NSMutableData *data2 = [NSMutableData dataWithCapacity:sizeof(unichar) * length];
    
    // this block is common between GTMNSString+HTML and GTMNSString+XML but
    // it's so short that it isn't really worth trying to share.
    const unichar *buffer = CFStringGetCharactersPtr((CFStringRef)self);
    if (!buffer) {
        // We want this buffer to be autoreleased.
        NSMutableData *data = [NSMutableData dataWithLength:length * sizeof(UniChar)];
        if (!data) {
            // COV_NF_START - Memory fail case
            //LOG(@"couldn't alloc buffer");
            return nil;
            // COV_NF_END
        }
        [self getCharacters:[data mutableBytes]];
        buffer = [data bytes];
    }
    
    if (!buffer || !data2) {
        // COV_NF_START
        //LOG(@"Unable to allocate buffer or data2");
        return nil;
        // COV_NF_END
    }
    
    unichar *buffer2 = (unichar *)[data2 mutableBytes];
    
    NSUInteger buffer2Length = 0;
    
    for (NSUInteger i = 0; i < length; ++i) {
        HTMLEscapeMap *val = bsearch(&buffer[i], table,
                                     size / sizeof(HTMLEscapeMap),
                                     sizeof(HTMLEscapeMap), EscapeMapCompare);
        if (val || (escapeUnicode && buffer[i] > 127)) {
            if (buffer2Length) {
                CFStringAppendCharacters((CFMutableStringRef)finalString,
                                         buffer2,
                                         buffer2Length);
                buffer2Length = 0;
            }
            if (val) {
                [finalString appendString:val->escapeSequence];
            }
            else {
                if(escapeUnicode && buffer[i] > 127) //LOG(@"Illegal Character");
                [finalString appendFormat:@"&#%d;", buffer[i]];
            }
        } else {
            buffer2[buffer2Length] = buffer[i];
            buffer2Length += 1;
        }
    }
    if (buffer2Length) {
        CFStringAppendCharacters((CFMutableStringRef)finalString,
                                 buffer2,
                                 buffer2Length);
    }
    return finalString;
}

- (NSString *)gtm_stringByEscapingForHTML {
    return [self gtm_stringByEscapingHTMLUsingTable:gUnicodeHTMLEscapeMap
                                             ofSize:sizeof(gUnicodeHTMLEscapeMap)
                                    escapingUnicode:NO];
} // gtm_stringByEscapingHTML

- (NSString *)gtm_stringByEscapingForAsciiHTML {
    return [self gtm_stringByEscapingHTMLUsingTable:gAsciiHTMLEscapeMap
                                             ofSize:sizeof(gAsciiHTMLEscapeMap)
                                    escapingUnicode:YES];
} // gtm_stringByEscapingAsciiHTML

- (NSString *)gtm_stringByUnescapingFromHTML {
    NSRange range = NSMakeRange(0, [self length]);
    NSRange subrange = [self rangeOfString:@"&" options:NSBackwardsSearch range:range];
    
    // if no ampersands, we've got a quick way out
    if (subrange.length == 0) return self;
    NSMutableString *finalString = [NSMutableString stringWithString:self];
    do {
        NSRange semiColonRange = NSMakeRange(subrange.location, NSMaxRange(range) - subrange.location);
        semiColonRange = [self rangeOfString:@";" options:0 range:semiColonRange];
        range = NSMakeRange(0, subrange.location);
        // if we don't find a semicolon in the range, we don't have a sequence
        if (semiColonRange.location == NSNotFound) {
            continue;
        }
        NSRange escapeRange = NSMakeRange(subrange.location, semiColonRange.location - subrange.location + 1);
        NSString *escapeString = [self substringWithRange:escapeRange];
        NSUInteger length = [escapeString length];
        // a squence must be longer than 3 (&lt;) and less than 11 (&thetasym;)
        if (length > 3 && length < 11) {
            if ([escapeString characterAtIndex:1] == '#') {
                unichar char2 = [escapeString characterAtIndex:2];
                if (char2 == 'x' || char2 == 'X') {
                    // Hex escape squences &#xa3;
                    NSString *hexSequence = [escapeString substringWithRange:NSMakeRange(3, length - 4)];
                    NSScanner *scanner = [NSScanner scannerWithString:hexSequence];
                    unsigned value;
                    if ([scanner scanHexInt:&value] &&
                        value < USHRT_MAX &&
                        value > 0
                        && [scanner scanLocation] == length - 4) {
                        unichar uchar = value;
                        NSString *charString = [NSString stringWithCharacters:&uchar length:1];
                        [finalString replaceCharactersInRange:escapeRange withString:charString];
                    }
                    
                } else {
                    // Decimal Sequences &#123;
                    NSString *numberSequence = [escapeString substringWithRange:NSMakeRange(2, length - 3)];
                    NSScanner *scanner = [NSScanner scannerWithString:numberSequence];
                    int value;
                    if ([scanner scanInt:&value] &&
                        value < USHRT_MAX &&
                        value > 0
                        && [scanner scanLocation] == length - 3) {
                        unichar uchar = value;
                        NSString *charString = [NSString stringWithCharacters:&uchar length:1];
                        [finalString replaceCharactersInRange:escapeRange withString:charString];
                    }
                }
            } else {
                // "standard" sequences
                for (unsigned i = 0; i < sizeof(gAsciiHTMLEscapeMap) / sizeof(HTMLEscapeMap); ++i) {
                    if ([escapeString isEqualToString:gAsciiHTMLEscapeMap[i].escapeSequence]) {
                        [finalString replaceCharactersInRange:escapeRange withString:[NSString stringWithCharacters:&gAsciiHTMLEscapeMap[i].uchar length:1]];
                        break;
                    }
                }
            }
        }
    } while ((subrange = [self rangeOfString:@"&" options:NSBackwardsSearch range:range]).length != 0);
    return finalString;
} // gtm_stringByUnescapingHTML

-(NSString *)stringByRemovingHTMLtag {
    NSRange r;
    NSString *s = [self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(BOOL)isEmpty{
    NSString *s = [self copy];
    return (s==nil) || [s isEqualToString:@""];
}

-(BOOL)uguale:(NSString*)s{
    return [self isEqualToString:s];
}

@end

//==============================
// NSMutableURLRequest
//==============================
#pragma mark NSMutableURLRequest
@implementation NSMutableURLRequest (WebServiceClient)

+ (NSString *) encodeFormPostParameters: (NSDictionary *) parameters {
    NSMutableString *formPostParams = [[NSMutableString alloc] init];
    
    NSEnumerator *keys = [parameters keyEnumerator];
    
    NSString *name = [keys nextObject];
    while (nil != name) {
        NSString *encodedValue = ((NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef) [parameters objectForKey: name], NULL, CFSTR("=/:"), kCFStringEncodingUTF8)));
        
        [formPostParams appendString: name];
        [formPostParams appendString: @"="];
        [formPostParams appendString: encodedValue];
        name = [keys nextObject];
        
        if (nil != name) {
            [formPostParams appendString: @"&"];
        }
    }
    //LOG(@"formPost: %@",formPostParams);
    return formPostParams;
}

- (void)setFormPostParameters: (NSDictionary *) parameters
{
    NSString *jsonRequest = [NSMutableURLRequest encodeFormPostParameters:parameters];
    //LOG(@"jsonRequest is %@", jsonRequest);
    
    
    
    
    NSData *requestData = [jsonRequest dataUsingEncoding:NSUTF8StringEncoding];
    
    [self setHTTPMethod:@"POST"];
    [self setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [self setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [self setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [self setHTTPBody:requestData];
    
    
    //LOG(@"fine FormPost:%@",self);
}

@end


//=================
// NSDate
//=================
#pragma mark NSDate
@implementation NSDate(Utils)

-(NSDate *) toLocalTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

-(NSDate *) toGlobalTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}
@end

//==================
// NSArray
//==================
#pragma mark NSArray
@implementation NSArray (Shuffling)

- (NSArray*)shuffle
{
    NSMutableArray* tmp = [NSMutableArray arrayWithArray:self];
    
    NSUInteger count = [tmp count];
    for (NSUInteger i = 0; i < count; ++i)
    {
        // Select a random element between i and end of array to swap with.
        NSInteger nElements = count - i;
        NSInteger n = (arc4random() % nElements) + i;
        [tmp exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    return [NSArray arrayWithArray:tmp];
}
@end


//==================
// UILabel
//==================
#pragma mark UILabel
@implementation UILabel (letterByLetter)

- (void)animateText:(NSString*)string
{
    [self setText:@""];
    if(string == nil || [string isEqualToString:@""])
    {
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:string forKey:@"string"];
    [dict setObject:@0 forKey:@"currentCount"];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(typingLabel:) userInfo:dict repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

-(void)typingLabel:(NSTimer*)theTimer
{
    NSString *theString = [theTimer.userInfo objectForKey:@"string"];
    int currentCount = [[theTimer.userInfo objectForKey:@"currentCount"] intValue];
    currentCount ++;
    // NSLog(@"typingLabel:%@",[theString substringToIndex:currentCount]);
    
    [theTimer.userInfo setObject:[NSNumber numberWithInt:currentCount] forKey:@"currentCount"];
    
    if (currentCount > theString.length-1) {
        [theTimer invalidate];
    }
    
    [self setText:[theString substringToIndex:currentCount]];
}

- (UILabel *)initWithLabel{
    NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:self];
    return (UILabel *)[NSKeyedUnarchiver unarchiveObjectWithData:archivedData];
}



@end

//==================
// UIWebView
//==================
#pragma mark UIWebView
@implementation UIWebView (customWebFunction)
- (NSString *)selectedText {
    return [self stringByEvaluatingJavaScriptFromString:@"window.getSelection().toString()"];
}

- (NSString *)selectedHTMLText {
     return [self stringByEvaluatingJavaScriptFromString:@"getSelectionHtml()"];
}

@end

//==================
// UIView
//==================
#pragma mark UIView
@implementation UIView (UpdateAutoLayoutConstraints)
- (BOOL) setConstraintConstant:(CGFloat)constant forAttribute:(NSLayoutAttribute)attribute
{
    NSLayoutConstraint * constraint = [self constraintForAttribute:attribute];
    if(constraint)
    {
        [constraint setConstant:constant];
        return YES;
    }else
    {
        [self.superview addConstraint: [NSLayoutConstraint constraintWithItem:self attribute:attribute relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:constant]];
        return NO;
    }
}
- (CGFloat) constraintConstantforAttribute:(NSLayoutAttribute)attribute
{
    NSLayoutConstraint * constraint = [self constraintForAttribute:attribute];
    if (constraint) {
        return constraint.constant;
    }else
    {
        return NAN;
    }
}
- (NSLayoutConstraint*) constraintForAttribute:(NSLayoutAttribute)attribute
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstAttribute = %d && firstItem = %@", attribute, self];
    NSArray *fillteredArray = [[self.superview constraints] filteredArrayUsingPredicate:predicate];
    if(fillteredArray.count == 0)
    {
        return nil;
    }else
    {
        return fillteredArray.firstObject;
    }
}
- (void)hideByHeight:(BOOL)hidden
{
    [self hideView:hidden byAttribute:NSLayoutAttributeHeight];
}
- (void)hideByWidth:(BOOL)hidden
{
    [self hideView:hidden byAttribute:NSLayoutAttributeWidth];
}
- (void)hideView:(BOOL)hidden byAttribute:(NSLayoutAttribute)attribute
{
    if (self.hidden != hidden) {
        CGFloat constraintConstant = [self constraintConstantforAttribute:attribute];
        if (hidden)
        {
            if (!isnan(constraintConstant)) {
                self.alpha = constraintConstant;
            }else
            {
                CGSize size = [self getSize];
                self.alpha = (attribute == NSLayoutAttributeHeight)?size.height:size.width;
            }
            [self setConstraintConstant:0 forAttribute:attribute];
            self.hidden = YES;
        }else
        {
            if (!isnan(constraintConstant) ) {
                self.hidden = NO;
                [self setConstraintConstant:self.alpha forAttribute:attribute];
                self.alpha = 1;
            }
        }
    }
}

-(void)applyFadeTop:(BOOL)top bottom:(BOOL)bottom andPercentage:(NSNumber*)p
{
    if(!top && !bottom) return;
    if(!p)p = @0.2;
    
    CAGradientLayer *maskLayer = [CAGradientLayer layer];
    
    CGColorRef outerColor = [UIColor colorWithWhite:1.0 alpha:0.0].CGColor;
    CGColorRef innerColor = [UIColor colorWithWhite:1.0 alpha:1.0].CGColor;
    
    [maskLayer setColors:[NSArray arrayWithObjects:
                          (__bridge id)outerColor,
                          (__bridge id)innerColor,
                          (__bridge id)innerColor,
                          (__bridge id)outerColor,
                          nil]];
    [maskLayer setLocations:[NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.0],
                             [NSNumber numberWithFloat:top?p.floatValue:0.0],
                             [NSNumber numberWithFloat:bottom?(1.0-p.floatValue):1],
                             [NSNumber numberWithFloat:1.0],
                             nil
                             ]
     ];
    
    [maskLayer setBounds:self.bounds];
    [maskLayer setAnchorPoint:CGPointZero];
    self.layer.mask = maskLayer;
}

- (CGSize) getSize
{
    [self updateSizes];
    return CGSizeMake(self.bounds.size.width, self.bounds.size.height);
}
- (void)updateSizes
{
    [self setNeedsLayout];
    [self layoutIfNeeded];
}
- (void)sizeToSubviews
{
    [self updateSizes];
    CGSize fittingSize = [self systemLayoutSizeFittingSize: UILayoutFittingCompressedSize];
    self.frame = CGRectMake(0, 0, 320, fittingSize.height);
}

-(void)addShineAnimationWithDuration:(CGFloat)duration
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    [gradient setStartPoint:CGPointMake(0, 1)];
    [gradient setEndPoint:CGPointMake(1, 0)];
    gradient.frame = CGRectMake(0, 0, self.bounds.size.width*3, self.bounds.size.height);
    float lowerAlpha = 0.78;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[[UIColor colorWithWhite:1 alpha:lowerAlpha] CGColor],
                       (id)[[UIColor colorWithWhite:1 alpha:lowerAlpha] CGColor],
                       (id)[[UIColor colorWithWhite:1 alpha:1.0] CGColor],
                       (id)[[UIColor colorWithWhite:1 alpha:1.0] CGColor],
                       (id)[[UIColor colorWithWhite:1 alpha:1.0] CGColor],
                       (id)[[UIColor colorWithWhite:1 alpha:lowerAlpha] CGColor],
                       (id)[[UIColor colorWithWhite:1 alpha:lowerAlpha] CGColor],
                       nil];
    gradient.locations = [NSArray arrayWithObjects:
                          [NSNumber numberWithFloat:0.0],
                          [NSNumber numberWithFloat:0.4],
                          [NSNumber numberWithFloat:0.45],
                          [NSNumber numberWithFloat:0.5],
                          [NSNumber numberWithFloat:0.55],
                          [NSNumber numberWithFloat:0.6],
                          [NSNumber numberWithFloat:1.0],
                          nil];
    
    CABasicAnimation *theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
    theAnimation.duration = duration;
    theAnimation.repeatCount = 1;
    theAnimation.autoreverses = NO;
    theAnimation.removedOnCompletion = YES;
    theAnimation.fillMode = kCAFillModeForwards;
    theAnimation.fromValue=[NSNumber numberWithFloat:-self.frame.size.height*2];
    theAnimation.toValue=[NSNumber numberWithFloat:0];
    [gradient addAnimation:theAnimation forKey:@"animateLayer"];
    
    self.layer.mask = gradient;
}

@end

#pragma mark UIButton
//===========================
// UIButton
//===========================
@implementation UIButton (tintColor)

-(void)setImage:(UIImage*)image withTintColor:(UIColor*)tintColor{
    UIImage* imageForRendering = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self setImage:imageForRendering forState:UIControlStateNormal];
    [self setTintColor:tintColor];
}


-(void)setHighlightImage:(UIImage*)image withTintColor:(UIColor*)tintColor{
    UIImage* imageForRendering = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self setImage:imageForRendering forState:UIControlStateHighlighted];
    [self setTintColor:tintColor];
}

-(void)rotateButtonImage{
    
    UIView *superview = self.superview;
    
    [UIView transitionWithView: self
                      duration: 0.7
                       options: UIViewAnimationOptionTransitionFlipFromLeft
                    animations: ^{
                        [self removeFromSuperview];
                        UIImage *image = [self imageForState:UIControlStateNormal];
                        UIImage* flippedImage = [UIImage imageWithCGImage:image.CGImage
                                                                    scale:image.scale
                                                              orientation:UIImageOrientationUpMirrored];
                        [self setImage:flippedImage forState:UIControlStateNormal];
                        [superview addSubview: self]; }
                    completion:^(BOOL finished){
                        // NSLog(@"Fine");
                    }];
}


@end

