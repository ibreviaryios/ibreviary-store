//
//  UITextViewMax.h
//  iBreviary
//
//  Created by leyo on 12/11/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextViewMax : UITextView

@property (nonatomic) CGFloat maxHeight;
@property (nonatomic) CGFloat minHeight;
// TODO:
//@property(nonatomic) UIControlContentVerticalAlignment verticalAlignment;
@end
