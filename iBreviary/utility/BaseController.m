//
//  BaseController.m
//  ScheletroAPP
//
//  Created by ileyo Parenti on 1/08/12.
//  Copyright (c) 2012 NetGuru All rights reserved.
//

#import "BaseController.h"
#import "Reachability.h"
#import "GlobalFunction.h"
#include <netinet/in.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import <objc/runtime.h>
#import "Home.h"



#define TUTORIAL_MODALITA_BOOK @"TUTORIAL_MODALITA_BOOKCustom"

@interface BaseController()<CustomIOSAlertViewDelegate>

@property (strong, nonatomic) Reachability *internetReachability;

@end


#define STATUS_BAR_HEIGHT 22.0f
#define STATUS_BAR_ANIMATION_LENGTH 0.25f
#define FONT_SIZE 12.0f
#define FONT_SIZE_IPAD 18.0f
#define FONT_SIZE_FAQ 8.0f
#define FONT_SIZE_FAQ_IPAD 10.0f
@implementation BaseController

//NAV-BAR
@synthesize sfondo_nav, btn_back, btn_home;

@synthesize internetReachability;

NSString const *CWStatusBarIsHiddenKey = @"CWStatusBarIsHiddenKey";
NSString const *CWStatusBarNotificationIsShowingKey = @"CWStatusBarNotificationIsShowingKey";
NSString const *CWStatusBarNotificationLabelKey = @"CWStatusBarNotificationLabelKey";

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_5_IOS8 (IS_IPHONE && ([[UIScreen mainScreen] nativeBounds].size.height/[[UIScreen mainScreen] nativeScale]) == 568.0f)
#define IS_IPHONE_6_IOS8 (IS_IPHONE && ([[UIScreen mainScreen] nativeBounds].size.height/[[UIScreen mainScreen] nativeScale]) == 667.0f)
#define IS_IPHONE_6P_IOS8 (IS_IPHONE && ([[UIScreen mainScreen] nativeBounds].size.height/[[UIScreen mainScreen] nativeScale]) == 736.0f)

#pragma mark -
#pragma mark VARIABILI DI SISTEMA
//=========================
// VARIABILI DI SISTEMA
//=========================
-(BOOL)isIpad{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}
-(BOOL)isIphone{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
}

-(BOOL)isIphone5{
#ifdef __IPHONE_8_0
    if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
        return IS_IPHONE_5_IOS8 || IS_IPHONE_6_IOS8 || IS_IPHONE_6P_IOS8;
#endif
    return (self.isIphone && [[UIScreen mainScreen] bounds].size.height >= 568);
    
}

-(BOOL)isLand{
    return UIInterfaceOrientationIsLandscape([self interfaceOrientation]);
}

-(BOOL)isPort{
    return UIInterfaceOrientationIsPortrait([self interfaceOrientation]);
}

-(BOOL)isRetina{
    return ([[UIScreen mainScreen] scale]>=2);
}

-(BOOL)isIos7orUpper{
    return [[[UIDevice currentDevice] systemVersion] floatValue]>=7;
}

+(BOOL)isIos7orUpper{
    return [[[UIDevice currentDevice] systemVersion] floatValue]>=7;
}

-(NSUserDefaults*)defaults{
    NSUserDefaults *sharedDefaults = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    return sharedDefaults;
}

-(NSString*)iBreviary_version{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
}

-(NSFileManager*)fileManager{
    return [NSFileManager defaultManager];
}

-(BOOL)fileExistsAtPath:(NSString*)path{
    return [self.fileManager fileExistsAtPath:path];
}

//CREA CARTELLA
-(BOOL)creaCartellaAtPath:(NSString*)p error:(NSError **)e{
    
    if([self fileExistsAtPath:p]) {NSLog(@"La cartella %@ esiste già",p); return NO;}
    // Check degli errori nella creazione cartelle
    return [[NSFileManager defaultManager] createDirectoryAtPath:[self.cacheDir stringByAppendingPathComponent:p] withIntermediateDirectories:YES attributes:nil error:e];
}

//DOCUMENT DIRECTORY
- (NSString *)documentDir {
    NSArray* documentPathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* docPath = [documentPathArray lastObject];
    // NSLog(@"DocDir:%@",docPath);
    return docPath;
}

//CACHE DIRECTORY
- (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}

-(NSArray*)contentsOfDirectoryAtPath:(NSString*)path{
    NSURL *doc = [NSURL URLWithString:path];
    NSArray * pContents =
    [[NSFileManager defaultManager] contentsOfDirectoryAtURL:doc
                                  includingPropertiesForKeys:@[]
                                                     options:NSDirectoryEnumerationSkipsHiddenFiles
                                                       error:nil];
    NSLog(@"contentsOfDirectoryAtPath:%@",pContents);
    return pContents;
}

-(BOOL)connesso{
    BOOL i = [GlobalFunction checkNetworkStatus];
    // NSLog(@"connesso:%i",i);
    return i;
}


/*
-(BOOL)connesso{
    return [GlobalFunction checkNetworkStatus];
    NetworkStatus netStatus = [self.internetReachability currentReachabilityStatus];
    // NSLog(@"checkCONNESSO:%i",(int)netStatus);
    switch (netStatus) {
        case ReachableViaWiFi:
            // NSLog(@"ReachableViaWiFi");
            break;
        case NotReachable:
            // NSLog(@"NotReachable");
            break;
        case ReachableViaWWAN:
            // NSLog(@"ReachableViaWWAN");
            break;
            
        default:
            break;
    }
    return (netStatus!=NotReachable);
}
*/

#pragma mark -
#pragma mark init
//=========================
// INIT
//=========================

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom INIT WITH NIB NAME
    }
    return self;
}

- (id)init{
    self = [super init];
    if (self) {
        // Custom INIT
    }
    return self;
}

- (void) loadXibWithNibName:(NSString *)nibName
{
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    self.view = aView;
}

#pragma mark -
#pragma mark viewDidLoad
//=========================
// DIDLOAD
//=========================

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //APP DELEGATE
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noNet) name:NOT_NONET object:nil];
    
}

-(IBAction)toggleBOOKPdf:(UIButton*)sender{
    BOOL toggle = [Home toggleBook];
    sender.selected = toggle;
}

-(void)titleOnNavBar:(NSString*)tit_string{
    //Titolo frame
    CGFloat height = IS_IPAD?18:16;
    CGFloat width = 250;
    CGFloat origin_x = 0;
    CGFloat origin_y = self.sfondo_nav.frame.size.height+5.;
    
//    if(self.bookBtnToggle){
//        width = self.bookBtnToggle.superview.frame.origin.x - origin_x - 5.;
//    }
    
    CGRect frame = (CGRect){origin_x, origin_y, width, height};
    //NSLog(@"frame:%@",NSStringFromCGRect(frame));
    UILabel *label_title = [[UILabel alloc] initWithFrame:frame];
    [label_title setBackgroundColor:[UIColor clearColor]];
    [label_title setFont: [UIFont fontWithName:FONT_HELVETICANEUE_CONDENSEDBLACK size:height]];
    [label_title setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.60]];
    [label_title setTextAlignment:NSTextAlignmentRight];
    [label_title setText:[tit_string uppercaseString]];

    [self.sfondo_nav addSubview:label_title];
        
}

-(void)titleOnNavBar2:(NSString*)tit_string{
    //Titolo frame
    CGFloat height = IS_IPAD?18:16;
    CGRect frame = self.sfondo_nav.bounds;
    frame.origin.y = 18.f;
    frame.size.height -= frame.origin.y;
    UILabel *label_title = [[UILabel alloc] initWithFrame:frame];
    //[label_title setBackgroundColor:[[UIColor redColor] colorWithAlphaComponent:0.1]];
    [label_title setFont: [UIFont fontWithName:FONT_HELVETICANEUE_CONDENSEDBLACK size:height]];
    [label_title setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.40]];
    [label_title setTextAlignment:NSTextAlignmentCenter];
    [label_title setNumberOfLines:0];
    [label_title setText:[tit_string uppercaseString]];
    
    [self.sfondo_nav addSubview:label_title];
    
    return;
    
    //Titolo
    UILabel *capo = [[UILabel alloc] initWithFrame:CGRectMake(0, -2, 44, 44)];
    [capo setBackgroundColor:[UIColor clearColor]];
    [capo setFont: [UIFont fontWithName:FONT_EILEENCAPS size: 30]];
    [capo setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.6]];
    capo.shadowOffset = CGSizeMake(1,2);
    [capo setTextAlignment:NSTextAlignmentLeft];
    [capo setText:[tit_string substringWithRange:NSMakeRange(0,1)]];
    
    UILabel *resto = [[UILabel alloc] initWithFrame:CGRectMake(26, 12, 320, 24)];
    resto.backgroundColor = [UIColor clearColor];
    [resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size: 20]];
    resto.textAlignment = NSTextAlignmentLeft;
    [resto setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.65]];
    
    resto.lineBreakMode = NSLineBreakByWordWrapping;
    resto.numberOfLines = 1;
    [resto setShadowColor:capo.shadowColor];
    resto.shadowOffset = CGSizeMake(0,2);
    
    resto.text = [tit_string substringFromIndex:(NSUInteger)1];
    
    CGSize expectedLabelSize;
    CGRect b  = [resto.text boundingRectWithSize:resto.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:resto.font} context:nil];
    
    expectedLabelSize = b.size;
    
    UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, expectedLabelSize.width+resto.frame.origin.x, 44)];
    Label.layer.transform = CATransform3DMakeScale(0.85, 0.85, 1.0);
    [Label setClipsToBounds:NO];
    UIView* back = [[UIView alloc ] initWithFrame:CGRectMake(-15, 7, expectedLabelSize.width+resto.frame.origin.x+30, 30)];
    [back setBackgroundColor:[UIColorFromRGB(0X422c20) colorWithAlphaComponent:0.1]];
    [Label setCenter:CGPointMake(130,43)];
    //[Label addSubview:back];
    [Label addSubview:capo];
    [Label addSubview:resto];
    
    if([NSLocalizedString(@"checklingua",nil) isEqualToString:@"ar"])
    {
        [resto setText:tit_string];
        [capo setText:@""];
        [Label setFrame:CGRectMake(0, 0, 200, Label.frame.size.height)];
        [Label setCenter:CGPointMake(130,43)];
        [resto setTextAlignment:NSTextAlignmentCenter];
        [resto setFont: [UIFont fontWithName:FONT_HOEFLER_ITALIC size: 25]];
        [resto setFrame:CGRectMake(0, 0, Label.frame.size.width, Label.frame.size.height)];
        
    }
    
    
    [sfondo_nav addSubview:Label];

}

#pragma mark -
#pragma mark ROTATE IMAGE
//===============================
//ROTATE IMAGE
//===============================
-(void)rotateImage:(UIView*)image withDuration:(float)duration onY_axis:(BOOL)isYaxis{
    [UIView animateWithDuration:duration/2.0 animations:^{image.transform = isYaxis?
        CGAffineTransformMake(image.transform.a * -1, 0, 0, 1, image.transform.tx, 0)
        :CGAffineTransformMake(1, 0, 0, image.transform.d * -1, 0, image.transform.ty);}
                     completion:^(BOOL finished)
     {
         //half animation
         [UIView animateWithDuration:duration/2.0 animations:^{image.transform = isYaxis?
             CGAffineTransformMake(image.transform.a * -1, 0, 0, 1, image.transform.tx, 0)
             :CGAffineTransformMake(1, 0, 0, image.transform.d * -1, 0, image.transform.ty);}
                          completion:^(BOOL finished)    { }];
     }];
}

CGRect cardFrame;

-(void)cardRotation:(UIButton*)source destination:(UIImage*)change{
    
    UIView *superview = source.superview;

    [UIView transitionWithView: source
                      duration: 0.7
                       options: UIViewAnimationOptionTransitionFlipFromLeft
                    animations: ^{
                                 [source removeFromSuperview];
                                 [source setImage:change forState:UIControlStateNormal];
                                 [superview addSubview: source]; }
                    completion:^(BOOL finished){
                                // NSLog(@"Fine");
                                }];
}

-(void)fineCardRotation{
        }

#pragma mark -
#pragma mark NOTIFICHE LOCALIZZATE STATUSBAR
//===============================
//NOTIFICHE LOCALIZZATE STATUSBAR
//===============================
//icloud
-(void)mostraICLoudUpdate{
    // NSLog(@"mostraNotificaTuttoOk--!");
    runOnMainQueueWithoutDeadlocking(^{
        [self mostraICLoudUpdateMM];
    });
}

-(void)mostraLocalNotificaBanner:(UILocalNotification*)ln{
    // NSLog(@"mostraLocalNotifica");
    runOnMainQueueWithoutDeadlocking(^{
        [self mostraLocalNotificaMM:ln];
    });
}

-(void)mostraNotificaAggiornamento2015_ok{
    // NSLog(@"mostraNotificaTuttoOk--!");
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"showedAggiornamento2015"]){
        [[NSUserDefaults standardUserDefaults] setObject:@1 forKey:@"showedAggiornamento2015"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [NSThread sleepForTimeInterval:0.1];
        
        runOnMainQueueWithoutDeadlocking(^{
            [self mostraNotificaAggiornamento2015_okMM];
        });
    }
}

-(void)mostraNotificaDayDownloaded:(NSString*)day{
    // NSLog(@"mostraNotificaDayDownloaded->%@",day);
    if(day==nil) {[self noNet]; return;}
    runOnMainQueueWithoutDeadlocking(^{
        [self mostraNotificaDayDownloadedMM:day];
    });
}

-(void)mostraNotificaError:(NSError*)errore{
    // NSLog(@"mostraNotificaError->%@",[errore localizedDescription]);
    runOnMainQueueWithoutDeadlocking(^{
        [self mostraNotificaErrorMM:errore];
    });
}

-(void)mostraNotificaWeekDownloaded{
    // NSLog(@"mostraNotificaWeekDownloaded");
    runOnMainQueueWithoutDeadlocking(^{
        [self mostraNotificaWeekDownloadedMM];
    });
}

-(void)mostraNotificaNoUpdate{
    // NSLog(@"mostraNotificaNoUpdate--!");
    runOnMainQueueWithoutDeadlocking(^{
        [self mostraNotificaNoUpdateMain];
    });
}

-(void)mostraNotificaDayERROR_INCOMPLETE:(NSString*)day{
    runOnMainQueueWithoutDeadlocking(^{
        [self mostraNotificaDayERROR_INCOMPLETEMM:day];
    });
}

-(void)mostraNotificaWeekERROR_INCOMPLETE:(NSString*)risposta{
    runOnMainQueueWithoutDeadlocking(^{
        [self mostraNotificaWeekERROR_INCOMPLETEMM:risposta];
    });
}

-(void)noNet{
    runOnMainQueueWithoutDeadlocking(^{
        [self mostraNotificaNoConnessioneMAIN];
    });
}

-(void)noMessaNet{
    runOnMainQueueWithoutDeadlocking(^{
        [self mostraNotificaNoMessaNetMAIN];
    });
}

//
-(void)noMessaNetIOS6{
    [[[UIAlertView alloc] initWithTitle:@"ERROR" message:NSLocalizedString(@"createMessaNoNet", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}


-(void)noNetIOS6{
    [[[UIAlertView alloc] initWithTitle:@"ERROR" message:NSLocalizedString(NOT_STATUS_NO_NET, nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

#pragma mark main method

-(void)mostraICLoudUpdateMM{
    
    UIImage *logo= [UIImage imageNamed:@"icon_icloud.png"];
    NSString *localizzata = NSLocalizedString(NOT_ICLOUD_UPDATE_OK, nil);
    NSDictionary *options = @{
                              kCRToastTimeIntervalKey   : @(1.5f),
                              kCRToastTextKey :localizzata,
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColorFromRGB(0X3470f3) colorWithAlphaComponent:0.9],
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastUnderStatusBarKey : @(NO),
                              kCRToastImageKey :  logo,
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastFontKey:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE],
                              };
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"Completed");
                                }];
    
}

-(void)mostraLocalNotificaMM:(UILocalNotification*)ln{
    // NSLog(@"mostraLocalNotifica");
    UIImage *logo= [UIImage imageNamed:@"Icon-57.png"];
    NSString *localizzata = ln.alertBody;
        NSArray *interactionResponders = @[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeAll automaticallyDismiss:NO block:^(CRToastInteractionType interactionType) {
                    // NSLog(@"Premuta notifica da Banner CRToast");
                    [Home mostraNotifca:ln];
                    //Respond to interaction
    }]];
    
    NSDictionary *options = @{
                              kCRToastInteractionRespondersKey : interactionResponders,
                              kCRToastTimeIntervalKey   : @(2.5f),
                              kCRToastTextKey :localizzata,
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColorFromRGB(0X4c3629) colorWithAlphaComponent:0.9],
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastUnderStatusBarKey : @(NO),
                              kCRToastImageKey :  logo,
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastFontKey:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE],
                              kCRToastSubtitleTextKey:[NSLocalizedString(@"NOT_PREMI_MEX", nil) uppercaseString],
                              kCRToastSubtitleFontKey:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_FAQ_IPAD:FONT_SIZE_FAQ],
                              kCRToastSubtitleTextColorKey:[[UIColor whiteColor] colorWithAlphaComponent:0.7]
                              };


    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"Completed");
                                }];

}

-(void)mostraNotificaAggiornamento2015_okMM{
    
    UIImage *logo= [UIImage imageNamed:@"infoW.png"];
    NSString *localizzata = NSLocalizedString(NOT_STATUS_UPDATE2014_OK, nil);    
    NSDictionary *options = @{
                              kCRToastTimeIntervalKey   : @(1.5f),
                              kCRToastTextKey :localizzata,
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColorFromRGB(0X4c3629) colorWithAlphaComponent:0.9],
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastUnderStatusBarKey : @(NO),
                              kCRToastImageKey :  logo,
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastFontKey:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE],
                              };
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"Completed");
                                }];
    
}

-(void)mostraNotificaErrorMM:(NSError*)errore{
    // NSLog(@"mostraNotificaErrorMM");
    UIImage *logo= [UIImage imageNamed:@"errorDesc.png"];
    NSDictionary *options = @{
                              kCRToastTimeIntervalKey   : @(1.5f),
                              kCRToastTextKey :[[errore localizedDescription] uppercaseString],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColorFromRGB(0Xab111f) colorWithAlphaComponent:0.8],
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastUnderStatusBarKey : @(NO),
                              kCRToastImageKey :  logo,
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastFontKey:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE],
                              };
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"mostraNotificaError - Completed");
                                }];

}

-(void)mostraNotificaDayDownloadedMM:(NSString*)day{
    // NSLog(@"mostraNotificaNoNetMain:%@",day);
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"d/M/yy"];
    NSDate *d = [dateFormatter dateFromString:day];
    // NSLog(@"d:%@",d);
	[dateFormatter setDateFormat:@"EEEE d MMMM"];
    // NSLog(@"mostraNotificaNoNetMain2:%@",[dateFormatter stringFromDate:d]);

    NSString *localizzaDayOk = NSLocalizedString(NOT_STATUS_DAY_OK, nil);
    UIImage *logo= [UIImage imageNamed:@"okW.png"];
    
    
    NSMutableDictionary *options = [NSMutableDictionary new];
    [options setObject:@(1.5f) forKey:kCRToastTimeIntervalKey];
    [options setObject:[[NSString stringWithFormat:localizzaDayOk,[dateFormatter stringFromDate:d]] uppercaseString] forKey:kCRToastTextKey];
    [options setObject:@(NSTextAlignmentCenter) forKey:kCRToastTextAlignmentKey];
    [options setObject:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE] forKey:kCRToastFontKey];
    [options setObject:[UIColorFromRGB(0X558e32) colorWithAlphaComponent:0.9] forKey:kCRToastBackgroundColorKey];
    [options setObject:@(CRToastPresentationTypeCover) forKey:kCRToastNotificationPresentationTypeKey];
    [options setObject:@(CRToastTypeNavigationBar) forKey:kCRToastNotificationTypeKey];
    [options setObject:@(NO) forKey:kCRToastUnderStatusBarKey];
    [options setObject:logo forKey:kCRToastImageKey];
    [options setObject:@(CRToastAnimationTypeLinear) forKey:kCRToastAnimationInTypeKey];
    [options setObject:@(CRToastAnimationTypeGravity) forKey:kCRToastAnimationOutTypeKey];
    [options setObject:@(CRToastAnimationDirectionTop) forKey:kCRToastAnimationInDirectionKey];
    [options setObject:@(CRToastAnimationDirectionTop) forKey:kCRToastAnimationOutDirectionKey];

    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"Completed");
                                }];

    
    //[self showStatusBarNotification: forDuration:1.5 withTextColor:[UIColor whiteColor] andBackColor:];
}

-(void)mostraNotificaWeekDownloadedMM{
    // NSLog(@"mostraNotificaWeekDownloaded-Main");
    UIImage *logo= [UIImage imageNamed:@"okW.png"];
    NSDictionary *options = @{
                              kCRToastTimeIntervalKey   : @(1.5f),
                              kCRToastTextKey :[NSLocalizedString(NOT_STATUS_WEEK_OK, nil) uppercaseString],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColorFromRGB(0X558e32) colorWithAlphaComponent:0.9],
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastUnderStatusBarKey : @(NO),
                              kCRToastImageKey :  logo,
                              kCRToastFontKey:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              };
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"Completed");
                                }];
}

-(void)mostraNotificaNoUpdateMain{
    // NSLog(@"mostraNotificaNoUpdateMain");
    NSDictionary *options = @{
                              kCRToastTimeIntervalKey   : @(2.0f),
                              kCRToastTextKey :[NSLocalizedString(NOT_STATUS_UPDATE2014_OK ,nil) uppercaseString],
                              kCRToastFontKey:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColorFromRGB(0X002458) colorWithAlphaComponent:0.9],
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastUnderStatusBarKey : @(NO),
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              };
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"Completed");
                                }];
}


-(void)mostraNotificaDayERROR_INCOMPLETEMM:(NSString*)day{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"d/M/yy"];
    NSDate *d = [dateFormatter dateFromString:day];
	[dateFormatter setDateFormat:@"EEEE d MMMM"];
    
    NSString *localizzaDayNO = [NSLocalizedString(NOT_STATUS_DAY_NO, nil) uppercaseString];
    UIImage *logo= [UIImage imageNamed:@"erroW.png"];

    
    NSMutableDictionary *options = [NSMutableDictionary new];
    [options setObject:@(2.5f) forKey:kCRToastTimeIntervalKey];
    [options setObject:[[NSString stringWithFormat:localizzaDayNO,[dateFormatter stringFromDate:d]] uppercaseString] forKey:kCRToastTextKey];
    [options setObject:@(NSTextAlignmentCenter) forKey:kCRToastTextAlignmentKey];
    [options setObject:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:12] forKey:kCRToastFontKey];
    [options setObject:[UIColorFromRGB(0Xab111f) colorWithAlphaComponent:0.9] forKey:kCRToastBackgroundColorKey];
    [options setObject:@(CRToastPresentationTypeCover) forKey:kCRToastNotificationPresentationTypeKey];
    [options setObject:@(CRToastTypeNavigationBar) forKey:kCRToastNotificationTypeKey];
    [options setObject:@(NO) forKey:kCRToastUnderStatusBarKey];
    [options setObject:logo forKey:kCRToastImageKey];
    [options setObject:@(CRToastAnimationTypeLinear) forKey:kCRToastAnimationInTypeKey];
    [options setObject:@(CRToastAnimationTypeGravity) forKey:kCRToastAnimationOutTypeKey];
    [options setObject:@(CRToastAnimationDirectionTop) forKey:kCRToastAnimationInDirectionKey];
    [options setObject:@(CRToastAnimationDirectionTop) forKey:kCRToastAnimationOutDirectionKey];
    [options setObject:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE] forKey:kCRToastFontKey];

    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"Completed");
                                }];
}

-(void)mostraNotificaNoSanto{
    //"shortcut_no_suggest" = "Nessun Preghiera suggerita";
    NSString *text = [NSLocalizedString(@"shortcut_no_santo", nil) uppercaseString];
    UIImage *logo= [UIImage imageNamed:@"erroW.png"];
    
    
    NSMutableDictionary *options = [NSMutableDictionary new];
    [options setObject:@(2.5f) forKey:kCRToastTimeIntervalKey];
    [options setObject:text forKey:kCRToastTextKey];
    [options setObject:@(NSTextAlignmentCenter) forKey:kCRToastTextAlignmentKey];
    [options setObject:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:12] forKey:kCRToastFontKey];
    [options setObject:[UIColorFromRGB(0Xab111f) colorWithAlphaComponent:0.9] forKey:kCRToastBackgroundColorKey];
    [options setObject:@(CRToastPresentationTypeCover) forKey:kCRToastNotificationPresentationTypeKey];
    [options setObject:@(CRToastTypeNavigationBar) forKey:kCRToastNotificationTypeKey];
    [options setObject:@(NO) forKey:kCRToastUnderStatusBarKey];
    [options setObject:logo forKey:kCRToastImageKey];
    [options setObject:@(CRToastAnimationTypeLinear) forKey:kCRToastAnimationInTypeKey];
    [options setObject:@(CRToastAnimationTypeGravity) forKey:kCRToastAnimationOutTypeKey];
    [options setObject:@(CRToastAnimationDirectionTop) forKey:kCRToastAnimationInDirectionKey];
    [options setObject:@(CRToastAnimationDirectionTop) forKey:kCRToastAnimationOutDirectionKey];
    [options setObject:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE] forKey:kCRToastFontKey];
    
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"Completed");
                                }];
}

-(void)mostraNotificaNoSuggestPray{
    NSString *text = [NSLocalizedString(@"shortcut_no_suggest", nil) uppercaseString];
    UIImage *logo= [UIImage imageNamed:@"erroW.png"];
    
    
    NSMutableDictionary *options = [NSMutableDictionary new];
    [options setObject:@(2.5f) forKey:kCRToastTimeIntervalKey];
    [options setObject:text forKey:kCRToastTextKey];
    [options setObject:@(NSTextAlignmentCenter) forKey:kCRToastTextAlignmentKey];
    [options setObject:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:12] forKey:kCRToastFontKey];
    [options setObject:[UIColorFromRGB(0Xab111f) colorWithAlphaComponent:0.9] forKey:kCRToastBackgroundColorKey];
    [options setObject:@(CRToastPresentationTypeCover) forKey:kCRToastNotificationPresentationTypeKey];
    [options setObject:@(CRToastTypeNavigationBar) forKey:kCRToastNotificationTypeKey];
    [options setObject:@(NO) forKey:kCRToastUnderStatusBarKey];
    [options setObject:logo forKey:kCRToastImageKey];
    [options setObject:@(CRToastAnimationTypeLinear) forKey:kCRToastAnimationInTypeKey];
    [options setObject:@(CRToastAnimationTypeGravity) forKey:kCRToastAnimationOutTypeKey];
    [options setObject:@(CRToastAnimationDirectionTop) forKey:kCRToastAnimationInDirectionKey];
    [options setObject:@(CRToastAnimationDirectionTop) forKey:kCRToastAnimationOutDirectionKey];
    [options setObject:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE] forKey:kCRToastFontKey];
    
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"Completed");
                                }];
}


-(void)mostraNotificaWeekERROR_INCOMPLETEMM:(NSString*)risposta{
    // NSLog(@"mostraNotificaTuttoOkMAIN");
    [self activityNetwork:NO];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
	[dateFormatter setDateFormat:@"d MMM"];
	NSDate *oggi = [NSDate date];
    NSMutableString *subtitle = [NSMutableString stringWithString:@""];
    int incomplete_count = 0;
    NSMutableArray *stringArray = [NSMutableArray array];
    for (int i = 0; i < [risposta length]; i++) {
        NSString *ch = [risposta substringWithRange:NSMakeRange(i, 1)];
        [stringArray addObject:ch];
    }
	for (int i=0; i < stringArray.count; i++)
	{
        if([[stringArray objectAtIndex:i] isEqualToString:@"0"])
        {
		NSDate *giornoSettimana = [oggi dateByAddingTimeInterval:60*60*24*(i+1)];
            [subtitle appendFormat:@" %@,",[dateFormatter stringFromDate:giornoSettimana]];
            incomplete_count ++;
        }
    }
    
    if(incomplete_count == 1)
    {
        [dateFormatter setDateFormat:@"EEEE d MMMM"];
        for (int i=0; i < stringArray.count; i++)
            if([[stringArray objectAtIndex:i] isEqualToString:@"0"])
            {
                NSDate *giornoSettimana = [oggi dateByAddingTimeInterval:60*60*24*(i+1)];
                subtitle = [NSMutableString stringWithFormat:@" %@,",[dateFormatter stringFromDate:giornoSettimana]];
            }
    }
    
    // NSLog(@"sub:%@ range:%@ lenght:%i",subtitle,NSStringFromRange(NSMakeRange([subtitle length]-2, 1)), (int)[subtitle length]);
    [subtitle replaceCharactersInRange:NSMakeRange([subtitle length]-1, 1) withString:(incomplete_count==1)?@"":@"."];

    
    NSString *localizzaWeekNO = (incomplete_count==1)?[[NSString stringWithFormat:NSLocalizedString(NOT_STATUS_WEEK_NO_SINGOLO, nil),subtitle] uppercaseString]:[NSLocalizedString(NOT_STATUS_WEEK_NO, nil) uppercaseString];
    UIImage *logo= [UIImage imageNamed:@"erroW.png"];
    
    NSMutableDictionary *options = [NSMutableDictionary new];
    [options setObject:@(2.5f) forKey:kCRToastTimeIntervalKey];
    [options setObject:localizzaWeekNO forKey:kCRToastTextKey];
    [options setObject:@(NSTextAlignmentCenter) forKey:kCRToastTextAlignmentKey];
    [options setObject:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE] forKey:kCRToastFontKey];
    [options setObject:@(NSTextAlignmentLeft) forKey:kCRToastSubtitleTextAlignmentKey];
    [options setObject:[UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size:self.isIpad?14:11] forKey:kCRToastSubtitleFontKey];
    [options setObject:[UIColorFromRGB(0Xab111f) colorWithAlphaComponent:0.9] forKey:kCRToastBackgroundColorKey];
    [options setObject:@(CRToastPresentationTypeCover) forKey:kCRToastNotificationPresentationTypeKey];
    [options setObject:@(CRToastTypeNavigationBar) forKey:kCRToastNotificationTypeKey];
    [options setObject:@(NO) forKey:kCRToastUnderStatusBarKey];
    [options setObject:logo forKey:kCRToastImageKey];
    [options setObject:@(CRToastAnimationTypeLinear) forKey:kCRToastAnimationInTypeKey];
    [options setObject:@(CRToastAnimationTypeGravity) forKey:kCRToastAnimationOutTypeKey];
    [options setObject:@(CRToastAnimationDirectionTop) forKey:kCRToastAnimationInDirectionKey];
    [options setObject:@(CRToastAnimationDirectionTop) forKey:kCRToastAnimationOutDirectionKey];
        if(incomplete_count>1)[options setObject:subtitle forKey:kCRToastSubtitleTextKey];
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"Completed");
                                }];

}

-(void)mostraNotificaNoMessaNetMAIN{
    // NSLog(@"mostraNotificaNoMessaNetMAIN");
    UIImage *logo= [UIImage imageNamed:@"noNet.png"];
    NSDictionary *options = @{
                              kCRToastTimeIntervalKey   : @(1.5f),
                              kCRToastTextKey :[NSLocalizedString(@"createMessaNoNet", nil) uppercaseString],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColorFromRGB(0Xab111f) colorWithAlphaComponent:0.8],
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastUnderStatusBarKey : @(NO),
                              kCRToastImageKey :  logo,
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastFontKey:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE],
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              };
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"Completed");
                                }];
    
}

-(void)mostraNotificaNoConnessioneMAIN{
    // NSLog(@"mostraNotificaNoConnessione");
    UIImage *logo= [UIImage imageNamed:@"noNet.png"];
    NSDictionary *options = @{
                              kCRToastTimeIntervalKey   : @(1.5f),
                              kCRToastTextKey :[NSLocalizedString(NOT_STATUS_NO_NET, nil) uppercaseString],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColorFromRGB(0Xab111f) colorWithAlphaComponent:0.8],
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastUnderStatusBarKey : @(NO),
                              kCRToastFontKey:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:self.isIpad?FONT_SIZE_IPAD:FONT_SIZE],
                              kCRToastImageKey :  logo,
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeLinear),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              };
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    // NSLog(@"Completed");
                                }];

}


#pragma mark - Appear
//===================
//APPEAR
//===================
- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    [self.bookBtnToggle setAlpha:0.8];
    [self.bookBtnToggle setSelected:[Home iSForBook]];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


#pragma mark - Disappear
//===================
//DISAPPEAR
//===================
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}


#pragma mark -
#pragma mark Activity Network
//=========================
// Activity Network
//=========================
-(BOOL)activityNetwork:(BOOL)b{
    
    
    runOnMainQueueWithoutDeadlocking(^{
        if(b) [self attivaRotella];
        else [self disattivaRotella];
    });
    return YES;
}

-(void)attivaRotella{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

-(void)disattivaRotella{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}


#pragma mark - Error CLass
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    // NSLog(@"viewDidUnload");
    [super viewDidUnload];
}

#pragma mark -
#pragma mark ANIMAZIONI
//=========================
// ANIMAZIONI
//=========================
// aggiunge vista con bounce idx(per ciclare vari bouce con delay da id ciclo for
// Prima add e alpha0 della vista!!
- (void)bounceView:(UIView *)viewToBouce withDuration:(float)d idx:(NSUInteger)idx initDelay:(CGFloat)inizialDelay {
    CGFloat delay = (inizialDelay + idx*0.5f);
    // NSLog(@"d:%f delay animation:%f",d,delay);
    viewToBouce.layer.transform = CATransform3DMakeScale(0.3, 0.3, 1.0);

    [UIView animateWithDuration:0.2 delay:delay options:UIViewAnimationOptionAllowAnimatedContent
                                         animations:^{[viewToBouce setAlpha:1];}
                                         completion:^(BOOL finished){
                                             CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
                                             bounceAnimation.values = [NSArray arrayWithObjects:
                                                                       [NSNumber numberWithFloat:1.2],
                                                                       [NSNumber numberWithFloat:0.9],
                                                                       [NSNumber numberWithFloat:1.0], nil];
                                             bounceAnimation.duration = d;
                                             [bounceAnimation setValue:@"mostraBouceView" forKey:@"MyAnimationType"];
                                             bounceAnimation.removedOnCompletion = NO;
                                             [viewToBouce.layer addAnimation:bounceAnimation forKey:@"bounce"];
                                             viewToBouce.layer.transform = CATransform3DIdentity;
                                                    }
     ];
}


-(void)fine{
    // NSLog(@"Finita ANimazione!!");
}

- (void)removeViewControllerFromRight{
    [self.parentViewController beginAppearanceTransition:YES animated:NO];
    [self.parentViewController willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    return;
    [self transitionFromViewController:self toViewController:self.parentViewController duration:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.view setFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }
                            completion:^(BOOL finished) {
                                [self.parentViewController endAppearanceTransition];
                                [self removeFromParentViewController];
                                [self.view removeFromSuperview];
                            }];

}


#pragma mark -
#pragma mark NOTIFICHE
//=========================
// NOTIFICHE
//=========================
-(void)notifica:(NSString*)notific{
    runOnMainQueueWithoutDeadlocking(^{
        [[NSNotificationCenter defaultCenter] postNotificationName:notific object:nil];
    });
}

#pragma mark -
#pragma mark ADD/REMOVE viewCTRL
//===============================
// ADD/REMOVE viewCTRL
//===============================

- (void)displayViewController:(UIViewController*)controller inView:(UIView*)view{
    //LOG(@"displayContentController[%@] %@",controller,controller.view);
    [self addChildViewController:controller];
    [view addSubview:controller.view];
    [controller didMoveToParentViewController:self];
}

- (void)removeViewController{
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (void)addChildCTRL:(UIViewController*)controller{
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
}

- (void)removeChildCTRL:(UIViewController*)controller{
    [controller willMoveToParentViewController:nil];
    [controller removeFromParentViewController];
}

#pragma mark -
#pragma mark addBLUR
//===============================
// addBLUR
//===============================
-(void)blurView:(UIView*)view withEffectStyle:(UIBlurEffectStyle)style
{
    if(self.supportBlur){
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:style];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
        effectView.frame = view.bounds;
        [view insertSubview:effectView atIndex:0];
    }
    else{
        //NO BLUR SUPPORT
        if(style == UIBlurEffectStyleDark){
            [view setBackgroundColor:[UIColorFromRGB(0X31251b) colorWithAlphaComponent:0.86]];
        }
        else{
            [view setBackgroundColor:[UIColorFromRGB(0Xf4c57e) colorWithAlphaComponent:0.88]];
        }
    }
}

#pragma mark -
#pragma mark Utility method
//===============================
// runOnMainQueueWithoutDeadlocking
//===============================
void runOnMainQueueWithoutDeadlocking(void (^block)(void))
{
    if ([NSThread isMainThread])
    {
        block();
    }
    else
    {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
};

#pragma mark - STATUSBAR NOTIFICA
//=========================
// STATUSBAR NOTIFICA
//=========================

# pragma mark - overriding functions
// The below functions produce warnings - not an issue

- (BOOL)prefersStatusBarHidden {
    return self.statusBarIsHidden;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

# pragma mark - helper functions

- (void)updateStatusBar {
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // >= iOS 7
        [self setNeedsStatusBarAppearanceUpdate];
    } else {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
}


#pragma mark -
#pragma mark CUSTOM ALERTVIEW
//=========================================
// CUSTOM ALERTVIEW
//=========================================
- (void)customTutorialBookMenuAlert
{
    // Here we need to pass a full frame
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self createTutorialMenu]];
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK", nil]];
    [alertView setDelegate:self];
    
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}


- (UIView *)createTutorialMenu
{
    
    UILabel *titolo = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 290, 25)];
    [titolo setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:20]];
    [titolo setTextAlignment:NSTextAlignmentCenter];
    [titolo setText:NSLocalizedString(@"book_titolo", nil)];
    

    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
    [icon setCenter:CGPointMake(titolo.center.x, 2.+ CGRectGetMaxY(titolo.frame)+icon.frame.size.height/2.)];
    UIImage* imageForRendering = [[UIImage imageNamed:@"book_icon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [icon setTintColor:[[UIColor blackColor] colorWithAlphaComponent:0.3]];
    [icon setImage:imageForRendering];

    
    UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(icon.frame)+5, 270, 125)];
    [text setText:NSLocalizedString(@"book_menu_mex", nil)];
    [text setFont:[UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size:16]];
    [text setNumberOfLines:0];
    [text sizeToFit];
    
    TNCircularCheckBoxData *bananaData = [[TNCircularCheckBoxData alloc] init];
    bananaData.identifier = NSLocalizedString(@"book_checkbox", nil);
    bananaData.labelText = NSLocalizedString(@"book_checkbox", nil);
    bananaData.labelMarginLeft = 5.f;
    bananaData.checked = NO;
    bananaData.borderColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    bananaData.circleColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    bananaData.borderRadius = 20;
    bananaData.circleRadius = 15;
    
    self.tutorialAlertGroup = [[TNCheckBoxGroup alloc] initWithCheckBoxData:@[bananaData] style:TNCheckBoxLayoutHorizontal];
    self.tutorialAlertGroup.labelColor = [[UIColor blackColor] colorWithAlphaComponent:0.55];
    self.tutorialAlertGroup.rowItemCount = 1;
    [self.tutorialAlertGroup create];
    self.tutorialAlertGroup.position = CGPointMake(25, CGRectGetMaxY(text.frame)+10);
    [self.tutorialAlertGroup setBackgroundColor:[UIColor clearColor]];
    
    NSLog(@"titolo:%@",titolo);
    NSLog(@"text:%@",text);
    NSLog(@"createTutorialContainer%@",self.tutorialAlertGroup);
    
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, CGRectGetMaxY(text.frame)+40)];
    [demoView addSubview:titolo];
    [demoView addSubview:icon];
    [demoView addSubview:text];
    [demoView addSubview:self.tutorialAlertGroup];
    
    return demoView;
}



#pragma mark -
#pragma mark FINALIZE
//=========================
// FINALIZE
//=========================
- (void)finalize {
    // NSLog(@"free baseCTRL");
    [self.internetReachability stopNotifier];
    [super finalize];
}

//=========================
// END CLASS
//=========================
@end

/*
=========================
 SEMAFORO FAQ
=========================

-(void)lavoroconDipendenze{
        //creo semaforo
        dispatch_semaphore_t sem = dispatch_semaphore_create(0);
 
 
        [self operazioneASYNCwithCompletionBlock:^(NSObject *obj){
                                                //>>>semaforo verde!
                                                dispatch_semaphore_signal(sem);
                                                }];
 
        //aspetto semaforo verde <<<
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
        //...
        //da qui verrà eseguito dopo segnale >>>semaforo verde scattato!
 
 }
 
 */

/*
 =========================
 NATIVE BLUR EFFECT
 =========================
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];

        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];

        visualEffectView.frame = self.view.bounds;
        [self.view addSubview:visualEffectView];
*/
