//
//  MyNavigationCTRL.m
//  Valconca
//
//  La navBar è impostata sempre Hidden ed ha la gestione dell'orientamento direttamente al TopViewController !
//
//  Created by leyo on 6/09/13.
//  Copyright (c) 2013 leyo. All rights reserved.
//

#import "MyNavigationCTRL.h"

@interface MyNavigationCTRL ()

@end

@implementation MyNavigationCTRL

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithRootViewController:(UIViewController *)rootViewController{

    self = [super initWithRootViewController:rootViewController];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================
-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return [self.topViewController supportedInterfaceOrientations];
}

-(BOOL)shouldAutorotate {
    return [self.topViewController shouldAutorotate];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    //do your post-rotation // NSLogic here
    return [self.topViewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [self.topViewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavigationBarHidden:YES];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
