//
//  SystemData.m
//  Valconca
//
//  Created by leyo on 22/01/13.
//  Copyright (c) 2013 leyo. All rights reserved.
//

#import "SystemData.h"
#import "GlobalFunction.h"
#import "Reachability.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#import "SSZipArchive.h"
#import "TestoProprio.h"
#import "Day.h"
#import "Fix.h"
#import "Messa.h"
#import "Home.h"
#import "NotificaObject.h"
#import <SystemConfiguration/SCNetworkReachability.h>
#import <objc/runtime.h>



@interface SystemData()

@property (strong, nonatomic) Reachability *internetReachability;


@end

#pragma mark -
#pragma mark CLASSE SystemData, IMPLEMENTAZIONE
@implementation SystemData : NSObject

@synthesize internetReachability;

BOOL queueIsCancelled = NO;

//Operation queue
NSOperationQueue *queue;

#pragma mark Init
- (id)init {
    // NSLog(@"SystemData INIT");
    if ((self = [super init])){
        if(queue==nil)
        {
            queue = [NSOperationQueue new];
            [queue setMaxConcurrentOperationCount:1];
        }
     }
    return self;
}


#pragma mark -
#pragma mark SYSTEM METHODs
//FUNZIONI DI SISTEMA
-(NSUserDefaults*)defaults{
    NSUserDefaults *def = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];
    return def;
}


-(NSString*)version{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
}

-(NSFileManager*)fileManager{
    return [NSFileManager defaultManager];
}

-(BOOL)fileExistsAtPath:(NSString*)path{
    return [self.fileManager fileExistsAtPath:path];
}

//DOCUMENT DIRECTORY
- (NSString *)documentDir {
    NSArray* documentPathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* docPath = [documentPathArray lastObject];
    // NSLog(@"DocDir:%@",docPath);
    return docPath;
}

//CACHE DIRECTORY
- (NSString *)cacheDir {
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    // NSLog(@"CacheDir:%@",cachePath);
    return cachePath;
}

-(BOOL)connesso{
    BOOL i = [GlobalFunction checkNetworkStatus];
    // NSLog(@"SY_connesso:%i",i);
    return i;
}

#pragma mark -
#pragma mark NOTIFICHE
//=========================
// NOTIFICHE
//=========================
-(void)notifica:(NSString*)notific{
    [self performSelectorOnMainThread:@selector(notificaMain:) withObject:notific waitUntilDone:NO];
}

-(void)notificaMain:(NSString*)notific{
    // NSLog(@"SD>>>NOT: %@",notific);
    [[NSNotificationCenter defaultCenter] postNotificationName:notific object:nil];
}

#pragma mark -
#pragma mark 2015
//=========================
// AGGIORNAMENTO 2015
//=========================
+(void)aggiornamento2015{
    NSLog(@"aggiornamento2015");
    __block SystemData *s = [SystemData new];
    NSUserDefaults *d = s.defaults;
    NSLog(@"%@",d);
    NSLog(@"%@",[s.defaults objectForKey:ROOT_TS_INDEX]);
    
    
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"AGGIORNAMENTO 2014: step 1");
        [s activityNetwork:YES];
    }];//fine step1
    
    
    //========
    // STEP 3
    //========
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"AGGIORNAMENTO 2014: step 3");
        SystemData *s = [SystemData new];
        if(![s.defaults objectForKey:ROOT_TS_INDEX]){
            [s salvaStrutturaBase];
        }
    }];//fine step3
    
    //========
    // STEP 4
    //========
    NSBlockOperation *step4 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"AGGIORNAMENTO 2014: step 4");
        SystemData *s = [SystemData new];
            [s checkLASTUPDATE];
    }];//fine step4
    
    //========
    // STEP 5
    //========
    NSBlockOperation *step5 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"AGGIORNAMENTO 2014: step 5");
        SystemData *s = [SystemData new];
        [s checking_default];
        
    }];//fine step5
    
    //========
    // STEP 6 A
    //========
    NSBlockOperation *step6A = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"AGGIORNAMENTO 2014: step 6");
        SystemData *s = [SystemData new];
        [s scaricaPrimoGiornoStep1];
    }];//fine step6
    
    //========
    // STEP 6 B
    //========
    NSBlockOperation *step6B = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"AGGIORNAMENTO 2014: step 6");
        SystemData *s = [SystemData new];
        [s scaricaPrimoGiornoStep2];
    }];//fine step6
    
    //========
    // STEP 6 C
    //========
    NSBlockOperation *step6C = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"AGGIORNAMENTO 2014: step 6");
        SystemData *s = [SystemData new];
        [s scaricaPrimoGiornoStep3];
    }];//fine step6
    
    //========
    // STEP 7
    //========
    NSBlockOperation *step7 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"AGGIORNAMENTO 2015: step 7");
        [Home changeHudText:[@"completed" uppercaseString]];
        [NSThread sleepForTimeInterval:0.7];
        SystemData *s = [SystemData new];
        [s FineAggiornamento2015];
        // NSLog(@"SystemData: FINE FineAggiornamento2014:%@",[Home sharedInstance].HUD);
    }];//fine step7
    
    //========
    // STEP 8
    //========
    NSBlockOperation *step8 = [NSBlockOperation blockOperationWithBlock:^{
        // NSLog(@"AGGIORNAMENTO 2015: step 8");
        SystemData *s = [SystemData new];
        [s initNotifiche];
    }];//fine step8
    
    
    [step3 addDependency:step1];
    [step4 addDependency:step3];
    [step5 addDependency:step4];
    [step6A addDependency:step5];
    [step6B addDependency:step6A];
    [step6C addDependency:step6B];
    [step7 addDependency:step6C];
    [step8 addDependency:step7];

    [[NSOperationQueue new] addOperation:step1];
    [[NSOperationQueue new] addOperation:step3];
    [[NSOperationQueue new] addOperation:step4];
    [[NSOperationQueue new] addOperation:step5];
    [[NSOperationQueue new] addOperation:step6A];
    [[NSOperationQueue new] addOperation:step6B];
    [[NSOperationQueue new] addOperation:step6C];
    [[NSOperationQueue new] addOperation:step7];
    [[NSOperationQueue new] addOperation:step8];
                      
}

+(NSArray*)scrollValue{
    return[NSArray arrayWithObjects:[NSNumber numberWithInt:1],
                              [NSNumber numberWithInt:2],
                              [NSNumber numberWithInt:4],
                              [NSNumber numberWithInt:6],
                              [NSNumber numberWithInt:8],
                              [NSNumber numberWithInt:10],
                              [NSNumber numberWithInt:12],
                              [NSNumber numberWithInt:14],
                              [NSNumber numberWithInt:16],
                              [NSNumber numberWithInt:20],
                              [NSNumber numberWithInt:25],
                              [NSNumber numberWithInt:30],
                              [NSNumber numberWithInt:35],
                              [NSNumber numberWithInt:40],
                              [NSNumber numberWithInt:45],
                              [NSNumber numberWithInt:50],
                              [NSNumber numberWithInt:55],
                              [NSNumber numberWithInt:70],
                              [NSNumber numberWithInt:80],
                              [NSNumber numberWithInt:90],
                              nil];
    
}

+(NSArray*)audioRate{
    CGFloat version = [[UIDevice currentDevice].systemVersion floatValue];
    if(version<9.f){
        return @[@0.01,@0.02,@0.04,@0.06,@0.08,@0.10,@0.13,@0.18,@0.22,@0.3];
    }
    return @[@0.36,@0.38,@0.4,@0.42,@0.44,@0.46,@0.48,@0.5,@0.52,@0.54];
}


//=========================
// 1) DELETE ALL CACHE
//=========================
-(void)deleteALLCache{
    NSLog(@"deleteALLCache:not used!!");
    if(![self.defaults objectForKey:ROOT_TS_INDEX]){

    NSFileManager *filemgr;
    filemgr = [NSFileManager defaultManager];
    
    if ([filemgr removeItemAtPath: [NSHomeDirectory() stringByAppendingString:@"/Library/Caches"] error: NULL]  == YES){
       // NSLog (@"Remove Cache folder successful");
    }
    else{
       // NSLog (@"Remove Cache folder failed");
    }
    
    [filemgr createDirectoryAtPath: [NSHomeDirectory() stringByAppendingString:@"/Library/Caches"] withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
}

//=========================
// 2) CHECKING DEFAULT
//=========================
-(void)checking_default{
    // NSLog(@"init_default");
    [Home changeHudText:[[NSString stringWithFormat:@"%@ options",![self.defaults objectForKey:AUTOSCROLL_INDEX]?@"initialize":@"checking"] uppercaseString]];
    
    //---->Settings
    //ZOOM
    if(![self.defaults objectForKey:ZOOM]){
        [self.defaults setObject:@"100" forKey:ZOOM];
    }
    //LUMINOSITA'
    if(![self.defaults objectForKey:LUMINOSITA]){
        [self.defaults setFloat:0 forKey:LUMINOSITA];
    }
    //CSS
    if(![self.defaults objectForKey:CSS]){
        [self.defaults setInteger:0 forKey:CSS];
    }
    //AUTOSCROLL
    if(![self.defaults objectForKey:AUTOSCROLL_INDEX]){
        [self.defaults setBool:NO forKey:AUTOSCROLL_BOOL];
        [self.defaults setObject:[NSNumber numberWithInteger:7] forKey:AUTOSCROLL_INDEX];
    }
    
    //VELOCITA' AUDIO
    if(![self.defaults objectForKey:AUDIORATE_INDEX]){
        [self.defaults setObject:[NSNumber numberWithInteger:5] forKey:AUDIORATE_INDEX];
    }
    
    
    //AUTO OGGI-DOWNLOAD
    if(![self.defaults objectForKey:AUTODOWNLOAD_OGGI_DATE]){
        [self.defaults setBool:NO forKey:AUTODOWNLOAD_OGGI];
        [self.defaults setObject:@"" forKey:AUTODOWNLOAD_OGGI_DATE];
    }
    
    
    //AUDIOLETTURA-DEFAULT
    if(![self.defaults objectForKey:AUTODOWNLOAD_OGGI_DATE]){
        [self.defaults setBool:NO forKey:AUDIOLETTURA_BOOL];
    }
    
    if(![self.defaults objectForKey:ROOT_TS_INDEX]){
        //PRIMA INSTALLAZIONE INSTALLAZIONE 5.0 o minori
        
        [self.defaults setObject:NSLocalizedString(@"checklingua", @"lingua da LocalizedString") forKey:LINGUA];
        
        //DATA AGGIORNAMENTO ->2015
        [self.defaults setObject:[[NSDate date] dateByAddingTimeInterval:5] forKey:DATA_PRIMA_INSTALLAZIONE];
        
        
        //TUTORIAL
        [self.defaults setBool:NO forKey:TUTORIAL_LISTA_VISTO];
        [self.defaults setBool:NO forKey:TUTORIAL_SETTINGS_VISTO];
        [self.defaults setBool:NO forKey:TUTORIAL_TP_VISTO];
        [self.defaults setBool:NO forKey:TUTORIAL_MESSA_LISTA_VISTO];
        //My spiritual è del 2015 ->6
        
        //EVENT_FLAG
        [self.defaults setObject:@"" forKey:EVENT_FLAG];
        
        
        
        // setto Indici Breviario
        NSArray *indiceT = [[NSArray alloc] initWithObjects:
                            @"Breviary",
                            @"Missal",
                            @"Lectures",
                            @"Prayers",
                            @"Rites",
                            @"Terra Sancta",
                            nil];
        
        [self.defaults setObject:indiceT forKey:ROOT_TS_INDEX];
        NSArray *breviario_index = [[NSArray alloc] initWithObjects:@"Office of Reading", @"Lauds", @"Daytime Prayers", @"Vespers", @"Compline",@"Common", nil];
        [self.defaults setObject:breviario_index forKey:BREVIARIO_INDEX];
        
        NSArray *breviario_comune_index = [[NSArray alloc] initWithObjects:@"First Vespers",@"Office of Reading", @"Lauds", @"Daytime Prayers", @"Second Vespers", nil];
        
        [self.defaults setObject:breviario_comune_index forKey:BREVIARIO_COMUNE_INDEX];
        
        
        
        NSArray *messale_index = [[NSArray alloc] initWithObjects:@"Mass' Order", @"Readings and Prayers", @"Preface", @"Eucharistic Prayer", @"Faithfuls' Prayer", @"Common", nil];
        [self.defaults setObject:messale_index forKey:MESSALE_INDEX];
        
        NSArray *terrasanta_index = [[NSArray alloc] initWithObjects:@"Breviary", @"Lectures", nil];
        [self.defaults setObject:terrasanta_index forKey:TERRASANTA_INDEX];
        
        
        [self.defaults setObject:NSLocalizedString(@"checklingua", nil) forKey:LINGUA];
        
        //LOCAL NOTIFICHE INIT
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
    
    [self.defaults setBool:NO forKey:TUTORIAL_MYSPIRITUAL_VISTO];
    [self.defaults synchronize];
    
    
    [NSThread sleepForTimeInterval:0.8];
}

//=========================
// 3) SALVA STRUTTURA BASE
//=========================
-(void)salvaStrutturaBase{
    
    //NON SERVE SPOSTAMENTO
    NSError *err = nil;
    if([[NSFileManager defaultManager] createDirectoryAtPath:[self.documentDir stringByAppendingPathComponent:@"/DATA"]
                                 withIntermediateDirectories:YES
                                                  attributes:nil
                                                       error:nil]){
        // NSLog(@"salvaStrutturaBase-documentDir:%@",err);
    }
    
    err = nil;
    
    if([[NSFileManager defaultManager] createDirectoryAtPath:[self.cacheDir stringByAppendingPathComponent:@"/DAY"]
                                 withIntermediateDirectories:YES
                                                  attributes:nil
                                                       error:nil]){
        // NSLog(@"salvaStrutturaBase-cacheDir:%@",err);
    }

    [self.defaults synchronize];
}

//=========================
// 4) INIT NOTIFICHE
//=========================
-(void)initNotifiche{
        [NSThread sleepForTimeInterval:2];
    if(![self.defaults objectForKey:CAN_SHOW_NOTIFICA]){
        [self.defaults setBool:YES forKey:CAN_SHOW_NOTIFICA];
        [NotificaObject checkNotifiche];
        [self.defaults synchronize];
    }
}

BOOL primogiorno_scaricato = NO;

//=========================
// 5) SCARICO PRIMO GIORNO
//=========================
-(void)scaricaPrimoGiornoStep1{
    NSLog(@"SystemData:scaricaPrimoGiornoStep1");
    if(primogiorno_scaricato) return;
    primogiorno_scaricato = YES;
    
    //update HUD
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"dd MMM"];
    [self.defaults setObject:NSLocalizedString(@"checklingua", nil) forKey:LINGUA];
    [self.defaults synchronize];
    [Home changeHudText:[[NSString stringWithFormat:@"loading %@ - %@",[f stringFromDate:[NSDate date]],[self.defaults objectForKey:LINGUA]] uppercaseString]];
}

-(void)scaricaPrimoGiornoStep2{
    NSLog(@"SystemData:scaricaPrimoGiornoStep2");
    [NSThread sleepForTimeInterval:1.4];
}


-(void)scaricaPrimoGiornoStep3{
    NSLog(@"SystemData:scaricaPrimoGiornoStep3");
    //scarica giorno...
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSString *oggi = [f stringFromDate:[NSDate date]];
    Day *ddd = [Day scaricaGiornoFirstconData:oggi withLang:[self.defaults objectForKey:LINGUA] eventFlag:nil];
    NSError *err=nil;
    if(ddd)
        {
            [ddd salvaGiornoInUso:&err];
            if(err!=nil)
            {
                NSLog(@"errore salvataggio primo giorno:%@", [err description]);
            }
            else {
                // NSLog(@"5) SCARICATO PRIMO GIORNO %@",ddd);
                [Home changeHudText:[NSString stringWithFormat:@"%@",[err localizedDescription]]];
            }
        }
    else [Home errorePrimoAggiornamento:nil];
    }

//=========================
// 6) FINE AGGIORNAMENTO
//=========================
-(void)FineAggiornamento2015{
    [self notifica:AGGIORNAMENTO2015_TERMINATO];
    [self.defaults setBool:YES forKey:AGGIORNAMENTO_2015];
    [self.defaults synchronize];
    runOnMainQueueWithoutDeadlocking(^{
        [Home fineHUD2015];
    });
    
}

#pragma mark -
#pragma mark LASTUPDATE
//=========================
// LASTUPDATE
//=========================
+(void)checkLASTUPDATE{
    SystemData *s = [SystemData new];
    [s checkLASTUPDATE];
}

-(void)checkLASTUPDATE{
    // NSLog(@"checkLASTUPDATE:%i",[GlobalFunction checkNetworkStatus]);


    if(![self.defaults objectForKey:AGGIORNAMENTO_2015]){
        //caso primo aggiornamento!
        [Home changeHudText:[@"connecting..." uppercaseString]];
        
        NSURL *url = [NSURL URLWithString:@"http://www.ibreviary.com/service/services.php?s=get_status"];
        NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
        NSError *err_check_net = nil;
        NSData *data_check_net_error = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err_check_net];
        if(err_check_net != nil)
        {
            // NSLog(@"ERRORE PRIMO AGGIORMANENTO:%@\n%@",[err_check_net description],data_check_net_error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [Home errorePrimoAggiornamento:err_check_net];
            });
            [NSThread sleepForTimeInterval:2];
            // CLOSE APP PROCEDURE
            UIApplication *app = [UIApplication sharedApplication];
            //-> going background
            [app performSelector:@selector(suspend)];
            //-> exit app in background
            exit(0);
        }
        else {
            data_check_net_error = nil;
            [NSThread sleepForTimeInterval:2];
        }
    }//FINE AGGIORNAMENTO_2015
    
    if(![GlobalFunction checkNetworkStatus])
    {
        // NSLog(@"noNet");
        [self activityNetwork:NO];
        return;
    }
    
    [self activityNetwork:YES];
    //DOWNLOAD
    // NSLog(@"starting downloading....");
    NSURL *url = [NSURL URLWithString:[URL_LASTUPDATE stringByAppendingFormat:@"&l=%@",[self.defaults objectForKey:LINGUA]]];
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_REQUEST];
    NSError *err = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:&err];
    NSLog(@"data downloaded %@",err!=nil?[err description]:@"with NO error");
    [self activityNetwork:NO];
    if(err!=nil)
    {
        NSLog(@"ERR lastUpdate: %@",[err description]);
        NSLog(@"FINE checkLASTUPDATE(exit1)");
        return;
    }
    else{
        NSMutableDictionary *risposta = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err]];
        // NSLog(@"checkLASTUPDATE:%@",[risposta objectForKey:LAST_UPDATE]);
        if(err)
        {
            // NSLog(@"Errore parsing Json lastupdate");
            // NSLog(@"FINE checkLASTUPDATE(exit2)");
            return;
        }
        
        if(![LinguaObject getLingue] || [[LinguaObject getLingue] count]==0){
            //caso primo aggiornamento LINGUE!
            [Home changeHudText:[@"loading languages" uppercaseString]];
            [LinguaObject setLingueFromArray:[[risposta objectForKey:@"languages"] objectForKey:@"list"]];
            [self.defaults setObject:[[risposta objectForKey:@"languages"] objectForKey:@"last_update"] forKey:LAST_UPDATE_LANG];
            [self.defaults synchronize];
            [NSThread sleepForTimeInterval:0.5];
        }
        
        if(![TestoProprio getTestiPropri] || [[TestoProprio getTestiPropri] count]==0){
            //caso primo aggiornamento TP!
            [Home changeHudText:[@"loading tp" uppercaseString]];
            [TestoProprio setTestiPropri:[[risposta objectForKey:@"event_type"] objectForKey:@"list"]];
            [self.defaults setObject:[[risposta objectForKey:@"event_type"] objectForKey:@"last_update"] forKey:LAST_UPDATE_EVENT];
            [self.defaults synchronize];
            [NSThread sleepForTimeInterval:0.5];
        }
        
        
        
        NSArray *lingue = [LinguaObject getLingue];
            // NSLog(@"Lingue:%@",lingue);
        
        BOOL trovatoTestoFissoDaInizializzare = NO;

        for (int i=0; i < lingue.count;i++ ) {
            // NSLog(@"iterFixTest:%i",i);
            LinguaObject *row = [lingue objectAtIndex:i];
            if(![Fix getFixForLang:[row sigla]]){
                // NSLog(@"row:%@",row);
                [Home changeHudText:[[NSString stringWithFormat:@"loading %@ local text",row.sigla] uppercaseString]];
                [Fix scaricaFixWithLang:[row sigla] andLastUpdate:[[risposta objectForKey:@"fixed_text"] objectForKey:LAST_UPDATE]];
                trovatoTestoFissoDaInizializzare = YES;
            }
        }
        
        if(trovatoTestoFissoDaInizializzare) return;
        
        //qui posso dedicarmi al testo fisso caricato e vedere se è da aggiornare
        
            Fix *f = [Fix getFixForLang:[self.defaults objectForKey:LINGUA]];
        
            //CONTROLLO TESTI FISSI LAST_UPDATE
            if(!f || ![f.last_update isEqualToString:[[risposta objectForKey:@"fixed_text"] objectForKey:LAST_UPDATE]])
            {
            [Fix scaricaFixWithLang:[self.defaults objectForKey:LINGUA] andLastUpdate:[[risposta objectForKey:@"fixed_text"] objectForKey:LAST_UPDATE]];
            }
            
        
        // NSLog(@"RISPO_LAST_LANG:%@\nDEFAULT LAST_LANG:%@",[[risposta objectForKey:@"languages"] objectForKey:@"last_update"],[self.defaults objectForKey:LAST_UPDATE_LANG] );
        
        //CONTROLLO LINGUE LAST_UPDATE_LANG
        if(![[[risposta objectForKey:@"languages"] objectForKey:@"last_update"] isEqualToString:[self.defaults objectForKey:LAST_UPDATE_LANG]]){
            [LinguaObject setLingueFromArray:[[risposta objectForKey:@"languages"] objectForKey:@"list"]];
            [self.defaults setObject:[[risposta objectForKey:@"languages"] objectForKey:@"last_update"] forKey:LAST_UPDATE_LANG];
            [self.defaults synchronize];
        }
        
        // NSLog(@"RISPO_LAST_EVENT:%@\nDEFAULT LAST_EVENT:%@",[[risposta objectForKey:@"event_type"] objectForKey:@"last_update"],[self.defaults objectForKey:LAST_UPDATE_EVENT] );
        
        
        //CONTROLLO TESTI PROPRI LAST_UPDATE_EVENT
        if(![[[risposta objectForKey:@"event_type"] objectForKey:@"last_update"] isEqualToString:[self.defaults objectForKey:LAST_UPDATE_EVENT]]){
            [TestoProprio setTestiPropri:[[risposta objectForKey:@"event_type"] objectForKey:@"list"]];
            [self.defaults setObject:[[risposta objectForKey:@"event_type"] objectForKey:@"last_update"] forKey:LAST_UPDATE_EVENT];
            [self.defaults synchronize];
            
        }
        
    }
    // NSLog(@"FINE checkLASTUPDATE(exit4)");
}

#pragma mark -
#pragma mark ACTIVITY NETWORK
//=========================
// Activity Network
//=========================
-(void)activityNetwork:(BOOL)b{
    [self performSelectorOnMainThread:b?@selector(attivaRotella):@selector(disattivaRotella) withObject:nil waitUntilDone:NO];
}

-(void)attivaRotella{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

-(void)disattivaRotella{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}


- (void)finalize {
    // NSLog(@"free SystemData");
   [super finalize];
}

#pragma mark -
#pragma mark VERSION & PLATFORM
//=========================
// VERSION & PLATFORM
//=========================
+(NSString*)versionApp{
    return (NSString *)[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
}

+(NSString *)platformString
{
    int mib[2];
    size_t len;
    char *machine;
    
    mib[0] = CTL_HW;
    mib[1] = HW_MACHINE;
    sysctl(mib, 2, NULL, &len, NULL, 0);
    machine = malloc(len);
    sysctl(mib, 2, machine, &len, NULL, 0);
    
    NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
    free(machine);
    
    //======IPOD
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPodTouch1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPodTouch2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPodTouch3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPodTouch4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPodTouch5G";
    
    //======IPHONE
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"VerizoniPhone4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone5(GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone5(GSM_CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone5c(GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone5c(GSM_CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone5s(GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone5s(GSM_CDMA)";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone6(GSM)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone6(Global)";
    if ([platform isEqualToString:@"iPhone7,3"])    return @"iPhone6plus(GSM)";
    if ([platform isEqualToString:@"iPhone7,4"])    return @"iPhone6plus(Global)";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone6sPlus";
    
    //======IPAD
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad2(WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad2(GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad2(CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad2(WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPadMini(WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPadMini(GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPadMini(GSM_CDMA)";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPadMini2(WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPadMini2(Cellular)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPadMini2";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad3(WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad3(GSM_CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad3(GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad4(WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad4(GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad4(GSM_CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPadAir1(WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPadAir1(Cellular)";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPadmini3(WiFi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPadmini3(Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPadmini3(China)";
    if ([platform isEqualToString:@"iPad5,1"])      return @"iPadMini4(WiFi)";
    if ([platform isEqualToString:@"iPad5,2"])      return @"iPadMini4(LTE)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPadAir2(WiFi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPadAir2(Cellular)";
    if ([platform isEqualToString:@"iPad6,8"])      return @"iPadPro";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    
    //unknown
    return [platform stringByAppendingString:@"(unknown)"];
}

@end
