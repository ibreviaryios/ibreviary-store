//
//  BaseController.h
//  ScheletroAPP
//
//  Created by ileyo Parenti on 1/08/12.
//  Copyright (c) 2012 NetGuru All rights reserved.
//




#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import <Accelerate/Accelerate.h>
#import "CRToast.h"
#import "TNCheckBoxGroup.h"
#import "Categorie.h"

#pragma mark -
#pragma mark BASEController interfrace
//====================================
// BASEController INTERFACE
//====================================

@interface BaseController : UIViewController<UIAlertViewDelegate>{
	//APPDELEGATE
    AppDelegate *appDelegate;
}

void runOnMainQueueWithoutDeadlocking(void (^block)(void));

@property (nonatomic, readwrite) BOOL statusBarIsHidden;

//VARIABILI DI SISTEMA - x chiamare self.NOMEVARIABILE
@property (nonatomic, readonly) BOOL isIpad;
@property (nonatomic, readonly) BOOL isIphone;
@property (nonatomic, readonly) BOOL isIphone5;
@property (nonatomic, readonly) BOOL isLand;
@property (nonatomic, readonly) BOOL isPort;
@property (nonatomic, readonly) BOOL isRetina;
@property (nonatomic, readonly) BOOL isIos7orUpper;
@property (nonatomic, readonly) NSUserDefaults *defaults;
@property (readonly, nonatomic) NSFileManager *fileManager;
@property (readonly, nonatomic) NSString *documentDir;
@property (readonly, nonatomic) NSString *cacheDir;
@property (readonly, nonatomic) NSString *iBreviary_version;
@property (readonly, nonatomic) BOOL connesso;
@property(nonatomic,strong) TNCheckBoxGroup *tutorialAlertGroup;


//NAV-BAR
@property (strong, nonatomic) IBOutlet UIView *sfondo_nav;
@property (strong, nonatomic) IBOutlet UIButton *btn_back;
@property (strong, nonatomic) IBOutlet UIButton *btn_home;


@property (weak, nonatomic) IBOutlet UIButton* bookBtnToggle;

-(BOOL)fileExistsAtPath:(NSString*)path;
-(void)loadXibWithNibName:(NSString*)nibName;
-(BOOL)activityNetwork:(BOOL)b;
-(void)notifica:(NSString*)notific;

+(BOOL)isIos7orUpper;

-(IBAction)toggleBOOKPdf:(UIButton*)sender;
-(void)titleOnNavBar:(NSString*)tit_string;
-(void)titleOnNavBar2:(NSString*)tit_string;

//===============================
//ROTATE IMAGE
//===============================
-(void)rotateImage:(UIView*)image withDuration:(float)duration onY_axis:(BOOL)isYaxis;
-(void)cardRotation:(UIButton*)source destination:(UIImage*)change;

//===============================
// ADD/REMOVE viewCTRL
//===============================
- (void)displayViewController:(UIViewController*)controller inView:(UIView*)view;
- (void)removeViewController;
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>
- (void)addChildCTRL:(UIViewController*)controller;
- (void)removeChildCTRL:(UIViewController*)controller;

//addBlur
-(void)blurView:(UIView*)view withEffectStyle:(UIBlurEffectStyle)style;

//===============================
//NOTIFICHE LOCALIZZATE STATUSBAR
//===============================
-(void)noNet;
-(void)noMessaNet;
-(void)mostraICLoudUpdate;
-(void)mostraNotificaAggiornamento2015_ok;
-(void)mostraNotificaDayDownloaded:(NSString*)day;
-(void)mostraNotificaWeekDownloaded;
-(void)mostraNotificaNoUpdate;
-(void)mostraNotificaError:(NSError*)errore;
-(void)mostraNotificaDayERROR_INCOMPLETE:(NSString*)day;
-(void)mostraNotificaWeekERROR_INCOMPLETE:(NSString*)risposta;
-(void)mostraLocalNotificaBanner:(UILocalNotification*)ln;

//NOTIFICHE LOCALIZZATE FORCE TOUCH
-(void)mostraNotificaNoSanto;
-(void)mostraNotificaNoSuggestPray;

//CUSTOM BOOK ALERT
- (void)customTutorialBookMenuAlert;
@end
