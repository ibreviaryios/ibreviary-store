//
//  CheckVersion.m
//  CheckVersion
//
/*
 Per traduzioni:
 
 //==============
 //CONTROLLO AGGIORNAMNTO (ITA)
 //==============
 "testoCheckVersion" = "E' disponibile una nuova versione di %@. Aggiorna alla versione %@ ora.";
 "checkVersionCancel" = "Non adesso";
 "checkVersionUpdate" = "Aggiorna";

 //==============
 //CONTROLLO AGGIORNAMNTO (ENG)
 //==============
 "testoCheckVersion" = "A new version of %@ is available. Please update to version %@ now.";
 "checkVersionCancel" = "not now";
 "checkVersionUpdate" = "update";
 */

#import "CheckVersion.h"

// AppID (guarda iTunes Connect)   ..iBreviary TS AppID..
#define checkKeyAppID                 @"422601705"

@interface CheckVersion ()

+ (void)showAlertWithAppStoreVersion:(NSString*)appStoreVersion;

@end

@implementation CheckVersion

static BOOL checkVersionForceUpdate = NO;


#pragma mark - Public Methods
+ (void)checkVersion
{

    // Asynchronously query iTunes AppStore for publically available version
    NSString *storeString = [NSString stringWithFormat:@"http://itunes.apple.com/lookup?id=%@", checkKeyAppID];
    NSURL *storeURL = [NSURL URLWithString:storeString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:storeURL];
    [request setHTTPMethod:@"GET"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
       
        if ( [data length] > 0 && !error ) { // Success
            
            NSDictionary *appData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];

            dispatch_async(dispatch_get_main_queue(), ^{
                
                // All versions that have been uploaded to the AppStore
                NSArray *versionsInAppStore = [[appData valueForKey:@"results"] valueForKey:@"version"];
                
                if ( ![versionsInAppStore count] ) { // No versions of app in AppStore
                    
                    return;
                    
                } else {

                    NSString *currentAppStoreVersion = [versionsInAppStore objectAtIndex:0];
                    
                    NSString *tester = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];

                    if ([tester compare:currentAppStoreVersion options:NSNumericSearch] == NSOrderedAscending) {
		                
                        [CheckVersion showAlertWithAppStoreVersion:currentAppStoreVersion];
	                
                    }
                    else {
		            
                        // Current installed version is the newest public version or newer	
                        // NSLog(@"CheckVersion: versione app aggiornata!");
                    }

                }
              
            });
        }
        
    }];
}


// Alert title and action buttons
#define checkKeyAlertViewTitle        
#define checkKeyUpdateButtonTitle     @"Update"

#pragma mark - Private Methods
+ (void)showAlertWithAppStoreVersion:(NSString *)currentAppStoreVersion
{
    
    NSString *appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    
    if ( checkVersionForceUpdate ) { // Force user to update app
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Update"
                                                            message:[NSString stringWithFormat:NSLocalizedString(@"testoCheckVersion", nil), appName, currentAppStoreVersion]
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"checkVersionUpdate", nil)
                                                  otherButtonTitles:nil, nil];
        
        [alertView show];
        
    } else { // Allow user option to update next time user launches your app
        
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Update"
                                                            message:[NSString stringWithFormat:NSLocalizedString(@"testoCheckVersion", nil), appName, currentAppStoreVersion]
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"checkVersionCancel", nil)
                                                  otherButtonTitles:NSLocalizedString(@"checkVersionUpdate", nil), nil];
        
        [alertView show];
        
    }
    
}

#pragma mark - UIAlertViewDelegate Methods
+ (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if ( checkVersionForceUpdate ) {

        NSString *iTunesString = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@", checkKeyAppID];
        NSURL *iTunesURL = [NSURL URLWithString:iTunesString];
        [[UIApplication sharedApplication] openURL:iTunesURL];
        
    } else {

        switch ( buttonIndex ) {
                
            case 0:{ // Cancel / Not now
        
                // Do nothing
                
            } break;
                
            case 1:{ // Update
                
                NSString *iTunesString = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@", checkKeyAppID];
                NSURL *iTunesURL = [NSURL URLWithString:iTunesString];
                [[UIApplication sharedApplication] openURL:iTunesURL];
                
            } break;
                
            default:
                break;
        }
        
    }

    
}

@end
