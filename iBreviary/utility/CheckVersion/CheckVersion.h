//
//  CheckVersion.h
//

#import <Foundation/Foundation.h>

@interface CheckVersion : NSObject <UIAlertViewDelegate>

/*
  If a newer version exists in the AppStore, it prompts the user to update your app.
 */
+ (void)checkVersion;

@end
