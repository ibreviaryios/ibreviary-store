//
//  MessaLista.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "CPPickerView.h"
#import "Lingua.h"
#import "Tutorial.h"
#import "FXBlurView.h"
#import "AdvancedSettings.h"
#import "BNHtmlPdfKit.h"


@interface MessaLista : BaseController<CPPickerViewDataSource, CPPickerViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,LinguaDelegate,TutorialDelegate,BNHtmlPdfKitDelegate,UIDocumentInteractionControllerDelegate>{
}


//SFONDO
@property (strong, nonatomic) IBOutlet UIImageView *logoVelina_lingua;

//STRUCTURE
@property (nonatomic, strong) NSMutableArray *list_day;
@property (nonatomic, strong) NSMutableArray *list_down;
@property (nonatomic, strong) NSMutableArray *list_lingua_img;
@property (nonatomic, strong) NSMutableDictionary *linguaPos;
@property (strong, nonatomic) IBOutlet UIImageView *cornice;
@property (strong, nonatomic) NSOperationQueue *queue;
//SHARE PDF
@property (strong, nonatomic) BNHtmlPdfKit *htmlPdfKit;
@property (strong) UIDocumentInteractionController *documentInteractionCtrl;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *rotellaShare;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *rotellaEdit;

//CLOSE
@property (strong, nonatomic) IBOutlet UIButton *close;

//ROTELLA
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *rotella;

//TABELLA - GIORNI DISPONIBILI
@property (strong, nonatomic) IBOutlet UITableView *tabella;

//DOWNLOAD
@property (strong, nonatomic) IBOutlet UILabel *title_messa_ctrl;
@property (strong, nonatomic) IBOutlet UIButton *button_crea;
@property (strong, nonatomic) IBOutlet UIButton *button_crea_comune;
@property (strong, nonatomic) IBOutlet UILabel *crea_comune_label;
@property (strong, nonatomic) IBOutlet UILabel *crea_feriale_label;
@property (strong, nonatomic) IBOutlet UILabel *title_lista_messa;
@property (strong, nonatomic) CPPickerView *picker_ctrl_day;
@property (strong, nonatomic) UIPickerView *picker_ctrl_lingua;
@property (strong, nonatomic) IBOutlet UIView *backPicker;
@property (strong, nonatomic) IBOutlet UIView *day_ctrl_view;
@property (strong, nonatomic) IBOutlet UIButton *up_btn;
@property (strong, nonatomic) IBOutlet UIButton *down_btn;
@property (strong, nonatomic) IBOutlet UIButton *sx_btn;
@property (strong, nonatomic) IBOutlet UIButton *dx_btn;
@property (strong, nonatomic) IBOutlet UIView *backPicker_lingua;
//-AREA DATA MESSA
@property (strong, nonatomic) IBOutlet UIView *area_scegli_giorno;
@property (strong, nonatomic) IBOutlet UILabel *label_data_messa;


@property (strong, nonatomic) IBOutlet UIButton *reset_btn;
@property (strong, nonatomic) IBOutlet UILabel *reset_label;

//->LINGUA
@property (strong, nonatomic) FXBlurView *blurView;
@property (strong, nonatomic) Lingua *linguaCTRL;
@property (strong, nonatomic) IBOutlet UIImageView *info_img;
@property (strong, nonatomic) IBOutlet UIButton *btn_lingua;


//TUTORIAL
@property (strong, nonatomic) Tutorial *tutorialCTRL;
@property (strong, nonatomic) IBOutlet UIImageView *tutorial_img;
@property (strong, nonatomic) IBOutlet UIButton *tutorial_btn;

//INIT
- (id)init;

//OPERATION QUEUE
-(void)aggiungiOperazione:(SEL)selector;
-(void)aggiungiOperazione:(SEL)selector withObject:(id)obj;
@end
