//
//  MessaLista.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "MessaLista.h"
#import "SystemData.h"
#import "Home.h"
#import "LinguaObject.h"
#import "TestoProprio.h"
#import "Messa.h"
#import "Fix.h"
#import "DGActivityIndicatorView.h"
#import "BLLoader.h"

@interface MessaLista ()

@property (weak, nonatomic) IBOutlet BLLoader *loader_uno;
@property (weak, nonatomic) IBOutlet BLLoader *loader_due;

@end

@implementation MessaLista

//STRUCTURE
@synthesize list_day, list_down, list_lingua_img, linguaPos, cornice, queue;
//SHARE
@synthesize htmlPdfKit, documentInteractionCtrl, rotellaShare;
//CLOSE
@synthesize close;
//ROTELLA
@synthesize rotella;
//TABELLA - GIORNI DISPONIBILI
@synthesize tabella;
//CREA-DOWNLOAD MESSA
@synthesize title_messa_ctrl, button_crea, button_crea_comune, crea_comune_label,crea_feriale_label,title_lista_messa;
@synthesize picker_ctrl_day,picker_ctrl_lingua, backPicker,backPicker_lingua;
@synthesize  logoVelina_lingua;
@synthesize day_ctrl_view,sx_btn,dx_btn;
@synthesize up_btn,down_btn;
@synthesize reset_btn,reset_label;
//->LINGUA
@synthesize linguaCTRL,btn_lingua,blurView,info_img;
//->TUTORIAL
@synthesize tutorialCTRL,tutorial_img,tutorial_btn;
//-AREA DATA MESSA
@synthesize area_scegli_giorno, label_data_messa;

//COLORI
UIColor *selectedCell;
UIColor *notSelectedCell;
UIColor *defaulf_color,*default_disabilita,*default_mese,*pickerColor,*reset_color;
UIColor *hightlight_color;

NSMutableDictionary *colorWeek;

NSArray *temp_color;


int processoMessaLista = 0;

int cellaMessaSelezionata = -1;

//Operation queue
NSOperationQueue *queue;


#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)init
{
    NSString *nibNameOrNil = @"MessaLista_ipad";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        list_day = [NSMutableArray new];
        queue = [NSOperationQueue new];
        [queue setMaxConcurrentOperationCount:1];

    }
    return self;
}


#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad
{
    [super viewDidLoad];
    // NSLog(@"MessaLista DIDload");
    
    [self initView];
    [self localizza];
}


- (void)initView{
    // NSLog(@"MessaLista initView");
    
    //COLORI
    selectedCell = [UIColor colorWithRed:171/255.f green:0 blue:0 alpha:0.9];
    notSelectedCell= [UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:150/255.f];
    defaulf_color = [UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:150/255.f];
    default_mese = [UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:255/255.f];
    hightlight_color = [UIColor colorWithRed:171/255.f green:0 blue:0 alpha:0.9];
    default_disabilita = [UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:100/255.f];
    pickerColor = [UIColor colorWithRed:122/255.f green:97/255.f blue:79/255.f alpha:1];
    reset_color = [UIColor colorWithRed:122/255.f green:97/255.f blue:79/255.f alpha:1];
    colorWeek = [[NSMutableDictionary alloc] init];
    [colorWeek setObject:[UIColorFromRGB(0Xbd995d) colorWithAlphaComponent:0.9] forKey:@"7"];  //SABATO
    [colorWeek setObject:[UIColorFromRGB(0Xbd995d) colorWithAlphaComponent:1] forKey:@"1"];   //DOMENICA
    [colorWeek setObject:[UIColorFromRGB(0Xa8936f) colorWithAlphaComponent:0.9] forKey:@"2"]; //LUNEDI'
    [colorWeek setObject:[UIColorFromRGB(0Xa8936f) colorWithAlphaComponent:0.9] forKey:@"3"]; //MARTEDI'
    [colorWeek setObject:[UIColorFromRGB(0Xa8936f) colorWithAlphaComponent:0.9] forKey:@"4"]; //MERCOLEDI'
    [colorWeek setObject:[UIColorFromRGB(0Xa8936f) colorWithAlphaComponent:0.9] forKey:@"5"]; //GIOVEDI'
    [colorWeek setObject:[UIColorFromRGB(0Xa8936f) colorWithAlphaComponent:0.9] forKey:@"6"]; //VENERDI'
 
    //COLOR
    [crea_comune_label setTextColor:pickerColor];
    
    tabella.backgroundView.hidden = YES;
    [tabella setBackgroundColor:[UIColor clearColor]];
    
    //AREA SCEGLI GIORNO
    //CAGradientLayer *gradient = [CAGradientLayer layer];
    //gradient.frame = area_scegli_giorno.bounds;
    //gradient.colors = [NSArray arrayWithObjects:(id)[[UIColorFromRGB(0X703f0b) colorWithAlphaComponent:0.01] CGColor],(id)[[UIColorFromRGB(0X703f0b) colorWithAlphaComponent:0.35] CGColor], (id)[[UIColorFromRGB(0X703f0b) colorWithAlphaComponent:0.01] CGColor], nil];
    //[area_scegli_giorno.layer insertSublayer:gradient atIndex:0];
    [area_scegli_giorno.layer setCornerRadius:10];
    [area_scegli_giorno.layer setMasksToBounds:YES];

    
    //PICKER LINGUA
    list_lingua_img = [LinguaObject getLingue];
    linguaPos = [[NSMutableDictionary alloc] init];
    for(int j=0; j< [list_lingua_img count];j++){
        LinguaObject *l = [[LinguaObject getLingue] objectAtIndex:j];
        [linguaPos setObject:[NSNumber numberWithInt:j] forKey:l.sigla];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(MainReload_listaView)
                                                 name:AGGIORNA_LISTA_MESSE
                                               object:nil];

    // NSLog(@"list_lingua_img %@",list_lingua_img);
    
    _loader_uno.lineWidth = 4.0;
    _loader_uno.color = [UIColorFromRGB(0X998371) colorWithAlphaComponent:0.3];


    _loader_due.lineWidth = 4.0;
    _loader_due.color = [UIColorFromRGB(0X998371) colorWithAlphaComponent:0.3];

    
}

-(void)localizza{
    //LOCALIZZAZIONE:
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = crea_comune_label.font.pointSize;
    style.maximumLineHeight = crea_comune_label.font.pointSize;
    [style setAlignment:NSTextAlignmentRight];
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    //set text with attribute
    crea_comune_label.attributedText = [[NSAttributedString alloc] initWithString:[NSLocalizedString(@"creaSpeciale", nil) uppercaseString] attributes:attributtes];

    
    [crea_feriale_label setText:[NSLocalizedString(@"creaNormale", nil) uppercaseString]];
    [title_lista_messa setText:[NSLocalizedString(@"ListaMesse", nil) uppercaseString]];
    [title_messa_ctrl setText:[NSLocalizedString(@"titolo_crea", nil) uppercaseString]];
    [label_data_messa setText:[NSLocalizedString(@"label_data_messa", nil) uppercaseString]];
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
}


-(void)configureView{
    // NSLog(@"MessaLista Will configureView");
    
    LinguaObject *l = [[Home getCurrentDay] linguaGiorno];
    [button_crea_comune setEnabled:[Fix checkComuniForLang:l.sigla]];
    
    [self initListPicker];
    
    [picker_ctrl_day selectItemAtIndex:1 animated:NO];
    
    
	UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
	v.backgroundColor = [UIColor clearColor];
	[self.tabella setTableFooterView:v];
	[self.tabella setTableHeaderView:v];
    [self AttivaRotella];
    
}

//=========================
// Appear
//=========================
#pragma mark - Appear
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureView];
}

- (void)viewDidAppear:(BOOL)animated {
    // NSLog(@"MessaLista DIDAppear");
    [super viewDidAppear:animated];
    if(![self.defaults boolForKey:TUTORIAL_MESSA_LISTA_VISTO]){
        [self.defaults setBool:YES forKey:TUTORIAL_MESSA_LISTA_VISTO];
        [self.defaults synchronize];
        [self showTutorial:nil];
    }
    [self MainReload_listaView];
    
}

//=========================
// Disappear
//=========================
#pragma mark - Disappear
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self activityNetwork:NO];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

//=========================
// LINGUA DELEGATE
//=========================
#pragma mark - LINGUA DELEGATE
-(IBAction)showLingua:(UIButton*)sender
{
    // NSLog(@"showLingua");
	linguaCTRL = [[Lingua alloc] initWithDelegate:self andIsLista:YES];
	linguaCTRL.view.alpha = 0;
    
    if(!self.supportBlur){
        blurView = [[FXBlurView alloc] initWithFrame:self.view.frame];
        [self.blurView setDynamic:NO];
        [self.blurView setTintColor:[UIColor brownColor]];
        self.blurView.alpha = 0;
        [self.view addSubview:self.blurView];
    }
    else{
        [linguaCTRL.view setBackgroundColor:[UIColor clearColor]];


        [linguaCTRL blurView:linguaCTRL.view
             withEffectStyle:UIBlurEffectStyleDark];
    }

    
	[self.view addSubview:linguaCTRL.view];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         linguaCTRL.view.alpha = 1;
                         self.blurView.alpha = 1;
                     } completion:^(BOOL finished) {
                         ;
                     }];
}

-(void)removeLingua{
    // NSLog(@"FineInfo");
    linguaCTRL.view.alpha = 1;
    [self rotateImage:info_img withDuration:0.6 onY_axis:YES];

    [UIView animateWithDuration:0.4
                     animations:^{
                         linguaCTRL.view.alpha = 0;
                         self.blurView.alpha = 0;
                     } completion:^(BOOL finished) {
                         [self.blurView removeFromSuperview];
                         [linguaCTRL.view removeFromSuperview];
                         self.blurView =  nil;
                         linguaCTRL =  nil;
                     }];

}

#pragma mark -
#pragma mark ROTELLA
//=========================
// ROTELLA
//=========================
-(BOOL)AttivaRotella{
    [self performSelectorOnMainThread:@selector(AttivaRotellaMain) withObject:nil waitUntilDone:YES];
    return YES;
}

-(void)AttivaRotellaMain{
    rotella.transform = CGAffineTransformMakeScale(1.2f, 1.2f);
    [rotella startAnimating];
    [rotella setHidden:NO];
}

-(void)rimuoviRotella{
    if(processoMessaLista>0) return;
    [self performSelectorOnMainThread:@selector(rimuoviRotellaMM) withObject:nil waitUntilDone:NO];
}
-(void)rimuoviRotellaMM{
    [rotella startAnimating];
    [rotella setHidden:NO];
    [self performSelector:@selector(rimuoviRotellaDelay) withObject:nil afterDelay:0.3];
}

-(void)rimuoviRotellaDelay{
    [rotella setHidden:YES];
}



#pragma mark -
#pragma mark AZIONI
//=========================
// AZIONI
//=========================
- (IBAction)azioneCTRL_Day:(UIButton*)sender
{
    int pos = [picker_ctrl_day rigaSelezionata];
    // NSLog(@"azioneCTRL Day pos:%i",pos);
    if (sender == dx_btn) {
        //CASO DX
        if (pos<(list_down.count - 1)) {
            pos +=1;
            [picker_ctrl_day selectItemAtIndex:pos animated:YES];
        }
        
        [sx_btn setAlpha:1];
        [dx_btn setAlpha:(pos==(list_down.count - 1))?0.3:1];
        // NSLog(@"DX selected:%i",pos);
        return;
    }
    //CASO SX
    if (pos!=0) {
        pos-=1;
        [picker_ctrl_day selectItemAtIndex:pos animated:YES];
    }
    // NSLog(@"SX selected:%i",pos);
    [sx_btn setAlpha:(pos==0)?0.3:1];
    [dx_btn setAlpha:1];

}

- (IBAction)azioneCTRL_Lang:(UIButton*)sender
{
    NSInteger pos = [picker_ctrl_lingua selectedRowInComponent:0];
    // NSLog(@"azioneCTRL Day pos:%i",pos);
    if (sender == down_btn) {
        //CASO UP
        if (pos<(list_down.count - 1)) {
            [picker_ctrl_lingua selectRow:pos+1 inComponent:0 animated:YES];
        }
        [down_btn setAlpha:([picker_ctrl_lingua selectedRowInComponent:0]==[list_lingua_img count]-1)?0.3:1];
        [up_btn setAlpha:([picker_ctrl_lingua selectedRowInComponent:0]==0)?0.3:1];
        return;
    }
    //CASO DOWN
    if (pos!=0) {
        [picker_ctrl_lingua selectRow:pos-1 inComponent:0 animated:YES];
        [down_btn setAlpha:([picker_ctrl_lingua selectedRowInComponent:0]==[list_lingua_img count]-1)?0.3:1];
        [up_btn setAlpha:([picker_ctrl_lingua selectedRowInComponent:0]==0)?0.3:1];
    }
}

#pragma mark -
#pragma mark ActivityOnButton
//==============================
// ActivityOnButton x creaMessa
//==============================
-(void)startActivityOn:(BLLoader*)sender{
   //DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:UIColorFromRGB(0XB79F8A) size:40.0f];
//    DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColorFromRGB(0Xd7be48) colorWithAlphaComponent:0.8] size:50.0f];
//    
    [self activityNetwork:YES];
    
//    CGPoint center = (CGPoint){(sender.frame.size.width/2.)+sender.frame.origin.x,(sender.frame.size.height/2.)+sender.frame.origin.y};
//    //[loader setCenter:center];
//    [loader setTag:1234];
    sender.hidden = NO;

    [sender startAnimation];
    
    //sotto
    //[sender.superview insertSubview:activityIndicatorView belowSubview:sender];
    
    //sopra
    //[sender.superview insertSubview:loader aboveSubview:sender];
}

-(void)stopActivityOn:(BLLoader*)sender{
    [sender stopAnimation];
}


#pragma mark -
#pragma mark CREA MESSA DAY
//=========================
// CREA MESSA DAY
//=========================
- (IBAction)azioneCreaMessaDay:(UIButton*)sender
{
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        //[self AttivaRotella];
        [self startActivityOn:self.loader_uno];
        // NSLog(@"azioneCreaMessaDay");
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        [self azioneCreaMessaDay2];
    }];//fine step2
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue currentQueue] addOperation:step2];
}

-(void)azioneCreaMessaDay2{
    // NSLog(@"azioneCreaMessaDay2:%i",[NSThread isMainThread]);
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    
    LinguaObject *lingua = [[LinguaObject getLingue] objectAtIndex:[picker_ctrl_lingua selectedRowInComponent:0]];
    
    __block Day *day = [Day searchForDay:[list_down objectAtIndex:[picker_ctrl_day rigaSelezionata]] withLang:lingua.sigla];
        
    if(day==nil)
        {
            // NSLog(@"=== GIORNO NON PRESENTE ===");
            if(self.connesso){
                [Day scaricaGiornoconData:[list_down objectAtIndex:[picker_ctrl_day rigaSelezionata]] withLang:lingua.sigla eventFlag:[TestoProprio getTestoProprioWithKey:@""] completionBlock:^(Day *ddd){
                    // NSLog(@"DAY DOWNLOADED!!:%@",day);
                    day = ddd;
                    dispatch_semaphore_signal(sem);
                    return ddd;
                }];
            }
            else{
                [self noMessaNet];
                dispatch_semaphore_signal(sem);
                return;
                }
            }
    else{
        // NSLog(@"=== GIORNO PRESENTE ===");
        dispatch_semaphore_signal(sem);
    }


    
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    if(day==nil)
        day = [Day searchForDay:[list_down objectAtIndex:[picker_ctrl_day rigaSelezionata]] withLang:lingua.sigla];
        
        if(day!=nil){
            Messa *messa = [Messa addMessaWithDay:day forContext:[AppDelegate mainManagedObjectContext]];
            [Home showMessaTitolo:messa thenComune:NO];
        }

}

#pragma mark -
#pragma mark CREA MESSA COMUNE
//============================
// CREA MESSA COMUNE
//============================
- (IBAction)azioneCreaMessaComune:(UIButton*)sender
{
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        //[self AttivaRotella];
        [self startActivityOn:self.loader_due];
        // NSLog(@"azioneCreaMessaCOMUNE");
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        [self azioneCreaMessaComune2];
    }];//fine step2
    
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue currentQueue] addOperation:step2];
}

-(void)azioneCreaMessaComune2{
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    LinguaObject *lingua = [[LinguaObject getLingue] objectAtIndex:[picker_ctrl_lingua selectedRowInComponent:0]];
    Day *day = [Day searchForDay:[list_down objectAtIndex:[picker_ctrl_day rigaSelezionata]] withLang:lingua.sigla];
    
    if(day==nil)
    {
        // NSLog(@"=== GIORNO NON PRESENTE ===");
        if(self.connesso){
            [Day scaricaGiornoconData:[list_down objectAtIndex:[picker_ctrl_day rigaSelezionata]] withLang:lingua.sigla eventFlag:[TestoProprio getTestoProprioWithKey:@""] completionBlock:^(Day *ddd){
                // NSLog(@"DAY DOWNLOADED!!:%@",day);
                dispatch_semaphore_signal(sem);
                return ddd;
            }];
        }
        else{
            [self noMessaNet];
            dispatch_semaphore_signal(sem);
            return;
        }
    }
    else{
        // NSLog(@"=== GIORNO PRESENTE ===");
        dispatch_semaphore_signal(sem);
    }
    
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    if(day==nil)
        day = [Day searchForDay:[list_down objectAtIndex:[picker_ctrl_day rigaSelezionata]] withLang:lingua.sigla];
    
    if(day!=nil){
        Messa *messa = [Messa addMessaWithDay:day forContext:[AppDelegate mainManagedObjectContext]];
        [messa setIsComune:YES];
        [Home showMessaTitolo:messa thenComune:YES];
    }


}

- (IBAction)azioneDismiss:(UIButton*)sender
{
    [self dismissViewControllerAnimated:YES completion:^{NSLog(@"azioneDismiss completata");}];
}

#pragma mark RESET
//=========================
// RESET
//=========================
-(IBAction)downSelettore:(UIButton*)sender{
    [reset_label setTextColor:hightlight_color];
  }

-(IBAction)outSelettore:(UIButton*)sender{
    [reset_label setTextColor:reset_color];
}

-(IBAction)azione_resetta:(UIButton*)sender{
    
    UIAlertView *resetAlert = [[UIAlertView alloc] initWithTitle:@"RESET" message:NSLocalizedString(@"reset_day_confirm", nil) delegate:self cancelButtonTitle:[NSLocalizedString(@"risposta_annulla", nil) uppercaseString] otherButtonTitles:[NSLocalizedString(@"risposta_si", nil) uppercaseString] ,nil];
    
    if(self.isIos7orUpper)
    {
        UIView *mask = [[UIView alloc] initWithFrame:CGRectMake(-120, -100, 240, 230)];
        [mask setBackgroundColor:[UIColor clearColor]];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(72, 25, 17, 17)];
        [imageView setAlpha:0.8];
        NSString *path = [[NSString alloc] initWithString:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"erro@2x.png"]];
        UIImage *bkgImg = [[UIImage alloc] initWithContentsOfFile:path];
        [imageView setImage:bkgImg];
    
    
        [mask addSubview:imageView];
        UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        [view2 setBackgroundColor:[UIColor clearColor]];
        [view2 addSubview:mask];
        [resetAlert setValue:view2 forKey:@"accessoryView"];
    }

    [resetAlert setTag:1];
    [resetAlert show];
}

-(void)resettaLista{
    [self AttivaRotella];
    processoMessaLista = 1;
    [Messa deleteAll];
    [reset_label setTextColor:hightlight_color];
    // NSLog(@"resettaLista");
}

-(void)fineResetta{
    [rotella setHidden:YES];
    [reset_label setTextColor:reset_color];
    processoMessaLista = 0;
    [self rimuoviRotella];
    // NSLog(@"fineResetta:%@",list_day);
    [self MainReload_listaView];
}

#pragma mark ALERTVIEW delegate
//=========================
// ALERTVIEW delegate
//=========================
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
        switch (buttonIndex) {
            case 0:
                // NSLog(@"NO premuto");
                [reset_label setTextColor:reset_color];
                break;
            case 1:
                // NSLog(@"Conferma Reset");
                [self aggiungiOperazione:@selector(resettaLista)];
                [self aggiungiOperazione:@selector(fineResetta)];
                break;
            default:
                return;
                break;
        }
    }
}



#pragma mark RELOAD LISTA
//=========================
// RELOAD LISTA
//=========================
-(void)MainReload_listaView{
    list_day = [NSMutableArray arrayWithArray:[Messa getMesseIn:[AppDelegate mainManagedObjectContext]]];
    [self performSelectorOnMainThread:@selector(MainReloadMM) withObject:nil waitUntilDone:YES];
    [self rimuoviRotella];
}

-(void)MainReloadMM{
    // NSLog(@"MessaLista-MainReload");
    [tabella reloadData];
}


#pragma mark -
#pragma mark ->> Table view
#pragma mark -
#pragma mark --------> Table view
#pragma mark Header/Footer
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// HEADER SESSION
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

// FOOTER SESSION
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

#pragma mark ROW

//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    [tabella setAlpha:(processoMessaLista==1)?0.6:1];
    NSInteger i = [list_day count];
    [tableView setScrollEnabled:(i>7)];
    return i;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 60.0;
}



#pragma mark CELL
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    // NSLog(@"cellForRowAtIndexPath:%@",indexPath);
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		UIView* backgroundView = [[UIView alloc ] initWithFrame:CGRectZero ];
		backgroundView.backgroundColor = [UIColor clearColor];
		cell.backgroundView.alpha = 0;
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.alpha = 0;
		cell.contentView.backgroundColor = [UIColor clearColor];
		[cell setBackgroundView: backgroundView];
        cell.textLabel.backgroundColor =[UIColor clearColor];
        cell.textLabel.text = @"";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [tabella setAlpha:(processoMessaLista==1)?0.6:1];
    
    //CUSTOM LABEL
    UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, tabella.frame.size.width, 59)];
    [Label setBackgroundColor:[UIColor colorWithRed:215/255.f green:170/255.f blue:105/255.f alpha:0.4]];
    [Label.layer setMasksToBounds:YES];
    [cell.contentView addSubview:Label];
    
    
    Messa *m = [list_day objectAtIndex:indexPath.row];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];        
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *data_giorno = [dateFormatter dateFromString:m.day.id_date];
    
    //-->NUMERO DELLA SETTIMANA
    UILabel *numeroGiorno = [[UILabel alloc] initWithFrame:CGRectMake(0, -7, 50, 50)];
    [numeroGiorno setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:25]];
    [numeroGiorno setTextAlignment:NSTextAlignmentCenter];
    [numeroGiorno setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.9]];
    [numeroGiorno setBackgroundColor:[UIColor clearColor]];
    [dateFormatter setDateFormat:@"dd"];
    [numeroGiorno setText:[dateFormatter stringFromDate:data_giorno]];

    //-->GIORNO DELLA SETTIMANA
    UILabel *giornoSett = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, 50, 20)];
    [giornoSett setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14]];
    [giornoSett setTextAlignment:NSTextAlignmentCenter];
    [giornoSett setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7]];
    [dateFormatter setDateFormat:@"EEE"];
    [giornoSett setBackgroundColor:[UIColor clearColor]];
    [giornoSett setText:[[dateFormatter stringFromDate:data_giorno] uppercaseString]];
    
    //-> CERCHIO
    UIView *cerchioUno = [[UIView alloc] initWithFrame:CGRectMake(12, 5, 50, 50)];
    [cerchioUno setBackgroundColor:[[m.day messaColorWeek] colorWithAlphaComponent:0.7]];
    [cerchioUno.layer setCornerRadius:25];
    [cerchioUno.layer setMasksToBounds:YES];
    [cerchioUno setTag:999];
    [cerchioUno addSubview:numeroGiorno];
    [cerchioUno addSubview:giornoSett];
    
    
    //-> MESE STRINGA
	UILabel *giorno = [[UILabel alloc] initWithFrame:CGRectMake(65, 0, 120, 59)];
	[giorno setBackgroundColor:[UIColor clearColor]];
    [giorno setFont:[UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size:22]];
    [giorno setTextColor:defaulf_color];
	[giorno setTextAlignment:NSTextAlignmentCenter];
    [giorno setAdjustsFontSizeToFitWidth:YES];
    [giorno setMinimumScaleFactor:0.5];
    [dateFormatter setDateFormat:@"MMMM"];
	[giorno setText:[[dateFormatter stringFromDate:data_giorno] capitalizedString]];
	giorno.numberOfLines = 1;
    
    //-> ANNO STRINGA
	UILabel *anno = [[UILabel alloc] initWithFrame:CGRectMake(102, 5, 100, 55)];
	[anno setBackgroundColor:[UIColor clearColor]];
    [anno setFont:[UIFont fontWithName:FONT_HELVETICANEUE_CONDENSEDBLACK size:45]];
    [anno setTextColor:[UIColorFromRGB(0Xddc29b) colorWithAlphaComponent:0.4]];
	[anno setTextAlignment:NSTextAlignmentLeft];
    [dateFormatter setDateFormat:@"YYYY"];
	[anno setText:[[dateFormatter stringFromDate:data_giorno] capitalizedString]];
	anno.numberOfLines = 1;
    
    //-->LINGUA
    LinguaObject *lingua = m.day.linguaGiorno;
    
    
    //LOGO Lingua
    UIView *logo1 = lingua.view;
    CGRect  logo_frame =  logo1.frame;
     logo_frame.origin.x = Label.frame.size.width-(logo_frame.size.width);
     logo_frame.origin.y = (Label.frame.size.height-logo_frame.size.height)/2.0;
    [logo1 setFrame:logo_frame];

    UIView* schiarire = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, Label.frame.size.width-(logo_frame.size.width), 70)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = schiarire.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColorFromRGB(0Xded6c9) colorWithAlphaComponent:0.9] CGColor], (id)[[[UIColor whiteColor] colorWithAlphaComponent:0.2] CGColor],(id)[[[UIColor whiteColor] colorWithAlphaComponent:0.2] CGColor], nil];
    
    [gradient setStartPoint:CGPointMake(0.0, 0.5)];
    [gradient setEndPoint:CGPointMake(1.0, 0.5)];
    
    [schiarire.layer insertSublayer:gradient atIndex:0];
    
    
    //-> TITOLO_MESSA
    // NSLog(@"titolo messa:%@",m.messa_name);
    if(m.messa_name==nil) m.messa_name = m.day.title;
    UILabel *titoloMessa = [[UILabel alloc] initWithFrame:CGRectMake(180, 0, Label.frame.size.width-(logo_frame.size.width+180), 59)];
	[titoloMessa setBackgroundColor:[UIColor clearColor]];
    [titoloMessa setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:20]];
    [titoloMessa setTextColor:UIColorFromRGB(0X745f4d)];
    NSMutableParagraphStyle *style  = [NSMutableParagraphStyle new];
    style.minimumLineHeight = titoloMessa.font.pointSize;
    style.maximumLineHeight = titoloMessa.font.pointSize;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style};
    //set text with attribute
    titoloMessa.attributedText = [[NSAttributedString alloc] initWithString:[m.messa_name uppercaseString]
                                                                       attributes:attributtes];

	[titoloMessa setTextAlignment:NSTextAlignmentCenter];
    [titoloMessa setAdjustsFontSizeToFitWidth:YES];
    [titoloMessa setMinimumScaleFactor:0.5];
	titoloMessa.numberOfLines = 2;
    
    [Label addSubview:schiarire];
    [Label addSubview:cerchioUno];
    if(m.isComune)
    {
        //-> CERCHIO COMUNE
        //UILabel *cerchioComune = [[UILabel alloc] initWithFrame:CGRectMake(48, 4, 18, 18)];
        UILabel *cerchioComune = [[UILabel alloc] initWithFrame:CGRectMake(49, 4, 18, 18)];
        [cerchioComune setBackgroundColor:[UIColorFromRGB(0X744414) colorWithAlphaComponent:0.7]];
        [cerchioComune setBackgroundColor:[UIColorFromRGB(0XFFFFFF) colorWithAlphaComponent:0.85]];
        [cerchioComune.layer setCornerRadius:cerchioComune.frame.size.width/2.0];
        [cerchioComune.layer setBorderColor:[[UIColorFromRGB(0XFF0000) colorWithAlphaComponent:0.8] CGColor]];
        [cerchioComune.layer setBorderWidth:1.5];
        [cerchioComune.layer setCornerRadius:cerchioComune.frame.size.width/2.0];
        
        [cerchioComune setTextColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7]];
        [cerchioComune setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:16]];
        [cerchioComune.layer setMasksToBounds:YES];
        NSMutableParagraphStyle *style  = [NSMutableParagraphStyle new];
        style.minimumLineHeight = cerchioComune.frame.size.height-2;
        style.maximumLineHeight = cerchioComune.frame.size.height-2;
        style.alignment = NSTextAlignmentCenter;
        //NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
        //set text with attribute
        //cerchioComune.attributedText = [[NSAttributedString alloc] initWithString:@"c" attributes:attributtes];
        
        [Label addSubview:cerchioComune];
    }
    [Label addSubview:anno];
    [Label addSubview:giorno];
    [Label addSubview:logo1];
    [Label addSubview:titoloMessa];
    
    
    if(cellaMessaSelezionata==indexPath.row){
        // NSLog(@"cellaInDownload=%i",cellaMessaSelezionata);
    }
    
    BOOL isNotCompleted = (m.sel_prefazio.intValue == -1 || m.sel_preghieraEucaristica.intValue == -1);
    
    if(isNotCompleted)
    {
        UIImageView *incompleto = [[UIImageView alloc] initWithFrame:CGRectMake(10, 4, 12, 12)];
        [incompleto setBackgroundColor:[UIColor clearColor]];
        [incompleto setContentMode:UIViewContentModeScaleAspectFit];
        [incompleto setAlpha:0.9];
        [incompleto setImage:[UIImage imageNamed:@"erro.png"]];
        [Label addSubview:incompleto];

        UILabel *incompleto_label = [[UILabel alloc] initWithFrame:CGRectMake(49, 4, 18, 18)];
        [incompleto_label setBackgroundColor:[UIColor clearColor]];
        [Label addSubview:incompleto_label];

    }


    //Label on cell
    [cell.contentView addSubview:Label];
    return cell;
}


#pragma mark -
#pragma mark EDIT TABLE
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#ifdef __IPHONE_8_0
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *shareAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"      " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        // maybe show an action sheet with more options
        // NSLog(@"Share pressed!");
        [self creaPDFConMessa:indexPath];
        //[tableView setEditing:NO];
        // NSLog(@"Fine Share pressed!");
    }];
    //shareAction.backgroundColor = UIColorFromRGB(0X007aff);
    shareAction.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"share_icon_cell.png"]];

    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"      " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self doDelete:indexPath];
    }];
    deleteAction.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"delete_icon_cell.png"]];

    
    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"      " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self doEdit:indexPath];
    }];
    editAction.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"edit_icon_cell.png"]];

    return @[deleteAction, shareAction, editAction];
}


#endif

// From Master/Detail Xcode template
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self doDelete:indexPath];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

-(void)doEdit:(NSIndexPath *)indexPath{
    Messa *edit = [list_day objectAtIndex:indexPath.row];
    NSLog(@"doEdit ->%@",edit);
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        //BLoccare la vista
        [self avviaRotellaEdit:indexPath];
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        [Home showEditModificaTitolo:edit];
    }];//fine step2
    
    
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
}

-(void)avviaRotellaEdit:(NSIndexPath*)indexPath{
    [self.view setUserInteractionEnabled:NO];
    CGRect frame = [[tabella cellForRowAtIndexPath:indexPath] frame];
    frame.origin.y += tabella.frame.origin.y;
    frame.origin.x = 308.f;
    frame.size.width = 40.f;
    self.rotellaEdit = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self.rotellaEdit setAlpha:0.8];
    [self.rotellaEdit setTransform:CGAffineTransformMakeScale(1.2f, 1.2f)];
    [self.rotellaEdit setCenter:CGPointMake(frame.origin.x+(frame.size.width/2.f), frame.origin.y+(frame.size.height/2.f))];
    [tabella.superview addSubview:self.rotellaEdit];
    [self.rotellaEdit startAnimating];
    [self.rotellaEdit setHidden:NO];
    [self.view setUserInteractionEnabled:YES];
    // NSLog(@"rotella Avviata");
}

-(void)rimuoviRotellaEdit{
    [self.rotellaEdit startAnimating];
    [self.rotellaEdit setHidden:NO];
    [self.rotellaEdit removeFromSuperview];
}


-(void)doDelete:(NSIndexPath *)indexPath{
    // NSLog(@"delete AVVIATO ->%ld",(long)indexPath.row);
    Messa *delete = [list_day objectAtIndex:indexPath.row];
    
    if([Home getCurrentMessa] == delete)
    {
        [tabella reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                       withRowAnimation: UITableViewRowAnimationBottom];
        return;
    }
    NSError *err = nil;
    if([Messa deleteMessa:delete andError:&err forContext:[AppDelegate mainManagedObjectContext]]){
        [list_day removeObjectAtIndex:indexPath.row];
        [tabella deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else{
        // NSLog(@"errore cancellando %@:%@",delete,[err description]);
    }

}

#pragma mark -
#pragma mark Table view DIDSELECT

// COMANDA DOWNLOAD TESTI PROPRI DAY
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // NSLog(@"didSelectRowAtIndexPath:%i",(int)indexPath.row);
    cellaMessaSelezionata = (int)indexPath.row;
    
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"ListaMessa: RING EFFECT 1");
        [self ringLine:indexPath];
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"ListaMessa: DISMISS POPOVER 2");
        Home *h = [Home sharedInstance];
        [h.messa_popover dismissPopoverAnimated:YES];
    }];//fine step2
    
    //========
    // STEP 3
    //========
    NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"ListaMessa: mostraMessa 3");
        cellaMessaSelezionata = -1;
        [Home mostraMessa:[list_day objectAtIndex:indexPath.row] editMode:NO];
    }];//fine step3
    
    /*
     performSelectorInBackground
     */
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    [[NSOperationQueue mainQueue] addOperation:step3];
}

-(void)pulseLine:(NSIndexPath *)indexPath{
    UIView *anello = [[UIView alloc] initWithFrame:CGRectMake(12, 5, 50, 50)];
    [anello setBackgroundColor:[UIColor clearColor]];
    [anello.layer setBorderWidth:4];
    [anello.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:1] CGColor]];
    CGFloat offset = 3.5;
    CGRect split = anello.frame;
    split.origin.x -=offset;
    split.origin.y -=offset;
    split.size.width += offset*2;
    split.size.height += offset*2;
    
    [anello setFrame:split];
    [anello.layer setCornerRadius:split.size.height/2];
    
    [[tabella cellForRowAtIndexPath:indexPath].contentView addSubview:anello];
    [self aggiungiRotellaAllaLinea:indexPath];
    
    
    CABasicAnimation *theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    theAnimation.duration=0.35;
    theAnimation.repeatCount=3;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:0.0];
    theAnimation.toValue=[NSNumber numberWithFloat:1.0];
    [anello.layer addAnimation:theAnimation forKey:@"animateOpacity"];

}

-(void)aggiungiRotellaAllaLinea:(NSIndexPath *)indexPath{
    UIView *anello = [[UIView alloc] initWithFrame:CGRectMake(12, 5, 50, 50)];
    UIActivityIndicatorView *rotellaDownload = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [rotellaDownload setColor:[[UIColor whiteColor] colorWithAlphaComponent:0.2]];
    [rotellaDownload setTransform:CGAffineTransformMakeScale(1.1f, 1.1f)];
    [rotellaDownload startAnimating];
    [rotellaDownload setCenter:anello.center];
    [rotellaDownload setAlpha:0];
    [[tabella cellForRowAtIndexPath:indexPath].contentView addSubview:rotellaDownload];
    [UIView animateWithDuration:0.2 animations:^{ [rotellaDownload setAlpha:1];}];

}

-(void)ringLine:(NSIndexPath *)indexPath
{
    UIView *anello = [[UIView alloc] initWithFrame:CGRectMake(12, 5, 50, 50)];
    [anello setBackgroundColor:[UIColor clearColor]];
    //[anello.layer setBorderWidth:4];
    //[anello.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:1] CGColor]];
    CGFloat offset = 3.5;
    CGRect split = anello.frame;
    split.origin.x -=offset;
    split.origin.y -=offset;
    split.size.width += offset*2;
    split.size.height += offset*2;
    
    [anello setFrame:split];
    [anello.layer setCornerRadius:split.size.height/2];
    
    
    
    [[tabella cellForRowAtIndexPath:indexPath].contentView addSubview:anello];
    
    UIColor *stroke = [UIColor whiteColor];
    
    CABasicAnimation *borderAnimation = [CABasicAnimation animationWithKeyPath:@"borderColor"];
    borderAnimation.fromValue = (id)[UIColor clearColor].CGColor;
    borderAnimation.toValue = (id)stroke.CGColor;
    borderAnimation.duration = 0.5f;
    [anello.layer addAnimation:borderAnimation forKey:nil];
    
    CGRect pathFrame = CGRectMake(-CGRectGetMidX(anello.bounds), -CGRectGetMidY(anello.bounds), anello.bounds.size.width, anello.bounds.size.height);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:pathFrame cornerRadius:anello.layer.cornerRadius];
    
    UIView *dove;
    if(self.isIpad){
        dove = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    }
    else dove = self.view;
    CGPoint shapePosition = [dove convertPoint:anello.center fromView:anello.superview];

    
    CAShapeLayer *circleShape = [CAShapeLayer layer];
    circleShape.path = path.CGPath;
    circleShape.position = shapePosition;
    circleShape.fillColor = [UIColor clearColor].CGColor;
    circleShape.opacity = 0;
    circleShape.strokeColor = stroke.CGColor;
    circleShape.lineWidth = 4;
    
    [dove.layer addSublayer:circleShape];
    
    
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    scaleAnimation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(2.5, 2.5, 1)];
    
    CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    alphaAnimation.fromValue = @1;
    alphaAnimation.toValue = @0;
    
    CAAnimationGroup *animation = [CAAnimationGroup animation];
    animation.animations = @[scaleAnimation, alphaAnimation];
    animation.duration = 0.5f;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    [circleShape addAnimation:animation forKey:nil];
    [self aggiungiRotellaAllaLinea:indexPath];
}

//==================================
// CONDIVIDI MESSA
//==================================
#pragma mark - CONDIVIDI MESSA -
NSIndexPath *shareIndex;

-(void)avviaRotellaShare:(NSIndexPath*)indexPath{
    [self.view setUserInteractionEnabled:NO];
    CGRect frame = [[tabella cellForRowAtIndexPath:indexPath] frame];
    frame.origin.y += tabella.frame.origin.y+10.f;
    frame.origin.x = 360.f;
    frame.size.width = 51.f;
    frame.size.height -= 20.f;
    rotellaShare = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [rotellaShare setAlpha:0.8];
    [rotellaShare setTransform:CGAffineTransformMakeScale(1.2f, 1.2f)];
    [rotellaShare setCenter:CGPointMake(1.f+frame.origin.x+(frame.size.width/2.f), 2.9f+frame.origin.y+(frame.size.height/2.f))];
    [tabella.superview addSubview:rotellaShare];
    [rotellaShare startAnimating];
    [rotellaShare setHidden:NO];
    // NSLog(@"rotella Avviata");
}

-(void)rimuoviRotellaShare{
    [rotellaShare startAnimating];
    [rotellaShare setHidden:NO];
    [rotellaShare removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
}

-(IBAction)condividiMessa:(NSIndexPath*)indexPath{
    // NSLog(@"self.isLand?%i frame:%@",self.isLand,NSStringFromCGRect(self.view.frame));
    [self creaPDFConMessa:indexPath];
}

-(void)creaPDFConMessa:(NSIndexPath*)indexPath
{
    //========
    // STEP 1
    //========
    __block Messa *m = [list_day objectAtIndex:indexPath.row];
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        //BLoccare la vista
        [self avviaRotellaShare:indexPath];
        shareIndex = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        [self alert:m.messa_name
             andMex:NSLocalizedString(@"MessaSharePdfText", nil)
     optionOneTitle:NSLocalizedString(@"MessaSharePdfOmeliaSI", nil)
          optionOne:^(UIAlertAction *action) {
              NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
                  [self creaPDFConMessaStep1:m conOmelia:YES];
              }];//fine step3
              [[NSOperationQueue mainQueue] addOperation:step3];
             }
     optionTwoTitle:NSLocalizedString(@"MessaSharePdfOmeliaNO", nil)
          optionTwo:^(UIAlertAction *action) {
              NSBlockOperation *step3 = [NSBlockOperation blockOperationWithBlock:^{
                  [self creaPDFConMessaStep1:m conOmelia:NO];
              }];//fine step3
              [[NSOperationQueue mainQueue] addOperation:step3];
          }];
    }];//fine step2
    
    
    
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    
}

-(void)creaPDFConMessaStep1:(Messa*)m conOmelia:(BOOL)visualizzaOmelia{
    // NSLog(@"creaPDFConMessa:%@",m);
    htmlPdfKit = [[BNHtmlPdfKit alloc] initWithPageSize:BNPageSizeA4];
    htmlPdfKit.delegate = self;
    NSString *messa_name = m.messa_name;
    if(messa_name==nil) messa_name = m.day.title;
    [htmlPdfKit saveHtmlAsPdf:[m htmlPdfStringWithOmelia:visualizzaOmelia] toFile:[self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"iBreviary - %@.pdf",messa_name]]];
}

//==================================
// PDF DELEGATE
//==================================
#pragma mark - PDF DELEGATE -
- (void)htmlPdfKit:(BNHtmlPdfKit *)h didSavePdfFile:(NSString *)file{
    // NSLog(@"didSavePdfFile[%i]:%@", h.pageSize,file);
    //========
    // STEP 1
    //========
    NSBlockOperation *step1 = [NSBlockOperation blockOperationWithBlock:^{
        self.documentInteractionCtrl = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:file]];
        [self.documentInteractionCtrl setUTI:@"com.adobe.pdf"];
        
        self.documentInteractionCtrl.delegate = self;
        CGRect frame = [[tabella cellForRowAtIndexPath:shareIndex] frame];
        frame.origin.y += tabella.frame.origin.y+10.f;
        frame.origin.x = 360.f;
        frame.size.width = 51.f;
        frame.size.height -= 20.f;
        // NSLog(@"presenting documentCTRL:%@ \n%@",self.documentInteractionCtrl,NSStringFromCGRect(frame));
        [self.documentInteractionCtrl presentOptionsMenuFromRect:frame inView:tabella.superview animated:YES];
        
    }];//fine step1
    
    //========
    // STEP 2
    //========
    NSBlockOperation *step2 = [NSBlockOperation blockOperationWithBlock:^{
        [self.view setUserInteractionEnabled:YES];
        shareIndex = nil;
        [self rimuoviRotellaShare];
    }];//fine step2
    [step2 addDependency:step1];
    [[NSOperationQueue mainQueue] addOperation:step1];
    [[NSOperationQueue mainQueue] addOperation:step2];
    
}
- (void)htmlPdfKit:(BNHtmlPdfKit *)htmlPdfKit didFailWithError:(NSError *)error{
    // NSLog(@"didFailWithError:%@",[error description]);
    
}

#pragma mark - document delegate
-(void)documentInteractionController:controller willBeginSendingToApplication:(NSString *)application{
    // NSLog(@"documentInteractionController:%@",application);
}

-(void)documentInteractionControllerWillPresentOptionsMenu:(UIDocumentInteractionController *)controller{
    // NSLog(@"documentInteractionControllerWillPresentOptionsMenu");
}
-(void)documentInteractionControllerDidDismissOptionsMenu:(UIDocumentInteractionController *)controller{
    // NSLog(@"documentInteractionControllerDidDismissOptionsMenu");
    [tabella setEditing:NO];
}

#pragma mark -
#pragma mark Picker
//=========================
// Picker
//=========================
- (void)initListPicker
{
    //PICKER DAY
	NSLog(@"INIT DAY PICK");
    
    if (!picker_ctrl_day) picker_ctrl_day = [[CPPickerView alloc] initWithFrame:CGRectMake(0, 0, 190, 60)];
    picker_ctrl_day.backgroundColor = [UIColor clearColor];
    picker_ctrl_day.dataSource = self;
    picker_ctrl_day.delegate = self;
    [picker_ctrl_day setItemFont:[UIFont fontWithName:FONT_HELVETICANEUE_THIN size:20]];
    [picker_ctrl_day setItemColor:pickerColor];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSMutableArray *lista_giorni = [[NSMutableArray alloc] init];
	
	for (int i=-1; i<16; i++)
    {
        NSString *test = [dateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*i]];
        [lista_giorni addObject:test];
    }
    
	list_down = [[NSMutableArray alloc] initWithArray:lista_giorni];
    // NSLog(@"list_down:%i",(int)list_down.count);

    [picker_ctrl_day reloadData];
    
    if(!picker_ctrl_day.superview){[backPicker addSubview:picker_ctrl_day];}

    //PICKER LINGUA
    // NSLog(@"INIT LINGUA PICK");
    if (!picker_ctrl_lingua)
    {
        picker_ctrl_lingua = [[UIPickerView alloc] initWithFrame:CGRectMake(-4, -57, 60, 70)];
        //[picker_ctrl_lingua setSoundsEnabled:NO];
        [picker_ctrl_lingua setBackgroundColor:[UIColor clearColor]];
        [backPicker_lingua.layer setMasksToBounds:YES];
        
        if(!self.isIos7orUpper){
            
            // NSLog(@"picker_ctrl_lingua:%@\n%@",picker_ctrl_lingua,picker_ctrl_lingua.subviews);
            
        }

        
    }

    [picker_ctrl_lingua setDelegate:self];
    [picker_ctrl_lingua setDataSource:self];
    
    
    if(!picker_ctrl_lingua.superview) [backPicker_lingua addSubview:picker_ctrl_lingua];
    [picker_ctrl_lingua setCenter:CGPointMake(backPicker_lingua.frame.size.width/2, backPicker_lingua.frame.size.height/2)];
    

    [picker_ctrl_lingua selectRow:[[linguaPos objectForKey:[[[Home getCurrentDay] linguaGiorno] sigla]] intValue] inComponent:0 animated:NO];
    
    [up_btn setAlpha:([picker_ctrl_lingua selectedRowInComponent:0]==0)?0.3:1];
    
    [down_btn setAlpha:([picker_ctrl_lingua selectedRowInComponent:0]==[list_lingua_img count]-1)?0.3:1];
    
    // NSLog(@"Fine Picker");

}


#pragma mark - CPPickerViewDataSource
- (NSInteger)numberOfItemsInPickerView:(UIPickerView *)pickerView
{
    // NSLog(@"Picker[%@]:numberOfItemsInPickerView %i",[pickerView class],(int)list_down.count);
    return list_down.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    NSInteger i = ([pickerView isKindOfClass:[CPPickerView class]])?list_down.count:list_lingua_img.count;
    // NSLog(@"Picker[%@]:numberOfRowsInComponent %i",list_down,(int)i);
    return i;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForItem:(NSInteger)index
{
    if(!([pickerView isKindOfClass:[CPPickerView class]]))
        return nil;
    if((index > list_down.count)) return @"errore";
    if(([pickerView isKindOfClass:[CPPickerView class]]))
    {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setLocale:[NSLocale currentLocale]];
    
	// get NSDate from old string format
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
	NSDate *date = [dateFormatter dateFromString:[list_down objectAtIndex:index]];
	
	// get string in new date format
    [dateFormatter setDateFormat:@"EEEE d MMMM"];
    
    return [[dateFormatter stringFromDate:date] capitalizedString];
    }
    
    
    return nil;
}


#pragma mark - CPPickerViewDelegate
//=========================
// Picker delegate
//=========================
- (void)pickerView:(CPPickerView *)pickerView didSelectItem:(NSInteger)item{
    // NSLog(@"CPPickerView: didSelectItem:%i",(int)item);
    [dx_btn setAlpha:(item==(list_down.count - 1))?0.3:1];
    [sx_btn setAlpha:(item==0)?0.3:1];
}


- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    // NSLog(@"pickerView rowHeightForComponent:%@",[pickerView class]);
    if(pickerView==picker_ctrl_lingua) return 60.0;
    if([pickerView isKindOfClass:[CPPickerView class]]) return 60.0;
    return 60.0;
}

#pragma mark - PickerViewDelegate
//=========================
// Picker delegate
//=========================
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(thePickerView==picker_ctrl_lingua){
        // NSLog(@"DayPicker item selected:%i",(int)row);
        [up_btn setAlpha:(row==0)?0.3:1];
        [down_btn setAlpha:(row==[list_lingua_img count]-1)?0.3:1];
        
        LinguaObject *l = [list_lingua_img objectAtIndex:row];
        [button_crea_comune setEnabled:[Fix checkComuniForLang:l.sigla]];
    }
}


-(UIView *)pickerView:(UIPickerView *)pickerView
           viewForRow:(NSInteger)row
         forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    LinguaObject *l = [list_lingua_img objectAtIndex:row];
    // NSLog(@"view4lingua:%@",l);
    for(int s = 0; s < [[picker_ctrl_lingua subviews] count];s++)
    {
        if(self.isIos7orUpper)
        {
        UIView *sub = [[picker_ctrl_lingua subviews] objectAtIndex:s];
        [sub setHidden:(s!=0)];
        }
        else{
            // NSLog(@"ISO6->%@",[picker_ctrl_lingua subviews]);
            [(UIView*)[[picker_ctrl_lingua subviews] objectAtIndex:0] setHidden:YES];
            [(UIView*)[[picker_ctrl_lingua subviews] objectAtIndex:4] setHidden:YES];
            [(UIView*)[[picker_ctrl_lingua subviews] objectAtIndex:1] setHidden:YES];
            [(UIView*)[[picker_ctrl_lingua subviews] objectAtIndex:3] setHidden:YES];

        }
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.isIos7orUpper?60:50, self.isIos7orUpper?60:50)];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    [imageView setImage:l.logo];
    return imageView;
}


Day *dayMessaIncompleto = nil;

-(IBAction)incompletePressed:(UIButton*)sender{
    NSInteger index = sender.tag;
    NSArray *listaDay = [Day getListDays];
    if(index>=listaDay.count)
    {
        return;
    }
    NSString *dayPath = [self.cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",PATH_DAY,[[Day getListDays] objectAtIndex:index]]];
    if(![self fileExistsAtPath:dayPath])
    {
        return;
    }
    NSData *data = [[NSData alloc] initWithContentsOfFile:dayPath];
    dayMessaIncompleto = [Day new];
    dayMessaIncompleto = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *tit = self.isIos7orUpper?@"":@"DOWNLOAD";
    NSString *mex = [NSString stringWithFormat:NSLocalizedString(@"AGGIORNA_GIORNO_MEX", nil),dayMessaIncompleto.date];
    
    UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:tit message:mex delegate:self cancelButtonTitle:NSLocalizedString(@"AGGIORNA_GIORNO_PIU_TARDI", nil) otherButtonTitles:NSLocalizedString(@"AGGIORNA_GIORNO_OK", nil),nil];
    [successAlert setTag:2];
    
    if(self.isIos7orUpper)
    {
    UIView *mask = [[UIView alloc] initWithFrame:CGRectMake(-120, -108, 240, 230)];
    [mask setBackgroundColor:[UIColor clearColor]];

    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(53, 8, 18, 18)];
    [imageView setAlpha:0.9];
    NSString *path = [[NSString alloc] initWithString:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"erro@2x.png"]];
    UIImage *bkgImg = [[UIImage alloc] initWithContentsOfFile:path];
    [imageView setImage:bkgImg];
    
    UIImageView *imageLogo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 25)];
    [imageLogo setContentMode:UIViewContentModeScaleAspectFit];
    [imageLogo setCenter:CGPointMake(mask.frame.size.width/2, 21)];
    NSString *pathLogo = [[NSString alloc] initWithString:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"iBreviaryLOGO.png"]];
    UIImage *imglogo = [[UIImage alloc] initWithContentsOfFile:pathLogo];
    [imageLogo setImage:imglogo];
    [mask addSubview:imageLogo];

    
    [mask addSubview:imageView];
    UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    [view2 setBackgroundColor:[UIColor clearColor]];
    [view2 addSubview:mask];
    [successAlert setValue:view2 forKey:@"accessoryView"];
    }
    [successAlert show];
}

#pragma mark aggiorna INCOMPLETE
//=========================
// RISCARICA
//=========================
-(void)riscarica{
    // NSLog(@"Lista: riscarica started");
    if(!self.connesso) { [self noNet]; return; }
      
    [Day scaricaGiornoconData:dayMessaIncompleto.id_date withLang:dayMessaIncompleto.lingua eventFlag:[TestoProprio getTestoProprioWithKey:[dayMessaIncompleto.type isEqualToString:@"generic"]?@"":dayMessaIncompleto.type] completionBlock:^(Day *new){
            if(!new)
            {
                [self noNet];
                return new;
            }
        
            [self AttivaRotella];
        
            NSDateFormatter *f = [NSDateFormatter new];
            [f setDateFormat:@"yyyy-MM-dd"];
            NSDate *data_aggiornata = [f dateFromString:dayMessaIncompleto.id_date];
            [f setDateFormat:@"d/M/yy"];
            if(new.complete)
                //OK!
                [self mostraNotificaDayDownloaded:[f stringFromDate:data_aggiornata]];
            else
                [self mostraNotificaDayERROR_INCOMPLETE:[f stringFromDate:data_aggiornata]];
            [self rimuoviRotella];
        
            //deallocco oggetti utilizzati
            new = nil;
            dayMessaIncompleto = nil;
        
            [self MainReload_listaView];
            return new;
        }];
}


#pragma mark -
#pragma mark OPERATION QUEUE
//OPERATION QUEUE
-(void)aggiungiOperazione:(SEL)selector{
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self selector:selector object:nil];
    [queue addOperation:operation];
}

-(void)aggiungiOperazione:(SEL)selector withObject:(id)obj{
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self selector:selector object:obj?obj:nil];
    [queue addOperation:operation];
}

-(void)cancelAllOperation{
    [queue cancelAllOperations];
}

//===========================
// TUTORIAL DELEGATE
//===========================
#pragma mark - TUTORIAL DELEGATE
- (IBAction)showTutorial:(UIButton*)sender{
        // NSLog(@"showTutorial");
        [self rotateImage:tutorial_img withDuration:0.6 onY_axis:YES];

        tutorialCTRL = [[Tutorial alloc] initWithDelegate:self];
        CGRect frame = self.view.frame;
        if(!self.isIos7orUpper && !self.isIphone5)
        {
            frame.origin.y -= 20;
            frame.size.height +=20;
            [tutorialCTRL.view setFrame:frame];
            frame.origin.y += 20;

        }

        blurView = [[FXBlurView alloc] initWithFrame:frame];
    // NSLog(@"FRAME:%@",NSStringFromCGRect(frame));

        [self.blurView setDynamic:NO];
        [self.blurView setTintColor:[UIColor brownColor]];
        [self.blurView addSubview:tutorialCTRL.view];
        self.blurView.layer.transform = CATransform3DMakeScale(0.02, 0.02, 1.0);
        if(self.isIos7orUpper)[self.blurView setCenter:CGPointMake(sender.center.x-7, sender.center.y+2)];
        [self.blurView setAlpha:0];

        [self.view addSubview:self.blurView];

        [UIView beginAnimations:@"AppareTutorialCTRL" context:NULL];
        [UIView setAnimationDuration:0.1];
        [self.blurView setAlpha:1];
        [UIView commitAnimations];
    
    
    [UIView animateWithDuration:0.4 animations:^{
                                                self.blurView.layer.transform = CATransform3DMakeScale(1, 1, 1.0);
                                                if(self.isIos7orUpper)[self.blurView setCenter:self.view.center];

                                                //[self.blurView setFrame:to];
                                                }
                         completion:^(BOOL finished)    {
                                                        // NSLog(@"Fine Animation");
                                                        }];

}


-(void)removeTutorial{
    // NSLog(@"removeTutorial");
    [self rotateImage:tutorial_img withDuration:0.6 onY_axis:YES];

    [UIView animateWithDuration:0.4 animations:^{
                                                self.blurView.layer.transform = CATransform3DMakeScale(0.01, 0.01, 1.0);
                                                [self.blurView setCenter:CGPointMake(tutorial_btn.center.x-7, tutorial_btn.center.y+2)];

                                                }
                     completion:^(BOOL finished)    {
                         [UIView beginAnimations:@"SconpareTutorialCTRL" context:NULL];
                         [UIView setAnimationDuration:0.1];
                         [UIView setAnimationDelegate:self.blurView];
                         [UIView setAnimationDidStopSelector:@selector(removeFromSuperview)];
                         [self.blurView setAlpha:0];
                         [UIView commitAnimations];
                     }];


}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return [[DDMenuController sharedInstance] supportedInterfaceOrientations];
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    // NSLog(@"will Rotate");
    [[DDMenuController sharedInstance] willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    // NSLog(@"did Rotate");
    [[DDMenuController sharedInstance] didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}



#pragma mark -
#pragma mark ERROR CLASS
//=========================
// ERROR CLASS
//=========================
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTitle:nil];
    [self setLogoVelina_lingua:nil];
    [self setBackPicker:nil];
    [self setDay_ctrl_view:nil];
    [self setBackPicker_lingua:nil];
    [self setReset_btn:nil];
    [self setReset_label:nil];
     [super viewDidUnload];
}


@end
