//
//  SelettoreMessa.h
//  iBreviary
//
//  Created by leyo on 20/05/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import "BaseController.h"
#import "Editor.h"


@protocol SelettoreMessaDelegate <NSObject>
- (void)dismissMessaSelettoreGoingTo:(NSIndexPath*)indexPath;
- (void)dismissMessaSelettoreThenComune;
@end

@interface SelettoreMessa : BaseController<EditorDelegate>

@property (strong, nonatomic) Messa *messa;

@property (nonatomic, strong) BaseController<SelettoreMessaDelegate> *delegate;
@property (nonatomic, strong) IBOutlet UIButton *close;
@property (weak, nonatomic) IBOutlet UIButton *annulla_btn;
@property (weak, nonatomic) IBOutlet UIImageView *annulla_back;

@property (nonatomic, strong) IBOutlet UIButton *ok_btn;

@property (nonatomic,retain)IBOutlet UIView *subview;
@property (strong, nonatomic) IBOutlet UIView *subTabella;

@property (strong, nonatomic) NSMutableArray *lista;
@property (strong, nonatomic) NSMutableArray *listaFiltrata;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIButton *btn_closeKeyboard;

@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (strong, nonatomic) IBOutlet UIWebView *anteprimaWeb;
@property (strong, nonatomic) IBOutlet UILabel *label_uno;
@property (strong, nonatomic) IBOutlet UILabel *label_due;
@property (strong, nonatomic) IBOutlet UIView *linea;
@property (strong, nonatomic) IBOutlet UIImageView *backBlur;
@property (strong, nonatomic) IBOutlet UIView *backBlack;
@property (strong, nonatomic) IBOutlet UIButton *addReadings_btn;
@property (strong, nonatomic) IBOutlet UIImageView *save_btn_image;
@property (strong, nonatomic) Editor *messa_editor_ctrl;

@property (strong, nonatomic) NSNumber *index;
@property (strong, nonatomic) NSNumber *index_filtrato;

@property (weak, nonatomic) IBOutlet UIButton *edit_btn;


- (id)initWithMessa:(Messa*)m andDelegate:(BaseController<SelettoreMessaDelegate>*)d type:(Tipo_messa_lista)t;
- (id)initWithList:(NSArray*)a andMessa:(Messa*)m type:(Tipo_messa_lista)t;
@end
