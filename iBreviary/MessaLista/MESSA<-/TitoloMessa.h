//
//  TitoloMessa.h
//  iBreviary
//
//  Created by leyo on 20/05/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import "BaseController.h"
#import "Lingua.h"
#import "LinguaObject.h"

@class Messa;

@protocol TitoloMessaDelegate <NSObject>
-(void)titolosalvato:(NSString *)tit thenComune:(BOOL)thenComune;
-(void)titoloAnnullato;
@end

@protocol TitoloEditMessaDelegate <NSObject>
-(void)fineTitoloEdit;
@end

@interface TitoloMessa : BaseController

- (id)initWithDelegate:(BaseController<TitoloMessaDelegate>*)d
                andTit:(NSString*)tit
                  lang:(LinguaObject*)l
                  date:(NSString*)data
            thenComune:(BOOL)c;

- (id)initForEditTitoloAction:(BaseController<TitoloEditMessaDelegate>*)d
                        messa:(Messa*)m;

@end
