//
//  Editor.h
//  iBreviary
//
//  Created by leyo on 20/05/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import "BaseController.h"
#import "RichTextEditor.h"
#import "Messa.h"

@protocol EditorDelegate <NSObject>
-(void)dismissEditor:(NSIndexPath*)indexPath;
@end

@interface Editor : BaseController<RichTextEditorDataSource>
@property (weak, nonatomic) IBOutlet UILabel *title_label;

@property (nonatomic, strong) BaseController<EditorDelegate> *delegate;
@property (nonatomic, strong) IBOutlet UIButton *ok_btn;
@property (nonatomic, strong) Messa *messa;
@property (nonatomic, strong) NSString *attributeTosave;
@property (nonatomic, strong) NSNumber *letture_number;
@property (nonatomic, strong) NSAttributedString * attributeText;
@property (nonatomic, strong) IBOutlet RichTextEditor *richTextEditor;
@property (nonatomic, strong) IBOutlet UIImageView *sfondoEditor;


- (id)initWithDelegate:(BaseController<EditorDelegate>*)d andMessa:(Messa*)m forAttribute:(NSString*)a;
- (id)initLettureWithDelegate:(BaseController<EditorDelegate>*)d andMessa:(Messa*)m forLetture:(NSNumber*)n;

@end
