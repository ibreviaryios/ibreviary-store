//
//  Editor.m
//  iBreviary
//
//  Created by leyo on 20/05/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import "Editor.h"
#import "Home.h"

@interface Editor ()

@end

@implementation Editor

@synthesize delegate, ok_btn, messa, attributeTosave, title_label, letture_number;
@synthesize attributeText, richTextEditor, sfondoEditor;


#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<EditorDelegate>*)d andMessa:(Messa*)m forAttribute:(NSString*)a
{
    // NSLog(@"initWithMessa:%@",m);
    NSString *nibNameOrNil = @"Editor_ipad";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        self.delegate = d;
        self.messa = m;
        [self.view setCenter:d.view.center];
        if([a isEqualToString:@"omelia"]) [self setHTML:m.omelia];
        if([a isEqualToString:@"avvisi"]) [self setHTML:m.avvisi];
        self.attributeTosave = [NSString stringWithString:a];
        [self showTitle];
    }
    return self;
}

- (id)initLettureWithDelegate:(BaseController<EditorDelegate>*)d andMessa:(Messa*)m forLetture:(NSNumber*)n
{
    // NSLog(@"initWithMessa:%@",m);
    NSString *nibNameOrNil = @"Editor_ipad";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        self.delegate = d;
        self.messa = m;
        [self.view setCenter:d.view.center];
        [self setHTML:@""];
        if(n.integerValue < messa.letture.count)
            [self setHTML:[[messa.letture objectAtIndex:n.integerValue] contenuto]];
        self.attributeTosave = @"letture";
        self.letture_number = n;
        [self showTitle];
        }
    return self;
}

-(void)setHTML:(NSString*)htmlToConvert{
    attributeText = [[NSAttributedString alloc] initWithData:[htmlToConvert dataUsingEncoding:NSUTF8StringEncoding]
                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                               NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                          documentAttributes:nil error:nil];
    

}



#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    [self gui];
    // NSLog(@"viewDidLoad:%@",self.attributeTosave);
    // NSLog(@"[%@]title:%@",attributeTosave,title_label.text);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didRotate)
                                                     name:DID_ROTATE object:nil];
        
    }

-(void)gui{
    CGFloat radius = 5;
    [richTextEditor setBackgroundColor:[UIColor clearColor]];
    [richTextEditor.layer setCornerRadius:radius];
    [richTextEditor.layer setMasksToBounds:YES];
    [sfondoEditor.layer setCornerRadius:radius];
    [sfondoEditor.layer setMasksToBounds:YES];
    
}

-(void)showTitle{
    if([attributeTosave isEqualToString:@"omelia"])
        [title_label setText:[[NSString stringWithFormat:@"- editor %@ -",NSLocalizedString(@"indexMessa_3", nil)] uppercaseString]];
    if([attributeTosave isEqualToString:@"avvisi"])
        [title_label setText:[[NSString stringWithFormat:@"- editor %@ -",NSLocalizedString(@"indexMessa_13", nil)] uppercaseString]];
    if([attributeTosave isEqualToString:@"letture"])
        [title_label setText:[[NSString stringWithFormat:@"- editor %@ -",NSLocalizedString(@"indexMessa_2", nil)] uppercaseString]];

}

#pragma mark - Appear
//===================
//APPEAR
//===================
- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    [richTextEditor setAttributedText:attributeText];
}

- (void)viewDidAppear:(BOOL)animated{
    // NSLog(@"Editor:viewDidAppear");
    [super viewDidAppear:animated];
}


#pragma mark - Disappear
//===================
//DISAPPEAR
//===================
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

#pragma mark -
#pragma mark OK/annulla
//=========================
// OK
//=========================
-(IBAction)okEditor:(UIButton*)sender{
    // NSLog(@"FineEditor");
    Messa *m = [Home getCurrentMessa];
    NSString *result = @"";
    if(richTextEditor.text.length!=0){
    result = [richTextEditor htmlString];
    if([attributeTosave isEqualToString:@"letture"]) {
        if(letture_number.integerValue>=m.letture.count){
            // NSLog(@"PRIMA:letture:%@",m.letture);
            Item *i = [Item new];
            i.titolo =  [NSString stringWithFormat:@"%@-%@",NSLocalizedString(@"indexMessa_2", nil),@(letture_number.integerValue+1)];
            i.contenuto = result;
            [m.letture addObject:i];
            // NSLog(@"m.letture:%@",m.letture);
        }
        else{
            Item *i = [m.letture objectAtIndex:letture_number.integerValue];
            i.contenuto = result;
            [m.letture replaceObjectAtIndex:letture_number.integerValue withObject:i];
        }
        [AppDelegate saveContext];
        // NSLog(@"LISTA MESSE:%@",[Messa getMesseIn:[AppDelegate mainManagedObjectContext]]);
        [delegate dismissEditor:nil];
        return;
    }
    }
    if([attributeTosave isEqualToString:@"omelia"]) m.omelia = result;
    if([attributeTosave isEqualToString:@"avvisi"]) m.avvisi = result;
    [AppDelegate saveContext];
    
    if([attributeTosave isEqualToString:@"omelia"])
        [delegate dismissEditor:[NSIndexPath indexPathForRow:3 inSection:0]];
    else
        [delegate dismissEditor:[NSIndexPath indexPathForRow:13 inSection:0]];
}

-(IBAction)annulla:(UIButton*)sender{
    // NSLog(@"FineEditor->Annulla");
    if([attributeTosave isEqualToString:@"omelia"])
        [delegate dismissEditor:[NSIndexPath indexPathForRow:3 inSection:0]];
    else
        [delegate dismissEditor:[NSIndexPath indexPathForRow:13 inSection:0]];
}

#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark -
#pragma mark RichTextEditorFeature
//=========================
// RichTextEditorFeature
//=========================
- (RichTextEditorFeature)featuresEnabledForRichTextEditor:(RichTextEditor *)richTextEditor
{
    return RichTextEditorFeatureFontSize | RichTextEditorFeatureFont | RichTextEditorFeatureAll;
}


#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)didRotate{
    // NSLog(@"Editor:didRotate");
    [self okEditor:nil];
}

@end

