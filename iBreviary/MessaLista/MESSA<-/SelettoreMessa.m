 //
//  SelettoreMessa.m
//  iBreviary
//
//  Created by leyo on 20/05/14.
//  Copyright (c) 2014 leyo. All rights reserved.
/*
 
 if(list_type == tipo_lista_letture && indexPath.row == listaFiltrata.count){
 UIImageView *im = [[UIImageView alloc] initWithFrame:CGRectMake(5, 15, 35, 35)];
 [im setImage:[UIImage imageNamed:@"download2014_white.png"]];
 [Label addSubview:im];
 CGRect f = resto.frame;
 f.origin.x += 50;
 f.size.width -=50;
 [resto setFrame:f];
 resto.textAlignment = NSTextAlignmentLeft;
 resto.text = NSLocalizedString(@"addCustomLecture", @"aggiungi lettura custom");
 }
 
 else
 
 */
//

#import "SelettoreMessa.h"
#import "Home.h"
#import "Fix.h"

@interface SelettoreMessa ()

@end

@implementation SelettoreMessa

@synthesize messa,messa_editor_ctrl,edit_btn,addReadings_btn;
@synthesize delegate,ok_btn,close,annulla_btn,annulla_back;
@synthesize subview, subTabella, anteprimaWeb, linea;
@synthesize save_btn_image, index, index_filtrato;
@synthesize tabella,backBlur,backBlack,label_uno,label_due,lista,listaFiltrata;
@synthesize searchBar,btn_closeKeyboard;

Tipo_messa_lista list_type = -1;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithMessa:(Messa*)m andDelegate:(BaseController<SelettoreMessaDelegate>*)d type:(Tipo_messa_lista)t
{
    NSString *nibNameOrNil = @"SelettoreMessa_ipad";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        messa = m;
        delegate = d;
        list_type = t;
        // NSLog(@"initWithMessa t:%i list_type:%i",t,list_type);

    }
    return self;
}

- (id)initWithList:(NSArray*)a andMessa:(Messa*)m type:(Tipo_messa_lista)t
{
    NSString *nibNameOrNil = @"SelettoreMessa_ipad";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        self.lista = [[NSMutableArray alloc] initWithArray:a];
        self.listaFiltrata = [[NSMutableArray alloc] initWithArray:a];
        self.messa = m;
        list_type = t;
        // NSLog(@"initWithList t:%i list_type:%i",t,list_type);
    }
    return self;
}


#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    // NSLog(@"SelettoreMessa DidLoad:%i",list_type);
    index_filtrato = [NSNumber numberWithInteger:-1];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                            selector:@selector(didRotate)
                                                                name:DID_ROTATE object:nil];

    [self gui];
}


-(void)gui{
    
    [self.subTabella.layer setCornerRadius:7];
    [self.subTabella.layer setMasksToBounds:YES];
  
    [self.anteprimaWeb.layer setCornerRadius:7];
    [self.anteprimaWeb.layer setMasksToBounds:YES];
    [self.anteprimaWeb setBackgroundColor:[UIColorFromRGB(0Xddcdb2) colorWithAlphaComponent:0.7]];
    
    [anteprimaWeb.scrollView setShowsHorizontalScrollIndicator:NO];
    [anteprimaWeb.scrollView setShowsVerticalScrollIndicator:NO];
    
    [self.subview setAlpha:0];
    [self.subview setBackgroundColor:[UIColor clearColor]];
    [self.subview.layer setMasksToBounds:YES];
    self.subview.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.6, 0.6);
    [annulla_btn setTitle:[NSLocalizedString(@"annulla", nil) uppercaseString] forState:UIControlStateNormal];
    [addReadings_btn setTitle:[NSLocalizedString(@"addCustomLecture", nil) uppercaseString] forState:UIControlStateNormal];
    [backBlur setImage:[UIImage imageNamed:self.isPort?@"blur_ipad_port.png":@"blur_ipad_land.png"]];
    
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    [searchBar setBackgroundColor:[UIColor clearColor]];
    searchBar.barTintColor = [UIColor clearColor];
    searchBar.backgroundImage = [UIImage new];
    
}

#pragma mark - Appear
//===================
//APPEAR
//===================
- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    
    // RESIZE LAND / PORTRAIT VIEW
    if(self.version<8.f){
        CGRect screenBound = [[UIScreen mainScreen] bounds];
        [self.view setFrame:CGRectMake(0, 0, self.isLand?screenBound.size.height:screenBound.size.width, self.isLand?screenBound.size.width:screenBound.size.height)];
    }
    
    if(self.version>=8){
        CGRect screenBound = [[UIScreen mainScreen] bounds];
        [self.view setFrame:CGRectMake(0, 0, screenBound.size.width, screenBound.size.height)];
    }
    
    [subview setCenter:self.view.center];

    
    [annulla_btn setHidden:YES];
    [annulla_back setHidden:annulla_btn.hidden];

    [edit_btn setHidden:YES];
    [addReadings_btn setHidden:YES];
    //[self didRotate];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // NSLog(@"viewDidAppear:showView");
    [self showView];
    [self initList];
}

-(void)initList{
    Fix *fix = [Fix getFixForLang:messa.day.lingua];
    self.lista = nil;
    switch (list_type) {
        case tipo_lista_messa_comune:
        {
            self.lista = [[NSMutableArray alloc] initWithArray:fix.missal_comuni_array];
            index = [NSNumber numberWithInt:0];
            // NSLog(@"tipo_lista_messa_comune:%@",index);
            break;
        }
        case tipo_lista_credo:
        {
            self.lista = [[NSMutableArray alloc] initWithArray:messa.credo];
            index = [NSNumber numberWithInt:messa.sel_credo.intValue+1];
            // NSLog(@"tipo_lista_credo:%@",index);
            break;
        }
        case tipo_lista_preghiera_fedeli:
        {
            self.lista = [[NSMutableArray alloc] initWithArray:messa.preghieraDeiFedeli];
            index = [NSNumber numberWithInt:messa.sel_preghieraDeiFedeli.intValue+1];
            // NSLog(@"tipo_lista_preghiera_fedeli:%@",index);
            break;
        }
        case tipo_lista_prefazio:
        {
            self.lista = [[NSMutableArray alloc] initWithArray:messa.prefazio];
            if(messa.sel_prefazio==nil) messa.sel_prefazio = [NSNumber numberWithInt:-1];
            if(messa.sel_prefazio.intValue == -1) index = [NSNumber numberWithInt:0];
            else index = messa.sel_prefazio;
            // NSLog(@"tipo_lista_prefazio:%@ messa:%@", index, messa.sel_prefazio);
            break;
        }
        case tipo_lista_preghiera_eucaristica:
        {
            self.lista = [[NSMutableArray alloc] initWithArray:messa.preghieraEucaristica];
            if(messa.sel_preghieraEucaristica==nil) messa.sel_preghieraEucaristica = [NSNumber numberWithInt:-1];
            if(messa.sel_preghieraEucaristica.intValue == -1) index = [NSNumber numberWithInt:0];
            else index = messa.sel_preghieraEucaristica;
            // NSLog(@"tipo_lista_preghiera_eucaristica:%@",index);
            break;
        }
        case tipo_lista_benedizione_solenne:
        {
            self.lista = [[NSMutableArray alloc] initWithArray:messa.benedizioneSolenne];
            index = [NSNumber numberWithInt:messa.sel_benedizioneSolenne.intValue+1];
            // NSLog(@"tipo_lista_benedizione_solenne:%@",index);
            break;
        }
        case tipo_lista_letture:
        {
            self.lista = [[NSMutableArray alloc] initWithArray:messa.letture];
            if(messa.sel_letture==nil) messa.sel_letture = [NSNumber numberWithInt:-1];
            if(messa.sel_letture.intValue == -1) index = [NSNumber numberWithInt:0];
            else index = messa.sel_letture;
            // NSLog(@"tipo_lista_letture:%@",index);
            [edit_btn setHidden:NO];
            [addReadings_btn setHidden:NO];
            break;
        }
            
        default:
            break;
    }
    
    if(list_type == tipo_lista_credo ||
       list_type == tipo_lista_preghiera_fedeli ||
       list_type == tipo_lista_benedizione_solenne){
                                Item *i = [Item new];
                                // NSLog(@"ITA_loc:%@",NSLocalizedString(@"NessunaSelezione", nil));
                                [i setTitolo:NSLocalizedString(@"NessunaSelezione", nil)];
                                [i setContenuto:@""];
                                [self.lista insertObject:i atIndex:0];
    }

    self.listaFiltrata = [[NSMutableArray alloc] initWithArray:lista];
    
    //controllo annulla btn!
    [annulla_btn setHidden:(list_type==0)];
    [annulla_back setHidden:annulla_btn.hidden];

    
    if(lista>0)
    {
    [tabella reloadData];
    [tabella setScrollEnabled:listaFiltrata.count>7];
    
    // NSLog(@"Selettore Messa: index inizializzato:%@",index);
    [self caricoWeb];
    }
}

#pragma mark - Disappear
//===================
//DISAPPEAR
//===================
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}




-(IBAction)salvaChooser:(UIButton*)sender{
    // NSLog(@"salvaChooser: index->%@",index);
    NSIndexPath *indexPath = nil;
    switch (list_type) {
        case tipo_lista_messa_comune:
        {
            messa.antifonaEcolletta = [[listaFiltrata objectAtIndex:index.intValue] antiphon_and_opening_prayer];
            messa.sulleofferte = [[listaFiltrata objectAtIndex:index.intValue] prayer_over_the_gifts];
            messa.antifonaDopo = [[listaFiltrata objectAtIndex:index.intValue] antiphon_and_prayer_after_communion];
            
            break;
        }
        case tipo_lista_credo:
        {
            messa.sel_credo = [NSNumber numberWithInt:index.intValue-1];
            // NSLog(@"Messa credo scelto:%@",messa.sel_credo);
            indexPath = [NSIndexPath indexPathForRow:4 inSection:0];
            break;
        }
        case tipo_lista_preghiera_fedeli:
        {
            messa.sel_preghieraDeiFedeli = [NSNumber numberWithInt:index.intValue-1];
            // NSLog(@"Messa preghieraDeiFedeli scelto:%@",messa.sel_preghieraDeiFedeli);
            indexPath = [NSIndexPath indexPathForRow:5 inSection:0];
            break;
        }
        case tipo_lista_prefazio:
        {
            messa.sel_prefazio = index;
            // NSLog(@"Messa prefazio scelto:%@",messa.sel_prefazio);
            indexPath = [NSIndexPath indexPathForRow:8 inSection:0];
            break;
        }
        case tipo_lista_preghiera_eucaristica:
        {
            messa.sel_preghieraEucaristica = index;
            // NSLog(@"Messa preghiera_eucaristica scelto:%@",messa.sel_preghieraEucaristica);
            indexPath = [NSIndexPath indexPathForRow:9 inSection:0];
            break;
        }
        case tipo_lista_benedizione_solenne:
        {
            messa.sel_benedizioneSolenne = [NSNumber numberWithInt:index.intValue-1];
            // NSLog(@"Messa benedizioneSolenne scelto:%@",messa.sel_benedizioneSolenne);
            indexPath = [NSIndexPath indexPathForRow:12 inSection:0];
            break;
        }
        case tipo_lista_letture:
        {
            messa.sel_letture = index;
            // NSLog(@"Messa letture scelto:%@",messa.sel_letture);
            indexPath = [NSIndexPath indexPathForRow:13 inSection:0];
            break;
        }
            
        default:
            break;
    }
  
    NSLog(@"Messa:%@",messa);
    [AppDelegate saveContext];
    NSLog(@"MessaSalvata:%@",messa);

    if(list_type == tipo_lista_messa_comune) [delegate dismissMessaSelettoreThenComune];
    else [delegate dismissMessaSelettoreGoingTo:indexPath];
}

-(IBAction)annullaSelettore:(UIButton*)sender{
    [delegate dismissMessaSelettoreGoingTo:nil];
}

-(IBAction)fakeBTN:(UIButton*)sender{
    // NSLog(@"fakeBTN!");
}

#pragma mark - CARICO WEB
//=========================
// CARICO WEB
//=========================
-(void)caricoWeb{
    
    NSNumber *indice = [NSNumber numberWithInteger:(index_filtrato.integerValue!=-1)?index_filtrato.integerValue:index.integerValue];
    
    // NSLog(@"caricoWeb[%@]",indice);
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    NSUserDefaults *def = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)?[[NSUserDefaults alloc] initWithSuiteName:@"group.ibreviary.com"]:[NSUserDefaults standardUserDefaults];

    if(lista.count>0 && [[lista objectAtIndex:0] isKindOfClass:[MissalObject class]])
    {
        MissalObject *missal = [listaFiltrata objectAtIndex:indice.intValue];
        [label_uno setText:[missal.title uppercaseString]];
        [label_due setText:messa.day.day_label];
        NSString *part1=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/header_anteprima.html",path] encoding:NSUTF8StringEncoding error:nil];
        //part1 = [part1 stringByAppendingFormat:@"<div class=\"testoComune\">%@<br /><br/></div>",missal.title];
        NSString *part2 = [NSString stringWithFormat:@"<div class=\"sezione\">%@</div>%@<hr /><br />",missal.antiphon_and_opening_prayer.titolo,missal.antiphon_and_opening_prayer.contenuto];
        NSString *part3 = [NSString stringWithFormat:@"<div class=\"sezione\">%@</div>%@<hr /><br />",missal.prayer_over_the_gifts.titolo,missal.prayer_over_the_gifts.contenuto];
        NSString *part4 = [NSString stringWithFormat:@"<div class=\"sezione\">%@</div>%@",missal.antiphon_and_prayer_after_communion.titolo,missal.antiphon_and_prayer_after_communion.contenuto];
        NSString *zoom=[NSString stringWithFormat:@"<div ID=\"ciccio\" class=\"zoomClass\" >%@</div>",[def objectForKey:ZOOM]];
        
        [anteprimaWeb loadHTMLString:[NSString stringWithFormat:@"%@%@%@%@%@<br /></body></html>",part1,zoom,part2,part3,part4] baseURL:baseURL];
    }
    
    if(lista.count>0 && [[lista objectAtIndex:0] isKindOfClass:[Item class]])
    {
        Item *content = [listaFiltrata objectAtIndex:indice.intValue];
        [label_uno setText:[content.titolo uppercaseString]];
        [label_due setText:messa.day.day_label];
        NSString *part1=[NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/header_anteprima.html",path] encoding:NSUTF8StringEncoding error:nil];
        //part1 = [part1 stringByAppendingFormat:@"<div class=\"testoComune\">%@<br /><br/></div>",content.titolo];
        NSString *zoom=[NSString stringWithFormat:@"<div ID=\"ciccio\" class=\"zoomClass\" >%@</div>",[def objectForKey:ZOOM]];
        
        [anteprimaWeb loadHTMLString:[NSString stringWithFormat:@"%@%@%@<br /></body></html>",part1,zoom,content.contenuto] baseURL:baseURL];
    }

}

#pragma mark -
#pragma mark --------> Table view
#pragma mark Header/Footer
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// HEADER SESSION
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

// FOOTER SESSION
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	UIView *customView = [[UIView alloc] initWithFrame:CGRectZero];
	return customView;
}

#pragma mark ROW

//NUMERO DI RIGHE TABELLA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listaFiltrata.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self heighAtIndexPath:indexPath];
}

-(CGFloat)heighAtIndexPath:(NSIndexPath *)indexPath{
    // NSLog(@"heighAtIndexPath[%i]:%@",(int)lista.count,indexPath);
    CGSize labelSize = CGSizeMake(0, 0);
    if(list_type == tipo_lista_letture){
        return 60;
    }
    if([[listaFiltrata objectAtIndex:indexPath.row] isKindOfClass:[Item class]])
    {
        CGRect b  = [[(Item*)[listaFiltrata objectAtIndex:indexPath.row] titolo] boundingRectWithSize:CGSizeMake(250, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_HOEFLER size: 20]} context:nil];
        
        labelSize = b.size;
    }
    if([[listaFiltrata objectAtIndex:indexPath.row] isKindOfClass:[BreviarioObject class]])
    {
        
        CGRect b  = [[(BreviarioObject*)[listaFiltrata objectAtIndex:indexPath.row] title] boundingRectWithSize:CGSizeMake(250, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_HOEFLER size: 20]} context:nil];
        
        labelSize = b.size;
    }
    if([[listaFiltrata objectAtIndex:indexPath.row] isKindOfClass:[MissalObject class]])
    {
        
        CGRect b  = [[(MissalObject*)[listaFiltrata objectAtIndex:indexPath.row] title] boundingRectWithSize:CGSizeMake(250, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_HOEFLER size: 20]} context:nil];
        
        labelSize = b.size;
    }
    CGFloat labelHeight = labelSize.height + 30.f;
    //if(labelHeight>100) return 100;
    if(labelHeight<60.f) return 60.f;
	return (float)labelHeight;
}
#pragma mark CELL
// CONFIGURARE LA CELLA
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell_tabella";
    float height = [self heighAtIndexPath:indexPath];
    BOOL reverse = [self.defaults integerForKey:CSS]==2;
    UIColor *colore = reverse?[[UIColor whiteColor] colorWithAlphaComponent:0.55]:[UIColor colorWithRed:74/255.f green:47/255.f blue:32/255.f alpha:0.9];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tabella.frame.size.width, height)];
		cell.backgroundColor = [UIColor clearColor];
		cell.contentView.backgroundColor = [UIColor clearColor];
		[cell setBackgroundView:nil];
    }
    
	UIView* Label = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, tabella.frame.size.width, height)];
	Label.backgroundColor = [UIColor clearColor];
    
    
	UILabel *resto = [[UILabel alloc] initWithFrame:CGRectMake(2, 3, tabella.frame.size.width-4, height-6)];
	resto.backgroundColor = [UIColor clearColor];
	[resto setFont: [UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size: 15]];
	resto.textAlignment = NSTextAlignmentCenter;
    [resto setAdjustsFontSizeToFitWidth:YES];
    [resto setMinimumScaleFactor:0.5];
	[resto setTextColor:[UIColorFromRGB(0XFFFFFF) colorWithAlphaComponent:0.8]];
	
	resto.lineBreakMode = NSLineBreakByTruncatingTail;
	resto.numberOfLines = 4;
    
    if([[listaFiltrata objectAtIndex:indexPath.row] isKindOfClass:[Item class]])
    {
        // NSLog(@"testo lista è un item:%i",(int)indexPath.row);
        Item *itemRow = [listaFiltrata objectAtIndex:indexPath.row];
        resto.text = [itemRow.titolo stringByRemovingHTMLtag];
        if(itemRow.sottotitolo && ![itemRow.sottotitolo isEqualToString:@""])
        {
            CGRect frame = Label.frame;
            frame.origin.y = height-23;
            frame.origin.x += 4;
            frame.size.height = 30;
            frame.size.width -= 8;
            UILabel *subtitle = [[UILabel alloc] initWithFrame:frame];
            subtitle.backgroundColor = [UIColor clearColor];
            [subtitle setFont: [UIFont fontWithName:FONT_HELVETICANEUE_LIGHT size: 12]];
            subtitle.textAlignment = NSTextAlignmentCenter;
            [subtitle setTextColor:[colore colorWithAlphaComponent:0.8]];
            [subtitle setMinimumScaleFactor:0.8];
            [subtitle setAdjustsFontSizeToFitWidth:YES];
            subtitle.numberOfLines = 1;
            [subtitle setText:itemRow.sottotitolo];
            [Label addSubview:subtitle];
            
        }
    }
    
    else if([[listaFiltrata objectAtIndex:indexPath.row] isKindOfClass:[BreviarioObject class]])
    {
        // NSLog(@"testo lista è un Breviario");
        resto.text = [[(BreviarioObject*)[listaFiltrata objectAtIndex:indexPath.row] title] stringByRemovingHTMLtag];
        
    }
    
    else if([[listaFiltrata objectAtIndex:indexPath.row] isKindOfClass:[MissalObject class]])
    {
        // NSLog(@"testo lista è un Messale");
        MissalObject *m = (MissalObject*)[listaFiltrata objectAtIndex:indexPath.row];
        resto.text = [[[m title] stringByRemovingHTMLtag] uppercaseString];
        
    }
    NSNumber *indice = [NSNumber numberWithInteger:(listaFiltrata.count>=lista.count)?index.integerValue:index_filtrato.integerValue];
    
    // NSLog(@"ROW:%li indice:%li",(long)indexPath.row ,(long)indice.integerValue);
    if(indexPath.row == indice.integerValue){
            UIView *pallino = [[UIView alloc] initWithFrame:CGRectMake(3, (height-14)/2.0, 14, 14)];
            [pallino.layer setCornerRadius:pallino.frame.size.width/2.0];
            [pallino.layer setMasksToBounds:YES];
            [pallino setBackgroundColor:[UIColorFromRGB(0Xcebfa6) colorWithAlphaComponent:0.9]];
            [Label addSubview:pallino];
        }
    [Label addSubview:resto];
    
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 50)];
    [selectedBackgroundView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.1]];
	[cell setSelectedBackgroundView:selectedBackgroundView];
    
    
    
    [cell.contentView addSubview:Label];
	return cell;
}

#pragma mark DELEGATE (tableView)

// COMANDA DETAILVIEW
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // NSLog(@"DIDSelectROW:index %@ index_filtrato %@ indexPath:%@",index ,index_filtrato ,[NSNumber numberWithInteger:indexPath.row]);
    [tabella deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if(list_type == tipo_lista_letture){
        //non c'è ricerca qui!!!! :D
        index_filtrato =[NSNumber numberWithInteger:-1];
        index = [NSNumber numberWithInteger:indexPath.row];
        [self caricoWeb];
        return;
    }
    
    if(indexPath.row>=listaFiltrata.count) {
        // NSLog(@"indexPath.row>=listaFiltrata.count\n%@",listaFiltrata);
        return;
    }
    NSIndexPath *vecchio;

    if(listaFiltrata.count >= lista.count){
    vecchio = [NSIndexPath indexPathForRow:index.integerValue inSection:0];
    index_filtrato =[NSNumber numberWithInteger:-1];
    index = [NSNumber numberWithInteger:[lista indexOfObject:[listaFiltrata objectAtIndex:indexPath.row]]];
    }
    else{
    vecchio = [NSIndexPath indexPathForRow:index_filtrato.integerValue inSection:0];
    index_filtrato = [NSNumber numberWithInteger:indexPath.row];
    }
    
    if(indexPath.row!=vecchio.row)
        {
            // NSLog(@"INDEX:%@ old:%@",index,vecchio);
            [tabella reloadRowsAtIndexPaths:@[indexPath,vecchio] withRowAnimation:UITableViewRowAnimationFade];
            [self caricoWeb];
        }
    else{
        // NSLog(@"Selezionato Stesso elemento");
    }
}

-(IBAction)addNewReadings:(UIButton*)sender{
        // NSLog(@"PROCEDURA LETTURE EDITOR DA CREARE");
        [self showLettureEditor:[NSIndexPath indexPathForRow:lista.count inSection:0]];
}


#pragma mark --------> FINE - Table view

#pragma mark -
#pragma mark SHOW LETTURA EDITOR
//=================================
// SHOW LETTURA EDITOR
//=================================
-(IBAction)editLetturaRow:(UIButton*)sender{
    [self showLettureEditor:[NSIndexPath indexPathForRow:index.integerValue inSection:0]];
}

-(void)showLettureEditor:(NSIndexPath*)indexPath
{
    // NSLog(@"showLettureEditor");
    if(messa_editor_ctrl != nil) return;
    messa_editor_ctrl = [[Editor alloc] initLettureWithDelegate:self andMessa:messa forLetture:[NSNumber numberWithInteger:indexPath.row]];
    //#define MENU_IPAD_SLIDE_OFFSET_PORT 510
    //#define MENU_IPAD_SLIDE_OFFSET_LAND 766
    [messa_editor_ctrl.view setFrame:self.view.frame];
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
    
    
    messa_editor_ctrl.view.alpha = 0;
    
    [self.view addSubview:messa_editor_ctrl.view];
    
    [UIView animateWithDuration:0.7
                     animations:^{messa_editor_ctrl.view.alpha = 1;}
                     completion:^(BOOL finito){
                         // NSLog(@"fine show Editor Messa:%@",NSStringFromCGRect(messa_editor_ctrl.view.frame));
                         
                     }];
}

-(void)dismissEditor:(NSIndexPath*)indexPath{
    // NSLog(@"dismissLettureEditor");
    [self initList];
    [self didRotate];
    [UIView beginAnimations:@"SparisceLettureEditor" context:NULL];
    [UIView setAnimationDuration:0.4];
    messa_editor_ctrl.view.alpha = 0;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(fineDismissEditor)];
    [UIView commitAnimations];
}

-(void)fineDismissEditor{
    [messa_editor_ctrl.view removeFromSuperview];
    messa_editor_ctrl = nil;
}


#pragma mark -
#pragma mark BOUCE APPEAR ANIMATION
//=================================
// BOUCE APPEAR ANIMATION
//=================================
- (void)showView
{
    
    [UIView animateWithDuration:0.2 animations:
     ^(void){
         self.subview.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.1f, 1.1f);
         self.subview.alpha = 0.5;
     }
                     completion:^(BOOL finished){
                         [self bounceOutAnimationStoped];
                     }];
}
- (void)bounceOutAnimationStoped
{
    [UIView animateWithDuration:0.1 animations:
     ^(void){
         self.subview.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.9, 0.9);
         self.subview.alpha = 0.8;
     }
                     completion:^(BOOL finished){
                         [self bounceInAnimationStoped];
                     }];
}
- (void)bounceInAnimationStoped
{
    [UIView animateWithDuration:0.1 animations:
     ^(void){
         self.subview.transform = CGAffineTransformScale(CGAffineTransformIdentity,1, 1);
         self.subview.alpha = 1.0;
         
     }
                     completion:^(BOOL finished){
                         [self animationStoped];
                     }];
}
- (void)animationStoped
{
    // NSLog(@"subview:%@",NSStringFromCGRect(self.subview.frame));
}

- (void)hideView
{
    [UIView animateWithDuration:0.2 animations:
     ^(void){
         self.subview.alpha = 0.5;
         self.subview.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.6, 0.6);
     }
                     completion:^(BOOL finished){
                         [self hideStep2];
                     }];
}

- (void)hideStep2
{
    [UIView animateWithDuration:0.2 animations:
     ^(void){
         self.subview.alpha = 5;
     }
                     completion:^(BOOL finished){
                         [self endHide];
                     }];
}

- (void)endHide
{
    self.subview.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.6, 0.6);
}



#pragma mark - SEARCHBAR
//=========================
// SEARCHBAR
//=========================
-(void)searchBar:(UISearchBar *)s textDidChange:(NSString *)searchText{
    // NSLog(@"textDidChangeQUI:%@",searchBar.text);
    
    if([searchBar.text isEqualToString:@""]) {
        // NSLog(@"cancel!");
        [self ripristinaLista];
        return;
    }
    
    id old;
    
    if(index_filtrato.integerValue>-1){
        old = [listaFiltrata objectAtIndex:index_filtrato.integerValue];
    }
    else old = [lista objectAtIndex:index.integerValue];
    
    // NSLog(@"old:%@",old);
    
    [listaFiltrata removeAllObjects];
    
    if([[lista objectAtIndex:0] isKindOfClass:[Item class]]){
        
        for (Item *dict in lista)
        {
            if(dict.titolo)
            {
                NSRange r = [dict.titolo rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (r.location != NSNotFound) [listaFiltrata addObject:dict];
            }
        }
        
    }
    
    //->ITEM-MESSALE(COMUNE)
    if([[lista objectAtIndex:0] isKindOfClass:[MissalObject class]])
    {
        for (MissalObject *dict in lista)
        {
            if(dict.title)
            {
                NSRange r = [dict.title rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (r.location != NSNotFound) [listaFiltrata addObject:dict];
            }
        }
        
    }
    
    //->ITEM-BREVIARIO(COMUNE)
    if([[lista objectAtIndex:0] isKindOfClass:[BreviarioObject class]])
    {
        for (BreviarioObject *dict in lista)
        {
            if(dict.title)
            {
                NSRange r = [dict.title rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (r.location != NSNotFound) [listaFiltrata addObject:dict];
            }
        }
        
    }
    
    if([listaFiltrata indexOfObject:old]!=NSNotFound){
        index_filtrato = [NSNumber numberWithInteger:[listaFiltrata indexOfObject:old]];
    }
    else{
        index_filtrato = [NSNumber numberWithInteger:-1];
    }
    
    // NSLog(@"INDEX:%@",index);
    // NSLog(@"INDEX_FILTRATO:%@",index_filtrato);
    
    [self ricaricaTabella];
}

#pragma mark delegate UISearchBar
//===============================
//DELEGATE UISearchBar
//===============================
- (IBAction)searchBarCancelButtonClicked:(UISearchBar *)s{
    // NSLog(@"searchBarCancelButtonClicked:%@",searchBar);
    searchBar.text = @"";
    [self ripristinaLista];
    [searchBar resignFirstResponder];
    [btn_closeKeyboard setHidden:YES];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    // NSLog(@"selectedScopeButtonIndexDidChange");
    [UIView transitionWithView: btn_closeKeyboard
                      duration: 0.7
                       options: UIViewAnimationOptionTransitionFlipFromTop
                    animations: ^{
                        [btn_closeKeyboard setHidden:NO];
                    }
                    completion:^(BOOL finished){
                        // NSLog(@"Fine");
                    }];
    
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)s{
    // NSLog(@"searchBarSearchButtonClicked");
    [searchBar resignFirstResponder];
    [btn_closeKeyboard setHidden:YES];
    
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)s{
    // NSLog(@"searchBarShouldBeginEditing");
    [UIView transitionWithView: btn_closeKeyboard
                      duration: 0.7
                       options: UIViewAnimationOptionTransitionFlipFromTop
                    animations: ^{
                        [btn_closeKeyboard setHidden:NO];
                    }
                    completion:^(BOOL finished){
                        // NSLog(@"Fine");
                    }];
    return YES;
}


-(IBAction)chiudiTastiera:(UIButton*)sender{
    [btn_closeKeyboard setHidden:YES];
    [searchBar performSelectorOnMainThread:@selector(resignFirstResponder) withObject:nil waitUntilDone:YES];
}

#pragma mark - WEB delegate
//=========================
// WEB DELEGATE
//=========================
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    // NSLog(@"webViewDidFinishLoad");
}

#pragma mark -
#pragma mark Reload Tabella
//=========================
// Reload Tabella
//=========================
-(void)ripristinaLista{
    // NSLog(@"RipristinaLista");
    if(index_filtrato.integerValue > -1){
        id old = [listaFiltrata objectAtIndex:index_filtrato.integerValue];
        index = [NSNumber numberWithInteger:[lista indexOfObject:old]];
        index_filtrato =[NSNumber numberWithInteger:-1];
    }
    [listaFiltrata removeAllObjects];
    [listaFiltrata addObjectsFromArray:lista];
    [self ricaricaTabella];
}

-(void)ricaricaTabella{
    [self performSelectorOnMainThread:@selector(mainRicaricaTabella) withObject:nil waitUntilDone:NO];
}

-(void)mainRicaricaTabella{
    [tabella reloadData];
}

#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================
-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)didRotate{
    // NSLog(@"Selettore:didRotate");
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    // NSLog(@"screenBounf:%@ self:%@",NSStringFromCGRect(screenBound),self.view);
    [self.view setFrame:CGRectMake(0, 0, screenBound.size.width, screenBound.size.height)];
    // NSLog(@"self:%@",self.view);
    [subview setCenter:self.view.center];
    CGRect frame = label_uno.frame;
    frame.origin.y = self.isLand?12:87;
    [label_uno setFrame:frame];
    
    frame = label_due.frame;
    frame.origin.y = self.isLand?716:922;
    [label_due setFrame:frame];
    
    frame = linea.frame;
    frame.origin.y = self.isLand?723:912;
    [linea setFrame:frame];

    
}

@end

