//
//  TitoloMessa.m
//  iBreviary
//
//  Created by leyo on 20/05/14.
//  Copyright (c) 2014 leyo. All rights reserved.
//

#import "TitoloMessa.h"
#import "Home.h"
#import "Messa.h"

@interface TitoloMessa ()

@property (nonatomic, strong) BaseController<TitoloMessaDelegate> *delegate;
@property (nonatomic, strong) NSString *titoloMessaUfficiale;
@property (nonatomic, strong) NSDate *data_messa;
@property (nonatomic, strong) LinguaObject *linguaMessa;
@property (nonatomic) BOOL thenComune;
@property (nonatomic, strong) NSNumber *multiLetture_selettore;
@property (nonatomic, strong) NSMutableArray *multiMissal;


@property (nonatomic, strong) IBOutlet UITextField *text;
@property (nonatomic, strong) IBOutlet UIButton *close;
@property (nonatomic, strong) IBOutlet UIButton *skip;
@property (strong, nonatomic) IBOutlet UILabel *titolo_label;
@property (strong, nonatomic) IBOutlet UILabel *titolo_content;
@property (strong, nonatomic) IBOutlet UIView *linea;
@property (strong, nonatomic) IBOutlet UIImageView *backBlur;
@property (strong, nonatomic) IBOutlet UIImageView *skip_back;

//edit titolo property
@property (nonatomic, strong) BaseController<TitoloEditMessaDelegate> *edit_delegate;
@property (assign,nonatomic) BOOL modificoTitolo;
@property (strong,nonatomic) Messa *messa_change_titolo;

@end

@implementation TitoloMessa

@synthesize titoloMessaUfficiale, data_messa, linguaMessa;
@synthesize text,delegate,skip,close;
@synthesize titolo_label, titolo_content;
@synthesize linea, backBlur,thenComune;
@synthesize multiLetture_selettore, multiMissal;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<TitoloMessaDelegate>*)d
                andTit:(NSString*)tit
                  lang:(LinguaObject*)l
                  date:(NSString*)data
            thenComune:(BOOL)c
{
    NSString *nibNameOrNil = @"TitoloMessa_ipad";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        titoloMessaUfficiale = tit;
        NSDateFormatter *f = [NSDateFormatter new];
        // NSLog(@"data:%@",data);
        [f setDateFormat:@"yyyy-MM-dd"];
        data_messa = [f dateFromString:data];
        linguaMessa = l;
        thenComune = c;
        self.modificoTitolo = NO;
    }
    return self;
}

- (id)initForEditTitoloAction:(BaseController<TitoloEditMessaDelegate>*)d
                        messa:(Messa*)m
{
    NSString *nibNameOrNil = @"TitoloMessa_ipad";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        self.edit_delegate = d;
        self.messa_change_titolo = m;
        titoloMessaUfficiale = m.day.title;
        NSDateFormatter *f = [NSDateFormatter new];
        // NSLog(@"data:%@",data);
        [f setDateFormat:@"yyyy-MM-dd"];
        data_messa = [f dateFromString:m.day.id_date];
        // NSLog(@"data:%@",data);
        thenComune = NO;
        self.modificoTitolo = YES;
    }
    return self;
}

//=========================
// StatusBar
//=========================
#pragma mark - StatusBar
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
-(IBAction)annullaMessa:(UIButton*)sender{
    // NSLog(@"annullaMessa!");
    [text resignFirstResponder];
    if(self.modificoTitolo){
        if(self.edit_delegate){
            [self.edit_delegate fineTitoloEdit];
        }
    }
    else{
        [delegate titoloAnnullato];
    }
}

-(IBAction)skippa:(UIButton*)sender{
    // NSLog(@"Skippa Titolo");
    [text resignFirstResponder];
    if(self.modificoTitolo){
        [self titoloEditato];
    }
    else{
        [delegate titolosalvato:titoloMessaUfficiale thenComune:thenComune];
    }
}

#pragma mark -
#pragma mark TEXTFIELD DELEGATE
//=========================
// TEXTFIELD DELEGATE
//=========================
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;

}


- (void)textFieldDidBeginEditing:(UITextField *)textField{
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;

}


- (void)textFieldDidEndEditing:(UITextField *)textField{
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;

}


- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    // NSLog(@"textField:%@ messa:%@",textField.text,titoloMessaUfficiale);
    if(self.modificoTitolo){
        [self titoloEditato];
    }
    else{
        [delegate titolosalvato:(text.text.length>0)?text.text:titoloMessaUfficiale thenComune:thenComune];
        }
    return YES;
}

-(void)titoloEditato{
    [self.messa_change_titolo setMessa_name:self.text.text];
    [AppDelegate saveContext];
    if(self.edit_delegate){
        [self.edit_delegate fineTitoloEdit];
    }
    
    runOnMainQueueWithoutDeadlocking(^{
        [[NSNotificationCenter defaultCenter] postNotificationName:AGGIORNA_LISTA_MESSE object:nil];
    });
}


#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"TitMessa DidLoad delegate:%@",NSStringFromCGRect(self.edit_delegate.view.frame));
    if(self.modificoTitolo){
        CGRect screenBound = self.edit_delegate.view.frame;
        [self.view setFrame:screenBound];
    }
    else{
        CGRect screenBound = delegate.view.frame;
        if(delegate.view.frame.origin.x>0) screenBound.origin.x = -delegate.view.frame.origin.x;
        [self.view setFrame:screenBound];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self
                                                            selector:@selector(didRotate)
                                                                name:DID_ROTATE object:nil];
    [self gui];
}


-(void)gui{
    
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"EEEE d MMMM YYYY"];
    [titolo_label setText:[[f stringFromDate:data_messa] uppercaseString]];
    
    [close setTitle:NSLocalizedString(@"annulla", nil) forState:UIControlStateNormal];
    [skip setTitle:NSLocalizedString(@"skip", nil) forState:UIControlStateNormal];
    UIColor *color = [[UIColor whiteColor] colorWithAlphaComponent:0.3];
    
    [titolo_content setText:[titoloMessaUfficiale uppercaseString]];
    text.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"titolo_messa_placeholder", nil) attributes:@{NSForegroundColorAttributeName: color}];
    [backBlur setImage:[UIImage imageNamed:self.isPort?@"blur_ipad_port.png":@"blur_ipad_land.png"]];
    
    if(self.modificoTitolo){
        NSLog(@"%@",self.messa_change_titolo.messa_name);
        NSLog(@"%@",self.messa_change_titolo.titolo_custom);
        NSLog(@"%@",self.messa_change_titolo.day.title);
        if(self.messa_change_titolo.messa_name==nil)
            self.messa_change_titolo.messa_name = self.messa_change_titolo.day.title;
        [text setText:self.messa_change_titolo.messa_name];
        [self.skip setHidden:YES];
        [self.skip_back setHidden:YES];
    }
}

#pragma mark - Appear
//===================
//APPEAR
//===================
- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    [text becomeFirstResponder];
    [self didRotate];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!self.modificoTitolo){
        [self checkMultiLetture];
    }
}

-(void)checkMultiLetture{
    multiMissal = [[NSMutableArray alloc] initWithArray:[[[Home getCurrentMessa] day] listaMissals]];
    // NSLog(@"multiMissal:%@",multiMissal);
    multiLetture_selettore = [NSNumber numberWithInteger:0];
    if(multiMissal.count > 1){
#ifdef __IPHONE_8_0
        UIAlertController * view =  [UIAlertController
                                     alertControllerWithTitle:NSLocalizedString(@"messaMultiMissalTitolo", nil)
                                     message:NSLocalizedString(@"messaMissalContent", nil)
                                     preferredStyle:UIAlertControllerStyleActionSheet];
        
        for(NSInteger i=0; i<multiMissal.count; i++){
            MissalObject *m = [multiMissal objectAtIndex:i];
            // NSLog(@"I:%i m:%@ t:%@ type:%@",(int)i,m,m.title,m.type);
            NSString *titolo = m.title?m.title:@"";
            if(m.type){
                titolo = [titolo stringByAppendingFormat:@" (%@)",m.type];
                        }
            UIAlertAction* m_i = [UIAlertAction actionWithTitle:titolo
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                                                {
                                                                [self selezionataMultiLetture:i];
                                                                [view dismissViewControllerAnimated:YES completion:nil];
                                                                }];
            [view addAction:m_i];
            }//end for
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     [self annullaMessa:nil];
                                }];
        
        
        [view addAction:cancel];
        /*
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self presentViewController:view animated:YES completion:nil];
        }); //*/
        
        [view setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [view popoverPresentationController];
        popPresenter.sourceView = titolo_label;
        popPresenter.sourceRect = titolo_label.bounds;
        [[DDMenuController sharedInstance] presentViewController:view animated:YES completion:^{NSLog(@"ActionSheet showed");}];
#endif
    }//end if multiMissal
    
}

-(void)selezionataMultiLetture:(NSInteger)n{
    // NSLog(@"selezionataMultiLettura:%i",(int)n);
    multiLetture_selettore = [NSNumber numberWithInteger:n];
    MissalObject *m = [multiMissal objectAtIndex:n];
    NSString *titolo = m.title;
    if(m.type){
        titolo = [titolo stringByAppendingFormat:@" (%@)",m.type];
    }
    [titolo_content setText:[titolo uppercaseString]];
    
    titoloMessaUfficiale = m.title;
    
    //insieme alle letture aggregate, il selettore cambia anche ANTIPHON OPEN, prayer_over_the_gifts, ANTIPHON AFTER
    [[Home getCurrentMessa] cambiaMessale:n];
    
}


#pragma mark - Disappear
//===================
//DISAPPEAR
//===================
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}



#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}



- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    // NSLog(@"TitoloMessa:didRotateFromInterfaceOrientation");
    [self didRotate];
    }

- (void)didRotate{
    // NSLog(@"TitoloMessa_DIDROTATE:%i",self.isLand);
    if(self.modificoTitolo){
        CGRect screenBound = self.edit_delegate.view.frame;
        [self.view setFrame:screenBound];
    }
    else{
        CGRect screenBound = delegate.view.frame;
        if(delegate.view.frame.origin.x>0) screenBound.origin.x = -delegate.view.frame.origin.x;
        [self.view setFrame:screenBound];
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    // NSLog(@"TitoloMessa: willRotateToInterfaceOrientation");

}


//=========================
// END CLASS
//=========================

@end


