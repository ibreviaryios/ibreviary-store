//
//  OggettoMailSelector.m
//  iBreviary_PRO
//
//  Created by ileyo on 07/12/10.
//  Copyright 2010 leyo.breviario. All rights reserved.
//

#import "OggettoMailSelector.h"
#import "AAMFeedbackViewController.h"
#import "AAMFeedbackTopicsViewController.h"
#import "SystemData.h"
#include <sys/types.h>
#include <sys/sysctl.h>

@implementation OggettoMailSelector

@synthesize POP_area, sfondo_pop_area, lista_oggetti,popPosition, annulla , delegate;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<OggettoMailSelectorDelegate>*)d fromButtonCenter:(CGPoint)b
{
    NSString *nibNameOrNil;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) nibNameOrNil = @"OggettoMailSelector_ipad";
    else
        nibNameOrNil = @"OggettoMailSelector5";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        
        popPosition = CGPointMake(b.x-16, b.y+20);
    }
    return self;
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    lista_oggetti = [[NSArray alloc]initWithObjects:
                     NSLocalizedString(@"FeedbackTopics001",nil),
                     NSLocalizedString(@"FeedbackTopics002",nil),
                     NSLocalizedString(@"FeedbackTopics007",nil),
                     NSLocalizedString(@"FeedbackTopics003",nil),
                     NSLocalizedString(@"FeedbackTopics006",nil),
                     NSLocalizedString(@"FeedbackTopics004",nil),
                     NSLocalizedString(@"FeedbackTopics005",nil), nil];
    
    // NSLog(@"OggettoMailSelector DidLoad:%@\n%@",NSStringFromCGPoint(popPosition),lista_oggetti);
    [self gui];
}

-(void)gui{
        
    CGFloat topoffset = 5;
    CGFloat row_height = 40;
    [POP_area setFrame:CGRectMake(31+popPosition.x-POP_area.frame.size.width, popPosition.y-((topoffset*2)+(row_height*lista_oggetti.count))-28, POP_area.frame.size.width, (topoffset*2)+(row_height*lista_oggetti.count))];
    // NSLog(@"POP_area:%@",NSStringFromCGRect(POP_area.frame));
    [sfondo_pop_area setFrame:CGRectMake(sfondo_pop_area.frame.origin.x-5, sfondo_pop_area.frame.origin.y-5, sfondo_pop_area.frame.size.width+10, POP_area.frame.size.height+19)];
    //top, left, bottom, right
    [sfondo_pop_area setImage:[[UIImage imageNamed:@"base_mailoggetto.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(30, 30, 30, 30)]];
    for(int i = 0; i < lista_oggetti.count; i++)
    {
        NSString *row = [lista_oggetti objectAtIndex:i];
        UIButton *bottone = [UIButton buttonWithType:UIButtonTypeCustom];
        [bottone setFrame:CGRectMake(0, topoffset+(i*row_height)+5, POP_area.frame.size.width, row_height-10)];
        [bottone setBackgroundColor:[UIColor clearColor]];
        [bottone setTag:i];
        [bottone setTitle:row forState:UIControlStateNormal];
        [bottone setTitleColor:[UIColorFromRGB(0XFFFFFF) colorWithAlphaComponent:0.95] forState:UIControlStateNormal];
        [bottone setTitleColor:[UIColorFromRGB(0Xf0b26e) colorWithAlphaComponent:0.8] forState:UIControlStateHighlighted];
        [bottone setImage:[UIImage imageWithImage:[UIImage imageNamed:@"tabella_notifiche_row1.png"] scaledToSize:CGSizeMake(15, 11)] forState:UIControlStateNormal];
        [bottone setAdjustsImageWhenHighlighted:NO];
        [bottone setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 20.0, 0.0, 0.0)];
        [bottone setImageEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)];
        [bottone.titleLabel setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:14]];
        [bottone setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [bottone addTarget:self action:@selector(selectRowButton:) forControlEvents:UIControlEventTouchUpInside];
        [POP_area addSubview:bottone];
    }
    
    UILabel *titolo = [[UILabel alloc] initWithFrame:CGRectMake(0, -10, POP_area.frame.size.width, 18)];
    [titolo setText:[NSLocalizedString(@"AAMFeedbackTitle", nil) uppercaseString]];
    [titolo setTextColor:[UIColor whiteColor]];
    [titolo setBackgroundColor:[UIColor clearColor]];
    [titolo setTextAlignment:NSTextAlignmentCenter];
    [titolo setFont:[UIFont fontWithName:FONT_HELVETICANEUE_REGULAR size:15]];
    [POP_area addSubview:titolo];
    
}


#pragma mark -
#pragma mark AZIONI
//=========================
// AZIONI
//=========================
-(IBAction)selectRowButton:(UIButton*)sender{
    // NSLog(@"Mail Oggetto: selectRowButton:%@",[lista_oggetti objectAtIndex:sender.tag]);
    //[delegate removeOggettoMailSelectorWithOggetto:[lista_oggetti objectAtIndex:sender.tag]];
    
    //MANDO MAIL
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    [picker setToRecipients:[NSArray arrayWithObject:@"ibreviary@me.com"]];
    
    NSArray *oggetto  = [[NSArray alloc]initWithObjects:
                                         @"Info",
                                         @"Richieste",
                                         @"Proposte",
                                         @"Bug Report",
                                         @"Crash Report",
                                         @"Segnala errore in un testo",
                                         @"Aiuta iBreviary", nil];
    
    [picker setSubject:[NSString stringWithFormat:@"%@[%@]: %@", [[[NSBundle mainBundle] infoDictionary] objectForKey:
                                                              @"CFBundleDisplayName"],[NSLocalizedString(@"checklingua", nil) uppercaseString],[oggetto objectAtIndex:sender.tag]]];
    [picker setMessageBody:[self feedbackBody] isHTML:YES];
    [self presentViewController:picker animated:YES completion:^{NSLog(@"Fine Appare MAIL composer");[self.view setAlpha:0];}];
}

- (NSString*)feedbackBody
{
    NSError *error= nil;
    NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@""];
    
    //iOS version
    NSString *display = [NSString stringWithString:([[UIScreen mainScreen] scale]>=2)?@"retina":@"normale"];
    NSString *fullPath = [[NSBundle mainBundle] pathForResource:@"infoDevice" ofType:@"html"];
    
    NSString *header = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding  error:&error];
    if(error){
        // NSLog(@"infoDevice NON trovato:%@",[error description]);
    }
    NSString *info = [NSString stringWithFormat:header,NSLocalizedString(@"AAMFeedbackDescriptionPlaceholder", nil),[SystemData platformString],[UIDevice currentDevice].systemVersion,display,self.iBreviary_version];
    
    [emailBody appendString:info];
    return emailBody;
}


#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
-(IBAction)FineOggettoMailSelector:(UIButton*)sender{
    // NSLog(@"FineAdvanced");
    [delegate removeMailOggettoCTRL];
}


#pragma mark -
#pragma mark MAIL DELEGATE
//=========================
// MAIL DELEGATE
//=========================
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self performSelectorOnMainThread:@selector(dismissEmail:) withObject:controller waitUntilDone:YES];
}

-(void)dismissEmail:(MFMailComposeViewController *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
    [delegate removeMailOggettoCTRL];
}

#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return self.isIpad?UIInterfaceOrientationMaskAll:UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate {
    return self.isIpad;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return self.isIpad;
}



@end
