//
//  OggettoMailSelector.h
//  iBreviary_PRO
//
//  Created by ileyo on 07/12/10.
//  Copyright 2010 leyo.breviario. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "BaseController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@protocol OggettoMailSelectorDelegate <NSObject>
-(void)removeMailOggettoCTRL;
@end


@interface OggettoMailSelector : BaseController<MFMailComposeViewControllerDelegate>

    
@property (nonatomic, strong) BaseController<OggettoMailSelectorDelegate> *delegate;
@property (strong, nonatomic) IBOutlet UIView *POP_area;
@property (strong, nonatomic) IBOutlet UIButton *annulla;
@property (strong, nonatomic) IBOutlet UIImageView *sfondo_pop_area;
@property (strong, nonatomic) NSArray *lista_oggetti;
@property (nonatomic) CGPoint popPosition;


- (id)initWithDelegate:(BaseController<OggettoMailSelectorDelegate>*)d fromButtonCenter:(CGPoint)b;

@end
