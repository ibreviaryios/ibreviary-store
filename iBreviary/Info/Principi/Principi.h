//
//  Principi.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"


@protocol PrincipiDelegate <NSObject>
- (void)removePrincipi;
@end


@interface Principi : BaseController


@property (nonatomic, strong) BaseController<PrincipiDelegate> *delegate;

@property (nonatomic, strong) IBOutlet UIWebView *webConteiner;
@property (nonatomic, strong) IBOutlet UIImageView *sfondo;
@property (nonatomic, strong) IBOutlet UIButton *close;
@property (strong, nonatomic) IBOutlet UILabel *data_label;


- (id)initWithDelegate:(BaseController<PrincipiDelegate>*)d;


@end

