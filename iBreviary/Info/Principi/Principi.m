//
//  Principi.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "Principi.h"


@implementation Principi


@synthesize webConteiner, sfondo, close,data_label,delegate;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<PrincipiDelegate>*)d
{
    //iPhone4
    NSString *nibNameOrNil = @"Principi";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"Principi_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"Principi5";
    if(IS_IPHONE_6) nibNameOrNil = @"Principi6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"Principi6Plus";

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        if(self.isIpad) [self.view setFrame:d.view.frame];
    }
    return self;
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    

	webConteiner.backgroundColor = [UIColor clearColor];
	webConteiner.opaque = NO;
    
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
        
	// immagine bottone santo
	NSString *path = [[NSBundle mainBundle] bundlePath];
	NSURL *baseURL = [NSURL fileURLWithPath:path];

    
    NSString *html=@"";
    
    NSString *princ_path = [NSString stringWithFormat:@"%@/principi_%@.html",path,NSLocalizedString(@"checklingua", nil)];
    if([self fileExistsAtPath:princ_path])
        html = [NSString stringWithContentsOfFile:princ_path encoding:NSUTF8StringEncoding error:nil];
    else
        html = [NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/principi_en.html",path] encoding:NSUTF8StringEncoding error:nil];

    
    [webConteiner loadHTMLString:html baseURL:baseURL];
    //[data_label setFont:[UIFont fontWithName:FONT_ROMAN_ANTIQUE size:21]];
    //[data_label setText:detailCtrl.day.date];

}

#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
- (IBAction)FinePrincipi:(UIButton *)sender{
    [delegate removePrincipi];
}


#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setData_label:nil];
    [super viewDidUnload];
}


#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

//=========================
// ENDCLASS
//=========================
@end


