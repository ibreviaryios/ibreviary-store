//
//  GuidaPreghiera.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//


#import "GuidaPreghiera.h"


@implementation GuidaPreghiera


@synthesize webConteiner, sfondo, close,data_label,delegate;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<GuidaPreghieraDelegate>*)d
{
    //iPhone4
    NSString *nibNameOrNil = @"GuidaPreghiera";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"GuidaPreghiera_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"GuidaPreghiera5";
    if(IS_IPHONE_6) nibNameOrNil = @"GuidaPreghiera6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"GuidaPreghiera6Plus";
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        if(self.isIpad) [self.view setFrame:d.view.frame];
    }
    return self;
}


#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    
	webConteiner.backgroundColor = [UIColor clearColor];
	webConteiner.opaque = NO;
    
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
    
	// link
	NSString *path = [[NSBundle mainBundle] bundlePath];
	NSURL *baseURL = [NSURL fileURLWithPath:path];

    NSString *html=@"";
    
    NSString *guida_path = [NSString stringWithFormat:@"%@/guida_%@.html",path,NSLocalizedString(@"checklingua", nil)];
    if([self fileExistsAtPath:guida_path])
        html = [NSString stringWithContentsOfFile:guida_path encoding:NSUTF8StringEncoding error:nil];
    else
    html = [NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/guida_en.html",path] encoding:NSUTF8StringEncoding error:nil];

    
    [webConteiner loadHTMLString:html baseURL:baseURL];
}

#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
- (IBAction)FineGuidaPreghiera:(UIButton *)sender{
    [delegate removeGuidaPreghiera];
}


#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setData_label:nil];
    [super viewDidUnload];
}


#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

//=========================
// ENDCLASS
//=========================
@end


