//
//  AAMFeedbackViewController.m
//  AAMFeedbackViewController
//
//  Created by 深津 貴之 on 11/11/30.
//  Copyright (c) 2011年 Art & Mobile. All rights reserved.
//

#import "AAMFeedbackViewController.h"
#import "AAMFeedbackTopicsViewController.h"
#include <sys/types.h>
#include <sys/sysctl.h>

@interface AAMFeedbackViewController(private)
    - (NSString*)_feedbackSubject;
    - (NSString*)_feedbackBody;
    - (NSString*)_appName;
    - (NSString*)_appVersion;
    - (NSString*)_selectedTopic;
    - (NSString*)_selectedTopicToSend;
    - (void)_updatePlaceholder;
@end


@implementation AAMFeedbackViewController

@synthesize descriptionText;
@synthesize topics;
@synthesize topicsToSend;
@synthesize toRecipients;
@synthesize ccRecipients;
@synthesize bccRecipients;


+ (BOOL)isAvailable
{
    if([MFMailComposeViewController class]){
        return YES;
    }
    return NO;
}

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
-(id)init
{
    NSString *nibNameOrNil = ([[UIScreen mainScreen] bounds].size.height == 568)?@"FeedMail5":@"FeedMail5";
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    
    if(self){

        self.topics = [[NSArray alloc]initWithObjects:
                       @"FeedbackTopics001",
                       @"FeedbackTopics002",
                       @"FeedbackTopics003",
                       @"FeedbackTopics004",
                       @"FeedbackTopics005",
                       @"FeedbackTopics006", nil];
        
        self.topicsToSend = [[NSArray alloc]initWithObjects:
                             @"Info",
                             @"Richieste/Proposte",
                             @"Bug Report",
                             @"Errore in testo",
                             @"Aiuta iBreviary",
                             @"Crash Bug", nil];
    }
    return self;
}

- (id)initWithTopics:(NSArray*)theIssues
{
    self = [self init];
    if(self){
        self.topics = theIssues;
        self.topicsToSend = theIssues;
    }
    return self;
}


#pragma mark - View lifecycle

- (void)loadView
{
    [super loadView];
    self.title = NSLocalizedString(@"AAMFeedbackTitle", nil);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelDidPress:)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"AAMFeedbackButtonMail", nil) style:UIBarButtonItemStyleDone target:self action:@selector(nextDidPress:)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabella.scrollEnabled = NO;

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    _descriptionPlaceHolder = nil;
    _descriptionTextView = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self _updatePlaceholder];
    [self.tabella reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(_isFeedbackSent){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
 
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================

-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

// Tell the system It should autorotate
- (BOOL) shouldAutorotate {
    return YES;
}
- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    // NSLog(@"Feedback will Rotate");
     [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    // NSLog(@"Feedback did Rotate");
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0){
        return 2;
    }
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0 && indexPath.row==1){
        float t = MAX(88, _descriptionTextView.contentSize.height);
        return t;
    }
    
    return 44.f;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return NSLocalizedString(@"FeedbackTopicsHeader", nil);
            break;
        case 1:
            return NSLocalizedString(@"FeedbackTopicsBasicInfo", nil);
            break;
        default:
            break;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        if(indexPath.section==1){
            //General Infos
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        }else{
            if(indexPath.row==0){
                //Topics
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1      reuseIdentifier:CellIdentifier];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }else{
                //Topics Description
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault      reuseIdentifier:CellIdentifier];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                _descriptionTextView = [[UITextView alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-100, 88)];
                _descriptionTextView.backgroundColor = [UIColor clearColor];
                _descriptionTextView.font = [UIFont systemFontOfSize:16];
                _descriptionTextView.delegate = self;
                _descriptionTextView.scrollEnabled = NO;
                _descriptionTextView.text = self.descriptionText;
                [cell.contentView addSubview:_descriptionTextView];
                
                _descriptionPlaceHolder = [[UITextField alloc]initWithFrame:CGRectMake(16, 8, 300, 20)];
                _descriptionPlaceHolder.font = [UIFont systemFontOfSize:16];
                _descriptionPlaceHolder.placeholder = NSLocalizedString(@"AAMFeedbackDescriptionPlaceholder", nil);
                _descriptionPlaceHolder.userInteractionEnabled = NO;
                [cell.contentView addSubview:_descriptionPlaceHolder];
                
                [self _updatePlaceholder];
            }
        }
    }
    
    // Configure the cell...
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    
                    cell.textLabel.text = NSLocalizedString(@"AAMFeedbackTopicsTitle", nil);
                    cell.detailTextLabel.text = NSLocalizedString([self _selectedTopic],nil);
                    break;
                case 1:
                default:
                    break;
            }
            break;
        case 1:
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Device";
                    cell.detailTextLabel.text = [AAMFeedbackViewController platformString];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    break;
                case 1:
                    cell.textLabel.text = @"iOS";
                    cell.detailTextLabel.text = [UIDevice currentDevice].systemVersion;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    break;
                case 2:
                    cell.textLabel.text = @"App Name";
                    cell.detailTextLabel.text = [self _appName];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    break;
                case 3:
                    cell.textLabel.text = @"App Version";
                    cell.detailTextLabel.text = [self _appVersion];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    
    // NSLog(@"cella:%@->%@",indexPath,cell.textLabel.text);
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0 && indexPath.row==0){
        [_descriptionTextView resignFirstResponder];
        
        AAMFeedbackTopicsViewController *vc = [[AAMFeedbackTopicsViewController alloc]initWithStyle:UITableViewStyleGrouped];
        vc.delegate = self;
        vc.selectedIndex = _selectedTopicsIndex;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (void)cancelDidPress:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)nextDidPress:(id)sender
{
    [_descriptionTextView resignFirstResponder];
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    [picker setToRecipients:self.toRecipients];
    [picker setCcRecipients:self.ccRecipients];  
    [picker setBccRecipients:self.bccRecipients];
    
    [picker setSubject:[self _feedbackSubject]];
    [picker setMessageBody:[self _feedbackBody] isHTML:YES];
    [self presentViewController:picker animated:YES completion:nil];
}


- (void)textViewDidChange:(UITextView *)textView
{
    CGRect f = _descriptionTextView.frame;
    f.size.height = _descriptionTextView.contentSize.height;
    _descriptionTextView.frame = f;
    [self _updatePlaceholder];
    self.descriptionText = _descriptionTextView.text;
    
    //Magic for updating Cell height
    NSArray *a = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:0]];
    [self.tabella reloadRowsAtIndexPaths:a withRowAnimation:UITableViewRowAnimationAutomatic];
}


-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if(result==MFMailComposeResultCancelled){
    }else if(result==MFMailComposeResultSent){
        [self performSelector:@selector(mailOk) withObject:nil afterDelay:2];
        _isFeedbackSent = YES;
    }else if(result==MFMailComposeResultFailed){
        [self performSelector:@selector(mailNO) withObject:nil afterDelay:0.2];
    }
    [self performSelectorOnMainThread:@selector(dismissEmail:) withObject:controller waitUntilDone:YES];
}

-(void)dismissEmail:(MFMailComposeViewController *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)mailOk{
    [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"FeedbackTopicsOK", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)mailNO{
    [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"FeedbackTopicsError", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)feedbackTopicsViewController:(AAMFeedbackTopicsViewController *)feedbackTopicsViewController didSelectTopicAtIndex:(NSInteger)selectedIndex {
    _selectedTopicsIndex = selectedIndex;
}

#pragma mark - Internal Info

- (void)_updatePlaceholder
{
    if([_descriptionTextView.text length]>0){
        _descriptionPlaceHolder.hidden = YES;
    }else{
        _descriptionPlaceHolder.hidden = NO;
    }
}

- (NSString*)_feedbackSubject
{
    return [NSString stringWithFormat:@"%@: %@", [self _appName],[self _selectedTopicToSend], nil];
}
   
- (NSString*)_feedbackBody
{
    NSError *error= nil;
    NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@""];
    
    //iOS version
    NSString *display = [NSString stringWithString:([[UIScreen mainScreen] scale]>=2)?@"retina":@"normale"];
    NSString *fullPath = [[NSBundle mainBundle] pathForResource:@"infoDevice" ofType:@"html"];
    
    NSString *header = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding  error:&error];
    if(error){
        // NSLog(@"infoDevice NON trovato:%@",[error description]);
    }
    NSString *text = [_descriptionTextView.text stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
    NSString *info = [NSString stringWithFormat:header,text,[AAMFeedbackViewController platformString],[UIDevice currentDevice].systemVersion,display,[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] ];

    [emailBody appendString:info];

    //NSString *body = [NSString stringWithFormat:@"%@%@",  _descriptionTextView.text, [self _appName],display, nil];

    return emailBody;
}

- (NSString*)_selectedTopic
{
    return [topics objectAtIndex:_selectedTopicsIndex];
}

- (NSString*)_selectedTopicToSend
{
    return [topicsToSend objectAtIndex:_selectedTopicsIndex];
}

- (NSString*)_appName
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:
            @"CFBundleDisplayName"];
}

- (NSString*)_appVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

+(NSString *)platformString
{
    int mib[2];
    size_t len;
    char *machine;
    
    mib[0] = CTL_HW;
    mib[1] = HW_MACHINE;
    sysctl(mib, 2, NULL, &len, NULL, 0);
    machine = malloc(len);
    sysctl(mib, 2, machine, &len, NULL, 0);
    
    NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
    free(machine);
    // NSLog(@"PLATFORM: %@",platform);
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"VerizoniPhone4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone5(GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone5(GSM_CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone5c(GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone5c(GSM_CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone5s(GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone5s(GSM_CDMA)";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPodTouch1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPodTouch2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPodTouch3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPodTouch4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPodTouch5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad2(WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad2(GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad2(CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad2(WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPadMini(WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPadMini(GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPadMini(GSM_CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad3(WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad3(GSM_CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad3(GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad4(WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad4(GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad4(GSM_CDMA)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return @"";
}

@end
