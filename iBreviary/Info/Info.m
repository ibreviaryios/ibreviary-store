//
//  Info.m
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import "Info.h"
#import "WebBrowser.h"

@implementation Info

@synthesize button_credits, button_email,button_guida,button_ibreviary,button_principi, delegate,close;
@synthesize title, credits_label, principi_label, email_label, website_label,mask;
@synthesize oggetto_mailCTRL;

#pragma mark -
#pragma mark INIT
//=========================
// INIT
//=========================
- (id)initWithDelegate:(BaseController<InfoDelegate>*)d
{
    //iPhone4
    NSString *nibNameOrNil = @"Info";
    
    //iPADs
    if(IS_IPAD) nibNameOrNil = @"Info_ipad";
    //if(IS_IPAD_PRO) nibNameOrNil = @"MenuViewController_ipadPro";
    
    //iPhoneS
    if(IS_IPHONE_5) nibNameOrNil = @"Info5";
    if(IS_IPHONE_6) nibNameOrNil = @"Info6";
    if(IS_IPHONE_6_PLUS) nibNameOrNil = @"Info6Plus";

    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        delegate = d;
        if(self.isIpad) [self.view setCenter:d.view.center];
    }
    return self;
}


#pragma mark -
#pragma mark AZIONI
//=========================
// AZIONI
//=========================
- (IBAction)azioneGuida:(UIButton*)sender
{
    [delegate showInfo_Guida];
}

- (IBAction)azionePrincipi:(UIButton*)sender
{
    [delegate showInfo_Principi];
}

- (IBAction)azioneWebIbreviary:(UIButton*)sender
{
    [delegate showInfo_iBreviary];
}

- (IBAction)azioneCredits:(UIButton*)sender
{
    [delegate showInfo_Credits];
}

- (IBAction)azioneEmail:(UIButton*)sender
{
    oggetto_mailCTRL = [[OggettoMailSelector alloc] initWithDelegate:self fromButtonCenter:sender.center];
	oggetto_mailCTRL.view.alpha = 0;
	[self.view addSubview:oggetto_mailCTRL.view];
    
    [button_principi setUserInteractionEnabled:NO];
    [button_guida setUserInteractionEnabled:NO];
	
	[UIView beginAnimations:@"AppareCredits" context:NULL];
    [UIView setAnimationDuration:0.5];
	oggetto_mailCTRL.view.alpha = 1;
    [UIView commitAnimations];
    
}

-(void)removeMailOggettoCTRL{
    // NSLog(@"removeMailOggettoCTRL");
    if([oggetto_mailCTRL.view superview])
    [UIView animateWithDuration:0.7
                     animations:^{
                         [oggetto_mailCTRL.view setAlpha:0];}
                     completion:^(BOOL finished){
                         // NSLog(@"FineRemoveMailOggetto");
                         [oggetto_mailCTRL.view removeFromSuperview];
                         oggetto_mailCTRL = nil;
                     }];
    else oggetto_mailCTRL = nil;
    
    [button_principi setUserInteractionEnabled:YES];
    [button_guida setUserInteractionEnabled:YES];
}

#pragma mark -
#pragma mark FINE
//=========================
// FINE
//=========================
-(IBAction)FineInfo:(UIButton*)sender{
    // NSLog(@"FineCUstodia");
    [delegate removeInfo];
}

#pragma mark -
#pragma mark DIDLOAD
//=========================
// DIDLOAD
//=========================
- (void)viewDidLoad {
    [super viewDidLoad];
    // NSLog(@"Info DidLoad");
    [self gui];
}

-(void)gui{
    if(self.isIpad)
    {
        [self.view.layer setCornerRadius:10];
        [self.view.layer setMasksToBounds:YES];
    }
    
    [button_ibreviary.layer setCornerRadius:7];
    [button_ibreviary.layer setMasksToBounds:YES];
    
    [mask.layer setCornerRadius:10];
    [mask.layer setMasksToBounds:YES];
    [mask.layer setBorderColor:[[UIColorFromRGB(0X2f261e) colorWithAlphaComponent:0.05] CGColor]];
    [mask.layer setBorderWidth:2];
    
    //CLOSE
    [close setTitle:NSLocalizedString(@"Fine", nil) forState:UIControlStateNormal];
    
    //GUIDA
    [button_guida.layer setCornerRadius:10];
    [button_guida.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:0.7] CGColor]];
    [button_guida setTitleColor:[UIColorFromRGB(0Xcacaca) colorWithAlphaComponent:0.8] forState:UIControlStateNormal];
    [button_guida setTitleColor:[UIColorFromRGB(0Xf0b26e) colorWithAlphaComponent:0.8] forState:UIControlStateHighlighted];
    [button_guida.layer setBorderWidth:1.2];
    [button_guida.layer setMasksToBounds:YES];
    [button_guida setTitle:[NSLocalizedString(@"bottone_Guida_Preghiera", @"Pray\nGuide") uppercaseString] forState:UIControlStateNormal];

    
    //PRINCIPI
    [button_principi.layer setCornerRadius:10];
    [button_principi.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:0.7] CGColor]];
    [button_principi.layer setBorderWidth:1.2];
    [button_principi.layer setMasksToBounds:YES];
    
    
    
    //PRINCIPI
    NSString *principi_text = [NSString stringWithFormat:@"%@\n%@", [NSLocalizedString(@"principi_text1", nil) uppercaseString],[NSLocalizedString(@"principi_text2", nil) uppercaseString] ];
    NSMutableParagraphStyle *style  = [NSMutableParagraphStyle new];
    [style setAlignment:NSTextAlignmentCenter];
    style.minimumLineHeight = button_principi.titleLabel.font.pointSize;
    style.maximumLineHeight = button_principi.titleLabel.font.pointSize;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,
                                  NSForegroundColorAttributeName :[UIColorFromRGB(0Xcacaca) colorWithAlphaComponent:0.8]};
    [[button_principi titleLabel] setNumberOfLines:3];
    [[button_principi titleLabel] setTextAlignment:NSTextAlignmentCenter];
    [[button_principi titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];
    [button_principi setAttributedTitle:[[NSAttributedString alloc] initWithString:principi_text attributes:attributtes] forState:UIControlStateNormal];
    NSDictionary *attributtesH = @{NSParagraphStyleAttributeName : style,
                                  NSForegroundColorAttributeName :[UIColorFromRGB(0Xf0b26e) colorWithAlphaComponent:0.8]
                                  };
    [button_principi setAttributedTitle:[[NSAttributedString alloc] initWithString:principi_text attributes:attributtesH] forState:UIControlStateHighlighted];

    

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!self.isIos7orUpper){
        CGRect fr = self.view.frame;
        fr.origin.y =-20;
        [self.view setFrame:fr];
        // NSLog(@"infoView:%@",self.view);
    }
}



#pragma mark -
#pragma mark Memory Warning
//=========================
// Memory Warning
//=========================
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - GESTIONE ROTAZIONE
//=========================
// GESTIONE ROTAZIONE
//=========================
-(supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}



@end
