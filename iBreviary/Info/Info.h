//
//  Info.h
//  iBreviary 2014
//
//  Created by Leonardo Parenti on 03/2014.
//  Copyright (c) 2014 Leonardo Parenti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "OggettoMailSelector.h"


@protocol InfoDelegate <NSObject>
- (void)removeInfo;
- (void)showInfo_Guida;
- (void)showInfo_Principi;
- (void)showInfo_Credits;
- (void)showInfo_iBreviary;
- (void)showInfo_email;

@end


@interface Info : BaseController<OggettoMailSelectorDelegate>

    
@property (nonatomic, strong) BaseController<InfoDelegate> *delegate;
@property (nonatomic, strong) IBOutlet UIButton *button_credits;
@property (nonatomic, strong) IBOutlet UIButton *button_guida;
@property (nonatomic, strong) IBOutlet UIButton *button_principi;
@property (nonatomic, strong) IBOutlet UIButton *button_ibreviary;
@property (nonatomic, strong) IBOutlet UIButton *button_email;

@property (nonatomic, strong) IBOutlet UILabel *principi_label;
@property (nonatomic, strong) IBOutlet UILabel *credits_label;
@property (nonatomic, strong) IBOutlet UILabel *email_label;
@property (nonatomic, strong) IBOutlet UILabel *website_label;
@property (nonatomic, strong) IBOutlet UIButton *close;

@property (nonatomic, strong) IBOutlet OggettoMailSelector *oggetto_mailCTRL;

@property (strong, nonatomic) IBOutlet UIView *mask;
- (id)initWithDelegate:(BaseController<InfoDelegate>*)d;

@end
